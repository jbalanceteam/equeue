package org.itx.jbalance.admin.gwt.client.services;

import org.itx.jbalance.admin.gwt.shared.JBSecurutyDTO;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface AdminServiceAsync {

	void getSecurity(AsyncCallback<JBSecurutyDTO> callback);

	
}
