package org.itx.jbalance.admin.gwt.client;

import java.util.List;

import org.itx.jbalance.admin.gwt.client.services.AdminService;
import org.itx.jbalance.admin.gwt.client.services.AdminServiceAsync;
import org.itx.jbalance.admin.gwt.client.services.LoginService;
import org.itx.jbalance.admin.gwt.client.services.LoginServiceAsync;
import org.itx.jbalance.admin.gwt.shared.JBSecurutyDTO;
import org.itx.jbalance.admin.gwt.shared.User;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Application shell for Showcase sample.
 */
public class JBAdminShell extends Composite {
	
	private final AdminServiceAsync adminService = GWT.create(AdminService.class);
	/**
	 * The panel that holds the content.
	 */
	@UiField
	SimplePanel contentPanel;

	private static JBAdminShell instance=new JBAdminShell();
	
	private JBAdminShell(){
		
		long startCeation = System.currentTimeMillis();
		EQueueUiBinder uiBinder= GWT.create(EQueueUiBinder.class);
		initWidget(uiBinder.createAndBindUi(this));
		Log.debug("EQueueShell creation - "+(System.currentTimeMillis()-startCeation));
		
		
		adminService.getSecurity(new AsyncCallback<JBSecurutyDTO>() {
			
			@Override
			public void onSuccess(JBSecurutyDTO result) {
				String html="";
				List<User> users = result.getUser();
				for (User u : users) {
					html+=u.getLogin() + "<br/>";
				}
				HTML w = new HTML(html);
				contentPanel.add(w);
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public synchronized static JBAdminShell instance(){
		return instance;
	}

	interface EQueueUiBinder extends UiBinder<Widget, JBAdminShell> {
	}
}
