package org.itx.jbalance.admin.gwt.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface LoginServiceAsync {

	void openSession(AsyncCallback<Void> callback);

	void closeSession(AsyncCallback<Void> callback);

	
}
