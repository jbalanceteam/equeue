package org.itx.jbalance.admin.gwt.client.services;



import org.itx.jbalance.admin.gwt.shared.JBSecurutyDTO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("admin")
public interface AdminService extends RemoteService {

	JBSecurutyDTO getSecurity();


	
	
	
}
