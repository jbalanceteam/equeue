package org.itx.jbalance.admin.gwt.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class JBSecurutyDTO  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<User> user=new ArrayList<User>();

	public List<User> getUser() {
		return user;
	}

	public void setUser(List<User> user) {
		this.user = user;
	}
	
	
}
