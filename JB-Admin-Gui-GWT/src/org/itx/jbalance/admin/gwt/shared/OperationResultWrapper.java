package org.itx.jbalance.admin.gwt.shared;

import java.io.Serializable;

public class OperationResultWrapper<T> implements Serializable{

	private static final long serialVersionUID = 1L;

	public enum Status{
		SUCCESS,VALIDATION_ERROR,SERVER_ERROR;
		
	}
	private T result;
	private Status status;
	private String comments;
	
	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	
	
	
	
}
