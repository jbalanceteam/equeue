package org.itx.jbalance.admin.gwt.shared;

import java.io.Serializable;

public class AddUserRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	private String firstname;
	private String lastname;
	private String login;
	
	
	public String getFirstName() {
		return firstname;
	}
	public void setFirstName(String name) {
		this.firstname = name;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
}
