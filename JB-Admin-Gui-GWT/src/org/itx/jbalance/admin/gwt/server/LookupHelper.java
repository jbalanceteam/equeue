package org.itx.jbalance.admin.gwt.server;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.NoSuchEJBException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.security.auth.login.LoginContext;

import org.itx.jbalance.l1.common.Common;
import org.itx.jbalance.l1.o.JuridicalsRemote;
import org.itx.jbalance.l1.o.OpersRemote;
import org.itx.jbalance.l1.o.PhysicalsRemote;
import org.itx.jbalance.l1.session.Sessionprofile;
import org.itx.jbalance.l1.session.SessionprofileRemote;
import org.jboss.security.auth.callback.UsernamePasswordHandler;

import com.allen_sauer.gwt.log.client.Log;

public class LookupHelper  {
	
	private static Map<String, Common> beans = Collections.synchronizedMap(new HashMap<String, Common>()); 
	
	public void login(String sessionId) {
		SessionprofileRemote sessionProfile = getBean(SessionprofileRemote.class, sessionId);
		try{
		sessionProfile.setUser();
		}catch(NoSuchEJBException e){
			beans.clear();
			login(sessionId);
		}catch(java.lang.reflect.UndeclaredThrowableException e){
			beans.clear();
			login(sessionId);
		}
	}
	
	public JuridicalsRemote getJuridicals(String sessionId){
		return getBean(JuridicalsRemote.class, sessionId);
	}
	
	public PhysicalsRemote getPhysical(String sessionId){
		return getBean(PhysicalsRemote.class, sessionId);
	}
	
	public OpersRemote getOpers(String sessionId){
		return getBean(OpersRemote.class, sessionId);
	}
	

	
	
	@SuppressWarnings("unchecked")
	public <T extends Common>  T getBean(Class<T> c, String sessionId){
		String keyName = getKeyName(c, sessionId);
		if (beans.containsKey(keyName)){
			T res = (T)beans.get(keyName);
//			Проверяем, живой EJB или нет
			try{
//				TODO Возможно нужно использовать другой(специальный?) метод
				res.postactivate();
				return res;
			}catch(javax.ejb.NoSuchEJBException e){
				Log.warn("",e);
				beans.remove(keyName);
				return getBean(c,sessionId);
			}
			
		}
		try{
			UsernamePasswordHandler handler = new UsernamePasswordHandler("admin", "11".toCharArray());
			System.setProperty("java.security.auth.login.config", 
					"/home/apv/projects/sphinx/Queue/JBv5-EQueue/resources/jaas.config");
			try {
				LoginContext lc = new LoginContext("client-login", handler);
				lc.login();
			} catch (Exception e) {
				// TODO: handle exception
			}
			Context context = new InitialContext();
			String simpleName = c.getSimpleName();
			simpleName=simpleName.substring(0,simpleName.indexOf("Remote"));
			T result = (T)context.lookup(simpleName + "/remote");
			
			if(! (result instanceof Sessionprofile))
				result.setRights((Sessionprofile)getBean(SessionprofileRemote.class, sessionId));
			
			beans.put(keyName, result);
			return result; 
		}catch (Throwable e) {
			return null;
		}
		
	}

	private static String getKeyName(Class<?> c, String sessionId) {
		return sessionId + "-" + c.getSimpleName();
	}
	
	
	
	public static LookupHelper getInstance(){
		return new LookupHelper();
	}

	
}
