package org.itx.jbalance.admin.gwt.server;

import java.util.List;

import org.itx.jbalance.admin.gwt.client.services.AdminService;
import org.itx.jbalance.admin.gwt.shared.AddUserRequest;
import org.itx.jbalance.admin.gwt.shared.JBSecurutyDTO;
import org.itx.jbalance.admin.gwt.shared.User;
import org.itx.jbalance.l0.o.Oper;
import org.itx.jbalance.l1.o.OpersRemote;


@SuppressWarnings("serial")
public class AdminServiceImpl extends MyGWTServlet 
	implements AdminService {
	


	@Override
	public JBSecurutyDTO getSecurity() {
		OpersRemote opers = LookupHelper.getInstance().getOpers(getSessionId());
		List<? extends Oper> all = opers.getAll();
		JBSecurutyDTO result=new JBSecurutyDTO();
		for (Oper oper : all) {
			User u = new User();
			u.setFirstName(oper.getRealName());
			u.setLastname(oper.getSurname());
			u.setLogin(oper.getLogin());
			result.getUser().add(u);
		}
		
		return result;
	}
	
	
	public void addUser(AddUserRequest request){
		OpersRemote opersEJB = LookupHelper.getInstance().getOpers(getSessionId());
		Oper oper = new Oper();
		oper.setLogin(request.getLogin());
		oper.setSurname(request.getLastname());
		oper.setRealName(request.getFirstName());
		opersEJB.setObject(oper);
		opersEJB.create();
	}

	
}
