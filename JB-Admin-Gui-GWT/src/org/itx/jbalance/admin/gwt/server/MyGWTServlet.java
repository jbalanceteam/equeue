package org.itx.jbalance.admin.gwt.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


public class MyGWTServlet extends RemoteServiceServlet {

	private static final long serialVersionUID = 1L;

	protected String getSessionId() {
		return this.getThreadLocalRequest().getSession().getId();
	}
	
}
