package org.itx.jbalance;

import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.*;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

public class SeleniumGrid {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;


  private static int N = 10;
  
  
  public void setUp() throws Exception {
      DesiredCapabilities capability = DesiredCapabilities.firefox();
      driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
      baseUrl = "http://admin:admin@192.168.1.11:8080";
      driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
  }

  
  public void test() throws Exception {
    driver.get(baseUrl + "/EQueue/");
    waitAjax(By.id("gwt-uid-6"));

    driver.findElement(By.id("gwt-uid-6")).click();
    driver.findElement(By.id("gwt-uid-3")).click();
    
    
    for(int i=0; i< N; i++){
    
        sleep(1000);
        waitAjax(By.cssSelector("select.gwt-ListBox"));

        new Select(driver.findElement(By.cssSelector("select.gwt-ListBox"))).selectByVisibleText("Льгота есть");
        sleep(1000);
        driver.findElement(By.cssSelector("td > button.gwt-Button")).click();
        sleep(2000);
        driver.findElement(By.cssSelector("img.gwt-Image")).click();
        
        /* Выбор садика */
//        for (int second = 0;; second++) {
//            if (second >= 10) fail("timeout");
//            try { if (isElementPresent(By.id("html body div div div div div div.GLVT0PEBOI div fieldset table tbody tr td table.gwt-DisclosurePanel tbody tr td div div.content div div table tbody tr td span.gwt-CheckBox input"))) 
//                break; } catch (Exception e) {}
//            Thread.sleep(1000);
//        }
        driver.findElement(By.cssSelector("table.gwt-DisclosurePanel tbody tr td div div.content div div table tbody tr td span.gwt-CheckBox input")).click();
        
        
        

        sleep(100);
        driver.findElement(By.cssSelector("td > button.gwt-Button")).click();
        /* Ждем 3 сек */
        sleep(3000);
        driver.findElement(By.xpath("(//button[@type='button'])[5]")).click();
        sleep(100);
        driver.findElement(By.cssSelector("input.gwt-TextBox")).clear();
        sleep(100);
        driver.findElement(By.cssSelector("input.gwt-TextBox")).sendKeys("123");
        sleep(100);
        driver.findElement(By.cssSelector("input.gwt-DateBox")).click();
        sleep(100);
        driver.findElement(By.xpath("//td/table/tbody/tr[3]/td[5]")).click();
        sleep(100);
        driver.findElement(By.cssSelector("button.gwt-Button")).click();
        sleep(100);
        driver.findElement(By.xpath("(//input[@type='text'])[4]")).clear();
        sleep(100);
        driver.findElement(By.xpath("(//input[@type='text'])[4]")).sendKeys("test");
        sleep(100);
        driver.findElement(By.xpath("(//input[@type='text'])[5]")).clear();
        sleep(100);
        driver.findElement(By.xpath("(//input[@type='text'])[5]")).sendKeys("test");
        sleep(100);
        driver.findElement(By.xpath("(//input[@type='text'])[6]")).clear();
        sleep(100);
        driver.findElement(By.xpath("(//input[@type='text'])[6]")).sendKeys("test");
        sleep(100);
        
        driver.findElement(By.cssSelector("html body div div div div div div.GLVT0PEBOI div table tbody tr td span.gwt-RadioButton input")).click();
        
        
        sleep(100);
        driver.findElement(By.xpath("(//input[@type='text'])[8]")).clear();
        sleep(100);
        driver.findElement(By.xpath("(//input[@type='text'])[8]")).sendKeys("123");
        sleep(100);
        driver.findElement(By.xpath("(//input[@type='text'])[9]")).clear();
        sleep(100);
        driver.findElement(By.xpath("(//input[@type='text'])[9]")).sendKeys("234");
        sleep(100);

        // ERROR: Caught exception [ERROR: Unsupported command [getSelectOptions | css=select.gwt-ListBox.GLVT0PEBFJ | ]]
        new Select(driver.findElement(By.cssSelector("select.gwt-ListBox.GLVT0PEBFJ"))).selectByVisibleText("Л1");
        driver.findElement(By.cssSelector("div.GLVT0PEBOI > div > button.gwt-Button")).click();
        sleep(100);   

        final By addConfirm = By.cssSelector("td > button.gwt-Button");
        waitAjax(addConfirm);

        sleep(100);
        driver.findElement(addConfirm).click();
        sleep(1000);
        /* Ждем уведомление об удачном сохранении*/
        waitAjax(By.cssSelector("td > button.gwt-Button"));
        sleep(1500);
        /* Закрываем уведомление об удачном сохранении*/
        driver.findElement(By.cssSelector("td > button.gwt-Button")).click();
        sleep(2000);




        driver.findElement(By.id("gwt-uid-6")).click();
        driver.findElement(By.id("gwt-uid-3")).click();
        sleep(2000);
        driver.findElement(By.cssSelector("input.gwt-TextBox")).clear();
        sleep(100);
        driver.findElement(By.cssSelector("input.gwt-TextBox")).sendKeys("test"+Math.random());
        sleep(100);
        driver.findElement(By.cssSelector("td > button.gwt-Button")).click();
        /* Ждем 3 секу */
        sleep(3000);
    }
    
    
  }

  public void close() {
    driver.quit();
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alert.getText();
    } finally {
      acceptNextAlert = true;
    }
  }

  
  private void sleep(long ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException ex) {
            Logger.getLogger(Selenium.class.getName()).log(Level.SEVERE, null, ex);
        }
  }
  
    private void waitAjax(By by) throws InterruptedException {
        for (int second = 0;; second++) {
                if (second >= 10) fail("timeout");
                try { if (isElementPresent(by)) break; } catch (Exception e) {}
                Thread.sleep(1000);
        }
    }

   
}
