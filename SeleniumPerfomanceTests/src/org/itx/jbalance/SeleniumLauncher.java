/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.itx.jbalance;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apv
 */
public class SeleniumLauncher {

    private static int THREAD_COUNT = 10;
    
    public static void main(String[] args) {
        final Logger logger = Logger.getLogger(SeleniumLauncher.class.getName());
        logger.warning("Launchuing...");
        
        Thread threads[] = new Thread[THREAD_COUNT] ;
        
        for(int i=0; i< THREAD_COUNT;i++){
            final SeleniumGrid seleniumGrid = new SeleniumGrid();
            try {
                seleniumGrid.setUp();
            } catch (Exception ex) {
                    Logger.getLogger(SeleniumLauncher.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            final Runnable oneThread = new Runnable() {
                @Override
                public void run() {

                    try {
                        seleniumGrid.test();
                    } catch (Exception ex) {
                        Logger.getLogger(SeleniumLauncher.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        seleniumGrid.close();
                    }
                }
            };
            
            threads[i] = new Thread(oneThread);
        }
        
          for(int i=0; i< THREAD_COUNT;i++){ 
            threads[i].start();
          }
          
           for(int i=0; i< THREAD_COUNT;i++){ 
            try {
                threads[i].join();
            } catch (InterruptedException ex) {
                Logger.getLogger(SeleniumLauncher.class.getName()).log(Level.SEVERE, null, ex);
            }
           }
          
        
        logger.warning("Finished...");
        
    }
}
