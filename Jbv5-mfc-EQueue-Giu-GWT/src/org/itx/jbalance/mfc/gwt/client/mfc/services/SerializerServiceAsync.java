package org.itx.jbalance.mfc.gwt.client.mfc.services;

import java.io.Serializable;
import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SerializerServiceAsync {
	void serializeToBase64(List<Serializable> obj, AsyncCallback<List<String>> callback);
}
