package org.itx.jbalance.mfc.gwt.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Application shell for Showcase sample.
 */
public class EQueueShell extends Composite {
	

	/**
	 * The panel that holds the content.
	 */
	@UiField
	SimplePanel contentPanel;

	@UiField
	FlowPanel menuPanel;
	
	private static EQueueShell instance=new EQueueShell();
	
	private EQueueShell(){
		EQueueUiBinder uiBinder= GWT.create(EQueueUiBinder.class);
		initWidget(uiBinder.createAndBindUi(this));
		setupMenu();
		setupHistory();
	}

	public synchronized static EQueueShell instance(){
		return instance;
	}

	interface EQueueUiBinder extends UiBinder<Widget, EQueueShell> {
	}
	
	
	MenuItem getReportMI(String miName,final String report){
		return new MenuItem(miName, new Command() {
			@Override
			public void execute() {
				Window.open("equeue/report?type="+report, "blank_", null);
				
			}
		});
	}
	
	
	/**
	 * Set the content to display.
	 * 
	 * @param content
	 *            the content
	 */
	public void setContent(final ContentWidget content) {
		if (content == null) {
			contentPanel.setWidget(null);
		} else {
			
			content.initialize();
			contentPanel.setWidget(content);
		}
	}

	private void setupMenu() {
		MenuBar mainMenu = new MenuBar();
		mainMenu.setAnimationEnabled(true);
		mainMenu.setAutoOpen(true);

		menuPanel.add(mainMenu);

		
		/**/
		MenuBar actionsMB = new MenuBar(true);
		actionsMB.setAnimationEnabled(true);
		actionsMB.setTitle("Действия");
		actionsMB.addItem("Новая регистрация" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_DOU_REQUEST_EDIT));
		actionsMB.addItem("Печать уведомления" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_PRINTNOTIFICATION));
		mainMenu.addItem("Действия", actionsMB);
		/**/
		
		
		
		MenuBar helpMB = new MenuBar(true);
		helpMB.setAnimationEnabled(true);
		helpMB.setTitle("Справка");
		helpMB.addItem("Сайт" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_SITE));
		helpMB.addItem("Вики учебник" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_WIKI));
		helpMB.addItem("Форум" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_FORUM));
		helpMB.addItem("Вопросы" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_FAQ));
		helpMB.addItem("О программе" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_ABOUT));
		
		mainMenu.addItem("Справка", helpMB);
		
		
		mainMenu.addItem("Выход", EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_LOGOUT));
		
	}


	
	void setupHistory() {
		// Setup a history handler to reselect the associate menu item.
		final ValueChangeHandler<String> historyHandler = new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				// Get the content widget associated with the history token.
				Command command = EQueueNavigation.instance().getMenuItemByUrl(event
						.getValue());
				if (command != null)
					command.execute();
				else
					setContent(null);

			}
		};
		History.addValueChangeHandler(historyHandler);

		
	}

}
