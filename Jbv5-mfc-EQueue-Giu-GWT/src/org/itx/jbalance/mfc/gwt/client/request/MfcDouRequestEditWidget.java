package org.itx.jbalance.mfc.gwt.client.request;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.PrivilegeDTO;
import org.itx.jbalance.l2_api.dto.RegionDTO;
import org.itx.jbalance.mfc.gwt.client.ContentWidget;
import org.itx.jbalance.mfc.gwt.client.DateChooserComponent;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;

import eu.maydu.gwt.validation.client.DefaultValidationProcessor;
import eu.maydu.gwt.validation.client.ValidationProcessor;
import eu.maydu.gwt.validation.client.ValidationResult;
import eu.maydu.gwt.validation.client.ValidationResult.ValidationError;
import eu.maydu.gwt.validation.client.Validator;
import eu.maydu.gwt.validation.client.actions.FocusAction;
import eu.maydu.gwt.validation.client.actions.StyleAction;
import eu.maydu.gwt.validation.client.description.PopupDescription;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;
import eu.maydu.gwt.validation.client.validators.standard.NotEmptyValidator;

public class MfcDouRequestEditWidget extends ContentWidget implements
		MfcDouRequestEditPresenter.Display {

	interface Binder extends UiBinder<Widget, MfcDouRequestEditWidget> {

	}

	MfcDouRequestEditPresenter presenter;
	
	// Сведения о ребенке
	@UiField(provided = false)
	TextBox lastNameTextBox;
	@UiField(provided = false)
	TextBox firstNameTextBox;
	@UiField(provided = false)
	TextBox patronymicTextBox;
	@UiField(provided = false)
	RadioButton sexMRadio;
	@UiField(provided = false)
	RadioButton sexWRadio;
	@UiField(provided = false)
	DateChooserComponent birthDateBox;
	@UiField(provided = false)
	TextBox bsSeriaBox;
	@UiField(provided = false)
	TextBox bsNumberBox;
	@UiField(provided = false)
	DateChooserComponent bsDateBox;

	// Детские сады
	@UiField
	SimplePanel dous;
	RaitingDouPanel chooseDouPanel;
	@UiField(provided = true)
	ValueListBox<Integer> yearListBox;

	// Адрес проживания ребенка
	@UiField(provided = false)
	TextBox childAddressTextBox;

	// Дополнительно
	@UiField
	ListBox privilegeListBox;
	@UiField(provided = false)
	TextArea commentsTextArea;
	@UiField(provided = false)
	/**
	 * Заявитель
	 */
	TextBox phoneNumberTextBox;
	
	@UiField(provided = false)
	TextBox emailTextBox;
	
	@UiField(provided = false)
	TextBox declarantLastnameTextBox;
	
	@UiField(provided = false)
	TextBox declarantFirstnameTextBox;
	
	@UiField(provided = false)
	TextBox declarantPatronymicTextBox;
	
	@UiField(provided = false)
	TextBox declarantPassportSeriaTextBox;
	
	@UiField(provided = false)
	TextBox declarantPassportNumberTextBox;

	@UiField(provided = false)
	DateChooserComponent declarantPassportIssueDateBox;
	
	@UiField(provided = false)
	TextBox declarantPassportAuthorityTextBox;
	
	ValidationProcessor validator = new DefaultValidationProcessor();

	@UiField
	Button importButton;
	@UiField
	Button cancelButton;
	@UiField
	Button forwardButton;
	@UiField
	Button clearButton;

	@UiField
	Button printFormButton;
	@UiField
	Button saveButton;
	@UiField
	Button printEvidenceButton;
	@UiField
	Button backButton;
	
	@UiField
	Button setNewButton;

	private List<String> validationMessages= new ArrayList<String>();
	
	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public MfcDouRequestEditWidget() {
		super();
	}

	public void initCreateMode() {
		firstButtonGroupVisible(true);
		secondButtonGroupVisible(false);
		thirdButtonGroupVisible(false);
		sexMRadio.setValue(true);
	}

	void firstButtonGroupVisible(boolean enabled) {
		importButton.setVisible(enabled);
		cancelButton.setVisible(enabled);
		forwardButton.setVisible(enabled);
		clearButton.setVisible(enabled);
	}

	void saveButtonGroupEnabled(boolean enabled) {
		saveButton.setEnabled(enabled);
	}

	void secondButtonGroupVisible(boolean enabled) {
		printFormButton.setVisible(enabled);
		saveButton.setVisible(enabled);
		backButton.setVisible(enabled);
	}
	void thirdButtonGroupVisible(boolean enabled) {
		printEvidenceButton.setVisible(enabled);
		setNewButton.setVisible(enabled);
	}


	 void otherElementsEnable(boolean enabled) {
		lastNameTextBox.setEnabled(enabled);
		firstNameTextBox.setEnabled(enabled);
		patronymicTextBox.setEnabled(enabled);
		sexMRadio.setEnabled(enabled);
		sexWRadio.setEnabled(enabled);
		birthDateBox.setEnable(enabled);
		bsSeriaBox.setEnabled(enabled);
		bsNumberBox.setEnabled(enabled);
		bsDateBox.setEnable(enabled);
		DOM.setElementPropertyBoolean(yearListBox.getElement(), "disabled", !enabled);
		//dous
		chooseDouPanel.setEnable(enabled);
		childAddressTextBox.setEnabled(enabled);
		privilegeListBox.setEnabled(enabled);
		commentsTextArea.setEnabled(enabled);
		phoneNumberTextBox.setEnabled(enabled);
		emailTextBox.setEnabled(enabled);
		
		declarantFirstnameTextBox.setEnabled(enabled);
		declarantLastnameTextBox.setEnabled(enabled);
		declarantPassportAuthorityTextBox.setEnabled(enabled);
		declarantPassportIssueDateBox.setEnable(enabled);
		declarantPassportNumberTextBox.setEnabled(enabled);
		declarantPassportSeriaTextBox.setEnabled(enabled);
		declarantPatronymicTextBox.setEnabled(enabled);
	}

	@Override
	public void initialize() {

		yearListBox = new ValueListBox<Integer>(
				new AbstractRenderer<Integer>() {
					@Override
					public String render(Integer object) {
						if (object == null)
							return "--";
						return object + "";
					}

				});

		Date date = new Date();
		Collection<Integer> years = new ArrayList<Integer>();
		int currentYear = date.getYear();
		for (int y = currentYear + 1900; y < currentYear + 1900 + 8; y++) {
			years.add(y);
		}
		yearListBox.setAcceptableValues(years);

		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));

	
//		DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd.MM.yy");
//		DateBox.Format format = new DateBox.DefaultFormat(dateTimeFormat);

		initValidation();
	}

	private PopupDescription popupDesc;

	private void initValidation() {
		FocusAction focusAction = new FocusAction();
		validator = new DefaultValidationProcessor();
		ValidationMessages messages = new ValidationMessages();

		popupDesc = new PopupDescription(messages);

		validator.addValidators("dousEmpty", new Validator<Validator>() {

			@Override
			public ValidationResult validate(ValidationMessages messages) {
				if (getSelectedDou().isEmpty()) {
					return new ValidationResult("Добавьте ДОУ");
				}
				return null;
			}

			@Override
			public void invokeActions(ValidationResult result) {
				// new StyleAction("validationFailedBorder").invoke(result,
				// choosedDouList);
				if (result != null) {
					ValidationError validationError = result.getErrors().get(0);
					validationMessages.add(validationError.error);
				}
			}
		}.addActionForFailure(focusAction)

		);

		addNotEmptyValidator(firstNameTextBox, focusAction);
		addNotEmptyValidator(lastNameTextBox, focusAction);
		addNotEmptyValidator(patronymicTextBox, focusAction);
		
//		валидация полей заявителя
		addNotEmptyValidator(declarantLastnameTextBox, focusAction);
		addNotEmptyValidator(declarantFirstnameTextBox, focusAction);
		addNotEmptyValidator(declarantPatronymicTextBox, focusAction);
		addNotEmptyValidator(declarantPassportSeriaTextBox, focusAction);
		addNotEmptyValidator(declarantPassportNumberTextBox, focusAction);
		addNotEmptyValidator(declarantPassportAuthorityTextBox, focusAction);
		
//		#304: Валидация полей свидетельства о рождении в EQueue и MFC 
		addDBValidator(focusAction);
		addDeclarantPassportValidator(focusAction);
		
		
		
		validator.addValidators("bsNumber",
				new   Validator<Validator>(){

			@Override
			public ValidationResult validate(ValidationMessages messages) {
				validationFailedStyleAction.reset(bsNumberBox);
				String bs_number = getBsNumberBox().getText();
				if(bs_number == null || bs_number.isEmpty()){
					return new ValidationResult("Заполните поле \"Номер свидетельства о рождении\"");
				}
			
				RegExp bs_number_regEx= RegExp.compile("^[0-9]{3,8}$");
				
				if(!bs_number_regEx.test(bs_number)){
					return new ValidationResult("Номер свидетельства о рождении: Разрешены только цифры. Максимум 8.");
		        }
				return null;
			}
			
			StyleAction validationFailedStyleAction = new StyleAction("validationFailedBorder");

			@Override
			public void invokeActions(ValidationResult result) {
				if(result!=null){
					validationFailedStyleAction.invoke(result, bsNumberBox);
					ValidationError validationError = result.getErrors().get(0);
					validationMessages.add( ""+validationError.error);
				} else {
				}
			}}
			.addActionForFailure(focusAction)
		);
		
		
		
		validator.addValidators("birthdayLessThenSertificateDate",
				new   Validator<Validator<?>>(){
			@Override
			public ValidationResult validate(ValidationMessages messages) {
				validationFailedStyleAction.reset(bsDateBox);
				validationFailedStyleAction.reset(birthDateBox);
				Date bsDate = getBsDateBox().getValue();
				Date birthdayDate = getBirthDate().getValue();
				if(birthdayDate.after(bsDate)){
					return new ValidationResult("Дата выдачи свидетельства о роджении не может быть больше даты рождения");
		        }
				return null;
			}
			
			StyleAction validationFailedStyleAction = new StyleAction("validationFailedBorder");

			@Override
			public void invokeActions(ValidationResult result) {
				if(result != null){
					validationFailedStyleAction.invoke(result, bsDateBox);
					validationFailedStyleAction.invoke(result, birthDateBox);
					ValidationError validationError = result.getErrors().get(0);
					validationMessages.add( ""+validationError.error);
				} else {
				}
			}}
			.addActionForFailure(focusAction)
		);
		
		addNotEmptyValidator(bsSeriaBox, focusAction);
		addNotEmptyValidator(bsNumberBox, focusAction);
		addNotEmptyValidator(childAddressTextBox, focusAction);
		
		validator.addValidators("yearNotEmpty",
				new   Validator<Validator>(){

					@Override
					public ValidationResult validate(ValidationMessages messages) {
						if(getYear()==null || getYear().getValue()==null){
							return new ValidationResult("Выберите планируемый год поступления");
						}
						return null;
					}

					@Override
					public void invokeActions(ValidationResult result) {
						// new StyleAction("validationFailedBorder").invoke(result,
						// choosedDouList);
						if (result != null) {
							ValidationError validationError = result.getErrors().get(0);
							validationMessages.add(validationError.error);
						}
					}
				});

	}

	private void addDBValidator(FocusAction focusAction) {
		validator.addValidators("bsSeria",
				new   Validator<Validator>(){
			@Override
			public ValidationResult validate(ValidationMessages messages) {
				
				validationFailedStyleAction.reset(bsSeriaBox);
				
				String bsSeria = getBsSeriaBox().getText();
				if(bsSeria == null || bsSeria.isEmpty()){
					return new ValidationResult("Заполните поле \"Серия свидетельства о рождении\"");
				}
				if(!bsSeria.contains("-") || bsSeria.indexOf("-") != bsSeria.lastIndexOf("-")) {
					return new ValidationResult("Серия свидетельства о рождении должна состоять из двух частей, разделенных тире (например 'XIV-АГ')");
				}

				RegExp bs_seria_regEx1= RegExp.compile("^[IVXLM]{1,5}-");
				RegExp bs_seria_regEx2= RegExp.compile("-[А-Я]{1,5}$");
				
				if(! bs_seria_regEx1.test(bsSeria)){
					return new ValidationResult("Серия свидетельства о рождении: до черты должны быть римские цифры (латинские заглавные буквы XIV).");
				} else if(! bs_seria_regEx2.test(bsSeria)){
					return new ValidationResult("Серия свидетельства о рождении: после черты должна быть кирилица в верхнем регистре (заглавные русские буквы).");
				}
				return null;
			}

			StyleAction validationFailedStyleAction = new StyleAction("validationFailedBorder");
			
			@Override
			public void invokeActions(ValidationResult result) {
				if(result!=null){
					validationFailedStyleAction.invoke(result, bsSeriaBox);
					ValidationError validationError = result.getErrors().get(0);
					validationMessages.add( ""+validationError.error);
				} else {
				}
			}}
			.addActionForFailure(focusAction)
		);
	}
	
	private void addDeclarantPassportValidator(FocusAction focusAction) {
		validator.addValidators("bsSeria",
				new   Validator<Validator>(){
			@Override
			public ValidationResult validate(ValidationMessages messages) {
				
				validationFailedStyleAction.reset(declarantPassportNumberTextBox);
				validationFailedStyleAction.reset(declarantPassportSeriaTextBox);
				
				String passportNumber = getDeclarantPassportNumber().getText();
				if(passportNumber == null || passportNumber.isEmpty()){
					return new ValidationResult("Заполните поле \"Номер паспорта заявителя\"");
				}
				
				String passportSeria = getDeclarantPassportNumber().getText();
				if(passportSeria == null || passportSeria.isEmpty()){
					return new ValidationResult("Заполните поле \"Серия паспорта заявителя\"");
				}
			

				RegExp declarant_passport_number_regEx1= RegExp.compile("^[\\d]{6}");
				RegExp declarant_passport_seria_regEx1= RegExp.compile("^[\\d]{4}");
				
				
				if(! declarant_passport_number_regEx1.test(passportNumber)){
					return new ValidationResult("Номер паспорта заявителя: 6 цифр.");
				} 
				
				if(! declarant_passport_seria_regEx1.test(passportSeria)){
					return new ValidationResult("Серия паспорта заявителя: 4 цифры.");
				}
				return null;
			}

			StyleAction validationFailedStyleAction = new StyleAction("validationFailedBorder");
			
			@Override
			public void invokeActions(ValidationResult result) {
				if(result!=null){
					validationFailedStyleAction.invoke(result, declarantPassportNumberTextBox);
					validationFailedStyleAction.invoke(result, declarantPassportSeriaTextBox);
					ValidationError validationError = result.getErrors().get(0);
					validationMessages.add( ""+validationError.error);
				} else {
				}
			}}
			.addActionForFailure(focusAction)
		);
	}

	private void addNotEmptyValidator(TextBox textBox, FocusAction focusAction) {
		validator.addValidators(textBox.getName() + "NotEmpty" + Math.random(),
				new NotEmptyValidator(textBox)
						.addActionForFailure(focusAction)
						.addActionForFailure(new StyleAction("validationFailedBorder")));
	}

	static class DouCell extends AbstractCell<DouDTO> {

		/**
		 * The html of the image used for contacts.
		 */
		// private final String imageHtml;

		public DouCell() {
			// this.imageHtml = AbstractImagePrototype.create(image).getHTML();
		}

		@Override
		public void render(Context context, DouDTO value, SafeHtmlBuilder sb) {
			// Value can be null, so do a null check..
			if (value == null) {
				return;
			}

			sb.appendHtmlConstant("<table colspacing='0' rowspacing='0' class='request-dou-list'>");

			// Add the contact image.
			sb.appendHtmlConstant("<tr><td class='request-dou-name'>");
			sb.appendHtmlConstant(value.getName());
			sb.appendHtmlConstant("</td></tr>");

			
			sb.appendHtmlConstant("</table>");
		}
	}

	public MfcDouRequestEditPresenter getPresenter() {
		return presenter;
	}

	public void setPresenter(MfcDouRequestEditPresenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public HasText getLastName() {
		return lastNameTextBox;
	}

	@Override
	public HasText getFirstName() {
		return firstNameTextBox;
	}

	@Override
	public HasText getPatronymic() {
		return patronymicTextBox;
	}

	@Override
	public HasValue<Date> getBirthDate() {
		return birthDateBox;
	}

	@Override
	public HasText getBsSeriaBox() {
		return bsSeriaBox;
	}

	@Override
	public HasText getBsNumberBox() {
		return bsNumberBox;
	}

	@Override
	public HasValue<Date> getBsDateBox() {
		return bsDateBox;
	}

	public void setPrivileges(List<PrivilegeDTO> privileges) {
		privilegeListBox.addItem(" без льгот ");
		for (PrivilegeDTO p : privileges) {
			privilegeListBox.addItem(p.getName());
		}
	}

	@Override
	public void setAvailableDou(Map<RegionDTO, List<DouDTO>> dous) {
		chooseDouPanel = new RaitingDouPanel(dous, null);
		this.dous.add(chooseDouPanel);
	}

	
	@UiHandler("cancelButton")
	public void cancel(ClickEvent event) {
		// EQueueNavigation.instance().goToDouRequestsListPage(null);
	}

	@UiHandler("clearButton")
	public void clear(ClickEvent event) {
		getPresenter().clearForm();
	}

	@UiHandler("forwardButton")
	public void forward(ClickEvent event) {
		validationMessages.clear();
		// локальная валидация
		boolean validate = validator.validate();
		if(validate)
			// серверная проверка
			getPresenter().checkDousRequest();
		else{
			StringBuffer sb = new StringBuffer();
			for (String m : validationMessages) {
				sb.append(m);
				sb.append("<br/>");
			}
			showWarningNotification(sb.toString());
		}
	}
	
	@UiHandler("importButton")
	public void importXml(ClickEvent event) {
		final FormPanel form = new FormPanel();
		form.setAction("equeueMfc/xmlUpload");
		// Because we're going to add a FileUpload widget, we'll need to set the
		// form to use the POST method, and multipart MIME encoding.
		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_POST);
		
		
		final DialogBox dialogBox = new DialogBox();
	    dialogBox.ensureDebugId("cwDialogBox");
		dialogBox.setText("Импорт из XML");


	    // Create a table to layout the content
	    DockPanel dialogContents = new DockPanel();
	    dialogContents.setSpacing(4);
	    dialogContents.setWidth("600px");
	    dialogContents.setHeight("200px");
//	    dialogBox.setWidget();

	    dialogBox.setWidget(form);
	    form.add(dialogContents);
	    HTML details = new HTML("Выберите файл для загрузки:");
	    details.setWidth("490px");
	    

	 // Create a FileUpload widget.
		FileUpload fileUpload = new FileUpload();
		fileUpload.setName("fileUpload");
		
		
		
		
//		String uuid = org.itx.jbalance.equeue.gwt.client.UUID.uuid();
//		Hidden hidden = new Hidden("key", uuid);
		
		FlowPanel flowPanel1 = new FlowPanel();
		dialogContents.add(flowPanel1, DockPanel.CENTER);
		flowPanel1.add(details);
		flowPanel1.add(fileUpload);
//		flowPanel1.add(hidden);
		
	    
	    Button okButton = new Button("Ok");
	    okButton.setWidth("100px");
	    ClickHandler okHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Log.debug("Befor submit");
				form.submit();
				Log.debug("After submit");
			}
		};
		okButton.addClickHandler(okHandler);
		
		
		Button cancelButton = new Button("Отмена");
	    cancelButton.setWidth("100px");
	    ClickHandler cancelHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		};

		cancelButton.addClickHandler(cancelHandler);
		
//		buttonsPanel.add(okButton);
//		dialogContents.add(buttonsPanel, DockPanel.SOUTH);
		FlowPanel flowPanel = new FlowPanel();
		flowPanel.add(okButton);
		flowPanel.add(cancelButton);
		dialogContents.add(flowPanel, DockPanel.SOUTH);
		
		form.addSubmitCompleteHandler(new SubmitCompleteHandler() {
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				String results = event.getResults();
				dialogBox.hide();
//				showInfoNotification("Вот что мы импортируем:" + results + "\n" );
				getPresenter().populateFormUsingXml(results);
			}
		});
		
		
//		Do some actions to show our modal panel
	    dialogBox.setModal(true);
	    dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);
	    dialogBox.center();
	    dialogBox.show();
	}
	
	
	@UiHandler("backButton")
	public void backButton(ClickEvent event) {
		getPresenter().setState(State.EDITED);
		firstButtonGroupVisible(true);
		secondButtonGroupVisible(false);
		//thirdButtonGroupVisible(false);
		otherElementsEnable(true);
	}


	@UiHandler("printEvidenceButton")
	public void printEvidenceButton(ClickEvent event) {
		getPresenter().runEvidenceReport();
			} 
	
	@UiHandler("printFormButton")
	public void printFormButton(ClickEvent event) {
		getPresenter().runChildCheckInfoBeforeDouRequestUrl();
//				 Window.open(urlStr, "blank_", null);
		
	} 
	
	
	
	public void disableUi(boolean enabled){
		importButton.setEnabled(enabled);
		cancelButton.setEnabled(enabled);
		forwardButton.setEnabled(enabled);
		clearButton.setEnabled(enabled);
		printFormButton.setEnabled(enabled);
		saveButton.setEnabled(enabled);
		printEvidenceButton.setEnabled(enabled);
		backButton.setEnabled(enabled);
		setNewButton.setEnabled(enabled);
		if(enabled){
			showDefaultCursor();
		} else {
			showWaitCursor();
		}
	}
	

	@UiHandler("saveButton")
	public void save(ClickEvent event) {
		validationMessages.clear();
		boolean validate = validator.validate();
		if(validate)
			getPresenter().makeDousRequest();
		else{
			StringBuffer sb = new StringBuffer();
			for (String m : validationMessages) {
				sb.append(m);
				sb.append("<br/>");
			}
			showWarningNotification(sb.toString());
		}
	}

	@UiHandler("setNewButton")
	public void setNew(ClickEvent event) {
		getPresenter().setState(State.EDITED);
		getPresenter().clearForm();
		firstButtonGroupVisible(true);
		secondButtonGroupVisible(false);
		thirdButtonGroupVisible(false);
		otherElementsEnable(true);
	}
	
	
	
	@Override
	public PrivilegeDTO getPrivilege() {
		if (privilegeListBox.getSelectedIndex() == 0)
			return null;
		else {

			return presenter.getAllPrivileges().get(
					privilegeListBox.getSelectedIndex() - 1);
		}
	}

	public void setPrivilege(PrivilegeDTO privilege) {
		Log.debug("setPrivilege(" + privilege + ")");
		if (privilege == null)
			privilegeListBox.setSelectedIndex(0);
		else {

			List<PrivilegeDTO> allPrivileges = presenter.getAllPrivileges();
			Log.debug("      allPrivileges = " + allPrivileges);
			privilegeListBox
					.setSelectedIndex(allPrivileges.indexOf(privilege) + 1);
		}

	}

	@Override
	public HasText getComments() {
		return commentsTextArea;
	}

	@Override
	public HasText getPhoneNumber() {
		return phoneNumberTextBox;
	}
	
	public HasText getDeclarantFirstname(){
		return declarantFirstnameTextBox;
	}
	
	public HasText getDeclarantLastname(){
		return declarantLastnameTextBox;
	}
	
	public HasText getDeclarantPassportAuthority(){
		return declarantPassportAuthorityTextBox;
	}
	
	public HasText getDeclarantPassportSeria(){
		return declarantPassportSeriaTextBox;
	}
	
	public HasText getDeclarantPassportNumber(){
		return declarantPassportNumberTextBox;
	}
	
	public HasValue<Date> getDeclarantPassportIssueDate(){
		return declarantPassportIssueDateBox;
	}
	
	public HasText getDeclarantPatronymic(){
		return declarantPatronymicTextBox;
	}
	
	@Override
	public HasText getEmail() {
		return emailTextBox;
	}
	

	public HasValue<Integer> getYear() {
		return yearListBox;
	}

	// public HasHTML getFormTitle() {
	// return title;
	// }

	public char getSex() {
		return sexMRadio.getValue() ? 'm' : 'f';
	}

	public void setSex(Character sex) {
		if (sex == null) {
			sexMRadio.setValue(true);
			sexWRadio.setValue(false);
		} else
			switch (sex) {
			case 'm':
				sexMRadio.setValue(true);
				break;
			case 'f':
				sexWRadio.setValue(true);
				break;
			default:
				sexMRadio.setValue(true);
				sexWRadio.setValue(false);
			}

	}

	public DialogBox createDialogBox(int matchFio, int matchFioAndAge,
			ClickHandler ignore, ClickHandler backToEdit,
			ClickHandler showMatches) {
		// Create a dialog box and set the caption text
		final DialogBox dialogBox = new DialogBox();
		dialogBox.ensureDebugId("cwDialogBox");
		dialogBox.setText("Внимание!!!");

		// Create a table to layout the content
		VerticalPanel dialogContents = new VerticalPanel();
		dialogContents.setSpacing(4);
		dialogBox.setWidget(dialogContents);

		// Add some text to the top of the dialog
		HTML details = new HTML("Найдены совпадения: <br/>"
				+ "Фамилия + имя + отчество = " + matchFio + "<br/>"
				+ "Фамилия + имя + отчество + дата рождения = "
				+ matchFioAndAge + "" + "<br/><br/> ");
		dialogContents.add(details);
		dialogContents.setCellHorizontalAlignment(details,
				HasHorizontalAlignment.ALIGN_CENTER);

		HorizontalPanel buttonsPanel = new HorizontalPanel();

		Button ignoreButton = new Button("Все равно добавить", ignore);
		ClickHandler closeHandler = new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		};
		ignoreButton.addClickHandler(closeHandler);
		buttonsPanel.add(ignoreButton);

		Button backToeditButton = new Button("Вернуться к редактированию",
				backToEdit);
		backToeditButton.addClickHandler(closeHandler);
		buttonsPanel.add(backToeditButton);

		Button showMatchesButton = new Button("Показать совпадения",
				showMatches);
		showMatchesButton.addClickHandler(closeHandler);
		buttonsPanel.add(showMatchesButton);

		dialogContents.add(buttonsPanel);

		return dialogBox;
	}

	public Map<DouDTO, Integer> getSelectedDou() {
		return chooseDouPanel.getSelectedDou();
	}

	public Map<Long, Integer> getSelectedDousUids() {
		Map<Long, Integer> selectedDousUids =new HashMap<Long, Integer>();
		for(Entry<DouDTO, Integer> entry : chooseDouPanel.getSelectedDou().entrySet())
			selectedDousUids.put(entry.getKey().getUId(), entry.getValue());
		return selectedDousUids;
	}
	
	public void chooseOneDou(DouDTO dou, Integer reting) {
		chooseDouPanel.selectOne(dou, reting);
	}

	public void removeAllDou() {
		chooseDouPanel.clearSelection();
	}

	@Override
	public HasText getChildAddress() {
		return childAddressTextBox;
	}

}
