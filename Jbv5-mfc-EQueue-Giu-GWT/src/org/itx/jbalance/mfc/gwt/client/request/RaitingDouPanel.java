package org.itx.jbalance.mfc.gwt.client.request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.RegionDTO;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
/**
 * Компонент, который позволяет проставлять рейтинг ДОУ
 * Группирует ДОУ по регионам
 * Используется при добавлении ДОУ
 * @author apv
 *
 */
public class RaitingDouPanel extends SimplePanel{

	private List<RegionDTO> regions; 
	private List<CB_DOU_REG> cbDouList; 
	private SelectDouListener selectListener;
	private ListBox regionLB = new ListBox();
	{
		regionLB.setTabIndex(99);
	}
	Grid grid=new  Grid();
	private static final int COLS = 4;
	
	private Button cleanSelection;
	private Button selectAll;
	
	public RaitingDouPanel(final Map<RegionDTO, List<DouDTO>> dous, final SelectDouListener selectListener) {
		super();
		this.selectListener = selectListener;
		
		cbDouList = new ArrayList<RaitingDouPanel.CB_DOU_REG>();
		regions = new ArrayList<RegionDTO>(dous.keySet().size());
		for(Entry<RegionDTO, List<DouDTO>>e: dous.entrySet()){
			regions.add(e.getKey());
			for (DouDTO dou : e.getValue()) {
				
				MyRating raiting = new MyRating(dou.getNumber(), dou.getName());
				
				
				raiting.addValueChangeHandler(new ValueChangeHandler<Integer>() {
					@Override
					public void onValueChange(ValueChangeEvent<Integer> event) {
						if(selectListener!=null)
							selectListener.selectionChanged(getSelectedDou());
					}
				});
				
				cbDouList.add(new CB_DOU_REG(dou, e.getKey(), raiting));
			}
		}
		
		Collections.sort(regions, new Comparator<RegionDTO>() {

			@Override
			public int compare(RegionDTO o1, RegionDTO o2) {
				if(o1==null || o1.getName()==null)
					return -1;
				if(o2==null || o2.getName()==null)
					return 1;
				return o1.getName().compareTo(o2.getName());
			}
		});
		
		Collections.sort(cbDouList, new Comparator<CB_DOU_REG>() {
			@Override
			public int compare(CB_DOU_REG o1, CB_DOU_REG o2) {
				return o1.dou.getNumber().compareTo(o2.dou.getNumber());
			}
		});
		
		
		initComponents();
		initListeners();
		updateGrid(null);
	}

	
	public RegionDTO getSelectedRegion(){
		String value = regionLB.getValue(regionLB.getSelectedIndex());
		RegionDTO selectedRegion = null;
		if(value .equals("all")){
			selectedRegion = null;
		}else{
			
			for(RegionDTO r: regions){
				if(value.equals(r.getUId().toString())){
					selectedRegion = r;
				}
			}
			if(selectedRegion == null){
				throw new RuntimeException("How did this happen?");
			}
		}
		return selectedRegion;
	}

	public void selectAllByRegion(RegionDTO region, Integer value){
		if(region == null){
			for (CB_DOU_REG item : cbDouList) {
				item.rating.setValue(value);
			}
		}else{
			for (CB_DOU_REG item : getItemsByRegion(region)) {
				item.rating.setValue(value);
			}
		}
	}
	
	private void initListeners() {
		regionLB.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
					updateGrid(getSelectedRegion());
					if(selectListener!=null)
						selectListener.selectionChanged(getSelectedDou());
			}
		});
	}
	
	
	void initComponents(){
		regionLB.addItem("Все", "all");
		
		for (RegionDTO region : regions) {
			regionLB.addItem(region.getName(), region.getUId().toString());
		}
		
		grid.setVisible(true);
		
		DockLayoutPanel dockLayoutPanel = new DockLayoutPanel(Unit.EM);
		dockLayoutPanel.addNorth(regionLB, 2);
		
		FlowPanel buttonsPanel = new FlowPanel();
		dockLayoutPanel.addSouth(buttonsPanel, 2);
		selectAll = new Button("Выбрать все");
		selectAll.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				selectAllByRegion(getSelectedRegion(), 2);
				if(selectListener!=null)
					selectListener.selectionChanged(getSelectedDou());
			}
		});
		buttonsPanel.add(selectAll);
		cleanSelection = new Button("Снять выделение");
		cleanSelection.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				selectAllByRegion(getSelectedRegion(), 0);
				if(selectListener!=null)
					selectListener.selectionChanged(getSelectedDou());
			}
		});
		buttonsPanel.add(cleanSelection);
		
		dockLayoutPanel.add(grid);
		int height = 16 * (cbDouList.size()/COLS  +  (cbDouList.size()%COLS!=0?1:0) )+100;
		dockLayoutPanel.setSize("480px", height+  "px");
		
		
		setWidth("520px");
		setHeight((height + 10) +  "px");
		this.add(dockLayoutPanel);
	}
	
	public List<DouDTO> getAllDous(){
		 List<DouDTO> res = new ArrayList<DouDTO>();
		 for (CB_DOU_REG  item : cbDouList ) {
			 res.add(item.dou);
		 }
		 return res;
	}
	
	private void updateGrid(RegionDTO reg){
		Log.info(reg+"");
		
		grid.clear();
		
		List<CB_DOU_REG> itemsByRegion ; 
		if(reg == null){
			 itemsByRegion = cbDouList;
		}else{
			 itemsByRegion = getItemsByRegion(reg);
		}
		Log.info("itemsByRegion: "+itemsByRegion);
		
		grid.resize(itemsByRegion.size() / COLS +1, 6);
		int i=0;
		for (CB_DOU_REG item : itemsByRegion) {
		//	Log.info(""+item + " "+i/5 + " "+i%5);
			grid.setWidget(i/COLS, i%COLS, item.rating);
			i++;
		}
	}
	
	private List<CB_DOU_REG>getItemsByRegion(RegionDTO reg){
		List<CB_DOU_REG> res=new ArrayList<CB_DOU_REG>();
		for(CB_DOU_REG item: cbDouList){
			if(item.region.equals(reg)){
				res.add(item);
			}
		}
		return res;
	}
	
	
	public boolean isAllSelected(){
		for(CB_DOU_REG item: cbDouList){
			if(item.rating.getValue() <= 0){
				return false;
			}
		}
		return true;
	}
	
	public boolean isAllUnselected(){
		for(CB_DOU_REG item: cbDouList){
			if(item.rating.getValue() > 0){
				return false;
			}
		}
		return true;
	}
	
	public Map<DouDTO, Integer> getSelectedDou(){
		Map<DouDTO, Integer> res=new HashMap<DouDTO, Integer>();
		for(CB_DOU_REG item: cbDouList){
			if(item.rating.getValue() > 0){
				res.put(item.dou, item.rating.getValue());
			}
		}
		return res;
	}
	
	
	public interface SelectDouListener{
		void selectionChanged(Map<DouDTO, Integer>selected);
	}
	
	
	public void clearSelection(){
		for(CB_DOU_REG item: cbDouList){
			item.rating.setValue(0);
		}
	}
	
	public void setEnable(boolean enabled){
		for(CB_DOU_REG dou_REG : cbDouList){
			dou_REG.rating.setEnabled(enabled);
		}
		regionLB.setEnabled(enabled);
		selectAll.setEnabled(enabled);
		cleanSelection.setEnabled(enabled);
	}
	
	public void selectOne(DouDTO dou, Integer value){
		for(CB_DOU_REG item: cbDouList){
			if(item.dou.equals(dou)){
				item.rating.setValue(value);
				break;
			}
		}
	}
	
	public void selectByUids(Map<Long, Integer> ratingMap){
		for (Entry<Long, Integer> entry : ratingMap.entrySet()) {
			for(CB_DOU_REG item: cbDouList){
				if(item.dou.getUId().equals(entry.getKey())){
					item.rating.setValue(entry.getValue());
					break;
				}
			}
		}
		if(selectListener!=null)
			selectListener.selectionChanged(getSelectedDou());
	}
	
	
	class CB_DOU_REG{
		CB_DOU_REG(DouDTO dou, RegionDTO region, MyRating rating){
			this.rating = rating;
			this.dou = dou;
			this.region = region;
		}
		
		MyRating rating;
		DouDTO dou;
		RegionDTO region;
		@Override
		public String toString() {
			return "CB_DOU_REG [checkBox=" + rating + ", dou=" + dou
					+ ", region=" + region + "]";
		}
		
		
	}
	

}
