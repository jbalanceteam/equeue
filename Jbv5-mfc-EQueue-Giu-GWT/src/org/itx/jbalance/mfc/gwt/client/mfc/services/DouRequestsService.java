package org.itx.jbalance.mfc.gwt.client.mfc.services;


import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.DouRequestResultDTO;
import org.itx.jbalance.l2_api.dto.ResultDTO;
import org.itx.jbalance.l2_api.dto.DouRequestPrerequisiteDTO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("douRequests")
public interface DouRequestsService extends RemoteService {

	/**
	 * метод возвращает все данные которые могут понадобиться для составления
	 * запроса о постановке ребенка на учет
	 * 
	 * @return DouRequestPrerequisiteDTO
	 */
	 DouRequestPrerequisiteDTO getDouRequestPrerequisite();

	/**
	 * метод проверяет стоит ли ребенок уже в очереди возвращает
	 * ResultDTO.SUCCESS если ребенок в очереди не стоит
	 * 
	 * @param dousRequestDTO
	 * @return ResultDTO
	 */
	 ResultDTO checkDousRequest(DouRequestDTO dousRequestDTO);

	/**
	 * метод ставит ребенка в очередь
	 * 
	 * @param dousRequestDTO
	 * @return ResultDTO
	 */
	 DouRequestResultDTO makeDousRequest(DouRequestDTO dousRequestDTO);

}
