package org.itx.jbalance.mfc.gwt.client.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.DouRequestPrerequisiteDTO;
import org.itx.jbalance.l2_api.dto.DouRequestResultDTO;
import org.itx.jbalance.l2_api.dto.PrivilegeDTO;
import org.itx.jbalance.l2_api.dto.RegionDTO;
import org.itx.jbalance.l2_api.dto.ResultDTO;
import org.itx.jbalance.mfc.gwt.client.Callback;
import org.itx.jbalance.mfc.gwt.client.MyPresenter;
import org.itx.jbalance.mfc.gwt.client.mfc.services.DouRequestsService;
import org.itx.jbalance.mfc.gwt.client.mfc.services.DouRequestsServiceAsync;
import org.itx.jbalance.mfc.gwt.client.mfc.services.SerializerService;
import org.itx.jbalance.mfc.gwt.client.mfc.services.SerializerServiceAsync;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class MfcDouRequestEditPresenterImpl extends
		MyPresenter<MfcDouRequestEditWidget> implements MfcDouRequestEditPresenter {

	public MfcDouRequestEditPresenterImpl(MfcDouRequestEditWidget display) {
		super(display);
	}

	private final DouRequestsServiceAsync requestsService = GWT
			.create(DouRequestsService.class);

	public void setPrivileges(List<PrivilegeDTO> privileges) {
		this.privileges = privileges;
	}

	private List<PrivilegeDTO> privileges;

	Map<RegionDTO, List<DouDTO>> dous;
	State state;

	DouRequestDTO douRequestDTO;
	DouRequestResultDTO douRequestResultDTO;

	public DouRequestResultDTO getDouRequestResultDTO() {
		return douRequestResultDTO;
	}

	public DouRequestDTO getDouRequestDTO() {
		return douRequestDTO;
	}

	@Override
	public State getState() {
		return state;
	}

	@Override
	public void setState(State state) {
		this.state = state;
	}

	public String runChildCheckInfoBeforeDouRequestUrl() {
		SerializerServiceAsync serializerServiceAsync= GWT.create(SerializerService.class);
		List<Serializable>list = new ArrayList<Serializable>(1);
		list.add( getDouRequestDTO());
		serializerServiceAsync.serializeToBase64( list, new AsyncCallback<List<String>>() {
			
			@Override
			public void onSuccess(List<String> result) {
				getDisplay().saveButtonGroupEnabled(true);
				String urlStr = "equeue/report?type=childCheckInfoBeforeDouRequest";
				urlStr+="&douRequestDto="+result.get(0);
				 Window.open(urlStr, "blank_", null);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Ошибка при генерации отчета!");
				Window.alert(caught.getMessage());
				Window.alert(caught.getStackTrace()[0].toString());
				Window.alert(caught.getStackTrace()[1].toString());
				Window.alert(caught.getStackTrace()[2].toString());
				Window.alert(caught.getStackTrace()[3].toString());
				caught.printStackTrace();
			}
		});
		return "";
	}

	@Override
	public void runEvidenceReport() {
		
		SerializerServiceAsync serializerServiceAsync= GWT.create(SerializerService.class);
		List<Serializable>list = new ArrayList<Serializable>(1);
		list.add( getDouRequestDTO());
		list.add( getDouRequestResultDTO());
		serializerServiceAsync.serializeToBase64(list , new AsyncCallback<List<String>>() {
			
			@Override
			public void onSuccess(List<String> result) {
				String urlStr = "equeue/report?type=registrationNotification";
				urlStr+="&douRequestDto="+result.get(0);
				urlStr+="&douRequestResultDTO="+result.get(1);
				 Window.open(urlStr, "blank_", null);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Ошибка при генерации отчета!");
				Window.alert(caught.getMessage());
				Window.alert(caught.getStackTrace()[0].toString());
				Window.alert(caught.getStackTrace()[1].toString());
				Window.alert(caught.getStackTrace()[2].toString());
				Window.alert(caught.getStackTrace()[3].toString());
				caught.printStackTrace();
			}
		});
		
	}

	DouRequestDTO initDouRequestDTO() {
		douRequestDTO = new DouRequestDTO();
		douRequestDTO.setFirstName(getDisplay().getFirstName().getText());
		douRequestDTO.setLastName(getDisplay().getLastName().getText());
		douRequestDTO.setPatronymic(getDisplay().getPatronymic().getText());
		douRequestDTO.setSex(getDisplay().getSex());
		douRequestDTO.setBirthDate(getDisplay().getBirthDate().getValue());
		douRequestDTO.setBsSeria(getDisplay().getBsSeriaBox().getText());
		douRequestDTO.setBsNumber(getDisplay().getBsNumberBox().getText());
		douRequestDTO.setBsDate(getDisplay().getBsDateBox().getValue());
		douRequestDTO.setDouUids(getDisplay().getSelectedDousUids());
		douRequestDTO.setYearEnter(getDisplay().getYear().getValue());
		douRequestDTO.setChildAddress(getDisplay().getChildAddress().getText());
		final Long privilegeUid = getDisplay().getPrivilege() == null ? null: getDisplay().getPrivilege().getUId();
		douRequestDTO.setPrivilegeUid(privilegeUid);
		douRequestDTO.setComments(getDisplay().getComments().getText());
		douRequestDTO.setPhoneNumber(getDisplay().getPhoneNumber().getText());
		douRequestDTO.setEmail(getDisplay().getEmail().getText());
		
		douRequestDTO.setDeclarantFirstname(getDisplay().getDeclarantFirstname().getText());
		douRequestDTO.setDeclarantLastname(getDisplay().getDeclarantLastname().getText());
		douRequestDTO.setDeclarantPatronymic(getDisplay().getDeclarantPatronymic().getText());
		douRequestDTO.setDeclarantPassportSeria(getDisplay().getDeclarantPassportSeria().getText());
		douRequestDTO.setDeclarantPassportNumber(getDisplay().getDeclarantPassportNumber().getText());
		douRequestDTO.setDeclarantPassportAuthority(getDisplay().getDeclarantPassportAuthority().getText());
		douRequestDTO.setDeclarantPassportIssue(getDisplay().getDeclarantPassportIssueDate().getValue());
		
		return douRequestDTO;

	}

	@Override
	public void checkDousRequest() { // final Callback callback
		getDisplay().disableUi(false);
		requestsService.checkDousRequest(initDouRequestDTO(),
				new AsyncCallback<ResultDTO>() {
					@Override
					public void onSuccess(ResultDTO result) {
						if (result.getStatus() == ResultDTO.Status.SUCCESS) {
							setState(State.CHECKED);
							getDisplay().firstButtonGroupVisible(false);
							getDisplay().secondButtonGroupVisible(true);
							getDisplay().saveButtonGroupEnabled(false);
							// getDisplay().thirdButtonGroupVisible(false);
							getDisplay().otherElementsEnable(false); // можно закомментить и не выдавать сообщение об
																		// успешной проверке
							getDisplay().showInfoNotification(
									result.getMessage());
						} else {
							getDisplay().showInfoNotification(
									result.getMessage());
						}
						getDisplay().disableUi(true);
					}

					@Override
					public void onFailure(Throwable caught) {
						Log.error("", caught);
						Window.alert(caught.getLocalizedMessage());
						getDisplay().disableUi(true);
					}
				});
	}

	@Override
	public void makeDousRequest() { // { DouRequestDTO dousRequestDTO, Callback
		getDisplay().disableUi(false);
		requestsService.makeDousRequest(getDouRequestDTO(),
				new AsyncCallback<DouRequestResultDTO>() {
					@Override
					public void onSuccess(DouRequestResultDTO result) {
						if (result.getStatus() == DouRequestResultDTO.Status.SUCCESS) {
							setState(State.SAVED);
							// getDisplay().firstButtonGroupVisible(true);//true
							getDisplay().secondButtonGroupVisible(false);
							getDisplay().thirdButtonGroupVisible(true);
							// getDisplay().otherElementsEnable(true);
							//clearForm();
							douRequestResultDTO = result;
							getDisplay().showInfoNotification(
									result.getMessage());
						} else {
							getDisplay().showInfoNotification(
									"Произошла ошибка сохранения.");
						}
						getDisplay().disableUi(true);
					}

					@Override
					public void onFailure(Throwable caught) {
						Log.error("", caught);
						Window.alert(caught.getLocalizedMessage());
						getDisplay().disableUi(true);
					}
				}); 
	}

	@Override
	public void initialize() {
		initialize(null);
	}

	@Override
	public void initialize(final Callback callback) {
		getDisplay().disableUi(false);
		requestsService
				.getDouRequestPrerequisite(new AsyncCallback<DouRequestPrerequisiteDTO>() {
					@Override
					public void onSuccess(DouRequestPrerequisiteDTO result) {
						// отображение льгот
						setPrivileges(result.getPrivilegeDtos());
						getDisplay().setPrivileges(result.getPrivilegeDtos());
						// и садиков с их районами
						dous = new HashMap<RegionDTO, List<DouDTO>>();
						for (RegionDTO regionDto : result.getRegionsDTOs()) {
							List<DouDTO> douDTOs = new ArrayList<DouDTO>();
							for (DouDTO douDTO : result.getDousDTOs()) {
								if (douDTO.getRegionUid() == regionDto.getUId())
									douDTOs.add(douDTO);
							}
							if (douDTOs.size() != 0)
								dous.put(regionDto, douDTOs);
						}
						getDisplay().setAvailableDou(dous);
						getDisplay().disableUi(true);
						callback.callback();
					}

					@Override
					public void onFailure(Throwable caught) {
						Log.error("", caught);
						Window.alert(caught.getLocalizedMessage());
					}
				});

	}

	@Override
	public void clearForm() {

		getDisplay().getLastName().setText("");
		getDisplay().getFirstName().setText("");
		getDisplay().getPatronymic().setText("");
		getDisplay().getBirthDate().setValue(new Date());
		getDisplay().setSex('m');

		getDisplay().getBsSeriaBox().setText("");
		getDisplay().getBsNumberBox().setText("");
		getDisplay().getBsDateBox().setValue(new Date());
		getDisplay().getYear().setValue(null);

		getDisplay().getChildAddress().setText("");
		getDisplay().getComments().setText("");

		getDisplay().setPrivilege(null);
		getDisplay().getPhoneNumber().setText("");
		getDisplay().getEmail().setText("");
		
		getDisplay().getDeclarantFirstname().setText("");
		getDisplay().getDeclarantLastname().setText("");
		getDisplay().getDeclarantPatronymic().setText("");
		getDisplay().getDeclarantPassportSeria().setText("");
		getDisplay().getDeclarantPassportNumber().setText("");
		getDisplay().getDeclarantPassportIssueDate().setValue(new Date());
		getDisplay().getDeclarantPassportAuthority().setText("");
		
		getDisplay().removeAllDou();
	}

	@Override
	public List<PrivilegeDTO> getAllPrivileges() {
		return privileges;
	}

	
	private String getVal(String xml, String tag){
		final RegExp pattern = RegExp.compile("<"+tag+">([\\S\\s]*)<\\/"+tag+">", "i");
		MatchResult matcher = pattern.exec(xml);
		final String res;
		if(matcher != null){
			res = matcher.getGroup(1);
		} else {
			res = null;
		}
		Log.debug(tag + ": "+res);
		return res;
	}
	
	DateTimeFormat ISO_DATE_FORMAT =DateTimeFormat.getFormat("yyyy-MM-dd");
	private Date getValDate(String xml, String tag){
		try {
			String val = getVal(xml, tag);
			return val==null? null: ISO_DATE_FORMAT.parse(val);
		} catch (Exception e) {
			Window.alert(e.getMessage());
			return null;
		}
	}
	
	private Integer getValInt(String xml, String tag){
		try {
			String val = getVal(xml, tag);
			return val==null? null: new Integer(val);
		} catch (Exception e) {
			Window.alert(e.getMessage());
			return null;
		}
	}
	
	@Override
	public void populateFormUsingXml(String xml) {
		getDisplay().disableUi(false);
		Log.debug("xml: " + xml);
		 clearForm();
//		getDisplay().getOldNumber().setText();
//		getDisplay().getRegistrationDate().setValue();
//		getDisplay().getRegistrationDateLabel().setHTML(DateTimeFormat.getFormat("dd.MM.yyyy").format(douRequest.getRegDate()));
		
		getDisplay().getLastName().setText(getVal(xml, "last_name"));
		getDisplay().getFirstName().setText(getVal(xml, "first_name"));
		getDisplay().getPatronymic().setText(getVal(xml, "patronimic"));
		Date birthday = getValDate(xml, "birthday");
		if(birthday!=null){
			getDisplay().getBirthDate().setValue(birthday);
		}
		getDisplay().getPhoneNumber().setText(getVal(xml, "applicant_phones"));
		getDisplay().getEmail().setText(getVal(xml, "applicant_email"));
		
		// Заявитель
		getDisplay().getDeclarantFirstname().setText(getVal(xml, "declarantFirstname"));
		getDisplay().getDeclarantLastname().setText(getVal(xml, "declarantLastname"));
		getDisplay().getDeclarantPatronymic().setText(getVal(xml, "declarantPatronymic"));
		getDisplay().getDeclarantPassportSeria().setText(getVal(xml, "declarantPassportSeria"));
		getDisplay().getDeclarantPassportNumber().setText(getVal(xml, "declarantPassportNumber"));
		getDisplay().getDeclarantPassportIssueDate().setValue(getValDate(xml, "declarantPassportIssueDate"));
		getDisplay().getDeclarantPassportAuthority().setText(getVal(xml, "declarantPassportAuthority"));
		
		getDisplay().getBsSeriaBox().setText(getVal(xml, "bs_seria"));
		getDisplay().getBsNumberBox().setText(getVal(xml, "bs_number"));
		getDisplay().getBsDateBox().setValue(getValDate(xml, "bs_date"));
		
		String sex = getVal(xml, "sex");
		if("M".equals(sex)){
			getDisplay().setSex('m');
		} else if("W".equals(sex)){
			getDisplay().setSex('f');
		}
		

		
		String region = getVal(xml, "region");
		String comment = getVal(xml, "comments");
		if(region!= null && !region.isEmpty()){
			comment += ("\nРайон: "+region);
		}
		
		getDisplay().getComments().setText(comment);
		
		getDisplay().getYear().setValue(getValInt(xml, "planYear"));
		getDisplay().getChildAddress().setText(getVal(xml, "address"));
		
		if(dous!=null){
			Collection<List<DouDTO>> values = dous.values();
			for (List<DouDTO> list : values) {
				for (DouDTO dou : list) {
					String val = getVal(xml, "dou_"+dou.getNumber()+"_raiting");
					if(val!=null && !val.isEmpty()){
//						TODO тут надо что-то мутить с рейтингом !!!!
						getDisplay().chooseOneDou(dou, new Integer(val));
					}
				}
			}
		}
		getDisplay().disableUi(true);
	}

}
