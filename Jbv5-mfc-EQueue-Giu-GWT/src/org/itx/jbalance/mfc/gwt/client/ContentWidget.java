package org.itx.jbalance.mfc.gwt.client;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;

public abstract class ContentWidget extends Composite implements MyDisplay {

//	private EQueueShell shell;

	public ContentWidget() {
//		MyCellTableResources.INSTANCE.cellTableStyle().ensureInjected();
	}
	
	@Deprecated
	public ContentWidget(String name) {
	}

	public void showExceptionNotification(Throwable caught){
		Log.error("",caught);
		DialogBox dialogBox = createNotificationDialog(caught.getLocalizedMessage(), NotificationType.ERROR, caught);
		dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);

	    dialogBox.center();
	    dialogBox.show();
	}
	
	public void showWarningNotification(String message){
		Log.warn(message);
		DialogBox dialogBox = createNotificationDialog(message, NotificationType.WARN, null);
	    dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);

	    dialogBox.center();
	    dialogBox.show();
	}
	
	public void showInfoNotification(String message){
		Log.info(message);
		
		DialogBox dialogBox = createNotificationDialog(message, NotificationType.INFO, null);
	    dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);

	    dialogBox.center();
	    dialogBox.show();
	}
	

	
	
	enum NotificationType{
		ERROR, WARN, INFO
	}
	
	private DialogBox createNotificationDialog(String message, NotificationType notificationType, Throwable caught) {
	    // Create a dialog box and set the caption text
	    final DialogBox dialogBox = new DialogBox();
	    dialogBox.ensureDebugId("cwDialogBox");
	    
	    switch (notificationType) {
			case ERROR:
				dialogBox.setText("Возникли неожиданные ошибки!");
				break;
			case WARN:
			case INFO:
				dialogBox.setText("Внимание!!!");
				break;
		}
	    

	    // Create a table to layout the content
	    DockPanel dialogContents = new DockPanel();
	    dialogContents.setSpacing(4);
	    dialogContents.setWidth("600px");
	    dialogContents.setHeight("200px");
	    dialogBox.setWidget(dialogContents);

	    Image img = null;
	    switch (notificationType) {
			case ERROR:
				img = new Image("images/error.png");
				break;
			case WARN:
				img = new Image("images/warn.png");
				break;
			case INFO:
				img = new Image("images/info.png");
				break;
	    }
	    
	    
	   
	    // Add some text to the top of the dialog
	    if(caught!=null && notificationType == NotificationType.ERROR){
		    for (StackTraceElement ste : caught.getStackTrace()){
		    	message += "\n" + ste.toString();
		    }
	    }
	    
	    HTML details = new HTML(message);
	    details.setWidth("490px");
	    dialogContents.add(details, DockPanel.CENTER);
	 //   dialogContents.setCellHorizontalAlignment(       details, HasHorizontalAlignment.ALIGN_CENTER);
	    dialogContents.add(img, DockPanel.WEST);
//	    HorizontalPanel buttonsPanel = new HorizontalPanel();
	    
	    
	    Button okButton = new Button("Ok");
	    okButton.setWidth("100px");
	    ClickHandler closeHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		};
		okButton.addClickHandler(closeHandler);
//		buttonsPanel.add(okButton);
//		dialogContents.add(buttonsPanel, DockPanel.SOUTH);
		dialogContents.add(okButton, DockPanel.SOUTH);
	    return dialogBox;
	  }
	    
	public abstract void initialize();

//	public EQueueShell getShell() {
//		return shell;
//	}
//
//	public void setShell(EQueueShell shell) {
//		this.shell = shell;
//	}

	public void showWaitCursor() {
	    DOM.setStyleAttribute(RootPanel.getBodyElement(), "cursor", "wait");
	}
	 
	public void showDefaultCursor() {
	    DOM.setStyleAttribute(RootPanel.getBodyElement(), "cursor", "default");
	}
	
}
