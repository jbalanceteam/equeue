//package org.itx.jbalance.mfc.gwt.client.request;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//
//import org.itx.jbalance.l2_api.dto.DouDTO;
//import org.itx.jbalance.l2_api.dto.RegionDTO;
//
//import com.allen_sauer.gwt.log.client.Log;
//import com.google.gwt.dom.client.Style.Unit;
//import com.google.gwt.event.dom.client.ChangeEvent;
//import com.google.gwt.event.dom.client.ChangeHandler;
//import com.google.gwt.event.dom.client.ClickEvent;
//import com.google.gwt.event.dom.client.ClickHandler;
//import com.google.gwt.event.logical.shared.ValueChangeEvent;
//import com.google.gwt.event.logical.shared.ValueChangeHandler;
//import com.google.gwt.user.client.ui.Button;
//import com.google.gwt.user.client.ui.CheckBox;
//import com.google.gwt.user.client.ui.DockLayoutPanel;
//import com.google.gwt.user.client.ui.FlowPanel;
//import com.google.gwt.user.client.ui.Grid;
//import com.google.gwt.user.client.ui.ListBox;
//import com.google.gwt.user.client.ui.SimplePanel;
//
//public class ChooseDouPanel extends SimplePanel{
//
//	private List<RegionDTO> regions; 
//	private List<CB_DOU_REG> cbDouList; 
//	private Button selectAll = new Button("Выбрать все");
//	private Button cleanSelection = new Button("Снять выделение");
//	
////	public List<CB_DOU_REG> getCbDouList() {
////		return cbDouList;
////	}
////
////	public void setCbDouList(List<CB_DOU_REG> cbDouList) {
////		this.cbDouList = cbDouList;
////	}
// 
//	public void setEnable(boolean enabled){
//	for(CB_DOU_REG dou_REG : cbDouList){
//		dou_REG.checkBox.setEnabled(enabled);
//		regionLB.setEnabled(enabled);
//		selectAll.setEnabled(enabled);
//		cleanSelection.setEnabled(enabled);
//	}
//	}
//
//	private ListBox regionLB = new ListBox();
//	Grid grid=new  Grid();
//	
//	public ChooseDouPanel(final Map<RegionDTO,List<DouDTO>>dous, final SelectDouListener selectListener) {
//		super();
//		setWidth("200px");
//		setHeight("350px");
//		cbDouList = new ArrayList<ChooseDouPanel.CB_DOU_REG>();
//		regions = new ArrayList<RegionDTO>(dous.keySet().size());
//		for(Entry<RegionDTO, List<DouDTO>>e: dous.entrySet()){
//			regions.add(e.getKey());
//			for (DouDTO dou : e.getValue()) {
//				
//				CheckBox cb = new CheckBox();
//				cb.setTitle(dou.getName());
//				cb.getElement().getStyle().setMarginLeft(15, Unit.PX);
//				cb.getElement().getStyle().setFontSize(11, Unit.PX);
//				cb.setText(dou.getNumber()+"");
//				cb.setValue(Boolean.FALSE);
//				
//				cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
//					@Override
//					public void onValueChange(ValueChangeEvent<Boolean> event) {
//						if(selectListener!=null)
//							selectListener.selectionChanged(getSelectedDou());
//					}
//				});
//				
//				cbDouList.add(new CB_DOU_REG(dou, e.getKey(), cb));
//			}
//			
//		}
//		
//		
//		initComponents();
//		initListeners();
//		updateGrid(null);
//	}
//
//	
//	public RegionDTO getSelectedRegion(){
//		String value = regionLB.getValue(regionLB.getSelectedIndex());
//		RegionDTO selectedRegion = null;
//		if(value .equals("all")){
//			selectedRegion = null;
//		}else{
//			
//			for(RegionDTO r: regions){
//				if(value.equals(r.getUId().toString())){
//					selectedRegion = r;
//				}
//			}
//			if(selectedRegion == null){
//				throw new RuntimeException("How did this happen?");
//			}
//		}
//		return selectedRegion;
//	}
//
//	public void selectAllByRegion(RegionDTO region, Boolean value){
//		if(region == null){
//			for (CB_DOU_REG item : cbDouList) {
//				item.checkBox.setValue(value);
//			}
//		}else{
//			for (CB_DOU_REG item : getItemsByRegion(region)) {
//				item.checkBox.setValue(value);
//			}
//		}
//	}
//	
//	private void initListeners() {
//		regionLB.addChangeHandler(new ChangeHandler() {
//			@Override
//			public void onChange(ChangeEvent event) {
//					updateGrid(getSelectedRegion());
//			}
//		});
//	}
//	
//	
//	void initComponents(){
//		Collections.sort(regions, new Comparator<RegionDTO>() {
//
//			@Override
//			public int compare(RegionDTO o1, RegionDTO o2) {
//				if(o1==null || o1.getName()==null)
//					return -1;
//				if(o2==null || o2.getName()==null)
//					return 1;
//				return o1.getName().compareTo(o2.getName());
//			}
//		});
//		
//		regionLB.addItem("Все", "all");
//		
//		for (RegionDTO region : regions) {
//			regionLB.addItem(region.getName(), region.getUId().toString());
//		}
//		
//		
//		grid.setVisible(true);
//
//		
//		DockLayoutPanel dockLayoutPanel = new DockLayoutPanel(Unit.EM);
//		dockLayoutPanel.addNorth(regionLB, 2);
//		
//		
//		FlowPanel buttonsPanel = new FlowPanel();
//		dockLayoutPanel.addSouth(buttonsPanel, 2);
////		Button selectAll = new Button("Выбрать все");
//		selectAll.addClickHandler(new ClickHandler() {
//			@Override
//			public void onClick(ClickEvent event) {
//				selectAllByRegion(getSelectedRegion(), true);
//			}
//		});
//		buttonsPanel.add(selectAll);
//	//	Button cleanSelection = new Button("Снять выделение");
//		cleanSelection.addClickHandler(new ClickHandler() {
//			@Override
//			public void onClick(ClickEvent event) {
//				selectAllByRegion(getSelectedRegion(), false);
//			}
//		});
//		buttonsPanel.add(cleanSelection);
//		
//		
//		dockLayoutPanel.add(grid);
//		dockLayoutPanel.setSize("250px", "300px");
//		
//		this.add(dockLayoutPanel);
//	}
//	
//	public List<DouDTO> getAllDous(){
//		 List<DouDTO> res = new ArrayList<DouDTO>();
//		 for (CB_DOU_REG  item : cbDouList ) {
//			 res.add(item.dou);
//		 }
//		 return res;
//	}
//	
//	
//	private void updateGrid(RegionDTO reg){
//		Log.info(reg+"");
//		
//		grid.clear();
//		
//		List<CB_DOU_REG> itemsByRegion ; 
//		if(reg == null){
//			 itemsByRegion = cbDouList;
//		}else{
//			 itemsByRegion = getItemsByRegion(reg);
//		}
//		Log.info("itemsByRegion: "+itemsByRegion);
//		
//		grid.resize(itemsByRegion.size() / 5 +1, 6);
//		int i=0;
//		for (CB_DOU_REG item : itemsByRegion) {
////			Log.debug(""+item + " "+i/5 + " "+i%5);
//			grid.setWidget(i/5, i%5, item.checkBox);
//			i++;
//		}
//	}
//	
//	private List<CB_DOU_REG>getItemsByRegion(RegionDTO reg){
//		List<CB_DOU_REG> res=new ArrayList<CB_DOU_REG>();
//		for(CB_DOU_REG item: cbDouList){
//			if(item.region.equals(reg)){
//				res.add(item);
//			}
//		}
//		return res;
//	}
//	
//	
//	public boolean isAllSelected(){
//		for(CB_DOU_REG item: cbDouList){
//			if(!item.checkBox.getValue()){
//				return false;
//			}
//		}
//		return true;
//	}
//	
//	public boolean isAllUnselected(){
//		for(CB_DOU_REG item: cbDouList){
//			if(item.checkBox.getValue()){
//				return false;
//			}
//		}
//		return true;
//	}
//	
//	public List<DouDTO> getSelectedDou(){
//		List<DouDTO> res=new ArrayList<DouDTO>();
//		for(CB_DOU_REG item: cbDouList){
//			if(item.checkBox.getValue()){
//				res.add(item.dou);
//			}
//		}
//		return res;
//	}
//	
//	
//	public interface SelectDouListener{
//		void selectionChanged(List<DouDTO>selected);
//	}
//	
//	
//	public void clearSelection(){
//		for(CB_DOU_REG item: cbDouList){
//			item.checkBox.setValue(Boolean.FALSE);
//		}
//	}
//	
//	public void selectOne(DouDTO dou, Boolean value){
//		for(CB_DOU_REG item: cbDouList){
//			if(item.dou.equals(dou)){
//				item.checkBox.setValue(value);
//				break;
//			}
//		}
//	}
//	
//	
//	class CB_DOU_REG{
//		CB_DOU_REG(DouDTO dou, RegionDTO region, CheckBox checkBox){
//			this.checkBox = checkBox;
//			this.dou = dou;
//			this.region = region;
//		}
//		
//		CheckBox checkBox;
//		DouDTO dou;
//		RegionDTO region;
//		@Override
//		public String toString() {
//			return "CB_DOU_REG [checkBox=" + checkBox + ", dou=" + dou
//					+ ", region=" + region + "]";
//		}
//		
//		
//	}
//	
//
//}
