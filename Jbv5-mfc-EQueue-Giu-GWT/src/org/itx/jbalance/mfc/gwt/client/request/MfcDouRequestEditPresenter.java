package org.itx.jbalance.mfc.gwt.client.request;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.PrivilegeDTO;
import org.itx.jbalance.l2_api.dto.RegionDTO;
import org.itx.jbalance.mfc.gwt.client.Callback;
import org.itx.jbalance.mfc.gwt.client.MyDisplay;


import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;



public interface MfcDouRequestEditPresenter {

	public interface Display extends MyDisplay{
		// Сведения о ребенке
		HasText getLastName();
		HasText getFirstName();
		HasText getPatronymic();
		HasValue<Date> getBirthDate();
		//пол
		HasText getBsSeriaBox();
		HasText getBsNumberBox();
		HasValue<Date> getBsDateBox();
		
		// Детские сады
		//List<PrivilegeDTO> getAllPrivileges();
//		void setChoosedDou  (List <Dou> availableDou);
		void setAvailableDou(Map<RegionDTO, List <DouDTO>> availableDou);		
		// Адрес проживания ребенка
		HasText getChildAddress();

		//Дополнительно	
		PrivilegeDTO getPrivilege();
		HasText getComments();
		HasText getPhoneNumber();
		HasText getEmail();
	}
	

	void initialize(Callback c);
	void checkDousRequest(); //Callback c
	void makeDousRequest();
	List<PrivilegeDTO> getAllPrivileges();
	State getState();
	void setState(State state);
	void clearForm();
	void runEvidenceReport();
	String runChildCheckInfoBeforeDouRequestUrl();
	void populateFormUsingXml(String results);
	
	
}
