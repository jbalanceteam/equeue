package org.itx.jbalance.mfc.gwt.client.mfc.services;

import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.DouRequestPrerequisiteDTO;
import org.itx.jbalance.l2_api.dto.DouRequestResultDTO;
import org.itx.jbalance.l2_api.dto.ResultDTO;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface DouRequestsServiceAsync {

	
	void getDouRequestPrerequisite(
			AsyncCallback<DouRequestPrerequisiteDTO> callback);

	void checkDousRequest(DouRequestDTO dousRequestDTO,
			AsyncCallback<ResultDTO> callback);

	void makeDousRequest(DouRequestDTO dousRequestDTO,
			AsyncCallback<DouRequestResultDTO> callback);

}
