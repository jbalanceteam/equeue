package org.itx.jbalance.mfc.gwt.client;


public abstract class MyPresenter<D extends MyDisplay> {

	protected D display;

	public MyPresenter(final D display) {
		this.display = display;
	}
	
	public D getDisplay(){
		return display;
	};
	
	
	public abstract void initialize();
	
}
