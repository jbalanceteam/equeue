package org.itx.jbalance.mfc.gwt.client;

//import com.allen_sauer.gwt.log.client.Log;
//import com.google.gwt.core.client.GWT;
//import com.google.gwt.user.client.History;
//import com.google.gwt.user.client.rpc.AsyncCallback;
//import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootLayoutPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class EQueueMfc implements EntryPoint {

	

	/**
	 * This is the entry point method.
	 */
	@Override
	public void onModuleLoad() {
				initApplication();
				EQueueNavigation.instance().goToDouRequestEditPage(null);
	}

	private void initApplication() {
		final long startTime=System.currentTimeMillis();
		final Panel root = RootLayoutPanel.get();
		root.add(EQueueShell.instance());

	}

}
