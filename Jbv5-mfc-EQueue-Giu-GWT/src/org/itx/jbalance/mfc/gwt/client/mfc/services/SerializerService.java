package org.itx.jbalance.mfc.gwt.client.mfc.services;



import java.io.Serializable;
import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("serializer")
public interface SerializerService extends RemoteService {
	List<String> serializeToBase64(List<Serializable> obj);
}
