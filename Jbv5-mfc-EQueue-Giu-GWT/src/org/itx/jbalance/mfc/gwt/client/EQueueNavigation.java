package org.itx.jbalance.mfc.gwt.client;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.itx.jbalance.mfc.gwt.client.help.About;
import org.itx.jbalance.mfc.gwt.client.printNotification.PrintNotificationWidget;
import org.itx.jbalance.mfc.gwt.client.request.MfcDouRequestEditPresenter;
import org.itx.jbalance.mfc.gwt.client.request.MfcDouRequestEditPresenterImpl;
import org.itx.jbalance.mfc.gwt.client.request.MfcDouRequestEditWidget;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;

/**
 * This class contains set of methods for navigation.
 * 
 * @author apv
 * 
 */
public class EQueueNavigation {

	static final String URL_DOU_REQUEST_EDIT = "QueueEdit";
	
	static final String URL_PRINTNOTIFICATION = "PrintNotification";

	static final String URL_LOGOUT = "Logout";

	static final String URL_ABOUT = "About";
	static final String URL_SITE = "Site";
	static final String URL_WIKI = "Wiki";
	static final String URL_FORUM = "Forum";
	static final String URL_FAQ = "Faq";

	static final String EXTERNAL_URL_SITE = "http://jbalance.org/portal/equeue/";
	static final String EXTERNAL_URL_WIKI = "http://jbalance.org/portal/equeue/wiki?initialURI=/portal/equeue/wiki";
	static final String EXTERNAL_URL_FORUM = "http://jbalance.org/portal/equeue/forum/";
	static final String EXTERNAL_URL_FAQ = "http://jbalance.org/portal/equeue/faq/";

	Map<String, Command> menuItems = new HashMap<String, Command>();

	/**
	 * singletone
	 */
	private static EQueueNavigation instance = new EQueueNavigation();

//	private final DouRequestsServiceAsync douRequestService = GWT
//	.create(DouRequestsService.class);
	
	/**
	 * Get instance
	 * 
	 * @return
	 */
	public static synchronized EQueueNavigation instance() {
		return instance;
	}

	/**
	 * Класс для хранения информации о пункте меню: Надпись, Действие,
	 * выполняемое по нажатию URL
	 * 
	 * 
	 * @author apv
	 * 
	 */
	public class EQueueGoToAction {

		public EQueueGoToAction(Command action, String url) {
			super();
			// this.label = label;
			this.action = action;
			this.url = url;
		}

		// public String label;
		final public Command action;
		final public String url;
	}

	/**
	 * Closed constructor
	 */
	private EQueueNavigation() {
		menuItems.put(URL_DOU_REQUEST_EDIT, new Command() {
			@Override
			public void execute() {
				goToDouRequestEditPage(null);
			}
		});

		
		menuItems.put(URL_PRINTNOTIFICATION, new Command() {
			@Override
			public void execute() {
				goToPrintNotificationPage();
			}
		});
		
		menuItems.put(URL_LOGOUT, new Command() {

			@Override
			public void execute() {
				logout();
			}
		});

		menuItems.put(URL_ABOUT, new Command() {
			@Override
			public void execute() {
				goToAbout(null);
			}
		});
		menuItems.put(URL_FAQ, new GoToURLCommang(EXTERNAL_URL_FAQ));
		menuItems.put(URL_FORUM, new GoToURLCommang(EXTERNAL_URL_FORUM));
		menuItems.put(URL_SITE, new GoToURLCommang(EXTERNAL_URL_SITE));
		menuItems.put(URL_WIKI, new GoToURLCommang(EXTERNAL_URL_WIKI));

	}

	Command getMenuItemByUrl(String url) {
		return menuItems.get(url);
	}

	private void logout() {
		if (Window.confirm("Выйти из системы?"))
		{
//			Window.alert("Вы вышли из системы.");
//			douRequestService.getThreadLocalRequest().getSession().invalidate();
//			
//			String url = Window.Location.getProtocol() + "//"
//					+ Window.Location.getHost()
//					+ Window.Location.getPath()
//					+ Window.Location.getQueryString();
//			RequestBuilder builder = new RequestBuilder(
//					RequestBuilder.GET, url);
//			builder.setUser("invalid");
//			builder.setPassword("invalid");
//			builder.setCallback(new RequestCallback() {
//
//				@Override
//				public void onResponseReceived(Request request,
//						Response response) {
//					Window.Location.reload();
//				}
//
//				@Override
//				public void onError(Request request, Throwable exception) {
//					Window.Location.reload();
//				}
//			});
//			try {
//				builder.send();
//			} catch (RequestException e) {
//				Log.error("", e);
//			}
		}
			
	}
	
	
	public void goToPrintNotificationPage() {
		final PrintNotificationWidget display = new PrintNotificationWidget();
		EQueueShell.instance().setContent(display);
		History.newItem(URL_PRINTNOTIFICATION, false);
	}

	public void goToDouRequestEditPage(Map<String, Serializable> p) {
		if (p == null)
			p = Collections.emptyMap();
		final Map<String, Serializable> params = p;
		final MfcDouRequestEditWidget display = new MfcDouRequestEditWidget();
		final MfcDouRequestEditPresenter presenter = new MfcDouRequestEditPresenterImpl(
				display);
		display.setPresenter(presenter);
		EQueueShell.instance().setContent(display);
		presenter.initialize(new Callback() {
			@Override
			public void callback() {
				display.initCreateMode();
				// if(params.containsKey("object")){
				// presenter.setObject((HDouRequest) params.get("object"));
				// display.initEditMode();
				// }else{
				// display.initCreateMode();
				// }
				History.newItem(URL_DOU_REQUEST_EDIT, false);
			}
		});

	}

	public void goToAbout(Map<String, Serializable> params) {
		EQueueShell.instance().setContent(new About());
		History.newItem(URL_ABOUT, false);
	}

	class GoToURLCommang implements Command {
		private String url;

		public GoToURLCommang(String url) {
			if (url == null || url.isEmpty())
				throw new RuntimeException();
			this.url = url;
		}

		@Override
		public void execute() {
			Window.open(url, "blank_", null);

		}

	}

}
