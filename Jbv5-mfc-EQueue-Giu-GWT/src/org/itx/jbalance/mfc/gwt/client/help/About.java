package org.itx.jbalance.mfc.gwt.client.help;

import org.itx.jbalance.mfc.gwt.client.ContentWidget;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Widget;


public class About extends ContentWidget {
 
	interface Binder extends UiBinder<Widget, About> {

	}
	
  public About() {

  }

  /**
   * Initialize this example.
   */

  @Override
  public void initialize() {
	// Create the UiBinder.
		Binder uiBinder = GWT.create(Binder.class);
		if(this.getWidget() == null){
			initWidget(uiBinder.createAndBindUi(this));
			}
   

  }
}
