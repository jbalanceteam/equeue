package org.itx.jbalance.mfc.gwt.client.printNotification;

import java.util.Date;

import org.itx.jbalance.mfc.gwt.client.ContentWidget;
import org.itx.jbalance.mfc.gwt.client.DateChooserComponent;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class PrintNotificationWidget extends ContentWidget {

	interface Binder extends UiBinder<Widget, PrintNotificationWidget> {

	}

	@UiField(provided = false)
	DateChooserComponent birthDateBox;
	
	@UiField(provided = false)
	TextBox rn;

	@UiField
	Button printEvidenceButton;

	public PrintNotificationWidget() {
		super();
	}



	@UiHandler("printEvidenceButton")
	public void printEvidenceButton(ClickEvent event) {
		String urlStr = "equeue/report?type=registrationNotification2";
		urlStr+="&rn="+rn.getText();
		Date birthday = birthDateBox.getValue();
		String isoDate =  (birthday.getYear()+1900)+"-"+(+birthday.getMonth()+1)+"-"+birthday.getDate();
		urlStr+="&birthday="+isoDate;
		Window.open(urlStr, "blank_", null);
	}



	@Override
	public void initialize() {
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
	} 

}
