package org.itx.jbalance.mfc.gwt.server;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.mfc.gwt.client.mfc.services.SerializerService;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


@SuppressWarnings("serial")
public class SerializerServiceImpl  extends RemoteServiceServlet //extends MyGWTServlet 
	implements Serializable ,SerializerService{

	@Override
	public List<String> serializeToBase64(List<Serializable> obj) {
		List<String>  res = new  ArrayList<String>(obj.size());
		for (Serializable s : obj) {
			String base64 = SerializerHelper.serializeToBase64(s);
			res.add(base64);
		}
		return res;
	}
}
