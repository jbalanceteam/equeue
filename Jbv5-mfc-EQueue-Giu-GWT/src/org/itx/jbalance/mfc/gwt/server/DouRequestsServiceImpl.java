package org.itx.jbalance.mfc.gwt.server;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.DouRequestPrerequisiteDTO;
import org.itx.jbalance.l2_api.dto.DouRequestResultDTO;
import org.itx.jbalance.l2_api.dto.ResultDTO;
import org.itx.jbalance.l2_api.ws.MfcWS;
import org.itx.jbalance.mfc.gwt.client.mfc.services.DouRequestsService;
import org.jboss.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class DouRequestsServiceImpl extends RemoteServiceServlet 
implements DouRequestsService {

	
	public MfcWS getDouRequestsSession(){
		try{
			Context context = new InitialContext();
			return (MfcWS)context.lookup(MfcWS.JNDI_NAME);
		}catch (Throwable e) {
			Logger.getLogger(getClass()).error("",e);
			return null;
		}
	}
	
	@Override
	public DouRequestPrerequisiteDTO getDouRequestPrerequisite() {
		return getDouRequestsSession().getDouRequestPrerequisite();
	}

	@Override
	public ResultDTO checkDousRequest(DouRequestDTO dousRequestDTO) {
		return getDouRequestsSession().checkDousRequest(dousRequestDTO);
	}

	@Override
	public DouRequestResultDTO makeDousRequest(DouRequestDTO dousRequestDTO) {
		return getDouRequestsSession().makeDousRequest(dousRequestDTO);
	}

}
