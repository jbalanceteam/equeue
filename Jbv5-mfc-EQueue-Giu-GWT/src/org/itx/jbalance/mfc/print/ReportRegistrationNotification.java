package org.itx.jbalance.mfc.print;

import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.DouRequestResultDTO;
import org.jboss.logging.Logger;
import org.odftoolkit.odfdom.OdfFileDom;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.doc.office.OdfOfficeDocument;
import org.odftoolkit.odfdom.doc.office.OdfOfficeSpreadsheet;
import org.w3c.dom.DOMException;

public final class ReportRegistrationNotification extends AbstractReport  {
	public static  Logger logger = Logger.getLogger(ReportRegistrationNotification.class.getName());
	
	String outputFileName;
	
	OdfFileDom contentDom;
	OdfOfficeSpreadsheet officeSpreadsheet;
	OdfOfficeDocument officeDocument;

	private OdfTextDocument getOutputDocument() {
		try {
			InputStream template = getTemplate("mfc_reportRegistrationNotification.odt");
			return (OdfTextDocument) OdfDocument.loadDocument(template);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	

	/**
	 * @param rec_num
	 * @param odfTableRow
	 * @param douRequest
	 */
	public File printRegistrationNotification(DouRequestDTO douRequestDTO, DouRequestResultDTO douRequestResultDTO) {
		try{
			OdfTextDocument outputDocument = getOutputDocument();
			processRegistrationNotification(douRequestDTO, douRequestResultDTO, outputDocument);
			File outputFile = File.createTempFile("reportRegistrationNotificationTemp", ".odt");
	
			outputDocument.save(outputFile);
			outputDocument.close();
			return outputFile;
		} catch(Exception e){
			throw  new RuntimeException(e);
		}
	}
	
	
	public File printRegistrationNotification(org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO req) {
		try{
			OdfTextDocument outputDocument = getOutputDocument();
			processRegistrationNotification(req, outputDocument);
			File outputFile = File.createTempFile("reportRegistrationNotificationTemp", ".odt");
	
			outputDocument.save(outputFile);
			outputDocument.close();
			return outputFile;
		} catch(Exception e){
			throw  new RuntimeException(e);
		}
	}
	
	

	private void processRegistrationNotification(
			org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO req,
			OdfTextDocument outputDocument) {
		try {
			DateFormat fmt = new SimpleDateFormat("dd.MM.yyyy");
			
			String fullName = req.getSurname() + " "
				+ req.getRealName() + " "
				+ req.getPatronymic();
			
			replace(outputDocument.getContentDom(),"p1", fullName);
			replace(outputDocument.getContentDom(),"p2", req.getBirthday()!= null? fmt.format(req.getBirthday()) : "");
			replace(outputDocument.getContentDom(),"p3", fmt.format(req.getRegDate()));
			replace(outputDocument.getContentDom(),"p4", req.getRegNumber() != null ? req.getRegNumber(): "");
			replace(outputDocument.getContentDom(),"p_zayavitel", req.getDeclarant() != null? req.getDeclarant() : "______________________");
			replace(outputDocument.getContentDom(),"p5", req.getDous() != null ? req.getDous() : "");
//			replace(doc.getContentDom(),"p_email", douRequestDTO.getDeclarant());
		} catch (DOMException e) {
			throw new RuntimeException(e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	private void processRegistrationNotification(DouRequestDTO douRequestDTO, DouRequestResultDTO douRequestResultDTO, OdfTextDocument outputDocument) {
		try {
			DateFormat fmt = new SimpleDateFormat("dd.MM.yyyy");
			
			String fullName = douRequestDTO.getLastName() + " "
			+ douRequestDTO.getFirstName() + " "
			+ douRequestDTO.getPatronymic();
			
			
			replace(outputDocument.getContentDom(),"p1", fullName);
			replace(outputDocument.getContentDom(),"p2", fmt.format(douRequestDTO.getBirthDate()));
			replace(outputDocument.getContentDom(),"p3", fmt.format(new Date()));
			replace(outputDocument.getContentDom(),"p4", douRequestResultDTO.getDnumber());
			
			replace(outputDocument.getContentDom(),"p_zayavitel", getDeclarantStr(douRequestDTO));
			replace(outputDocument.getContentDom(),"p5", getDouStr(sortRaiting(douRequestDTO.getDouUids())));
//			replace(doc.getContentDom(),"p_email", douRequestDTO.getDeclarant());
		} catch (DOMException e) {
			throw new RuntimeException(e);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
