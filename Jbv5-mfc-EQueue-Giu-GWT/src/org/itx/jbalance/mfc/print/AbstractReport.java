package org.itx.jbalance.mfc.print;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;

import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.DouRequestPrerequisiteDTO;
import org.itx.jbalance.l2_api.ws.MfcWS;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class AbstractReport {

	
	private static final String PROP_NAME_TEMPLATES_DIR="templates.dir";
	
	
	protected List<Long> sortRaiting(final Map<Long, Integer> rating){
		List<Long> res = new ArrayList<Long>(rating.size());
		res.addAll(rating.keySet());
		Collections.sort(res, new Comparator<Long>() {

			@Override
			public int compare(Long o1, Long o2) {
				return rating.get(o1).compareTo(rating.get(o2)) * -1;
			}
		});
		return res;
	}
	
	
	private static DouRequestPrerequisiteDTO douRequestPrerequisiteDTO;
	private static DouRequestPrerequisiteDTO getDouRequestPrerequisiteDTO (){
		if(douRequestPrerequisiteDTO==null){
			douRequestPrerequisiteDTO = getDouRequestsSession().getDouRequestPrerequisite();
		}
		return douRequestPrerequisiteDTO;
	}
	
	private static MfcWS getDouRequestsSession(){
		try{
			Context context = new InitialContext();
			return (MfcWS)context.lookup(MfcWS.JNDI_NAME);
		}catch (Throwable e) {
			throw new RuntimeException(e);
		}
	}
	
	protected String getDouStr(List<Long> uids){
		StringBuilder res = new StringBuilder();
		String sres="";
		List<DouDTO> dousDTOs = getDouRequestPrerequisiteDTO ().getDousDTOs();
		for (DouDTO douDTO : dousDTOs) {
			for (Long douUid : uids) {
				if(douDTO.getUId() . equals(douUid)){
					res.append(douDTO.getNumber());
					res.append( ", "); 
					break;
				}
			}
		}
		if (!res.equals("".toString())) 
			sres=res.toString().substring(0, res.length()-2);
		
		return sres;
	}
	
	
	protected String getDeclarantStr(DouRequestDTO douRequestDTO) {
		StringBuffer declarant = new StringBuffer();
		if(douRequestDTO.getDeclarantLastname() != null && !douRequestDTO.getDeclarantLastname().isEmpty()){
			declarant.append(douRequestDTO.getDeclarantLastname());
			declarant.append(" ");
		}
		
		if(douRequestDTO.getDeclarantFirstname() != null && !douRequestDTO.getDeclarantFirstname().isEmpty()){
			declarant.append(douRequestDTO.getDeclarantFirstname());
			declarant.append(" ");
		}
		
		if(douRequestDTO.getDeclarantPatronymic() != null && !douRequestDTO.getDeclarantPatronymic().isEmpty()){
			declarant.append(douRequestDTO.getDeclarantPatronymic());
			declarant.append(" ");
		}
		
		declarant.append(" (Паспорт ");
		if(douRequestDTO.getDeclarantPassportSeria() != null && !douRequestDTO.getDeclarantPassportSeria().isEmpty()){
			declarant.append(douRequestDTO.getDeclarantPassportSeria());
			declarant.append(" ");
		}
		
		if(douRequestDTO.getDeclarantPassportNumber() != null && !douRequestDTO.getDeclarantPassportNumber().isEmpty()){
			declarant.append(douRequestDTO.getDeclarantPassportNumber());
			declarant.append(" ");
		}
		
		if(douRequestDTO.getDeclarantPassportIssue() != null){
			DateFormat fmt = new SimpleDateFormat("dd.MM.yyyy");
			declarant.append(fmt.format(douRequestDTO.getDeclarantPassportIssue()));
			declarant.append(" ");
		}
		
		if(douRequestDTO.getDeclarantPassportAuthority() != null && !douRequestDTO.getDeclarantPassportAuthority().isEmpty()){
			declarant.append(douRequestDTO.getDeclarantPassportAuthority());
		}
		
		declarant.append(")");
		
		final String string = declarant.toString();
		return string;
	}
	
	
	InputStream getTemplate(String filename) throws FileNotFoundException {
			InputStream resourceAsStream = null;
			
			String templateDirectory = getTemplateDirectory();
			
			if(templateDirectory!=null) {
				File file = new File(templateDirectory+"/"+filename);
				if(file.exists()){
					resourceAsStream = new FileInputStream(file);
				}
			} 
			if(resourceAsStream==null)
			resourceAsStream = getClass().getClassLoader()
				.getResourceAsStream("./reportTemplates/"+filename);
			return resourceAsStream;
	}
	
	/**
	 * Метод используется в печатных формах на основе шаблонов.
	 * Заменяет <code>param</code> в шаблоне на <code>val</code>
	 * @param doc
	 * @param param
	 * @param val
	 */
	protected void replace(Node doc,String param,String val){
		NodeList childNodes = doc.getChildNodes();
		//param="#{"+param+"}";
		for (int i=0;i<childNodes.getLength();i++) {
			Node item = childNodes.item(i);
			replace(item, param, val);
		}
		if(childNodes.getLength()>0)
			return;
		
		if(doc.getTextContent().contains(param))
		doc.setTextContent(doc.getTextContent().replaceAll(param, val));
	}
	
	private String getTemplateDirectory(){
		return System.getProperty(PROP_NAME_TEMPLATES_DIR);
	}
}
