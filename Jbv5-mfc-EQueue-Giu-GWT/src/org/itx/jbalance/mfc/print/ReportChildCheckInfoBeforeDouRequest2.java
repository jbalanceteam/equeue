package org.itx.jbalance.mfc.print;

import java.io.File;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;


import javax.naming.Context;
import javax.naming.InitialContext;

import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.DouRequestPrerequisiteDTO;
import org.itx.jbalance.l2_api.ws.MfcWS;
import org.jboss.logging.Logger;
import org.odftoolkit.odfdom.OdfFileDom;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.doc.office.OdfOfficeDocument;
import org.odftoolkit.odfdom.doc.office.OdfOfficeSpreadsheet;
import org.w3c.dom.DOMException;

public final class ReportChildCheckInfoBeforeDouRequest2 extends AbstractReport  {

	
	public static  Logger logger = Logger.getLogger(ReportChildCheckInfoBeforeDouRequest2.class.getName());
	private static MfcWS getDouRequestsSession(){
		try{
			Context context = new InitialContext();
			return (MfcWS)context.lookup(MfcWS.JNDI_NAME);
		}catch (Throwable e) {
			logger.error("",e);
			return null;
		}
	}
	
	public ReportChildCheckInfoBeforeDouRequest2() {
	}

	
	String outputFileName;
	OdfDocument outputDocument;
	OdfFileDom contentDom;
	OdfOfficeSpreadsheet officeSpreadsheet;
	OdfOfficeDocument officeDocument;

	void setupOutputDocument() {
		try {
			InputStream template = getTemplate("childCheckInfoBeforeDouRequest.odt");
			outputDocument = OdfDocument.loadDocument(template);
		} catch (Exception e) {
			System.err.println("Unable to create output file.");
			System.err.println(e.getMessage());
			outputDocument = null;
		}
	}

	private File saveOutputDocument() {
		File outputFile = null;
		try {
			outputFile = File.createTempFile("childCheckInfoBeforeDouRequestTemp", ".odt");
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			outputDocument.save(outputFile);
			outputDocument.close();
		} catch (Exception e) {
			System.err.println("Unable to save document.");
			System.err.println(e.getMessage());
		}
		return outputFile;
	}

	/**
	 * @param rec_num
	 * @param odfTableRow
	 * @param douRequest
	 */
	public File printChildCheckInfoBeforeDouRequest(DouRequestDTO douRequestDTO) {
		setupOutputDocument();
		if (outputDocument != null) {
			processChildCheckInfoBeforeDouRequest(douRequestDTO);
			return saveOutputDocument();
		}
		return null;
	}

	private void processChildCheckInfoBeforeDouRequest(DouRequestDTO douRequestDTO) {
		try {
			
			DateFormat fmt = new SimpleDateFormat("dd.MM.yyyy");
			OdfTextDocument doc=(OdfTextDocument) outputDocument;
			
			String fullName = douRequestDTO.getLastName() + " "
					+ douRequestDTO.getFirstName() + " "
					+ douRequestDTO.getPatronymic();
			String sexStr = "";
			if (douRequestDTO.getSex() == 'm')
				sexStr = "муж.";
			else if (douRequestDTO.getSex() == 'f')
				sexStr = "жен.";
		
			replace(doc.getContentDom(),"q1", fullName);
			replace(doc.getContentDom(),"q2", sexStr);
			replace(doc.getContentDom(),"q3", fmt.format(douRequestDTO.getBirthDate()));
			replace(doc.getContentDom(),"q4", douRequestDTO.getBsSeria());
			replace(doc.getContentDom(),"q5", douRequestDTO.getBsNumber());
			replace(doc.getContentDom(),"q6", fmt.format(douRequestDTO.getBsDate()));
			replace(doc.getContentDom(),"q7", douRequestDTO.getYearEnter() + "");
			replace(doc.getContentDom(),"q8", getDouStr(sortRaiting(douRequestDTO.getDouUids())));
			replace(doc.getContentDom(),"q9", douRequestDTO.getChildAddress());
			replace(doc.getContentDom(),"q0", douRequestDTO.getPhoneNumber());
			replace(doc.getContentDom(),"p_zayavitel",  getDeclarantStr(douRequestDTO)); 
			replace(doc.getContentDom(),"p_email", douRequestDTO.getEmail());
			

		} catch (DOMException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return;
	}
	

	
	//OdfParagraph paragraph = new OdfParagraph(contentDom);
	//OdfFileDom contentDom = outputDocument.getContentDom();
	//XPath xpath = contentDom.g .getXPath();
	//TextPElement para = (TextPElement) xpath.evaluate("//text:p[1]", contentDom, XPathConstants.NODE);
	//OdfTextSpan spanElem = new OdfTextSpan(contentDom);
	//spanElem.setTextContent(TEST_SPAN_TEXT);
	//XPathFactory
	//para.appendChild(spanElem);
	//((OdfTextDocument)outputDocument).getContentRoot().setTextContent("ededweddddddddddddd");
	//((OdfTextDocument)outputDocument).
//	String str = ((OdfTextDocument)outputDocument).getContentRoot().getTextContent().replace("p1", douRequest.getChild().getName());
//	((OdfTextDocument)outputDocument).addText(str);
//	XPath xpath = ((OdfTextDocument)outputDocument).getXPath();
//	contentDom = ((OdfTextDocument)outputDocument).getContentDom();
//	TextPElement para = (TextPElement) xpath.evaluate("//text:T3", contentDom, XPathConstants.NODE);
//	OdfTextSpan spanElem = new OdfTextSpan(contentDom);
//	spanElem.setTextContent(douRequest.getChild().getName());
//	para.setNodeValue(douRequest.getChild().getName());
	// appendChild(spanElem);
	//<text:span text:style-name="T3">
	//OdfTextDocument.newTextTemplateDocument().getXPath();
	//((OdfTextDocument)outputDocument).getXPath().
	//OdfParagraph paragraph = new OdfParagraph(contentDom);
}
