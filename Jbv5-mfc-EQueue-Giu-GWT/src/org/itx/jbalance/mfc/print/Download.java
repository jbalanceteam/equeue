package org.itx.jbalance.mfc.print;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.StringFilter;
import org.itx.jbalance.l1.utils.DateUtils;
import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.DouRequestResultDTO;
import org.itx.jbalance.l2_api.services.DouRequestService;
import org.itx.jbalance.mfc.gwt.server.SerializerHelper;
import org.jboss.logging.Logger;

public class Download extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public static final int BUFSIZE = 256;

	public static  Logger logger = Logger.getLogger(ReportChildCheckInfoBeforeDouRequest2.class.getName());
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doRequest(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doRequest(req, resp);
	}

	private void doRequest(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		String reportType = req.getParameter("type");
		System.out.println("doRequest   reportType: "+reportType);
		if ("registrationNotification".equals(reportType)) {
			DouRequestDTO douRequestDTO = (DouRequestDTO) SerializerHelper.deserializeFromBase64(req.getParameter("douRequestDto"));
			DouRequestResultDTO douRequestResultDTO = (DouRequestResultDTO) SerializerHelper.deserializeFromBase64(req.getParameter("douRequestResultDTO"));
			File generateReport = new ReportRegistrationNotification().printRegistrationNotification(douRequestDTO, douRequestResultDTO);
			doDownload(req, resp, generateReport, generateReport.getName());
//			#330 Повторная печать уведомления в МФЦ
		} else if ("registrationNotification2".equals(reportType)) {
			String rn = req.getParameter("rn");
			String birthdayS = req.getParameter("birthday");
			try {
				Date birthday = new SimpleDateFormat("yyyy-MM-dd").parse(birthdayS);
				DouRequestsSearchParams params = new DouRequestsSearchParams();
				params.rn = new StringFilter(rn);
				DateUtils dateUtils = new DateUtils();
				params.birthday = DateFilter.build(dateUtils.getStartOfDay(birthday), dateUtils.getEndOfDay(birthday));
				List<org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO> douRequests = getDouRequestService().getDouRequests(params);
				if(douRequests.isEmpty()){
					outputErrorMessage("Регистрационный номер не найден", req, resp); 
		            return;
				}
				File generateReport = new ReportRegistrationNotification().printRegistrationNotification(douRequests.get(0));
				doDownload(req, resp, generateReport, generateReport.getName());
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
			
		} else if ("childCheckInfoBeforeDouRequest".equals(reportType)) {
			DouRequestDTO douRequestDTO = (DouRequestDTO) SerializerHelper.deserializeFromBase64(req.getParameter("douRequestDto"));
			
			File generateReport = new ReportChildCheckInfoBeforeDouRequest2().printChildCheckInfoBeforeDouRequest(douRequestDTO);
			System.out.println("doRequest   generateReport: "+generateReport);
			doDownload(req, resp, generateReport, generateReport.getName());
		}
	}

	private void outputErrorMessage(String message, HttpServletRequest req,
			HttpServletResponse resp) throws UnsupportedEncodingException,
			IOException {
			
		    
		   String  srcPage = req.getCharacterEncoding();
		 // Избегая проблем перестрахуемся задав вариант по умолчанию
		 if( srcPage==null ) 
			 srcPage="ISO-8859-1";

		 String  dstPage = req.getParameter("charset");
		 // Избегая проблем перестрахуемся задав вариант по умолчанию
		 if( dstPage==null ) dstPage="UTF-8";

		    StringBuffer sb = new StringBuffer();
		    sb.append( "<html><head>" +
		    		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">" );  
		    sb.append( "<title>Ошибка</title>" );  
		    sb.append( "</head>" );  
		    sb.append( "<body>" );  
		    sb.append( "<h1>"+message+"</h1>" );  
		    sb.append( "</body></html>" );  
		    
		    resp.setContentType( "text/html" );  
		    
		    resp.setHeader("Content-Type","text/html; charset=UTF-8");
		    resp.setCharacterEncoding("UTF-8");
		    resp.setContentType("text/html; charset=UTF-8");
		    PrintWriter out = resp.getWriter();
		    out.println(encode(sb.toString(), srcPage,dstPage));
		    out.flush();
		    
		    resp.flushBuffer();
		    out.close();
	}

	
	static public String encode(String src, String defpage ,String codepage){
		String answer = "";
		try{
			 answer= new String(src.getBytes(defpage), codepage);}
			//тут бывают ошибки если указаной кодировки не существует
		catch (Throwable e){
			answer=src;
		}
		return src;
	} 
	
	public DouRequestService getDouRequestService() throws NamingException{
		Context context = new InitialContext();
		return (DouRequestService)context.lookup(DouRequestService.JNDI_NAME);
	}
	
	/**
	 * Sends a file to the ServletResponse output stream. Typically you want the
	 * browser to receive a different name than the name the file has been saved
	 * in your local database, since your local names need to be unique.
	 * 
	 * @param req
	 *            The request
	 * @param resp
	 *            The response
	 * @param filename
	 *            The name of the file you want to download.
	 * @param original_filename
	 *            The name the browser should receive.
	 */
	private void doDownload(HttpServletRequest req, HttpServletResponse resp,
			File file, String original_filename) throws IOException {

		// File f = new File(filename);
		int length = 0;
		ServletOutputStream op = resp.getOutputStream();
		ServletContext context = getServletConfig().getServletContext();
		String mimetype = context.getMimeType(file.getAbsolutePath()); // .getAbsolutePath()

		//
		// Set the response and go!
		//
		//
		resp.setContentType((mimetype != null) ? mimetype
				: "application/octet-stream");
		resp.setContentLength((int) file.length());
		resp.setHeader("Content-Disposition", "attachment; filename=\""
				+ original_filename + "\"");// original_filename

		//
		// Stream to the requester.
		//
		byte[] bbuf = new byte[BUFSIZE];
		DataInputStream in = new DataInputStream(new FileInputStream(file));

		while ((in != null) && ((length = in.read(bbuf)) != -1)) {
			op.write(bbuf, 0, length);
		}

		in.close();
		op.flush();
		op.close();
	}

}