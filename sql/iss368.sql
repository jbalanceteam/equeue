


update Physical  set  birthday = '2011-01-01'  where uid in(
select p.uid 
from HDouRequest r inner join Physical p on r.child_uid = p.uid
where p.birthday is null and r.closeDate is null and r.version is null and p.closeDate is null and p.version is null
);


-- select date_part('year',regDate)||'-' || dnumber, surname, realname, patronymic , birthday from HDouRequest r inner join Physical p on r.child_uid = p.uid
-- where p.birthday is null and r.closeDate is null and r.version is null and p.closeDate is null and p.version is null
 

drop function IF EXISTS months_between (timestamp, timestamp);
drop function IF EXISTS months_of (interval);

create  function months_of(interval)
 returns int strict immutable language sql as $$
  select extract(years from $1)::int * 12 + extract(month from $1)::int
$$;

create  function months_between(timestamp, timestamp)
 returns int strict immutable language sql as $$
   select abs(months_of(age($1, $2)))
$$;


delete from Permit;

insert into  Permit(uid,
openDate, TS,
number, 
age, permitDate, Dou_UID, 
PRIViLEGE_UID,CHILD_UID, SREGISTER_UID,  HDOUREQUEST_UID)

-- postgres
select dr.uid, current_timestamp, current_timestamp,  
hr.dnumber || '-' ||  sr.number, 
 months_between(ch.birthday, hr.registerDate),

/*
-- MYSQL
select dr.uid,  CURDATE(), CURDATE(),  
hr.dnumber + '-' +  sr.number, 
 TIMESTAMPDIFF(MONTH, ch.birthday, hr.registerDate),
 hr.registerDate, 1, 
 */
 

 hr.registerDate, 
 sr.dou_uid,
 dr.privilege_uid,
 ch.uid,
 sr.uid,
 dr.uid
	  from SRegister sr inner join HRegister hr on sr.huid=hr.uid  
	 inner join HDouRequest dr on dr.uid=sr.DouRequest_uid   
	 inner join Physical ch on ch.uid=dr.child_uid 
	 where  
	 sr.version is null   
	 and hr.version is null   
	 and hr.status='THIRD'  
	 and sr.rnAction = 'GIVE_PERMIT'
	 and hr.closeDate is null   
	 and sr.closeDate is null  

	 and not exists (  
			 select sr2.uid from SRegister  sr2 inner join HRegister hr2 on sr2.huid=hr2.uid   
		 where hr2.closeDate is null 
		 and sr2.closeDate is null 
		 and hr2.version is null   
		 and sr2.version is null   
		 and sr2.DouRequest_uid = sr.DouRequest_uid  
		 and hr2.status='THIRD'  
		 and  hr2.dnumber >   hr.dnumber 
		 )
;


update Permit  set versionroot=uid, SysUser= (select max(uid) from Oper);

delete from hibernate_sequences where sequence_name = 'Permit';
insert into hibernate_sequences values ('Permit',1);

drop function IF EXISTS months_between (timestamp, timestamp);
drop function IF EXISTS months_of (interval);