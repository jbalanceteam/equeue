update HDouRequest r set status='PERMIT' where uid in (select dourequest_uid from SRegister sr inner join HRegister h on sr.Huid = h.uid where sr.version is null and sr.closeDate is null and sr.rnaction = 'GIVE_PERMIT'  
and not exists (select * from SRegister sr2 inner join HRegister h2 on sr2.Huid = h2.uid where h2.dnumber > h.dnumber and sr2.dourequest_uid=sr.dourequest_uid)  ) and r.closeDate is null and r.version is null and status <> 'PERMIT';

update HDouRequest r set status='DELETE_BY_7_YEAR' where uid in (select dourequest_uid from SRegister sr inner join HRegister h on sr.Huid = h.uid where sr.version is null and sr.closeDate is null and sr.rnaction = 'DELETE_BY_7_YEAR'  
and not exists (select * from SRegister sr2 inner join HRegister h2 on sr2.Huid = h2.uid where h2.dnumber > h.dnumber and sr2.dourequest_uid=sr.dourequest_uid)  ) and r.closeDate is null and r.version is null and status <> 'DELETE_BY_7_YEAR';


update HDouRequest r set status='DELETE_BY_PARRENTS_AGREE' where uid in (select dourequest_uid from SRegister sr inner join HRegister h on sr.Huid = h.uid where sr.version is null and sr.closeDate is null and sr.rnaction = 'DELETE_BY_PARRENTS_AGREE'  
and not exists (select * from SRegister sr2 inner join HRegister h2 on sr2.Huid = h2.uid where h2.dnumber > h.dnumber and sr2.dourequest_uid=sr.dourequest_uid)  ) and r.closeDate is null and r.version is null and status <> 'DELETE_BY_PARRENTS_AGREE';


update HDouRequest r set status='WAIT' where uid in (select dourequest_uid from SRegister sr inner join HRegister h on sr.Huid = h.uid where sr.version is null and sr.closeDate is null and sr.rnaction = 'BACK_TO_QUEUE'  
and not exists (select * from SRegister sr2 inner join HRegister h2 on sr2.Huid = h2.uid where h2.dnumber > h.dnumber and sr2.dourequest_uid=sr.dourequest_uid)  ) and r.closeDate is null and r.version is null and status <> 'WAIT';

