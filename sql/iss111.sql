-- !!!!!!!!!!! Перед выполнением JBOSS должен создать таблицы!


 -- Очищаем таблицы - полезно в случае повторного выполнения скрипта
 delete from SRegister;
 delete from HRegister;
 delete from SDouRequest;
 delete from HDouRequest;
 delete from Physical;
 delete from BirthSertificate;
 
 
 delete from SStaffDivision;
 delete from HStaffDivision;
 delete from StaffDivision;
 delete from Dou;
 delete from Privilege;
 delete from Region;
 
 delete from BirthSertificate;
 delete from HDouRequest;
 delete from Physical;
 delete from BirthSertificate;
 
 delete from Oper;
 
 -- Фикс ошибки с удаленными ДОУ
 select  dblink_exec('dbname=equ1','update ubiq set closedate = ''2013-01-01'' from SContractor sc where  ubiq.uid =sc.uid  and   sc.contractor in (select uid from ubiq where closedate is not null)');
  
  
  -- Опер
  insert into Oper (uid, opendate, ts, name, versionroot, login) values (1,current_timestamp,current_timestamp,'Administrator',1,'admin');
  
  
  -- льготы
   insert into Privilege (uid, opendate, TS  ,name,description)  SELECT *   FROM dblink('dbname=equ1','select u.uid, u.openDate, u.TS,  r.name, r.description from Ubiq u inner join Privilege p on u.uid=p.uid inner join relator r on r.uid=u.uid where u.version is null and u.closedate is null') as t1(uid numeric, openDate timestamp, ts timestamp, name text, description text);
   update Privilege set versionroot=uid, SysUser= (select max(uid) from Oper);
   
   
   -- регионы
   insert into Region (uid, opendate, TS ,name,description) SELECT *   FROM dblink('dbname=equ1','select u.uid, u.openDate , u.TS, r.name, r.description from Ubiq u inner join Region p on u.uid=p.uid inner join relator r on r.uid=u.uid where u.version is null and u.closedate is null') as t1(uid numeric, openDate timestamp , ts timestamp, name text, description text);
   update Region set versionroot=uid, SysUser= (select max(uid) from Oper);
   
   -- DOU
   insert into Dou (uid, opendate, TS ,name,description,iddou,douType,region_uid,JAddress,OKONH,OKPO,INN,PAddress,ContactInfo,Phone,EMail)SELECT *   FROM dblink('dbname=equ1','select u.uid,  u.openDate, u.TS, r.name,r.description, d.iddou,  d.douType, d.region_uid, j.JAddress, j.OKONH, j.OKPO, c.INN,c.PAddress, c.ContactInfo , c.Phone, c.EMail  from Ubiq u inner join Juridical j on j.uid=u.uid inner join relator r on r.uid=u.uid inner join dou d on d.uid=u.uid inner join contractor c on c.uid=u.uid  where u.version is null and u.closedate is null') as t1(uid numeric, openDate timestamp, ts timestamp, name text, description text, idDou numeric, dtype text, regionId numeric, JAddress text, OKONH numeric,OKPO numeric, INN text, PAddress text, ContactInfo text, Phone text, EMail text);
   update Dou set versionroot=uid, SysUser= (select max(uid) from Oper);
   
   -- StaffDivision  группы ДОУ
   insert into StaffDivision (uid , opendate , TS, name,description,groupId,douGroupsId,km) SELECT *   FROM dblink('dbname=equ1','select u.uid, u.openDate, u.TS , r.name, r.description , p.groupId, p.douGroupsId, p.km  from Ubiq u inner join StaffDivision p on u.uid=p.uid inner join relator r on r.uid=u.uid where u.version is null and u.closedate is null') as t1(uid numeric, openDate timestamp, ts timestamp, name text, description text,groupId numeric, douGroupsId numeric, km numeric);
   update StaffDivision set versionroot=uid, SysUser= (select max(uid) from Oper);
   
   -- HStaffDivision  группы ДОУ
   insert into HStaffDivision (uid, opendate , TS, Info,Foundation,Dnumber,dou_uid) SELECT *   FROM dblink('dbname=equ1','select u.uid, u.openDate,u.TS , d.Info, d.Foundation , d.Dnumber,sd.dou_uid from Ubiq u inner join HDocument d on u.uid=d.uid inner join HStaffDivision sd on sd.uid=u.uid  where u.version is null and u.closedate is null') as t1(uid numeric,  openDate timestamp, ts timestamp, info text, Foundation text, Dnumber numeric, douUid numeric);
   update HStaffDivision set versionroot=uid, SysUser= (select max(uid) from Oper);
   
   -- SStaffDivision  группы ДОУ
   insert into SStaffDivision (uid, opendate , TS, division_uid,SeqNumber,HUId) SELECT *   FROM dblink('dbname=equ1','select u.uid, u.openDate, u.TS , s.division_uid , sd.SeqNumber, sd.HUId from Ubiq u inner join SStaffDivision s on u.uid=s.uid inner join sdocument sd  on sd.uid=u.uid where u.version is null and u.closedate is null') as t1(uid numeric, openDate timestamp, ts timestamp, division_uid numeric, SeqNumber numeric,HUId numeric);
   update SStaffDivision set versionroot=uid, SysUser= (select max(uid) from Oper);
   
   
   -- BirthSertificate
   insert into BirthSertificate (uid, openDate, TS , seria, number, date, guardian1, guardian2, sex, Name, description) SELECT *   FROM dblink('dbname=equ1','select u.uid, u.openDate, u.TS , bs.seria, bs.number, bs.date, bs.guardian1, bs.guardian2, bs.sex, r.Name, r.description from Ubiq u inner join BirthSertificate bs  on u.uid=bs.uid inner join relator r on r.uid=u.uid where u.version is null and u.closedate is null') as t1(uid numeric, openDate timestamp, ts timestamp, seria text, number text,date timestamp, guardian1 text,  guardian2 text, sex text, Name text, description text);
   update BirthSertificate set versionroot=uid, SysUser= (select max(uid) from Oper);
   
   -- Physical
   insert into  Physical (uid, openDate, TS , RealName , Surname, Patronymic, Passport, fax, birthday, Phone, EMail, ContactInfo, PAddress, INN,name, description )SELECT *   FROM dblink('dbname=equ1','select u.uid, u.openDate, u.TS , p.RealName , p.Surname, p.Patronymic, p.Passport, p.fax, p.birthday, c.Phone, c.EMail, c.ContactInfo, c.PAddress, c.INN, r.name, r.description  from Ubiq u inner join Physical p on u.uid=p.uid inner join relator r on r.uid=u.uid inner join contractor c on c.uid=u.uid  where u.version is null and u.closedate is null and exists (select uid from HDouRequest where child_uid = u.uid)') as t1 (uid numeric, openDate timestamp, ts timestamp, RealName text , Surname text, Patronymic text, Passport text, fax text, birthday timestamp, Phone text, EMail text, ContactInfo text, PAddress text, INN text, name text, description text);
   update Physical set versionroot=uid, SysUser= (select max(uid) from Oper);
   
   -- HDouRequest
   insert into  HDouRequest ( uid, openDate, TS , Info, Foundation , Dnumber, child_uid, birthSertificate_uid, regDate, address, status, year , privilege_uid, comments, oldNumber, sverkDate, recordColor )SELECT *   FROM dblink('dbname=equ1','select u.uid, u.openDate, u.TS , d.Info, d.Foundation , d.Dnumber, req.child_uid, req.birthSertificate_uid, req.regDate, req.address, req.status, req.year , req.privilege_uid, req.comments, req.oldNumber, req.sverkDate, req.recordColor  from Ubiq u inner join HDocument d on u.uid=d.uid inner join  HDouRequest req on req.uid = u.uid where  u.version is null and u.closedate is null') as t1(uid numeric, openDate timestamp, ts timestamp, Info text , Foundation text, Dnumber numeric, child_uid numeric, birthSertificate_uid numeric, regDate timestamp, address text, status text, year numeric , privilege_uid numeric, comments text, oldNumber text , sverkDate timestamp, recordColor numeric);
   update HDouRequest set versionroot=uid, SysUser= (select max(uid) from Oper);
   
   
   -- SDouRequest
    insert into  SDouRequest  (uid, opendate , TS, contractor,SeqNumber, HUId) select * from dblink('dbname=equ1','select u.uid, u.openDate, u.TS , sc.contractor , sd.SeqNumber, sd.HUId from Ubiq u inner join SDouRequest s on u.uid=s.uid inner join sdocument sd  on sd.uid=u.uid inner join Scontractor sc on sc.uid=u.uid  where u.version is null and u.closedate is null') as t1(uid numeric, openDate timestamp, ts timestamp, contractor numeric, SeqNumber numeric,HUId numeric);
    update SDouRequest set versionroot=uid, SysUser= (select max(uid) from Oper);
    
    
    -- HRegister
    insert into  HRegister (uid, openDate, TS , Info, Foundation , Dnumber, registerDate, member1, member2, member3 ) SELECT *   FROM dblink('dbname=equ1','select u.uid, u.openDate, u.TS , d.Info, d.Foundation , d.Dnumber, r.registerDate, r.member1, r.member2, r.member3 from Ubiq u inner join HDocument d on u.uid=d.uid inner join   HRegister r on r.uid=u.uid  where u.version is null and u.closedate is null') as t1(uid numeric, openDate timestamp, ts timestamp, Info text , Foundation text, Dnumber numeric,  registerDate timestamp, member1 text, member2 text, member3 text);
    update HRegister set versionroot=uid, SysUser= (select max(uid) from Oper);
    
    -- SRegister
     insert into  SRegister (uid, openDate, TS , SeqNumber, HUId, rnAction, number, douRequest_uid, dou_uid, division_uid, foundation) SELECT *  FROM dblink('dbname=equ1','select u.uid, u.openDate, u.TS , sd.SeqNumber, sd.HUId, r.rnAction, r.number, r.douRequest_uid, r.dou_uid, r.division_uid, r.foundation from Ubiq u inner join  SRegister r on r.uid=u.uid  inner join sdocument sd  on sd.uid=u.uid  where u.version is null and u.closedate is null and exists (select uid from Ubiq u2  where u2.closeDate is null and u2.version is null and u2.uid = sd.huid )  and exists (select uid from Ubiq u2  where u2.closeDate is null and u2.version is null and u2.uid = r.dourequest_uid )  ') as t1(uid numeric, openDate timestamp, ts timestamp, SeqNumber numeric, HUId numeric, rnAction text, number text, douRequest_uid numeric, dou_uid numeric, division_uid numeric , foundation numeric);
     update SRegister set versionroot=uid, SysUser= (select max(uid) from Oper);
    
    delete from hibernate_sequences;
    insert into hibernate_sequences values ('Contractor',1);
    insert into hibernate_sequences values ('BirthSertificate',1);
    insert into hibernate_sequences values ('Hcontractor',1);
    insert into hibernate_sequences values ('Scontractor',1);
    insert into hibernate_sequences values ('Privilege',1);
    insert into hibernate_sequences values ('Region',1);
    insert into hibernate_sequences values ('HRegister',1);
    insert into hibernate_sequences values ('SRegister',1);
    insert into hibernate_sequences values ('HStaffDivision',1);
    insert into hibernate_sequences values ('SStaffDivision',1);

    
    -- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!  ПОСЛЕ ВЫПОЛНЕНИЯ СКРИПТОВ, ПЕРЕЗАПУСТИТЬ JBOSS, ЧТОБЫ ОН ПОДХВАТИЛ ЗНАЧЕНИЯ UID
    
    