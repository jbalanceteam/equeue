    alter table hibernate_sequences add constraint qwerty unique (sequence_name);

    insert into hibernate_sequences values ('Contractor',1);
    insert into hibernate_sequences values ('BirthSertificate',1);
    insert into hibernate_sequences values ('Hcontractor',1);
    insert into hibernate_sequences values ('Scontractor',1);
    insert into hibernate_sequences values ('Privilege',1);
    insert into hibernate_sequences values ('Region',1);
    insert into hibernate_sequences values ('HRegister',1);
    insert into hibernate_sequences values ('SRegister',1);
    insert into hibernate_sequences values ('HStaffDivision',1);
    insert into hibernate_sequences values ('SStaffDivision',1);

    alter table hibernate_sequences drop constraint qwerty ;
