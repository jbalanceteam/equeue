package org.itx.jbalance;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.itx.jbalance.equeue.gwt.client.DateUtils;
import org.junit.Assert;
import org.junit.Test;

public class DateUtilsTest {

	@Test
	public void test() {

		Assert.assertEquals("0.0", DateUtils.getAge(parseStr("2000-01-01"),
				parseStr("2000-01-01")));
		Assert.assertEquals("1.0", DateUtils.getAge(parseStr("2001-01-01"),
				parseStr("2000-01-01")));
		Assert.assertEquals("2.0", DateUtils.getAge(parseStr("2002-01-01"),
				parseStr("2000-01-01")));
		Assert.assertEquals("0.11", DateUtils.getAge(parseStr("2001-01-01"),
				parseStr("2000-01-02")));
		Assert.assertEquals("5.0", DateUtils.getAge(parseStr("2011-06-06"),
				parseStr("2006-06-02")));
		Assert.assertEquals("100.1", DateUtils.getAge(parseStr("2100-08-09"),
				parseStr("2000-07-06")));
		Assert.assertEquals("6.11", DateUtils.getAge(parseStr("1999-12-31"),
				parseStr("1993-01-01")));
		Assert.assertEquals("6.0", DateUtils.getAge(parseStr("1999-01-01"),
				parseStr("1993-01-01")));
		Assert.assertEquals("6.1", DateUtils.getAge(parseStr("1999-02-01"),
				parseStr("1993-01-01")));
		Assert.assertEquals("6.1", DateUtils.getAge(parseStr("1999-02-07"),
				parseStr("1993-01-07")));


	}

	DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

	private Date parseStr(String s) {
		try {
			return df.parse(s);
		} catch (ParseException e) {
			return null;
		}
	}
}
