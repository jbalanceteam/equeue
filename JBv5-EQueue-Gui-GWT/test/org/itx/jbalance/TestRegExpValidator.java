package org.itx.jbalance;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.itx.jbalance.equeue.gwt.client.DateUtils;
import org.junit.Assert;
import org.junit.Test;

import eu.maydu.gwt.validation.client.validators.standard.RegularExpressionValidator;

public class TestRegExpValidator {

	@Test
	public void test() {
		String regex = "^[а-яА-ЯёЁ]+([ -][а-яА-ЯёЁ]+)*$";
		Assert.assertTrue( "фы-в-ыва".matches(regex));
		Assert.assertTrue( "ё-Ё".matches(regex));
//		new RegularExpressionValidator(new , regexPattern, customMsgKey)
	}
}
