package org.itx.jbalance.equeue.gwt.server;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.itx.jbalance.equeue.gwt.client.services.DouRequestsSearchResultsWrapper;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsService;
import org.itx.jbalance.equeue.gwt.shared.AddDouRequestOperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.CreateNewDouRequestRequest;
import org.itx.jbalance.equeue.gwt.shared.DouRequestDetailsDTO;
import org.itx.jbalance.equeue.gwt.shared.DouRequestListPrerequesedData;
import org.itx.jbalance.equeue.gwt.shared.GetReport5_1Request;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.UpdateDouRequestRequest;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper.Status;
import org.itx.jbalance.equeue.gwt.shared.Report5_1DTO;
import org.itx.jbalance.equeue.gwt.shared.Report5_2DTO;
import org.itx.jbalance.equeue.gwt.shared.Report5_3DTO;
import org.itx.jbalance.equeue.gwt.shared.ReportAgeWithCount;
import org.itx.jbalance.equeue.gwt.shared.ReportsAge;
import org.itx.jbalance.equeue.gwt.shared.ReportsDou;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.DouRequestsSearchParams.OrderStatus;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.IntegerFilter;
import org.itx.jbalance.l1.api.filters.LongFilter;
import org.itx.jbalance.l1.api.filters.NumberFilterType;
import org.itx.jbalance.l1.api.filters.StringFilter;
import org.itx.jbalance.l1.api.filters.StringFilterType;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.d.HRegisterRemote;
import org.itx.jbalance.l1.o.BirthSertificatesRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.PhysicalsRemote;
import org.itx.jbalance.l1.o.PrivilegesRemote;
import org.itx.jbalance.l1.utils.DateUtils;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;
import org.itx.jbalance.l2_api.services.DouRequestService;
import org.itx.jbalance.l2_api.utils.ConvertHelper;

import static org.itx.jbalance.l1.utils.DateUtils.moveDatesToBounds;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class DouRequestsServiceImpl extends MyGWTServlet implements
    DouRequestsService {

	@Override
	public synchronized  DouRequestsSearchResultsWrapper search(DouRequestsSearchParams searchParams) {
		HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(getSessionId());
		DouRequestService service = LookupHelper.getInstance().getDouRequestsSession();
//		System.out.println(recordColor);
		
		moveDatesToBounds(searchParams.regDate);
		moveDatesToBounds(searchParams.birthday);
		moveDatesToBounds(searchParams.sverkDate);

		
		List<DouRequestDTO> douRequests = service.getDouRequests(searchParams);

		DouRequestsSearchParams searchParams4Actives = new DouRequestsSearchParams();
		searchParams4Actives.orderStatus = OrderStatus.ACTIVE_QUEUE;
		return new DouRequestsSearchResultsWrapper(douRequests, ejb.getAllCount(), ejb.getSearchCount(searchParams) , ejb.getSearchCount(searchParams4Actives));
	}

	@Override
	public synchronized  OperationResultWrapper<HDouRequest> delete(Long id) {
		OperationResultWrapper<HDouRequest> result = new OperationResultWrapper<HDouRequest>();
		
		HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(getSessionId());
		
		HRegisterRemote register = LookupHelper.getInstance().getRegister(getSessionId());
		List<SRegister> specByDouRequest = register.getSpecByDouRequest(id);
		if(!specByDouRequest.isEmpty()){
			StringBuffer message = new StringBuffer();
			message.append("РН включен в протокол");
			if(specByDouRequest.size()>1){
				message.append("ы");
			}
			message.append(" ");
			
			for (SRegister sRegister : specByDouRequest) {
				message.append(sRegister.getHUId().getDnumber());
				message.append(", ");
			}
			message.delete(message.length()-2, message.length());
			message.append(". Удаление невозможно!");
			
			result.setComments(message.toString());
			result.setStatus(Status.VALIDATION_ERROR);
		}else{
			HDouRequest deletedObject = ejb.delete(ejb.getByUId(id, HDouRequest.class));
			result.setResult(deletedObject);
			result.setStatus(Status.SUCCESS);
		}
		return result;
	} 

	@Override
	public synchronized  AddDouRequestOperationResultWrapper  createNew(CreateNewDouRequestRequest request) {
		Physical child = request.getChild();
		Physical declarant = request.getDeclarant();
		BirthSertificate birthSertificate = request.getBirthSertificate();
		HDouRequest douRequest = request.getP();
		
		AddDouRequestOperationResultWrapper res = new AddDouRequestOperationResultWrapper();

		HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(getSessionId());
		
		//		#33 Проверка на дублирование записей очередников в списке
		if(!request.isForce()){
			DouRequestsSearchParams params = new DouRequestsSearchParams();
//			Сперва ищем только по ФИО
			params.firstname  = new StringFilter(StringFilterType.EQUALS_IC, child.getRealName());
			params.lastname   = new StringFilter(StringFilterType.EQUALS_IC, child.getSurname());
			params.middlename = new StringFilter(StringFilterType.EQUALS_IC, child.getPatronymic());
			Integer matchFIO = ejb.getSearchCount(params);
			if(matchFIO > 0){
				res.setMatchFio(matchFIO);
//				а теперь к фильтру по ФИО добавляем дату рождения...
				DateUtils dateUtils = new DateUtils();
				params.birthday = DateFilter.build(dateUtils.getStartOfDay(child.getBirthday()), dateUtils.getEndOfDay(child.getBirthday()));
//				Logger.getLogger(getClass()).error("params: "+params);
				Integer matchFIOAndAge = ejb.getSearchCount(params);
				res.setMatchFioAndBirthday(matchFIOAndAge);
				res.setStatus(Status.VALIDATION_ERROR);
				return res;
			}
		}
		
//		Проверка даты
		
//		System.out.println("p.getRegDate(): "+p.getRegDate());
//		
		Date maxDate = ejb.validate(douRequest.getRegDate());
		if(maxDate!=null){
//			res.setComments("Дата должна быть больше или равна дате предыдущего РН за этот год ("+new SimpleDateFormat("dd.MM.yyyy").format( maxDate)+").");
			res.setComments("Дата должна быть больше или равна дате предыдущего РН ("+new SimpleDateFormat("dd.MM.yyyy").format( maxDate)+").");
			res.setStatus(Status.VALIDATION_ERROR);
			return res;
		}

		Date currentDate = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(currentDate);
		cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
		cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
		
//		#35 Проверка на дату рождения - она не может быть больше текущего числа.
		if(cal.getTimeInMillis() < child.getBirthday().getTime()){
			res.setComments("Ошибка в дате рождения, она должна быть меньше чем " + new SimpleDateFormat("dd.MM.yyyy").format( cal.getTime())+".");
			res.setStatus(Status.VALIDATION_ERROR);
			return res;
		}
		
//		#231 проверка на дату рождения
		DateUtils dateUtils = new DateUtils();
		Calendar regCal = Calendar.getInstance();
		regCal.setTime(douRequest.getRegDate());
		regCal.add(Calendar.DATE, -5);
		Date regMin5 = dateUtils.getStartOfDay(regCal.getTime());
		if(regMin5.getTime() < child.getBirthday().getTime()){
			res.setComments("Дата рождения должна быть меньше чем дата регистрации - 5 дней(" + new SimpleDateFormat("dd.MM.yyyy").format(regMin5) + ").");
			res.setStatus(Status.VALIDATION_ERROR);
			return res;
		}
		
//		#34 Некорректная дата регистрации
		if(cal.getTimeInMillis() < douRequest.getRegDate().getTime()){
			res.setComments("Некорректная дата регистрации, она должна быть меньше чем " + new SimpleDateFormat("dd.MM.yyyy").format( cal.getTime()) + ".");
			res.setStatus(Status.VALIDATION_ERROR);
			return res;
		}
		
		if(request.getRaiting()==null || request.getRaiting().isEmpty()){
			res.setComments("Список ДОУ не должен быть пустым.");
			res.setStatus(Status.VALIDATION_ERROR);
			return res;
		}
		
		PhysicalsRemote physicalsRemote = LookupHelper.getInstance().getPhysical(getSessionId());
		child =  physicalsRemote.create(child);
		
		douRequest.setChild(child);
		
		declarant = physicalsRemote.create(declarant);
		douRequest.setDeclarant(declarant);
		
		BirthSertificatesRemote birthSertificatesRemote = LookupHelper.getInstance().getBirthSertificates(getSessionId());
		birthSertificate = birthSertificatesRemote.create(birthSertificate);
		douRequest.setBirthSertificate(birthSertificate);
		
		douRequest.setStatus(org.itx.jbalance.l0.h.HDouRequest.Status.WAIT);
		
		douRequest = ejb.create(douRequest);
		for (Entry<Dou, Integer> entry : request.getRaiting().entrySet()) {
			if(entry.getValue() > 0){
				SDouRequest line=new SDouRequest();
				line.setArticleUnit(entry.getKey());
				line.setRating(entry.getValue());
				line = ejb.createSpecificationLine(line, douRequest);
			}
		}
		res.setResult(douRequest);
		res.setStatus(Status.SUCCESS);
		return  res;
	}

	@Override
	public synchronized DouRequestDetailsDTO getDetails(Long uid) {
		HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(getSessionId());
		HDouRequest doc = ejb.getByUId(uid, HDouRequest.class);
		
		System.out.println("doc.getChild().getBirthday(): " + doc.getChild().getBirthday());
//		ставим 12 часов, чтобы при изменении таймзоны не поплыла дата		
		if(doc.getChild().getBirthday() != null){
			doc.getChild().getBirthday().setHours(12);
		}
		
		DouRequestDetailsDTO res=new DouRequestDetailsDTO();
		res.setDoc(doc);
		res.setSpec(ejb.getSpecificationLines(doc));
		
		HRegisterRemote registerEJB = LookupHelper.getInstance().getRegister(getSessionId());
		res.setMoves(registerEJB.getSpecByDouRequest(uid));
		return res;
	}


	@Override
	public synchronized  OperationResultWrapper<HDouRequest> update(UpdateDouRequestRequest request) {
		Physical child = request.getChild();
		BirthSertificate birthSertificate = request. getBirthSertificate(); 
		HDouRequest p = request.getP();
		Physical declarant = request.getDeclarant();
		
		OperationResultWrapper<HDouRequest> res=new OperationResultWrapper<HDouRequest>();
		PhysicalsRemote physicalsEJB = LookupHelper.getInstance().getPhysical(getSessionId());
		child = physicalsEJB.update(child);
		
		if(declarant.getTS() != null){
			declarant = physicalsEJB.update(declarant);
		} else {
			declarant = physicalsEJB.create(declarant);
			p.setDeclarant(declarant);
		}
		
		BirthSertificatesRemote birthSertificatesEJB = LookupHelper.getInstance().getBirthSertificates(getSessionId());
		birthSertificate = birthSertificatesEJB.update(birthSertificate);
		
		HDouRequestsRemote hDouRequestsEJB = LookupHelper.getInstance().getHDouRequests(getSessionId());
		HDouRequest oldVersion = hDouRequestsEJB.getByUId(p.getUId(), HDouRequest.class);
		p.setStatus(oldVersion.getStatus());
		p.setFoundation(oldVersion.getFoundation());
		p = hDouRequestsEJB.update(p);
		hDouRequestsEJB.synchronizeSpecification(p, request.getRaiting());
		res.setResult(p);
		res.setStatus(Status.SUCCESS);
		return res;
	}

	/**
	 * Возвращает наполнение  для 1-го отчета "Распределение по возрасту"
	 */
	@Override
	public List<Report5_1DTO> getReport5_1Data(GetReport5_1Request request) {
		HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(getSessionId());
		
		Calendar cal = Calendar.getInstance();
		if(request.getEffectiveDate() != null){
			cal.setTime(request.getEffectiveDate());
		}
		int currentYear = cal.get(Calendar.YEAR);
		
		for (Report5_1DTO r : request.getAges()) {
			DouRequestsSearchParams params = new DouRequestsSearchParams();
			params.setEffectiveDate(request.getEffectiveDate());
			params.ageFrom=r.getFrom();
			params.ageTo=r.getTo();
			params.plainYear = null;
			params.orderStatus=OrderStatus.ACTIVE_QUEUE;
			Integer cnt = ejb.getSearchCountWithEffectiveDate(params);
			r.setCount(cnt);
			
			params.orderStatus=OrderStatus.REJECT;
			Integer rejectedCount = ejb.getSearchCountWithEffectiveDate(params);
			r.setRejectedCount(rejectedCount);	
			
			/* Issue #370  Добавить в отчтет №1 Потребность в местах */
			params.orderStatus=OrderStatus.ACTIVE_QUEUE;
			params.plainYear = new IntegerFilter(NumberFilterType.LTE, currentYear);
			Integer lte = ejb.getSearchCountWithEffectiveDate(params);
			
			params.plainYear = new IntegerFilter(NumberFilterType.NULL, (Integer)null);
			Integer nll = ejb.getSearchCountWithEffectiveDate(params);
			
			Integer howMuchNeed = lte + nll;
			r.setHowMuchNeed(howMuchNeed);
			
		}	
		return request.getAges();
	}

	@Override
	public Report5_2DTO getReport5_2Data(Report5_2DTO dto) {
		HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(getSessionId());
		PrivilegesRemote privilegesEjb = LookupHelper.getInstance().getPrivileges(getSessionId());

		@SuppressWarnings("unchecked")
		List<Privilege> all = (List<Privilege>) privilegesEjb.getAll(Privilege.class);
		dto.setPrivileges(new ArrayList<Privilege>( all));

		for (ReportAgeWithCount r : dto.getAges()) {
			for (Privilege privilege : all) {
				DouRequestsSearchParams params = new DouRequestsSearchParams();
				params.ageFrom=r.getFrom();
				params.ageTo=r.getTo();
				params.orderStatus=OrderStatus.ACTIVE_QUEUE;
				params.exact_privilege=privilege.getUId();
				Integer cnt = ejb.getSearchCount(params);
				r.getCount().put(privilege.getUId(), cnt);
			}	
		}
		return dto;
	}

	@Override
	public Report5_3DTO getReport5_3Data(Report5_3DTO dto) {
		HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(getSessionId());
		DousRemote dousEjb = LookupHelper.getInstance().getDous(getSessionId());

		List<? extends Dou> all = dousEjb.getAll(Dou.class);
		dto.setRows(new ArrayList<ReportsDou>());
		for (Dou d : all) { 
			ReportsDou e = new ReportsDou();
			e.setDou(d);
			for (ReportsAge r : dto.getAges()) {

				DouRequestsSearchParams params = new DouRequestsSearchParams();
				params.ageFrom=r.getFrom();
				params.ageTo=r.getTo();
				params.orderStatus=OrderStatus.ACTIVE_QUEUE;
				params.searchedDous=new ArrayList<Long>();
				params.searchedDous.add(d.getUId());
				Integer cnt = ejb.getSearchCount(params);

				e.getCount().put(r, cnt);

			}	
			dto.getRows().add(e);
		}
		return dto;
	}

	@Override
	public OperationResultWrapper<DouRequestDTO> updatePhoneNumber(Long uid,
			String newPhone) {
		OperationResultWrapper<DouRequestDTO> operationResultWrapper = new OperationResultWrapper<DouRequestDTO>();
		operationResultWrapper.setStatus(Status.SERVER_ERROR);
		HDouRequestsRemote hDouRequestsRemote = LookupHelper.getInstance().getHDouRequests(getSessionId());
		PhysicalsRemote physicalsRemote = LookupHelper.getInstance().getPhysical(getSessionId());
		
		HDouRequest douRequest = hDouRequestsRemote.getByUId(uid, HDouRequest.class);
		douRequest.getChild().setPhone(newPhone);
		
//		hDouRequestsRemote.update(douRequest);
		physicalsRemote.update(douRequest.getChild());
		
		DouRequestsSearchParams params = new DouRequestsSearchParams();
		params.uid = new LongFilter(uid);
		List<DouRequestDTO> douRequests = LookupHelper.getInstance().getDouRequestsSession().getDouRequests(params);
		if(!douRequests.isEmpty()){
			operationResultWrapper.setResult(douRequests.get(0));
			operationResultWrapper.setStatus(Status.SUCCESS);
		} 
		return operationResultWrapper;
	}

	@Override
	public DouRequestListPrerequesedData getDouRequestListPrerequesedData() {
		DouRequestListPrerequesedData result = new DouRequestListPrerequesedData();
		
		DousRemote dousRemote = LookupHelper.getInstance().getDous(getSessionId());
		List<? extends Dou> allDou = dousRemote.getAll(Dou.class);
		
		Map <Region,List<Dou>> dous = new HashMap<Region, List<Dou>>();	
		for (Dou dou : allDou) {
			Region region = dou.getRegion();
			if(region == null){
				continue;
			}
			
			if(!dous.containsKey(region)){
				dous.put(region, new ArrayList<Dou>());
			}
			
			dous.get(dou.getRegion()).add(dou);
		}
		result.setDous(dous);
		
		PrivilegesRemote privilegesEjb = LookupHelper.getInstance().getPrivileges(getSessionId());
		List<? extends Privilege> all = privilegesEjb.getAll(Privilege.class);
		for (Privilege privilege : all) {
			result.getPrivileges().add(privilege);
		}
		
		return result;
	}

	@Override
	public List<DouRequestDTO> getBadBdRecords() {
		HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(getSessionId());
		final DouRequestsSearchParams params = new DouRequestsSearchParams();
		params.setFrom(0);
		params.setTo(5000);
		final List<HDouRequest> dbos = ejb.search(params);
		
		final ArrayList<DouRequestDTO> result = new ArrayList<DouRequestDTO>(5000);
		
		for (HDouRequest dbo : dbos) {
			final DouRequestDTO dto = new DouRequestDTO();
			ConvertHelper.douRequestsDBOToDTO(dbo, dto);
			result.add(dto);
		}
		return result;
	}



	@Override
	public void fixBadBdRecords(Map<Long, String> map) {
		final HDouRequestsRemote douRequestsRemote = LookupHelper.getInstance().getHDouRequests(getSessionId());
		final PhysicalsRemote physicalsRemote = LookupHelper.getInstance().getPhysical(getSessionId());
		final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		for (Long id : map.keySet()) {
			final HDouRequest byUId = douRequestsRemote.getByUId(id, HDouRequest.class);
			final Physical child = byUId.getChild();
			try {
				final Date birthday = dateFormat.parse(map.get(id));
				 child.setBirthday(birthday);
				 physicalsRemote.update(child);
			} catch (ParseException e) {
				e.printStackTrace();
//				Logger.getLogger(getClass()).error("",  e);
			}
		}
	}
		
}
