package org.itx.jbalance.equeue.gwt.server;

import java.util.ArrayList;

import org.itx.jbalance.equeue.gwt.client.services.RegionsService;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l1.o.RegionsRemote;


/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class RegionsServiceImpl extends MyGWTServlet implements
  RegionsService {
	

	@SuppressWarnings("unchecked")
	@Override
	public synchronized ArrayList<Region> getAll() {
			return new ArrayList<Region>(LookupHelper.getInstance().getRegions(getSessionId()).getAll(Region.class));
	}  

	@Override
	public synchronized void createNew(Region p) { 
		System.out.println("createNew("+p+")");
		RegionsRemote service = LookupHelper.getInstance().getRegions(getSessionId());
		p = service.create(p);
		
	}
	@Override
	public synchronized void delete(Long id) {
		RegionsRemote service = LookupHelper.getInstance().getRegions(getSessionId());
		service.delete(id);

	}

	@Override
	public synchronized void update(Region p) {
		RegionsRemote service = LookupHelper.getInstance().getRegions(getSessionId());
		service.update(p);
	}

}
