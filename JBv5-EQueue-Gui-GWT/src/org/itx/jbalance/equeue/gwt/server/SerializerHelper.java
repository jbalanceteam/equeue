package org.itx.jbalance.equeue.gwt.server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.google.gwt.user.server.Base64Utils;

public class SerializerHelper {
	
	public static String serializeToBase64(Serializable obj) {
		try {
		ByteArrayOutputStream arrayOutputStream=new ByteArrayOutputStream();
		ObjectOutputStream objectOutputStream=new ObjectOutputStream(arrayOutputStream);
		objectOutputStream.writeObject(obj);
		objectOutputStream.flush();
		objectOutputStream.close();
		arrayOutputStream.flush();
		arrayOutputStream.close();
		return Base64Utils.toBase64(arrayOutputStream.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
			
	}
	
	
	
	public static Serializable deserializeFromBase64(String base64) {
		try {
			
			 byte[] bytes = Base64Utils.fromBase64(base64);
		ByteArrayInputStream bis=new ByteArrayInputStream(bytes);
		ObjectInputStream ois=new ObjectInputStream(bis);
		
		ois.close();
		bis.close();
		return (Serializable) ois.readObject();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		
	}
}
