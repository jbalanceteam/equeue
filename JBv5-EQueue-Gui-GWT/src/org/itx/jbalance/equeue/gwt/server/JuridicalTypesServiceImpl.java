package org.itx.jbalance.equeue.gwt.server;

import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.itx.jbalance.equeue.gwt.client.services.JuridicalTypesService;
import org.itx.jbalance.l0.o.JuridicalType;
import org.itx.jbalance.l1.o.JuridicalTypesRemote;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class JuridicalTypesServiceImpl extends RemoteServiceServlet implements
    JuridicalTypesService {
	
	@Override
	public ArrayList<JuridicalType> getAll() {
			return new ArrayList<JuridicalType>(getService().getAll());
	}  
	
	
	
	private JuridicalTypesRemote getService() {
		try {
			Context context = new InitialContext();
			return (JuridicalTypesRemote) context.lookup("JuridicalTypes/remote");
		} catch (NamingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
	}

}
