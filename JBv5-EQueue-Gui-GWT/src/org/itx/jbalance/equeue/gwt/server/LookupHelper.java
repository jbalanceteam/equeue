package org.itx.jbalance.equeue.gwt.server;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.security.auth.login.LoginContext;

import org.itx.jbalance.l1.common.Common;
import org.itx.jbalance.l1.d.HAutoRegisterRemote;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.d.HFreeDivisionsRemote;
import org.itx.jbalance.l1.d.HRegisterRemote;
import org.itx.jbalance.l1.d.HStaffDivisionsRemote;
import org.itx.jbalance.l1.o.BirthSertificatesRemote;
import org.itx.jbalance.l1.o.DOUGroupsRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.JuridicalsRemote;
import org.itx.jbalance.l1.o.PhysicalsRemote;
import org.itx.jbalance.l1.o.PrivilegesRemote;
import org.itx.jbalance.l1.o.RegionsRemote;
import org.itx.jbalance.l1.o.StaffDivisionsRemote;
import org.itx.jbalance.l2_api.services.AttendanceService;
import org.itx.jbalance.l2_api.services.DouRequestService;
import org.itx.jbalance.l2_api.services.RegisterService;
import org.jboss.logging.Logger;
//import org.jboss.security.auth.callback.UsernamePasswordHandler;

public class LookupHelper  {
	
//	private static Map<String, Common<?>> beans = Collections.synchronizedMap(new HashMap<String, Common<?>>()); 
	
	public void login(String sessionId) {
//		SessionprofileRemote sessionProfile = getBean(SessionprofileRemote.class, sessionId);
//		try{
//		sessionProfile.setUser();
//		}catch(NoSuchEJBException e){
//			beans.clear();
//			login(sessionId);
//		}catch(java.lang.reflect.UndeclaredThrowableException e){
//			beans.clear();
//			login(sessionId);
//		}
	}
	
	public JuridicalsRemote getJuridicals(String sessionId){
		return getBean(JuridicalsRemote.class, sessionId);
	}
	
	public PhysicalsRemote getPhysical(String sessionId){
		return getBean(PhysicalsRemote.class, sessionId);
	}
	
	public HRegisterRemote getRegister(String sessionId){
		return getBean(HRegisterRemote.class, sessionId);
	}
	
	public DOUGroupsRemote getDOUGroups(String sessionId){
		try{
			Context context = new InitialContext();
			String simpleName = DOUGroupsRemote.class.getSimpleName();
			simpleName=simpleName.substring(0, simpleName.indexOf("Remote"));
			DOUGroupsRemote result = (DOUGroupsRemote)context.lookup(simpleName + "/remote");
			return result;
		}catch (Throwable e) {
			Logger.getLogger(getClass()).error("Error on Lockup EJB ", e);
			throw new RuntimeException(e);
		}
	}
	
	public HAutoRegisterRemote getAutoRegister(String sessionId){
		return getBean(HAutoRegisterRemote.class, sessionId);
	}
	
	public AttendanceService getAttendanceService(){
		try{
			Context context = new InitialContext();
			return (AttendanceService)context.lookup(AttendanceService.JNDI_NAME);
		}catch (Throwable e) {
			Logger.getLogger(getClass()).error("",e);
			return null;
		}
	}
	
	public PrivilegesRemote getPrivileges(String sessionId){
		return getBean(PrivilegesRemote.class, sessionId);
	}
	
	public HDouRequestsRemote getHDouRequests(String sessionId){
		return getBean(HDouRequestsRemote.class, sessionId);
	}
	
	public DouRequestService getDouRequestsSession(){
		try{
			Context context = new InitialContext();
			return (DouRequestService)context.lookup(DouRequestService.JNDI_NAME);
		}catch (Throwable e) {
			Logger.getLogger(getClass()).error("",e);
			return null;
		}
	}
	
	
	public RegisterService getRegisterService(){
		try{
			Context context = new InitialContext();
			return (RegisterService)context.lookup(RegisterService.JNDI_NAME);
		}catch (Throwable e) {
			Logger.getLogger(getClass()).error("",e);
			return null;
		}
	}
	
	public DousRemote getDous(String sessionId) {
		return getBean(DousRemote.class, sessionId);
	}
	
	
	public StaffDivisionsRemote getStaffDivisions(String sessionId) {
		return getBean(StaffDivisionsRemote.class, sessionId);
	}
	public HStaffDivisionsRemote getHStaffDivision(String sessionId) {
		return getBean(HStaffDivisionsRemote.class, sessionId);
	}
	public HFreeDivisionsRemote getHFreeDivision(String sessionId) {
		return getBean(HFreeDivisionsRemote.class, sessionId);
	}
	
	public BirthSertificatesRemote getBirthSertificates(String sessionId) {
		return getBean(BirthSertificatesRemote.class, sessionId);
	}
	public RegionsRemote getRegions(String sessionId) {
		return getBean(RegionsRemote.class, sessionId);
	}
	
//	private void debugPrintState(){
//		Set<String> keysSet = beans.keySet();
//		ArrayList<String> keysList = new ArrayList<String>(keysSet);
//		Collections.sort(keysList);
//		for (String string : keysList) {
//			Common<?> common = beans.get(string);
//			Logger.getLogger(getClass()).debug(string + " - "+common);
//		}
//	}
	
//	public JuridicalTypesRemote getJuridicalTypes(String sessionId){
//		JuridicalTypesRemote p = getBean(JuridicalTypesRemote.class, sessionId);
//		p.setRights((Sessionprofile)getBean(SessionprofileRemote.class, sessionId));
//		return p;
//	}
	
	

	
	@SuppressWarnings("unchecked")
	public <T extends Common<?>>  T getBean(Class<T> c, String sessionId){
//		debugPrintState();
//		String keyName = getKeyName(c, sessionId);
//		Logger.getLogger(getClass()).debug("getBean("+c+", "+sessionId+")   keyName: " +keyName);
//		if (beans.containsKey(keyName)){
//			T res = (T)beans.get(keyName);
//			Проверяем, живой EJB или нет
//			try{
////				TODO Возможно нужно использовать другой(специальный?) метод
//				res.postactivate();
//				return res;
//			}catch(javax.ejb.NoSuchEJBException e){
//				Log.warn("",e);
//				beans.remove(keyName);
//				return getBean(c,sessionId);
//			}
//			
//		}
		try{
//			UsernamePasswordHandler handler = new UsernamePasswordHandler("admin", "11".toCharArray());
//			System.setProperty("java.security.auth.login.config", 
//					"/home/apv/projects/sphinx/Queue/JBv5-EQueue/resources/jaas.config");
			try {
				LoginContext lc = new LoginContext("client-login"/*, handler*/);
				lc.login();
			} catch (Exception e) {
				// TODO: handle exception
			}
			Context context = new InitialContext();
			String simpleName = c.getSimpleName();
			simpleName=simpleName.substring(0, simpleName.indexOf("Remote"));
			T result = (T)context.lookup(simpleName + "/remote");
			
//			if(! (result instanceof Sessionprofile))
//				result.setRights((Sessionprofile)getBean(SessionprofileRemote.class, sessionId));
			
//			beans.put(keyName, result);
			return result; 
		}catch (Throwable e) {
			Logger.getLogger(getClass()).error("Error on Lockup EJB ", e);
			throw new RuntimeException(e);
		}
	}
//
//	private static String getKeyName(Class<?> c, String sessionId) {
//		return sessionId + "-" + c.getSimpleName();
//	}
//	
//	
//	
	public static LookupHelper getInstance(){
		return new LookupHelper();
	}

	
}
