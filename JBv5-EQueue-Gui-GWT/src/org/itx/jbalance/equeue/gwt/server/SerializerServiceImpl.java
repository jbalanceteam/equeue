package org.itx.jbalance.equeue.gwt.server;

import java.io.Serializable;

import org.itx.jbalance.equeue.gwt.client.services.SerializerService;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


@SuppressWarnings("serial")
public class SerializerServiceImpl  extends RemoteServiceServlet //extends MyGWTServlet 
	implements Serializable ,SerializerService{

	@Override
	public String serializeToBase64(Serializable obj) {
		return SerializerHelper.serializeToBase64(obj);
	}
}
