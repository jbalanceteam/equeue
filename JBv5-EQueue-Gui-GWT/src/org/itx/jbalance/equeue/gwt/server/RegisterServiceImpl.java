package org.itx.jbalance.equeue.gwt.server;

import static org.itx.jbalance.l1.utils.DateUtils.moveDatesToBounds;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.services.PermitsSearchResultsWrapper;
import org.itx.jbalance.equeue.gwt.client.services.RegisterService;
import org.itx.jbalance.equeue.gwt.client.services.SearchResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper.Status;
import org.itx.jbalance.equeue.gwt.shared.RegisterEditData;
import org.itx.jbalance.equeue.gwt.shared.RegisterLine;
import org.itx.jbalance.equeue.gwt.shared.SaveRegisterResult;
import org.itx.jbalance.equeue.gwt.shared.SetRegNumsRequest;
import org.itx.jbalance.equeue.gwt.shared.SetRegNumsResult;
import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Foundation;
import org.itx.jbalance.l1.api.RegisterSearchParams;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.d.HRegisterRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsResult;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterLineDTO;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterPrerequisiteInfo;
import org.itx.jbalance.l2_api.dto.equeue.RegisterDTO;
import org.itx.jbalance.l2_api.services.AttendanceService;


/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class RegisterServiceImpl extends MyGWTServlet implements RegisterService {

	@Override
	public synchronized SearchResultWrapper<RegisterDTO> search(RegisterSearchParams params) {
		org.itx.jbalance.l2_api.services.RegisterService registerService = LookupHelper.getInstance().getRegisterService();
		HRegisterRemote ejb = LookupHelper.getInstance().getRegister(getSessionId());

		moveDatesToBounds(params.registerDate);
		
		List<RegisterDTO> registers = registerService.getRegisters(params);
		
		SearchResultWrapper<RegisterDTO> res = new SearchResultWrapper<RegisterDTO>(registers,
				ejb.getAllCount(),
				ejb.getSearchCount(params));
		return res;
	}

	@Override
	public SaveRegisterResult create(HRegister register,
			List<RegisterLine> spec) {
	
		SaveRegisterResult result=new SaveRegisterResult();
		
		if(	spec==null || spec.isEmpty()){
			result.setComments("Список очередников не должен быть пустым.");
			result.setStatus(Status.VALIDATION_ERROR);
			return result;
		}
		
		
		
		try{
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(getSessionId());
			HDouRequestsRemote requestsEjb = LookupHelper.getInstance().getHDouRequests(getSessionId());
			register = ejb.create(register);
			
			for (RegisterLine s : spec) {
				if(s.getDouRequest()!=null && s.getDouRequest().getUId()!=null){
				 s.setDouRequest(requestsEjb.getByUId(s.getDouRequest().getUId(), HDouRequest.class));
				 SRegister createSpecificationLine = ejb.createSpecificationLine(s.getSRegister() , register );
				 s.setDouRequest(createSpecificationLine.getDouRequest());
				 s.setSRegister(createSpecificationLine);
				 if(s.getSDouRequests() == null || s.getSDouRequests().isEmpty()){
					 List<SDouRequest> specificationLines = requestsEjb.getSpecificationLines(createSpecificationLine.getDouRequest());
					 s.setSDouRequests(specificationLines);
				 }
				}
			}
			result.setStatus(Status.SUCCESS);
			register = ejb.getByUId(register.getUId(), HRegister.class);
			result.setResult(register);
			result.setRegisterLines(getSpec(register));
		}catch(Exception e){
//			Log.error("",e);
			result.setComments(e.getMessage());
			result.setStatus(Status.SERVER_ERROR);
//			throw new RuntimeException(e);
		}
		
		
		return result;
	}

	@Override
	public List<SRegister> getSpec(Long uid) {
		HRegisterRemote ejb = LookupHelper.getInstance().getRegister(getSessionId());
		HRegister register = ejb.getByUId(uid, HRegister.class);
		return ejb.getSpecificationLines(register);
	}
	
	
	@Override
	public synchronized  void delete(Long id) {
		HRegisterRemote ejb = LookupHelper.getInstance().getRegister(getSessionId());
		ejb.delete(ejb.getByUId(id, HRegister.class));

	}

	@Override
	public List<Foundation> getFoundations() {
		HRegisterRemote ejb = LookupHelper.getInstance().getRegister(getSessionId());
		return ejb.getFoundations();
	}

	@Override
	public SaveRegisterResult update(HRegister register, List<RegisterLine> spec) {
		try{
			SaveRegisterResult result=new SaveRegisterResult();
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(getSessionId());
			register = ejb.update(register);
			
			List<SRegister> specToSync = new ArrayList<SRegister>();
			for (RegisterLine registerLine : spec) {
				specToSync.add(registerLine.getSRegister());
			}
			ejb.synchronizeSpecification(register, specToSync);
			
			result.setStatus(Status.SUCCESS);
			populateSaveRegisterResult(result, register.getUId());
			return result;
		} catch(Exception e){
			e.printStackTrace();
			SaveRegisterResult res = new SaveRegisterResult();
			populateSaveRegisterResult(res, register.getUId());
			res.setStatus(Status.SERVER_ERROR);
			res.setComments(e.getMessage());
			return res;
		}
	}

	private void populateSaveRegisterResult(SaveRegisterResult registerResult, Long uid) {
		try{
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(getSessionId());
			HRegister register = ejb.getByUId(uid, HRegister.class);
			registerResult.setResult(register);
			registerResult.setRegisterLines(getSpec(register));
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	private List<RegisterLine> getSpec(HRegister register) {
		HDouRequestsRemote requestsEjb = LookupHelper.getInstance().getHDouRequests(getSessionId());
		HRegisterRemote ejb = LookupHelper.getInstance().getRegister(getSessionId());
		
		List<SRegister> specificationLines = ejb.getSpecificationLines(register);
		List<RegisterLine> registerLines = new ArrayList<RegisterLine>();
		for (SRegister sRegister : specificationLines) {
			RegisterLine registerLine = new RegisterLine();
			HDouRequest douRequest = sRegister.getDouRequest();
			registerLine.setDouRequest(douRequest);
			
			List<SDouRequest> douRequests = requestsEjb.getSpecificationLines(douRequest);
			registerLine.setSDouRequests(douRequests);
			registerLine.setSRegister(sRegister);
			
			
			List<SRegister> specByDouRequest = ejb.getSpecByDouRequest(douRequest.getUId());
			for (SRegister sReg : specByDouRequest) {
//				этот же протокол
				if(sReg.getHUId().getUId() == null || sReg.getHUId().getUId().equals(register.getUId())){
					continue;
				}
				
				if(sReg.getHUId().getStatus().getNumber() == 3){
					continue;
				}
				registerLine.getWhereElseChildIs().add(sReg);
			}
			registerLines.add(registerLine);
			
		}
		return registerLines;
	}
	
	
	@Override
	public RegisterEditData getEditData(Long uid) {
		RegisterEditData res = new RegisterEditData();
		res.setAllDou(getAllDous());
		res.setAgeGroups( LookupHelper.getInstance().getDOUGroups(getSessionId()).getAll());
		
		if(uid!=null){
			HRegister  register = LookupHelper.getInstance().getRegister(getSessionId()).getByUId(uid, HRegister.class);
			res.setRegister(register);
			res.setRegisterLines(getSpec(register));
		}
		return res;
	}

	private ArrayList<Dou> getAllDous() {
		DousRemote ejb = LookupHelper.getInstance().getDous(getSessionId());
		ArrayList<Dou> allDous = new ArrayList<Dou>((Collection<? extends Dou>) ejb.getAll(Dou.class));
		return allDous;
	}
	
	@Override
	public SetRegNumsResult getSetRegNumsData(SetRegNumsRequest request) {
		HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(getSessionId());
		HRegisterRemote registerEjb = LookupHelper.getInstance().getRegister(getSessionId());
		SetRegNumsResult res = new SetRegNumsResult();
		res.setAllDou(getAllDous());
		res.setAgeGroups( LookupHelper.getInstance().getDOUGroups(getSessionId()).getAll());
		
		for (Long id : request.getIds()) {
			HDouRequest douRequest = ejb.getByUId(id, HDouRequest.class);
			RegisterLine registerLine = new RegisterLine();
			registerLine.setDouRequest(douRequest);
			List<SDouRequest> specificationLines = LookupHelper.getInstance().getHDouRequests(getSessionId()).getSpecificationLines(douRequest);
			 
			
			List<SRegister> specByDouRequest = registerEjb.getSpecByDouRequest(douRequest.getUId());
			for (SRegister sReg : specByDouRequest) {
//				этот же протокол
				if(request.getRegister() == null || sReg.getHUId().getUId() == null || sReg.getHUId().getUId().equals(request.getRegister().getUId())){
					continue;
				}
				
				if(sReg.getHUId().getStatus().getNumber() == 3){
					continue;
				}
				registerLine.getWhereElseChildIs().add(sReg);
			}
			
			registerLine.setSDouRequests(specificationLines);
			res.getRegisterLines().add(registerLine);
		}
		return res;
	}

	@Override
	public SaveRegisterResult changeStatus(HRegister register, List<RegisterLine> spec, RegisterStatus newStatus){
		try{
			SaveRegisterResult update = update(register, spec);
			if(update.getStatus() != OperationResultWrapper.Status.SUCCESS){
				return update;
			} else {
				register = update.getResult();
			}
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(getSessionId());
			register = ejb.changeStatus(register.getUId(), newStatus);
			SaveRegisterResult result = new SaveRegisterResult();
			result.setStatus(Status.SUCCESS);
			register = ejb.getByUId(register.getUId(), HRegister.class);
			result.setResult(register);
			result.setRegisterLines(getSpec(register));
			return result;
		} catch(Exception e){
			e.printStackTrace();
			SaveRegisterResult res = new SaveRegisterResult();
			res.setStatus(Status.SERVER_ERROR);
			res.setComments(e.getMessage());
			return res;
		}
		
	}

	@Override
	public PermitsSearchResultsWrapper searchPermits(
			GetPermitsRequest searchParams) {
		AttendanceService attendanceService = LookupHelper.getInstance().getAttendanceService();
		GetPermitsResult permits = attendanceService.getPermits(searchParams);
		return new PermitsSearchResultsWrapper(permits.getPermits(), permits.getPermitsCount(), permits.getPermitsByQueryCount());
	}


	@Override
	public AutoRegisterPrerequisiteInfo getAutoRegisterPrerequisiteInfo() {
		return LookupHelper.getInstance().getRegisterService().getAutoRegisterPrerequisiteInfo();
	}
	

	@Override
	public OperationResultWrapper<RegisterDTO> autoRegister(
			HAutoRegister autoRegister, List<AutoRegisterLineDTO> lines) {
		autoRegister = LookupHelper.getInstance().getRegisterService().autoRegister(autoRegister, lines);
		OperationResultWrapper<RegisterDTO> result = new OperationResultWrapper<RegisterDTO>();
		RegisterDTO dto = new RegisterDTO();
		dto.setDocNumber(autoRegister.getHRegister().getDnumber() + "");
		dto.setUId(autoRegister.getHRegister().getUId());
		result.setResult(dto);
		result.setStatus(Status.SUCCESS);
		return result;
	}
}
