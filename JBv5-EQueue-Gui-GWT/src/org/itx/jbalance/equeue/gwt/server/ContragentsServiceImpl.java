package org.itx.jbalance.equeue.gwt.server;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.client.services.ContragentsService;
import org.itx.jbalance.equeue.gwt.shared.ContractorsListDTO;
import org.itx.jbalance.equeue.gwt.shared.DeleteDouOperationResult;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper.Status;
import org.itx.jbalance.l0.h.HFreeDivision;
import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.o.StaffDivision;
import org.itx.jbalance.l0.s.SFreeDivision;
import org.itx.jbalance.l0.s.SStaffDivision;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.DouRequestsSearchParams.OrderStatus;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.d.HFreeDivisionsRemote;
import org.itx.jbalance.l1.d.HStaffDivisionsRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.JuridicalsRemote;
import org.itx.jbalance.l1.o.StaffDivisionsRemote;

public class ContragentsServiceImpl extends MyGWTServlet implements ContragentsService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Override
	public void updateDouInList(HStaffDivision staffDivisions) {
		DousRemote ejb = LookupHelper.getInstance().getDous(getSessionId());
			ejb.update(staffDivisions.getDou());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.itx.jbalance.equeue.gwt.client.services.ContragentsService#updateDou
	 * (org.itx.jbalance.l0.h.HStaffDivision, java.util.List, java.util.List)
	 */
	@Override
	public OperationResultWrapper<String> updateDou(HStaffDivision hStaffDivision,
			List<SStaffDivision> staffDivisionsEdited,
			List<SStaffDivision> sStaffDivisionsNew) {
		DousRemote dousEjb = LookupHelper.getInstance().getDous(getSessionId());

		HStaffDivisionsRemote hStaffDivisionsEjb = LookupHelper.getInstance()	.getHStaffDivision(getSessionId());

		// апдэйтим спкцификацию
		updateEditedSpecLines(staffDivisionsEdited, hStaffDivisionsEjb);
		StaffDivisionsRemote staffDivisionsEJB = LookupHelper.getInstance()	.getStaffDivisions(getSessionId());

		
		
		for (SStaffDivision sStaffDivision : sStaffDivisionsNew) {
			StaffDivision staffDivision = staffDivisionsEJB.create(sStaffDivision.getDivision());
			SStaffDivision sStaffDivision1 = new SStaffDivision();
			sStaffDivision1.setDivision(staffDivision);
			hStaffDivisionsEjb.createSpecificationLine(sStaffDivision1, hStaffDivision );
		}

		// апдэйтим Доу
		dousEjb.update(hStaffDivision.getDou());

		// заголовок
		hStaffDivision = hStaffDivisionsEjb.update(hStaffDivision);

		OperationResultWrapper<String> operationResultWrapper = new OperationResultWrapper<String>();
		operationResultWrapper.setStatus(Status.SUCCESS);
		return operationResultWrapper;
	}



	/**
	 * @param staffDivisionsEdited
	 * @param hStaffDivisionsEjb
	 */
	private void updateEditedSpecLines(
			List<SStaffDivision> staffDivisionsEdited,
			HStaffDivisionsRemote hStaffDivisionsEjb) {

		StaffDivisionsRemote staffDivisionsEJB = LookupHelper.getInstance()
				.getStaffDivisions(getSessionId());

		// for(SStaffDivision sStaffDivision : sStaffDivisionsNew) {
		// staffDivisionsEJB.setObject(sStaffDivision.getDivision());
		// staffDivisionsEJB.create();

		if(staffDivisionsEdited!=null)
		for (SStaffDivision sStaffDivision : staffDivisionsEdited) {
			// hStaffDivisionsEjb.setSpecificationLine(division);

			staffDivisionsEJB.update(sStaffDivision.getDivision());

		}
	}

	@Override
	public void updateJur(Juridical p) {
		JuridicalsRemote ejb = LookupHelper.getInstance().getJuridicals(getSessionId());
			p = ejb.update(p);

	}

	@Override
	public DeleteDouOperationResult deleteDou(Long p, Boolean deleteAssociatedRequests) {
		// DousRemote dousRemoteEjb =
		// LookupHelper.getInstance().getDous(getSessionId());
		// dousRemoteEjb.setObject(dousRemoteEjb.getByUId(p));
		// try {
		// dousRemoteEjb.delete();
		// } catch (JBException e) {
		// e.printStackTrace();
		// }
		//
		
		HStaffDivisionsRemote divisionsRemote = LookupHelper.getInstance().getHStaffDivision(getSessionId());
		HStaffDivision hStaffDivision = (HStaffDivision) divisionsRemote.getByUId(p,HStaffDivision.class);
		Long douUid = hStaffDivision.getDou().getUId();
		
		
		DeleteDouOperationResult result = new DeleteDouOperationResult();
		
		if(!deleteAssociatedRequests){
			HDouRequestsRemote hDouRequests = LookupHelper.getInstance().getHDouRequests(getSessionId());
			DouRequestsSearchParams params = new DouRequestsSearchParams();
			params.searchedDous = new ArrayList<Long>();
			params.searchedDous.add(douUid);
			params.orderStatus = OrderStatus.ACTIVE_QUEUE;
			Integer searchCount = hDouRequests.getSearchCount(params);
			if(searchCount>0){
				result.setResult(null);
				result.setAssociadedRequestsAmount(searchCount);
				result.setStatus(Status.VALIDATION_ERROR);
				return result;
			}
		}else{
			
		}
		
		
//		TODO вынести удаление hStaffDivision в код ядра. При удалении контрагента удалять структуру.
		divisionsRemote.delete(hStaffDivision);


		DousRemote dousRemoteEjb = LookupHelper.getInstance().getDous(getSessionId());
		Dou delete = dousRemoteEjb.delete(dousRemoteEjb.getByUId(douUid,Dou.class));
		result.setResult(delete);
		result.setStatus(Status.SUCCESS);
		return result;
	}

	public void deleteJur(Long p) {
		JuridicalsRemote ejb = LookupHelper.getInstance().getJuridicals(getSessionId());
		ejb.delete(ejb.getByUId(p, Juridical.class));
	}

	@Override
	public void createNewDou(Dou p, HStaffDivision doc,
			List<StaffDivision> divisions) { 
		DousRemote ejb = LookupHelper.getInstance().getDous(getSessionId());
		p = ejb.create(p);


		doc.setDou(p);
		HStaffDivisionsRemote hStaffDivisionEJB = LookupHelper.getInstance()
				.getHStaffDivision(getSessionId());
		doc = hStaffDivisionEJB.create(doc);

		StaffDivisionsRemote staffDivisionsEJB = LookupHelper.getInstance().getStaffDivisions(getSessionId());

		for (StaffDivision staffDivision : divisions) {
			staffDivision = staffDivisionsEJB.create(staffDivision);
			SStaffDivision sStaffDivision = new SStaffDivision();
			sStaffDivision.setDivision(staffDivision);
			hStaffDivisionEJB.createSpecificationLine(sStaffDivision,doc);
		}
	}

	@Override
	public void createNewJur(Juridical p) {
		JuridicalsRemote ejb = LookupHelper.getInstance().getJuridicals(getSessionId());
		ejb.create(p);
	}
//
//	@Override
//	public Map <Region,List<Dou>> getAllDouByReg() {
//		DousRemote dousRemote = LookupHelper.getInstance().getDous(getSessionId());
//		List<? extends Dou> allDou = dousRemote.getAll(Dou.class);
//		
//		Map <Region,List<Dou>> res = new HashMap<Region, List<Dou>>();	
//		for (Dou dou : allDou) {
//			Region region = dou.getRegion();
//			if(region == null){
//				continue;
//			}
//			
//			if(!res.containsKey(region)){
//				res.put(region, new ArrayList<Dou>());
//			}
//			
//			res.get(dou.getRegion()).add(dou);
//		}
//
//		return res;
//	}

	@Override
	public ArrayList<Dou> getAllDou() {
		DousRemote ejb = LookupHelper.getInstance().getDous(getSessionId());
		return new ArrayList<Dou>((Collection<? extends Dou>) ejb.getAll(Dou.class));
	}
	
	
	public ArrayList<HStaffDivision> getAllHStaffDivision() {
		HStaffDivisionsRemote ejb = LookupHelper.getInstance().getHStaffDivision(getSessionId());
		return new ArrayList<HStaffDivision>((Collection<? extends HStaffDivision>) ejb.getAll(HStaffDivision.class));
	}

	@Override
	public List<SStaffDivision> getSStaffDivision(HStaffDivision hStaffDivision) {
		HStaffDivisionsRemote ejb = LookupHelper.getInstance()
				.getHStaffDivision(getSessionId());
		List<SStaffDivision> sStaffDivisions = ejb.getSpecificationLines(hStaffDivision);
		return sStaffDivisions;
	}

	@Override
	public void createNewFreeGroups(HStaffDivision doc, List<SFreeDivision> list) {
		
		 StaffDivisionsRemote ejbStaffDivisions =
		 LookupHelper.getInstance().getStaffDivisions(getSessionId());

		HFreeDivisionsRemote ejbHFreeDivisions = LookupHelper.getInstance()
		.getHFreeDivision(getSessionId());
		
		HFreeDivision hFreeDivision = new HFreeDivision();
		hFreeDivision.setDou(doc.getDou());
		

		hFreeDivision = ejbHFreeDivisions.create(hFreeDivision);
		
		for(SFreeDivision sFreeDivision : list) {
//			StaffDivision staffDivision = sFreeDivision.getDivision();
//			ejbStaffDivisions.setObject(staffDivision);
			//ejbStaffDivisions.create(); //если это новые обьекты - клоны StaffDivision-ов из описания ДОУ
			//staffDivision = (StaffDivision) ejbStaffDivisions.getObject();
			sFreeDivision = ejbHFreeDivisions.createSpecificationLine(sFreeDivision, hFreeDivision);

		}

	}

	@Override
	public void updateNewFreeGroups(HStaffDivision doc, List<SFreeDivision> list) {
//		 StaffDivisionsRemote ejbStaffDivisions =
//			 LookupHelper.getInstance().getStaffDivisions(getSessionId());
	}			

	@Override
	public void deleteNewFreeGroups(HStaffDivision doc) {
		HStaffDivisionsRemote ejb = LookupHelper.getInstance().getHStaffDivision(getSessionId());
		ejb.delete(doc);
	}


	@Override
	public OperationResultWrapper<ContractorsListDTO> getContractors() {
		OperationResultWrapper<ContractorsListDTO> operationResultWrapper = new OperationResultWrapper<ContractorsListDTO>();
		ContractorsListDTO result = new ContractorsListDTO();
		
//		TODO replace to special method
		HStaffDivisionsRemote hStaffDivisionsRemote = LookupHelper.getInstance().getHStaffDivision(getSessionId());
		List<HStaffDivision> staffDivisions = (List<HStaffDivision>) hStaffDivisionsRemote.getAll(HStaffDivision.class);
		Map<String, Integer> kgMap = new HashMap<String, Integer>();
		Map<String, Integer> kmMap = new HashMap<String, Integer>();
		for(HStaffDivision hStaffDivision : staffDivisions) {
			String douNumber = hStaffDivision.getDou().getNumber();
			int sumKm=0;
			List<SStaffDivision> sStaffDivisions = hStaffDivisionsRemote.getSpecificationLines(hStaffDivision);
			Integer kg = new Integer(sStaffDivisions.size());
			kgMap.put(douNumber, kg);

			for (SStaffDivision sStaffDivision : sStaffDivisions) {
				sumKm+=sStaffDivision.getDivision().getKm();
			}

			kmMap.put(douNumber, sumKm);
		}
		result.setKg(kgMap);
		result.setKm(kmMap);
		result.setStaffDivisions(staffDivisions);
		
		
		JuridicalsRemote juridicalsRemote = LookupHelper.getInstance().getJuridicals(getSessionId());
		List<Juridical> juridicals = (List<Juridical>) juridicalsRemote.getAll(Juridical.class);
		result.setJuridicals(juridicals);
		
		operationResultWrapper.setResult(result);
		return operationResultWrapper;
	}

}
