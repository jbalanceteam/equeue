package org.itx.jbalance.equeue.gwt.server;

import org.itx.jbalance.equeue.gwt.client.services.LoginService;


@SuppressWarnings("serial")
public class LoginServiceImpl extends MyGWTServlet 
	implements LoginService {
	
	public void openSession(){
		LookupHelper.getInstance().login(getSessionId());
	}

	
	public void closeSession(){
		this.getThreadLocalRequest().getSession().invalidate();
	}

	
}
