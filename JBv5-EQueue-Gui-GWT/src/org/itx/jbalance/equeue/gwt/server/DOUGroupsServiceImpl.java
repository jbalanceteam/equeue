package org.itx.jbalance.equeue.gwt.server;

import java.util.ArrayList;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.itx.jbalance.equeue.gwt.client.services.DOUGroupsService;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l1.o.DOUGroupsRemote;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;


/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class DOUGroupsServiceImpl extends RemoteServiceServlet implements
    DOUGroupsService {
	
	@Override
	public ArrayList<DOUGroups> getAll() {
			return new ArrayList<DOUGroups>(getService().getAll());
	}  
	
	
	
	private DOUGroupsRemote getService() {
		try {
			Context context = new InitialContext();
			return (DOUGroupsRemote) context.lookup("DOUGroups/remote");
		} catch (NamingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
	}

}
