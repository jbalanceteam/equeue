package org.itx.jbalance.equeue.gwt.server;

import java.util.ArrayList;

import org.itx.jbalance.equeue.gwt.client.services.PrivilegesService;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l1.o.PrivilegesRemote;


/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class PrivilegesServiceImpl extends MyGWTServlet implements
    PrivilegesService {
	

	@SuppressWarnings("unchecked")
	@Override
	public synchronized ArrayList<Privilege> getAll() {
			return new ArrayList(LookupHelper.getInstance().getPrivileges(getSessionId()).getAll(Privilege.class));
	}  

	@Override
	public synchronized void createNew(Privilege p) {
		System.out.println("createNew("+p+")");
		PrivilegesRemote service = LookupHelper.getInstance().getPrivileges(getSessionId());
		p = service.create(p);
		
	}
	@Override
	public synchronized void delete(Long id) {
		PrivilegesRemote service = LookupHelper.getInstance().getPrivileges(getSessionId());
		service.delete(id);

	}

	@Override
	public synchronized void update(Privilege p) {
		PrivilegesRemote service = LookupHelper.getInstance().getPrivileges(getSessionId());
		service.update(p);
	}

}
