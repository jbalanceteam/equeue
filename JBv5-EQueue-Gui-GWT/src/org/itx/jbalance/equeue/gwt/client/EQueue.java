package org.itx.jbalance.equeue.gwt.client;

import org.itx.jbalance.equeue.gwt.client.services.LoginService;
import org.itx.jbalance.equeue.gwt.client.services.LoginServiceAsync;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.RootLayoutPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class EQueue implements EntryPoint {

	private final LoginServiceAsync loginService = GWT
			.create(LoginService.class);

	

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
				initApplication();
	}

	private void initApplication() {
		final long startTime=System.currentTimeMillis();
		final Panel root = RootLayoutPanel.get();

		final HTML waitLable = new HTML("Wait a few seconds, please.");
		root.add(waitLable);
		loginService.openSession(new AsyncCallback<Void>() {
			@Override
			public void onSuccess(Void result) {
				long loggedTime=System.currentTimeMillis();
				Log.debug("We are succesfully logged in the JB in "+(loggedTime-startTime)+" ms.");
				root.remove(waitLable);
				root.add(EQueueShell.instance());
				long shellAdded=System.currentTimeMillis();
				Log.debug("We have added shell in "+(shellAdded-loggedTime)+" ms.");
				History.fireCurrentHistoryState();
			}

			@Override
			public void onFailure(Throwable caught) {
				Log.fatal("",caught);
				root.remove(waitLable);
				root.add(new HTML("Unexpected error! Try to reload this page."));
			}
		});
	}

}
