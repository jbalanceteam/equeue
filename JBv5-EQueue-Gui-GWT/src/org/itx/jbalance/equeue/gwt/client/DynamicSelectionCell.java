package org.itx.jbalance.equeue.gwt.client;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.cell.client.AbstractInputCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.SelectElement;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

import java.util.List;



/**
 * A {@link Cell} used to render a drop-down list.
 */
public class DynamicSelectionCell  <T ,D > extends AbstractInputCell<T, D> {

	
	public interface ToString<V>{
		public String toStr(V o);
	}
	
	public interface OptionsHolder<V,K >{
		public List<V>getOptionsForVal(K k);
//		public List<V>getAllOptions(K k);
	}
	
	
	private ToString<T> toString;
	
  interface Template extends SafeHtmlTemplates {
    @Template("<option value=\"{0}\">{0}</option>")
    SafeHtml deselected(String option);

    @Template("<option value=\"{0}\" selected=\"selected\">{0}</option>")
    SafeHtml selected(String option);
  }

  private static Template template;

//  private HashMap<String, Integer> indexForOption = new HashMap<String, Integer>();

  //private final List<String> options;
  final OptionsHolder<T,D> options;
  /**
   * Construct a new {@link SelectionCell} with the specified options.
   *
   * @param options the options in the cell
   */
  public DynamicSelectionCell(OptionsHolder<T,D> options,ToString<T> toString) {
    super("change");
    
    
    if(toString!=null){
    	this.toString=toString;
    }else{
    	this.toString=new ToString<T>() {
    		@Override
			public String toStr(T o) {
				return String.valueOf(o);
			}
		};
    }
    if (template == null) {
      template = GWT.create(Template.class);
    }
    this.options = options;
//    int index = 0;
//    for (T option : options.getOptions()) {
//      indexForOption.put(option.toString(), index++);
//    }
  }

//  public void addOption(String newOp){
//      String option = new String(newOp);
//      options.add(option);
//      refreshIndexes();
//  }

//  public void removeOption(String op){
//      String option = new String(op);
//      options.remove(indexForOption.get(option));
//      refreshIndexes();
//  }

//  private void refreshIndexes(){
//        int index = 0;
//        for (Object option : options.getOptions()) {
//          indexForOption.put(option.toString(), index++);
//        }
//  }

  @Override
  public void onBrowserEvent(Context context, Element parent, T value,
      NativeEvent event, ValueUpdater<T> valueUpdater) {
    super.onBrowserEvent(context, parent, value, event, valueUpdater);
    
    
    String type = event.getType();
    if ("change".equals(type)) {
    	Log.debug("onBrowserEvent change - "+value);
      Object key = context.getKey();
      SelectElement select = parent.getFirstChild().cast();
      T newValue = options.getOptionsForVal((D) key).get(select.getSelectedIndex());
//      setViewData(key, newValue);
      finishEditing(parent, newValue, key, valueUpdater);
      if (valueUpdater != null) {
        valueUpdater.update(newValue);
      }
    	
    }
  }

  @Override
  public void render(Context context, T value, SafeHtmlBuilder sb) {
    // Get the view data.
	D key = (D) context.getKey();
    D viewData = getViewData(key);
    Log.debug("!!!!!render!!!!!!!!!");
    Log.debug("key - "+key);
    Log.debug("viewData - "+viewData);
    Log.debug("value - "+value);
//    if (viewData != null && viewData.equals(value)) {
//      clearViewData(key);
//      viewData = null;
//    }

//    int selectedIndex = getSelectedIndex(viewData == null ? value : viewData);
    sb.appendHtmlConstant("<select style=\"min-width: 70px\" tabindex=\"-1\">");
    
    
    
    
    int index = 0;
    for (T option : options.getOptionsForVal(key)) {
      if (option==value || (option!=null && option.equals(value))) {
        sb.append(template.selected(toString.toStr(option)));
      } else {
        sb.append(template.deselected(toString.toStr(option)));
      }
      index++;
    }
    sb.appendHtmlConstant("</select>");
  }

//  private int getSelectedIndex(T value) {
//	  
//	  
//	  Log.debug("getSelectedIndex - "+value);
//	  
//    Integer index = options.get(value);
//    if (index == null) {
//      return -1;
//    }
//    return index.intValue();
//	  
//	  return 1;
//  }
}
