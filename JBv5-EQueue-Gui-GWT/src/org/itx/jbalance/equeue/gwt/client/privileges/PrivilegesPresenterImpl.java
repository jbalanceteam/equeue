package org.itx.jbalance.equeue.gwt.client.privileges;

import java.util.ArrayList;

import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.privileges.PrivilegesPresenter.Display;
import org.itx.jbalance.equeue.gwt.client.services.PrivilegesService;
import org.itx.jbalance.equeue.gwt.client.services.PrivilegesServiceAsync;
import org.itx.jbalance.l0.o.Privilege;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class PrivilegesPresenterImpl extends MyPresenter<Display>
		implements PrivilegesPresenter {

	
	private final PrivilegesServiceAsync privilegesService = GWT.create(PrivilegesService.class);
	
	ArrayList<Privilege> rows;
	
	public PrivilegesPresenterImpl(PrivilegesWidget display) {
		super(display);
	}

	@Override
	public void create() {
		
		final String name = getDisplay().getPrevelegisName().getText().trim();
		
		if(name.isEmpty()){
			getDisplay().showWarningNotification("Заполните поле \"Наименование\"");
		return;
		}
		
		
		for(Privilege p:rows){
			if(p.getName().equalsIgnoreCase(name)){
				getDisplay().showWarningNotification("Такое ужо есьт");
				return;
			}
		}
		Privilege p = new Privilege();
		p.setName(name);
		p.setDescription(getDisplay().getDescription().getText());
		privilegesService.createNew(p, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				loadData();
				getDisplay().getPrevelegisName().setText("");
				getDisplay().getDescription().setText("");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}

	@Override
	public void delete(Long id) {
		privilegesService.delete(id, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				loadData();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}

	@Override
	public void initialize() {
		loadData();
		initListeners();
	}

	private void initListeners() {
		getDisplay().getSaveClickHandlers().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				create();
				
			}
		});
		
	}

	public void loadData(){
		privilegesService .getAll(new AsyncCallback<ArrayList<Privilege>>() {
			
			@Override
			public void onSuccess(ArrayList<Privilege> result) {
				rows=result;
				getDisplay().showPrivileges(rows);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}

	@Override
	public void update(Privilege privilege) {
		privilegesService.update(privilege, new AsyncCallback<Void>() {
			@Override
			public void onSuccess(Void result) {}
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}
}









//public void initialize() {
//ProvidesKey<Privilege> keyProvider = new ProvidesKey<Privilege>() {
//	@Override
//	public Object getKey(Privilege item) {
//		return item==null?null:item.getId();
//	}
//};
//
//// // Add a selection model so we can select cells.
////final SingleSelectionModel<Privilege> selectionModel = new SingleSelectionModel<Privilege>(keyProvider);
////getDisplay().getTable().setSelectionModel(selectionModel,
////    DefaultSelectionEventManager.<Privilege>createDefaultManager());
//
////selectionModel.addSelectionChangeHandler(new Handler() {
////	
////	@Override
////	public void onSelectionChange(SelectionChangeEvent event) {
////		selectionModel.getSelectedObject();
////		
////	}
////});
//
//initTableColumns(/*selectionModel*/);
//initTable();
//initListeners();
//}
//
//
//private final PrivilegesServiceAsync privilegesService = GWT.create(PrivilegesService.class);
//
//ArrayList<Privilege> rows;
//
//
//@Override
//public void create() {
//final String name = getDisplay().getNameField().getText().trim();
//
//if(name.isEmpty()){
//Window.al1ert("Заполните поле \"Наименование\"");
//return;
//}
//
//
//for(Privilege p:rows){
//if(p.getName().equalsIgnoreCase(name)){
//	Window.al1ert("Такое ужо есьт");
//	return;
//}
//}
//
//Privilege newRow = new Privilege();
//newRow.setName(name);
//newRow.setDescription(getDisplay().getDescriptionField().getText());
//privilegesService.createNew(newRow, new AsyncCallback<Void>() {
//
//@Override
//public void onSuccess(Void result) {
//	initTable();
//	getDisplay().getNameField().setText("");
//	getDisplay().getDescriptionField().setText("");
//}
//
//@Override
//public void onFailure(Throwable caught) {
//	Window.al1ert(caught.getMessage());
//	
//}
//});
//
//}
//
//
//
//@Override
//public void delete(Long id) {
//System.out.println("deleteRow("+id+")");
//
//if(Window.confirm("Одумайтесь!!!")){
//
//privilegesService.delete(id,  new AsyncCallback<Void>() {
//@Override
//public void onSuccess(Void result) {
//	initTable();
//}
//
//@Override
//public void onFailure(Throwable caught) {
//	Window.ale1rt(caught.getMessage());
//	
//}
//});
//
//}
//
//}
//
//
//public void initListeners(){
//
//getDisplay().getCreateButton().addClickHandler(new ClickHandler() {
//
//@Override
//public void onClick(ClickEvent event) {
//	create();
//	
//}
//});
//
//getDisplay().getNameField().addKeyDownHandler(new KeyDownHandler() {
//
//@Override
//public void onKeyDown(KeyDownEvent event) {
//	if(event.getNativeKeyCode()==KeyCodes.KEY_ENTER)
//		create();
//}
//});
//}
//
//
//private void initTable() {
//privilegesService.getAll(new AsyncCallback<ArrayList<Privilege>>() {
//@Override
//public void onSuccess(ArrayList<Privilege> result) {
//	rows=result;
//	getDisplay().getTable().setRowData(result);
//}
//
//@Override
//public void onFailure(Throwable caught) {
//	Window.ale1rt("ERR: " +caught.getMessage());					
//}
//});
//
//}
//
//
//private void initTableColumns(/*final SelectionModel<Privilege> selectionModel*/) {
//Column <Privilege,String>idColumn =new Column<Privilege,String>(
//	new TextCell()){
//		@Override
//		public String getValue(Privilege object) {
//			return object.getId()+"";
//		}}; 
//		
//getDisplay().getTable().addColumn(idColumn, SafeHtmlUtils.fromSafeConstant("ID"));
//
//
///* name */
//Column <Privilege,String>nameColumn =new Column<Privilege,String>(
//	new EditTextCell()){
//		@Override
//		public String getValue(Privilege object) {
//			return object.getName()+"";
//		}}; 
//		
//getDisplay().getTable().addColumn(nameColumn, SafeHtmlUtils.fromSafeConstant("Наименование"));
//nameColumn.setFieldUpdater(new FieldUpdater<Privilege, String>() {
//
//@Override
//public void update(int index, Privilege p, String value) {
//	p.setName(value);
//	privilegesService.update(p, new AsyncCallback<Void>() {
//		
//		@Override
//		public void onSuccess(Void result) {}
//		
//		@Override
//		public void onFailure(Throwable caught) {
//			Window.a1lert(caught.getMessage());
//		}
//	});
//	
//}
//});
//
//
//
//
///* description */
//Column <Privilege,String>descriptionColumn =new Column<Privilege,String>(
//	new EditTextCell()){
//		@Override
//		public String getValue(Privilege object) {
//			return object.getDescription()+"";
//		}}; 
//
//getDisplay().getTable().addColumn(descriptionColumn, SafeHtmlUtils.fromSafeConstant("Описание"));
//descriptionColumn.setFieldUpdater(new FieldUpdater<Privilege, String>() {
//
//@Override
//public void update(int index, Privilege p, String value) {
//	p.setDescription(value);
//	privilegesService.update(p, new AsyncCallback<Void>() {
//		
//		@Override
//		public void onSuccess(Void result) {}
//		
//		@Override
//		public void onFailure(Throwable caught) {
//			Window.ale1rt(caught.getMessage());
//		}
//	});
//	
//}
//});
//
//
//
//
//
//
//ActionCell<Privilege> delCell = new ActionCell<Privilege>(SafeHtmlUtils.fromSafeConstant("X"),new Delegate<Privilege>() {
//
//@Override
//public void execute(Privilege p) {
//	delete(p.getId());
//}
//});
//
//
//Column <Privilege,Privilege>delColumn =new Column<Privilege,Privilege>(
//	delCell){
//		@Override
//		public Privilege getValue(Privilege object) {
//			return object;
//		}}; 
//		
//
//
//List<HasCell<Privilege, ?>>  hasCells = new ArrayList<HasCell<Privilege, ?>>();
//
//hasCells.add(delColumn);
//
//CompositeCell<Privilege> cell=new CompositeCell<Privilege>(
//	hasCells );
//
//
//Column <Privilege,Privilege>actionColumn =new Column<Privilege,Privilege>(
//	cell){
//
//		@Override
//		public Privilege getValue(Privilege object) {
//			// TODO Auto-generated method stub
//			return object;
//		}}; 
//getDisplay().getTable().addColumn(actionColumn, SafeHtmlUtils.fromSafeConstant("Действие"));
//
//
//}