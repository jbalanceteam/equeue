package org.itx.jbalance.equeue.gwt.client.register;


import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.itx.jbalance.equeue.gwt.client.Callback;
import org.itx.jbalance.equeue.gwt.client.MyDisplay;
import org.itx.jbalance.equeue.gwt.shared.RegisterLine;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l0.o.Dou;


public interface RegisterEditPresenter extends Serializable{

	public interface Display extends MyDisplay{

	}
	
	void save(Callback onSuccess);
	void setObject(Long objectUid);
	void initialize();
	//public void setRegNums(List<Long> ids);
	public List<RegisterLine> getTableData();
	public void setRegNums(Set<Long> ids);
//	public Map<HDouRequest,List<SDouRequest>> getMap();
	public void clearRequests();
	List<Dou> getAllDou();
	/**
	 * #238: Панель протокола
	 * @return
	 */
	ChildCount getChildCount();
	
	HRegister getRegister();
//	String getDous(HDouRequest douRequest);
	void goBack();
	void goForward();
	List<DOUGroups> getAgeGroups();
	Map<Integer, DOUGroups> getAgeGroupsMap();
}
