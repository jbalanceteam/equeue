package org.itx.jbalance.equeue.gwt.client.request;
/**
 * Этот класс описывает один элемент комбобокса Льгота
 * Может описывать конкретную льготу, либо любую, либо отсутствие
 * @author apv
 *
 */
public class PrivilegeSearchItem {
	public enum Type{
		/**
		 * Нет фильтрации по льготе
		 */
		ANY, 
		/**
		 * ПОказать без льготы
		 */
		NO_PRIVILEGE, 
		/**
		 * ПОказать все со льготой
		 */
		YES_PRIVILEGE, 
		/**
		 * Отфильтровать по конкретной льготе
		 */
		EXACT_PRIVELEGE
	}
	
	public static final PrivilegeSearchItem ANY = new PrivilegeSearchItem(Type.ANY);
	public static final PrivilegeSearchItem NO_PRIVILEGE = new PrivilegeSearchItem(Type.NO_PRIVILEGE);
	public static final PrivilegeSearchItem YES_PRIVILEGE = new PrivilegeSearchItem(Type.YES_PRIVILEGE);
	
	public PrivilegeSearchItem() {
		super();
	}
	private Type type;
	private Long exactPrivelageId;
	private String exactPrivelageName;
	
	public PrivilegeSearchItem(Type type) {
		super();
		this.type = type;
	}
	
	public PrivilegeSearchItem(Type type, Long exactPrivelageId,
			String exactPrivelageName) {
		super();
		this.type = type;
		this.exactPrivelageId = exactPrivelageId;
		this.exactPrivelageName = exactPrivelageName;
	}
	public Type getType() {
		return type;
	}
	public void setType(Type type) {
		this.type = type;
	}
	public Long getExactPrivelageId() {
		return exactPrivelageId;
	}
	public void setExactPrivelageId(Long exactPrivelageId) {
		this.exactPrivelageId = exactPrivelageId;
	}
	public String getExactPrivelageName() {
		return exactPrivelageName;
	}
	public void setExactPrivelageName(String exactPrivelageName) {
		this.exactPrivelageName = exactPrivelageName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrivilegeSearchItem other = (PrivilegeSearchItem) obj;
		if (exactPrivelageId != null) {
			return exactPrivelageId.equals(other.exactPrivelageId);
		} 
		return type.equals(other.getType());
	}
	
	
	
	
}
