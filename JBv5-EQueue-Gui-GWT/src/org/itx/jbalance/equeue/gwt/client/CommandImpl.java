package org.itx.jbalance.equeue.gwt.client;

import java.io.Serializable;
import java.util.Map;

import com.google.gwt.user.client.Command;

public abstract class CommandImpl implements Command{

	
	public abstract void execute(Map<String,Serializable> params);
	
	@Override
	public void execute() {
		execute(null);
	}

}
