package org.itx.jbalance.equeue.gwt.client;

import com.google.gwt.resources.client.ClientBundle;

/**
 * The images and styles used throughout the Showcase.
 */
public interface EQueueResources extends ClientBundle {


  /**
   * The styles used in LTR mode.
   */
//  @NotStrict
//  @Source("Showcase.css")
//  CssResource css();

 

//  ImageResource loading();
//
//  /**
//   * Indicates the locale selection box.
//   */
//  ImageResource locale();
}