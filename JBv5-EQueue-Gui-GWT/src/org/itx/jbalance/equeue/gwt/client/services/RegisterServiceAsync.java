package org.itx.jbalance.equeue.gwt.client.services;

import java.util.List;

import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.RegisterEditData;
import org.itx.jbalance.equeue.gwt.shared.RegisterLine;
import org.itx.jbalance.equeue.gwt.shared.SaveRegisterResult;
import org.itx.jbalance.equeue.gwt.shared.SetRegNumsRequest;
import org.itx.jbalance.equeue.gwt.shared.SetRegNumsResult;
import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Foundation;
import org.itx.jbalance.l1.api.RegisterSearchParams;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterLineDTO;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterPrerequisiteInfo;
import org.itx.jbalance.l2_api.dto.equeue.RegisterDTO;

import com.google.gwt.user.client.rpc.AsyncCallback;


public interface RegisterServiceAsync {
	void search(RegisterSearchParams params,AsyncCallback<SearchResultWrapper<RegisterDTO>> callback);

	void create(HRegister register, List<RegisterLine> tableData,
			AsyncCallback<SaveRegisterResult> callback);

	void getSpec(Long uid, AsyncCallback<List<SRegister>> callback);
	
	void getEditData(Long uid, AsyncCallback<RegisterEditData> callback);

	void delete(Long id, AsyncCallback<Void> callback);

	void getFoundations(AsyncCallback<List<Foundation>> callback);

	void update(HRegister register, List<RegisterLine> spec,
			AsyncCallback<SaveRegisterResult> callback);
	
	void getSetRegNumsData(SetRegNumsRequest request, AsyncCallback <SetRegNumsResult> callback);

	void changeStatus(HRegister register, List<RegisterLine> spec , RegisterStatus newStatus,
			AsyncCallback<SaveRegisterResult> callback);

	void searchPermits(GetPermitsRequest searchParams,		AsyncCallback<PermitsSearchResultsWrapper> asyncCallback);

	/**
	 * #118 Автоматическое комплектование
	 * Получение данныз для отображения формы
	 * @param callback
	 */
	void getAutoRegisterPrerequisiteInfo(
			AsyncCallback<AutoRegisterPrerequisiteInfo> callback);

	void autoRegister(HAutoRegister autoRegister,List<AutoRegisterLineDTO> lines, AsyncCallback<OperationResultWrapper<RegisterDTO>> callback);

}
