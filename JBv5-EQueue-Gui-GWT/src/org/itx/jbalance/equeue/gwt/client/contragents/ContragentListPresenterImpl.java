package org.itx.jbalance.equeue.gwt.client.contragents;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.client.EQueueNavigation;
import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.contragents.ContragentListPresenter.Display;
import org.itx.jbalance.equeue.gwt.client.services.ContragentsService;
import org.itx.jbalance.equeue.gwt.client.services.ContragentsServiceAsync;
import org.itx.jbalance.equeue.gwt.shared.ContractorsListDTO;
import org.itx.jbalance.equeue.gwt.shared.DeleteDouOperationResult;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.DouRequestsSearchParams.OrderStatus;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;

public class ContragentListPresenterImpl extends MyPresenter<Display>  
        implements ContragentListPresenter {

	
	private final ContragentsServiceAsync contragentsService = GWT.create(ContragentsService.class);
	
	List<HStaffDivision> rows;
	List<Juridical> contrRows;

	public ContragentListPresenterImpl(ContragentListWidget display) {
		super(display);
	}
	
	public void update(HStaffDivision juridical){
		updateDou(juridical);
	}
	
	public void updateJur(Juridical juridical){
         contragentsService.updateJur(juridical, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				loadData();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}
	
	private void updateDou(HStaffDivision juridical){
       contragentsService.updateDouInList(juridical, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				loadData();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}
	
	
	
	@Override
	public void deleteDou(final Long id, Boolean releteAssociatedRequests) {
		contragentsService.deleteDou(id, releteAssociatedRequests , new AsyncCallback<DeleteDouOperationResult>() {
			
			@Override
			public void onSuccess(DeleteDouOperationResult result) {
				switch (result.getStatus()) {
				case VALIDATION_ERROR:
					if(result.getAssociadedRequestsAmount() > 0 ){
						
						ClickHandler removeDouFromRequests = new ClickHandler() {
					          public void onClick(ClickEvent event) {
					        	  deleteDou(id, true);
					          }
					        };
					        
				        ClickHandler cancel = new ClickHandler() {
					          public void onClick(ClickEvent event) {
					        	  
					          }
					        };
						
					    /* Обработчик для кнопки "Просмотреть совпадения" */
				        ClickHandler showMatches = new ClickHandler() {
					          public void onClick(ClickEvent event) {
					        	  DouRequestsSearchParams searchParams=new DouRequestsSearchParams();
					        	  searchParams.searchedDous = new ArrayList<Long>();
					        	  
					        	  for(HStaffDivision hsd: rows){
					        		  if(hsd.getUId().equals(id)){
					        			  searchParams.searchedDous.add(hsd.getDou().getUId());
					        			  break;
					        		  }
					        	  }					        	  
					        	  
					        	  searchParams.orderStatus = OrderStatus.ACTIVE_QUEUE;
					        	  Map<String,Serializable> hashMap = new HashMap<String,Serializable>();
					        	  hashMap.put("searchParams", searchParams);
								  EQueueNavigation.instance().goToDouRequestsListPage(hashMap);
						  		  
					          }
					        };
						
					    /* Показываем модальную панель с предупреждением и кнопками*/
						final DialogBox dialogBox = getDisplay().createDialogBox(result.getAssociadedRequestsAmount().intValue(), 
								showMatches, removeDouFromRequests, cancel	);
					    dialogBox.setGlassEnabled(true);
					    dialogBox.setAnimationEnabled(true);

					    dialogBox.center();
					    dialogBox.show();
					    break;
						
					}
				case SERVER_ERROR:
					getDisplay().showWarningNotification(result.getComments());
					break;

				case SUCCESS:
					getDisplay().showInfoNotification("Успешно удалено!");
					loadData();
					break;
			}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}
	
	@Override
	public void deleteJur(Long id) {
		contragentsService.deleteJur(id, new AsyncCallback<Void>() {

			@Override
			public void onSuccess(Void result) {
				getDisplay().showInfoNotification("Успешно удалено!");
				loadData();
			}

			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}
	
	
	@Override
	public void initialize() {
		loadData();
		initListeners();
	}

	private void initListeners() {
//		getDisplay().getDelClickHandlers().addClickHandler(new ClickHandler() {
//
//			@Override
//			public void onClick(ClickEvent event) {
//				delete();
//
//			}
//		});
	}

	
	private void setKmsToRows(Map<String, Integer> result) {
		for (HStaffDivision hStaffDivision : rows) {
			String idDou = hStaffDivision.getDou().getNumber();
			Integer km = result.get(idDou);
			hStaffDivision.setKm(km);
		}
	}

	private void setKgsToRows(Map<String, Integer> result) {
		for (HStaffDivision hStaffDivision : rows) {
			String idDou = hStaffDivision.getDou().getNumber();
			Integer kg = result.get(idDou);
			hStaffDivision.setKg(kg);
		}
	}
	
	public void loadData(){
		
		contragentsService.getContractors(new AsyncCallback<OperationResultWrapper<ContractorsListDTO>>() {
			
			@Override
			public void onSuccess(OperationResultWrapper<ContractorsListDTO> result) {
				rows = result.getResult().getStaffDivisions();
				setKmsToRows(result.getResult().getKm());
				setKgsToRows(result.getResult().getKg());
				getDisplay().showDous(rows);
				
				contrRows=result.getResult().getJuridicals();
				ArrayList<Juridical> juridicals = new ArrayList<Juridical>();
				for(Juridical juridical : contrRows) {
					if (!(juridical instanceof Dou))
					juridicals.add(juridical);
				}
				getDisplay().showContragents(juridicals);
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		

	}

	
}