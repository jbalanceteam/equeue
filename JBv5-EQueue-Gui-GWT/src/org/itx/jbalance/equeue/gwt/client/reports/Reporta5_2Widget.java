package org.itx.jbalance.equeue.gwt.client.reports;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.RepeatableProgressBar;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.equeue.gwt.shared.Report5_2DTO;
import org.itx.jbalance.equeue.gwt.shared.ReportAgeWithCount;
import org.itx.jbalance.l0.o.Privilege;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Reporta5_2Widget extends ContentWidget implements Reporta5_1Presenter.Display {

	interface Binder extends UiBinder<Widget, Reporta5_2Widget> {

	}

	@UiField(provided=true)
	CellTable<ReportAgeWithCount> table=new CellTable<ReportAgeWithCount>(0,MyCellTableResources.INSTANCE);
	
	
	
	@UiField
	SimplePanel progressPanel;
	
	RepeatableProgressBar progressBar;
	
	@Override
	public void initialize() {
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		progressBar = new RepeatableProgressBar();
		progressPanel.add(progressBar);
	}

	
	
	private void initTableColumns(List<Privilege> list){
		/* Age */
		Column<ReportAgeWithCount, String> ageColumn = new Column<ReportAgeWithCount, String>(
				new TextCell()) {
			@Override
			public String getValue(ReportAgeWithCount o) {
				
				 Integer from = o.getFrom();
				Integer to = o.getTo();
				return from/12+"."+ from%12 +" - "+to/12+"."+ to%12+"";
			}
		};

		table.addColumn(ageColumn, SafeHtmlUtils.fromSafeConstant("Возраст"));
//		table.setColumnWidth(ageColumn,"10px");
		

		for (final Privilege privilege : list) {
			Column<ReportAgeWithCount, String> countColumn = new Column<ReportAgeWithCount, String>(
					new TextCell()) {
				@Override
				public String getValue(ReportAgeWithCount o) {
					
					 return o.getCount().get(privilege.getUId())+"";
				}
			};

			table.addColumn(countColumn, SafeHtmlUtils.fromSafeConstant(privilege.getName()));


			
			
		}
		
	}
	
	public void setReportData(Report5_2DTO data){
		initTableColumns(data.getPrivileges());
		table.setRowData(data.getAges());
		progressBar.stop();
		progressBar = null;
		progressPanel.clear();
	}
	
}
