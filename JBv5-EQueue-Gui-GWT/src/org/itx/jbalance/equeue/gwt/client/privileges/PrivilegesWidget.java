package org.itx.jbalance.equeue.gwt.client.privileges;

import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.privileges.PrivilegesPresenter.Display;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.l0.o.Privilege;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ActionCell.Delegate;
import com.google.gwt.cell.client.CompositeCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.HasCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class PrivilegesWidget extends ContentWidget implements Display {

	interface Binder extends UiBinder<Widget, PrivilegesWidget> {

	}

	@UiField(provided = true)
	CellTable<Privilege> cellTable=new CellTable<Privilege>(0,MyCellTableResources.INSTANCE);

	@UiField
	TextBox nameTextBox;

	@UiField
	TextBox descriptionField;

	@UiField
	Button addNewButton;

	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public PrivilegesWidget() {
	}

	@Override
	public void initialize() {
		addNewButton = new Button("Добавить");
		// Create the UiBinder.
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		initTableColumns();

	}

	private void initTableColumns() {
		Column<Privilege, String> idColumn = new Column<Privilege, String>(
				new TextCell()) {
			@Override
			public String getValue(Privilege object) {
				return object.getUId() + "";
			}
		};

		cellTable.addColumn(idColumn, SafeHtmlUtils.fromSafeConstant("ID"));

		/* name */
		Column<Privilege, String> nameColumn = new Column<Privilege, String>(
				new  EditTextCell()) {
			@Override
			public String getValue(Privilege object) {
				return object.getName() + "";
			}
		};

		cellTable.addColumn(nameColumn,
				SafeHtmlUtils.fromSafeConstant("Наименование"));
		nameColumn.setFieldUpdater(new FieldUpdater<Privilege, String>() {

			@Override
			public void update(int index, Privilege object, String value) {
				object.setName(value);
				getPresenter().update(object);

			}

		});

		/* description */
		Column<Privilege, String> descriptionColumn = new Column<Privilege, String>(
				new EditTextCell()) {
			@Override
			public String getValue(Privilege object) {
				return object.getDescription() + "";
			}
		};

		cellTable.addColumn(descriptionColumn,
				SafeHtmlUtils.fromSafeConstant("Описание"));
		descriptionColumn
				.setFieldUpdater(new FieldUpdater<Privilege, String>() {

					@Override
					public void update(int index, Privilege object, String value) {
						object.setDescription(value);
						getPresenter().update(object);
					}
				});
	
	
		
		/* actions */
		ActionCell<Privilege> delCell = new ActionCell<Privilege>(SafeHtmlUtils.fromSafeConstant("X"),new Delegate<Privilege>() {
		
			@Override
			public void execute(Privilege p) {
				if(Window.confirm("Удалить безвозвратно?"))
						getPresenter().delete(p.getUId());
			}
		});
		
		
		Column <Privilege,Privilege>delColumn =new Column<Privilege,Privilege>(
			delCell){
				@Override
				public Privilege getValue(Privilege object) {
					return object;
				}}; 
				
		
		
		List<HasCell<Privilege, ?>>  hasCells = new ArrayList<HasCell<Privilege, ?>>();
		
		hasCells.add(delColumn);
		
		CompositeCell<Privilege> cell=new CompositeCell<Privilege>(
			hasCells );
		
		
		Column <Privilege,Privilege>actionColumn =new Column<Privilege,Privilege>(
			cell){
		
				@Override
				public Privilege getValue(Privilege object) {
					return object;
				}}; 
		cellTable.addColumn(actionColumn, SafeHtmlUtils.fromSafeConstant("Действие"));
	
	}
	
	
	@Override
	public HasText getPrevelegisName() {
		return nameTextBox;
	}

	@Override
	public HasText getDescription() {
		return descriptionField;
	}

	@Override
	public void showPrivileges(List<Privilege> list) {
		cellTable.setRowData(list);
	}

	@Override
	public HasClickHandlers getSaveClickHandlers() {
		return addNewButton;
	}

	
	
	
	
	
	PrivilegesPresenter presenter;

	public PrivilegesPresenter getPresenter() {
		return presenter;
	}


	public void setPresenter(PrivilegesPresenter presenter) {
		this.presenter=presenter;
	}
	
	

}
