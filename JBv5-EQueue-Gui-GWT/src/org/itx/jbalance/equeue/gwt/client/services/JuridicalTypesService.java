package org.itx.jbalance.equeue.gwt.client.services;

import java.util.ArrayList;

import org.itx.jbalance.l0.o.JuridicalType;


import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("jurTypes")
public interface JuridicalTypesService extends RemoteService {
	public ArrayList<JuridicalType> getAll();
}
