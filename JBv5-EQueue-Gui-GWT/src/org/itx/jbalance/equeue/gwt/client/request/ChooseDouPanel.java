package org.itx.jbalance.equeue.gwt.client.request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Region;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DockLayoutPanel;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
/**
 * Компонент, который позволяет отмечать ДОУ галочками
 * Группирует ДОУ по регионам
 * Используется в фильтрах
 * @author apv
 *
 */
public class ChooseDouPanel extends SimplePanel{

	private List<Region> regions; 
	private List<CB_DOU_REG> cbDouList; 
	private SelectDouListener selectListener;
	private ListBox regionLB = new ListBox();
	Grid grid = new  Grid();
	
	public ChooseDouPanel(final Map<Region,List<Dou>>dous, final SelectDouListener selectListener) {
		super();
		this.selectListener = selectListener;
		
		cbDouList = new ArrayList<ChooseDouPanel.CB_DOU_REG>();
		regions = new ArrayList<Region>(dous.keySet().size());
		for(Entry<Region, List<Dou>>e: dous.entrySet()){
			regions.add(e.getKey());
			for (Dou dou : e.getValue()) {
				
				CheckBox cb = new CheckBox();
				cb.setTitle(dou.getName());
				cb.getElement().getStyle().setMarginLeft(15, Unit.PX);
				cb.getElement().getStyle().setFontSize(11, Unit.PX);
				cb.setText(dou.getNumber()+"");
				cb.setValue(Boolean.FALSE);
				
				cb.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
					@Override
					public void onValueChange(ValueChangeEvent<Boolean> event) {
						if(selectListener!=null)
							selectListener.selectionChanged(getSelectedDou());
					}
				});
				
				cbDouList.add(new CB_DOU_REG(dou, e.getKey(), cb));
				
			}
			
		}
		
		Collections.sort(regions);
		Collections.sort(cbDouList);
		
		
		initComponents();
		initListeners();
		updateGrid(null);
	}

	
	public Region getSelectedRegion(){
		String value = regionLB.getValue(regionLB.getSelectedIndex());
		Region selectedRegion = null;
		if(value .equals("all")){
			selectedRegion = null;
		}else{
			
			for(Region r: regions){
				if(value.equals(r.getUId().toString())){
					selectedRegion = r;
				}
			}
			if(selectedRegion == null){
				throw new RuntimeException("How did this happen?");
			}
		}
		return selectedRegion;
	}

	public void selectAllByRegion(Region region, Boolean value){
		if(region == null){
			for (CB_DOU_REG item : cbDouList) {
				item.checkBox.setValue(value);
			}
		}else{
			for (CB_DOU_REG item : getItemsByRegion(region)) {
				item.checkBox.setValue(value);
			}
		}
	}
	
	private void initListeners() {
		regionLB.addChangeHandler(new ChangeHandler() {
			@Override
			public void onChange(ChangeEvent event) {
					updateGrid(getSelectedRegion());
					if(selectListener!=null)
						selectListener.selectionChanged(getSelectedDou());
			}
		});
	}
	
	
	void initComponents(){
		regionLB.addItem("Все", "all");
		
		for (Region region : regions) {
			regionLB.addItem(region.getName(), region.getUId().toString());
		}
		
		
		grid.setVisible(true);

		
		DockLayoutPanel dockLayoutPanel = new DockLayoutPanel(Unit.EM);
		dockLayoutPanel.addNorth(regionLB, 2);
		
		
		FlowPanel buttonsPanel = new FlowPanel();
		dockLayoutPanel.addSouth(buttonsPanel, 2);
		Button selectAll = new Button("Выбрать все");
		selectAll.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				selectAllByRegion(getSelectedRegion(), true);
				if(selectListener!=null)
					selectListener.selectionChanged(getSelectedDou());
			}
		});
		buttonsPanel.add(selectAll);
		Button cleanSelection = new Button("Снять выделение");
		cleanSelection.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				selectAllByRegion(getSelectedRegion(), false);
				if(selectListener!=null)
					selectListener.selectionChanged(getSelectedDou());
			}
		});
		buttonsPanel.add(cleanSelection);
		
		
		dockLayoutPanel.add(grid);
		int height = 16 * (cbDouList.size()/5  +  (cbDouList.size()%5!=0?1:0) )+90;
		dockLayoutPanel.setSize("280px", height+  "px");
		
		
		setWidth("220px");
		setHeight((height + 10) +  "px");
		this.add(dockLayoutPanel);
	}
	
	public List<Dou> getAllDous(){
		 List<Dou> res = new ArrayList<Dou>();
		 for (CB_DOU_REG  item : cbDouList ) {
			 res.add(item.dou);
		 }
		 return res;
	}
	
	
	private void updateGrid(Region reg){
		Log.info(reg+"");
		
		grid.clear();
		
		List<CB_DOU_REG> itemsByRegion ; 
		if(reg == null){
			 itemsByRegion = cbDouList;
		}else{
			 itemsByRegion = getItemsByRegion(reg);
		}
		Log.info("itemsByRegion: "+itemsByRegion);
		
		grid.resize(itemsByRegion.size() / 5 +1, 6);
		int i=0;
		for (CB_DOU_REG item : itemsByRegion) {
		//	Log.info(""+item + " "+i/5 + " "+i%5);
			grid.setWidget(i/5, i%5, item.checkBox);
			i++;
		}
	}
	
	private List<CB_DOU_REG>getItemsByRegion(Region reg){
		List<CB_DOU_REG> res=new ArrayList<CB_DOU_REG>();
		for(CB_DOU_REG item: cbDouList){
			if(item.region.equals(reg)){
				res.add(item);
			}
		}
		return res;
	}
	
	
	public boolean isAllSelected(){
		for(CB_DOU_REG item: cbDouList){
			if(!item.checkBox.getValue()){
				return false;
			}
		}
		return true;
	}
	
	public boolean isAllUnselected(){
		for(CB_DOU_REG item: cbDouList){
			if(item.checkBox.getValue()){
				return false;
			}
		}
		return true;
	}
	
	public List<Dou> getSelectedDou(){
		List<Dou> res=new ArrayList<Dou>();
		for(CB_DOU_REG item: cbDouList){
			if(item.checkBox.getValue()){
				res.add(item.dou);
			}
		}
		return res;
	}
	
	
	public interface SelectDouListener{
		void selectionChanged(List<Dou>selected);
	}
	
	
	public void clearSelection(){
		for(CB_DOU_REG item: cbDouList){
			item.checkBox.setValue(Boolean.FALSE);
		}
	}
	
	public void selectOne(Dou dou, Boolean value){
		for(CB_DOU_REG item: cbDouList){
			if(item.dou.equals(dou)){
				item.checkBox.setValue(value);
				break;
			}
		}
	}
	
	public void selectByUids(List<Long> uids, Boolean value){
		for (Long long1 : uids) {
			for(CB_DOU_REG item: cbDouList){
				Log.debug("++>>"+item.dou.getUId());
				if(item.dou.getUId().equals(long1)){
					item.checkBox.setValue(value);
					break;
				}
			}
		}
		if(selectListener!=null)
			selectListener.selectionChanged(getSelectedDou());
	}
	
	
	class CB_DOU_REG implements Comparable<CB_DOU_REG>{
		CB_DOU_REG(Dou dou, Region region, CheckBox checkBox){
			this.checkBox = checkBox;
			this.dou = dou;
			this.region = region;
		}
		
		CheckBox checkBox;
		Dou dou;
		Region region;
		@Override
		public String toString() {
			return "CB_DOU_REG [checkBox=" + checkBox + ", dou=" + dou
					+ ", region=" + region + "]";
		}
		@Override
		public int compareTo(CB_DOU_REG o) {
			if(this.dou == null){
				if(o.dou == null){
					return 0;
				}
				return -1;
			} else if (o.dou == null){
				return 1;
			} else {
				return this.dou.compareTo(o.dou);
			}
		}
		
		
	}
	

}
