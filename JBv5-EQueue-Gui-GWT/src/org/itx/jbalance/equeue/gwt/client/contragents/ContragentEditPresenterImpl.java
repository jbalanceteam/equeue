package org.itx.jbalance.equeue.gwt.client.contragents;

import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.services.ContragentsService;
import org.itx.jbalance.equeue.gwt.client.services.ContragentsServiceAsync;
import org.itx.jbalance.equeue.gwt.client.services.DOUGroupsService;
import org.itx.jbalance.equeue.gwt.client.services.DOUGroupsServiceAsync;
import org.itx.jbalance.equeue.gwt.client.services.JuridicalTypesService;
import org.itx.jbalance.equeue.gwt.client.services.JuridicalTypesServiceAsync;
import org.itx.jbalance.equeue.gwt.client.services.RegionsService;
import org.itx.jbalance.equeue.gwt.client.services.RegionsServiceAsync;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.o.JuridicalType;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.o.StaffDivision;
import org.itx.jbalance.l0.s.SStaffDivision;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author dv6
 *
 */
public class ContragentEditPresenterImpl extends MyPresenter<ContragentEditWidget> implements ContragentEditPresenter {
	
	private final JuridicalTypesServiceAsync juridicalTypesService = GWT.create(JuridicalTypesService.class);
	private final RegionsServiceAsync regionsService = GWT.create(RegionsService.class);
	private final ContragentsServiceAsync contragentsService = GWT.create(ContragentsService.class);
	private final DOUGroupsServiceAsync douGroupsService = GWT.create(DOUGroupsService.class);

	ArrayList<DOUGroups> items;
	ArrayList<JuridicalType> juridicalTypesItems;
	ArrayList<Region> regionsItems;
	private HStaffDivision editedHStaffDivision;
	private Juridical editedJuridical;
	private State state;
	private List<SStaffDivision> sStaffDivisionsEdit;
	private int itogo;
	private List<SStaffDivision> sStaffDivisionsNew=new ArrayList<SStaffDivision>();
	
	
	public String getDOUGroupsNameById(int id) {
		for (DOUGroups douGroups : items) {
			if (douGroups.getId() == id)
				return douGroups.getName();
		}
		return null;
	}

	/**
	 * @param display
	 */
	public ContragentEditPresenterImpl(ContragentEditWidget display) {
		super(display);
	}



	@Override
	public void initialize() {
		loadData();
		//initListeners();
	}

	

	public void loadData() {
		douGroupsService.getAll(new AsyncCallback<ArrayList<DOUGroups>>() {
			@Override
			public void onSuccess(ArrayList<DOUGroups> result) {
				items = result;
				getDisplay().setDOUGroupsItems(items);
				juridicalTypesService.getAll(new AsyncCallback<ArrayList<JuridicalType>>() {
					@Override
					public void onSuccess(ArrayList<JuridicalType> result) {
						juridicalTypesItems = result;
						getDisplay().setJuridicalTypesItems(juridicalTypesItems);
						regionsService.getAll(new AsyncCallback<ArrayList<Region>>() {
							@Override
							public void onSuccess(ArrayList<Region> result) {
								regionsItems = result;
								getDisplay().setRegionsItems(regionsItems);
								getDisplay().initData();
								if(regionsItems == null || regionsItems.isEmpty()){
									getDisplay().showWarningNotification("Перед добавлением контрагентов, заполните справочник районов!");
								}
							}

							@Override
							public void onFailure(Throwable caught) {
								handleException(caught);
							}
						});
					}

					@Override
					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});
			}

			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}

	@Override
	public void saveDou() {
		if (getDisplay().getIsDou().getValue()) {
			Dou p = new Dou();
			p.setName(getDisplay().getName().getText());
			p.setINN(getDisplay().getInn().getText());
			p.setJAddress(getDisplay().getJurAddress().getText());
			p.setPAddress(getDisplay().getFizAddress().getText());
			p.setDouType(getDisplay().getDouType());
			
			Region region = getSelectedRegion(); 
			
			if(region == null){
				getDisplay().showWarningNotification("Выберите район!");
				return;
			}
			
			p.setRegion(region);
			//p.setDouTypeId(getDisplay().getDouTypeId());
			//p.setRegionId(new Integer(getDisplay().getRegionId()).intValue());
			p.setPhone(getDisplay().getTel().getText());
			p.setEMail(getDisplay().getEmail().getText());
			p.setNumber(getDisplay().getIdDou().getText());

			HStaffDivision doc = new HStaffDivision();
			doc.setFoundation("bb");
			List<SStaffDivision> groupsRows = getDisplay().getGroupsRows();
			List<StaffDivision> staffDivisions = new ArrayList<StaffDivision>();
			for(SStaffDivision division : groupsRows){
				staffDivisions.add(division.getDivision());
			}

			contragentsService.createNewDou(p, doc, staffDivisions,
					new AsyncCallback<Void>() {

						@Override
						public void onSuccess(Void result) {
							getDisplay().showInfoNotification("Ура! Новый МДОУ сохранен.");

						}

						@Override
						public void onFailure(Throwable caught) {
							handleException(caught);
						}
					});
		}
	}

	private Region getSelectedRegion() {
		String regionName = getDisplay().getRegion();
		if(regionName==null){
			return null;
		}
		Region region = null;
		for (Region	reg : regionsItems) {
			if(reg.getName().equals(regionName)){
				region = reg;
				break;
			}
		}
		return region;
	}
	
	@Override
	public void saveContragent() {
	  if (!getDisplay().getIsDou().getValue()) {
		Juridical p = new Juridical();
		p.setName(getDisplay().getName().getText());
		p.setINN(getDisplay().getInn().getText());
		p.setJAddress(getDisplay().getJurAddress().getText());
		p.setPAddress(getDisplay().getFizAddress().getText());
		p.setEMail(getDisplay().getEmail().getText());
		p.setPhone(getDisplay().getTel().getText());
		
		contragentsService.createNewJur(p, new AsyncCallback<Void>() {
			@Override
			public void onSuccess(Void result) {
				getDisplay().showInfoNotification("Ура! Новый контрагент сохранен.");
			}
		
			@Override
			public void onFailure(Throwable caught) {
					handleException(caught);
				}
			});
		}
	}


	@Override
	public void setDou(HStaffDivision division) {
		this.editedHStaffDivision = division;
		getDisplay().getEmail().setText(editedHStaffDivision.getDou().getEMail());
		getDisplay().getInn().setText(editedHStaffDivision.getDou().getINN());
		getDisplay().getFizAddress().setText(editedHStaffDivision.getDou().getPAddress());
		getDisplay().getJurAddress().setText(editedHStaffDivision.getDou().getJAddress());
		getDisplay().getName().setText(editedHStaffDivision.getDou().getName());
		// Phone!! //getDisplay().getTel().setText(editedJuridical.get);
		getDisplay().getIsDou().setValue(true);
		getDisplay().getIdDou().setText(editedHStaffDivision.getDou().getNumber()+"");
		getDisplay().getTel().setText(editedHStaffDivision.getDou().getPhone());
		getDisplay().disableComponents(true);

		juridicalTypesService
				.getAll(new AsyncCallback<ArrayList<JuridicalType>>() {
					@Override
					public void onSuccess(ArrayList<JuridicalType> result) {
						juridicalTypesItems = result;
						getDisplay()
								.setJuridicalTypesItems(juridicalTypesItems);
						getDisplay().ititDouTypes();
						String douType = editedHStaffDivision.getDou().getDouType();
						if(douType!=null){
							getDisplay().setdouTypeSelected(douType);
						    getDisplay().ititdouTypeSelected();
						}
						regionsService.getAll(new AsyncCallback<ArrayList<Region>>() {
									@Override
									public void onSuccess(ArrayList<Region> result) {
										regionsItems = result;
										getDisplay().setRegionsItems(regionsItems);
										getDisplay().ititRegions();
										Dou dou = editedHStaffDivision.getDou();
										String region = dou.getRegion()==null?null:dou.getRegion().getName();
										if(region!=null){
											getDisplay().setRegionSelected(region);
										    getDisplay().ititRegionSelected();
										}
									}

									@Override
									public void onFailure(Throwable caught) {
										handleException(caught);
									}
								});

						douGroupsService
								.getAll(new AsyncCallback<ArrayList<DOUGroups>>() {
									@Override
									public void onSuccess(
											ArrayList<DOUGroups> result) {
										items = result;
										getDisplay().setDOUGroupsItems(items);
									}

									@Override
									public void onFailure(Throwable caught) {
										handleException(caught);
									}
								});

					}

					@Override
					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});

		douGroupsService.getAll(new AsyncCallback<ArrayList<DOUGroups>>() {
			@Override
			public void onSuccess(ArrayList<DOUGroups> result) {
				items = result;
				getDisplay().setDOUGroupsItems(items);
				// /getDisplay().initTableColumns();
				contragentsService.getSStaffDivision(editedHStaffDivision,
						new AsyncCallback<List<SStaffDivision>>() {
							@Override
							public void onSuccess(List<SStaffDivision> result) {
								itogo = 0;
								for (SStaffDivision sStaffDivision : result)
									itogo += sStaffDivision.getDivision()
											.getKm();
								getDisplay().rows = result;
								sStaffDivisionsEdit = (List<SStaffDivision>) ((ArrayList<SStaffDivision>) result).clone();
								getDisplay().getGroupCellTable().setRowData(
										getDisplay().rows);
								getDisplay().getItogoTextBox().setText(
										new Integer(itogo).toString());
								getDisplay().initTableColumns();
							}

							@Override
							public void onFailure(Throwable caught) {
								handleException(caught);
							}
						});
			}

			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});

	}

	@Override
	public void setContragent(Juridical juridical) {
		this.editedJuridical=juridical;
		getDisplay().getEmail().setText(editedJuridical.getEMail());
		getDisplay().getInn().setText(editedJuridical.getINN());
		getDisplay().getFizAddress().setText(editedJuridical.getPAddress());
		getDisplay().getJurAddress().setText(editedJuridical.getJAddress());
		getDisplay().getName().setText(editedJuridical.getName());
		getDisplay().getIsDou().setValue(false);
		getDisplay().getTel().setText(editedJuridical.getPhone());
		getDisplay().disableComponents(false); 
	}

	@Override
	public void setState(State state) {
		this.state=state;
	}
	
	@Override
	public State getState() {
		return state;
	}

	@Override
	public void updateDou() {
			Dou dou = editedHStaffDivision.getDou();
			dou.setName(getDisplay().getName().getText());
			dou.setINN(getDisplay().getInn().getText());
			dou.setJAddress(getDisplay().getJurAddress().getText());
			dou.setPAddress(getDisplay().getFizAddress().getText());
			dou.setDouType(getDisplay().getDouType());
			dou.setRegion(getSelectedRegion());
			
			//dou.setDouTypeId(getDisplay().getDouTypeId());
			//dou.setRegionId(new Integer(getDisplay().getRegionId()).intValue());
			dou.setPhone(getDisplay().getTel().getText());
			dou.setNumber(getDisplay().getIdDou().getText());
		
			dou.setEMail(getDisplay().getEmail().getText());

			editedHStaffDivision.setFoundation("bb");
			
//			List<SStaffDivision> groupsRows = getDisplay().getGroupsRows();
		
			contragentsService.updateDou(editedHStaffDivision, sStaffDivisionsEdit,sStaffDivisionsNew, // groupsRows,
					new AsyncCallback<OperationResultWrapper<String>>() {

						@Override
						public void onSuccess(OperationResultWrapper<String> result) {
							switch(result.getStatus()){
							case SERVER_ERROR:
							case VALIDATION_ERROR:
								getDisplay().showWarningNotification("Контрагент не сохранен.");
							case SUCCESS:
								getDisplay().showInfoNotification("Ура! МДОУ сохранен.");
							}
							
						}

						@Override
						public void onFailure(Throwable caught) {
							handleException(caught);
						}
					});
		
	}

	@Override
	public void updateContragent() {
		editedJuridical.setName(getDisplay().getName().getText());
		editedJuridical.setINN(getDisplay().getInn().getText());
		editedJuridical.setJAddress(getDisplay().getJurAddress().getText());
		editedJuridical.setPAddress(getDisplay().getFizAddress().getText());
		editedJuridical.setEMail(getDisplay().getEmail().getText());
		editedJuridical.setPhone(getDisplay().getTel().getText());
		
		contragentsService.updateJur(editedJuridical,
				new AsyncCallback<Void>() {

					@Override
					public void onSuccess(Void result) {
						getDisplay().showInfoNotification("Ура! Контрагент сохранен.");
					}

					@Override
					public void onFailure(Throwable caught) {
						handleException(caught);
					}
				});

	}
	
	@Override
	public List<SStaffDivision> getsStaffDivisionsNew() {
		return sStaffDivisionsNew;
	}
	
	@Override
	public void setsStaffDivisionsNew(List<SStaffDivision> sStaffDivisionsNew) {
		this.sStaffDivisionsNew = sStaffDivisionsNew;
	}

}