package org.itx.jbalance.equeue.gwt.client.register;

import java.util.Date;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.services.RegisterService;
import org.itx.jbalance.equeue.gwt.client.services.RegisterServiceAsync;
import org.itx.jbalance.equeue.gwt.client.services.SearchResultWrapper;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l1.api.RegisterSearchParams;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.DateFilterType;
import org.itx.jbalance.l2_api.dto.equeue.RegisterDTO;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class RegisterListPresenterImpl extends MyPresenter<RegisterListWidget>
		implements RegisterListPresenter {

	
	private final RegisterServiceAsync service = GWT.create(RegisterService.class);
//	private final ContragentsServiceAsync  contragentsService= GWT.create(ContragentsService.class);
//	
//	ArrayList<Privilege> rows;
	
	public RegisterListPresenterImpl(RegisterListWidget display) {
		super(display);
	}



	@Override
	public void initialize() {
		Log.debug("initialization METRICS [1]");
		search();
		initListeners();
		Log.debug("initialization METRICS [2]");
	}

	private void initListeners() {}

	


	@Override
	public void populateDetails(RegisterDTO r) {
		service.getSpec(r.getUId(), new AsyncCallback<List<SRegister>>() {
			
			@Override
			public void onSuccess(List<SRegister> result) {
				getDisplay().populateSpec(result);
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}

	
	
	@Override
	public void delete(Long id) {
		
//		if(state!=State.VIEW_REQUESTS)
//			throw new RuntimeException();
//		else{
			Log.debug("delete Register with id="+id);
			service.delete(id, new AsyncCallback<Void>() {
				@Override
				public void onSuccess(Void result) {
					loadData(0,PAGE_SIZE-1,null);
				}
				@Override
				public void onFailure(Throwable caught) {
					handleException(caught);
				}
			});
//		}
	}


	RegisterSearchParams searchParams=new RegisterSearchParams();

	@Override
	public void loadData(final int start, int end,
			List<SortedColumnInfo> sortedColumns) {
		final long startTime = System.currentTimeMillis();
		searchParams.setFrom(start);
		searchParams.setTo(end);
		service.search(searchParams,new AsyncCallback< SearchResultWrapper<RegisterDTO>>() {
			
			@Override
			public void onSuccess( SearchResultWrapper<RegisterDTO> result) {
				long searchEndedTime = System.currentTimeMillis();
				Log.debug("Search - "+(searchEndedTime-startTime)+" ms.");
				getDisplay().updateRowData(start, result.getList());
				Log.debug("Rendering - "+(System.currentTimeMillis()-searchEndedTime)+" ms.");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}



	private RegisterSearchParams getSearchParams() {
		searchParams.custom=getDisplay().getSearchField().getText();
		
		Date openDateFrom = getDisplay().getOpenDateFrom().getValue();
		
		Date openDateTo = getDisplay().getOpenDateTo().getValue();
		
		searchParams.registerDate=DateFilter.build(openDateFrom,openDateTo);
		return searchParams;
	}



	@Override
	public void search() {
		Log.debug("search()");
		searchParams.setFrom(0);
		searchParams.setTo(PAGE_SIZE-1);
		service.search(getSearchParams(),new AsyncCallback< SearchResultWrapper<RegisterDTO>>() {
			
			@Override
			public void onSuccess( SearchResultWrapper<RegisterDTO> result) {
				getDisplay().showRegisters(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}

	
}

