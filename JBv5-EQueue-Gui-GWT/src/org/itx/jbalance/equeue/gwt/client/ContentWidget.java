package org.itx.jbalance.equeue.gwt.client;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public abstract class ContentWidget extends Composite implements MyDisplay {

	public ContentWidget() {
	}
	
	@Deprecated
	public ContentWidget(String name) {
	}

	public abstract void initialize();

	public void showExceptionNotification(Throwable caught){
		Log.error("",caught);
		Throwable root = caught;
		while(root.getCause() != null){
			root = root.getCause();
		}
		DialogBox dialogBox = createNotificationDialog(caught.getMessage(), NotificationType.ERROR, root);
		dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);

	    dialogBox.center();
	    dialogBox.show();
	}
	
	public void showWarningNotification(String message){
		Log.warn(message);
		DialogBox dialogBox = createNotificationDialog(message, NotificationType.WARN, null);
	    dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);

	    dialogBox.center();
	    dialogBox.show();
	}
	
	public void showInfoNotification(String message){
		Log.info(message);
		
		DialogBox dialogBox = createNotificationDialog(message, NotificationType.INFO, null);
	    dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);

	    dialogBox.center();
	    dialogBox.show();
	}
	
	enum NotificationType{
		ERROR, WARN, INFO
	}
	
	private DialogBox createNotificationDialog(String message, NotificationType notificationType, Throwable caught) {
	    // Create a dialog box and set the caption text
	    final DialogBox dialogBox = new DialogBox();
	    dialogBox.ensureDebugId("cwDialogBox");
	    
	    switch (notificationType) {
			case ERROR:
				dialogBox.setText("Возникли неожиданные ошибки!");
				break;
			case WARN:
			case INFO:
				dialogBox.setText("Внимание!!!");
				break;
		}

	    // Create a table to layout the content
	    DockPanel dialogContents = new DockPanel();
	    dialogContents.setSpacing(4);
	    dialogContents.setWidth("600px");
	    dialogContents.setHeight("200px");
	    dialogBox.setWidget(dialogContents);

	    Image img = null;
	    switch (notificationType) {
			case ERROR:
				img = new Image("images/error.png");
				break;
			case WARN:
				img = new Image("images/warn.png");
				break;
			case INFO:
				img = new Image("images/info.png");
				break;
	    }
	    
	    HTML details = new HTML(message);
	    details.setWidth("490px");
	    dialogContents.add(details, DockPanel.CENTER);
	    dialogContents.add(img, DockPanel.WEST);
	    
	    Button okButton = new Button("Ok");
	    okButton.setWidth("100px");
	    ClickHandler closeHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		};
		okButton.addClickHandler(closeHandler);
		dialogContents.add(okButton, DockPanel.SOUTH);
	    return dialogBox;
	  }

	/**
	 * Показывает окошко с кнопочками
	 * @param title
	 * @param details
	 * @param buttons
	 * @return
	 */
	public DialogBox askQuestion(final String title, final HTML details, Button ... buttons) {
	    // Create a dialog box and set the caption text
	    final DialogBox dialogBox = new DialogBox();
	    dialogBox.ensureDebugId("cwDialogBox");
	    dialogBox.setText(title);

	    // Create a table to layout the content
	    VerticalPanel dialogContents = new VerticalPanel();
	    dialogContents.setSpacing(4);
	    dialogBox.setWidget(dialogContents);

	    
	    dialogContents.add(details);
	    dialogContents.setCellHorizontalAlignment(
	        details, HasHorizontalAlignment.ALIGN_CENTER);

	    HorizontalPanel buttonsPanel = new HorizontalPanel();
	    
	    ClickHandler closeHandler = new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		};
	    
	    for (Button b : buttons) {
	    	b.addClickHandler(closeHandler);
	    	buttonsPanel.add(b);
		}

		dialogContents.add(buttonsPanel);
		
	    return dialogBox;
	  }
	
	public void showWaitCursor() {
	    DOM.setStyleAttribute(RootPanel.getBodyElement(), "cursor", "wait");
	}
	 
	public void showDefaultCursor() {
	    DOM.setStyleAttribute(RootPanel.getBodyElement(), "cursor", "default");
	}
}
