package org.itx.jbalance.equeue.gwt.client;

import com.google.gwt.user.client.Timer;

public class RepeatableProgressBar extends ProgressBar{
	
	 Timer timer ;
	
	 public RepeatableProgressBar() {
		 this(4,500);
	 }
	 
	public RepeatableProgressBar(final int inc, int ms) {
		super();
//		setTextVisible(textVisible)ext("Загрузка");
//		
		timer =  new Timer() {
			@Override
			public void run() {
				 double progress = getProgress()+inc;
			     if (progress>100) progress= 0;
			     setProgress(progress);
			}
		 };
		
		timer.scheduleRepeating(ms);
		
	}

	public void stop(){
		timer.cancel();
	}
}
