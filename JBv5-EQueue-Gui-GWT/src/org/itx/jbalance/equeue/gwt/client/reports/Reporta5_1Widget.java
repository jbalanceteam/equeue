package org.itx.jbalance.equeue.gwt.client.reports;

import java.util.Date;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.DateChooserComponent;
import org.itx.jbalance.equeue.gwt.client.RepeatableProgressBar;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.equeue.gwt.shared.Report5_1DTO;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Reporta5_1Widget extends ContentWidget implements Reporta5_1Presenter.Display {

	interface Binder extends UiBinder<Widget, Reporta5_1Widget> {
	}

	@UiField(provided=true)
	CellTable<Report5_1DTO> table=new CellTable<Report5_1DTO>(0,MyCellTableResources.INSTANCE);
	
	@UiField
	SimplePanel progressPanel;
	
	@UiField
	SimplePanel effectiveDatePanel;
	
	RepeatableProgressBar progressBar;
	
	private DateChooserComponent effectiveDate = new DateChooserComponent();
	
	private Reporta5_1Presenter presenter;
	
	@Override
	public void initialize() {
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		initTableColumns();
		progressBar = new RepeatableProgressBar();
		progressPanel.add(progressBar);
		effectiveDatePanel.add(effectiveDate);
		effectiveDate.addValueChangeHandler(new ValueChangeHandler<Date>() {
			@Override
			public void onValueChange(ValueChangeEvent<Date> event) {
				progressBar = new RepeatableProgressBar();
				progressPanel.clear();
				progressPanel.add(progressBar);
				
				getPresenter().updateOnDate();
			}
		});
	}

	
	
	private void initTableColumns(){
		/* Age */
		Column<Report5_1DTO, String> ageColumn = new Column<Report5_1DTO, String>(
				new TextCell()) {
			@Override
			public String getValue(Report5_1DTO o) {
				if(o.getFrom() == null && o.getTo() == null){
					return "ВСЕГО";
				}
				int from=o.getFrom();
				int to=o.getTo();
				return from/12+"."+from%12 +" - "+to/12+"."+to%12;
			}
		};

		table.addColumn(ageColumn, SafeHtmlUtils.fromSafeConstant("Возраст"));
		
		/* Количество Детей в очереди */
		Column<Report5_1DTO, String> countColumn = new Column<Report5_1DTO, String>(
				new TextCell()) {
			@Override
			public String getValue(Report5_1DTO o) {
				
				 return o.getCount()+"";
			}
		};

		table.addColumn(countColumn, SafeHtmlUtils.fromSafeConstant("Количество детей в очереди"));

		/* Количество Детей в очереди */
		Column<Report5_1DTO, String> rejectedCountColumn = new Column<Report5_1DTO, String>(
				new TextCell()) {
			@Override
			public String getValue(Report5_1DTO o) {
				
				 return o.getRejectedCount()+"";
			}
		};

		table.addColumn(rejectedCountColumn, SafeHtmlUtils.fromSafeConstant("Нехватило мест"));
		
		/* 
		  Issue #370 
		  Добавить в отчтет №1 Потребность в местах
		  */
		Column<Report5_1DTO, String> howMuchNeetCountColumn = new Column<Report5_1DTO, String>(
				new TextCell()) {
			@Override
			public String getValue(Report5_1DTO o) {
				 return o.getHowMuchNeed()+"";
			}
		};

		table.addColumn(howMuchNeetCountColumn, SafeHtmlUtils.fromSafeConstant("Потребность в местах"));
		
	}
	 
	public Reporta5_1Presenter getPresenter() {
		return presenter;
	}

	public void setPresenter(Reporta5_1Presenter presenter) {
		this.presenter = presenter;
	}

	public void setReportData(List<Report5_1DTO>data){
		if(progressBar != null){
			progressBar.stop();
			progressBar = null;
		}
		progressPanel.clear();
		table.setRowData(data);
	}
	
	public HasValue<Date> getEffectiveDate(){
		return effectiveDate;
	}
}
