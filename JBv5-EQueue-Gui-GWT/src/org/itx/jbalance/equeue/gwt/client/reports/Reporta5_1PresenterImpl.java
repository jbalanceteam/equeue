package org.itx.jbalance.equeue.gwt.client.reports;

import java.util.Date;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsService;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsServiceAsync;
import org.itx.jbalance.equeue.gwt.shared.GetReport5_1Request;
import org.itx.jbalance.equeue.gwt.shared.Report5_1DTO;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Контроллер(пресентор) для формы просмотра списка заявок.
 * @author apv
 *
 */
public class Reporta5_1PresenterImpl extends MyPresenter<Reporta5_1Widget>
		implements Reporta5_1Presenter {


	public Reporta5_1PresenterImpl(Reporta5_1Widget display) {
		super(display);
	
	}

	private final DouRequestsServiceAsync service = GWT.create(DouRequestsService.class);

	@Override
	public void initialize() {
		updateTable(null);
	}
	
	@Override
	public void updateOnDate() {
		Date effectiveDate = getDisplay().getEffectiveDate().getValue();
		Log.debug("updateOnDate() effectiveDate: "+effectiveDate);
		updateTable(effectiveDate);
	}

	private void updateTable(Date effectiveDate) {
		GetReport5_1Request request = new GetReport5_1Request();
		
		request.setEffectiveDate(effectiveDate);
		
		request.getAges().add(new Report5_1DTO(0,12)); // 0.0-1.0
		request.getAges().add(new Report5_1DTO(13,18));// 1.1-1.6
		request.getAges().add(new Report5_1DTO(19,24));// 1.7-2.0
		request.getAges().add(new Report5_1DTO(25,30));// 2.1-2.6
		request.getAges().add(new Report5_1DTO(31,36));// 2.7-3.0
		request.getAges().add(new Report5_1DTO(37,42));// 3.1-3.6
		request.getAges().add(new Report5_1DTO(43,48));// 3.7-4.0
		request.getAges().add(new Report5_1DTO(49,60));// 4.1-5.0
		request.getAges().add(new Report5_1DTO(61,72));// 5.1-6.0
		request.getAges().add(new Report5_1DTO(73,78));// 6.1-6.6
		request.getAges().add(new Report5_1DTO(79,84));// 6.7-7.0
		
		request.getAges().add(new Report5_1DTO(null, null)); //#353  Сделать дополнительную строку в отчете №1
//		Значения "ИТОГО" могут отличаться от суммы всех строк, т.к. в очереди могут быть дете вне заданных интервалов
		
		service.getReport5_1Data(request, new AsyncCallback<List<Report5_1DTO>>() {
			
			@Override
			public void onSuccess(List<Report5_1DTO> result) {
//				Log.debug(""+result);
				getDisplay().setReportData(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}
}

