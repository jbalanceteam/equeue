package org.itx.jbalance.equeue.gwt.client.reports;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsService;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsServiceAsync;
import org.itx.jbalance.equeue.gwt.shared.GetReport5_1Request;
import org.itx.jbalance.equeue.gwt.shared.Report5_1DTO;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;


/**
 * Контроллер(пресентор) для формы просмотра списка заявок.
 * @author apv
 *
 */
public class Reporta5_4PresenterImpl extends MyPresenter<Reporta5_4Widget>
		implements Reporta5_4Presenter {


	
	
	public Reporta5_4PresenterImpl(Reporta5_4Widget display) {
		super(display);
	}

	private final DouRequestsServiceAsync service = GWT.create(DouRequestsService.class);

	@Override
	public void initialize() {
		
		GetReport5_1Request request = new GetReport5_1Request();
		request.getAges().add(new Report5_1DTO(1,4));
		request.getAges().add(new Report5_1DTO(5,7));
		request.getAges().add(new Report5_1DTO(10,18));
		
		
		service.getReport5_1Data(request, new AsyncCallback<List<Report5_1DTO>>() {
			
			@Override
			public void onSuccess(List<Report5_1DTO> result) {
				Log.debug(""+result);
				getDisplay().setReportData(result);
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}
}

