package org.itx.jbalance.equeue.gwt.client.regions;

import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.regions.RegionsPresenter.Display;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.s.SStaffDivision;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ActionCell.Delegate;
import com.google.gwt.cell.client.CompositeCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.HasCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class RegionsWidget extends ContentWidget implements Display {

	interface Binder extends UiBinder<Widget, RegionsWidget> {

	}

	@UiField(provided = true)
	CellTable<Region> cellTable;

	@UiField
	TextBox nameTextBox;

	@UiField
	TextBox descriptionField;

	@UiField
	Button addNewButton;

	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public RegionsWidget() {
	}

	@Override
	public void initialize() {
		cellTable = new CellTable<Region>(0,MyCellTableResources.INSTANCE);
		cellTable.setRowData(new ArrayList<Region>());
		addNewButton = new Button("Добавить");
		// Create the UiBinder.
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		initTableColumns();
	}

	private void initTableColumns() {
		Column<Region, String> idColumn = new Column<Region, String>(
				new TextCell()) {
			@Override
			public String getValue(Region object) {
				return object.getUId() + "";
			}
		};

		cellTable.addColumn(idColumn, SafeHtmlUtils.fromSafeConstant("ID"));

		/* name */
		Column<Region, String> nameColumn = new Column<Region, String>(
				new  EditTextCell()) {
			@Override
			public String getValue(Region object) {
				return object.getName() + "";
			}
		};

		cellTable.addColumn(nameColumn,
				SafeHtmlUtils.fromSafeConstant("Наименование"));
		nameColumn.setFieldUpdater(new FieldUpdater<Region, String>() {

			@Override
			public void update(int index, Region object, String value) {
				object.setName(value);
				getPresenter().update(object);

			}

		});

		/* description */
		Column<Region, String> descriptionColumn = new Column<Region, String>(
				new EditTextCell()) {
			@Override
			public String getValue(Region object) {
				return object.getDescription() + "";
			}
		};

		cellTable.addColumn(descriptionColumn,
				SafeHtmlUtils.fromSafeConstant("Описание"));
		descriptionColumn
				.setFieldUpdater(new FieldUpdater<Region, String>() {

					@Override
					public void update(int index, Region object, String value) {
						object.setDescription(value);
						getPresenter().update(object);
					}
				});
	
	
		
		/* actions */
		ActionCell<Region> delCell = new ActionCell<Region>(SafeHtmlUtils.fromSafeConstant("X"),new Delegate<Region>() {
		
			@Override
			public void execute(Region p) {
				if(Window.confirm("Удалить безвозвратно?"))
						getPresenter().delete(p.getUId());
			}
		});
		
		
		Column <Region,Region>delColumn =new Column<Region,Region>(
			delCell){
				@Override
				public Region getValue(Region object) {
					return object;
				}}; 
				
		
		
		List<HasCell<Region, ?>>  hasCells = new ArrayList<HasCell<Region, ?>>();
		
		hasCells.add(delColumn);
		
		CompositeCell<Region> cell=new CompositeCell<Region>(
			hasCells );
		
		
		Column <Region,Region>actionColumn =new Column<Region,Region>(
			cell){
		
				@Override
				public Region getValue(Region object) {
					return object;
				}}; 
		cellTable.addColumn(actionColumn, SafeHtmlUtils.fromSafeConstant("Действие"));
	
	}
	
//	@UiHandler("addNewButton")
//	public void handlerOk(ClickEvent event){
//		Window.al1ert("Hey!");
//	}
	
	@Override
	public HasText getRegionsName() {
		return nameTextBox;
	}

	@Override
	public HasText getDescription() {
		return descriptionField;
	}

	@Override
	public void showRegions(List<Region> list) {
		cellTable.setRowData(list);
	}

	@Override
	public HasClickHandlers getSaveClickHandlers() {
		return addNewButton;
	}

	
	
	
	
	
	RegionsPresenter presenter;

	public RegionsPresenter getPresenter() {
		return presenter;
	}


	public void setPresenter(RegionsPresenter presenter) {
		this.presenter=presenter;
	}


}
