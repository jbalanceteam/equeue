package org.itx.jbalance.equeue.gwt.client.register;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.itx.jbalance.equeue.gwt.client.Callback;
import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.services.RegisterService;
import org.itx.jbalance.equeue.gwt.client.services.RegisterServiceAsync;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper.Status;
import org.itx.jbalance.equeue.gwt.shared.RegisterEditData;
import org.itx.jbalance.equeue.gwt.shared.RegisterLine;
import org.itx.jbalance.equeue.gwt.shared.SaveRegisterResult;
import org.itx.jbalance.equeue.gwt.shared.SetRegNumsRequest;
import org.itx.jbalance.equeue.gwt.shared.SetRegNumsResult;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l0.s.SRegister.Foundation;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class RegisterEditPresenterImpl extends MyPresenter<RegisterEditWidget>
		implements RegisterEditPresenter {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final RegisterServiceAsync registerService = GWT
			.create(RegisterService.class);
	// final List<SRegister>spec=new ArrayList<SRegister>();
	final List<RegisterLine> tableData = new ArrayList<RegisterLine>();

	private List<Dou> allDou;
	private List<DOUGroups> ageGroups;
	private Map<Integer, DOUGroups> ageGroupsMap = new HashMap<Integer, DOUGroups>();
	private HRegister register;

	public RegisterEditPresenterImpl(RegisterEditWidget display) {
		super(display);
	}

	@Override
	public void save(final Callback onSuccess) {
		// Исключение возможности повторного сохранения протокола
		getDisplay().disableSaveButton();
		getDisplay().disableSave1Button();
		getDisplay().progressStart();
		// List<String> committee=new ArrayList<String>();
		// Date registerDate=new Date();

		populateRegisterFromFormFields();

		if (getRegister().getUId() == null) {
			registerService.create(register, tableData,
					new AsyncCallback<SaveRegisterResult>() {

						@Override
						public void onSuccess(SaveRegisterResult result) {
							if (Status.SUCCESS != result.getStatus()) {
								getDisplay().showWarningNotification(
										result.getComments());
							} else {
								
								getDisplay().showInfoNotification(
										"Протокол успешно сохранен.<br/>Номер протокола: "
												+ result.getResult()
														.getDnumber());
								
								if(onSuccess!=null){
									onSuccess.callback();
								} 
							}
							if (result.getResult() != null)
								register = result.getResult();

							if (result.getRegisterLines() != null
									&& !result.getRegisterLines().isEmpty()) {
								tableData.clear();
								tableData.addAll(result.getRegisterLines());
							}

							updateView();
							getDisplay().enableSaveButton();
							getDisplay().enableSave1Button();
							getDisplay().progressStop();
						}

						@Override
						public void onFailure(Throwable caught) {
							getDisplay().progressStop();
							handleException(caught);
							getDisplay().enableSaveButton();
							getDisplay().enableSave1Button();
						}
					});
		} else {
			registerService.update(register, tableData,
					new AsyncCallback<SaveRegisterResult>() {

						@Override
						public void onSuccess(SaveRegisterResult result) {
							if (Status.SUCCESS != result.getStatus()) {
								getDisplay().showWarningNotification(
										result.getComments());
							} else {
								if(onSuccess!=null){
									onSuccess.callback();
									return;
								} else {
									getDisplay().showInfoNotification(
											"Протокол успешно модифицирован.<br/>Номер протокола: "
												+ result.getResult().getDnumber());
								}
							}
							if (result.getResult() != null) {
								register = result.getResult();
							}

							if (result.getRegisterLines() != null
									&& !result.getRegisterLines().isEmpty()) {
								tableData.clear();
								tableData.addAll(result.getRegisterLines());
							}

							updateView();
							getDisplay().enableSaveButton();
							getDisplay().progressStop();
						}

						@Override
						public void onFailure(Throwable caught) {
							getDisplay().progressStop();
							handleException(caught);
							getDisplay().enableSaveButton();
							getDisplay().enableSave1Button();
						}
					});

		}

	}

	private void populateRegisterFromFormFields() {
		register.setFoundation("hz");
		register.setMember1(getDisplay().getMember1().getText());
		register.setMember2(getDisplay().getMember2().getText());
		register.setMember3(getDisplay().getMember3().getText());
		register.setReceivePermitDueDate(getDisplay().getReceiveDueDate().getValue());
		if (getDisplay().getAgeFlagCheckBox().getValue() == Boolean.TRUE){
			register.setAgereprocessdate(getDisplay().getAgeReprocessDate().getValue());
		} else {
			register.setAgereprocessdate(null);
		}
	}

	@Override
	public void initialize() {
	}

	@Override
	public void setObject(final Long objectUid) {
		getDisplay().progressStart();

		registerService.getEditData(objectUid,
				new AsyncCallback<RegisterEditData>() {
					@Override
					public void onSuccess(RegisterEditData result) {
						register = result.getRegister();

						allDou = result.getAllDou();
						ageGroups = result.getAgeGroups();
						getDisplay().setAgeGroups(ageGroups);
						for (DOUGroups ageGroup : ageGroups) {
							ageGroupsMap.put(ageGroup.getId(), ageGroup);
						}
						tableData.clear();
						List<RegisterLine> registerLines = result
								.getRegisterLines();

						if (getRegister().getStatus() != RegisterStatus.THIRD) {
							for (RegisterLine registerLine : registerLines) {
								if (registerLine.getDouRequest().getStatus() == HDouRequest.Status.PERMIT) {
									registerLine.getSRegister().setRnAction(Action.BACK_TO_QUEUE);
								}
							}
						}

						tableData.addAll(registerLines);
						updateView();
						getDisplay().progressStop();
					}

					@Override
					public void onFailure(Throwable caught) {
						getDisplay().progressStop();
						handleException(caught);
					}
				});
	}

	public void updateView() {
		getDisplay().getMember1().setText(getRegister().getMember1());
		getDisplay().getMember2().setText(getRegister().getMember2());
		getDisplay().getMember3().setText(getRegister().getMember3());
		getDisplay().getReceiveDueDate().setValue(getRegister().getReceivePermitDueDate());

		getDisplay().setOpenDate(
				DateTimeFormat.getFormat("dd.MM.yyyy").format(
						getRegister().getRegisterDate()));
		if (getRegister().getDnumber()!=null){
			getDisplay().setRegisterNumber(getRegister().getDnumber().toString());
		}
		
		getDisplay().setRequests(tableData);
		getDisplay().initByStatus();

		if (getRegister().getAgereprocessdate() != null) 
		//если есть сохраненная дата пересчета возраста то ставим ее 
		// и выставляем контролы
		{
			getDisplay().getAgeReprocessDate().setValue(
					getRegister().getAgereprocessdate());
			getDisplay().setWidgetsIfAgeReprocessDate(true);
		}
		else 
			//если нет сохраненной даты пересчета возраста то ставим дефолтную 
			// и выставляем контролы
		{ 
			String defaultReprocessAgeString = "01.09."+String.valueOf(new Date().getYear()+1900);
		      DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd.MM.yy");
		      Date defaultReprocessAgeDate = dateTimeFormat.parse(defaultReprocessAgeString);
			  getDisplay().getAgeReprocessDate().setValue(defaultReprocessAgeDate);
			  getDisplay().setWidgetsIfAgeReprocessDate(false);
		}
	}

	public void clearRequests() {
		tableData.clear();
		getDisplay().setRequests(tableData);
	}

	// @Override
	// public List<SRegister> getSpec(){
	// return spec;
	// }

	/**
	 * При переходе от списка РН к формированию протокола
	 */
	public void setRegNums(final Set<Long> ids) {
		getDisplay().progressStart();
		// douService.getAllDou(new AsyncCallback<List<Dou>>() {
		//
		// @Override
		// public void onSuccess(List<Dou> result) {
		// allDou=result;

		SetRegNumsRequest request = new SetRegNumsRequest();
		request.setIds(ids);
		registerService.getSetRegNumsData(request,
				new AsyncCallback<SetRegNumsResult>() {

					@Override
					public void onSuccess(SetRegNumsResult result) {
						allDou = result.getAllDou();
						ageGroups = result.getAgeGroups();
						getDisplay().setAgeGroups(ageGroups);
						for (DOUGroups ageGroup : ageGroups) {
							ageGroupsMap.put(ageGroup.getId(), ageGroup);
						}
						
						Log.debug("setRegNums(" + ids + ")  +++>>>>" + result);

						List<RegisterLine> removeThem = new ArrayList<RegisterLine>();
						// Пере
						for (RegisterLine t : tableData) {
							if (!ids.contains(t.getDouRequest().getUId())) {
								removeThem.add(t);
							}
						}
						for (int i = 0; i < removeThem.size(); i++) {
							tableData.remove(removeThem.get(i));
						}

						ex: for (RegisterLine registerLine : result
								.getRegisterLines()) {
							for (RegisterLine t : tableData) {
								if (t.getDouRequest()
										.getUId()
										.equals(registerLine.getDouRequest()
												.getUId()))
									continue ex;
							}

							final SRegister sr = new SRegister();
							switch (registerLine.getDouRequest().getStatus()) {
							case WAIT:
								// Ставим действие по умолчанию
								sr.setRnAction(Action.GIVE_PERMIT);
								// Ставим ДОУ по умолчанию
								List<SDouRequest> reqSpec = registerLine
										.getSDouRequests();
								if (reqSpec != null && !reqSpec.isEmpty()) {
									sr.setDou((Dou) reqSpec.get(
											reqSpec.size() - 1)
											.getArticleUnit());
								}
								break;
							case DELETE_BY_7_YEAR:
							case DELETE_BY_PARRENTS_AGREE:
							case PERMIT:
								sr.setRnAction(Action.BACK_TO_QUEUE);
								sr.setDou(null);
								break;
							}
							sr.setDouRequest(registerLine.getDouRequest());
							// DEFAULT FOUNDATION
							sr.setFoundation(Foundation.DEFAULT);
							registerLine.setSRegister(sr);
							registerLine.setNewFlag(true);
							tableData.add(registerLine);
						}

						updateView();
						getDisplay().progressStop();
					}

					@Override
					public void onFailure(Throwable caught) {
						getDisplay().progressStop();
						handleException(caught);
					}
				});

	}

	public ChildCount getChildCount() {
		return new ChildCount(tableData);
	}

	@Override
	public List<Dou> getAllDou() {
		return allDou;
	}
	
	@Override
	public List<DOUGroups> getAgeGroups() {
		return ageGroups;
	}
	
	@Override
	public Map<Integer, DOUGroups> getAgeGroupsMap() {
		return ageGroupsMap;
	}

	@Override
	public HRegister getRegister() {
		if (register == null) {
			register = new HRegister();
			register.setRegisterDate(getDisplay().getRegisterDate().getValue());
			register.setStatus(RegisterStatus.FIRST);
		}
		return register;
	}

	@Override
	public List<RegisterLine> getTableData() {
		return tableData;
	}

	@Override
	public void goForward() {

		if (getRegister() == null || getRegister().getUId() == null) {
			getDisplay().showWarningNotification(
					"Необходимо сохранить протокол!");
			return;
		} else {
			if (getTableData().isEmpty()) {
				getDisplay().showWarningNotification(
						"Протокол не может быть пустой!");
			}

			populateRegisterFromFormFields();

			RegisterStatus status = getRegister().getStatus();
			RegisterStatus newStatus = null;
			switch (status) {
			case FIRST:
				newStatus = RegisterStatus.SECOND;
				break;
			case SECOND:

				for (RegisterLine line : tableData) {
					SRegister sRegister = line.getSRegister();
					switch (sRegister.getRnAction()) {
					case GIVE_PERMIT:
						if (sRegister.getDou() == null) {
							getDisplay().showWarningNotification(
									"При выдаче путевки необходимо заполнить поле ДОУ (РН "
											+ sRegister.getDouRequest() + ")");
							return;
						}
						break;

					}
				}

				newStatus = RegisterStatus.THIRD;
				break;
			case THIRD:
				return;
			}
			getDisplay().progressStart();
			registerService.changeStatus(getRegister(), tableData, newStatus,
					new AsyncCallback<SaveRegisterResult>() {

						@Override
						public void onSuccess(SaveRegisterResult result) {
							getDisplay().progressStop();
							if (Status.SUCCESS != result.getStatus()) {
								getDisplay().showWarningNotification(
										result.getComments());
							} else {
								getDisplay()
										.showInfoNotification(
												"Новый статус протокола: "
														+ result.getResult()
																.getStatus()
																.getLabel());

							}
							if (result.getResult() != null)
								register = result.getResult();
							if (result.getRegisterLines() != null
									&& !result.getRegisterLines().isEmpty()) {
								tableData.clear();
								tableData.addAll(result.getRegisterLines());
							}
							updateView();
						}

						@Override
						public void onFailure(Throwable caught) {
							getDisplay().progressStop();
							handleException(caught);
						}
					});
		}
	}

	@Override
	public void goBack() {
		RegisterStatus status = getRegister().getStatus();
		RegisterStatus newStatus = null;
		switch (status) {
		case FIRST:
			return;
		case SECOND:
			newStatus = RegisterStatus.FIRST;
			break;
		case THIRD:
			return;
		}
		getDisplay().progressStart();
		populateRegisterFromFormFields();
		registerService.changeStatus(getRegister(), tableData, newStatus,
				new AsyncCallback<SaveRegisterResult>() {

					@Override
					public void onSuccess(SaveRegisterResult result) {
						if (Status.SUCCESS != result.getStatus()) {
							getDisplay().showWarningNotification(
									result.getComments());
						} else {
							getDisplay().showInfoNotification(
									"Новый статус протокола: "
											+ result.getResult().getStatus()
													.getLabel());
						}

						if (result.getResult() != null){
							register = result.getResult();
						}
						tableData.clear();
						tableData.addAll(result.getRegisterLines());
						updateView();
						getDisplay().progressStop();
					}

					@Override
					public void onFailure(Throwable caught) {
						getDisplay().progressStop();
						handleException(caught);
					}
				});

	}

	
}