package org.itx.jbalance.equeue.gwt.client.services;

import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.shared.AddDouRequestOperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.CreateNewDouRequestRequest;
import org.itx.jbalance.equeue.gwt.shared.DouRequestDetailsDTO;
import org.itx.jbalance.equeue.gwt.shared.DouRequestListPrerequesedData;
import org.itx.jbalance.equeue.gwt.shared.GetReport5_1Request;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.Report5_1DTO;
import org.itx.jbalance.equeue.gwt.shared.Report5_2DTO;
import org.itx.jbalance.equeue.gwt.shared.Report5_3DTO;
import org.itx.jbalance.equeue.gwt.shared.UpdateDouRequestRequest;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface DouRequestsServiceAsync {

	void delete(Long id,
			AsyncCallback<OperationResultWrapper<HDouRequest>> callback);

	void search(DouRequestsSearchParams searchParams,
			AsyncCallback< DouRequestsSearchResultsWrapper> callback);

	void getDetails(Long id, AsyncCallback<DouRequestDetailsDTO> callback);

	void createNew(CreateNewDouRequestRequest createNewDouRequestRequest,
			AsyncCallback<AddDouRequestOperationResultWrapper> callback);

	void update(UpdateDouRequestRequest updateDouRequest,
			AsyncCallback<OperationResultWrapper<HDouRequest>> callback);

	void getReport5_1Data(GetReport5_1Request request ,AsyncCallback<List<Report5_1DTO>> callback);

	void getReport5_2Data(Report5_2DTO dto,AsyncCallback<Report5_2DTO> callback);

	void getReport5_3Data(Report5_3DTO dto,AsyncCallback<Report5_3DTO> callback);
	
	void updatePhoneNumber(Long uid, String newPhone, AsyncCallback<OperationResultWrapper<DouRequestDTO>> callback);

	void getDouRequestListPrerequesedData(
			AsyncCallback<DouRequestListPrerequesedData> callback);
	
	
	void getBadBdRecords(
			AsyncCallback<List<DouRequestDTO>> callback);

	void fixBadBdRecords(Map<Long, String> map, AsyncCallback<Void> callback);
}
