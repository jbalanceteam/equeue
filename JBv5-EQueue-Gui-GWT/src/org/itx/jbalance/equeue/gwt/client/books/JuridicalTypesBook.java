package org.itx.jbalance.equeue.gwt.client.books;

import java.util.ArrayList;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.MyDisplay;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.equeue.gwt.client.services.JuridicalTypesService;
import org.itx.jbalance.equeue.gwt.client.services.JuridicalTypesServiceAsync;
import org.itx.jbalance.l0.o.JuridicalType;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.VerticalPanel;


/**
 * Example file.
 */


public class JuridicalTypesBook extends ContentWidget implements MyDisplay{

	
	JuridicalTypesServiceAsync JuridicalTypesService = GWT.create(JuridicalTypesService.class);
	CellTable<JuridicalType> cellTable=new CellTable<JuridicalType>(0,MyCellTableResources.INSTANCE);
  public JuridicalTypesBook() {
	  super("Справочник типов МДОУ");

  }

 
  @Override
  public void initialize() {
    // Create a vertical panel to align the check boxes
    VerticalPanel vPanel = new VerticalPanel();
   
    Column <JuridicalType,String>idColumn =new Column<JuridicalType,String>(
	    	new TextCell()){
				@Override
				public String getValue(JuridicalType object) {
					return object.getUId()+"";
				}}; 
				
    cellTable.addColumn(idColumn, SafeHtmlUtils.fromSafeConstant("ID"));
    
    
    Column <JuridicalType,String>nameColumn =new Column<JuridicalType,String>(
	    	new TextCell()){
				@Override
				public String getValue(JuridicalType object) {
					return object.getName();
				}}; 
				
    cellTable.addColumn(nameColumn, SafeHtmlUtils.fromSafeConstant("Наименование"));
    
 
    
    initTable();
    vPanel.add(cellTable);
    
    initWidget(vPanel);
  }
  
  
  private void initTable() {
		JuridicalTypesService.getAll(new AsyncCallback<ArrayList<JuridicalType>>() {
			@Override
			public void onSuccess(ArrayList<JuridicalType> result) {
				cellTable.setRowData(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				showExceptionNotification(caught);	
			}
		});
		
	}

//  @Override
//  protected void asyncOnInitialize(final AsyncCallback<Widget> callback) {
//      callback.onSuccess(onInitialize());
//  }
}
