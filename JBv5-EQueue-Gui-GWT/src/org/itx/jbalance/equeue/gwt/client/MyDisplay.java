package org.itx.jbalance.equeue.gwt.client;

import com.google.gwt.user.client.ui.Widget;

public interface MyDisplay {
	Widget asWidget();
	/**
	 * Показывает user-friendly предепреждение о возникших ошибках.
	 */
	void showExceptionNotification(Throwable caught);
	void showWarningNotification(String message);
	void showInfoNotification(String message);
}
