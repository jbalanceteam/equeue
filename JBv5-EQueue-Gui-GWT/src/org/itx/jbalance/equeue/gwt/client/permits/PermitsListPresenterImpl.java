package org.itx.jbalance.equeue.gwt.client.permits;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.request.PrivilegeSearchItem;
import org.itx.jbalance.equeue.gwt.client.request.PrivilegeSearchItem.Type;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsService;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsServiceAsync;
import org.itx.jbalance.equeue.gwt.client.services.PermitsSearchResultsWrapper;
import org.itx.jbalance.equeue.gwt.client.services.RegisterService;
import org.itx.jbalance.equeue.gwt.client.services.RegisterServiceAsync;
import org.itx.jbalance.equeue.gwt.shared.DouRequestListPrerequesedData;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.IntegerFilter;
import org.itx.jbalance.l1.api.filters.LongFilter;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;


/**
 * #368 Выданные путевки
 * Контроллер(пресентор) для формы просмотра путевок.
 * @author apv
 */
public class PermitsListPresenterImpl extends MyPresenter<PermitsListWidget>
		implements PermitsListPresenter {


	
	private final RegisterServiceAsync service = GWT.create(RegisterService.class);
	private final DouRequestsServiceAsync douRequestsService = GWT.create(DouRequestsService.class);
//	private final ContragentsServiceAsync contragentsService= GWT.create(ContragentsService.class);
	
	
	GetPermitsRequest searchParams=new GetPermitsRequest();
	
	/**
	 * @param display
	 * @param state - Режим работы формы
	 */
	public PermitsListPresenterImpl(PermitsListWidget display) {
		super(display);
	}

	


	
	@Override
	public void initialize() {
		douRequestsService.getDouRequestListPrerequesedData(new AsyncCallback<DouRequestListPrerequesedData>() {
			
			@Override
			public void onSuccess(DouRequestListPrerequesedData result) {
				getDisplay().setDouList(result.getDous());
				Collections.sort(result.getPrivileges(), new Comparator<Privilege>() {
					@Override
					public int compare(Privilege o1, Privilege o2) {
						return o1.getName().compareToIgnoreCase(o2.getName());
					}
				});

					
				getDisplay().setPrivileges(result.getPrivileges());
				fillSearchForm();
				initListeners();
				search();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}

	private void fillSearchForm() {
		getDisplay().getSearchField().setText(searchParams.getSearchByAllTextFields());
		
		if(searchParams.getExact_privilege() != null){
			getDisplay().getPrivilege().setValue(new PrivilegeSearchItem(Type.EXACT_PRIVELEGE, searchParams.getExact_privilege().getFilterValue(), null));
		}else if(searchParams.getPrivilege() != null){
			if(searchParams.getPrivilege()){
				getDisplay().getPrivilege().setValue(PrivilegeSearchItem.YES_PRIVILEGE);
			} else {
				getDisplay().getPrivilege().setValue(PrivilegeSearchItem.NO_PRIVILEGE);
			}
		} else {
			getDisplay().getPrivilege().setValue(PrivilegeSearchItem.ANY);
		}
		

		if(searchParams.getBirthday() != null){
			getDisplay().getBirthdayFromDateBox().setValue(searchParams.getBirthday().getFilterValues().get(0));
			getDisplay().getBirthdayToDateBox().setValue(searchParams.getBirthday().getFilterValues().get(1));
		}
		
		
		if(searchParams.getPermitDate() != null){
			getDisplay().getPermitDateFrom().setValue(searchParams.getPermitDate().getFilterValues().get(0));
			getDisplay().getPermitDateTo().setValue(searchParams.getPermitDate().getFilterValues().get(1));
		}
		
	}



	private void initListeners() {}

	
	
	
	/**
	 * Подгрузка данных
	 */
	public void loadData(final int from,final  int to, List<SortedColumnInfo>sortInfo){
		searchParams.setFrom(from);
		searchParams.setTo(to);
		searchParams.setSortInfo(sortInfo);
		service.searchPermits(searchParams, new AsyncCallback< PermitsSearchResultsWrapper>() {
			@Override
			public void onSuccess( PermitsSearchResultsWrapper result) {
				getDisplay().updateRowData(from, result.getList());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}



	

	/**
	 * Запуск поиска
	 */
	public void search(){
		GetPermitsRequest params = getSearchParams();
		params.setFrom(0);
		params.setTo(PAGE_SIZE-1);
		service .searchPermits(params,new AsyncCallback<PermitsSearchResultsWrapper>() {
		
			final long startTime=System.currentTimeMillis();
			@Override
			public void onSuccess( PermitsSearchResultsWrapper result) {
				long searchEndedTime = System.currentTimeMillis();
				Log.debug("Search - "+(searchEndedTime-startTime)+" ms.");
				getDisplay().showDouRequests(result);
				Log.debug("Rendering - "+(System.currentTimeMillis()-searchEndedTime)+" ms.");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}
	
	
	@Override
	public GetPermitsRequest getSearchParams() {
		searchParams.setSearchByAllTextFields(getDisplay().getSearchField().getText());
		 
		switch(getDisplay().getPrivilege().getValue().getType()){
			case ANY: searchParams.setPrivilege(null); searchParams.setExact_privilege (null); break;
			case NO_PRIVILEGE: searchParams.setPrivilege(false); 	searchParams.setExact_privilege (null);  break;
			case YES_PRIVILEGE: searchParams.setPrivilege( true); 	searchParams.setExact_privilege (null); break;
			case EXACT_PRIVELEGE: searchParams.setPrivilege(null); 	searchParams.setExact_privilege (new LongFilter(getDisplay().getPrivilege().getValue().getExactPrivelageId()));
		}
		if(getDisplay().getSearchedDou() != null && !getDisplay().getSearchedDou().isEmpty()){
			searchParams.setDous(new LongFilter(getDisplay().getSearchedDou()));
		} else {
			searchParams.setDous(null);
		}
		
		
		searchParams.setBirthday(DateFilter.build( 
				getDisplay().getBirthdayFromDateBox().getValue(),
				getDisplay().getBirthdayToDateBox().getValue()));
		
		
		
		searchParams.setPermitDate(DateFilter.build( 
				getDisplay().getPermitDateFrom().getValue(),
				getDisplay().getPermitDateTo().getValue()));
		
		
		Integer ageFrom = null;
		try{
			ageFrom = new Integer(getDisplay().getAgeFrom().getText());
		}catch(NumberFormatException e){}
		
		Integer ageTo = null;
		try{
			ageTo = new Integer(getDisplay().getAgeTo().getText());
		}catch(NumberFormatException e){}
		
		searchParams.setAge(IntegerFilter.build(ageFrom, ageTo));
		
		return searchParams;
	}




	public void setSearchParams(GetPermitsRequest sp) {
		searchParams = sp;
	}

}

