package org.itx.jbalance.equeue.gwt.client.privileges;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyDisplay;
import org.itx.jbalance.l0.o.Privilege;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.HasText;

public interface PrivilegesPresenter {

	public interface Display extends MyDisplay{
		
		public HasText getPrevelegisName();

		public HasText getDescription();

		public void showPrivileges(List<Privilege> list);
		
		public HasClickHandlers getSaveClickHandlers();
	}
	
	void create();
	void delete(Long id);
	void update(Privilege privilege);
}
