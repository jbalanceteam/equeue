package org.itx.jbalance.equeue.gwt.client.help;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;

import com.google.gwt.user.client.ui.Frame;


public class UserGuide extends ContentWidget {
 
//	interface Binder extends UiBinder<Widget, UserGuide> {
//
//	}
	
	private static final String USER_GUIDE_PATH= "http://jbalance.org/";
	
  public UserGuide() {

  }

  /**
   * Initialize this example.
   */

  @Override
  public void initialize() {
	// Create the UiBinder.
//		Binder uiBinder = GWT.create(Binder.class);
//		if(this.getWidget() == null){
//			initWidget(uiBinder.createAndBindUi(this));
//			}
	  
	// Make a new frame, and point it at Google.

	    Frame frame = new Frame(USER_GUIDE_PATH);
	    frame.setWidth("100%");
	    frame.setHeight("500px");
	    
	    // Add it to the root panel.
	    initWidget(frame);
   

  }
}
