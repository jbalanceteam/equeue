package org.itx.jbalance.equeue.gwt.client.register;

import java.util.Date;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.RepeatableProgressBar;
import org.itx.jbalance.equeue.gwt.client.register.AutoRegisterEditPresenterImpl.TableLine;
import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l2_api.dto.RegionDTO;

import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

import eu.maydu.gwt.validation.client.DefaultValidationProcessor;
import eu.maydu.gwt.validation.client.ValidationProcessor;

public class AutoRegisterEditWidget extends ContentWidget implements AutoRegisterEditPresenter.Display {

	interface Binder extends UiBinder<Widget, AutoRegisterEditWidget> {

	}

	ValidationProcessor validator = new DefaultValidationProcessor();
	
	@UiField(provided=false)
	RadioButton wishesMethodRadioButton;
	
	@UiField(provided=false)
	RadioButton douFillRadioButton;
	
	@UiField(provided=false)
	CheckBox includeRefusesCheckBox;
	
	@UiField(provided=false)
	RadioButton  curDateRadioButton;
	
	@UiField(provided=false)
	RadioButton fstSpmRadioButton;
	
	@UiField(provided=false)
	TextBox privelegePartTextBox;
	
	@UiField(provided=false)
	RadioButton userSelectedRadioButton;
	
	@UiField(provided=false)
	ListBox regionsListBox;
	
	@UiField(provided=false)
	CellTable<TableLine> table;
	
	@UiField(provided = false)
	DateBox ageCalculationDatePanel;
	
	@UiField(provided = false)
	Button saveButton;
	
	@UiField(provided = false)
	Button clearButton;
	
	AutoRegisterEditPresenter presenter;
	
	RepeatableProgressBar progressBar;
	
	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public AutoRegisterEditWidget() {
		super();
	}
	
	@Override
	public void initialize() {
		if(this.getWidget() == null){
//			requestsTable=new CellTable<RegisterLine>(0,MyCellTableResources.INSTANCE);
			Binder uiBinder = GWT.create(Binder.class);
			initWidget(uiBinder.createAndBindUi(this));
			
			privelegePartTextBox.setValue(HAutoRegister.DAFAULT_PRIVELEGE_PART + "");

			wishesMethodRadioButton.setValue(true);
			includeRefusesCheckBox.setValue(true);
			
			   // формат вывода датапикера
		     DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd.MM.yyyy");
			 DateBox.Format format=new DateBox.DefaultFormat(dateTimeFormat); 
			 ageCalculationDatePanel.setFormat(format);
			
			curDateRadioButton.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					ageCalculationDatePanel.setEnabled(false);
					ageCalculationDatePanel.setValue(new Date());
				}
			});
			
			fstSpmRadioButton.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					ageCalculationDatePanel.setEnabled(false);
					Date date = new Date();
					if(date.getMonth() > 8){
						date.setYear(date.getYear() + 1);
						date.setMonth(8);
						date.setDate(1);
					} else {
						date.setMonth(8);
						date.setDate(1);
					}
					ageCalculationDatePanel.setValue(date);
				}
			});
			
			userSelectedRadioButton.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
				@Override
				public void onValueChange(ValueChangeEvent<Boolean> event) {
					ageCalculationDatePanel.setEnabled(true);
				}
			});
			curDateRadioButton.setValue(true, true);
			
			regionsListBox.addChangeHandler(new ChangeHandler() {
				@Override
				public void onChange(ChangeEvent event) {
					RegionDTO selectedRegion = getSelectedRegion();
					table.setRowData(getPresenter().getTableData(selectedRegion));
				}
			});
			
		    initValidation();
		}
	}
	
	
	public RegionDTO getSelectedRegion(){
		String value = regionsListBox.getValue(regionsListBox.getSelectedIndex());
		RegionDTO selectedRegion = null;
		if(value .equals("all")){
			selectedRegion = null;
		}else{
			
			for(RegionDTO r: getPresenter().getRegions()){
				if(value.equals(r.getUId().toString())){
					selectedRegion = r;
				}
			}
			if(selectedRegion == null){
				throw new RuntimeException("How did this happen?");
			}
		}
		return selectedRegion;
	}
	

	

	private void initValidation() {
//		validator = new DefaultValidationProcessor();
////		ValidationMessages messages = new ValidationMessages();
//		
////		popupDesc = new PopupDescription(messages);
//
//		validator.addValidators("dousEmpty", 
//				new   Validator<Validator>(){
//
//					@Override
//					public ValidationResult validate(ValidationMessages messages) {
////						if(requestsTable.getRowCount()<=0){
////							return new ValidationResult("Список пуст, добавите очередника");
////						}
//						return null;
//					}
//
//					@Override
//					public void invokeActions(ValidationResult result) {
////						new StyleAction("validationFailedBorder").invoke(result, choosedDouList);
//						if(result!=null){
//							ValidationError validationError = result.getErrors().get(0);
//							showWarningNotification(""+validationError.error);
//						}
//					}}
////					.addActionForFailure(focusAction)
//					
//		);
	}


	public AutoRegisterEditPresenter getPresenter() {
		return presenter;
	}


	public void setPresenter(AutoRegisterEditPresenter presenter) {
		this.presenter=presenter;
	}

	@UiHandler("clearButton")
	public void clearButton(ClickEvent event){
		wishesMethodRadioButton.setValue(true);
		includeRefusesCheckBox.setValue(true);
		curDateRadioButton.setValue(true, true);
		privelegePartTextBox.setValue(HAutoRegister.DAFAULT_PRIVELEGE_PART + "");
		getPresenter().clear();
		regionsListBox.setSelectedIndex(0);
		table.setRowData(getPresenter().getTableData(null));
		table.redraw();
	}
	

	public void setEnabledUI(boolean v){
		saveButton.setEnabled(v);
		clearButton.setEnabled(v);
		if(! v){
			showWaitCursor();
		} else {
			showDefaultCursor();
		}
	}
	
	@UiHandler("saveButton")
	public void save(ClickEvent event){
		boolean validate = validator.validate();
		if(validate){
			presenter.save(null);
		}
	}
	

	public void populateForm(List<RegionDTO> regions, List<TableLine> tableLines, List<DOUGroups> ages) {
		regionsListBox.addItem("Все", "all");
		for (RegionDTO region : regions) {
			regionsListBox.addItem(region.getName(), region.getUId().toString());
		}
		

		
		/************** ДОУ ****************/
		Column<TableLine, String> douColumn = new Column<TableLine, String>(
				new TextCell()) {
			@Override
			public String getValue(TableLine object) {
				if(object.isSum){
					return "СУММА";
				}
				return object.dou.getName();
			}
		};
		table.addColumn(douColumn, SafeHtmlUtils.fromSafeConstant("МДОУ"));
		/************** ДОУ ****************/
		
		
		/************** КОЛ-ВО МЕСТ ****************/
		for (final DOUGroups age : ages) {
			final EditTextCell editTextCell = new EditTextCell();
			Column<TableLine, String> ageColumn = new Column<TableLine, String>(
					editTextCell) {
				@Override
				public String getValue(TableLine object) {
					Integer integer = object.groups.get(age.getId());
					if(integer != null && integer > 0){
						return integer + "";
					} else {
						return "";
					}
				}
			};
			
			ageColumn.setFieldUpdater(new FieldUpdater<AutoRegisterEditPresenterImpl.TableLine, String>() {
				
				@Override
				public void update(int index, TableLine object, String value) {
					try{
						Integer newVal = new Integer(value);
						object.groups.put(age.getId(), newVal);
					} catch(NumberFormatException e){
						editTextCell.clearViewData(object);
//						table.setRowData(getPresenter().getTableData());
					}
					getPresenter().recalculateSum();
					table.redraw();
				}
			});

			table.addColumn(ageColumn, SafeHtmlUtils.fromSafeConstant(age.getFromToStr()));
		}
		/************** КОЛ-ВО МЕСТ ****************/
		
		
		/************** Сумма для сада ****************/
		Column<TableLine, String> sumColumn = new Column<TableLine, String>(
				new TextCell()) {
			@Override
			public String getValue(TableLine object) {
				return object.getSum() + "";
			}
		};
		table.addColumn(sumColumn, SafeHtmlUtils.fromSafeConstant("Всего"));
		/************** Сумма для сада ****************/
		
		table.setRowData(tableLines);

	}
	
	public HasValue<Boolean>getIncludeRefuses(){
		return includeRefusesCheckBox;
	}
	
	public HasValue<Boolean>getWishesMethod(){
		return wishesMethodRadioButton;
	}
	
	public HasValue<Boolean>getDouFillMethod(){
		return douFillRadioButton;
	}
	
	public HasValue<String> getPrivelegePart(){
		return privelegePartTextBox;
		
	}
	
	public HasValue<Date> getCalculationDate(){
		return ageCalculationDatePanel;
		
	}
	
}
