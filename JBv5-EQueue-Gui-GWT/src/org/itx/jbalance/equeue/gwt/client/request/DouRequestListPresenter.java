package org.itx.jbalance.equeue.gwt.client.request;

import java.util.List;
import java.util.Set;

import org.itx.jbalance.equeue.gwt.client.MyDisplay;
import org.itx.jbalance.equeue.gwt.client.register.RegisterEditPresenter;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;

public interface DouRequestListPresenter {

	public final static int PAGE_SIZE=50;
	
	public interface Display extends MyDisplay{
//		
//		public HasText getPrevelegisName();
//
//		public HasText getDescription();
//
//		public void showDouRequests(SearchResultWrapper<HDouRequest> res);

		
//		
//		public HasClickHandlers getSaveClickHandlers();
	}
	
	void delete(Long id);
	void update(Privilege privilege);
	void initialize();
	/**
	 * #354 Редактирование номера телефона из строки в списке очередников
	 */
	void updatePhone(Long uid, String phone);
	
	/**
	 * Run search and populate requests table on the View
	 * @param end 
	 * @param start 
	 */
	void loadData(int start, int end,List<SortedColumnInfo>sortInfo);
	void populateDetails(DouRequestDTO selectedRequest);
	void search();
	DouRequestsSearchParams getSearchParams();
	State getState();
	void select(Long uid);
	boolean isSelected(Long uid);
	void deselect(Long uid);
	Set<Long> getSelectedNums();
	Set<Long> getPrewSelectedNums();
	
	/**
	 * В режиме выбора номеров, мы храним этот презентер
	 * @return
	 */
	public RegisterEditPresenter getRegisterPresenter( );
//	void generateRegister();
}
