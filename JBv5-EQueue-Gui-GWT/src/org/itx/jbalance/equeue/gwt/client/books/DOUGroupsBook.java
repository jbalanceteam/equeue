package org.itx.jbalance.equeue.gwt.client.books;

import java.util.ArrayList;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.equeue.gwt.client.services.DOUGroupsService;
import org.itx.jbalance.equeue.gwt.client.services.DOUGroupsServiceAsync;
import org.itx.jbalance.l0.o.DOUGroups;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.VerticalPanel;


public class DOUGroupsBook extends ContentWidget {
	private final DOUGroupsServiceAsync douGroupsService = GWT.create(DOUGroupsService.class);
	CellTable<DOUGroups> cellTable=new CellTable<DOUGroups>(0,MyCellTableResources.INSTANCE);
	
	/**
	 * Constructor
	 * @param constants the constants
	 */
	public DOUGroupsBook() {
		  
	}

	/**
	 * Initialize this example.
	 */
	@Override
	public void initialize() {
	    // Create a vertical panel to align the check boxes
	    VerticalPanel vPanel = new VerticalPanel();
	    Column <DOUGroups,String>idColumn =new Column<DOUGroups,String>(
		    	new TextCell()){
					@Override
					public String getValue(DOUGroups object) {
						return object.getId()+"";
					}}; 
					
	    cellTable.addColumn(idColumn, SafeHtmlUtils.fromSafeConstant("ID"));

	    Column <DOUGroups,String>nameColumn =new Column<DOUGroups,String>(
	    	new TextCell()){
				@Override
				public String getValue(DOUGroups object) {
					return object.getName();
				}}; 
				
				cellTable.addColumn(nameColumn, SafeHtmlUtils.fromSafeConstant("Наименование"));
    
    
				Column <DOUGroups,String>ageFromColumn =new Column<DOUGroups,String>(
		    	new TextCell()){
					@Override
					public String getValue(DOUGroups g) {
						if(g.getAgeFrom()==null)
							return "";
						return (g.getAgeFrom() / 12 ) +"."+(g.getAgeFrom() % 12 ); 
					}
				}; 
				
				cellTable.addColumn(ageFromColumn, SafeHtmlUtils.fromSafeConstant("От"));
    
    
				Column <DOUGroups,String>ageToColumn =new Column<DOUGroups,String>(
						new TextCell()){
					@Override
					public String getValue(DOUGroups g) {
						if(g.getAgeTo()==null)
							return "";
						return (g.getAgeTo() / 12 ) +"."+(g.getAgeTo() % 12 );
					}
				}; 
		cellTable.addColumn(ageToColumn, SafeHtmlUtils.fromSafeConstant("До"));
	    initTable();
	    vPanel.add(cellTable);
	    initWidget(vPanel);
  }

  
  private void initTable() {
		douGroupsService.getAll(new AsyncCallback<ArrayList<DOUGroups>>() {
			@Override
			public void onSuccess(ArrayList<DOUGroups> result) {
				cellTable.setRowData(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				showExceptionNotification(caught);	
			}
		});
		
	}

}
