package org.itx.jbalance.equeue.gwt.client.reports;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.RepeatableProgressBar;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.equeue.gwt.shared.Report5_3DTO;
import org.itx.jbalance.equeue.gwt.shared.ReportsAge;
import org.itx.jbalance.equeue.gwt.shared.ReportsDou;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Reporta5_3Widget extends ContentWidget implements Reporta5_1Presenter.Display {

	interface Binder extends UiBinder<Widget, Reporta5_3Widget> {
	}

	@UiField(provided=true)
	CellTable<ReportsDou> table=new CellTable<ReportsDou>(0,MyCellTableResources.INSTANCE);
	
	@UiField
	SimplePanel progressPanel;
	
	RepeatableProgressBar progressBar;
	
	@Override
	public void initialize() {
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		progressBar = new RepeatableProgressBar(1, 4000);
		progressPanel.add(progressBar);
	}

	public void initTableColumns(List<ReportsAge> list){
		/* МДОУ */
		Column<ReportsDou, String> ageColumn = new Column<ReportsDou, String>(
				new TextCell()) {
			@Override
			public String getValue(ReportsDou o) {
				if("ИТОГО".equals(o.getDou().getName()))
					return "ИТОГО";
				return o.getDou().getName()+" ("+o.getDou().getNumber()+")";
			}
		};

		table.addColumn(ageColumn, SafeHtmlUtils.fromSafeConstant("МДОУ"));
//		table.setColumnWidth(ageColumn,"10px");
		

		for (final ReportsAge a : list) {
			Column<ReportsDou, String> countColumn = new Column<ReportsDou, String>(
					new TextCell()) {
				@Override
				public String getValue(ReportsDou o) {
					
					return o.getCount().get(a)+"";
				}
			};
			 Integer from = a.getFrom();
			 Integer to = a.getTo();
			table.addColumn(countColumn, SafeHtmlUtils.fromSafeConstant(from/12+"."+from%12 +" - "+to/12+"."+to%12));
		}
	}
	
	public void setReportData(Report5_3DTO data){
		table.setRowData(data.getRows());
		progressBar.stop();
		progressBar = null;
		progressPanel.clear();
	}
}
