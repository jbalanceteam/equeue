package org.itx.jbalance.equeue.gwt.client.request.bdIssue;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;

public class BdIssueWidget extends ContentWidget implements BdIssuePresenter.Display {

	interface Binder extends UiBinder<Widget, BdIssueWidget> {
	}

	@UiField(provided=true)
	CellTable<DouRequestDTO> table=new CellTable<DouRequestDTO>();
	
	@UiField
	Button fixButton;
	
	private BdIssuePresenter presenter;
	
	@Override
	public void initialize() {
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		initTableColumns();
	}

	private void initTableColumns(){
		/* Age */
		Column<DouRequestDTO, String> rnColumn = new Column<DouRequestDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(DouRequestDTO o) {
				return o.getRegNumber();
			}
		};
		table.addColumn(rnColumn, SafeHtmlUtils.fromSafeConstant("РН"));
		
		
		
		Column<DouRequestDTO, String> serverBdColumn = new Column<DouRequestDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(DouRequestDTO o) {
				return o.getBirthdayFormated();
			}
		};
		table.addColumn(serverBdColumn, SafeHtmlUtils.fromSafeConstant("Серверная дата рождения"));
		
		
		Column<DouRequestDTO, String> clientBdColumn = new Column<DouRequestDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(DouRequestDTO o) {
				return o.getBirthday() + "";
			}
		};
		table.addColumn(clientBdColumn, SafeHtmlUtils.fromSafeConstant("Клиентская дата рождения"));
		
	}
	
	@UiHandler("fixButton")
	public void fix(ClickEvent event){
		presenter.fixBirthdays();
	}
	 
	public BdIssuePresenter getPresenter() {
		return presenter;
	}

	public void setPresenter(BdIssuePresenter presenter) {
		this.presenter = presenter;
	}

	public void setReportData(List<DouRequestDTO>data){
		table.setRowData(data);
	}
	
}
