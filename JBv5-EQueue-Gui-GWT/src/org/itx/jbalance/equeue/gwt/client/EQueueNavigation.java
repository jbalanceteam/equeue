package org.itx.jbalance.equeue.gwt.client;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.itx.jbalance.equeue.gwt.client.books.DOUGroupsBook;
import org.itx.jbalance.equeue.gwt.client.books.FoundatinsBook;
import org.itx.jbalance.equeue.gwt.client.books.JuridicalTypesBook;
import org.itx.jbalance.equeue.gwt.client.charts.Chart1;
import org.itx.jbalance.equeue.gwt.client.contragents.ContragentEditPresenterImpl;
import org.itx.jbalance.equeue.gwt.client.contragents.ContragentEditWidget;
import org.itx.jbalance.equeue.gwt.client.contragents.ContragentListPresenterImpl;
import org.itx.jbalance.equeue.gwt.client.contragents.ContragentListWidget;
import org.itx.jbalance.equeue.gwt.client.help.About;
import org.itx.jbalance.equeue.gwt.client.permits.PermitsListPresenterImpl;
import org.itx.jbalance.equeue.gwt.client.permits.PermitsListWidget;
import org.itx.jbalance.equeue.gwt.client.privileges.PrivilegesPresenterImpl;
import org.itx.jbalance.equeue.gwt.client.privileges.PrivilegesWidget;
import org.itx.jbalance.equeue.gwt.client.regions.RegionsPresenterImpl;
import org.itx.jbalance.equeue.gwt.client.regions.RegionsWidget;
import org.itx.jbalance.equeue.gwt.client.register.AutoRegisterEditPresenter;
import org.itx.jbalance.equeue.gwt.client.register.AutoRegisterEditPresenterImpl;
import org.itx.jbalance.equeue.gwt.client.register.AutoRegisterEditWidget;
import org.itx.jbalance.equeue.gwt.client.register.RegisterEditPresenter;
import org.itx.jbalance.equeue.gwt.client.register.RegisterEditPresenterImpl;
import org.itx.jbalance.equeue.gwt.client.register.RegisterEditWidget;
import org.itx.jbalance.equeue.gwt.client.register.RegisterListPresenterImpl;
import org.itx.jbalance.equeue.gwt.client.register.RegisterListWidget;
import org.itx.jbalance.equeue.gwt.client.reports.Reporta5_1PresenterImpl;
import org.itx.jbalance.equeue.gwt.client.reports.Reporta5_1Widget;
import org.itx.jbalance.equeue.gwt.client.reports.Reporta5_2PresenterImpl;
import org.itx.jbalance.equeue.gwt.client.reports.Reporta5_2Widget;
import org.itx.jbalance.equeue.gwt.client.reports.Reporta5_3PresenterImpl;
import org.itx.jbalance.equeue.gwt.client.reports.Reporta5_3Widget;
import org.itx.jbalance.equeue.gwt.client.reports.Reporta5_4PresenterImpl;
import org.itx.jbalance.equeue.gwt.client.reports.Reporta5_4Widget;
import org.itx.jbalance.equeue.gwt.client.reports.Reporta5_5PresenterImpl;
import org.itx.jbalance.equeue.gwt.client.reports.Reporta5_5Widget;
import org.itx.jbalance.equeue.gwt.client.request.DouRequestEditPresenter;
import org.itx.jbalance.equeue.gwt.client.request.DouRequestEditPresenterImpl;
import org.itx.jbalance.equeue.gwt.client.request.DouRequestEditWidget;
import org.itx.jbalance.equeue.gwt.client.request.DouRequestListPresenterImpl;
import org.itx.jbalance.equeue.gwt.client.request.DouRequestListWidget;
import org.itx.jbalance.equeue.gwt.client.request.State;
import org.itx.jbalance.equeue.gwt.client.request.bdIssue.BdIssuePresenterImpl;
import org.itx.jbalance.equeue.gwt.client.request.bdIssue.BdIssueWidget;
import org.itx.jbalance.equeue.gwt.client.services.LoginService;
import org.itx.jbalance.equeue.gwt.client.services.LoginServiceAsync;
import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;


/**
 * This class contains set of methods for navigation.
 * 
 * @author apv
 *
 */
public class EQueueNavigation {
 
	static final String URL_REGISTER_EDIT =    "RegisterEdit";
	static final String URL_REGISTER_AUTO =    "RegisterAuto";
	static final String URL_REGISTER_LIST =    "RegisterList";
	static final String URL_CONTRACTORS_NEW =  "ContractorsNew";
	static final String URL_CONTRACTORS_EDIT = "ContractorsEdit";
	static final String URL_CONTRACTORS_LIST = "ContractorsList";
	static final String URL_DOU_REQUEST_EDIT = "QueueEdit";
	static final String URL_DOU_REQUESTS_LIST= "QueueList";
	static final String URL_DOU_REQUESTS_IMPORT= "QueueImport";
	static final String URL_PERMITS_LIST	=  "PermitsList";
	
	
	static final String URL_PRIVILEGES_LIST= 		"PrivilegesList";
	static final String URL_REGIONS_LIST= 			"RegionsList";
	static final String URL_JURIDICAL_TYPES_LIST=	"DouKindsList";
	static final String URL_DOU_GROUPS_LIST=		"DouGroupsList";
	static final String URL_FOUNDATIONS_LIST=		"FoundationsList";
	
	static final String REPORT5_1=		"Report5_1";
	static final String REPORT5_2=		"Report5_2";
	static final String REPORT5_3=		"Report5_3";
	static final String REPORT5_4=		"Report5_4";
	static final String REPORT5_5=		"Report5_5";
	
	static final String CHART1=		"Chart1";
	
	static final String BAD_BD_ISSUE=		"BadBdIssue";
	
//	static final String URL_USER_GUIDE=		"UserGuide";
	static final String URL_ABOUT=		"About";
	static final String URL_SITE=		"Site";
	static final String URL_DOU_DIET=		"DouDiet";
//	static final String URL_WIKI=		"Wiki";
//	static final String URL_REGISTRATION =		"Reg";
//	static final String URL_FORUM=		"Forum";
//	static final String URL_FAQ=		"Faq";
	
	static final String EXTERNAL_URL_SITE = " http://jbalance.org/software_mdou";
//	static final String EXTERNAL_URL_WIKI = "http://jbalance.org/portal/equeue/wiki?initialURI=/portal/equeue/wiki";
//	static final String EXTERNAL_URL_FORUM = "http://jbalance.org/portal/equeue/forum/";
//	static final String EXTERNAL_URL_FAQ = "http://jbalance.org/portal/equeue/faq/";
//	static final String EXTERNAL_URL_REGISTRATION = "http://jbalance.org/portal/equeue/newAccount";
	static final String EXTERNAL_URL_DOU_DIET = " http://jbalance.org/diet_balance_3";
	
//	сайт http://jbalance.org/portal/equeue/
//		Вики учебник http://jbalance.org/portal/equeue/wiki?initialURI=/portal/equeue/wiki
//		Форум - http://jbalance.org/portal/equeue/forum
//		Вопросы - http://jbalance.org/portal/equeue/faq

	
	static final String URL_LOGOUT =		"Logout";
	
	/**
	 * singletone
	 */
	private static EQueueNavigation instance=new EQueueNavigation();
	
	private final LoginServiceAsync loginService = GWT
	.create(LoginService.class);
	
	/**
	 * Get instance
	 * @return
	 */
	public static synchronized EQueueNavigation instance(){
		return instance;
	}
	
	
	
	/**
	 * Класс для хранения информации о пункте меню:
	 * Надпись, 
	 * Действие, выполняемое по нажатию
	 * URL
	 * 
	 * 
	 * @author apv
	 *
	 */
	public class EQueueGoToAction {

		public EQueueGoToAction(Command action, String url) {
			super();
//			this.label = label;
			this.action = action;
			this.url = url;
		}

//		public String label;
		final public Command action;
		final public String url;
	}
	
	Map<String,CommandImpl> menuItems = new HashMap<String,CommandImpl>();
	
	
	
	
	
	/**
	 * Closed constructor
	 */
	private EQueueNavigation(){	
		
		menuItems.put(URL_REGISTER_AUTO,
				new CommandImpl() {
			@Override
			public void execute(Map<String, Serializable> params) {
				goToAutoRegisterPage(params);
			}
		});
		
		menuItems.put(URL_REGISTER_EDIT,
			new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
					goToRegisterEditPage(params);
				}
			});
		
		
		
		menuItems.put(URL_REGISTER_LIST,
				new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToRegistersListPage(params);
					}
				});
		

		menuItems.put(URL_CONTRACTORS_NEW,
			new CommandImpl() {
			@Override
			public void execute(Map<String, Serializable> params) {
					goToContractorNewPage(params);
				}
			});
			
		menuItems.put(URL_FOUNDATIONS_LIST,
				new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToFoundationsListPage(params);
					}
				});
		
			
		menuItems.put(URL_CONTRACTORS_LIST,
			new CommandImpl() {
			@Override
			public void execute(Map<String, Serializable> params) {
					goToContractorsListPage(params);
				}
			});

		
		menuItems.put(URL_JURIDICAL_TYPES_LIST,
			new CommandImpl() {
			@Override
			public void execute(Map<String, Serializable> params) {
					goToJuridicalTypesListPage(params);
				}
			});

		menuItems.put(URL_DOU_GROUPS_LIST,
			new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToDouGroupsListPage(params);
					}
				});
		
		

			
		menuItems.put(URL_DOU_REQUESTS_LIST,
			new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
					goToDouRequestsListPage(params);					
				}
			});
		
		menuItems.put(URL_DOU_REQUESTS_IMPORT,
				new CommandImpl() {
					@Override
					public void execute(Map<String, Serializable> params) {
						goToDouRequestsImportPage(params);					
					}
				});
		
		menuItems.put(URL_PERMITS_LIST,
				new CommandImpl() {
					@Override
					public void execute(Map<String, Serializable> params) {
						goToPermitsListPage(params);					
					}
				});
		
			
		menuItems.put(URL_DOU_REQUEST_EDIT, 
			new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
					goToDouRequestEditPage(params);
				}
			});
			
		

			
		menuItems.put(URL_PRIVILEGES_LIST,
			new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
					goToPrivilegesListPage(params);
				}
			});
		
		menuItems.put(URL_REGIONS_LIST,
			new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
					goToRegionsListPage(null);
			}
		});
		
		
		menuItems.put(REPORT5_1,
				new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToReport5_1Page(null);
					}
				});
		
		
		menuItems.put(REPORT5_2,
				new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToReport5_2Page(null);
					}
				});
			
		
		menuItems.put(REPORT5_3,
				new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToReport5_3Page(null);
					}
				});
		
		
		menuItems.put(REPORT5_4,
				new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToReport5_4Page(null);
					}
				});
		
		
		menuItems.put(REPORT5_5,
				new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToReport5_5Page(null);
					}
				});
		
		
		menuItems.put(BAD_BD_ISSUE,
				new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToBdIssuePage(null);
					}
				});
		
		menuItems.put(CHART1,
				new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToChart1Page(null);
					}
				});
		
		menuItems.put(URL_ABOUT,
				new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToAbout(null);
					}
				});
		

//		menuItems.put(URL_USER_GUIDE,
//				new Command() {
//					@Override
//					public void execute() {
//						goToUserGuide(null);
//					}
//				});

		menuItems.put(URL_ABOUT,
				new CommandImpl() {
				@Override
				public void execute(Map<String, Serializable> params) {
						goToAbout(null);
					}
				});
//		menuItems.put(URL_FAQ,	new GoToURLCommang(EXTERNAL_URL_FAQ));
//		menuItems.put(URL_FORUM ,new GoToURLCommang(EXTERNAL_URL_FORUM));
		menuItems.put(URL_SITE,new GoToURLCommang(EXTERNAL_URL_SITE));
//		menuItems.put(URL_WIKI,new GoToURLCommang(EXTERNAL_URL_WIKI));
//		menuItems.put(URL_REGISTRATION,new GoToURLCommang(EXTERNAL_URL_REGISTRATION));
		menuItems.put(URL_DOU_DIET, new GoToURLCommang(EXTERNAL_URL_DOU_DIET));
		
		menuItems.put(URL_LOGOUT, new CommandImpl() {
			
			@Override
			public void execute(Map<String, Serializable> params) {
				logout();
			}

			
		});
		
		}
	
	
	
	CommandImpl getMenuItemByUrl(String url) {
		return menuItems.get(url);
	}
	
	private void logout() {
		if(Window.confirm("Выйти из системы?"))
		loginService.closeSession(new AsyncCallback<Void>() {
			@Override
			public void onSuccess(Void result) {
				
				Window.alert("Вы вышли из системы.");
				String url = Window.Location.getProtocol()+"//"+Window.Location.getHost()+Window.Location.getPath()+Window.Location.getQueryString();
				RequestBuilder builder=new RequestBuilder(RequestBuilder.GET,url);
				builder.setUser("invalid");
				builder.setPassword("invalid");
				builder.setCallback(new RequestCallback() {
					
					@Override
					public void onResponseReceived(Request request, Response response) {
						Window.Location.reload();
					}
					
					@Override
					public void onError(Request request, Throwable exception) {
						Window.Location.reload();
					}
				});
				try {
					builder.send();
				} catch (RequestException e) {
					Log.error("", e);
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				Log.error("", caught);
			}
		});
	}
	
	public void goToRegisterEditPage(Map<String,Serializable> params){
		if(params==null)
			params=Collections.emptyMap();

		

		final RegisterEditPresenter presenter;
		if(params.containsKey("registerEditPresenter")){
			presenter=(RegisterEditPresenter) params.get("registerEditPresenter");
			EQueueShell.instance(). setContent((ContentWidget) ((MyPresenter)presenter).getDisplay());
			
			presenter.setRegNums((Set<Long>) params.get("regNums"));
		
		}else{
			final RegisterEditWidget display;
			display = new RegisterEditWidget();
			presenter = new RegisterEditPresenterImpl(display);
			display.setPresenter(presenter);
			EQueueShell.instance(). setContent(display);
			presenter.initialize();
			
			if(params.containsKey("objectUId")){
				Serializable objectUId = params.get("objectUId");
				if(objectUId instanceof Long){
					presenter.setObject((Long) objectUId);
				} else {
					presenter.setObject(new Long(String.valueOf(objectUId)));
				}
				History.newItem(URL_REGISTER_EDIT+"?objectUId="+objectUId, false);
			} else {
				History.newItem(URL_REGISTER_EDIT, false);
			}
			((RegisterEditPresenterImpl)presenter).updateView();
		}
	}
	
	
	public void goToAutoRegisterPage(Map<String,Serializable> params){
		if(params==null)
			params=Collections.emptyMap();

		final AutoRegisterEditPresenter presenter;
		final AutoRegisterEditWidget display;
		display = new AutoRegisterEditWidget();
		presenter = new AutoRegisterEditPresenterImpl(display);
		display.setPresenter(presenter);
		EQueueShell.instance(). setContent(display);
		presenter.initialize();
		History.newItem(URL_REGISTER_AUTO, false);	
////			if(params.containsKey("objectUId")){
////				Serializable objectUId = params.get("objectUId");
////				if(objectUId instanceof Long){
////					presenter.setObject((Long) objectUId);
////				} else {
////					presenter.setObject(new Long(String.valueOf(objectUId)));
////				}
////				History.newItem(URL_REGISTER_EDIT+"?objectUId="+objectUId, false);
////			} else {
////				History.newItem(URL_REGISTER_EDIT, false);
////			}
////			((RegisterEditPresenterImpl)presenter).updateView();
//		}
	}
	
	
	public void goToRegistersListPage(Map<String,Serializable> params){
		if(params==null)
			params=Collections.emptyMap();
		
		RegisterListWidget display = new RegisterListWidget();
//		display.setShell(EQueueShell.this);
		RegisterListPresenterImpl presenter = new RegisterListPresenterImpl(display);
		display.setPresenter(presenter);
		EQueueShell.instance().setContent(display);
		presenter.initialize();
		History.newItem(URL_REGISTER_LIST, false);
	}
	
	
	public void goToContractorNewPage(Map<String,Serializable> params){
		if(params==null)
			params=Collections.emptyMap();
		
		ContragentEditWidget display = new ContragentEditWidget();
		ContragentEditPresenterImpl presenter = new ContragentEditPresenterImpl(
				display);
		display.setPresenter(presenter);
		presenter.initialize();
		EQueueShell.instance().setContent(display);
		History.newItem(URL_CONTRACTORS_NEW, false);
		
	}
	public void goToContractorEditPage(Map<String,Serializable> params){
		if(params==null)
			params=Collections.emptyMap();
		ContragentEditWidget display = new ContragentEditWidget();
		ContragentEditPresenterImpl presenter = new ContragentEditPresenterImpl(display);
		display.setPresenter(presenter);
		//EQueueShell.instance().setContent((ContentWidget) ((MyPresenter)presenter).getDisplay());
		EQueueShell.instance().setContent(display);
		History.newItem(URL_CONTRACTORS_EDIT, false);
		org.itx.jbalance.equeue.gwt.client.contragents.State state = (org.itx.jbalance.equeue.gwt.client.contragents.State)params.get("state");
		if(state==org.itx.jbalance.equeue.gwt.client.contragents.State.EDIT_CONTRAGENT){
			presenter.setState(state);
			presenter.setContragent((Juridical)params.get("juridical"));
		}else if (state == org.itx.jbalance.equeue.gwt.client.contragents.State.EDIT_DOU) {
			presenter.setState(state);
			presenter.setDou((HStaffDivision)params.get("dou"));
		}
	}
	
	
	public void goToPermitsListPage(Map<String,Serializable> params) {
		if(params==null){
			params=Collections.emptyMap();
		}
		PermitsListWidget display = new PermitsListWidget();
		
		PermitsListPresenterImpl presenter= new PermitsListPresenterImpl(display);
		
		display.setPresenter(presenter);
		EQueueShell.instance(). setContent(display);
		presenter.initialize();
		History.newItem(URL_PERMITS_LIST, false);
	}
	
	public void goToDouRequestsListPage(Map<String,Serializable> params) {
		if(params==null)
			params=Collections.emptyMap();
		DouRequestListWidget display = new DouRequestListWidget();

		State state;
		if(params.containsKey("state")){
			state=(State) params.get("state");
		}else{
			state=State.VIEW_REQUESTS;
		}
		DouRequestListPresenterImpl presenter= new DouRequestListPresenterImpl(
				display,state);
		
		if(params.containsKey("registerEditPresenter")){
			presenter.setRegisterPresenter((RegisterEditPresenter) params.get("registerEditPresenter"));
		}
		
		if(params.containsKey("searchParams")){
			DouRequestsSearchParams searchParams =  (DouRequestsSearchParams) params.get("searchParams");
			Log.debug("searchedDous: "+searchParams.searchedDous);
			presenter.setSearchParams(searchParams);
		}
		
		display.setPresenter(presenter);
		EQueueShell.instance(). setContent(display);
		presenter.initialize();
		History.newItem(URL_DOU_REQUESTS_LIST, false);
	}
	
	public void goToDouRequestsImportPage(Map<String,Serializable> params){
		final FormPanel form = new FormPanel();
		form.setAction("equeue/queueImport");
		// Because we're going to add a FileUpload widget, we'll need to set the
		// form to use the POST method, and multipart MIME encoding.
		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_POST);
		
		final DialogBox dialogBox = new DialogBox();
	    dialogBox.ensureDebugId("cwDialogBox");
		dialogBox.setText("Импорт очередников из PreschoolChildRegister");

	    // Create a table to layout the content
	    DockPanel dialogContents = new DockPanel();
	    dialogContents.setSpacing(4);
	    dialogContents.setWidth("800px");
	    dialogContents.setHeight("200px");
//	    dialogBox.setWidget();

	    dialogBox.setWidget(form);
	    form.add(dialogContents);
	    HTML details = new HTML("Выберите файл для загрузки:");
	    details.setWidth("490px");
	    
	 // Create a FileUpload widget.
		FileUpload fileUpload = new FileUpload();
		fileUpload.setName("fileUpload");
		
		final FlowPanel flowPanel1 = new FlowPanel();
		dialogContents.add(flowPanel1, DockPanel.CENTER);
		flowPanel1.add(details);
		flowPanel1.add(fileUpload);
	    
	    final Button okButton = new Button("Ok");
	    okButton.setWidth("100px");
	    ClickHandler okHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Log.debug("Befor submit");
				form.submit();
				Log.debug("After submit");
			}
		};
		okButton.addClickHandler(okHandler);
		
		final Button cancelButton = new Button("Отмена");
	    cancelButton.setWidth("100px");
	    ClickHandler cancelHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		};

		cancelButton.addClickHandler(cancelHandler);
		
		final FlowPanel flowPanel = new FlowPanel();
		flowPanel.add(okButton);
		flowPanel.add(cancelButton);
		dialogContents.add(flowPanel, DockPanel.SOUTH);
		
		form.addSubmitCompleteHandler(new SubmitCompleteHandler() {
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				String results = event.getResults();
				flowPanel1.clear();
				final ScrollPanel scroll = new ScrollPanel();
				
				
				scroll.add(new HTML(results));
				flowPanel1.add(scroll);
				
				flowPanel.remove(okButton);
				cancelButton.setText("Закрыть");
			}
		});
		
//		Do some actions to show our modal panel
	    dialogBox.setModal(true);
	    dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);
	    dialogBox.center();
	    dialogBox.show();
	}
	
	public void goToDouRequestEditPage(Map<String,Serializable> p){
		if(p==null)
			p=Collections.emptyMap();

		final Map<String,Serializable> params=p;
		
		long time1=System.currentTimeMillis();
		Log.debug("goToDouRequestEditPage()");
		final DouRequestEditWidget display = new DouRequestEditWidget();
		long time2=System.currentTimeMillis();
		Log.debug("DouRequestEditWidget widjet has been created in "+(time2-time1)+" ms.");
//		display.setShell(EQueueShell.this);
		final DouRequestEditPresenter presenter = new DouRequestEditPresenterImpl(
				display);
		final long time3=System.currentTimeMillis();
		Log.debug("DouRequestEditPresenter widjet has been created in "+(time3-time2)+" ms.");
		display.setPresenter(presenter);
		EQueueShell.instance().setContent(display);
		presenter.initialize(new Callback() {

			@Override
			public void callback() {
				if(params.containsKey("objectUId")){
					Long uid = null;
					if(params.get("objectUId") instanceof Long){
						uid = (Long)params.get("objectUId");
					} else {
						uid = new Long(String.valueOf(params.get("objectUId")));
					}
					presenter.setObjectUId( uid);
					display.initEditMode();
					History.newItem(URL_DOU_REQUEST_EDIT+"?objectUId="+uid, false);
				}else{
					display.initCreateMode();
					History.newItem(URL_DOU_REQUEST_EDIT, false);
				}
				long time4=System.currentTimeMillis();
				Log.debug("DouRequestEditPresenter has been initialized in "+(time4-time3)+" ms.");
			}
		});
	}

	public void goToContractorsListPage(Map<String,Serializable> params) {
		ContragentListWidget display = new ContragentListWidget();
//		display.setShell(EQueueShell.this);
		ContragentListPresenterImpl presenter = new ContragentListPresenterImpl(
				display);
		display.setPresenter(presenter);
		presenter.initialize();
		EQueueShell.instance().setContent(display);
		//contentPanel.setWidget(display);
		History.newItem(URL_CONTRACTORS_LIST, false);
		
	}
	
	public void goToPrivilegesListPage(Map<String,Serializable> params) {
		PrivilegesWidget display = new PrivilegesWidget();
		PrivilegesPresenterImpl presenter = new PrivilegesPresenterImpl(
				display);
		display.setPresenter(presenter);
		EQueueShell.instance().	setContent(display);
		presenter.initialize();
		History.newItem(URL_PRIVILEGES_LIST, false);
	}
	
	public void goToRegionsListPage(Map<String,Serializable> params) {
		RegionsWidget display = new RegionsWidget();
		RegionsPresenterImpl presenter = new RegionsPresenterImpl(
				display);
		display.setPresenter(presenter);
		EQueueShell.instance().setContent(display);
		presenter.initialize();
		History.newItem(URL_REGIONS_LIST, false);
	}
	
	public void goToBdIssuePage(Map<String,Serializable> params) {
		BdIssueWidget display = new BdIssueWidget();
		BdIssuePresenterImpl presenter = new BdIssuePresenterImpl(display);
		
		display.setPresenter(presenter);
		EQueueShell.instance().	setContent(display);
		presenter.initialize();
		History.newItem(BAD_BD_ISSUE, false);
	}
	
	public void goToReport5_1Page(Map<String,Serializable> params) {
		Reporta5_1Widget display = new Reporta5_1Widget();
		Reporta5_1PresenterImpl presenter = new Reporta5_1PresenterImpl(
				display);
		
		display.setPresenter(presenter);
		EQueueShell.instance().	setContent(display);
		presenter.initialize();
		History.newItem(REPORT5_1, false);
	}

	
	public void goToReport5_2Page(Map<String,Serializable> params) {
		Reporta5_2Widget display = new Reporta5_2Widget();
		Reporta5_2PresenterImpl presenter = new Reporta5_2PresenterImpl(
				display);
		EQueueShell.instance().	setContent(display);
		presenter.initialize();
		History.newItem(REPORT5_2, false);
	}

	
	public void goToReport5_3Page(Map<String,Serializable> params) {
		Reporta5_3Widget display = new Reporta5_3Widget();
		Reporta5_3PresenterImpl presenter = new Reporta5_3PresenterImpl(
				display);
		EQueueShell.instance().	setContent(display);
		presenter.initialize();
		History.newItem(REPORT5_3, false);
	}
	
	public void goToReport5_4Page(Map<String,Serializable> params) {
		Reporta5_4Widget display = new Reporta5_4Widget();
		Reporta5_4PresenterImpl presenter = new Reporta5_4PresenterImpl(
				display);
		EQueueShell.instance().	setContent(display);
		presenter.initialize();
		History.newItem(REPORT5_4, false);
	}
	
	public void goToReport5_5Page(Map<String,Serializable> params) {
		Reporta5_5Widget display = new Reporta5_5Widget();
		Reporta5_5PresenterImpl presenter = new Reporta5_5PresenterImpl(
				display);
		EQueueShell.instance().	setContent(display);
		presenter.initialize();
		History.newItem(REPORT5_5, false);
	}
	
	public void goToChart1Page(Map<String,Serializable> params) {
		Chart1 display = new Chart1();
		EQueueShell.instance().	setContent(display);
		History.newItem(CHART1, false);
	}

	
	public void goToJuridicalTypesListPage(Map<String,Serializable> params){
		EQueueShell.instance().setContent(new JuridicalTypesBook());
		History.newItem(URL_JURIDICAL_TYPES_LIST, false);
	}
	
	public void goToDouGroupsListPage(Map<String,Serializable> params){
		EQueueShell.instance().setContent(new DOUGroupsBook());
		History.newItem(URL_DOU_GROUPS_LIST, false);
	}

	public void goToFoundationsListPage(Map<String,Serializable> params){
		EQueueShell.instance().setContent(new FoundatinsBook());
		History.newItem(URL_FOUNDATIONS_LIST, false);
	}
	
	public void goToAbout(Map<String,Serializable> params){
		EQueueShell.instance().setContent(new About());
		History.newItem(URL_ABOUT, false);
	}
	
	class GoToURLCommang extends CommandImpl{
		private String url;
		
		public GoToURLCommang(String url) {
			if(url==null || url.isEmpty())
				throw new RuntimeException();
			this.url = url;
		}

		@Override
		public void execute(Map<String, Serializable> params) {
			Window.open(url, "blank_", null);
		}
	}
	
}
