package org.itx.jbalance.equeue.gwt.client.reports;

import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsService;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsServiceAsync;
import org.itx.jbalance.equeue.gwt.shared.Report5_2DTO;
import org.itx.jbalance.equeue.gwt.shared.ReportAgeWithCount;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;


/**
 * Контроллер(пресентор) для формы просмотра списка заявок.
 * @author apv
 *
 */
public class Reporta5_2PresenterImpl extends MyPresenter<Reporta5_2Widget>
		implements Reporta5_2Presenter {


	
	
	public Reporta5_2PresenterImpl(Reporta5_2Widget display) {
		super(display);
	
	}

	private final DouRequestsServiceAsync service = GWT.create(DouRequestsService.class);

	@Override
	public void initialize() {
		Report5_2DTO dto=new Report5_2DTO();
		
		dto.ages.add(new ReportAgeWithCount(0,12)); // 0.0-1.0
		dto.ages.add(new ReportAgeWithCount(13,18));// 1.1-1.6
		dto.ages.add(new ReportAgeWithCount(19,24));// 1.7-2.0
		dto.ages.add(new ReportAgeWithCount(25,30));// 2.1-2.6
		dto.ages.add(new ReportAgeWithCount(31,36));// 2.7-3.0
		dto.ages.add(new ReportAgeWithCount(37,42));// 3.1-3.6
		dto.ages.add(new ReportAgeWithCount(43,48));// 3.7-4.0
		dto.ages.add(new ReportAgeWithCount(49,60));// 4.1-5.0
		dto.ages.add(new ReportAgeWithCount(61,72));// 5.1-6.0
		dto.ages.add(new ReportAgeWithCount(73,78));// 6.1-6.6
		dto.ages.add(new ReportAgeWithCount(79,84));// 6.7-7.0
		
	

		
		
		service.getReport5_2Data(dto, new AsyncCallback<Report5_2DTO>() {
			
			@Override
			public void onSuccess(Report5_2DTO result) {
				Log.debug(""+result);
				getDisplay().setReportData(result);
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}


}

