package org.itx.jbalance.equeue.gwt.client.register;


import java.io.Serializable;
import java.util.List;
import java.util.Set;

import org.itx.jbalance.equeue.gwt.client.Callback;
import org.itx.jbalance.equeue.gwt.client.MyDisplay;
import org.itx.jbalance.equeue.gwt.client.register.AutoRegisterEditPresenterImpl.TableLine;
import org.itx.jbalance.equeue.gwt.shared.RegisterLine;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l2_api.dto.RegionDTO;


public interface AutoRegisterEditPresenter extends Serializable{

	public interface Display extends MyDisplay{

	}
	
//	void save(Callback onSuccess);
//	void setObject(Long objectUid);
	void initialize();
	//public void setRegNums(List<Long> ids);
//	public List<RegisterLine> getTableData();
//	public void setRegNums(Set<Long> ids);
//	public Map<HDouRequest,List<SDouRequest>> getMap();
//	public void clearRequests();
//	List<Dou> getAllDou();
	/**
	 * #238: Панель протокола
	 * @return
	 */
//	ChildCount getChildCount();

	void clear();
	void save(Object object);
	List<TableLine> getTableData(RegionDTO selectedRegion);
	/**
	 * Пересчитывает сумму по возр. группам
	 */
	void recalculateSum();
//	HRegister getRegister();
//	String getDous(HDouRequest douRequest);
//	void goBack();
//	void goForward();
	List<RegionDTO> getRegions();
}
