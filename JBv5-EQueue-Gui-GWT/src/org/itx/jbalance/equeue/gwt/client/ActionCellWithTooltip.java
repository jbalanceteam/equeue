package org.itx.jbalance.equeue.gwt.client;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;

public class ActionCellWithTooltip<C> extends ActionCell<C>{

	private final SafeHtml html;
	
	public ActionCellWithTooltip(SafeHtml message, Delegate<C> delegate,String tooltip) {
		super(message, delegate);
		this.html = new SafeHtmlBuilder().appendHtmlConstant(
        "<button title='"+tooltip+"' type=\"button\" tabindex=\"-1\">").append(message).appendHtmlConstant(
        "</button>").toSafeHtml();
	}
	
	 @Override
	  public void render(Context context, C value, SafeHtmlBuilder sb) {
	    sb.append(html);
	  }

}
