package org.itx.jbalance.equeue.gwt.client.reports;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsService;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsServiceAsync;
import org.itx.jbalance.equeue.gwt.shared.Report5_3DTO;
import org.itx.jbalance.equeue.gwt.shared.ReportsAge;
import org.itx.jbalance.equeue.gwt.shared.ReportsDou;
import org.itx.jbalance.l0.o.Dou;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;


/**
 * Контроллер(пресентор) для формы просмотра списка заявок.
 * @author apv
 *
 */
public class Reporta5_3PresenterImpl extends MyPresenter<Reporta5_3Widget>
		implements Reporta5_3Presenter {


	
	
	public Reporta5_3PresenterImpl(Reporta5_3Widget display) {
		super(display);
	
	}

	private final DouRequestsServiceAsync service = GWT.create(DouRequestsService.class);

	@Override
	public void initialize() {
		Report5_3DTO dto=new Report5_3DTO();
		
		dto.getAges().add(new ReportsAge(0,12)); // 0.0-1.0
		dto.getAges().add(new ReportsAge(13,18));// 1.1-1.6
		dto.getAges().add(new ReportsAge(19,24));// 1.7-2.0
		dto.getAges().add(new ReportsAge(25,30));// 2.1-2.6
		dto.getAges().add(new ReportsAge(31,36));// 2.7-3.0
		dto.getAges().add(new ReportsAge(37,42));// 3.1-3.6
		dto.getAges().add(new ReportsAge(43,48));// 3.7-4.0
		dto.getAges().add(new ReportsAge(49,60));// 4.1-5.0
		dto.getAges().add(new ReportsAge(61,72));// 5.1-6.0
		dto.getAges().add(new ReportsAge(73,78));// 6.1-6.6
		dto.getAges().add(new ReportsAge(79,84));// 6.7-7.0
		
		

		getDisplay().initTableColumns(dto.getAges());
		
		service.getReport5_3Data(dto, new AsyncCallback<Report5_3DTO>() {
			
			@Override
			public void onSuccess(Report5_3DTO result) {
				Log.debug(""+result);
				
				
				
				
				ReportsDou e = new ReportsDou();
				Dou dou = new Dou();
				dou.setName("ИТОГО");
				e.setDou(dou);
				List<ReportsDou> rows = result.getRows();
				for (ReportsDou r : rows) {
					for (ReportsAge ra:  r.getCount().keySet()){
						int sum=e.getCount().get(ra)==null?0:e.getCount().get(ra);
						
						e.getCount().put(ra,sum+r.getCount().get(ra));
					}
					
				}
						
				rows.add(0,e);
				
				
				getDisplay().setReportData(result);
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}


}

