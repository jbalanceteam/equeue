package org.itx.jbalance.equeue.gwt.client.register;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.shared.RegisterLine;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.Dou;

/**
 * #238: Панель протокола
 * Для статистики в протоколе
 * Статистика протокола должна показывать
 *     Сколько очередников включено в протокол.
 *     Сколько очередников в каждом саду которые включены в текущий протокол 
 *     сколько льготников в проколе.
 *     сколько льготников по каждому саду
 * @author apv
 *
 */
public class ChildCount {
	/**
	 * ДОУ ID - ChildCountLine
	 */
	private Map<String, ChildCountLine> byDou = new HashMap<String, ChildCount.ChildCountLine>();
	/**
	 * На весь сад
	 */
	private ChildCountLine all = new ChildCountLine();
	
	
	
	
	public ChildCount(List<RegisterLine>spec) {
		super();
		init(spec);
	}

//	public void put(Integer douId, ChildCountLine count){
//		byDou.put(douId, count);
//		recalculate();
//	}
//	
//	private void recalculate(){
//		Collection<ChildCountLine> values = byDou.values();
//		Integer privelegesCnt = 0;
//		Integer allCnt = 0;
//		
//		for (ChildCountLine childCountLine : values) {
//			privelegesCnt += childCountLine.getPrivelegesCnt();
//			allCnt += 		 childCountLine.getAllCnt();
//		}
//		all.setAllCnt(allCnt);
//		all.setPrivelegesCnt(privelegesCnt);
//	}
	
	public List<String> getAllDou(){
		List<String> res = new ArrayList<String> (byDou.keySet());
		Collections.sort(res, Dou.douNumbersComparator);
		return res;
	}
	
	public ChildCountLine getAll(){
		return all;
	}
	
	public ChildCountLine getByDou(String douId){
		return byDou.get(douId);
	}
	
	
	public void init(List<RegisterLine>spec){
		Integer privelegesCnt = 0;
		Integer allCnt = 0;
		for (RegisterLine registerLine : spec) {
			HDouRequest douRequest = registerLine.getDouRequest();
			if(douRequest.getPrivilege() != null){
				privelegesCnt ++;
			}
			allCnt ++;
			
			Dou dou = registerLine.getSRegister().getDou();
			if(dou != null){
				if(! byDou.containsKey(dou.getNumber())){
					 byDou.put(dou.getNumber(), new ChildCountLine());
				}
				ChildCountLine childCountLine = byDou.get(dou.getNumber());
				
				if(douRequest.getPrivilege() != null){
					childCountLine.addToPrivelegesCnt();
				}
				childCountLine.addToAllCnt();
			}
		}
		all.setAllCnt(allCnt);
		all.setPrivelegesCnt(privelegesCnt);
	}
	
	public class ChildCountLine{
		private int privelegesCnt = 0;
		private int allCnt  = 0;
		
		public Integer getPrivelegesCnt() {
			return privelegesCnt;
		}
		public void setPrivelegesCnt(Integer privelegesCnt) {
			this.privelegesCnt = privelegesCnt;
		}
		public Integer getAllCnt() {
			return allCnt;
		}
		public void setAllCnt(Integer allCnt) {
			this.allCnt = allCnt;
		}
		
		
		public void addToPrivelegesCnt() {
			privelegesCnt ++;
		}

		public void addToAllCnt() {
			allCnt ++;
		}
	}
	
	
}
