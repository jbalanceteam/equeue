package org.itx.jbalance.equeue.gwt.client.services;

import java.util.List;

import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.RegisterEditData;
import org.itx.jbalance.equeue.gwt.shared.RegisterLine;
import org.itx.jbalance.equeue.gwt.shared.SaveRegisterResult;
import org.itx.jbalance.equeue.gwt.shared.SetRegNumsRequest;
import org.itx.jbalance.equeue.gwt.shared.SetRegNumsResult;
import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Foundation;
import org.itx.jbalance.l1.api.RegisterSearchParams;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterLineDTO;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterPrerequisiteInfo;
import org.itx.jbalance.l2_api.dto.equeue.RegisterDTO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("register")
public interface RegisterService extends RemoteService {
	
	
	SaveRegisterResult create(HRegister register,List<RegisterLine>spec);
	
	SaveRegisterResult update(HRegister register,List<RegisterLine>spec);
	
	List<SRegister> getSpec (Long uid);
	
	void delete(Long id);
	SearchResultWrapper<RegisterDTO> search(RegisterSearchParams params);
	
	List<Foundation>getFoundations();

	RegisterEditData getEditData(Long uid);
	

	SetRegNumsResult getSetRegNumsData(SetRegNumsRequest request);

	SaveRegisterResult changeStatus(HRegister register, List<RegisterLine> spec,
			RegisterStatus newStatus);

	PermitsSearchResultsWrapper searchPermits(GetPermitsRequest searchParams);
	
	AutoRegisterPrerequisiteInfo getAutoRegisterPrerequisiteInfo();

	/**
	 * Сохраняет  HAutoRegister и генерит протокол
	 * @param autoRegister
	 * @return
	 */
	OperationResultWrapper<RegisterDTO> autoRegister(HAutoRegister result, List<AutoRegisterLineDTO> lines);
}
