package org.itx.jbalance.equeue.gwt.client.services;

import java.util.ArrayList;

import org.itx.jbalance.l0.o.DOUGroups;



import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("douGroups")
public interface DOUGroupsService extends RemoteService {
	public ArrayList<DOUGroups> getAll();
}
