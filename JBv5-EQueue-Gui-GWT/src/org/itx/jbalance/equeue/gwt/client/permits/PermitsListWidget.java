package org.itx.jbalance.equeue.gwt.client.permits;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.client.ClickableSafeHtmlCell;
import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.EQueueNavigation;
import org.itx.jbalance.equeue.gwt.client.request.ChooseDouPanel;
import org.itx.jbalance.equeue.gwt.client.request.ChooseDouPanel.SelectDouListener;
import org.itx.jbalance.equeue.gwt.client.request.PrivilegeSearchItem;
import org.itx.jbalance.equeue.gwt.client.request.PrivilegeSearchItem.Type;
import org.itx.jbalance.equeue.gwt.client.services.PermitsSearchResultsWrapper;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l2_api.dto.attendance.PermitDTO;

import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortEvent.AsyncHandler;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DisclosurePanel;
import com.google.gwt.user.client.ui.DisclosurePanelImages;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasConstrainedValue;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ProvidesKey;

/**
 * Форма показывающая выданые путевки.
 * #368 Выданные путевки
 * @author apv
 *
 */
public class PermitsListWidget extends ContentWidget implements PermitsListPresenter.Display {

	interface Binder extends UiBinder<Widget, PermitsListWidget> {

	}

	
	@UiField(provided = true)
	CellTable<PermitDTO> permitsTable = new CellTable<PermitDTO>(PermitsListPresenter.PAGE_SIZE);

	
	/**
	 * Поиск по всем полям
	 */
	@UiField(provided = false)
	TextBox searchField;
	
	
	DisclosurePanelHeader douSearchHeaderWidget;
	
	@UiField(provided=false)
	TextBox ageFrom;
	
	@UiField(provided=false)
	TextBox ageTo;
	
	@UiField(provided=false)
	DateBox permitDateFrom;
	
	@UiField(provided=false)
	DateBox permitDateTo;
	
	
	@UiField(provided = false)
	HTML searchResLabel;
//	EnumRenderer<PrivilegesSearchItems> requesterTypeRenderer = new EnumRenderer<PrivilegesSearchItems>();
	
	AsyncDataProvider<PermitDTO> provider;

	ChooseDouPanel chooseDouPanel;
	
	
	Renderer<Integer> intRenderer=  new AbstractRenderer<Integer>() {
    	@Override
		public String render(Integer object) {
    		if(object==null)
    			return "--";
			return object+"";
		}
	};
	
	
	@UiField(provided=false)
	DisclosurePanel searchDouPanel;
	
	
	final DisclosurePanelImages images = (DisclosurePanelImages)GWT.create(DisclosurePanelImages.class);

	class DisclosurePanelHeader extends HorizontalPanel {
	HTML html = new HTML();
	
	public DisclosurePanelHeader(boolean isOpen, String html){
	add(isOpen ? images.disclosurePanelOpen().createImage()
          : images.disclosurePanelClosed().createImage());
    this.html .setHTML(html);
    add(this.html);
}

	public HTML getHtml() {
		return html;
	}
}


	/**
	 * Поле фильтра - дата рождения "С"
	 */
	@UiField(provided = false)
	DateBox bithdayFrom;
	
	/**
	 * Поле фильтра - дата рождения "ПО"
	 */
	@UiField(provided = false)
	DateBox bithdayTo;
	
	 
	

	
	@UiField(provided = true)
	ValueListBox<PrivilegeSearchItem> privilegeListBox;

	@UiField(provided = true)
	SimplePager simplePager=new SimplePager();

	@UiField(provided = false)
	Button printListButton;
	
	
	
	public PermitsListWidget() {
		super();
	}
	ProvidesKey<PermitDTO> keyProvider;
	@Override
	public void initialize() {
		privilegeListBox = new ValueListBox<PrivilegeSearchItem>(new  AbstractRenderer<PrivilegeSearchItem>() {
			@Override
			public String render(PrivilegeSearchItem object) {
				switch(object.getType()){
					case ANY: return "Все";
					case NO_PRIVILEGE: return "Льгот нет";
					case YES_PRIVILEGE: return "Льгота есть";
					case EXACT_PRIVELEGE: return object.getExactPrivelageName();
					default: return "";
				}
			}
		});
		
		
		
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		initTableColumns();
		initTableStyles();
		keyProvider=new ProvidesKey<PermitDTO>() {

			@Override
			public Object getKey(PermitDTO item) {
				return item.getUId();
			}
		};
//		final SingleSelectionModel<PermitDTO> selectionModel=new SingleSelectionModel<PermitDTO>(keyProvider);
//		permitsTable.setSelectionModel(selectionModel);
		
		permitsTable.setPageSize(PermitsListPresenter.PAGE_SIZE);
		simplePager.setDisplay(permitsTable);

	    // Add a ColumnSortEvent.AsyncHandler to connect sorting to the
	    // AsyncDataPRrovider.
	    AsyncHandler columnSortHandler = new AsyncHandler(permitsTable);
	    permitsTable.addColumnSortHandler(columnSortHandler);

		
	    
	    
	    DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd.MM.yy");
		 DateBox.Format format=new DateBox.DefaultFormat(dateTimeFormat); 
		 bithdayFrom.setFormat(format); 
		 bithdayTo.setFormat(format); 
		 permitDateFrom.setFormat(format); 
		 permitDateTo.setFormat(format); 

		
		
//		/* init Date Filter*/
//		years=new ArrayList<Integer>();
//		for(int y=currentYear+1900;y>currentYear+1900-10;y--){
//			years.add(y);
//		}
//		yearFrom.setAcceptableValues(years);
//		yearFrom.setValue(null);
//		
//		yearTo.setAcceptableValues(years);
//		yearTo.setValue(null);
		
		
		 douSearchHeaderWidget = new DisclosurePanelHeader(false,"Любой");
		 
		 searchDouPanel.setHeader(douSearchHeaderWidget);
		 searchDouPanel.getElement().getStyle().setPosition(Position.ABSOLUTE);
		 searchDouPanel.getElement().getStyle().setBackgroundColor("white");
		 
		 /**
		  * #121
		  * Поиск при нажатии Enter в поле ввода
		  */
		 KeyPressHandler searchHandler = new KeyPressHandler() {
			@Override
			public void onKeyPress(KeyPressEvent event) {
				if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {
					getPresenter().search();
			    }
			}
		};
		searchField.addKeyPressHandler(searchHandler);
		bithdayFrom.getTextBox().addKeyPressHandler(searchHandler);
		bithdayTo.getTextBox().addKeyPressHandler(searchHandler);
		permitDateFrom.getTextBox().addKeyPressHandler(searchHandler);
		permitDateTo.getTextBox().addKeyPressHandler(searchHandler);
		ageFrom.addKeyPressHandler(searchHandler);
		ageTo.addKeyPressHandler(searchHandler);
		
	}

	
	private void initTableStyles() {
//		permitsTable.setRowStyles(new RowStyles<PermitDTO>() {
//			@Override
//			public String getStyleNames(PermitDTO row, int rowIndex) {
//				if(row!=null && row.getRecordColor()!=null)
//					return row.getRecordColor().name();
//				return null;
//			}
//		});
	}


	@UiHandler("searchButton")
	public void runSearch(ClickEvent event){
		getPresenter().search();
	}

	@UiHandler("cleanButton")
	public void cleanSearch(ClickEvent event){
		getPrivilege().setValue(PrivilegeSearchItem.ANY);

		getAgeFrom().setText("");
		getAgeTo().setText("");
		getPermitDateFrom().setValue(null);
		getPermitDateTo().setValue(null);
		getSearchField().setText("");
		getBirthdayFromDateBox().setValue(null);
		getBirthdayToDateBox().setValue(null);
//		getPresenter().getSearchParams().firstname=null;
//		getPresenter().getSearchParams().lastname=null;
//		getPresenter().getSearchParams().middlename=null;
		
		chooseDouPanel.clearSelection();
		
		updateDouSearchPanelHeader();
		
		getPresenter().search();
	}
	
	@UiHandler("printListButton")
	public void print(ClickEvent event){
//		SerializerServiceAsync serializerServiceAsync= GWT.create(SerializerService.class);
//		serializerServiceAsync.serializeToBase64(getPresenter().getSearchParams(), new AsyncCallback<String>() {
//			
//			@Override
//			public void onSuccess(String result) {
//				String urlStr = "equeue/report?type=requestsList";
//				urlStr+="&searchParams="+result;
//				 Window.open(urlStr, "blank_", null);
//			}
//			
//			@Override
//			public void onFailure(Throwable caught) {
//				showExceptionNotification(caught);
//			}
//		});
		showInfoNotification("Ожидайте в следующих версиях...");
		
	}
	
	

	
	/**
	 * Список садов для фильтра
	 */
	public void setDouList(Map<Region, List<Dou>> dous){
		searchDouPanel.clear();
//		cb2douid.clear();
//		douid2douNumber.clear();
		SelectDouListener listener = new SelectDouListener() {
			@Override
			public void selectionChanged(List<Dou> selected) {
				updateDouSearchPanelHeader();
				
			}
		};
		chooseDouPanel = new ChooseDouPanel(dous, listener);
		searchDouPanel.add(chooseDouPanel);
	}
	
	private void updateDouSearchPanelHeader(){
		String title="";
		List<Dou> selectedDou = chooseDouPanel.getSelectedDou();
		if(selectedDou==null || selectedDou.isEmpty())
			title = "Любой";
		else{
			for (int i=0;i<  selectedDou.size() ; i++) {
				Dou dou = selectedDou.get(i);
				title += dou.getNumber() ;
				if(i!=selectedDou.size()-1)
					title += ", ";
			}
		}
		douSearchHeaderWidget.html.setHTML(title);
	}
	
	
	PermitsListPresenter presenter;

	public PermitsListPresenter getPresenter() {
		return presenter;
	}


	public void setPresenter(PermitsListPresenter presenter) {
		this.presenter=presenter;
	}

	
	public void updateRowData(int start,List<PermitDTO>data){
		provider.updateRowData(start, data);
	}
	int start=0;
//	@Override
	public void showDouRequests(PermitsSearchResultsWrapper searchRes) {
		
		if(provider==null){
		provider = new AsyncDataProvider<PermitDTO>(keyProvider) {
		      @Override
		      protected void onRangeChanged(HasData<PermitDTO> display) {
		        start = display.getVisibleRange().getStart();
		        int end = start + display.getVisibleRange().getLength();
		        ColumnSortList columnSortList = permitsTable.getColumnSortList();
		        
		        List<SortedColumnInfo>sortedColumns=new LinkedList<SortedColumnInfo>();
//		        for(int i=0;i<columnSortList.size();i++){
		        if(columnSortList.size()>0)	
		        	sortedColumns.add(new SortedColumnInfo("rn", columnSortList.get(0).isAscending()));
		        else
		        	sortedColumns.add(new SortedColumnInfo("rn", false));
//		        	 columnSortList.get(i).getColumn().get
//		        }
		       
		        getPresenter().loadData(start,end,sortedColumns);
		        
		      }
		    };
		    provider.addDataDisplay(permitsTable);
		}else{
		
		}
		    
		if(searchRes.getSearchCount() != null){
			provider.updateRowCount(searchRes.getSearchCount(), true);
		} else {
			provider.updateRowCount(searchRes.getList().size(), true);
		}
		 provider.updateRowData(0, searchRes.getList());
		 
//		permitsTable.setRowData(searchRes.getList());
		searchResLabel.setHTML("Найдено по запросу <b>"+searchRes.getSearchCount() +"</b>.     Всего путевок в системе <b>"+searchRes.getAllCount()+"</b>.");
		
	}

	public HasText getSearchField() {
		return searchField;
	}

	public void setSearchField(TextBox searchField) {
		this.searchField = searchField;
	}
	
	public HasConstrainedValue<PrivilegeSearchItem>getPrivilege(){
		return privilegeListBox;
	}
	
	


	public void populateDousList(List<SDouRequest> result) {
		String html = "";
		for (int i=0;i<  result.size() ; i++) {
			SDouRequest sDouRequest = result.get(i);
			Dou dou = sDouRequest.getArticleUnit();
			html += dou.getNumber() ;
			if(i!=result.size()-1)
				html += ", ";
		}
		
//		douList.setHTML(html);
	}
	
	
	
	
//	public HasConstrainedValue<Dou> getSearchedDou(){
//		return searchDouListBox;
//	}

	
	private void initTableColumns() {
		
		/************** № п/п **************/
		Column<PermitDTO, String> dynNumColumn = new Column<PermitDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(PermitDTO object) {
				 return (permitsTable.getVisibleItems().indexOf(object)+1+start)+"";
			}
		};
		permitsTable.addColumn(dynNumColumn, SafeHtmlUtils.fromSafeConstant("#"));
		permitsTable.setColumnWidth(dynNumColumn,"10px");
		/************** № п/п **************/

		
		/************** Рег номер **************/
		Column<PermitDTO, SafeHtml> rnColumn = new Column<PermitDTO, SafeHtml>(
		        new ClickableSafeHtmlCell()) {
		    @Override
		    public SafeHtml getValue(PermitDTO object) {
		        SafeHtmlBuilder sb = new SafeHtmlBuilder();
		        sb.appendHtmlConstant("<a href='javascript:void(0);'>");
		        sb.appendEscaped(object.getRegNumber().toString());
		        sb.appendHtmlConstant("</a>");
		        return sb.toSafeHtml();
		    }
		};

		rnColumn.setFieldUpdater(new FieldUpdater<PermitDTO, SafeHtml>() {
		        @Override
		        public void update(int index, PermitDTO object, SafeHtml value) {
		            HashMap<String, Serializable> p = new HashMap<String, Serializable>();
		            p.put("objectUId", object.getRegUid());
					EQueueNavigation.instance().goToDouRequestEditPage(p);
		        }
		    });
		
		
//		Column<PermitDTO, String> idColumn = new Column<PermitDTO, String>(
//				new TextCell()) {
//			@Override
//			public String getValue(PermitDTO object) {
//				return object.getRegNumber();
//			}
//		};
//		idColumn.setSortable(true);
		permitsTable.addColumn(rnColumn, SafeHtmlUtils.fromSafeConstant("Рег. номер"));
		/************** Рег номер **************/
		
		
		/************** Фамилия **************/
		Column<PermitDTO, String> lastNameColumn = new Column<PermitDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(PermitDTO object) {
				return object.getChild().getLastName();
			}
		};
		permitsTable.addColumn(lastNameColumn,	SafeHtmlUtils.fromSafeConstant("Фамилия"));
		permitsTable.setColumnWidth(lastNameColumn, "150px");
		/************** Фамилия **************/
		
		
		/************** Имя **************/
		Column<PermitDTO, String> firstNameColumn = new Column<PermitDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(PermitDTO object) {
				return object.getChild().getFirstName();
			}
		};
		permitsTable.addColumn(firstNameColumn,	SafeHtmlUtils.fromSafeConstant("Имя"));
		permitsTable.setColumnWidth(firstNameColumn, "100px");
		/************** Имя **************/
		
		
		
		/************** Отчество **************/
		Column<PermitDTO, String> patronymicColumn = new Column<PermitDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(PermitDTO object) {
				return object.getChild().getPatronymic();
			}
		};
		permitsTable.addColumn(patronymicColumn,	SafeHtmlUtils.fromSafeConstant("Отчество"));
		permitsTable.setColumnWidth(patronymicColumn, "100px");
		/************** Отчество **************/
		
		
		
		/************** Дата рождения **************/
		Column<PermitDTO, String> birthdayColumn = new Column<PermitDTO, String>(new TextCell()){

			@Override
			public String getValue(PermitDTO object) {
				return object.getChild().getBirthdayFormated();
			}};	
		permitsTable.addColumn(birthdayColumn,	SafeHtmlUtils.fromSafeConstant("Дата рожд."));
		/************** Дата рождения **************/
	
		
		
		
		/************** номер протокола **************/
		Column<PermitDTO, SafeHtml> registerNumberColumn = new Column<PermitDTO, SafeHtml>(
		        new ClickableSafeHtmlCell()) {
		    @Override
		    public SafeHtml getValue(PermitDTO object) {
		        SafeHtmlBuilder sb = new SafeHtmlBuilder();
		        sb.appendHtmlConstant("<a href='javascript:void(0);'>");
		        sb.appendEscaped(object.getRegisterNumber());
		        sb.appendHtmlConstant("</a>");
		        return sb.toSafeHtml();
		    }
		};

		registerNumberColumn.setFieldUpdater(new FieldUpdater<PermitDTO, SafeHtml>() {
		        @Override
		        public void update(int index, PermitDTO object, SafeHtml value) {
		            HashMap<String, Serializable> p = new HashMap<String, Serializable>();
		            p.put("objectUId", object.getRegisterUid());
					EQueueNavigation.instance().goToRegisterEditPage(p);
		        }
		    });
		
//		Column<PermitDTO, String> registerNumberColumn = new Column<PermitDTO, String>(
//				new TextCell()) {
//			@Override
//			public String getValue(PermitDTO object) {
//				return object.getRegisterNumber();
//			}
//		};
		permitsTable.addColumn(registerNumberColumn,	SafeHtmlUtils.fromSafeConstant("Номер протокола"));
		permitsTable.setColumnWidth(registerNumberColumn, "30px");
		/************** номер протокола **************/
		
		
		
		/************** дата протокола **************/
		Column<PermitDTO, String> registerDateColumn = new Column<PermitDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(PermitDTO object) {
				return object.getRegisterDateFormatted();
			}
		};
		permitsTable.addColumn(registerDateColumn,	SafeHtmlUtils.fromSafeConstant("Дата протокола"));
		permitsTable.setColumnWidth(registerDateColumn, "70px");
		/************** дата протокола **************/
		
		
		
		/************** номер сада **************/
		Column<PermitDTO, String> douNumberColumn = new Column<PermitDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(PermitDTO object) {
				return object.getDouNumber();
			}
		};
		permitsTable.addColumn(douNumberColumn,	SafeHtmlUtils.fromSafeConstant("МДОУ"));
		permitsTable.setColumnWidth(douNumberColumn, "150px");
		/************** номер сада **************/
		
		
		
		
		/************** возраст в котором получил путевку ( не меняется от времени открытия списка,) **************/
		Column<PermitDTO, String> ageOnPermitDateColumn = new Column<PermitDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(PermitDTO object) {
				if(object.getAgeOnPermitDate()!=null){
					 return  object.getAgeOnPermitDate()+""; 
				}else
					return null;
			}
		};
		permitsTable.addColumn(ageOnPermitDateColumn, SafeHtmlUtils.fromSafeConstant("Возраст в котором получил путевку"));
		/************** возраст в котором получил путевку ( не меняется от времени открытия списка,) **************/
		
		
		/************** льгота **************/
		Column<PermitDTO, String> privilegeColumn = new Column<PermitDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(PermitDTO object) {
				if(object.getPrivilege()!=null){
					return object.getPrivilege().getName();
				} else {
					return null;
				}
			}
		};
		permitsTable.addColumn(privilegeColumn,	SafeHtmlUtils.fromSafeConstant("Льгота"));
		/************** льгота **************/
		
		
		
		
		/************** статус **************/
		Column<PermitDTO, String> statusColumn = new Column<PermitDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(PermitDTO object) {
				if(object.getPermitStatus() != null){
					return object.getPermitStatus().getDescription();
				} else {
					return "";
				}
			}
		};
		permitsTable.addColumn(statusColumn,	SafeHtmlUtils.fromSafeConstant("Cтатус"));
		/************** статус **************/
		
	}
	
	
	public HasText getAgeFrom(){
		return ageFrom;
	}
	
	public HasText getAgeTo(){
		return ageTo;
	}
	
	
	public  HasValue<Date> getPermitDateFrom(){
		return permitDateFrom;
	}
	
	public  HasValue<Date> getPermitDateTo(){
		return permitDateTo;
	}
	
	public HasValue<Date> getBirthdayFromDateBox() {
		return bithdayFrom;
	}
	
	public HasValue<Date> getBirthdayToDateBox() {
		return bithdayTo;
	}
	

	public List<Long> getSearchedDou() {
		if(chooseDouPanel==null || chooseDouPanel.isAllSelected() || chooseDouPanel.isAllUnselected())
			return null;
		List<Dou> selectedDou = chooseDouPanel.getSelectedDou();
		List<Long> res= new ArrayList<Long>();
		for (Dou dou : selectedDou) {
			res.add(dou.getUId());
		}
		return res;
	}
	 
	
	public  void setSearchedDou(List<Long> doueId) {
		if(chooseDouPanel==null)
			return ;
		chooseDouPanel.selectByUids(doueId, true);
	}


	public void setPrivileges(List<Privilege> privileges) {
		List <PrivilegeSearchItem>vals = new ArrayList<PrivilegeSearchItem>();
		vals.add(PrivilegeSearchItem.ANY);
		vals.add(PrivilegeSearchItem.YES_PRIVILEGE);
		vals.add(PrivilegeSearchItem.NO_PRIVILEGE);
		for (Privilege p : privileges) {
			vals.add(new PrivilegeSearchItem(Type.EXACT_PRIVELEGE, p.getUId(), p.getName()));
		}
		privilegeListBox.setValue(vals.get(0));
		privilegeListBox.setAcceptableValues(vals);
	}
	
}
