package org.itx.jbalance.equeue.gwt.client.register;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.itx.jbalance.equeue.gwt.client.Callback;
import org.itx.jbalance.equeue.gwt.client.ClickableSafeHtmlCell;
import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.DateChooserComponent;
import org.itx.jbalance.equeue.gwt.client.DynamicSelectionCell;
import org.itx.jbalance.equeue.gwt.client.DynamicSelectionCell.OptionsHolder;
import org.itx.jbalance.equeue.gwt.client.DynamicSelectionCell.ToString;
import org.itx.jbalance.equeue.gwt.client.EQueueNavigation;
import org.itx.jbalance.equeue.gwt.client.RepeatableProgressBar;
import org.itx.jbalance.equeue.gwt.client.register.ChildCount.ChildCountLine;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.equeue.gwt.client.request.State;
import org.itx.jbalance.equeue.gwt.shared.RegisterLine;
import org.itx.jbalance.l0.AgeCalculator;
import org.itx.jbalance.l0.h.HDouRequest.Status;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l0.s.SRegister.Foundation;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.ImageCell;
import com.google.gwt.cell.client.SafeHtmlCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ValueUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ShowRangeEvent;
import com.google.gwt.event.logical.shared.ShowRangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueListBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

import eu.maydu.gwt.validation.client.DefaultValidationProcessor;
import eu.maydu.gwt.validation.client.ValidationProcessor;
import eu.maydu.gwt.validation.client.ValidationResult;
import eu.maydu.gwt.validation.client.ValidationResult.ValidationError;
import eu.maydu.gwt.validation.client.Validator;
import eu.maydu.gwt.validation.client.description.PopupDescription;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;

public class RegisterEditWidget extends ContentWidget implements RegisterEditPresenter.Display {

	interface Binder extends UiBinder<Widget, RegisterEditWidget> {

	}

	
	ValidationProcessor validator = new DefaultValidationProcessor();
	DateChooserComponent registerDate=new DateChooserComponent();
	@UiField(provided = false)
	SimplePanel registerDatePanel;
	
	@UiField(provided = false)
	SimplePanel numberRegisterPanel;
	
	@UiField(provided=true)
	CellTable<RegisterLine> requestsTable;
	
	
	DateChooserComponent receiveDueDate =new DateChooserComponent(new Date().getYear()+1900+1);
	

	@UiField(provided = false)
	VerticalPanel receiveDueDatePanel;
	
	@UiField(provided=false)
	TextBox member1;
	
	@UiField(provided=false)
	TextBox member2;
	
	@UiField(provided=false)
	TextBox member3;
	
	@UiField(provided=false)
	Button selectRNButton;
	
	@UiField(provided=false)
	Button clearButton;
	
	@UiField(provided=false)
	Button saveButton;
	
	@UiField(provided=false)
	Button goForwardButton;
	
	@UiField(provided=false)
	Button goBackButton;
	
	///////////////////////////////
	@UiField(provided=false)
	Button selectRNButton1;
	
	@UiField(provided=false)
	Button clearButton1;
	
	@UiField(provided=false)
	Button saveButton1;
	
	@UiField(provided=false)
	Button goForwardButton1;
	
	@UiField(provided=false)
	Button goBackButton1;
	/**
	 * #238: Панель протокола
	 */
	@UiField(provided=false)
	Grid childCountGrig;
	
	@UiField(provided=false)
	HTML title;
	
	@UiField(provided=false)
	CheckBox ageFlagCheckBox;
	
	@UiField(provided = false)
	DateBox ageReprocessDate;
 
	@UiField Image alarmPic;
		
	
	RegisterEditPresenter presenter;
	
	@UiField
	SimplePanel progressPanel;
	
	@UiField(provided = true)
	ValueListBox<DOUGroups> ageGroupValueListBox = new ValueListBox<DOUGroups>(new AbstractRenderer<DOUGroups>() {
		@Override
		public String render(DOUGroups object) {
			if(object == null){
				return "ВСЕ";
			} else {
				return object.getFromToStr();
			}
		}
	}); 
	
	RepeatableProgressBar progressBar;
	
	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public RegisterEditWidget() {
		super();
	}
//


	
	private static Date zeroTime(final Date date)
	{
	    return DateTimeFormat.getFormat("yyyyMMdd").parse(DateTimeFormat.getFormat("yyyyMMdd").format(date));
	}

	private static Date nextDay(final Date d)
	{
	   com.google.gwt.user.datepicker.client.CalendarUtil.addDaysToDate(d, 1);
	   return d;
	}
	private static Date prevDay(final Date d)
	{
	   com.google.gwt.user.datepicker.client.CalendarUtil.addDaysToDate(d, -1);
	   return d;
	}
//
	
	@Override
	public void initialize() {
		if(this.getWidget() == null){
			requestsTable=new CellTable<RegisterLine>(0,MyCellTableResources.INSTANCE);
			Binder uiBinder = GWT.create(Binder.class);
			initWidget(uiBinder.createAndBindUi(this));
			
			//registerDatePanel.add(registerDate);

		    initValidation();
		    
		    requestsTable.setRowCount(0);
		    requestsTable.setWidth("100%", true);
		    
		    childCountGrig.setBorderWidth(1);
		    childCountGrig.setCellSpacing(1);
		    childCountGrig.setCellPadding(2);

		    receiveDueDatePanel.add(receiveDueDate);
		    
		   ageFlagCheckBox.setValue(false);
		   ageReprocessDate.setEnabled(false);
		   
		   alarmPic.setUrl("images/alarm.png");
		  
//		   if(getPresenter().getRegister().getStatus()==RegisterStatus.THIRD)
//		   ageFlagCheckBox.setEnabled(false);
		   
		   // формат вывода датапикера
		     DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd.MM.yyyy");
			 DateBox.Format format=new DateBox.DefaultFormat(dateTimeFormat); 
			 ageReprocessDate.setFormat(format);
		
			 // передергиваем колонку возрасты в таблице если поменялась дата в ageReprocessDate
		   ageReprocessDate.addValueChangeHandler(new ValueChangeHandler<java.util.Date>() {
			   @Override
			   public void onValueChange(ValueChangeEvent<Date> event) {
				   requestsTable.redraw();
			  }
			});
		   
		   // если чувак вручную вбил даты не входящие в диапазон текущего года то ставим границы года
		   ageReprocessDate.addValueChangeHandler(new ValueChangeHandler<Date>()
		   {
		       @Override
		       public void onValueChange(final ValueChangeEvent<Date> dateValueChangeEvent)
		       {
		    	   String begCurrYearString = "01.01."+String.valueOf(new Date().getYear()+1900);
		    	   String endCurrYearString = "31.12."+String.valueOf(new Date().getYear()+1900);
				   DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd.MM.yy");
				      
		           if (dateValueChangeEvent.getValue().before( dateTimeFormat.parse(begCurrYearString)))
		           {
		        	   ageReprocessDate.setValue(dateTimeFormat.parse(begCurrYearString), false);
		           }
		           if (dateValueChangeEvent.getValue().after( dateTimeFormat.parse(endCurrYearString)))
		           {
		        	   ageReprocessDate.setValue(dateTimeFormat.parse(endCurrYearString), false);
		           }
		       }
		   });
		   
		   // вырубаем возможность выбрать даты не относящиеся к текущему году
		   ageReprocessDate.getDatePicker().addShowRangeHandler(new ShowRangeHandler<Date>()
		   {
		      // @Override
		       public void onShowRange(final ShowRangeEvent<Date> dateShowRangeEvent)
		       {
		    	   String begCurrYearString = "01.01."+String.valueOf(new Date().getYear()+1900);
		    	   String endCurrYearString = "31.12."+String.valueOf(new Date().getYear()+1900);
				  DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd.MM.yy");
				  Date begCurrYear = dateTimeFormat.parse(begCurrYearString);
				  Date endCurrYear = dateTimeFormat.parse(endCurrYearString);    
		    	   
		           Date d = zeroTime(dateShowRangeEvent.getStart());
		           while (d.before(begCurrYear))
		           {
		        	   ageReprocessDate.getDatePicker().setTransientEnabledOnDates(false, d);
		               d = nextDay(d);
		           }
		           d= zeroTime(dateShowRangeEvent.getEnd());
		           while (d.after(endCurrYear))
		           {
		        	   ageReprocessDate.getDatePicker().setTransientEnabledOnDates(false, d);
		               d = prevDay(d);
		           }
		       }
		   });
		   //изменяем видимость датапикера в соответствии с галкой и если галки нет 
		   //то ставим дефолтное 1 сентября
		   ageFlagCheckBox.addClickHandler(new ClickHandler() {
			      public void onClick(ClickEvent event) {
			          boolean checked = ((CheckBox) event.getSource()).getValue();		          
			          ageReprocessDate.setEnabled(checked);
			          if (!checked) {
					      String dateString = "01.09."+String.valueOf(new Date().getYear()+1900);
					      DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd.MM.yy");
					      Date defaultReprocessAgeDate=dateTimeFormat.parse(dateString);
					      ageReprocessDate.setValue(defaultReprocessAgeDate);
			          }
			          requestsTable.redraw();
			      }
			});
		   
		   ageGroupValueListBox.addValueChangeHandler(new ValueChangeHandler<DOUGroups>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<DOUGroups> event) {
				List<RegisterLine> tableData = getPresenter().getTableData();
				if(event.getValue() != null){
					List<RegisterLine> filtered = new ArrayList<RegisterLine>();
					for (RegisterLine registerLine : tableData) {
						if(registerLine.getSRegister().getAgeGroupId() == null){
							continue;
						}
						if(event.getValue().getId() == registerLine.getSRegister().getAgeGroupId()){
							filtered.add(registerLine);
						}
					}
					requestsTable.setRowData(filtered);
				} else {
					requestsTable.setRowData(tableData);
				}
				
			}
		});
		   
		}
		updateChildCountGrig();
	}
	
	
	public void initByStatus(){
		HRegister register = getPresenter().getRegister();
		RegisterStatus status;
		if(register != null){
			status = register.getStatus();
		} else {
			status = RegisterStatus.FIRST;
		}
		
		switch(status){
			case FIRST:
				goBackButton.setEnabled(false);
				goForwardButton.setEnabled(true);
				selectRNButton.setEnabled(true);
		    	clearButton.setEnabled(true);
		    	
		    	
		    	goBackButton1.setEnabled(false);
				goForwardButton1.setEnabled(true);
				selectRNButton1.setEnabled(true);
		    	clearButton1.setEnabled(true);
		    	
		    	receiveDueDate.setEnable(false);
		    	alarmPic.setVisible(isAlarm());
				break;
			case SECOND:
				goBackButton.setEnabled(true);
				goForwardButton.setEnabled(true);
				selectRNButton.setEnabled(false);
				clearButton.setEnabled(false);
				
				goBackButton1.setEnabled(true);
				goForwardButton1.setEnabled(true);
				selectRNButton1.setEnabled(false);
				clearButton1.setEnabled(false);
				
				receiveDueDate.setEnable(true);
				alarmPic.setVisible(isAlarm());
				break;
			case THIRD:
				goBackButton.setEnabled(false);
				goForwardButton.setEnabled(false);
				selectRNButton.setEnabled(false);
				clearButton.setEnabled(false);
				
				goBackButton1.setEnabled(false);
				goForwardButton1.setEnabled(false);
				selectRNButton1.setEnabled(false);
				clearButton1.setEnabled(false);
				
				receiveDueDate.setEnable(false);
				
				ageFlagCheckBox.setEnabled(false);
				ageReprocessDate.setEnabled(false);
				break;
		}
		initTableColumns();
		title.setHTML(status.getLabel());
	}
	
	

	private PopupDescription popupDesc;

	private void initValidation() {
		validator = new DefaultValidationProcessor();
		ValidationMessages messages = new ValidationMessages();
		
		popupDesc = new PopupDescription(messages);

		validator.addValidators("dousEmpty", 
				new   Validator<Validator>(){

					@Override
					public ValidationResult validate(ValidationMessages messages) {
						if(requestsTable.getRowCount()<=0){
							return new ValidationResult("Список пуст, добавите очередника");
						}
						return null;
					}

					@Override
					public void invokeActions(ValidationResult result) {
//						new StyleAction("validationFailedBorder").invoke(result, choosedDouList);
						if(result!=null){
							ValidationError validationError = result.getErrors().get(0);
							showWarningNotification(""+validationError.error);
						}
					}}
//					.addActionForFailure(focusAction)
					
		);
	}


	private void initTableColumns() {
		/************** ПОРЯДКОВЫЙ НОМЕР ****************/
		int columnCount = requestsTable.getColumnCount();
		for(int i = columnCount - 1; i >= 0; i--)
			requestsTable.removeColumn(i);
		
		Column<RegisterLine, String> nColumn = new Column<RegisterLine, String>(
				new TextCell()) {
			@Override
			public String getValue(RegisterLine object) {
				return (requestsTable.getVisibleItems().indexOf(object)+1)+"";
			}
		};

		requestsTable.addColumn(nColumn, SafeHtmlUtils.fromSafeConstant("#"));
		requestsTable.setColumnWidth(nColumn,"35px");
		/************** ПОРЯДКОВЫЙ НОМЕР ****************/
		

		/************** ПРИЗНАК ДУБЛИРОВАНИЯ В ДРУГОМ ПРОТОКОЛЕ ****************/
		ImageCell horrorImageCell = new ImageCell(){
			
			@Override
		       public Set<String> getConsumedEvents() {
		           Set<String> consumedEvents = new HashSet<String>();
		           consumedEvents.add("click");
		           return consumedEvents;
		       }

			@Override
			public void onBrowserEvent(Context context, Element parent,
					String value, NativeEvent event,
					ValueUpdater<String> valueUpdater) {
				super.onBrowserEvent(context, parent, value, event, valueUpdater);
				
			    switch (DOM.eventGetType((Event) event)) {
		           case Event.ONCLICK:
		        	   RegisterLine registerLine = (RegisterLine) context.getKey();
		        	   if(registerLine.getWhereElseChildIs().isEmpty())
		        		   return;
		               String message = "РН "+registerLine.getDouRequest()+" присутствует в протоколах: ";
		               
		               List<SRegister> whereElseChildIs = registerLine.getWhereElseChildIs();
		               for (SRegister sRegister : whereElseChildIs) {
		            	   message += "<br/>"+ sRegister.getHUId().getDnumber();
		               }
		               showWarningNotification(message);
		           default:
		               break;
		           }
			}
		};
		
		
		Column<RegisterLine, String> horrorColumn = new Column<RegisterLine, String>(horrorImageCell) {
			@Override
			public String getValue(RegisterLine registerLine) {
				if(!registerLine.getWhereElseChildIs().isEmpty() && getPresenter().getRegister().getStatus() != RegisterStatus.THIRD){
					return "images/alarm24.png";
				}else{
					return "";
				}
			}};

		requestsTable.addColumn(horrorColumn);
		requestsTable.setColumnWidth(horrorColumn,"40px");
		/************** ПРИЗНАК ДУБЛИРОВАНИЯ В ДРУГОМ ПРОТОКОЛЕ ****************/
		
		
		
		/************** РН ****************/
//		#351: USABILITY: Возможность открыть РН по ссылке из протокола
		Column<RegisterLine, SafeHtml> regNumColumn = new Column<RegisterLine, SafeHtml>(
		        new ClickableSafeHtmlCell()) {
		    @Override
		    public SafeHtml getValue(RegisterLine line) {
		        SafeHtmlBuilder sb = new SafeHtmlBuilder();
		        sb.appendHtmlConstant("<a href='javascript:void(0);'>");
		        sb.appendEscaped(line.getDouRequest().toString());
		        sb.appendHtmlConstant("</a>");
		        return sb.toSafeHtml();
		    }
		};

		regNumColumn.setFieldUpdater(new FieldUpdater<RegisterLine, SafeHtml>() {
		        @Override
		        public void update(int index, RegisterLine line, SafeHtml value) {
		            HashMap<String, Serializable> p = new HashMap<String, Serializable>();
		            p.put("objectUId", line.getDouRequest().getUId());
					EQueueNavigation.instance().goToDouRequestEditPage(p);
		        }
		    });

		
		requestsTable.addColumn(regNumColumn, SafeHtmlUtils.fromSafeConstant("РН"));
		requestsTable.setColumnWidth(regNumColumn,"85px");
		/************** РН ****************/
		
		
		/************** ФИО очередника ****************/
		Column<RegisterLine, String> nameColumn = new Column<RegisterLine, String>(
				new TextCell()) {
			@Override
			public String getValue(RegisterLine object) {
				Physical child = object.getDouRequest().getChild();
				return child.getSurname()+" "+child.getName()+" "+child.getPatronymic();
			}
		};

		requestsTable.addColumn(nameColumn, SafeHtmlUtils.fromSafeConstant("ФИО"));
		/************** ФИО очередника ****************/
		
		
		/************** возраст ****************/
		Column<RegisterLine, String> ageColumn = new Column<RegisterLine, String>(
				new TextCell()) {
			@Override
			public String getValue(RegisterLine object) {
				String age = processAge(object, ageFlagCheckBox.getValue().booleanValue());
				return age;
			}
		};
		requestsTable.addColumn(ageColumn, SafeHtmlUtils.fromSafeConstant("Возраст"));
		/************** возраст ****************/
		
		
		/************** возрастная группа ****************/
//		для закрытого протокола просто показываем строку без возможности редактирования
		if(getPresenter().getRegister().getStatus() == RegisterStatus.THIRD){
			Column<RegisterLine, String> ageGroupColumn = new Column<RegisterLine, String>(
					new TextCell()) {
				@Override
				public String getValue(RegisterLine object) {
					if(object.getSRegister().getAgeGroupId() != null){
						DOUGroups ageGroup = getPresenter().getAgeGroupsMap().get(object.getSRegister().getAgeGroupId());
						return ageGroup.getFromToStr();
					} else {
						return null;
					}
				}
			};
			requestsTable.addColumn(ageGroupColumn, SafeHtmlUtils.fromSafeConstant("Возрастная группа"));
		} else {
			OptionsHolder<DOUGroups, RegisterLine> optionsHolder = new OptionsHolder<DOUGroups, RegisterLine>() {
				@Override
				public List<DOUGroups> getOptionsForVal(RegisterLine k) {
					final List<DOUGroups> ageGroups = new ArrayList<DOUGroups>();
					ageGroups.add(null);
					if(getPresenter().getAgeGroups() != null){
						ageGroups.addAll(getPresenter().getAgeGroups());
					}
					if(k.getSRegister().getRnAction() == Action.GIVE_PERMIT || k.getSRegister().getRnAction() == Action.GKP){
						return ageGroups;
					} else {
						return Collections.singletonList(null);
					}
				}
			};
		
			ToString<DOUGroups> toString = new ToString<DOUGroups>() {
				@Override
				public String toStr(DOUGroups o) {
					if(o == null){
						return "";
					}
					return o.getFromToStr();
				}
			};
			
			DynamicSelectionCell<DOUGroups,RegisterLine> dynamicSelectionCell = new DynamicSelectionCell<DOUGroups, RegisterLine>(optionsHolder, toString);
			Column<RegisterLine, DOUGroups> ageGroupColumn = new Column<RegisterLine, DOUGroups>(dynamicSelectionCell) {
				@Override
				public DOUGroups getValue(RegisterLine object) {
					if(object.getSRegister().getAgeGroupId() != null){
						Map<Integer, DOUGroups> ageGroupsMap = getPresenter().getAgeGroupsMap();
						return ageGroupsMap.get(object.getSRegister().getAgeGroupId());
					}
					return null;
				}
			};
		 
			ageGroupColumn.setFieldUpdater(new FieldUpdater<RegisterLine, DOUGroups>() {
				@Override
				public void update(int index, RegisterLine line, DOUGroups ageGroup) {
					if(ageGroup != null){
						line.getSRegister().setAgeGroupId(ageGroup.getId());
					} else {
						line.getSRegister().setAgeGroupId(null);
					}
				}
			});
			requestsTable.addColumn(ageGroupColumn, SafeHtmlUtils.fromSafeConstant("Возростная группа"));
		}
		
		
		
		
		
		
			
		/************** Список желаемых ДОУ ****************/
		Column<RegisterLine, SafeHtml> dousColumn = new Column<RegisterLine, SafeHtml>(new SafeHtmlCell()){

			@Override
			public SafeHtml getValue(RegisterLine object) {
				SafeHtmlBuilder safeHtmlBuilder = new SafeHtmlBuilder();
				safeHtmlBuilder.append(SafeHtmlUtils.fromSafeConstant("<span style='color: #00a100'>"));
				safeHtmlBuilder.append(SafeHtmlUtils.fromString(object.getDousByRating(3)));
				safeHtmlBuilder.append(SafeHtmlUtils.fromSafeConstant("</span>"));
				
				safeHtmlBuilder.append(SafeHtmlUtils.fromSafeConstant("<span style='color: #321db1'>"));
				safeHtmlBuilder.append(SafeHtmlUtils.fromString(object.getDousByRating(2)));
				safeHtmlBuilder.append(SafeHtmlUtils.fromSafeConstant("</span>"));
				
				safeHtmlBuilder.append(SafeHtmlUtils.fromSafeConstant("<span style='color: #b11d1d'>"));
				safeHtmlBuilder.append(SafeHtmlUtils.fromString(object.getDousByRating(1)));
				safeHtmlBuilder.append(SafeHtmlUtils.fromSafeConstant("</span>"));
				return safeHtmlBuilder.toSafeHtml();
			}};

		requestsTable.addColumn(dousColumn, SafeHtmlUtils.fromSafeConstant("Желаемые ДОУ"));
		/************** Список желаемых ДОУ ****************/
		
		
		if(getPresenter().getRegister().getStatus().getNumber() < 3){
			final List<Action> actions4waitStatus=new ArrayList<Action> ();
			actions4waitStatus.add(Action.DELETE_BY_7_YEAR);
			actions4waitStatus.add(Action.DELETE_BY_PARRENTS_AGREE);
			actions4waitStatus.add(Action.GIVE_PERMIT);
			actions4waitStatus.add(Action.GKP);
			actions4waitStatus.add(Action.REFUSAL);
			
			final List<Action> actions4GKP=new ArrayList<Action> ();
			actions4GKP.add(Action.DELETE_BY_7_YEAR);
			actions4GKP.add(Action.DELETE_BY_PARRENTS_AGREE);
			actions4GKP.add(Action.GIVE_PERMIT);
			actions4GKP.add(Action.REFUSAL);
			
			final List<Action> actionsNot4WaitStatus = Collections.singletonList(Action.BACK_TO_QUEUE);
			OptionsHolder<Action, RegisterLine> optionsHolder = new OptionsHolder<Action, RegisterLine>() {
				@Override
				public List<Action> getOptionsForVal(RegisterLine k) {
					if(k.getDouRequest().getStatus().equals(Status.WAIT)){
						if(k.getDouRequest().isGkp()){
							return actions4GKP;
						} else {
							return actions4waitStatus;
						}
					} else {
						return actionsNot4WaitStatus;
					}
				}
			};
		

			ToString<Action> toString = new ToString<Action>() {
				@Override
				public String toStr(Action o) {
					return o.getDescription();
				}
			};
			
			DynamicSelectionCell<Action,RegisterLine> dynamicSelectionCell = new DynamicSelectionCell<Action,RegisterLine>(optionsHolder, toString);
			Column<RegisterLine, Action> actionColumn = new Column<RegisterLine, Action>(dynamicSelectionCell) {
				@Override
				public Action getValue(RegisterLine object) {
					return object.getSRegister().getRnAction();
				}
			};
		
		 
			actionColumn.setFieldUpdater(new FieldUpdater<RegisterLine, Action>() {
				
				@Override
				public void update(int index, RegisterLine line, Action action) {
					line.getSRegister().setRnAction(action);
					switch(line.getSRegister().getRnAction()){
						case BACK_TO_QUEUE:
						case DELETE_BY_7_YEAR:
						case DELETE_BY_PARRENTS_AGREE:
						case REFUSAL:
							line.getSRegister().setDou(null);
						case GIVE_PERMIT:
						case GKP:
					}
					
					requestsTable.redraw();
					updateChildCountGrig();
				}
			});
			requestsTable.addColumn(actionColumn, SafeHtmlUtils.fromSafeConstant("Действие"));
		} else {
			Column<RegisterLine, String> actionColumn = new Column<RegisterLine, String>(
					new TextCell()) {
				@Override
				public String getValue(RegisterLine line) {
					return line.getSRegister().getRnAction().getDescription();
				}
			};

			requestsTable.addColumn(actionColumn, SafeHtmlUtils.fromSafeConstant("Действие"));
		}
		
		
		
//		#162: связанные комбобоксы Действие и Основание при редактированиии протокола
		ToString<Foundation> toString = new ToString<Foundation>() {

			@Override
			public String toStr(Foundation o) {
				return o.getDescription();
			}
		};
		OptionsHolder<Foundation, RegisterLine> optionHolder = new OptionsHolder<Foundation, RegisterLine>() {

			@Override
			public List<Foundation> getOptionsForVal(RegisterLine line) {
				List<Foundation>foundations = new ArrayList<Foundation>();
				switch (line.getSRegister().getRnAction()){
					case BACK_TO_QUEUE:
						foundations.addAll(Arrays.asList(Foundation.values()));
						break;
					case DELETE_BY_7_YEAR:
						foundations.add(Foundation.O1);
						foundations.add(Foundation.O2);
						foundations.add(Foundation.O3);
						foundations.add(Foundation.O4);
						break;
					case DELETE_BY_PARRENTS_AGREE:
						foundations.add(Foundation.O4);
						foundations.add(Foundation.O11);
						break;
					case GIVE_PERMIT:
					case GKP:
						foundations.addAll(Arrays.asList(Foundation.values()));
						break;
					case REFUSAL:
						foundations.add(Foundation.O1);
						foundations.add(Foundation.O4);
						break;
				}
				return foundations;
			}
		};
		DynamicSelectionCell<Foundation, RegisterLine> dynamicSelectionCell = new DynamicSelectionCell<Foundation, RegisterLine>(optionHolder, toString);
		Column<RegisterLine, Foundation> foundationColumn = new Column<RegisterLine, Foundation>(dynamicSelectionCell) {
			@Override
			public Foundation getValue(RegisterLine line) {
				return line.getSRegister().getFoundation();
			}
		};
		
//		final Foundation[] foundations_ = Foundation.values();
//		List<String>foundations=new ArrayList<String>(foundations_.length);
//		for (Foundation a : foundations_) {
//			foundations.add(a.getDescription());
//		}
//		Column<RegisterLine, String> foundationColumn = new Column<RegisterLine, String>(
//				new SelectionCell(foundations)) {
//			@Override
//			public String getValue(RegisterLine object) {
//				return object.getFoundation()==null? "--": object.getFoundation().getDescription();
//			}
//		};

		foundationColumn.setFieldUpdater(new FieldUpdater<RegisterLine, Foundation>() {
			
			@Override
			public void update(int index, RegisterLine line, Foundation value) {
				line.getSRegister().setFoundation(value);
			}
		});
		requestsTable.addColumn(foundationColumn, SafeHtmlUtils.fromSafeConstant("Основание"));
		
		
		
		
//		 ArrayList<String> options = new ArrayList<String>();
//		 options.add("111");
//		 options.add("222");
//		 options.add("333");
//		 options.add("444");
//		DynamicSelectionCell<Dou> categoryCell = new DynamicSelectionCell<Dou>(new  OptionsHolder<Dou>() {
//
//			@Override
//			public List<Dou> getOptions() {
//				return null;
//			}
//		});
		
		
		/************** ДОУ ****************/
		OptionsHolder<Dou, RegisterLine> optionsHolder = new OptionsHolder<Dou, RegisterLine>() {

			@Override
			public List<Dou> getOptionsForVal(RegisterLine line) {
//				List<SDouRequest> list = getPresenter().getAMap().get(k.getDouRequest());
//				 List<Dou> res=new ArrayList<Dou>(list.size());
//				 for (SDouRequest sdr : list) {
//					res.add((Dou) sdr.getArticleUnit());
//				}
				List<Dou>res = new ArrayList<Dou>();
				if(line.getSRegister().getRnAction()==Action.GIVE_PERMIT || line.getSRegister().getRnAction()==Action.GKP){
					List<Dou> allDou = getPresenter().getAllDou();
					Collections.sort(allDou);
					res.addAll(allDou);
					res.add(0, null);
				} 
				return res;
			}
		};
		ToString<Dou> douToString = new ToString<Dou>() {
			@Override
			public String toStr(Dou o) {
				if(o != null)
					return o.getNumber() + "";
				return "--";
			}
		};
		DynamicSelectionCell<Dou,RegisterLine> categoryCell = 
			new DynamicSelectionCell<Dou,RegisterLine>(optionsHolder,douToString) ;
		
		 Column<RegisterLine, Dou> douColumn = new Column<RegisterLine, Dou>(categoryCell) {
				@Override
				public Dou getValue(RegisterLine line) {
					if(line.getSRegister().getDou()==null)
						return null;
					return line.getSRegister().getDou();
				}
			};
			requestsTable.addColumn(douColumn, "МДОУ");
			douColumn.setFieldUpdater(new FieldUpdater<RegisterLine, Dou>() {
		      public void update(int index, RegisterLine line, Dou value) {
//		        Log.debug("update - "+value);
		    	  line.getSRegister().setDou(value);
		        updateChildCountGrig();
		      }
		    });
			requestsTable.setColumnWidth(douColumn,"110px");
			/************** ДОУ ****************/
			
			
			/************** наличие льготы ****************/
			Column<RegisterLine, String> privColumn = new Column<RegisterLine, String>(
					new TextCell()) {
				@Override
				public String getValue(RegisterLine object) {
					Privilege priv = object.getDouRequest().getPrivilege();
					return priv!=null?"есть" : ""; 
				}
			};

			requestsTable.addColumn(privColumn, SafeHtmlUtils.fromSafeConstant("Льгота"));
			/************** наличие льготы ****************/

//		requestsTable.addColumn(regNumColumn, SafeHtmlUtils.fromSafeConstant("МДОУ"));

		
			ImageCell imageCell = new ImageCell(){
				
				@Override
			       public Set<String> getConsumedEvents() {
			           Set<String> consumedEvents = new HashSet<String>();
			           consumedEvents.add("dblclick");
			           return consumedEvents;
			       }

				@Override
				public void onBrowserEvent(Context context, Element parent,
						String value, NativeEvent event,
						ValueUpdater<String> valueUpdater) {
					super.onBrowserEvent(context, parent, value, event, valueUpdater);
					
				    switch (DOM.eventGetType((Event) event)) {
			           case Event.ONDBLCLICK:
			               RegisterLine registerLine = (RegisterLine) context.getKey();
			               registerLine.setNewFlag(!registerLine.isNewFlag());
			               requestsTable.redraw();
			           default:
			               break;
			           }
				}
			};
			
			
			Column<RegisterLine, String> column = new Column<RegisterLine, String>(imageCell) {

				@Override
				public String getValue(RegisterLine registerLine) {
					if(registerLine.isNewFlag()){
						return "images/star_wow.png";
					}else{
						return "";
					}
				}};
			requestsTable.addColumn(column);
			requestsTable.setColumnWidth(column,"40px");
			
	}
	
	protected String processAge(RegisterLine regLine,Boolean ageFlag) {
		RegisterStatus registerStatus = getPresenter().getRegister().getStatus();
		String age="";
		//если стоит галка то на считаем на момент указаный в дата пикере
		if(ageFlag.booleanValue()) {
			age= AgeCalculator.calculateAge(
					regLine.getDouRequest().getChild().getBirthday(),ageReprocessDate.getValue()).toString();
		} else {
//			#349 В Сочи в протоколе не видно содержание
			if(regLine == null || regLine.getDouRequest() == null || regLine.getDouRequest().getChild().getBirthday() == null){
				return "ошибка";
			}
			Date birthday = regLine.getDouRequest().getChild().getBirthday();
			// если не стоит галка то
			// если протокол на первом или втором этапе то показываем текущий возраст
			if(registerStatus==RegisterStatus.FIRST||registerStatus==RegisterStatus.SECOND) {
				age= AgeCalculator.calculateAge(birthday, new Date()).toString();
			} else if(registerStatus==RegisterStatus.THIRD){
		    // если на третьем то возраст на момент перевода на третий этап
				age = AgeCalculator.calculateAge(birthday, getPresenter().getRegister().getRegisterDate()).toString();
			}
		}
		return age;
	}


	public void updateChildCountGrig(){
		childCountGrig.clear();
		
		ChildCount childCount = getPresenter().getChildCount();
		
		
		List<String> allDou = childCount.getAllDou();
		childCountGrig.resize(3, allDou.size()+2);
		childCountGrig.setWidget(0, 0, getWidget(""));
		childCountGrig.setWidget(1, 0, getWidget("Всего"));
		childCountGrig.setWidget(2, 0, getWidget("Льготников"));
		int i=1;
		for (String douId : allDou) {

			
			childCountGrig.setWidget(0, i, getWidget(douId+""));
			ChildCountLine byDou = childCount.getByDou(douId);
			childCountGrig.setWidget(1, i, getWidget(""+byDou.getAllCnt()));
			childCountGrig.setWidget(2, i, getWidget(""+byDou.getPrivelegesCnt()));
			i++;
		}
		childCountGrig.setWidget(0, i, getWidget("Всего"));
		ChildCountLine all = childCount.getAll();
		childCountGrig.setWidget(1, i, getWidget(""+all.getAllCnt()));
		childCountGrig.setWidget(2, i, getWidget(""+all.getPrivelegesCnt()));
	}
	
	IsWidget getWidget(String text){
		HTML widget = new HTML(text);
		widget.getElement().getStyle().setPaddingLeft(5, Unit.PX);
		widget.getElement().getStyle().setPaddingRight(5, Unit.PX);
		widget.getElement().getStyle().setPaddingBottom(2, Unit.PX);
		widget.getElement().getStyle().setPaddingTop(2, Unit.PX);
		return widget;
	}

	public RegisterEditPresenter getPresenter() {
		return presenter;
	}


	public void setPresenter(RegisterEditPresenter presenter) {
		this.presenter=presenter;
	}

	
	
	@UiHandler("selectRNButton")
	public void selectRNButton(ClickEvent event){
		HashMap<String, Serializable> params = new HashMap<String, Serializable>();
		params.put("state", State.SELECT_REQUESTS);
		params.put("registerEditPresenter",getPresenter());
		
		
		EQueueNavigation.instance().goToDouRequestsListPage(params);
	}
	
	
	@UiHandler("goBackButton")
	public void goBackButtonAction(ClickEvent event){
		getPresenter().goBack();
	}
	
	@UiHandler("goForwardButton")
	public void goForwardButtonAction(ClickEvent event){
		getPresenter().goForward();
	}
	
	
	@UiHandler("clearButton")
	public void clearButton(ClickEvent event){
		ageGroupValueListBox.setValue(null);
		getPresenter().clearRequests();
		updateChildCountGrig();
	}
	
	@UiHandler("cancelButton")
	public void cancel(ClickEvent event){
		doCancel();
	}


	/**
	 * Это действие выполняется при нажатии кнопки "К списку протоколов"
	 */
	private void doCancel() {
		if(isDirty()){
			Button save = new Button("Сохранить изменения", new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					boolean validate = validator.validate();
					if(validate)
						presenter.save(new Callback() {
							@Override
							public void callback() {
								EQueueNavigation.instance().goToRegistersListPage(null);
							}
						});
				}
			});
			
			
			Button dontSave = new Button("Не сохранять", new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					EQueueNavigation.instance().goToRegistersListPage(null);
				}
			});
			
		
			Button cancel = new Button("Продолжить редектирование", new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {}
			});
			
			
			DialogBox dialog = askQuestion("Возможно в протоколе есть несохраненные изиенения", 
					new HTML("Произвести сохренение?"),
					save,  dontSave,  cancel);
			dialog.center();
			dialog.setModal(true);
			dialog.show();
		} else {
			EQueueNavigation.instance().goToRegistersListPage(null);
		}
	}

	
	private boolean isDirty() {
		return getPresenter().getRegister().getStatus() != RegisterStatus.THIRD;
	}
	@UiHandler("saveButton")
	public void save(ClickEvent event){
		boolean validate = validator.validate();
		if(validate)
			presenter.save(null);
	}
	//////////////////////////////////////////
	@UiHandler("selectRNButton1")
	public void selectRNButton1(ClickEvent event){
		HashMap<String, Serializable> params = new HashMap<String, Serializable>();
		params.put("state", State.SELECT_REQUESTS);
		params.put("registerEditPresenter",getPresenter());
		
		
		EQueueNavigation.instance().goToDouRequestsListPage(params);
	}
	
	
	@UiHandler("goBackButton1")
	public void goBackButton1Action(ClickEvent event){
		getPresenter().goBack();
	}
	
	@UiHandler("goForwardButton1")
	public void goForwardButton1Action(ClickEvent event){
		getPresenter().goForward();
	}
	
	
	@UiHandler("clearButton1")
	public void clearButton1(ClickEvent event){
		ageGroupValueListBox.setValue(null);
		getPresenter().clearRequests();
		updateChildCountGrig();
	}
	
	@UiHandler("cancelButton1")
	public void cancel1(ClickEvent event){
		EQueueNavigation.instance().goToRegistersListPage(null);
	}

	
	@UiHandler("saveButton1")
	public void save1(ClickEvent event){
		boolean validate = validator.validate();
		if(validate)
			presenter.save(null);
	}

	
	public void setAgeGroups(List<DOUGroups> newValues){
		ArrayList<DOUGroups> values = new ArrayList<DOUGroups>();
		values.add(null);
		values.addAll(newValues);
		ageGroupValueListBox.setAcceptableValues(values);
	}
	
	
	public void setRequests(List<RegisterLine> values){
		Log.debug("setRequests("+values);
		
		Collections.sort(values,new Comparator<RegisterLine>() {

			@Override
			public int compare(RegisterLine o1, RegisterLine o2) {

				if(
						o1==null|| o1.getDouRequest()==null || // o1.getDouRequest(). getRegDate()==null || 
						
						o2==null|| o2.getDouRequest()==null //||  o2.getDouRequest(). getRegDate()==null 
												
						)
					return 0;
				
				
				Integer o1_year = Integer.valueOf(o1.getDouRequest().getRegDate().getYear()+1900) ;
				Integer o2_year = Integer.valueOf(o2.getDouRequest().getRegDate().getYear()+1900) ;
			
				Object oo1 =o1.getDouRequest().getPrivilege();
				Object oo2 =o2.getDouRequest().getPrivilege();
				
				int res=0;
				if (oo1 == null && oo2 != null)
					res = 1;
				else if (oo1 != null && oo2 == null)
					res = -1;
				else if (oo1 != null && oo2 != null || oo1 == null
						&& oo2 == null) {
                    if (o1.getDouRequest().getRegDate()==null || o2.getDouRequest().getRegDate()==null)
                    	return 0;
                    
					res = o1_year.compareTo(o2_year);

					if (res == 0) {
						res = o1.getDouRequest().getDnumber()
								.compareTo(o2.getDouRequest().getDnumber());
					}
				}
				
				return res;
			}
		});
		requestsTable.setRowData( values);
		updateChildCountGrig();
	}
	
   public boolean isAlarm() {
	   boolean b=false;
	   List<RegisterLine> registerLines = presenter.getTableData();
	   for(RegisterLine line : registerLines) {
		   if(!line.getWhereElseChildIs().isEmpty())
    		   return true;
	   }
	   return b;
   }
	
	public void progressStart(){
		if(progressBar!=null){
			progressBar.stop();
		}
		progressBar = new RepeatableProgressBar(3, 200);
		progressPanel.clear();
		progressPanel.add(progressBar);
	}
	
	public void progressStop(){
		if(progressBar!=null){
			progressBar.stop();
			progressBar = null;
		}
		progressPanel.clear();
	}

	public HasText getMember1(){
		return member1;
	}
	
	public HasValue<Date> getReceiveDueDate(){
		return receiveDueDate;
	}
	
	public HasText getMember2(){
		return member2;
	}
	
	public HasText getMember3(){
		return member3;
	}
	
	public HasValue<Date> getRegisterDate(){
		return registerDate;
	}
	
	public HasValue<Boolean> getAgeFlagCheckBox() {
		return ageFlagCheckBox;
	}
		
	public  HasValue<Date> getAgeReprocessDate() {
		return ageReprocessDate;
	}


	public void setOpenDate(String s){
		registerDatePanel.clear();
		registerDatePanel.add(new HTML( s));
	}
	public void setRegisterNumber(String s){
		numberRegisterPanel.clear();
		numberRegisterPanel.add(new HTML( s));
	}
	/**
	 * Метод делает кнопку "Сохранить" не активной
	 */
	public void disableSaveButton(){
		saveButton.setEnabled(false);
	}
	public void disableSave1Button(){
		saveButton1.setEnabled(false);
	}
	/**
	 * Метод делает кнопку "Сохранить" не активной
	 */
	public void enableSaveButton(){
		saveButton.setEnabled(true);
	}
	public void enableSave1Button(){
		saveButton1.setEnabled(true);
	}
	
	public void setWidgetsIfAgeReprocessDate(boolean b) {
	if (b) {
		ageFlagCheckBox.setValue(Boolean.TRUE);
		ageReprocessDate.setEnabled(true);
	}	
	else {
		ageFlagCheckBox.setValue(Boolean.FALSE);////
		ageReprocessDate.setEnabled(false);
	}
	
	}
	
}
