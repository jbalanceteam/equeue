package org.itx.jbalance.equeue.gwt.client.services;

import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.shared.ContractorsListDTO;
import org.itx.jbalance.equeue.gwt.shared.DeleteDouOperationResult;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.o.StaffDivision;
import org.itx.jbalance.l0.s.SFreeDivision;
import org.itx.jbalance.l0.s.SStaffDivision;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface ContragentsServiceAsync {

//	void getList(Long from, Long to, String findCriteria,
//			AsyncCallback<ArrayList<Juridical>> callback);
//	void getCount(String findCriteria,
//			AsyncCallback<Long> callback);
//	void getCountAll(AsyncCallback<Long> callback);

	void createNewJur(Juridical p, AsyncCallback<Void> callback);

	void deleteJur(Long p, AsyncCallback<Void> callback);

	void updateJur(Juridical p, AsyncCallback<Void> callback);
//	void getAllDouByReg(AsyncCallback<Map<Region,List<Dou>>> callback);
	void getAllDou(AsyncCallback<List<Dou>> callback);
//	void getAllHStaffDivision(AsyncCallback<ArrayList<HStaffDivision>> callback);
	//void createNewDou(Dou p,HStaffDivision doc,List<SStaffDivision> spec, AsyncCallback<Void> callback);
	void createNewDou(Dou p,HStaffDivision doc,List<StaffDivision> spec, AsyncCallback<Void> callback);
	void deleteDou(Long p, Boolean deleteAssociatedRequests, AsyncCallback<DeleteDouOperationResult> callback);
	void updateDou(HStaffDivision hStaffDivision, List<SStaffDivision> sStaffDivisionsEdited,
			List<SStaffDivision> sStaffDivisionsNew, AsyncCallback<OperationResultWrapper<String>> callback);
	void updateDouInList(HStaffDivision hStaffDivision, AsyncCallback<Void> callback);
	//void getAllJur(AsyncCallback<ArrayList<Juridical>> callback);
//	void getAllJuridical(AsyncCallback<ArrayList<Juridical>> asyncCallback);
	void getSStaffDivision(HStaffDivision hStaffDivision,
			AsyncCallback<List<SStaffDivision>> callback);
	void createNewFreeGroups(HStaffDivision doc,
			List<SFreeDivision> list, AsyncCallback<Void> callback);
	void updateNewFreeGroups(HStaffDivision doc, List<SFreeDivision> list,AsyncCallback<Void> callback);
	void deleteNewFreeGroups(HStaffDivision doc, AsyncCallback<Void> callback);
	//void getKm(HStaffDivision hStaffDivision, AsyncCallback<Integer> callback);
	//void getKg(HStaffDivision hStaffDivision, AsyncCallback<Integer> callback);
//	void getKm(AsyncCallback<Map<Integer, Integer>> callback);
//	void getKg(AsyncCallback<Map<Integer, Integer>> callback);
	 void getContractors(AsyncCallback<OperationResultWrapper<ContractorsListDTO>> callback);
	
	//void getAllGroupTypes(AsyncCallback<ArrayList<DOUGroups>> callback);
	

	



	

}
