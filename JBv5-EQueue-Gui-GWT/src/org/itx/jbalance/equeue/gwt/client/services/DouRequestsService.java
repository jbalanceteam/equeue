package org.itx.jbalance.equeue.gwt.client.services;

import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.shared.AddDouRequestOperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.CreateNewDouRequestRequest;
import org.itx.jbalance.equeue.gwt.shared.DouRequestDetailsDTO;
import org.itx.jbalance.equeue.gwt.shared.DouRequestListPrerequesedData;
import org.itx.jbalance.equeue.gwt.shared.GetReport5_1Request;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.Report5_1DTO;
import org.itx.jbalance.equeue.gwt.shared.Report5_2DTO;
import org.itx.jbalance.equeue.gwt.shared.Report5_3DTO;
import org.itx.jbalance.equeue.gwt.shared.UpdateDouRequestRequest;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("douRequests")
public interface DouRequestsService extends RemoteService {
	
	public DouRequestListPrerequesedData getDouRequestListPrerequesedData();
	
	public DouRequestsSearchResultsWrapper search(DouRequestsSearchParams searchParams);
	
	
	AddDouRequestOperationResultWrapper createNew(
			CreateNewDouRequestRequest createNewDouRequestRequest);
	OperationResultWrapper<HDouRequest> delete(Long id);
	OperationResultWrapper<HDouRequest> update(UpdateDouRequestRequest updateDouRequest);
	DouRequestDetailsDTO getDetails(Long id);
	
//	public Map<HDouRequest,List<SDouRequest>>  getById( List<Long>Ids);
	
	
	public List<Report5_1DTO>getReport5_1Data(GetReport5_1Request ages);
	public Report5_2DTO getReport5_2Data(Report5_2DTO dto);
	public Report5_3DTO getReport5_3Data(Report5_3DTO dto);


	/**
	 * #354
	 * Редактирование номера телефона из строки в списке очередников
	 * @param uid
	 * @param newPhone
	 * @return
	 */
	OperationResultWrapper<DouRequestDTO> updatePhoneNumber(Long uid,String newPhone);

	List<DouRequestDTO> getBadBdRecords();
	void fixBadBdRecords(Map<Long, String> map);
}
