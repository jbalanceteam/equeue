package org.itx.jbalance.equeue.gwt.client.request;


/**
 * Режим работы
 * @author apv
 *
 */
public enum State{
	/**
	 * Просмотр/редактирование заявок
	 */
	VIEW_REQUESTS, 
	/**
	 * Выбор заявок для формирование путевок(протокола заседание комиссии)
	 */
	SELECT_REQUESTS
}