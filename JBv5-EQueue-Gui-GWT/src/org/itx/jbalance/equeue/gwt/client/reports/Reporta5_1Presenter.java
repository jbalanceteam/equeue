package org.itx.jbalance.equeue.gwt.client.reports;

import org.itx.jbalance.equeue.gwt.client.MyDisplay;

public interface Reporta5_1Presenter {

	public final static int PAGE_SIZE=50;
	
	public interface Display extends MyDisplay{

	}

	/**
	 * #348: Добавить в отчете о состоянии очереди поле "дата"
	 */
	void updateOnDate();
}
