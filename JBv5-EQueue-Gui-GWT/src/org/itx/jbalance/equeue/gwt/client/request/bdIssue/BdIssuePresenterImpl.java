package org.itx.jbalance.equeue.gwt.client.request.bdIssue;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsService;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsServiceAsync;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * @author apv
 *
 */
public class BdIssuePresenterImpl extends MyPresenter<BdIssueWidget>
		implements BdIssuePresenter {

	private List<DouRequestDTO>badRecords = new ArrayList<DouRequestDTO>();

	public BdIssuePresenterImpl(BdIssueWidget display) {
		super(display);
	}

	private final DouRequestsServiceAsync service = GWT.create(DouRequestsService.class);

	@Override
	public void initialize() {
		updateTable(null);
	}
	
	private void updateTable(Date effectiveDate) {
		service.getBadBdRecords(new AsyncCallback<List<DouRequestDTO>>() {
			@Override
			public void onSuccess(List<DouRequestDTO> result) {
				filterBadRecords(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}
	
	private void filterBadRecords(List<DouRequestDTO> result) {
		badRecords.clear();
		for (DouRequestDTO douRequestDTO : result) {
			String date = formatDdmmyy(douRequestDTO);
			if(!douRequestDTO.getBirthdayFormated().equals(date)){
				badRecords.add(douRequestDTO);
			}
		}
		
		getDisplay().setReportData(badRecords);
	}

	private String formatDdmmyy(DouRequestDTO douRequestDTO) {
		final Date birthday = douRequestDTO.getBirthday();
		int year = (birthday.getYear() + 1900 ) % 2000;
		int month = birthday.getMonth() + 1;
		int day = birthday.getDate();
		String dayS = day + "";
		String monthS =  month + "";
		String yearS =  year + "";
		if(dayS.length() == 1){
			dayS = "0" + dayS;
		}
		if(monthS.length() == 1){
			monthS = "0" + monthS;
		}
		
		if(yearS.length() == 1){
			yearS = "0" + yearS;
		}
		String date= dayS +  "."  + monthS + "." + yearS;
		return date;
	}
	
	private String formatYyyy_MM_dd(Date date) {
		int year = date.getYear() + 1900 ;
		int month = date.getMonth() + 1;
		int day = date.getDate();
		String dayS = day + "";
		String monthS =  month + "";
		if(dayS.length() == 1){
			dayS = "0" + dayS;
		}
		if(monthS.length() == 1){
			monthS = "0" + monthS;
		}
		
		return year + "-"  + monthS + "-" + dayS;
	}

	@Override
	public void fixBirthdays() {
		Map<Long, String> toFix = new HashMap<Long, String> ();
		for (DouRequestDTO badRecord : badRecords) {
			toFix.put(badRecord.getUId(), formatYyyy_MM_dd(badRecord.getBirthday()));
		}
		service.fixBadBdRecords(toFix, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				getDisplay().showInfoNotification("Ура!");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}
}

