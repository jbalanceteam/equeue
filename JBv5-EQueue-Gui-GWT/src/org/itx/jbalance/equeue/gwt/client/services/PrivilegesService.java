package org.itx.jbalance.equeue.gwt.client.services;

import java.util.ArrayList;

import org.itx.jbalance.l0.o.Privilege;



import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("privileges")
public interface PrivilegesService extends RemoteService {
	public ArrayList<Privilege> getAll();
	public void createNew(Privilege p);
	void delete(Long p);
	void update(Privilege p);
}
