package org.itx.jbalance.equeue.gwt.client.request;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ActionCellWithTooltip;
import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.DateUtils;
import org.itx.jbalance.equeue.gwt.client.EQueueNavigation;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsSearchResultsWrapper;
import org.itx.jbalance.l0.h.RecordColor;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ActionCell.Delegate;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.CompositeCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.HasCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.ColumnSortList.ColumnSortInfo;
import com.google.gwt.user.cellview.client.RowStyles;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.DisclosurePanelImages;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;

public class HistoryListWidget extends ContentWidget implements DouRequestListPresenter.Display {

	interface Binder extends UiBinder<Widget, HistoryListWidget> {

	}

	 class RecordColorWrapper{
		 boolean withoutColor = false;
		 RecordColor recordColor;
	 }

	
	@UiField(provided = true)
	CellTable<DouRequestDTO> requestsTable = new CellTable<DouRequestDTO>(DouRequestListPresenter.PAGE_SIZE, MyCellTableResources.INSTANCE);


	
	DisclosurePanelHeader douSearchHeaderWidget;
	
	AsyncDataProvider<DouRequestDTO> provider;

	ChooseDouPanel chooseDouPanel;
	
	
	Renderer<Integer> intRenderer=  new AbstractRenderer<Integer>() {
    	@Override
		public String render(Integer object) {
    		if(object==null)
    			return "--";
			return object+"";
		}
	};
	
//	
//	@UiField(provided=false)
//	DisclosurePanel searchDouPanel;
//	
	
	final DisclosurePanelImages images = (DisclosurePanelImages)GWT.create(DisclosurePanelImages.class);

	class DisclosurePanelHeader extends HorizontalPanel {
	HTML html = new HTML();
	
	public DisclosurePanelHeader(boolean isOpen, String html){
	add(isOpen ? images.disclosurePanelOpen().createImage()
          : images.disclosurePanelClosed().createImage());
    this.html .setHTML(html);
    add(this.html);
}

	public HTML getHtml() {
		return html;
	}
}


	


	
	public HistoryListWidget() {
		super();
	}
	ProvidesKey<DouRequestDTO> keyProvider;
	@Override
	public void initialize() {
			
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		initTableColumns();
		initTableStyles();

		
		final SingleSelectionModel<DouRequestDTO> selectionModel=new SingleSelectionModel<DouRequestDTO>(keyProvider);
		
		requestsTable.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {

			}
		});
		
		requestsTable.setPageSize(DouRequestListPresenter.PAGE_SIZE);
	}

	
	

	private void initTableStyles() {
		requestsTable.setRowStyles(new RowStyles<DouRequestDTO>() {
			@Override
			public String getStyleNames(DouRequestDTO row, int rowIndex) {
				if(row!=null && row.getRecordColor()!=null)
					return row.getRecordColor().name();
				return null;
			}
		});
	}


	
	DouRequestListPresenter presenter;

	public DouRequestListPresenter getPresenter() {
		return presenter;
	}


	public void setPresenter(DouRequestListPresenter presenter) {
		this.presenter=presenter;
	}

	
	public void updateRowData(int start,List<DouRequestDTO>data){
		provider.updateRowData(start, data);
	}
	int start=0;
//	@Override
	public void showDouRequests(DouRequestsSearchResultsWrapper searchRes) {
		
		if(provider==null){
		provider = new AsyncDataProvider<DouRequestDTO>(keyProvider) {
		      @Override
		      protected void onRangeChanged(HasData<DouRequestDTO> display) {
		        start = display.getVisibleRange().getStart();
		        int end = start + display.getVisibleRange().getLength();
		        ColumnSortList columnSortList = requestsTable.getColumnSortList();
		        
		        List<SortedColumnInfo>sortedColumns=new LinkedList<SortedColumnInfo>();
//		        for(int i=0;i<columnSortList.size();i++){
		        if(columnSortList.size()>0)	
		        	sortedColumns.add(new SortedColumnInfo("rn", columnSortList.get(0).isAscending()));
		        else
		        	sortedColumns.add(new SortedColumnInfo("rn", false));
//		        	 columnSortList.get(i).getColumn().get
//		        }
		       
		        getPresenter().loadData(start,end,sortedColumns);
		        
		      }
		    };
		    provider.addDataDisplay(requestsTable);
		}else{
		
		}
		    
		 provider.updateRowCount(searchRes.getSearchCount(), true);
		 provider.updateRowData(0, searchRes.getList());
		 
////		requestsTable.setRowData(searchRes.getList());
//		searchResLabel.setHTML("Найдено по запросу <b>"+searchRes.getSearchCount() +"</b>.  Активная очередь <b>"+searchRes.getActiveCount() +"</b>.   Всего номеров в системе <b>"+searchRes.getAllCount()+"</b>.");
//		
	}


	
	
	
//	public HasConstrainedValue<Dou> getSearchedDou(){
//		return searchDouListBox;
//	}

	
	private void initTableColumns() {
//		ColumnSortList columnSortList = requestsTable.getColumnSortList();
		
		
		/* Dynamic number */
		Column<DouRequestDTO, String> dynNumColumn = new Column<DouRequestDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(DouRequestDTO object) {
				
				 return (requestsTable.getVisibleItems().indexOf(object)+1+start)+"";
			}
		};

		requestsTable.addColumn(dynNumColumn, SafeHtmlUtils.fromSafeConstant("#"));
		requestsTable.setColumnWidth(dynNumColumn,"10px");
		
		
		
		
		if(State.SELECT_REQUESTS == getPresenter().getState()){
			/* select request */
			Column<DouRequestDTO, Boolean> checkColumn = new Column<DouRequestDTO, Boolean>(
					new CheckboxCell()) {
				@Override
				public Boolean getValue(DouRequestDTO object) {
					return getPresenter().isSelected(object.getUId());
				}
			};
			checkColumn.setFieldUpdater(new FieldUpdater<DouRequestDTO, Boolean>() {
				@Override
				public void update(int index, DouRequestDTO object, Boolean value) {
					if(value){
						getPresenter().select(object.getUId());
					}else{
						getPresenter().deselect(object.getUId());
					}
					
				}
			});
			
			requestsTable.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("Включить в протокол"));
		}
		
		
		/* reg num */
		Column<DouRequestDTO, String> idColumn = new Column<DouRequestDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(DouRequestDTO object) {
				return object.getRegNumber();
			}
		};

		idColumn.setSortable(true);
		requestsTable.addColumn(idColumn, SafeHtmlUtils.fromSafeConstant("Рег. номер"));

		/* iss64:  init sorting */
		if(getPresenter().getState() == State.SELECT_REQUESTS){
			ColumnSortInfo sortedColumnInfo = new ColumnSortInfo(idColumn,true);
			requestsTable.getColumnSortList().push(sortedColumnInfo);
		}
		
		/* old reg num */
		Column<DouRequestDTO, String> oldRegNumColumn = new Column<DouRequestDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(DouRequestDTO object) {
				return object.getOldNumber();
			}
		};

		requestsTable.addColumn(oldRegNumColumn, SafeHtmlUtils.fromSafeConstant("Старый РН"));
		
		
		/* reg date */
		Column<DouRequestDTO, String> regDateColumn = new Column<DouRequestDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(DouRequestDTO object) {
				return DateUtils.format(object.getRegDate());
			}
		};

		requestsTable.addColumn(regDateColumn, SafeHtmlUtils.fromSafeConstant("Дата рег."));
		requestsTable.setColumnWidth(regDateColumn, "130px");

		
		
		
		
		/* Sverk date */
		Column<DouRequestDTO, String> sverkDateColumn = new Column<DouRequestDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(DouRequestDTO object) {
				return DateUtils.format(object.getSverkDate());
			}
		};

		requestsTable.addColumn(sverkDateColumn, SafeHtmlUtils.fromSafeConstant("Дата сверки"));
		requestsTable.setColumnWidth(sverkDateColumn, "130px");

				
		
		/* name */
	/*	Column<DouRequestDTO, String> nameColumn = new Column<DouRequestDTO, String>(
//				new EditTextCell()) {
				new TextCell()) {
			@Override
			public String getValue(DouRequestDTO object) {
				if(object.getChild()!=null){
					StringBuffer res=new StringBuffer(); 
					res.append(object.getChild().getSurname());
					res.append(" ");
					res.append(object.getChild().getName());
					res.append(" ");
					res.append(object.getChild().getPatronymic());
					return  res.toString();
				}else
					return null;
			}
		};

		requestsTable.addColumn(nameColumn,
				SafeHtmlUtils.fromSafeConstant("Имя"));
		
		*/
		
		
		
		
		
		
		
		
	Column<DouRequestDTO, String> nameColumn = new Column<DouRequestDTO, String>(
		new TextCell()) {
	@Override
	public String getValue(DouRequestDTO object) {
		return object.getSurname();
	}
};

requestsTable.addColumn(nameColumn,
		SafeHtmlUtils.fromSafeConstant("Фамилия"));
requestsTable.setColumnWidth(nameColumn, "150px");
//requestsTable.

Column<DouRequestDTO, String> name2Column = new Column<DouRequestDTO, String>(
		new TextCell()) {
	@Override
	public String getValue(DouRequestDTO object) {
		return object.getRealName();
	}
};

requestsTable.addColumn(name2Column,
		SafeHtmlUtils.fromSafeConstant("Имя"));
requestsTable.setColumnWidth(name2Column, "50px");




Column<DouRequestDTO, String> name3Column = new Column<DouRequestDTO, String>(
		new TextCell()) {
	@Override
	public String getValue(DouRequestDTO object) {
		return object.getPatronymic();
	}
};

requestsTable.addColumn(name3Column,
		SafeHtmlUtils.fromSafeConstant("Отчество"));

		


/* dous */
Column<DouRequestDTO, String> dous = new Column<DouRequestDTO, String>(
		new TextCell()) {
	@Override
	public String getValue(DouRequestDTO object) {
		return object.getDous();
	}
};

requestsTable.addColumn(dous,
		SafeHtmlUtils.fromSafeConstant("МДОУ"));
		/* age */
		Column<DouRequestDTO, String> ageColumn = new Column<DouRequestDTO, String>(
				new TextCell()) {

			@Override
			public String getValue(DouRequestDTO object) {
				if(object.getAge()!=null){
					 return  object.getAge()+""; 
				}else
					return null;
			}
		};

		requestsTable.addColumn(ageColumn,
				SafeHtmlUtils.fromSafeConstant("Возр."));
		
		
		
		
		/* birthday */
		
		Column<DouRequestDTO, String> birthdayColumn = new Column<DouRequestDTO, String>(new TextCell()){

			@Override
			public String getValue(DouRequestDTO object) {
				return object.getBirthdayFormated();
			}};	
		requestsTable.addColumn(birthdayColumn,
				SafeHtmlUtils.fromSafeConstant("Дата рожд."));
		
		
		
//		/* birth sertificate number */
//		Column<DouRequestDTO, String> birthSertNumColumn = new Column<DouRequestDTO, String>(
////				new EditTextCell()) {
//				new TextCell()) {
//			@Override
//			public String getValue(DouRequestDTO object) {
//				if(object.getChild()!=null && object.getBirthSertificate()!=null){
//					return object.getBirthSertificate().getSeria()+" "+ object.getBirthSertificate().getNumber();
//				}else
//					return null;
//			}
//		};
//
//		requestsTable.addColumn(birthSertNumColumn,
//				SafeHtmlUtils.fromSafeConstant("№ свидетельства"));
		
		
		/* phone number */
		Column<DouRequestDTO, String> phoneNumColumn = new Column<DouRequestDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(DouRequestDTO object) {
				return object.getPhone();
			}
		};

		requestsTable.addColumn(phoneNumColumn,
				SafeHtmlUtils.fromSafeConstant("Тел."));
		
		/* privilege */
		Column<DouRequestDTO, String> privilegeColumn = new Column<DouRequestDTO, String>(
//				new EditTextCell()) {
				new TextCell()) {
			
			
			@Override
			public String getValue(DouRequestDTO object) {
				if(object.getPrivilege()!=null){
					return object.getPrivilege().getName();
				}else
					return null;
			}
		};
		
		requestsTable.addColumn(privilegeColumn,
				SafeHtmlUtils.fromSafeConstant("Льгота"));
		
		
		
		/* privilege */
		Column<DouRequestDTO, String> yearColumn = new Column<DouRequestDTO, String>(
//				new EditTextCell()) {
				new TextCell()) {
			
			
			@Override
			public String getValue(DouRequestDTO object) {
				if(object.getYear()!=null){
					return object.getYear()+"";
				}else
					return null;
			}
		};
		
		requestsTable.addColumn(yearColumn,
				SafeHtmlUtils.fromSafeConstant("Год поступ."));
		
		
//		/*********     Статус     ***********/
//		Column<SRegister, String> statusColumn = new Column<SRegister, String>(
//				new TextCell()) {
//			@Override
//			public String getValue(SRegister object) {
//				return object.getDouRequest().getStatus().getDescription();
//			}
//		};
//
//		requestsTable.addColumn(statusColumn , SafeHtmlUtils.fromSafeConstant("Статус"));
		
//		nameColumn.setFieldUpdater(new FieldUpdater<Privilege, String>() {
//
//			@Override
//			public void update(int index, Privilege object, String value) {
//				object.setName(value);
//				getPresenter().update(object);
//
//			}
//
//		});
//
//		/* description */
//		Column<Privilege, String> descriptionColumn = new Column<Privilege, String>(
//				new EditTextCell()) {
//			@Override
//			public String getValue(Privilege object) {
//				return object.getDescription() + "";
//			}
//		};
//
//		requestsTable.addColumn(descriptionColumn,
//				SafeHtmlUtils.fromSafeConstant("Описание"));
//		descriptionColumn
//				.setFieldUpdater(new FieldUpdater<Privilege, String>() {
//
//					@Override
//					public void update(int index, Privilege object, String value) {
//						object.setDescription(value);
//
//					}
//
//				});
//	
//	
//		
		
		if(State.VIEW_REQUESTS == getPresenter().getState()){ 
			/* actions */
			List<HasCell<DouRequestDTO, ?>>  hasCells = new ArrayList<HasCell<DouRequestDTO, ?>>();
			
			
			/* edit */ 
			ActionCell<DouRequestDTO> editCell = new ActionCellWithTooltip<DouRequestDTO>(
					SafeHtmlUtils.fromSafeConstant("<img src='images/edit.png' alt='Редактировать' />"),new Delegate<DouRequestDTO>() {
			
				@Override
				public void execute(DouRequestDTO p) {
					HashMap<String, Serializable> params = new HashMap<String, Serializable>();
					params.put("objectUId", p.getUId());
					EQueueNavigation.instance().goToDouRequestEditPage(params);
				}
			},"Редактировать запись");
			
			
			Column <DouRequestDTO,DouRequestDTO>editColumn =new Column<DouRequestDTO,DouRequestDTO>(
					editCell){
					@Override
					public DouRequestDTO getValue(DouRequestDTO object) {
						return object;
					}}; 
			hasCells.add(editColumn);		
			
			/* delete */ 
			Delegate<DouRequestDTO> delDelegate = new Delegate<DouRequestDTO>() {

				@Override
				public void execute(DouRequestDTO p) {
					if(Window.confirm("Удалить безвозвратно?")){
							getPresenter().delete(p.getUId());
					}
				}
			};
			ActionCell<DouRequestDTO> delCell = new ActionCellWithTooltip<DouRequestDTO>(
					SafeHtmlUtils.fromSafeConstant("<img src='images/delete.png' alt='Удалить' title='Удалить запись без возможности восстановления' />"),delDelegate,
					"Удалить запись без возможности восстановления.");
		
			
			Column <DouRequestDTO,DouRequestDTO>delColumn =new Column<DouRequestDTO,DouRequestDTO>(
				delCell){
					@Override
					public DouRequestDTO getValue(DouRequestDTO object) {
						return object;
					}
			}; 

			hasCells.add(delColumn);		
					
					
			
//			/* permit */ 
//			ActionCell<DouRequestDTO> exportCell
//			= new ActionCell<DouRequestDTO>(SafeHtmlUtils.fromSafeConstant("<img src='images/export.png' alt='Экспорт'/>"),new Delegate<DouRequestDTO>() {
//				@Override
//				public void execute(DouRequestDTO p) {
//					Window.al1ert("Ожидайте в следующей версии");
//				}
//			});
//			
//			
//			Column <DouRequestDTO,DouRequestDTO>exportColumn =new Column<DouRequestDTO,DouRequestDTO>(
//					exportCell){
//					@Override
//					public DouRequestDTO getValue(DouRequestDTO object) {
//						return object;
//					}}; 
//					
//			hasCells.add(exportColumn);
			
			/*  */ 
			ActionCell<DouRequestDTO> printCell = new ActionCellWithTooltip<DouRequestDTO>(
					SafeHtmlUtils.fromSafeConstant("<img src='images/export.png' alt='Экспорт'/>"),new Delegate<DouRequestDTO>() {
				@Override
				public void execute(DouRequestDTO p) {
					String urlStr = "equeue/report?type=registrationNotification";
					urlStr+="&hDouRequestUidParam="+p.getUId().toString();
					Window.open(urlStr, "blank_", null);
				}
			},"Вывести на печать уведомление о постановки в очередь");
			
			
			Column <DouRequestDTO,DouRequestDTO>printColumn =new Column<DouRequestDTO,DouRequestDTO>(
					printCell){
					@Override
					public DouRequestDTO getValue(DouRequestDTO object) {
						return object;
					}}; 
			hasCells.add(printColumn);		
			
					
					
			/*  */ 
			ActionCell<DouRequestDTO> historyCell = new ActionCellWithTooltip<DouRequestDTO>(
					SafeHtmlUtils.fromSafeConstant("<img src='images/history.png' alt='История радактирования'/>"),new Delegate<DouRequestDTO>() {
				@Override
				public void execute(DouRequestDTO p) {
					showInfoNotification("Здесь будет история...");
				}
			},"История радактирования");
			
			
			Column <DouRequestDTO,DouRequestDTO>historyColumn =new Column<DouRequestDTO,DouRequestDTO>(
					historyCell){
					@Override
					public DouRequestDTO getValue(DouRequestDTO object) {
						return object;
					}}; 					
					
			hasCells.add(historyColumn);
			
			
			CompositeCell<DouRequestDTO> cell=new CompositeCell<DouRequestDTO>(	hasCells );
			
			
			Column <DouRequestDTO,DouRequestDTO>actionColumn =new Column<DouRequestDTO,DouRequestDTO>(cell){
			
					@Override
					public DouRequestDTO getValue(DouRequestDTO object) {
						return object;
					}}; 
					
			requestsTable.addColumn(actionColumn, SafeHtmlUtils.fromSafeConstant("Действие"));
			requestsTable.setColumnWidth(actionColumn, "118px");
		}	else {
			
		}
		
	}
	

	
	
	 class RecordColorRenderer extends AbstractRenderer<RecordColorWrapper> {
			private String nullString="Все";
			
			public RecordColorRenderer() {
				super();
			}
			@Override
			public String render(RecordColorWrapper o) {
				if(o==null){
					return nullString;
				}else if(o.withoutColor){
					return "Без цвета";
				} else {
					return o.recordColor.getColorName();
				}
			}
	 }


	public List<Long> getSearchedDou() {
		if(chooseDouPanel==null || chooseDouPanel.isAllSelected() || chooseDouPanel.isAllUnselected())
			return null;
		List<Dou> selectedDou = chooseDouPanel.getSelectedDou();
		List<Long> res= new ArrayList<Long>();
		for (Dou dou : selectedDou) {
			res.add(dou.getUId());
		}
		return res;
	}
	 
	
	public  void setSearchedDou(List<Long> doueId) {
		if(chooseDouPanel==null)
			return ;
		chooseDouPanel.selectByUids(doueId, true);
	}
	
	
	 
	 
	
}
