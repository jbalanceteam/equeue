package org.itx.jbalance.equeue.gwt.client.services;

import java.util.ArrayList;

import org.itx.jbalance.l0.o.Privilege;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface PrivilegesServiceAsync {

	void getAll(AsyncCallback<ArrayList<Privilege>> callback);

	void createNew(Privilege p, AsyncCallback<Void> callback);
	
	void delete(Long p, AsyncCallback<Void> callback);

	void update(Privilege p, AsyncCallback<Void> callback);

}
