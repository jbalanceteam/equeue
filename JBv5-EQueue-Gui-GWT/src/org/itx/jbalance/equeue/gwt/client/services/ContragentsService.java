package org.itx.jbalance.equeue.gwt.client.services;

import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.shared.ContractorsListDTO;
import org.itx.jbalance.equeue.gwt.shared.DeleteDouOperationResult;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.o.StaffDivision;
import org.itx.jbalance.l0.s.SFreeDivision;
import org.itx.jbalance.l0.s.SStaffDivision;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("contragents")
public interface ContragentsService extends RemoteService {
//	public ArrayList<Juridical> getList(Long from, Long to, String findCriteria);
//	public Long getCount(String findCriteria);
//	public Long getCountAll();
	public void createNewJur(Juridical p);
	//public void createNewDou(Dou p,HStaffDivision doc,List<SStaffDivision> spec);
	public void createNewDou(Dou p,HStaffDivision doc, List<StaffDivision> divisions );
	void deleteJur(Long p);
	DeleteDouOperationResult deleteDou(Long p,Boolean deleteAssociatedRequests);
	void updateJur(Juridical p);
	OperationResultWrapper<String> updateDou(HStaffDivision hStaffDivision, List<SStaffDivision> staffDivisionsEdited,List<SStaffDivision> staffDivisionsNew);
	void updateDouInList(HStaffDivision hStaffDivision);
		
//	public Map<Region,List<Dou>> getAllDouByReg();
	public List<Dou> getAllDou();
//	public ArrayList<HStaffDivision> getAllHStaffDivision();
	//public ArrayList<Juridical> getAllJur();
	//public ArrayList<DOUGroups> getAllGroupTypes();
//	ArrayList<Juridical> getAllJuridical();
	List<SStaffDivision> getSStaffDivision(HStaffDivision hStaffDivision);
	void createNewFreeGroups(HStaffDivision doc, List<SFreeDivision> list);
	void updateNewFreeGroups(HStaffDivision doc, List<SFreeDivision> list);
	void deleteNewFreeGroups(HStaffDivision doc);
	
	OperationResultWrapper<ContractorsListDTO> getContractors();
	
	//public int getKm(HStaffDivision hStaffDivision);
	//public int getKg(HStaffDivision hStaffDivision);
//	Map<Integer, Integer> getKm();
//	Map<Integer, Integer> getKg();
}
