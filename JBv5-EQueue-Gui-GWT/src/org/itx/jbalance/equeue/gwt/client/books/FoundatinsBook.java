package org.itx.jbalance.equeue.gwt.client.books;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.equeue.gwt.client.services.RegisterService;
import org.itx.jbalance.equeue.gwt.client.services.RegisterServiceAsync;
import org.itx.jbalance.l0.s.SRegister.Foundation;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.VerticalPanel;


public class FoundatinsBook extends ContentWidget {
 
	private final RegisterServiceAsync service = GWT.create(RegisterService.class);
	CellTable<Foundation> table=new CellTable<Foundation>(0,MyCellTableResources.INSTANCE);
  /**
   * Constructor.
   *
   * @param constants the constants
   */
  public FoundatinsBook() {

  }

  /**
   * Initialize this example.
   */

  @Override
  public void initialize() {
    // Create a vertical panel to align the check boxes
    VerticalPanel vPanel = new VerticalPanel();
    
    
    Column <Foundation,String>idColumn =new Column<Foundation,String>(
	    	new TextCell()){
				@Override
				public String getValue(Foundation object) {
					return object.getShortName()+"";
				}}; 
				
    table.addColumn(idColumn, SafeHtmlUtils.fromSafeConstant("Короткое наименование"));
    
    
    Column <Foundation,String>nameColumn =new Column<Foundation,String>(
	    	new TextCell()){
				@Override
				public String getValue(Foundation object) {
					return object.getDescription();
				}}; 
				
    table.addColumn(nameColumn, SafeHtmlUtils.fromSafeConstant("Подсказка"));
   
    
    initTable();
    vPanel.add(table);
    initWidget(vPanel);
  }

  
  private void initTable() {
	  service.getFoundations(new AsyncCallback<List<Foundation>>() {
			@Override
			public void onSuccess(List<Foundation> result) {
				table.setRowData(result);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				showExceptionNotification(caught);			
			}
		});
		
	}

}
