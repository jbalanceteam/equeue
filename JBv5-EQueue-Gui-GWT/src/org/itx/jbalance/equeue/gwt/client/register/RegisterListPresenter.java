package org.itx.jbalance.equeue.gwt.client.register;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyDisplay;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l2_api.dto.equeue.RegisterDTO;

public interface RegisterListPresenter {
	public final static int PAGE_SIZE=50;
	
	public interface Display extends MyDisplay{
//		public void showDouRequests(SearchResultWrapper<HRegister> res);


	}
	
	
	void initialize();
	/**
	 * Run search and populate requests table on the View
	 * @param sortedColumns 
	 * @param end 
	 * @param start 
	 */
	void loadData(int start, int end, List<SortedColumnInfo> sortedColumns);
	
	void populateDetails(RegisterDTO r);
	
	void delete(Long id);
	void search();
	
}
