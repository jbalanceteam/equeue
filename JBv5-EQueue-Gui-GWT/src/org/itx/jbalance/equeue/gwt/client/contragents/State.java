package org.itx.jbalance.equeue.gwt.client.contragents;


/**
 * Режим работы
 * @author budnikov
 *
 */
public enum State{
	/**
	 * Создание нового МДОУ
	 */
	NEW_DOU,
	/**
	 * Создание нового Контрагента
	 */
	NEW_CONTRAGENT,
	/**
	 * Редактирование ДОУ
	 */
	EDIT_DOU,
	/**
	 * Редактирование Контрагента
	 */
	EDIT_CONTRAGENT
}