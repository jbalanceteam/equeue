package org.itx.jbalance.equeue.gwt.client.request;


import java.util.Date;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.client.Callback;
import org.itx.jbalance.equeue.gwt.client.MyDisplay;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.Region;

import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;


public interface DouRequestEditPresenter {

	public interface Display extends MyDisplay{
		HasText getLastName();
		HasText getFirstName();
		HasText getPatronymic();
		HasValue<Date> getBirthDate();
		HasText getBsSeriaBox();
		HasText getBsNumberBox();
		HasValue<Date> getBsDateBox();
//		#942
//		HasText getGuardian1();
//		HasText getGuardian2();
		Privilege getPrivilege();
		
		void setAvailableDou(Map<Region, List <Dou>> availableDou);
//		void setChoosedDou  (List <Dou> availableDou);
		
//		Button addAllDouButton;
//		Button addOneDouButton;
//		Button removeOneDouButton;
//		Button removeAllDouButton;
//		Button saveButton;
//		Button cancelButton;
	}
	
	void save();
//	void setId(Long uid);
	void setObjectUId(Long uid);
	void initialize(Callback c);
	
//	void chooseOneDou(Dou dou);
//	void chooseAllDou();
//	void removeOneDou(Dou dou);
//	void removeAllDou();
	
	List<Privilege> getAllPrivileges();
//	List<Dou> getAvailableDous();
//	List<Dou> getChoosedDous();
	void populateFormUsingXml(String results);
}
