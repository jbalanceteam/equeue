package org.itx.jbalance.equeue.gwt.client.regions;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyDisplay;
import org.itx.jbalance.l0.o.Region;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.HasText;

public interface RegionsPresenter {

	public interface Display extends MyDisplay{
		
		public HasText getRegionsName();

		public HasText getDescription();

		public void showRegions(List<Region> list);
		
		public HasClickHandlers getSaveClickHandlers();
	}
	
	void create();
	void delete(Long id);
	void update(Region region);
}
