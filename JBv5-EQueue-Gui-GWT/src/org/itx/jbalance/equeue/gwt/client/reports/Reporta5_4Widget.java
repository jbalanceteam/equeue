package org.itx.jbalance.equeue.gwt.client.reports;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.RepeatableProgressBar;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.equeue.gwt.shared.Report5_1DTO;

import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

public class Reporta5_4Widget extends ContentWidget implements Reporta5_1Presenter.Display {

	interface Binder extends UiBinder<Widget, Reporta5_4Widget> {

	}

	@UiField(provided=true)
	CellTable<Report5_1DTO> table=new CellTable<Report5_1DTO>(0,MyCellTableResources.INSTANCE);
	
	
	@UiField
	SimplePanel progressPanel;
	
	RepeatableProgressBar progressBar;
	
	@Override
	public void initialize() {
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		initTableColumns();
		progressBar = new RepeatableProgressBar();
		progressPanel.add(progressBar);
	}

	
	
	private void initTableColumns(){
		/* Age */
		Column<Report5_1DTO, String> ageColumn = new Column<Report5_1DTO, String>(
				new TextCell()) {
			@Override
			public String getValue(Report5_1DTO o) {
				
				 Integer from = o.getFrom();
				return from +" - "+o.getTo()+" мес.";
			}
		};

		table.addColumn(ageColumn, SafeHtmlUtils.fromSafeConstant("Возраст"));
//		table.setColumnWidth(ageColumn,"10px");
		

		
		/* Age */
		Column<Report5_1DTO, String> countColumn = new Column<Report5_1DTO, String>(
				new TextCell()) {
			@Override
			public String getValue(Report5_1DTO o) {
				
				 return o.getCount()+"";
			}
		};

		table.addColumn(countColumn, SafeHtmlUtils.fromSafeConstant("Количество детей в очереди"));
//		table.setColumnWidth(ageColumn,"10px");

		
		
	}
	
	public void setReportData(List<Report5_1DTO>data){
		table.setRowData(data);
		progressBar.stop();
		progressBar = null;
		progressPanel.clear();
	}
	
}
