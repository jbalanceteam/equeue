package org.itx.jbalance.equeue.gwt.client.services;

import java.util.ArrayList;

import org.itx.jbalance.l0.o.Region;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface RegionsServiceAsync {

	void getAll(AsyncCallback<ArrayList<Region>> callback);

	void createNew(Region p, AsyncCallback<Void> callback);
	
	void delete(Long p, AsyncCallback<Void> callback);

	void update(Region p, AsyncCallback<Void> callback);

}
