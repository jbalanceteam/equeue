package org.itx.jbalance.equeue.gwt.client.services;

import java.util.ArrayList;

import org.itx.jbalance.l0.o.JuridicalType;


import com.google.gwt.user.client.rpc.AsyncCallback;

public interface JuridicalTypesServiceAsync {

	void getAll(AsyncCallback<ArrayList<JuridicalType>> callback);

}
