package org.itx.jbalance.equeue.gwt.client.contragents;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ActionCellWithTooltip;
import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.EQueueNavigation;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.Juridical;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.CompositeCell;
import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.HasCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.cell.client.ActionCell.Delegate;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.Window;
//import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SingleSelectionModel;

public class ContragentListWidget extends ContentWidget implements ContragentListPresenter.Display {

	interface Binder extends UiBinder<Widget, ContragentListWidget> {

	}

	@UiField(provided = true)
    CellTable<Juridical> contrListCellTable;
	@UiField(provided = true)
    CellTable<HStaffDivision> douListCellTable;
	
	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public ContragentListWidget() {
	}

	@Override
	public void initialize() {
		douListCellTable = new CellTable<HStaffDivision>(0,MyCellTableResources.INSTANCE);
		contrListCellTable = new CellTable<Juridical>(0,MyCellTableResources.INSTANCE);
		// // Create the UiBinder.
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		initDouTableColumns();
		ProvidesKey<HStaffDivision> keyProvider=new ProvidesKey<HStaffDivision>() {
			@Override
			public Object getKey(HStaffDivision item) {
				return item.getUId();
			}
		}; 
		final SingleSelectionModel<HStaffDivision> selectionModel=new SingleSelectionModel<HStaffDivision>(keyProvider);
		douListCellTable.setSelectionModel(selectionModel);
		// contr
		initContrTableColumns();
		ProvidesKey<Juridical> contrKeyProvider=new ProvidesKey<Juridical>() {
			@Override
			public Object getKey(Juridical item) {
				return item.getUId();
			}
		}; 
		final SingleSelectionModel<Juridical> contrSelectionModel=new SingleSelectionModel<Juridical>(contrKeyProvider);
		contrListCellTable.setSelectionModel(contrSelectionModel);
		
		
		
	}

	private void initDouTableColumns() {

		/* Номер ДОУ number */
		Column<HStaffDivision, String> douNumberColumn = new Column<HStaffDivision, String>(
				new EditTextCell()) {
			@Override
			public String getValue(HStaffDivision object) {
				return object.getDou().getNumber() + "";
			}
		};
		douListCellTable.addColumn(douNumberColumn,
				SafeHtmlUtils.fromSafeConstant("Номер МДОУ"));
		
		douNumberColumn.setFieldUpdater(new FieldUpdater<HStaffDivision, String>() {
			@Override
			public void update(int index, HStaffDivision object, String value) {
				object.getDou().setNumber(value); 
				getPresenter().update(object);
			}
		});
		
		/* name */
		Column<HStaffDivision, String> nameColumn = new Column<HStaffDivision, String>(
				new EditTextCell()) {
			@Override
			public String getValue(HStaffDivision object) {
				return object.getDou().getName() + "";
			}
		};
		douListCellTable.addColumn(nameColumn,
				SafeHtmlUtils.fromSafeConstant("Имя МДОУ"));
		
		nameColumn.setFieldUpdater(new FieldUpdater<HStaffDivision, String>() {
			@Override
			public void update(int index, HStaffDivision object, String value) {
				object.getDou().setName(value); 
				getPresenter().update(object);
			}
		});
		
		/* Тип МДОУ*/
		Column<HStaffDivision, String> typeDouColumn = new Column<HStaffDivision, String>(
				new EditTextCell()) {
			@Override
			public String getValue(HStaffDivision object) {
				return object.getDou().getDouType();
			}
		};
		douListCellTable.addColumn(typeDouColumn,
				SafeHtmlUtils.fromSafeConstant("Тип МДОУ"));
		
		/* inn */
//		Column<HStaffDivision, String> innColumn = new Column<HStaffDivision, String>(
//				new EditTextCell()) {
//			@Override
//			public String getValue(HStaffDivision object) {
//				return object.getDou().getINN() + "";
//			}
//		};
//		douListCellTable.addColumn(innColumn,
//				SafeHtmlUtils.fromSafeConstant("ИНН"));
//		
//		innColumn.setFieldUpdater(new FieldUpdater<HStaffDivision, String>() {
//			@Override
//			public void update(int index, HStaffDivision object, String value) {
//				object.getDou().setINN(value); 
//				getPresenter().update(object);
//			}
//		});

		/* director */
//		 Column<Juridical, String> directorColumn = new Column<Juridical,
//		 String>(
//		 new EditTextCell()) {
//		 @Override
//		 public String getValue(Juridical object) {
//		 return object. + "";
//		 }
//		 };
//		 contragentListCellTable.addColumn(directorColumn,
//		 SafeHtmlUtils.fromSafeConstant("Директор"));

		/* telephone */
		Column<HStaffDivision, String> telColumn = new Column<HStaffDivision, String>(new TextCell()) {
			@Override
			public String getValue(HStaffDivision object) {
				return object.getDou().getPhone();
			}
		};
		 douListCellTable.addColumn(telColumn, SafeHtmlUtils.fromSafeConstant("Контактный телефон"));

//		 telColumn.setFieldUpdater(new FieldUpdater<HStaffDivision, String>() {
//				@Override
//				public void update(int index, HStaffDivision object, String value) {
//					object.getDou().setPhone(value); 
//					getPresenter().update(object);
//				}
//			});
		 
		 
		 /* Район */
		Column<HStaffDivision, String> regionColumn = new Column<HStaffDivision, String>(
				new TextCell()) {
			@Override
			public String getValue(HStaffDivision object) {
				return object.getDou().getRegion() == null?  "" : object.getDou().getRegion().getName();
			}
		};
		douListCellTable.addColumn(regionColumn,SafeHtmlUtils.fromSafeConstant("Район"));
		 
		/* Физ Адрес */
		Column<HStaffDivision, String> fizAddrColumn = new Column<HStaffDivision, String>(
				new EditTextCell()) {
			@Override
			public String getValue(HStaffDivision object) {
				return object.getDou().getPAddress() + "";
			}
		};
		douListCellTable.addColumn(fizAddrColumn,
				SafeHtmlUtils.fromSafeConstant("Физ Адрес"));
		
		fizAddrColumn.setFieldUpdater(new FieldUpdater<HStaffDivision, String>() {
			@Override
			public void update(int index, HStaffDivision object, String value) {
				object.getDou().setPAddress(value); 
				getPresenter().update(object);
			}
		});
		/* КМ - обшее количество мест в ДОУ во всех группах */
		Column<HStaffDivision, String> kmColumn = new Column<HStaffDivision, String>(
				new EditTextCell()) {
			@Override
			public String getValue(HStaffDivision object) {
				return object.getKm().toString();
			}
		};
		douListCellTable.addColumn(kmColumn,
				SafeHtmlUtils.fromSafeConstant("КМ"));
		
//		kmColumn.setFieldUpdater(new FieldUpdater<HStaffDivision, String>() {
//			@Override
//			public void update(int index, HStaffDivision object, String value) {
//				object.getDou().setPAddress(value); 
//				getPresenter().update(object);
//			}
//		});

		/* КГ - количество групп  в ДОУ*/
		Column<HStaffDivision, String> kgColumn = new Column<HStaffDivision, String>(
				new EditTextCell()) {
			@Override
			public String getValue(HStaffDivision object) {
				return object.getKg().toString();
			}
		};
		douListCellTable.addColumn(kgColumn,
				SafeHtmlUtils.fromSafeConstant("КГ"));
		
//		kgColumn.setFieldUpdater(new FieldUpdater<HStaffDivision, String>() {
//			@Override
//			public void update(int index, HStaffDivision object, String value) {
//				object.getDou().setPAddress(value); 
//				getPresenter().update(object);
//			}
//		});
		
		/* Юр Адрес */
//		Column<HStaffDivision, String> jurAddrColumn = new Column<HStaffDivision, String>(
//				new EditTextCell()) {
//			@Override
//			public String getValue(HStaffDivision object) {
//				return object.getDou().getJAddress() + "";
//			}
//		};
//		douListCellTable.addColumn(jurAddrColumn,
//				SafeHtmlUtils.fromSafeConstant("Юр Адрес"));
//		
//		jurAddrColumn.setFieldUpdater(new FieldUpdater<HStaffDivision, String>() {
//			@Override
//			public void update(int index, HStaffDivision object, String value) {
//				object.getDou().setJAddress(value); 
//				getPresenter().update(object);
//			}
//		});


		/* Email */
//		Column<HStaffDivision, String> emailColumn = new Column<HStaffDivision, String>(
//				new EditTextCell()) {
//			@Override
//			public String getValue(HStaffDivision object) {
//				return object.getDou().getEMail() + "";
//			}
//		};
//		douListCellTable.addColumn(emailColumn,
//				SafeHtmlUtils.fromSafeConstant("Email"));
//		
//		emailColumn.setFieldUpdater(new FieldUpdater<HStaffDivision, String>() {
//			@Override
//			public void update(int index, HStaffDivision object, String value) {
//				object.getDou().setEMail(value); 
//				getPresenter().update(object);
//			}
//		});
		
		
		/*   #142 Форма контрагентов - перенести кнопки редактирования и удаления в таблицу */ 	
		/* actions */
		List<HasCell<HStaffDivision, ?>>  hasCells = new ArrayList<HasCell<HStaffDivision, ?>>();
		
		
		/* edit */ 
		ActionCell<HStaffDivision> editCell = new ActionCellWithTooltip<HStaffDivision>(
				SafeHtmlUtils.fromSafeConstant("<img src='images/edit.png' alt='Редактировать' />"),new Delegate<HStaffDivision>() {
		
			@Override
			public void execute(HStaffDivision p) {
				HashMap<String, Serializable> params = new HashMap<String, Serializable>();
				params.put("dou", p);
				params.put("state", State.EDIT_DOU);
				EQueueNavigation.instance().goToContractorEditPage(params);
			}
		},"Редактировать запись");
		
		
		Column <HStaffDivision,HStaffDivision>editColumn =new Column<HStaffDivision,HStaffDivision>(
				editCell){
				@Override
				public HStaffDivision getValue(HStaffDivision object) {
					return object;
				}}; 
		hasCells.add(editColumn);		
		
		/* delete */ 
		Delegate<HStaffDivision> delDelegate = new Delegate<HStaffDivision>() {

			@Override
			public void execute(HStaffDivision p) {
				if(Window.confirm("Удалить безвозвратно?")){
					getPresenter().deleteDou(p.getUId(),false);
				}
			}
		};
		ActionCell<HStaffDivision> delCell = new ActionCellWithTooltip<HStaffDivision>(
				SafeHtmlUtils.fromSafeConstant("<img src='images/delete.png' alt='Удалить' title='Удалить запись без возможности восстановления' />"),delDelegate,
				"Удалить запись без возможности восстановления.");
	
		
		Column <HStaffDivision,HStaffDivision>delColumn =new Column<HStaffDivision,HStaffDivision>(
			delCell){
				@Override
				public HStaffDivision getValue(HStaffDivision object) {
					return object;
				}
		}; 

		hasCells.add(delColumn);		
		CompositeCell<HStaffDivision> cell=new CompositeCell<HStaffDivision>(
			hasCells );
		Column <HStaffDivision,HStaffDivision>actionColumn =new Column<HStaffDivision,HStaffDivision>(
			cell){
		
				@Override
				public HStaffDivision getValue(HStaffDivision object) {
					return object;
				}}; 
				
		douListCellTable.addColumn(actionColumn, SafeHtmlUtils.fromSafeConstant("Действие"));
		douListCellTable.setColumnWidth(actionColumn, "90px");
		/**/
		

	}
	
	private void initContrTableColumns() {

		/* name */
		Column<Juridical, String> nameColumn = new Column<Juridical, String>(
				new EditTextCell()) {
			@Override
			public String getValue(Juridical object) {
				return object.getName();
			}
		
		
		};
		contrListCellTable.addColumn(nameColumn,
				SafeHtmlUtils.fromSafeConstant("Имя контрагента"));
		
		nameColumn.setFieldUpdater(new FieldUpdater<Juridical, String>() {
			@Override
			public void update(int index, Juridical object, String value) {
				object.setName(value); 
				getPresenter().updateJur(object);
			}
		});
		
//		/* inn */
//		Column<Juridical, String> innColumn = new Column<Juridical, String>(
//				new EditTextCell()) {
//			@Override
//			public String getValue(Juridical object) {
//				return object.getINN();
//			}
//		};
//		contrListCellTable.addColumn(innColumn,
//				SafeHtmlUtils.fromSafeConstant("ИНН"));
//		
//		innColumn.setFieldUpdater(new FieldUpdater<Juridical, String>() {
//			@Override
//			public void update(int index, Juridical object, String value) {
//				object.setINN(value); 
//				getPresenter().updateJur(object);
//			}
//		});

		/* Физ Адрес */
		Column<Juridical, String> fizAddrColumn = new Column<Juridical, String>(
				new EditTextCell()) {
			@Override
			public String getValue(Juridical object) {
				return object.getPAddress();
			}
		};
		contrListCellTable.addColumn(fizAddrColumn,
				SafeHtmlUtils.fromSafeConstant("Физ Адрес"));
		
		fizAddrColumn.setFieldUpdater(new FieldUpdater<Juridical, String>() {
			@Override
			public void update(int index, Juridical object, String value) {
				object.setPAddress(value); 
				getPresenter().updateJur(object);
			}
		});

		/* telephone */
		Column<Juridical, String> telColumn = new Column<Juridical, String>(
				new EditTextCell()) {
			@Override
			public String getValue(Juridical object) {
				return object.getPhone();
			}
		};
		contrListCellTable.addColumn(telColumn,
		 SafeHtmlUtils.fromSafeConstant("Контактный телефон"));
		
		telColumn.setFieldUpdater(new FieldUpdater<Juridical, String>() {
			@Override
			public void update(int index, Juridical object, String value) {
				object.setPhone(value); 
				getPresenter().updateJur(object);
			}
		});
		
		/* Юр Адрес */
//		Column<Juridical, String> jurAddrColumn = new Column<Juridical, String>(
//				new EditTextCell()) {
//			@Override
//			public String getValue(Juridical object) {
//				return object.getJAddress() + "";
//			}
//		};
//		contrListCellTable.addColumn(jurAddrColumn,
//				SafeHtmlUtils.fromSafeConstant("Юр Адрес"));
//		
//		jurAddrColumn.setFieldUpdater(new FieldUpdater<Juridical, String>() {
//			@Override
//			public void update(int index, Juridical object, String value) {
//				object.setJAddress(value); 
//				getPresenter().updateJur(object);
//			}
//		});


		/* Email */
//		Column<Juridical, String> emailColumn = new Column<Juridical, String>(
//				new EditTextCell()) {
//			@Override
//			public String getValue(Juridical object) {
//				return object.getEMail() + "";
//			}
//		};
//		contrListCellTable.addColumn(emailColumn,
//				SafeHtmlUtils.fromSafeConstant("Email"));
//		
//		emailColumn.setFieldUpdater(new FieldUpdater<Juridical, String>() {
//			@Override
//			public void update(int index, Juridical object, String value) {
//				object.setEMail(value); 
//				getPresenter().updateJur(object);
//			}
//		});

		/*   #142 Форма контрагентов - перенести кнопки редактирования и удаления в таблицу */ 	
		/* actions */
		List<HasCell<Juridical, ?>>  hasCells = new ArrayList<HasCell<Juridical, ?>>();
		
		
		/* edit */ 
		ActionCell<Juridical> editCell = new ActionCellWithTooltip<Juridical>(
				SafeHtmlUtils.fromSafeConstant("<img src='images/edit.png' alt='Редактировать' />"),new Delegate<Juridical>() {
		
			@Override
			public void execute(Juridical juridical) {
				HashMap<String, Serializable> params = new HashMap<String, Serializable>();
				params.put("juridical", juridical);
				params.put("state", State.EDIT_CONTRAGENT);
				EQueueNavigation.instance().goToContractorEditPage(params);
			}
		},"Редактировать запись");
		
		
		Column <Juridical,Juridical>editColumn =new Column<Juridical,Juridical>(
				editCell){
				@Override
				public Juridical getValue(Juridical object) {
					return object;
				}}; 
		hasCells.add(editColumn);		
		
		/* delete */ 
		Delegate<Juridical> delDelegate = new Delegate<Juridical>() {

			@Override
			public void execute(Juridical p) {
				if(Window.confirm("Удалить безвозвратно?")){
					getPresenter().deleteJur(p.getUId());
				}
			}
		};
		ActionCell<Juridical> delCell = new ActionCellWithTooltip<Juridical>(
				SafeHtmlUtils.fromSafeConstant("<img src='images/delete.png' alt='Удалить' title='Удалить запись без возможности восстановления' />"),delDelegate,
				"Удалить запись без возможности восстановления.");
	
		
		Column <Juridical,Juridical>delColumn =new Column<Juridical,Juridical>(
			delCell){
				@Override
				public Juridical getValue(Juridical object) {
					return object;
				}
		}; 

		hasCells.add(delColumn);		
		CompositeCell<Juridical> cell=new CompositeCell<Juridical>(
			hasCells );
		Column <Juridical,Juridical>actionColumn =new Column<Juridical,Juridical>(
			cell){
		
				@Override
				public Juridical getValue(Juridical object) {
					return object;
				}}; 
				
		contrListCellTable.addColumn(actionColumn, SafeHtmlUtils.fromSafeConstant("Действие"));
		contrListCellTable.setColumnWidth(actionColumn, "90px");
		/**/
		
	}
	
	// @Override
	public void showDous(List<HStaffDivision> list) {
			douListCellTable.setRowData(list);
	}
	
	public void showContragents(List<Juridical> list) {
		contrListCellTable.setRowData(list);
	}

	

	ContragentListPresenter presenter;

	public ContragentListPresenter getPresenter() {
		return presenter;
	}

	public void setPresenter(ContragentListPresenter presenter) {
		this.presenter = presenter;
	}

	@UiHandler("newButton")
	public void save(ClickEvent event) {
		EQueueNavigation.instance().goToContractorNewPage(null);
	}
	
//	@UiHandler("editButton")
//	public void edit(ClickEvent event) {
//		HashMap<String, Serializable> params = new HashMap<String, Serializable>();
//		HStaffDivision dou = getSelectedDou();
//		if (dou == null) {
//			Juridical juridical = getSelectedContragent();
//			if (juridical == null) {
//				showWarningNotification("Выберите пожалуйста контрагента.");
//				return;
//			} else {
//				params.put("juridical", juridical);
//				params.put("state", State.EDIT_CONTRAGENT);
//			}
//		} else {
//			params.put("dou", dou);
//			params.put("state", State.EDIT_DOU);
//		}
//		EQueueNavigation.instance().goToContractorEditPage(params);
//	}
//	
	
//	@UiHandler("delButton")
//	public void del(ClickEvent event) {
//		HStaffDivision dou = getSelectedDou();
//		Juridical juridical = null;
//		if (dou==null) {
//			juridical = getSelectedContragent();
//		}
//		
//		if (dou==null && juridical==null){
//			showWarningNotification("Выберите пожалуйста контрагента.");
//			return;
//		}else {
//			if(Window.confirm("Удалить безвозвратно?")){
//				if(dou != null){
//					getPresenter().deleteDou(dou.getUId(),false);
//				} else {
//					getPresenter().deleteJur(juridical.getUId());
//				}
//			}
//		} 
//	}

	
	private HStaffDivision getSelectedDou() {
		for (HStaffDivision juridical : douListCellTable.getVisibleItems()) {
			if(douListCellTable.getSelectionModel().isSelected(juridical)){ 
				return juridical;
			}
		}
		return null;
	}

	private Juridical getSelectedContragent() {
		for (Juridical juridical : contrListCellTable.getVisibleItems()) {
			if(contrListCellTable.getSelectionModel().isSelected(juridical)){ 
				return juridical;
			}
		}
		return null;
	}
	
	
	/**
	 * при удалении ДОУ выводить предупреждение "В этот ДОУ записались 14 человек".
	 * и варианты действий:
	 * 1. Просмотреть рег. номера с этим ДОУ
	 * 2. Удалить ДОУ из заявок
	 * 3. Отмена
	 * @return
	 */
	public DialogBox createDialogBox(int requestsAmount, ClickHandler showRequests,
			ClickHandler  removeDouFromRequests, ClickHandler cancel) {
	    // Create a dialog box and set the caption text
	    final DialogBox dialogBox = new DialogBox();
	    dialogBox.ensureDebugId("cwDialogBox");
	    dialogBox.setText("Внимание!!!");

	    // Create a table to layout the content
	    VerticalPanel dialogContents = new VerticalPanel();
	    dialogContents.setSpacing(4);
	    dialogBox.setWidget(dialogContents);

	    // Add some text to the top of the dialog
	    HTML details = new HTML("В этот ДОУ записались "+requestsAmount+" человек" +
	    				"<br/><br/> ");
	    dialogContents.add(details);
	    dialogContents.setCellHorizontalAlignment(
	        details, HasHorizontalAlignment.ALIGN_CENTER);

	  

	    HorizontalPanel buttonsPanel = new HorizontalPanel();
	    
	    Button ignoreButton = new Button(     "Просмотреть рег. номера с этим ДОУ", showRequests );
	    ClickHandler closeHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		};
		ignoreButton.addClickHandler(closeHandler);
		buttonsPanel.add(ignoreButton);
	    
	    
	    
	    Button backToeditButton = new Button("Удалить ДОУ из заявок", removeDouFromRequests );
	    backToeditButton.addClickHandler(closeHandler);
		buttonsPanel.add(backToeditButton);
	    
	    Button showMatchesButton = new Button(     "Отмена", cancel );
	    showMatchesButton.addClickHandler(closeHandler);
		buttonsPanel.add(showMatchesButton);
	   
		dialogContents.add(buttonsPanel);
	    return dialogBox;
	  }

	
}
