package org.itx.jbalance.equeue.gwt.client.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.DateChooserComponent;
import org.itx.jbalance.equeue.gwt.client.EQueueNavigation;
import org.itx.jbalance.l0.h.RecordColor;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.Region;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.DockPanel;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteHandler;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ValueListBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;

import eu.maydu.gwt.validation.client.DefaultValidationProcessor;
import eu.maydu.gwt.validation.client.ValidationProcessor;
import eu.maydu.gwt.validation.client.ValidationResult;
import eu.maydu.gwt.validation.client.ValidationResult.ValidationError;
import eu.maydu.gwt.validation.client.Validator;
import eu.maydu.gwt.validation.client.actions.FocusAction;
import eu.maydu.gwt.validation.client.actions.StyleAction;
import eu.maydu.gwt.validation.client.description.PopupDescription;
import eu.maydu.gwt.validation.client.i18n.ValidationMessages;
import eu.maydu.gwt.validation.client.validators.standard.NotEmptyValidator;
import eu.maydu.gwt.validation.client.validators.standard.RegularExpressionValidator;

public class DouRequestEditWidget extends ContentWidget implements DouRequestEditPresenter.Display {

	interface Binder extends UiBinder<Widget, DouRequestEditWidget> {

	}

	HTML registrationDateLabel=new HTML();
	
	DateChooserComponent registrationDate=new DateChooserComponent();
	
	@UiField(provided = false)
	HTML title;
	
	@UiField(provided = false)
	HorizontalPanel registrationDatePanel;
	
	
	CheckBox registrationDateEditableCheckbox;
	
	@UiField(provided = false)
	TextBox oldNumber;

	@UiField(provided = false)
	TextBox lastNameTextBox;
	
	@UiField(provided = false)
	TextBox firstNameTextBox;
	
	@UiField(provided = false)
	RadioButton sexMRadio;
	
	@UiField(provided = false)
	RadioButton sexWRadio; 
	
	@UiField(provided = false)
	TextBox patronymicTextBox;
	
	@UiField(provided = false)
	DateChooserComponent birthDateBox;
	
	@UiField(provided = false)
	TextBox bsSeriaBox;
	
	@UiField(provided = false)
	TextBox addressBox;
	
	@UiField(provided = false)
	TextBox bsNumberBox;
	
	@UiField(provided = true)
	ValueListBox<Integer> yearListBox;
	
	@UiField(provided = false)
	DateBox sverkDate;
	
	@UiField(provided = false)
	DateChooserComponent bsDateBox;
	
	@UiField(provided = false)
	TextArea commentsTextArea;

	@UiField(provided = false)
	TextBox phoneNumberTextBox;
	
	@UiField(provided = false)
	TextBox emailTextBox;
	
	@UiField(provided=true)
	ValueListBox<RecordColor> recordColorListBox;
	
	@UiField
	ListBox privilegeListBox;
	
	@UiField
	SimplePanel dous;
	
	
	ValidationProcessor validator = new DefaultValidationProcessor();
	
	RaitingDouPanel chooseDouPanel;
	
	@UiField
	Button importButton;
	
	@UiField(provided = false)
	TextBox declarantLastnameTextBox;
	
	@UiField(provided = false)
	TextBox declarantFirstnameTextBox;
	
	@UiField(provided = false)
	TextBox declarantPatronymicTextBox;
	
	@UiField(provided = false)
	TextBox declarantPassportSeriaTextBox;
	
	@UiField(provided = false)
	TextBox declarantPassportNumberTextBox;

	@UiField(provided = false)
	DateChooserComponent declarantPassportIssueDateBox;
	
	@UiField(provided = false)
	TextBox declarantPassportAuthorityTextBox;
	
	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public DouRequestEditWidget() {
		super();
	}

	
	
	public void initCreateMode(){
		importButton.setEnabled(true);
		registrationDatePanel.add(registrationDate);
		registrationDateEditableCheckbox =new CheckBox();
		registrationDateEditableCheckbox.setTitle("Открыть для редактирования");
		registrationDatePanel.add(registrationDateEditableCheckbox);
		
		registrationDateEditableCheckbox.setValue(false);
		registrationDate.setEnable(false);
		registrationDateEditableCheckbox.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				registrationDate.setEnable(event.getValue());
			}
		});
		
		getFormTitle().setText("Добавление нового регистрационного номера");
	}

	public void initEditMode(){
		registrationDatePanel.add(registrationDateLabel);	
		importButton.setEnabled(false);
	}
	
	
	@Override
	public void initialize() {
		recordColorListBox=new ValueListBox<RecordColor>
			(new RecordColorRenderer());
		recordColorListBox.addValueChangeHandler(new ValueChangeHandler<RecordColor>() {

			@Override
			public void onValueChange(ValueChangeEvent<RecordColor> event) {
				for(RecordColor rc:RecordColor.values())
					recordColorListBox.removeStyleName(rc.name());
				RecordColor value = event.getValue();
				if(value!=null)
					recordColorListBox.setStyleName(value.name());
				
				
			}
		});
		recordColorListBox.setAcceptableValues(Arrays.asList(RecordColor.values()));
		
		
		
		yearListBox=new ValueListBox<Integer>(new AbstractRenderer<Integer>() {
    	@Override
		public String render(Integer object) {
    		if(object==null)
    			return "--";
			return object+"";
		}
    	
    	
	});
		
		Date date = new Date();
		Collection<Integer> years=new ArrayList<Integer>();
		int currentYear = date.getYear();
		for(int y=currentYear+1900;y<currentYear+1900+8;y++){
			years.add(y);
		}
		yearListBox.setAcceptableValues(years);
		
		
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		
	
//		availableDouList=new CellList<Dou>(new DouCell());
//		availableDouList.addStyleName("dou-cell-list");
//		availableDouList.setPageSize(11);
//		availableDouList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
//		availableDouList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);
//		
//		availableDouList.setSelectionModel(new SingleSelectionModel<Dou>());
//		
//		availableDouPagerPanel.setDisplay(availableDouList);
//		availableDouRangeLabelPager.setDisplay(availableDouList);
//		
//		
//		
//		choosedDouList=new CellList<Dou>(new DouCell());
//		choosedDouList.addStyleName("dou-cell-list");
//		choosedDouList.setPageSize(11);
//		choosedDouList.setKeyboardPagingPolicy(KeyboardPagingPolicy.INCREASE_RANGE);
//		choosedDouList.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.BOUND_TO_SELECTION);
//		
//		choosedDouList.setSelectionModel(new SingleSelectionModel<Dou>());
//		
//		choosedDouPagerPanel.setDisplay(choosedDouList);
//		choosedDouRangeLabelPager.setDisplay(choosedDouList);
//		
//		
//		 availableDouDataProvider=new ListDataProvider<Dou>();
//		 availableDouDataProvider.addDataDisplay(availableDouList);
//		 
//		 choosedDouDataProvider=new ListDataProvider<Dou>();
//		 choosedDouDataProvider.addDataDisplay(choosedDouList);
//		
		 
		 
		 DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd.MM.yy");
		 DateBox.Format format=new DateBox.DefaultFormat(dateTimeFormat); 
		 sverkDate.setFormat(format); 

		 
		 initValidation();
	}

	
	private PopupDescription popupDesc  ;
 
	/**
	 * #138 Сделать проверку на поля ФИО
	 * Выводить сообщение о некоретных данных и подсвечивать красным поля ФИО, 
	 * если поля пустые, если есть числа в полях, если написано латиницей.
	 * @return
	 */
	private  Validator<?>[]  getPartOfNameValidator (TextBox suggestBox,final String fieldName){
		FocusAction focusAction = new FocusAction();
		
		Validator<?>[] validators = new Validator[2]; 
		NotEmptyValidator notEmptyValidator = new NotEmptyValidator(suggestBox){
			
			@Override
			public void invokeActions(ValidationResult result) {
				super.invokeActions(result);
				if(result!=null){
					validationMessages.add( "Заполните поле '"+fieldName+"'");
				}
			}
		}
			.addActionForFailure(focusAction)
			.addActionForFailure(new StyleAction("validationFailedBorder"));
		
		
		RegularExpressionValidator regExpValidator = new RegularExpressionValidator(suggestBox,"^[а-яА-ЯёЁ]+([ -][а-яА-ЯёЁ]+)*$", null){
			@Override
			public void invokeActions(ValidationResult result) {
				super.invokeActions(result);
				if(result!=null){
					validationMessages.add( "Поле '"+fieldName + "' должно содержать только кириллицу");
				}
			}
		}
			.addActionForFailure(focusAction)
			.addActionForFailure(new StyleAction("validationFailedBorder"));
		
		
		validators[0] = notEmptyValidator;
		validators[1] = regExpValidator;
		
		return validators;
	}
	
	private void initValidation() {
		FocusAction focusAction = new FocusAction();
		validator = new DefaultValidationProcessor();
		ValidationMessages messages = new ValidationMessages();
		
		popupDesc = new PopupDescription(messages);

		validator.addValidators("dousEmpty", 
				new   Validator<Validator>(){

					@Override
					public ValidationResult validate(ValidationMessages messages) {
						if(getSelectedDou().isEmpty()){
							return new ValidationResult("Добавьте ДОУ");
						}
						return null;
					}

					@Override
					public void invokeActions(ValidationResult result) {
//						new StyleAction("validationFailedBorder").invoke(result, choosedDouList);
						if(result!=null){
							ValidationError validationError = result.getErrors().get(0);
							validationMessages.add( ""+validationError.error);
						}
					}}
					.addActionForFailure(focusAction)
					
		);
		
//		popupDesc.addDescription("test", firstNameTextBox);

		validator.addValidators("firstName",getPartOfNameValidator(firstNameTextBox, "Имя"));
		validator.addValidators("lastNamenotEmpty",getPartOfNameValidator(lastNameTextBox, "Фамилия"));
		validator.addValidators("patronymicnotEmpty",getPartOfNameValidator(patronymicTextBox, "Отчество"));
		
		
//		#304: Валидация полей свидетельства о рождении в EQueue и MFC 
		validator.addValidators("bsSeria",
				new   Validator<Validator>(){

			@Override
			public ValidationResult validate(ValidationMessages messages) {
				
				validationFailedStyleAction.reset(bsSeriaBox);
				
				String bsSeria = getBsSeriaBox().getText();
				if(bsSeria == null || bsSeria.isEmpty()){
					return new ValidationResult("Заполните поле \"Серия свидетельства о рождении\"");
				}
				if(!bsSeria.contains("-")  || bsSeria.indexOf("-") != bsSeria.lastIndexOf("-")) {
					return new ValidationResult("Серия свидетельства о рождении должна состоять из двух частей, разделенных тире (например 'XIV-АГ')");
				}

				RegExp bs_seria_regEx1= RegExp.compile("^[IVXLM]{1,5}-");
				RegExp bs_seria_regEx2= RegExp.compile("-[А-Я]{1,5}$");
				
				if(! bs_seria_regEx1.test(bsSeria)){
					return new ValidationResult("Серия свидетельства о рождении: до черты должны быть римские цифры (латинские заглавные буквы XIV).");
				} else if(! bs_seria_regEx2.test(bsSeria)){
					return new ValidationResult("Серия свидетельства о рождении: после черты должна быть кирилица в верхнем регистре (заглавные русские буквы).");
				}
				return null;
			}

			StyleAction validationFailedStyleAction = new StyleAction("validationFailedBorder");
			
			@Override
			public void invokeActions(ValidationResult result) {
				if(result!=null){
					validationFailedStyleAction.invoke(result, bsSeriaBox);
					ValidationError validationError = result.getErrors().get(0);
					validationMessages.add( ""+validationError.error);
				} else {
				}
			}}
			.addActionForFailure(focusAction)
		);
		
		
		
		
		validator.addValidators("bsNumber",
				new   Validator<Validator>(){

			@Override
			public ValidationResult validate(ValidationMessages messages) {
				validationFailedStyleAction.reset(bsNumberBox);
				String bs_number = getBsNumberBox().getText();
				if(bs_number == null || bs_number.isEmpty()){
					return new ValidationResult("Заполните поле \"Номер свидетельства о рождении\"");
				}
			
				RegExp bs_number_regEx= RegExp.compile("^[0-9]{3,8}$");
				
				if(!bs_number_regEx.test(bs_number)){
					return new ValidationResult("Номер свидетельства о рождении: Разрешены только цифры. Максимум 8.");
		        }
				return null;
			}
			
			StyleAction validationFailedStyleAction = new StyleAction("validationFailedBorder");

			@Override
			public void invokeActions(ValidationResult result) {
				if(result!=null){
					validationFailedStyleAction.invoke(result, bsNumberBox);
					ValidationError validationError = result.getErrors().get(0);
					validationMessages.add( ""+validationError.error);
				} else {
				}
			}}
			.addActionForFailure(focusAction)
		);
		
		
//		var bs_seria = $('#bs_seria').val();
//		 var bs_seria_regEx1=new RegExp("^[IVXLM]{1,5}-");
//		 var bs_seria_regEx2=new RegExp("-[А-Я]{1,5}$");
//		 var bs_number_regEx=new RegExp("^[0-9]{3,8}$");



	}



	static class DouCell extends AbstractCell<Dou> {

	    /**
	     * The html of the image used for contacts.
	     */
//	    private final String imageHtml;

	    public DouCell() {
//	      this.imageHtml = AbstractImagePrototype.create(image).getHTML();
	    }

	    @Override
	    public void render(Context context, Dou value, SafeHtmlBuilder sb) {
	      // Value can be null, so do a null check..
	      if (value == null) {
	        return;
	      }

	      sb.appendHtmlConstant("<table colspacing='0' rowspacing='0' class='request-dou-list'>");

	      // Add the contact image.
	      sb.appendHtmlConstant("<tr><td class='request-dou-name'>");
	      sb.appendHtmlConstant(value.getName());
	      sb.appendHtmlConstant("</td></tr>");

	      // Add the name and address.
	      sb.appendHtmlConstant("<tr><td class='request-dou-address'>");
	      sb.appendEscaped(value.getJAddress()==null?"":value.getJAddress());
	      sb.appendHtmlConstant("</td></tr>"); 
	      
	      sb.appendHtmlConstant("<tr><td class='request-dou-address'>");
	      sb.appendEscaped(value.getPAddress()==null?"":value.getPAddress());
	      sb.appendHtmlConstant("</td></tr>"); 
	      
	      sb.appendHtmlConstant("<tr><td class='request-dou-address'>");
	      sb.appendEscaped(value.getEMail()==null?"":value.getEMail());
	      sb.appendHtmlConstant("</td></tr>");
	      
	      sb.appendHtmlConstant("</table>");
	    }
	  }
	
	
	
	DouRequestEditPresenter presenter;

	public DouRequestEditPresenter getPresenter() {
		return presenter;
	}


	public void setPresenter(DouRequestEditPresenter presenter) {
		this.presenter=presenter;
	}

	@Override
	public HasText getLastName() {
		return lastNameTextBox;
	}

	@Override
	public HasText getFirstName() {
		return firstNameTextBox;
	}

	@Override
	public HasText getPatronymic() {
		return patronymicTextBox;
	}

	@Override
	public HasValue<Date> getBirthDate() {
		return birthDateBox;
	}

	@Override
	public HasText getBsSeriaBox() {
		return bsSeriaBox;
	}

	@Override
	public HasText getBsNumberBox() {
		return bsNumberBox;
	}

	@Override
	public HasValue<Date> getBsDateBox() {
		return bsDateBox;
	}

//	#942
//	@Override
//	public HasText getGuardian1() {
//		return guardian1TextBox;
//	}
//
//	@Override
//	public HasText getGuardian2() {
//		return guardian2TextBox;
//	}


	
	public void setPrivileges(List<Privilege> privileges) {
		privilegeListBox.addItem(" без льгот ");
		for (Privilege p : privileges) {
			privilegeListBox.addItem(p.getName());
	}
	}
	
	
	
	@Override
	public void setAvailableDou(Map<Region ,List<Dou>> dous) {
		chooseDouPanel = new RaitingDouPanel(dous, null);
		this.dous.add(chooseDouPanel);
	}

//	@Override
//	public void setChoosedDou(List<Dou> availableDou) {
//		choosedDouDataProvider.setList(availableDou);
//	}
	
//	ListDataProvider<Dou> chooProvider=new ListDataProvider<Dou>(availableDou);

//	@UiHandler("addAllDouButton")
//	public void addAllDouButton(ClickEvent event){
//		presenter.chooseAllDou();
//	}
//
//
//	@UiHandler("addOneDouButton")
//	public void addOneDouButton(ClickEvent event){
//		SingleSelectionModel<Dou> selectionModel = (SingleSelectionModel<Dou>)availableDouList.getSelectionModel();
////		selectionModel.
//		Dou selectedObject = selectionModel.getSelectedObject();
//		if(selectedObject!=null ){
//			Log.debug("selected dou - " + selectedObject );
//			presenter.chooseOneDou(selectedObject);
//		}
//	}
//	
//	@UiHandler("removeOneDouButton")
//	public void removeOneDouButton(ClickEvent event){
//		Dou selectedObject = ((SingleSelectionModel<Dou>)choosedDouList.getSelectionModel()).getSelectedObject();
//		if(selectedObject!=null){
//			Log.debug("selected dou - " + selectedObject );
//			presenter.removeOneDou(selectedObject);
//		}
//	}
//	
//	@UiHandler("removeAllDouButton")
//	public void removeAllDouButton(ClickEvent event){
//		presenter.removeAllDou();
//	}
//	

	private List<String> validationMessages= new ArrayList<String>();
	
	@UiHandler("saveButton")
	public void save(ClickEvent event){
		validationMessages.clear();
		boolean validate = validator.validate();
		if(validate)
			presenter.save();
		else{
			StringBuffer sb = new StringBuffer();
			for (String m : validationMessages) {
				sb.append(m);
				sb.append("<br/>");
			}
			showWarningNotification(sb.toString());
		}
	}
	
	@UiHandler("cancelButton")
	public void cancel(ClickEvent event){
		EQueueNavigation.instance().goToDouRequestsListPage(null);
	}

	
	@UiHandler("importButton")
	public void importXml(ClickEvent event){
		final FormPanel form = new FormPanel();
		form.setAction("equeue/xmlUpload");
		// Because we're going to add a FileUpload widget, we'll need to set the
		// form to use the POST method, and multipart MIME encoding.
		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_POST);
		
		
		final DialogBox dialogBox = new DialogBox();
	    dialogBox.ensureDebugId("cwDialogBox");
		dialogBox.setText("Импорт из XML");

	    // Create a table to layout the content
	    DockPanel dialogContents = new DockPanel();
	    dialogContents.setSpacing(4);
	    dialogContents.setWidth("600px");
	    dialogContents.setHeight("200px");
//	    dialogBox.setWidget();

	    dialogBox.setWidget(form);
	    form.add(dialogContents);
	    HTML details = new HTML("Выберите файл для загрузки:");
	    details.setWidth("490px");
	    
	 // Create a FileUpload widget.
		FileUpload fileUpload = new FileUpload();
		fileUpload.setName("fileUpload");
		
//		String uuid = org.itx.jbalance.equeue.gwt.client.UUID.uuid();
//		Hidden hidden = new Hidden("key", uuid);
		
		FlowPanel flowPanel1 = new FlowPanel();
		dialogContents.add(flowPanel1, DockPanel.CENTER);
		flowPanel1.add(details);
		flowPanel1.add(fileUpload);
//		flowPanel1.add(hidden);
	    
	    Button okButton = new Button("Ok");
	    okButton.setWidth("100px");
	    ClickHandler okHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Log.debug("Befor submit");
				form.submit();
				Log.debug("After submit");
			}
		};
		okButton.addClickHandler(okHandler);
		
		Button cancelButton = new Button("Отмена");
	    cancelButton.setWidth("100px");
	    ClickHandler cancelHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dialogBox.hide();
			}
		};

		cancelButton.addClickHandler(cancelHandler);
		
		FlowPanel flowPanel = new FlowPanel();
		flowPanel.add(okButton);
		flowPanel.add(cancelButton);
		dialogContents.add(flowPanel, DockPanel.SOUTH);
		
		form.addSubmitCompleteHandler(new SubmitCompleteHandler() {
			@Override
			public void onSubmitComplete(SubmitCompleteEvent event) {
				String results = event.getResults();
				dialogBox.hide();
				getPresenter().populateFormUsingXml(results);
			}
		});
		
//		Do some actions to show our modal panel
	    dialogBox.setModal(true);
	    dialogBox.setGlassEnabled(true);
	    dialogBox.setAnimationEnabled(true);
	    dialogBox.center();
	    dialogBox.show();
	}
	
	@Override
	public Privilege getPrivilege() {
		if(privilegeListBox.getSelectedIndex()==0)
			return null;
		else{
			
			return presenter.getAllPrivileges() .get(privilegeListBox.getSelectedIndex()-1);
		}
	}

	
	public void setPrivilege(Privilege privilege) {
		Log.debug("setPrivilege("+privilege+")");
		if(privilege==null)
			privilegeListBox.setSelectedIndex(0);
		else{
			
			List<Privilege> allPrivileges = presenter.getAllPrivileges();
			Log.debug("      allPrivileges = "+allPrivileges);
			privilegeListBox.setSelectedIndex(allPrivileges.indexOf(privilege)+1);
		}
		
	}

	public HasText getComments() {
		return commentsTextArea;
	}


	public HasValue< Date> getRegistrationDate() {
		return registrationDate;
	}


	public HasText getOldNumber() {
		return oldNumber;
	}

	public HasHTML getRegistrationDateLabel(){
		return registrationDateLabel;
	}
	
	public HasText getPhoneNumber(){
		return phoneNumberTextBox;
	}
	
	public HasText getEmail(){
		return emailTextBox;
	}
	
	public HasValue<Integer> getYear(){
		return yearListBox;
	}
	
	public HasText getAddress(){
		return addressBox;
	}
	
	public HasValue<Date> getSverkDate(){
		return sverkDate;
	}
	
	public HasHTML getFormTitle(){
		return title;
	}
	
	public HasValue<RecordColor>getRecordColor(){
		return recordColorListBox;
	}
	
	public Sex getSex(){
		return sexMRadio.getValue()?Sex.M:sexWRadio.getValue()?Sex.W:null;
	}
	
	public void setSex(Sex sex){
		if(sex==null){
			 sexMRadio.setValue(true);  sexWRadio.setValue(false);
		}
		else
		switch(sex){
		case M: sexMRadio.setValue(true); break;
		case W: sexWRadio.setValue(true); break;
		default: sexMRadio.setValue(true);  sexWRadio.setValue(false);
		}
		
	}

	public HasText getDeclarantFirstname(){
		return declarantFirstnameTextBox;
	}
	
	public HasText getDeclarantLastname(){
		return declarantLastnameTextBox;
	}
	
	public HasText getDeclarantPassportAuthority(){
		return declarantPassportAuthorityTextBox;
	}
	
	public HasText getDeclarantPassportSeria(){
		return declarantPassportSeriaTextBox;
	}
	
	public HasText getDeclarantPassportNumber(){
		return declarantPassportNumberTextBox;
	}
	
	public HasValue<Date> getDeclarantPassportIssueDate(){
		return declarantPassportIssueDateBox;
	}
	
	public HasText getDeclarantPatronymic(){
		return declarantPatronymicTextBox;
	}
	
	public DialogBox createDialogBox(int matchFio,int matchFioAndAge , ClickHandler ignore,
			ClickHandler  backToEdit, ClickHandler showMatches) {
	    
	    
	    Button ignoreButton = new Button("Все равно добавить", ignore );
//	     #88 Отменить добавление двойника в очередь
	    ignoreButton.setEnabled(matchFioAndAge==0);
	    
	    Button backToeditButton = new Button(     "Вернуться к редактированию", backToEdit );
	    
	    Button showMatchesButton = new Button(     "Показать совпадения", showMatches );
		
		
	    HTML details = new HTML("Найдены совпадения: <br/>" +
	    		"Фамилия + имя + отчество = " + matchFio + "<br/>" +
	    		"Фамилия + имя + отчество + дата рождения = "+matchFioAndAge+"" +
	    				"<br/><br/> ");
	    
		return askQuestion("Внимание!!!", details, ignoreButton, backToeditButton, showMatchesButton);
	  }



	public Map<Dou, Integer> getSelectedDou() {
		return chooseDouPanel.getSelectedDou();
	}



	public void chooseOneDou(Dou dou, Integer raiting) {
		chooseDouPanel.selectOne(dou, raiting);
	}

	public void removeAllDou() {
		chooseDouPanel.clearSelection();
	}

}
