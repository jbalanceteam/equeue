package org.itx.jbalance.equeue.gwt.client.request;

import org.cobogw.gwt.user.client.ui.Rating;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.dom.client.Style.Visibility;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;

/**
 * Компонент который показывает рейтинг + надпись + ссылку очистки
 * @author apv
 *
 */
public class MyRating extends HorizontalPanel implements HasValue<Integer>{
	private Rating rating = new Rating(0, 3);
	private Label label;
	
	
	public MyRating(String douId, String douName){
		rating.setTitle(douName);
		
//		rating.getElement().getStyle().setFontSize(11, Unit.PX);
		label = new Label(douId);
		label.setWidth("20px");
		
		final Anchor clear = new Anchor("X");
		clear.getElement().getStyle().setMarginRight(25, Unit.PX);
		clear.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				rating.setValue(0);
			}
		});
		
		add(label);
		add(rating);
		add(clear);
		
		
		clear.getElement().getStyle().setVisibility(Visibility.HIDDEN);
		rating.addValueChangeHandler(new ValueChangeHandler<Integer>() {
			
			@Override
			public void onValueChange(ValueChangeEvent<Integer> event) {
				if(event.getValue() > 0){
					clear.getElement().getStyle().setVisibility(Visibility.VISIBLE);
				} else {
					clear.getElement().getStyle().setVisibility(Visibility.HIDDEN);
				}
			}
		});
	}
	
	@Override
	public HandlerRegistration addValueChangeHandler(
			ValueChangeHandler<Integer> handler) {
		return rating.addValueChangeHandler(handler);
	}

	@Override
	public Integer getValue() {
		return rating.getValue();
	}

	@Override
	public void setValue(Integer value, boolean fireEvents) {
		rating.setValue(value);
	}

	@Override
	public void setValue(Integer value) {
		rating.setValue(value);
	}

	public void setEnabled(boolean enabled) {
		rating.setReadOnly(!enabled);
	}
	 
}
