package org.itx.jbalance.equeue.gwt.client.services;

import java.util.List;

import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;

public class DouRequestsSearchResultsWrapper extends SearchResultWrapper<DouRequestDTO>{

	private static final long serialVersionUID = 8938653614067881449L;
	
	public DouRequestsSearchResultsWrapper() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DouRequestsSearchResultsWrapper(List<DouRequestDTO> list,
			Integer allCount, Integer searchCount, Integer active) {
		super(list, allCount, searchCount);
		this.activeCount = active;
	}

	Integer activeCount;
	
	public Integer getActiveCount() {

		return activeCount;
	}

	public void setActiveCount(Integer activeCount) {
		this.activeCount = activeCount;
	}
	
	
	

}
