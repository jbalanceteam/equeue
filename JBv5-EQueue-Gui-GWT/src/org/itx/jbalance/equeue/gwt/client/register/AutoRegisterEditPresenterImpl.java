package org.itx.jbalance.equeue.gwt.client.register;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.itx.jbalance.equeue.gwt.client.EQueueNavigation;
import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.services.RegisterService;
import org.itx.jbalance.equeue.gwt.client.services.RegisterServiceAsync;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper.Status;
import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l0.h.HAutoRegister.Method;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l1.o.RegionsRemote;
import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.RegionDTO;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterLineDTO;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterPrerequisiteInfo;
import org.itx.jbalance.l2_api.dto.equeue.RegisterDTO;

import com.google.gwt.core.client.GWT;
import com.google.gwt.thirdparty.guava.common.base.Predicate;
import com.google.gwt.thirdparty.guava.common.collect.Collections2;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class AutoRegisterEditPresenterImpl extends MyPresenter<AutoRegisterEditWidget>
		implements AutoRegisterEditPresenter {

	private static final long serialVersionUID = 1L;
	private final RegisterServiceAsync registerService = GWT.create(RegisterService.class);
	List<TableLine>  tableLines;
	List<DOUGroups> ageGroups;
	List<RegionDTO> regions;
	public AutoRegisterEditPresenterImpl(AutoRegisterEditWidget display) {
		super(display);
	}

	@Override
	public void initialize() {
		registerService.getAutoRegisterPrerequisiteInfo(new AsyncCallback<AutoRegisterPrerequisiteInfo>() {
			@Override
			public void onSuccess(AutoRegisterPrerequisiteInfo result) {
				ageGroups = result.getAgeGroups();
				tableLines = new ArrayList<AutoRegisterEditPresenterImpl.TableLine>(result.getDous().size());
				for (DouDTO dou : result.getDous()) {
					TableLine tableLine = new TableLine();
					tableLine.dou = dou;
					for (DOUGroups age : ageGroups) {
						tableLine.groups.put(age.getId(), 0);
					}
					tableLines.add(tableLine);
				}
				
				TableLine sumLine = new TableLine();
				sumLine.isSum = true;
				tableLines.add(sumLine);
				regions =  result.getRegions();
				recalculateSum();
				getDisplay().populateForm( regions , tableLines, result.getAgeGroups());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}

	
	
	@Override
	public void clear() {
		for (TableLine l : tableLines) {
			for (DOUGroups ageGroup: ageGroups) {
				l.groups.put(ageGroup.getId(), 0);
			}
		}
	}

	@Override
	public void save(Object object) {
		getDisplay().setEnabledUI(false);
		registerService.autoRegister(getAutoRegister(), getLines(),  new AsyncCallback<OperationResultWrapper<RegisterDTO>>() {
			
			@Override
			public void onSuccess(OperationResultWrapper<RegisterDTO> result) {
				if(result.getStatus() == Status.SUCCESS){
					getDisplay().showInfoNotification("Протокол " + result.getResult().getDocNumber()+ " успешно создан <br/>" +
							"Нажмите OK для перехода к протоколу.");
					
					HashMap<String, Serializable> p = new HashMap<String, Serializable>();
		            p.put("objectUId", result.getResult().getUId());
		            getDisplay().setEnabledUI(true);
					EQueueNavigation.instance().goToRegisterEditPage(p);
				} else {
					getDisplay().showInfoNotification("При генерации протокола произошли ошибки: " + result.getComments());
				}
				getDisplay().setEnabledUI(true);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				getDisplay().setEnabledUI(true);
				handleException(caught);
			}
		});
	}

	private List<AutoRegisterLineDTO> getLines() {
		List<AutoRegisterLineDTO> res = new ArrayList<AutoRegisterLineDTO> (tableLines.size() * 7);
		for (TableLine l : tableLines) {
			if(l.isSum){
				continue;
			}
			Set<Entry<Integer, Integer>> entrySet = l.groups.entrySet();
			for (Entry<Integer, Integer> entry : entrySet) {
				if(entry.getValue() > 0){
					res.add(new AutoRegisterLineDTO(l.dou.getUId(),entry.getKey(), entry.getValue()));
				}
			}
		}
		return res;
	}

	private HAutoRegister getAutoRegister(){
		HAutoRegister hAutoRegister = new HAutoRegister();
		if(getDisplay().getWishesMethod().getValue()){
			hAutoRegister.setMethod(Method.WISHES);
		} else {
			hAutoRegister.setMethod(Method.DOU_FILL);
		}
		try{
			hAutoRegister.setPrivelegePart(new Integer(getDisplay().getPrivelegePart().getValue()));
		} catch(NumberFormatException e){
			hAutoRegister.setPrivelegePart(HAutoRegister.DAFAULT_PRIVELEGE_PART);
		}
		
		hAutoRegister.setIncludeRefuses(getDisplay().getIncludeRefuses().getValue());
		hAutoRegister.setCalculationDate(getDisplay().getCalculationDate().getValue());
		
		return hAutoRegister;
	}
	

	public class TableLine{
		/**
		 * Для того чтобы показывать сумму по возрастной шруппе, введем дополнительный TableLine
		 * Он будет помечен этим флагом
		 */
		boolean isSum = false;
		DouDTO dou;
		Map<Integer,Integer> groups= new HashMap<Integer,Integer>();
		
		
		/**
		 * Сумма по саду
		 * @return
		 */
		public int getSum(){
			int res = 0 ; 
			Collection<Integer> values = groups.values();
			for (Integer v : values) {
				res += v;
			}
			return res;
		}
	}
	
	/**
	 * Пересчитывает сумму по возрастным группам
	 */
	public void recalculateSum(){
		TableLine sum = null;
		Map<Integer, Integer> newSumMap = new HashMap<Integer, Integer>();
		for (TableLine l : tableLines) {
			if(l.isSum){
				sum = l;
			} else {
				Map<Integer, Integer> map = l.groups;
				for (Entry<Integer, Integer> e : map.entrySet()) {
					if(newSumMap.containsKey(e.getKey())){
						newSumMap.put(e.getKey(), ((Integer)newSumMap.get(e.getKey())) + e.getValue());
					} else {
						newSumMap.put(e.getKey(), e.getValue());
					}
				}
			}
		}
		sum.groups.putAll(newSumMap);
	}


	 

	@Override
	public List<TableLine> getTableData(RegionDTO selectedRegion) {
		if(selectedRegion == null){
			return tableLines;
		} else {
			List<TableLine> res = new ArrayList<AutoRegisterEditPresenterImpl.TableLine>();
			for (TableLine l : tableLines) {
				if(l.isSum || l.dou.getRegionUid().equals(selectedRegion.getUId())){
					res.add(l);
				}
			}
			return res;
		}
	}

	@Override
	public List<RegionDTO> getRegions() {
		return regions;
	}
	
}