package org.itx.jbalance.equeue.gwt.client.contragents;

import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyDisplay;
import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.o.StaffDivision;
import org.itx.jbalance.l0.s.SStaffDivision;

public interface ContragentEditPresenter {

	public interface Display extends MyDisplay{
//		//		
//		TextBox nameTextBox;			
//		TextBox telTextBox;
//		TextBox emailTextBox;
//		TextBox innTextBox;
//		TextBox fizAddressTextBox;
//		TextBox jurAddressTextBox;
//		CheckBox jurAddressCheckBox;
//		ListBox douTypeListBox;
//		Button addNewGroupButton;
//		Button delNewGroupButton;
//		CellTable<StaffDivision> groupCellTable;
//		TextBox itogoTextBox;
//		Button saveButton;
//		//
				
//		public HasText getContragentName();
//		public HasText getContragentTel();
//		public HasText getContragentEmail();
//		public HasText getContragentInn();
//		public HasText getContragentFizAddress();
//		public HasText getContragentJurAddress();
//		public HasText getContragentItogo();
	
		public void setDOUGroupsItems(ArrayList<DOUGroups> list);		
		//public HasClickHandlers getSaveClickHandlers();
	}
	

	void updateDou();	
	void updateContragent();	
    public String getDOUGroupsNameById(int douGroupsId);
	void saveDou();
	void saveContragent();
	void setDou(HStaffDivision division);
	void setContragent(Juridical juridical);
	void setState(State state);
	public State getState();
	List<SStaffDivision> getsStaffDivisionsNew();
	void setsStaffDivisionsNew(List<SStaffDivision> sStaffDivisionsNew);
//	public void setRegionSelected(String region);
//	public void ititRegionSelected();
}
