package org.itx.jbalance.equeue.gwt.client.services;

import java.io.Serializable;

import com.google.gwt.user.client.rpc.AsyncCallback;


/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface SerializerServiceAsync {

	void serializeToBase64(Serializable obj, AsyncCallback<String> callback);



	
}
