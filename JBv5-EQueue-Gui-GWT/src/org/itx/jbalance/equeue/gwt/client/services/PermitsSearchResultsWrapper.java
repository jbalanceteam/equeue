package org.itx.jbalance.equeue.gwt.client.services;

import java.util.List;

import org.itx.jbalance.l2_api.dto.attendance.PermitDTO;

public class PermitsSearchResultsWrapper extends SearchResultWrapper<PermitDTO>{

	private static final long serialVersionUID = 8938653614067881449L;
	
	public PermitsSearchResultsWrapper() {
	}

	public PermitsSearchResultsWrapper(List<PermitDTO> list,
			Integer allCount, Integer searchCount) {
		super(list, allCount, searchCount);
	}
}
