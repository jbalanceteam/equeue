package org.itx.jbalance.equeue.gwt.client;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Application shell for Showcase sample.
 */
public class EQueueShell extends Composite {
	

	/**
	 * The panel that holds the content.
	 */
	@UiField
	SimplePanel contentPanel;

	@UiField
	FlowPanel menuPanel;
	
	private static EQueueShell instance=new EQueueShell();
	
	private EQueueShell(){
		
		long startCeation = System.currentTimeMillis();
		EQueueUiBinder uiBinder= GWT.create(EQueueUiBinder.class);
		initWidget(uiBinder.createAndBindUi(this));
		setupMenu();
		setupHistory();
		Log.debug("EQueueShell creation - "+(System.currentTimeMillis()-startCeation));
		
	}
	
	public synchronized static EQueueShell instance(){
		return instance;
	}

	interface EQueueUiBinder extends UiBinder<Widget, EQueueShell> {
	}
	
	
	private void setupMenu() {
		MenuBar mainMenu = new MenuBar();
		mainMenu.setAnimationEnabled(true);
		mainMenu.setAutoOpen(true);

		menuPanel.add(mainMenu);

		MenuBar actionMB = new MenuBar(true);
		actionMB.setAnimationEnabled(true);
		actionMB.setTitle("Действия");
		actionMB.addItem("Очередь",				EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_DOU_REQUESTS_LIST	));
		actionMB.addItem("Добавить РН",			EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_DOU_REQUEST_EDIT));
		actionMB.addItem("Импорт очереди из PreschoolChildRegister",	EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_DOU_REQUESTS_IMPORT));
		
		
		actionMB.addSeparator();
		
		actionMB.addItem("Протоколы",	EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_REGISTER_LIST));
		actionMB.addItem("Добавить протокол",	EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_REGISTER_EDIT));
		actionMB.addItem("Автопротокол",	EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_REGISTER_AUTO));
		
		
		actionMB.addSeparator();
		
		actionMB.addItem("Контрагенты",			EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_CONTRACTORS_LIST	));
		actionMB.addItem("Выданные путевки",	EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_PERMITS_LIST		));

		
		
		mainMenu.addItem("Действия", actionMB);

		MenuBar reportsMB = new MenuBar(true);
		reportsMB.setAnimationEnabled(true);
		reportsMB.setTitle("Отчеты");
		reportsMB.addItem("1. Распределение по возрасту",EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.REPORT5_1));
		reportsMB.addItem("2. Распределение по возрасту, льготе",EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.REPORT5_2));
		reportsMB.addItem("3. Распределение по возрасту, МДОУ",EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.REPORT5_3));
//		reportsMB.addItem("4. Распределение путевок по МДОУ",EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.REPORT5_4));
//		reportsMB.addItem("5. Освобожденные места в МДОУ",EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.REPORT5_5));
//		
		 
		reportsMB.addItem(getReportMI("4. Прогноз по годам","reportPrognoz"));
		reportsMB.addItem(getReportMI("5. Распределение очередников по «живой очереди» МДОУ и районам","reportQueueByMdou"));
		reportsMB.addItem("6. Структура очереди по годам",EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.REPORT5_5));
//		reportsMB.addItem("График 1. Динамика поступления заявок в очередь за период. По возрастам",EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.CHART1));
		mainMenu.addItem("Отчеты", reportsMB);

		MenuBar booksMB = new MenuBar(true);
		booksMB.setAnimationEnabled(true);
		booksMB.setTitle("Справочники");
		booksMB.addItem("Вид МДОУ" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_JURIDICAL_TYPES_LIST ) );
		booksMB.addItem("Группы МДОУ" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_DOU_GROUPS_LIST));
		booksMB.addItem("Льготы" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_PRIVILEGES_LIST));
		booksMB.addItem("Районы" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_REGIONS_LIST));
		booksMB.addItem("Основания для протокола" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_FOUNDATIONS_LIST));

//		booksMB.addItem(new MenuItem("Возраст", ACTION_EMPTY));
		mainMenu.addItem("Справочники", booksMB);
		
		
		MenuBar helpMB = new MenuBar(true);
		helpMB.setAnimationEnabled(true);
		helpMB.setTitle("Справка");
//		helpMB.addItem("Руководство пользователя" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_USER_GUIDE) );
		helpMB.addItem("Сайт" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_SITE));
//		helpMB.addItem("Вики учебник" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_WIKI));
//		helpMB.addItem("Форум" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_FORUM));
//		helpMB.addItem("Вопросы" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_FAQ));
//		helpMB.addItem("Регистрация на ресурсе JBalance" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_REGISTRATION));
		helpMB.addItem("Питание в ДОУ" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_DOU_DIET));
		
		helpMB.addSeparator();
		helpMB.addItem("О программе" , EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_ABOUT));
		

//		сайт http://jbalance.org/portal/equeue/
//		Вики учебник http://jbalance.org/portal/equeue/wiki?initialURI=/portal/equeue/wiki
//		Форум - http://jbalance.org/portal/equeue/forum
//		Вопросы - http://jbalance.org/portal/equeue/faq
		
		mainMenu.addItem("Справка", helpMB);
		
		
		mainMenu.addItem("Выход", EQueueNavigation.instance().getMenuItemByUrl(EQueueNavigation.URL_LOGOUT));
		
	}

	
	MenuItem getReportMI(String miName,final String report){
		return new MenuItem(miName, new Command() {
			@Override
			public void execute() {
				Window.open("equeue/report?type="+report, "blank_", null);
				
			}
		});
	}
	
	
	/**
	 * Set the content to display.
	 * 
	 * @param content
	 *            the content
	 */
	public void setContent(final ContentWidget content) {
		if (content == null) {
			contentPanel.setWidget(null);
		} else {
			content.initialize();
			contentPanel.setWidget(content);
		}
	}

	void setupHistory() {
		// Setup a history handler to reselect the associate menu item.
		final ValueChangeHandler<String> historyHandler = new ValueChangeHandler<String>() {
			public void onValueChange(ValueChangeEvent<String> event) {
				
//				#350 USABILITY: Менять URL (добавлять UID редактируемой записи) при редектировании РН/протокола/контрагента
//				Не уверен, как сделать это красиво
				String url = event.getValue();
				String path;
				int ind = url.indexOf('?');
				Map<String,Serializable> params = null;
//				Ок, значит есть параметры в URL, давай их парсить
				if(ind != -1){
					path = url.substring(0, ind);
					params = parseParams(url.substring(ind+1));
				} else {
//					параметров в URL нет
					path = url;
				}
				Log.debug("path: "+path);
				// Get the content widget associated with the history token.
				CommandImpl command = EQueueNavigation.instance().getMenuItemByUrl(path);
				if (command != null){
					command.execute(params);
				} else {
					setContent(null);
				}
			}

			private Map<String, Serializable> parseParams(String paramsStr) {
				Map<String, Serializable> res = new HashMap<String, Serializable>();
				
				String[] pairs = paramsStr.split("&");
			    for (String pair : pairs) {
			        int idx = pair.indexOf("=");
					res.put(pair.substring(0, idx),pair.substring(idx + 1));
			    }
				Log.debug("parseParams("+paramsStr+") => "+res);
				return res;
			}
		};
		History.addValueChangeHandler(historyHandler);
	}

}
