package org.itx.jbalance.equeue.gwt.client.contragents;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyDisplay;
import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.Juridical;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.DialogBox;

public interface ContragentListPresenter {

	public interface Display extends MyDisplay{
		public void showDous(List<HStaffDivision> list);
		public void showContragents(List<Juridical> list);
		public DialogBox createDialogBox(int requestsAmount, ClickHandler showRequests,
				ClickHandler  removeDouFromRequests, ClickHandler cancel);
	}
	
	void deleteDou(Long id,Boolean releteAssociatedRequests);
	void deleteJur(Long id);
	void update(HStaffDivision juridical);
	void updateJur(Juridical juridical);
//	int getKm(HStaffDivision hStaffDivision);
//	int getKg(HStaffDivision hStaffDivision);
	//Map<Integer, Integer> getKm();
	//Map<Integer, Integer> getKg();
	
}
