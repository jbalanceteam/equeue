package org.itx.jbalance.equeue.gwt.client.permits;

import java.util.List;

import org.itx.jbalance.equeue.gwt.client.MyDisplay;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;

public interface PermitsListPresenter {
	public final static int PAGE_SIZE=50;
	public interface Display extends MyDisplay{}
	void initialize();
	
	/**
	 * Run search and populate requests table on the View
	 * @param end 
	 * @param start 
	 */
	void loadData(int start, int end,List<SortedColumnInfo>sortInfo);
	void search();
	GetPermitsRequest getSearchParams();
}
