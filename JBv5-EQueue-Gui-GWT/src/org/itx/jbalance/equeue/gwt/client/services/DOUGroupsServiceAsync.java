package org.itx.jbalance.equeue.gwt.client.services;

import java.util.ArrayList;

import org.itx.jbalance.l0.o.DOUGroups;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface DOUGroupsServiceAsync {

	void getAll(AsyncCallback<ArrayList<DOUGroups>> callback);

}
