package org.itx.jbalance.equeue.gwt.client.services;

import java.io.Serializable;
import java.util.List;

public class  SearchResultWrapper <T> implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private List<T>list;
	private Integer allCount;
	private Integer searchCount;
	public SearchResultWrapper(){};
	public SearchResultWrapper(List<T> list, Integer allCount,
			Integer searchCount) {
		super();
		this.list = list;
		this.allCount = allCount;
		this.searchCount = searchCount;
	}
	public List<T> getList() {
		return list;
	}
	public Integer getAllCount() {
		return allCount;
	}
	public Integer getSearchCount() {
		return searchCount;
	}
	
	
	
}
