package org.itx.jbalance.equeue.gwt.client.regions;

import java.util.ArrayList;

import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.regions.RegionsPresenter.Display;
import org.itx.jbalance.equeue.gwt.client.services.RegionsService;
import org.itx.jbalance.equeue.gwt.client.services.RegionsServiceAsync;
import org.itx.jbalance.l0.o.Region;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class RegionsPresenterImpl extends MyPresenter<Display>
		implements RegionsPresenter {

	
	private final RegionsServiceAsync regionsService = GWT.create(RegionsService.class);
	
	ArrayList<Region> rows;
	
	public RegionsPresenterImpl(RegionsWidget display) {
		super(display);
	}

	@Override
	public void create() {
		
		final String name = getDisplay().getRegionsName().getText().trim();
		
		if(name.isEmpty()){
			getDisplay().showWarningNotification("Заполните поле \"Наименование\"");
		return;
		}
		
		
		for(Region p:rows){
			if(p.getName().equalsIgnoreCase(name)){
				getDisplay().showWarningNotification("Такое ужо есьт");
				return;
			}
		}
		Region p = new Region();
		p.setName(name);
		p.setDescription(getDisplay().getDescription().getText());
		regionsService.createNew(p, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				loadData();
				getDisplay().getRegionsName().setText("");
				getDisplay().getDescription().setText("");
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}

	@Override
	public void delete(Long id) {
		regionsService.delete(id, new AsyncCallback<Void>() {
			
			@Override
			public void onSuccess(Void result) {
				loadData();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}

	@Override
	public void initialize() {
		loadData();
		initListeners();
	}

	private void initListeners() {
		getDisplay().getSaveClickHandlers().addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				create();
				
			}
		});
		
	}

	public void loadData(){
		regionsService .getAll(new AsyncCallback<ArrayList<Region>>() {
			
			@Override
			public void onSuccess(ArrayList<Region> result) {
				rows=result;
				getDisplay().showRegions(rows);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}

	@Override
	public void update(Region region) {
		regionsService.update(region, new AsyncCallback<Void>() {
			@Override
			public void onSuccess(Void result) {}
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
		
	}
}









