package org.itx.jbalance.equeue.gwt.client;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.i18n.client.constants.TimeZoneConstants;

public class DateUtils {
	
	/**
	 * @deprecated calculate age on the server side using AgeCalculator
	 * @param currentDate
	 * @param birthday
	 * @return
	 */
	@Deprecated
	static public  String getAge(Date currentDate,Date birthday){
		if (currentDate== null)
			currentDate=new Date();
			
		int dd2=currentDate.getDate();
	     int mm2=currentDate.getMonth();
	     int yy2=currentDate.getYear();
	     
	     
	     int dd1= birthday.getDate();
	     int mm1= birthday.getMonth();
	     int yy1= birthday.getYear();

		
	     int year=0;
	     int mm3=0;
	   				     
	     if(mm1>mm2)
	     {
	         year=yy2-yy1-1;
	         mm3=12+mm2-mm1;         
	     }  else if(mm1<mm2&&dd1>dd2) {
	    	 year=yy2-yy1;
	    	 mm3=mm2-mm1-1;     
	     }  else if(mm1<mm2) {	 
			 year=yy2-yy1;
			 mm3=mm2-mm1;
	     } else if(mm1==mm2&&dd1<dd2) {
	         year=yy2-yy1;
	         mm3=mm2-mm1;     
	     } else if(mm1==mm2&&dd1>dd2) {
	         year=yy2-yy1-1;
	         mm3=11;     
	     } else if(mm1==mm2&&dd1==dd2) {
	    	 year=yy2-yy1;
	    	 mm3=mm2-mm1;
	     }
	   
		
		return year+"."+mm3;
	}
	
	static TimeZoneConstants t = (TimeZoneConstants) GWT.create(TimeZoneConstants.class);
	static TimeZone msk = TimeZone.createTimeZone(t.europeMoscow());
	  
	static public String format(Date date,String format){ 
	  if(date==null)
		  return "";
	  int offset = /* msk.isDaylightTime(date) ? (- 3*60) :*/ (-4*60 );
	  TimeZone tz = TimeZone.createTimeZone(offset);
	  DateTimeFormat f = DateTimeFormat.getFormat(format); 
	  return f.format(date,tz);
	}
	
	static public String format(Date date){ 
	  return format(date,"dd.MM.yy");
	}
}
