package org.itx.jbalance.equeue.gwt.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.text.shared.AbstractRenderer;
import com.google.gwt.text.shared.Renderer;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ValueListBox;

public class DateChooserComponent extends FlowPanel
	implements HasValue<Date>{
	

	Renderer<Integer> intRenderer=  new AbstractRenderer<Integer>() {
    	@Override
		public String render(Integer object) {
			return object+"";
		}
	};
	
	Renderer<Integer> monthRenderer=  new AbstractRenderer<Integer>() {
		private String[] names={"Янв","Фев","Мар","Апр","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"};
    	@Override
		public String render(Integer m) {
    		if(m==null)
    			return "";
			return names[m-1];
		}
	};
	
	
	private ValueListBox<Integer> yearListBox=new ValueListBox<Integer>(intRenderer);
	private ValueListBox<Integer> monthListBox=new ValueListBox<Integer>(monthRenderer);
	private ValueListBox<Integer> dayListBox=new ValueListBox<Integer>(intRenderer);
	
	public DateChooserComponent (){
		super();
		Date date = new Date();
		int currentYear = date.getYear()+1900;
		init(currentYear);
	}

	/**
	 * При заполнении комбобокса с годами, максимальное значене будет  maxYear
	 * @param maxYear
	 */
	public DateChooserComponent (Integer maxYear){
		super();
		init(maxYear);
	}



	private void init(Integer maxYear) {
		Date date = new Date();
		Collection<Integer> years=new ArrayList<Integer>();
		
		for(int y=maxYear;y>maxYear-15;y--){
			years.add(y);
		}
		yearListBox.setValue(date.getYear()+1900);
		yearListBox.setAcceptableValues(years);
		Collection<Integer> monthes=new ArrayList<Integer>();
		for(int m=1;m<=12;m++){
			monthes.add(m);
		}
		monthListBox.setValue(date.getMonth()+1);
		monthListBox.setAcceptableValues(monthes);
		Collection<Integer> days=new ArrayList<Integer>();
		for(int m=1;m<=31;m++){
			days.add(m);
		}
		dayListBox.setValue(date.getDate());
		dayListBox.setAcceptableValues(days);
		
		
		add(dayListBox);
		add(monthListBox);
		add(yearListBox);
	}
	
	public void setEnable(Boolean e){
		if(e==null){
			return;
		}
		DOM.setElementPropertyBoolean(yearListBox.getElement(), "disabled", !e);
		DOM.setElementPropertyBoolean(monthListBox.getElement(), "disabled", !e);
		DOM.setElementPropertyBoolean(dayListBox.getElement(), "disabled", !e);
	}

//	List<ValueChangeHandler<Date>> valueChangeHandlers=new ArrayList<ValueChangeHandler<Date>>();
	
	 
	@Override
	public HandlerRegistration addValueChangeHandler(
			final ValueChangeHandler<Date> handler) {
		
		ValueChangeHandler<Integer> internalHandler = new ValueChangeHandler<Integer>() {
			@Override
			public void onValueChange(ValueChangeEvent<Integer> event) {
				ValueChangeEvent<Date> valueChangeEvent = new ValueChangeEvent<Date>(DateChooserComponent.this.getValue()){};
				handler.onValueChange(valueChangeEvent);
			}
		};
		yearListBox.addValueChangeHandler(internalHandler);
		monthListBox.addValueChangeHandler(internalHandler);
		dayListBox.addValueChangeHandler(internalHandler);
		return null;//addHandler(handler ,new GwtEvent.Type<Date>());
	}

	@Override
	public Date getValue() {
		Date date = new Date(yearListBox.getValue() - 1900, monthListBox.getValue() - 1, dayListBox.getValue());
		date.setHours(12);
		return date;
	}

	@Override
	public void setValue(Date value) {
		if(value==null){
			value = new Date();
		}
		yearListBox.setValue(value.getYear()+1900);
		monthListBox.setValue(value.getMonth()+1);
		dayListBox.setValue(value.getDate());
	}

	@Override
	public void setValue(Date value, boolean fireEvents) {
		setValue( value);
	}
}
