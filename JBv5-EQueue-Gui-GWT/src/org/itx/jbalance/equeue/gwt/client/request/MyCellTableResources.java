package org.itx.jbalance.equeue.gwt.client.request;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.CellTable.Resources;
public interface MyCellTableResources extends Resources {

        public MyCellTableResources INSTANCE =
                GWT.create(MyCellTableResources.class);

        /**
         * The styles used in this widget.
         */
        @Source("CellTable.css")
        CellTable.Style cellTableStyle();

}
