package org.itx.jbalance.equeue.gwt.client.services;

import java.util.ArrayList;

import org.itx.jbalance.l0.o.Region;



import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("regions")
public interface RegionsService extends RemoteService {
	public ArrayList<Region> getAll();
	public void createNew(Region p);
	void delete(Long p);
	void update(Region p);
}
