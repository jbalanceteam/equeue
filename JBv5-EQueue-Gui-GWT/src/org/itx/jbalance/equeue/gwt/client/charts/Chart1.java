package org.itx.jbalance.equeue.gwt.client.charts;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;

import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.visualizations.corechart.AxisOptions;
import com.google.gwt.visualization.client.visualizations.corechart.ColumnChart;
import com.google.gwt.visualization.client.visualizations.corechart.CoreChart;
import com.google.gwt.visualization.client.visualizations.corechart.Options;

public class Chart1 extends ContentWidget{

	
		
		
	
	
	
	 public Widget getWidget1() {
		    VerticalPanel result = new VerticalPanel();
		    Options options = CoreChart.createOptions();
		    options.setHeight(440);
		    options.setTitle("Динамика поступления заявок в очередь за период. По возрастам");
		    options.setWidth(800);

		    AxisOptions vAxisOptions = AxisOptions.create();
		    vAxisOptions.setMinValue(0);
//		    vAxisOptions.setMaxValue(1200);
		    options.setVAxisOptions(vAxisOptions);

		    DataTable data = getCompanyPerformance();
		    ColumnChart viz = new ColumnChart(data, options);

//		    Label status = new Label();
//		    Label onMouseOverAndOutStatus = new Label();
		    
		    
//		    viz.addSelectHandler(new SelectionDemo(viz, status));
//		    viz.addReadyHandler(new ReadyDemo(status));
//		    viz.addOnMouseOverHandler(new OnMouseOverDemo(onMouseOverAndOutStatus));
//		    viz.addOnMouseOutHandler(new OnMouseOutDemo(onMouseOverAndOutStatus));
//		    result.add(status);
		    result.add(viz);
//		    result.add(onMouseOverAndOutStatus);
		    return result;
		  }
	 
	 
	 static DataTable getCompanyPerformance() {
		    DataTable data = getCompanyPerformanceWithNulls();
		    
		   
		    return data;
		  }
	 
	 
	 static DataTable getCompanyPerformanceWithNulls() {
		    DataTable data = DataTable.create();
		    data.addColumn(ColumnType.STRING, "Год");
		    data.addColumn(ColumnType.NUMBER, "Девочки");
		    data.addColumn(ColumnType.NUMBER, "Мальчики");
		    data.addRows(4);
		    data.setValue(0, 0, "2008");
		    data.setValue(0, 1, 12);
		    data.setValue(0, 2, 151);
		    data.setValue(1, 0, "2009");
		    data.setValue(1, 1, 345);
		    data.setValue(1, 2, 122);
		    
		    data.setValue(2, 0, "2010");
		    data.setValue(2, 1, 235);
		    data.setValue(2, 2, 123);
		    
		    data.setValue(3, 0, "2011");
		    data.setValue(3, 1, 112);
		    data.setValue(3, 2, 421);
		    return data;
		  }


	@Override
	public void initialize() {
//VisualizationUtils.loadVisualizationApi(new Runnable() {
//			
//			@Override
//			public void run() {
				initWidget( getWidget1());
//				initWidget(new Label("ХУЙ"));
//			}
//		},AnnotatedTimeLine.PACKAGE, CoreChart.PACKAGE,
//        Gauge.PACKAGE, GeoMap.PACKAGE, ImageChart.PACKAGE,
//        ImageLineChart.PACKAGE, ImageAreaChart.PACKAGE, ImageBarChart.PACKAGE,
//        ImagePieChart.PACKAGE, IntensityMap.PACKAGE,
//        MapVisualization.PACKAGE, MotionChart.PACKAGE, OrgChart.PACKAGE,
//        Table.PACKAGE,
//        ImageSparklineChart.PACKAGE);
//		
	}

	 
}
