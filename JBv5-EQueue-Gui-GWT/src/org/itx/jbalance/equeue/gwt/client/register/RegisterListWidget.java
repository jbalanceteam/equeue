package org.itx.jbalance.equeue.gwt.client.register;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ClickableSafeHtmlCell;
import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.EQueueNavigation;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.equeue.gwt.client.services.SearchResultWrapper;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l2_api.dto.equeue.RegisterDTO;

import com.google.gwt.cell.client.ActionCell;
import com.google.gwt.cell.client.ActionCell.Delegate;
import com.google.gwt.cell.client.CompositeCell;
import com.google.gwt.cell.client.DateCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.HasCell;
import com.google.gwt.cell.client.ImageCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.DateTimeFormat.PredefinedFormat;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.ColumnSortList;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent.Handler;
import com.google.gwt.view.client.SingleSelectionModel;

public class RegisterListWidget extends ContentWidget implements RegisterListPresenter.Display {

	interface Binder extends UiBinder<Widget, RegisterListWidget> {

	}

	@UiField(provided = false)
	TextBox searchField;
	
//	@UiField(provided = false)
//	Button searchButton;
	
	@UiField(provided = false)
	Button cleanButton;
	
	
	@UiField(provided = false)
	HTML searchResLabel;
	
	@UiField(provided = false)
	Button addButton;
	
	@UiField(provided = true)
	CellTable<SRegister> requestsTable=new CellTable<SRegister>(0,MyCellTableResources.INSTANCE);
	
	@UiField(provided = true)
	SimplePager simplePager=new SimplePager();
	
	@UiField(provided=false)
	DateBox openDateFrom;
	
	@UiField(provided=false)
	DateBox openDateTo;
	
	AsyncDataProvider<RegisterDTO> provider;

	ProvidesKey<RegisterDTO> keyProvider;
	
	@UiField (provided = true)
	CellTable<RegisterDTO> registerTable=new CellTable<RegisterDTO>(RegisterListPresenter.PAGE_SIZE,MyCellTableResources.INSTANCE);
	
	@Override
	public void initialize() {
       
		Binder uiBinder = GWT.create(Binder.class);
		initWidget(uiBinder.createAndBindUi(this));
		initTableColumns();
		initSpecTableColumns();
		
		keyProvider=new ProvidesKey<RegisterDTO>() {

			@Override
			public Object getKey(RegisterDTO item) {
				return item.getUId();
			}
		};
		
		final SingleSelectionModel<RegisterDTO> selectionModel=new SingleSelectionModel<RegisterDTO>(keyProvider);
		registerTable.setSelectionModel(selectionModel);
		registerTable.setPageSize(RegisterListPresenter.PAGE_SIZE);
		simplePager.setDisplay(registerTable);
		requestsTable.setRowCount(0);
		
		DateTimeFormat dateTimeFormat = DateTimeFormat.getFormat("dd.MM.yy");
		DateBox.Format format=new DateBox.DefaultFormat(dateTimeFormat); 
		openDateFrom.setFormat(format); 
		openDateTo.setFormat(format); 

		selectionModel.addSelectionChangeHandler(new Handler() {
			@Override
			public void onSelectionChange(SelectionChangeEvent event) {
				RegisterDTO selectedRequest = selectionModel.getSelectedObject();
				getPresenter().populateDetails(selectedRequest);
			}
		});
	}

	
	@UiHandler("searchButton")
	public void runSearch(ClickEvent event){
		if(getOpenDateFrom().getValue() != null && getOpenDateTo().getValue() != null && 
				getOpenDateFrom().getValue().after(getOpenDateTo().getValue())
		) {
			showWarningNotification("Дата \"с\" была меньше даты \"по\".");
			return;
		}
		getPresenter().search();
	}
	
	@UiHandler("cleanButton")
	public void cleanSearch(ClickEvent event){
		getSearchField().setText("");
		getOpenDateFrom().setValue(null);
		getOpenDateTo().setValue(null);
		getPresenter().search();
	}
	
	@UiHandler("addButton")
	public void clickNew(ClickEvent event){
		EQueueNavigation.instance().goToRegisterEditPage(null);
	}
	
	private void initTableColumns() {
		/* num */
		Column<RegisterDTO, String> idColumn = new Column<RegisterDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(RegisterDTO object) {
				return object.getDocNumber();
			}
		};

		registerTable.addColumn(idColumn, SafeHtmlUtils.fromSafeConstant("Номер протокола"));
		
		
		/* reg date */
		Column<RegisterDTO, Date> regDateColumn = new Column<RegisterDTO, Date>(
				new DateCell(DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT))) {
			@Override
			public Date getValue(RegisterDTO object) {
				return object.getRegisterDate() ;
			}
		};

		registerTable.addColumn(regDateColumn, SafeHtmlUtils.fromSafeConstant("Дата"));
		
		/* receive due date */
		Column<RegisterDTO, Date> receiveDueDateColumn = new Column<RegisterDTO, Date>(
				new DateCell(DateTimeFormat.getFormat(PredefinedFormat.DATE_SHORT))) {
			@Override
			public Date getValue(RegisterDTO object) {
				return object.getReceiveDueDate() ;
			}
		};

		registerTable.addColumn(receiveDueDateColumn, SafeHtmlUtils.fromSafeConstant("Дата уведомления"));

	
		/* child count */
		Column<RegisterDTO, String> childCountColumn = new Column<RegisterDTO, String>(new TextCell()) {
			@Override
			public String getValue(RegisterDTO object) {
				return object.getChildrenCount() + "" ;
			}
		};

		registerTable.addColumn(childCountColumn, SafeHtmlUtils.fromSafeConstant("Количество очередников"));
		
		
		
		ImageCell imageCell = new ImageCell();
		Column<RegisterDTO, String> statusColumn = new Column<RegisterDTO, String>(imageCell) {

			@Override
			public String getValue(RegisterDTO register) {
				switch(register.getStatus()){
				case FIRST: 	return "images/one-icon.png";
				case SECOND: 	return "images/two-icon.png";
				case THIRD: 	return "images/three-icon.png";
			}
			return "";
			
			}};
			registerTable.addColumn(statusColumn, "Статус");
			registerTable.setColumnWidth(statusColumn,"50px");
		
		

		
		/*  members */
		Column<RegisterDTO, String> membersColumn = new Column<RegisterDTO, String>(
				new TextCell()) {
			@Override
			public String getValue(RegisterDTO object) {
				StringBuffer sb=new StringBuffer();
				if(object.getMember1()!=null)
					sb.append(object.getMember1());
				
				if(object.getMember2()!=null){
					if(sb.length()>0)
						sb.append(", ");
					sb.append(object.getMember2());
				}
				
				if(object.getMember3()!=null){
					if(sb.length()>0)
						sb.append(", ");
					sb.append(object.getMember3());
				}
				return sb.toString();
			}
		};

		registerTable.addColumn(membersColumn, SafeHtmlUtils.fromSafeConstant("Комиссия"));

	
		
		
		
		
		
		/* actions */
		final List<HasCell<RegisterDTO, ?>>  hasCells = new ArrayList<HasCell<RegisterDTO, ?>>();
		
		
		/* edit */ 
		ActionCell<RegisterDTO> editCell = new ActionCell<RegisterDTO>(
				SafeHtmlUtils.fromSafeConstant("<img src='images/edit.png' />"),new Delegate<RegisterDTO>() {
		
			@Override
			public void execute(RegisterDTO p) {
				HashMap<String, Serializable> params = new HashMap<String, Serializable>();
				params.put("objectUId", p == null ? null : p.getUId());
				EQueueNavigation.instance().goToRegisterEditPage(params);
			}
		});
		
		
		Column <RegisterDTO,RegisterDTO>editColumn =new Column<RegisterDTO,RegisterDTO>(
				editCell){
				@Override
				public RegisterDTO getValue(RegisterDTO object) {
					return object;
				}}; 
		hasCells.add(editColumn);		
		
		/* delete */ 
		ActionCell<RegisterDTO> delCell = new ActionCell<RegisterDTO>(
				SafeHtmlUtils.fromSafeConstant("<img src='images/delete.png' />"),new Delegate<RegisterDTO>() {
		
			@Override
			public void execute(RegisterDTO p) {
				if(p.getStatus().getNumber() > 1){
					showWarningNotification("Только \"черновой\" или \"предварительный\" протокол можно удалить");
				} else if(Window.confirm("Удалить безвозвратно?")){
					getPresenter().delete(p.getUId());
				}
			}
		});
		
		
		Column <RegisterDTO,RegisterDTO>delColumn =new Column<RegisterDTO,RegisterDTO>(
			delCell){
				@Override
				public RegisterDTO getValue(RegisterDTO object) {
					return object;
				}}; 
		hasCells.add(delColumn);		
				
				
		
		
		/* export protocol */ 
		final SafeHtml fromSafeConstant = SafeHtmlUtils.fromSafeConstant("<img src='images/export.png' alt='Экспорт протокола'/>" );
		final ActionCell<RegisterDTO> exportCell = new ActionCell<RegisterDTO>(
				fromSafeConstant,
				new Delegate<RegisterDTO>() {
			@Override
			public void execute(final RegisterDTO p) {
				downloadRegisterReport(p);
			}

			
		});
		
		
		Column <RegisterDTO,RegisterDTO>exportColumn =new Column<RegisterDTO,RegisterDTO>(
				exportCell){
				@Override
				public RegisterDTO getValue(RegisterDTO object) {
					return object;
				}}; 
				
		hasCells.add(exportColumn);
		
		/* notifications report  */ 
		ActionCell<RegisterDTO> printCell = new ActionCell<RegisterDTO>(
				SafeHtmlUtils.fromSafeConstant("<img src='images/print_icon.png' alt='Экспорт путевок'/>"),new Delegate<RegisterDTO>() {
			@Override
			public void execute(RegisterDTO p) {
				downloadNotificationReport(p);
			}
		});
		
		Column <RegisterDTO,RegisterDTO> printColumn =new Column<RegisterDTO,RegisterDTO>(
				printCell){
				@Override
				public RegisterDTO getValue(RegisterDTO object) {
					return object;
				}}; 
				
  		hasCells.add(printColumn);
  		/*  */ 
		
		CompositeCell<RegisterDTO> cell=new CompositeCell<RegisterDTO>(
			hasCells );
		
		
		Column <RegisterDTO,RegisterDTO>actionColumn =new Column<RegisterDTO,RegisterDTO>(
			cell){
		
				@Override
				public RegisterDTO getValue(RegisterDTO object) {
					return object;
				}}; 
		registerTable.addColumn(actionColumn, SafeHtmlUtils.fromSafeConstant("Действие"));
	
	}
	
	
	private void downloadNotificationReport(final RegisterDTO p) {
		if(p.getStatus().getNumber() < 2){
			showWarningNotification("Запрещена печать списка очередников из \"чернового\" протокола.");
		} else {
			
			
			Button all = new Button("Все", new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					String urlStr = "equeue/report?type=tickets";
					urlStr+="&hRegisterUidParam="+p.getUId().toString();
					Window.open(urlStr, "blank_", null);
				}
			});
			
			Button givePermit = new Button("Выданные путевки", new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					String urlStr = "equeue/report?type=ticketsGivePermit";
					urlStr+="&hRegisterUidParam="+p.getUId().toString();
					Window.open(urlStr, "blank_", null);
				}
			});
			
			
			Button refusal = new Button("Отказы", new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					String urlStr = "equeue/report?type=ticketsRefusal";
					urlStr+="&hRegisterUidParam="+p.getUId().toString();
					Window.open(urlStr, "blank_", null);

				}
			});
			
			Button backToQueue = new Button("Возвращенные в очередь", new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					String urlStr = "equeue/report?type=ticketsBackToQueue";
					urlStr+="&hRegisterUidParam="+p.getUId().toString();
					Window.open(urlStr, "blank_", null);
				}
			});
			
			Button cancel = new Button("Отмена", new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {}
			});
			
			
			DialogBox dialog = askQuestion("Выберите тип отчета", new HTML("Какие записи экспортировать?"),
					all, givePermit, refusal, backToQueue, cancel);
			dialog.center();
			dialog.setModal(true);
			dialog.show();
		}
		
	}
	

	/**
	 * Отчет с содержимым протокола
	 * @param p 
	 */
	private void downloadRegisterReport(final RegisterDTO p) {
		Button all = new Button("Все", new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String urlStr = "equeue/report?type=protocol"
					+ "&hRegisterUidParam="+p.getUId().toString();
					Window.open(urlStr, "blank_", null);
			}
		});
		
		Button givePermit = new Button("Выданные путевки", new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String urlStr = "equeue/report?type=protocolGivePermit"
					+ "&hRegisterUidParam="+p.getUId().toString();
					Window.open(urlStr, "blank_", null);
			}
		});
		
		
		Button refusal = new Button("Отказы", new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String urlStr = "equeue/report?type=protocolRefusal"
					+ "&hRegisterUidParam="+p.getUId().toString();
					Window.open(urlStr, "blank_", null);
			}
		});
		
		Button backToQueue = new Button("Возвращенные в очередь", new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				String urlStr = "equeue/report?type=protocolBackToQueue"
					+ "&hRegisterUidParam="+p.getUId().toString();
					Window.open(urlStr, "blank_", null);
			}
		});
		
		Button cancel = new Button("Отмена", new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {}
		});
		
		
		DialogBox dialog = askQuestion("Выберите тип отчета", new HTML("Какие записи экспортировать?"),
				all, givePermit, refusal, backToQueue, cancel);
		dialog.center();
		dialog.setModal(true);
		dialog.show();
	}
	
	private void initSpecTableColumns(){
		
		/*********     number     ***********/
//		#351: USABILITY: Возможность открыть РН по ссылке из протокола
		Column<SRegister, SafeHtml> idColumn = new Column<SRegister, SafeHtml>(
		        new ClickableSafeHtmlCell()) {
		    @Override
		    public SafeHtml getValue(SRegister line) {
		        SafeHtmlBuilder sb = new SafeHtmlBuilder();
		        sb.appendHtmlConstant("<a href='javascript:void(0);'>");
		        sb.appendEscaped(line.getDouRequest().toString());
		        sb.appendHtmlConstant("</a>");
		        return sb.toSafeHtml();
		    }
		};

		idColumn.setFieldUpdater(new FieldUpdater<SRegister, SafeHtml>() {
		        @Override
		        public void update(int index, SRegister line, SafeHtml value) {
		            HashMap<String, Serializable> p = new HashMap<String, Serializable>();
		            p.put("objectUId", line.getDouRequest().getUId());
					EQueueNavigation.instance().goToDouRequestEditPage(p);
		        }
		    });
		
//		Column<SRegister, String> idColumn = new Column<SRegister, String>(
//				new TextCell()) {
//			@Override
//			public String getValue(SRegister object) {
//				return object.getSeqNumber()+"";
//			}
//		};

		requestsTable.addColumn(idColumn, SafeHtmlUtils.fromSafeConstant("#"));
		
		
		/*********     РН     ***********/
		Column<SRegister, String> rnColumn = new Column<SRegister, String>(
				new TextCell()) {
			@Override
			public String getValue(SRegister object) {
				return object.getDouRequest()+"";
			}
		};
		
		requestsTable.addColumn(rnColumn, SafeHtmlUtils.fromSafeConstant("РН"));
		
		/*********     ФИО     ***********/
		
		Column<SRegister, String> nameColumn = new Column<SRegister, String>(
				new TextCell()) {
			@Override
			public String getValue(SRegister object) {
				Physical child = object.getDouRequest().getChild();
				return child.getSurname()+" " + child.getName()+" "+child.getPatronymic();
			}
		};

		requestsTable.addColumn(nameColumn, SafeHtmlUtils.fromSafeConstant("ФИО"));


		
		/*********     МДОУ     ***********/
		Column<SRegister, String> douColumn = new Column<SRegister, String>(
				new TextCell()) {
			@Override
			public String getValue(SRegister object) {
				if(object.getDou()!=null)
					return object.getDou().getNumber()+"  ("+object.getDou().getName()+")";
				else
					return "";
			}
		};
		
		requestsTable.addColumn(douColumn, SafeHtmlUtils.fromSafeConstant("МДОУ"));

		
		/*********     Действие     ***********/
		Column<SRegister, String> actionColumn = new Column<SRegister, String>(
				new TextCell()) {
			@Override
			public String getValue(SRegister object) {
				return object.getRnAction().getDescription();
			}
		};

		requestsTable.addColumn(actionColumn , SafeHtmlUtils.fromSafeConstant("Действие"));
		
		
		/*********     Основание     ***********/
		Column<SRegister, String>  foundationColumn = new Column<SRegister, String>(
				new TextCell()) {
			@Override
			public String getValue(SRegister object) {
				return object.getFoundation()==null?"":object.getFoundation().getDescription();
			}
		};

		requestsTable.addColumn(foundationColumn , SafeHtmlUtils.fromSafeConstant("Основание"));
		
		
		
		/*********     Статус     ***********/
		Column<SRegister, String> statusColumn = new Column<SRegister, String>(
				new TextCell()) {
			@Override
			public String getValue(SRegister object) {
				return object.getDouRequest().getStatus().getDescription();
			}
		};

		requestsTable.addColumn(statusColumn , SafeHtmlUtils.fromSafeConstant("Текущий статус РН"));
		
	
	}
	
	RegisterListPresenter presenter;

	public RegisterListPresenter getPresenter() {
		return presenter;
	}


	public void setPresenter(RegisterListPresenter presenter) {
		this.presenter=presenter;
	}

	/*@Override
	public void showDouRequests(SearchResultWrapper<RegisterDTO> searchRes) {
		registerTable.setRowData(searchRes.getList());
		searchResLabel.setHTML("Найдено <b>"+searchRes.getSearchCount() +"</b>. Всего <b>"+searchRes.getAllCount()+"</b>.");
		
	}*/
	
	int start=0;

	
	private List<SRegister> sRegisterList;
	
	
	public void updateRowData(int start,List<RegisterDTO>data){
		provider.updateRowData(start, data);
	}
	
public void showRegisters(SearchResultWrapper<RegisterDTO> searchRes) {
		if(provider==null){
		provider = new AsyncDataProvider<RegisterDTO>(keyProvider) {
		      @Override
		      protected void onRangeChanged(HasData<RegisterDTO> display) {
		        start = display.getVisibleRange().getStart();
		        int end = start + display.getVisibleRange().getLength();
		        ColumnSortList columnSortList = registerTable.getColumnSortList();
		        
		        List<SortedColumnInfo>sortedColumns=new LinkedList<SortedColumnInfo>();
//		        for(int i=0;i<columnSortList.size();i++){
		        if(columnSortList.size()>0)	
		        	sortedColumns.add(new SortedColumnInfo("rn",columnSortList.get(0).isAscending()));
		        else
		        	sortedColumns.add(new SortedColumnInfo("rn",false));
//		        }
		       
		        getPresenter().loadData(start,end,sortedColumns);
		      }
		    };
		    provider.addDataDisplay(registerTable);
		}else{
		
		}
		    
		 provider.updateRowCount(searchRes.getSearchCount(), true);
		 provider.updateRowData(0, searchRes.getList());
		 
//		requestsTable.setRowData(searchRes.getList());
		searchResLabel.setHTML("Найдено <b>"+searchRes.getSearchCount() +"</b>. Всего <b>"+searchRes.getAllCount()+"</b>.");
		
	}
	

	public HasText getSearchField() {
		return searchField;
	}

	public void setSearchField(TextBox searchField) {
		this.searchField = searchField;
	}

	public void populateSpec(List<SRegister> result) {
		setsRegisterList(result);
		requestsTable.setRowData(result);
	}


	public void setsRegisterList(List<SRegister> sRegisterList) {
		this.sRegisterList = sRegisterList;
	}


	public List<SRegister> getsRegisterList() {
		return sRegisterList;
	}
	

	public  HasValue<Date> getOpenDateFrom(){
		return openDateFrom;
	}
	
	public  HasValue<Date> getOpenDateTo(){
		return openDateTo;
	}
	

}
