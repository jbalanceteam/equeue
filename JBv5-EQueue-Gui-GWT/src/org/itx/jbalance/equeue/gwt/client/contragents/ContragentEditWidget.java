package org.itx.jbalance.equeue.gwt.client.contragents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.itx.jbalance.equeue.gwt.client.ContentWidget;
import org.itx.jbalance.equeue.gwt.client.EQueueNavigation;
import org.itx.jbalance.equeue.gwt.client.contragents.ContragentEditPresenter.Display;
import org.itx.jbalance.equeue.gwt.client.request.MyCellTableResources;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l0.o.JuridicalType;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.o.StaffDivision;
import org.itx.jbalance.l0.s.SStaffDivision;

import com.google.gwt.cell.client.EditTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.SelectionCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.ProvidesKey;
import com.google.gwt.view.client.SingleSelectionModel;

import eu.maydu.gwt.validation.client.DefaultValidationProcessor;
import eu.maydu.gwt.validation.client.ValidationProcessor;
import eu.maydu.gwt.validation.client.actions.FocusAction;
import eu.maydu.gwt.validation.client.actions.StyleAction;
import eu.maydu.gwt.validation.client.validators.standard.NotEmptyValidator;

public class ContragentEditWidget extends ContentWidget implements Display {

	interface Binder extends UiBinder<Widget, ContragentEditWidget> {

	}

	@UiField
	TextBox nameTextBox;

	@UiField
	TextBox telTextBox;

	@UiField
	TextBox emailTextBox;

	@UiField
	TextBox innTextBox;

	@UiField
	TextBox fizAddressTextBox;

	@UiField
	TextBox jurAddressTextBox;

	@UiField
	CheckBox isDouCheckBox;

	@UiField
	ListBox douTypeListBox;

	@UiField
	ListBox	regionListBox;;
	
	@UiField
	TextBox idDouTextBox;
	
	@UiField
	Button addNewGroupButton;

	@UiField
	Button delNewGroupButton;

	@UiField(provided = true)
	//CellTable<StaffDivision> groupCellTable;
    CellTable<SStaffDivision> groupCellTable;

	List<SStaffDivision> rows = new ArrayList<SStaffDivision>();

	private ArrayList<JuridicalType> juridicalTypesItems = new ArrayList<JuridicalType>();
	private ArrayList<Region> regionsItems = new ArrayList<Region>();

	@UiField
	TextBox itogoTextBox;

	@UiField
	Button saveButton;

	@UiField
	Button canselButton;

	private ArrayList<DOUGroups> items = new ArrayList<DOUGroups>();

	/**
	 * Constructor.
	 * 
	 * @param constants
	 *            the constants
	 */
	public ContragentEditWidget() {
		super("Справочник котрагентов");
	}
	
	public void disableComponents(boolean checked) {
		itogoTextBox.setEnabled(checked);
          addNewGroupButton.setEnabled(checked);
          delNewGroupButton.setEnabled(checked);
          douTypeListBox.setEnabled(checked);
          regionListBox.setEnabled(checked);
          itogoTextBox.setEnabled(checked);
          idDouTextBox.setEnabled(checked);
          if (checked) 
           saveButton.setText("Сохранить МДОУ");
          else saveButton.setText("Сохранить контрагента");
	}

	@Override
	public void initialize() {
		setGroupCellTable(new CellTable<SStaffDivision>(0,MyCellTableResources.INSTANCE));
		getGroupCellTable().setRowData(new ArrayList<SStaffDivision>());
		// Create the UiBinder.
		Binder uiBinder = GWT.create(Binder.class);
		if(this.getWidget() == null){
			initWidget(uiBinder.createAndBindUi(this));
			}

		isDouCheckBox.setValue(new Boolean(true), false);
		isDouCheckBox.addClickHandler(new ClickHandler() {
		      public void onClick(ClickEvent event) {
		          boolean checked = ((CheckBox) event.getSource()).getValue();		          
		          disableComponents(checked);
		      }
		});

		refreshItogo();
		
		ProvidesKey<SStaffDivision> keyProvider = new ProvidesKey<SStaffDivision>() {
			@Override
			public Object getKey(SStaffDivision item) {
				return item.getDivision().getUId();
			}
		};
		final SingleSelectionModel<SStaffDivision> selectionModel = new SingleSelectionModel<SStaffDivision>(
				keyProvider);
		getGroupCellTable().setSelectionModel(selectionModel);
		initValidation();
	}
	



	private void initValidation() {
		FocusAction focusAction = new FocusAction();
		validator = new DefaultValidationProcessor();
		douValidator = new DefaultValidationProcessor();
		validator.addValidators("namenotEmpty",
				new NotEmptyValidator(nameTextBox)
					.addActionForFailure(focusAction)
					.addActionForFailure(new StyleAction("validationFailedBorder"))
		);
		
//		validator.addValidators("namenotEmpty",
//				new RegularExpressionValidator(nameTextBox,"[А-Яа-яA-Za-z]", "Поле Имя допускает только буквы") 
//		.addActionForFailure(focusAction)
//		.addActionForFailure(new StyleAction("validationFailedBorder"))
//		);

		validator.addValidators("telTextBoxnotEmpty",
				new NotEmptyValidator(telTextBox)
					.addActionForFailure(focusAction)
					.addActionForFailure(new StyleAction("validationFailedBorder"))
		);
		
//		validator.addValidators("emailTextBoxnotEmpty",
//				new NotEmptyValidator(emailTextBox)
//					.addActionForFailure(focusAction)
//					.addActionForFailure(new StyleAction("validationFailedBorder"))
//		);
//
//		validator.addValidators("innTextBoxnotEmpty",
//				new NotEmptyValidator(innTextBox)
//					.addActionForFailure(focusAction)
//					.addActionForFailure(new StyleAction("validationFailedBorder"))
//		);

		validator.addValidators("fizAddressTextBoxnotEmpty",
				new NotEmptyValidator(fizAddressTextBox)
					.addActionForFailure(focusAction)
					.addActionForFailure(new StyleAction("validationFailedBorder"))
		);
		
//		validator.addValidators("jurAddressTextBoxnotEmpty",
//				new NotEmptyValidator(jurAddressTextBox)
//					.addActionForFailure(focusAction)
//					.addActionForFailure(new StyleAction("validationFailedBorder"))
//		);
		
		
//		validator.addValidators("idDouTextBoxnotEmpty",
//				new NotEmptyValidator(idDouTextBox)
//					.addActionForFailure(focusAction)
//					.addActionForFailure(new StyleAction("validationFailedBorder"))
//		);
		
		douValidator.addValidators("idDouTextBoxnotEmpty",
				new NotEmptyValidator(idDouTextBox)
					.addActionForFailure(focusAction)
					.addActionForFailure(new StyleAction("validationFailedBorder"))
		);

	}
	
	public void initData() {
		ititDouTypes();
		ititRegions();
		initTableColumns();
	}

	public void ititDouTypes() {
		douTypeListBox.clear();
		for (JuridicalType juridicalType : juridicalTypesItems) {
			String name = juridicalType.getName();
			douTypeListBox.addItem(name);
		}
	}
	
	public void ititRegions() {
		regionListBox.clear();
		for (Region region : regionsItems) {
			String name = region.getName();
			regionListBox.addItem(name);
		}
	}

	private void refreshItogo(){
		int itogo=0;
		for(SStaffDivision division : rows){
			itogo=itogo+division.getDivision().getKm();
		}
		getItogoTextBox().setText(Integer.toString(itogo));
	}
	
	
	public void initTableColumns() {

		//getGroupCellTable().setPixelSize(300, 200);
		/* (0) number of group */
		Column<SStaffDivision, String> numGroupColumn = new Column<SStaffDivision, String>(
				new EditTextCell()) {
			@Override
			public String getValue(SStaffDivision object) {
				return object.getDivision().getGroupId() + "";
			}
		};
		getGroupCellTable().addColumn(numGroupColumn,
				SafeHtmlUtils.fromSafeConstant("№ группы"));
		numGroupColumn.setFieldUpdater(new FieldUpdater<SStaffDivision, String>() {
			@Override
			public void update(int index, SStaffDivision object, String value) {
				Integer num;
				try{
					num = new Integer(value);
					object.getDivision().setGroupId(num);
				}catch(NumberFormatException e){}
			}
		});
		
		/* (1) name */
		Column<SStaffDivision, String> nameColumn = new Column<SStaffDivision, String>(
				new EditTextCell()) {
			@Override
			public String getValue(SStaffDivision object) {
				return object.getDivision().getName() + "";
			}
		};
		getGroupCellTable().addColumn(nameColumn,
				SafeHtmlUtils.fromSafeConstant("Наименование"));
		nameColumn.setFieldUpdater(new FieldUpdater<SStaffDivision, String>() {
			@Override
			public void update(int index, SStaffDivision object, String value) {
				object.getDivision().setName(value);
				// getPresenter().update(object);
			}
		});

		/* (2) group - Вид Группы */
		List<String> itemNames = new ArrayList<String>();
		final HashMap<String, Integer> t = new HashMap<String, Integer>();
		final HashMap<Integer,String> tr = new HashMap<Integer,String>();
		for (DOUGroups douGroups : items) {
			String itemName = douGroups.getName();
			int itemId = douGroups.getId();
			itemNames.add(itemName);
			t.put(itemName, new Integer(itemId));
			tr.put(new Integer(itemId),itemName);
		}
		SelectionCell groupSelectionCell = new SelectionCell(itemNames);
		Column<SStaffDivision, String> groupColumn = new Column<SStaffDivision, String>(
				groupSelectionCell) {
			@Override
			public String getValue(SStaffDivision object) {
				return tr.get(object.getDivision().getDouGroupsId()) ;
			}
		};
		getGroupCellTable().addColumn(groupColumn,
				SafeHtmlUtils.fromSafeConstant("Группа"));
		getGroupCellTable().setColumnWidth(groupColumn, 230, Unit.PX);
		groupColumn.setFieldUpdater(new FieldUpdater<SStaffDivision, String>() {
			@Override
			public void update(int index, SStaffDivision object, String value) {
				object.getDivision().setDouGroupsId(t.get(value).intValue());
				// getPresenter().update(object);
			}
		});

		/* (3) km - количество мест в группе */
		Column<SStaffDivision, String> kmColumn = new Column<SStaffDivision, String>(
				new EditTextCell()) {
			@Override
			public String getValue(SStaffDivision object) {
				return object.getDivision().getKm() + "";
			}
		};
		getGroupCellTable()
				.addColumn(kmColumn, SafeHtmlUtils.fromSafeConstant("КМ"));
		kmColumn.setFieldUpdater(new FieldUpdater<SStaffDivision, String>() {

			@Override
			public void update(int index, SStaffDivision object, String value) {
				object.getDivision().setKm(new Integer(value).intValue());
				refreshItogo();
			}
		});
	}

	ValidationProcessor validator;
	
	ValidationProcessor douValidator;
	
	@Override
	public void setDOUGroupsItems(ArrayList<DOUGroups> list) {
		items = list;
	}

	List<SStaffDivision> getGroupsRows() {
		return rows;
	}

	ContragentEditPresenter presenter;

	private String selectedRegion;

	private String selectedDouType;

	public ContragentEditPresenter getPresenter() {
		return presenter;
	}

	public void setPresenter(ContragentEditPresenter presenter) {
		this.presenter = presenter;
	}

	@UiHandler("saveButton")
	public void save(ClickEvent event) {
		boolean validate = validator.validate();
		if (getIsDou().getValue()){
		 boolean douValidate = douValidator.validate();
		 if(validate&&douValidate)
			 if (getPresenter().getState()==State.EDIT_DOU)
			    getPresenter().updateDou();
			 else //if (getPresenter().getState()==State.NEW_DOU)
				 getPresenter().saveDou();
		}
		else {
			if(validate)
				if (getPresenter().getState()==State.EDIT_CONTRAGENT)
					getPresenter().updateContragent();
				else //if (getPresenter().getState()==State.NEW_CONTRAGENT)
					getPresenter().saveContragent();
		}
	}
	@UiHandler("canselButton")
	public void cansel(ClickEvent event) {
		//Map<String, Serializable> null;
		EQueueNavigation.instance().goToContractorsListPage(null);	
	}

	@UiHandler("addNewGroupButton")
	public void addNewGroup(ClickEvent event) {
		SStaffDivision e = new SStaffDivision();
		StaffDivision division = new StaffDivision();
		division.setName("");
		e.setDivision(division);
		//if(getPresenter().getState()==State.NEW_DOU){
		  rows.add(e);
		  getGroupCellTable().setRowData(rows);
		//}
		if(getPresenter().getState()==State.EDIT_DOU){
			getPresenter().getsStaffDivisionsNew().add(e);
		}
	}

	public HasValue<Boolean> getIsDou() {
		return isDouCheckBox;
	}

	public HasText getName() {
		return nameTextBox;
	}

	public HasText getTel() {
		return telTextBox;
	}

	public HasText getEmail() {
		return emailTextBox;
	}

	public HasText getInn() {
		return innTextBox;
	}

	public HasText getFizAddress() {
		return fizAddressTextBox;
	}

	public HasText getJurAddress() {
		return jurAddressTextBox;
	}
	public HasText getIdDou() {
		return idDouTextBox;
	}
	
	public HasText getItogoTextBox() {
		return itogoTextBox;
	}

	public String getDouType() {
		int selectedIndex = douTypeListBox.getSelectedIndex();
		return douTypeListBox.getValue(selectedIndex);
    }
	
	public int getDouTypeId() {
		return douTypeListBox.getSelectedIndex();
	}
	public ListBox getDouTypeListBox() {
		return douTypeListBox;
	}
	//
	public String getRegion() {
		int selectedIndex = regionListBox.getSelectedIndex();
		if(selectedIndex < 0)
			return null;
		return regionListBox.getValue(selectedIndex);
	}
	
	public int getRegionId() {
		return regionListBox.getSelectedIndex();
	}
	public ListBox getRegionListBox() {
		return regionListBox;
	}

	public void setJuridicalTypesItems(
			ArrayList<JuridicalType> juridicalTypesItems) {
		this.juridicalTypesItems = juridicalTypesItems;
	}
	public void setRegionsItems(
			ArrayList<Region> regionsItems) {
		this.regionsItems = regionsItems;
	}

	public CellTable<SStaffDivision> getGroupCellTable() {
		return groupCellTable;
	}
	
	public void setGroupCellTable(CellTable<SStaffDivision> groupCellTable) {
		this.groupCellTable = groupCellTable;
	}

	public void setRegionSelected(String region) {
		this.selectedRegion = region;
	}

	public void ititRegionSelected() {
		for (int i=0; i< getRegionListBox().getItemCount(); i++)
		 if (getRegionListBox().getItemText(i)==selectedRegion)
			 getRegionListBox().setItemSelected(i, true);
	}

	public void setdouTypeSelected(String douType) {
		this.selectedDouType = douType;		
	}

	public void ititdouTypeSelected() {
		for (int i=0; i< getDouTypeListBox().getItemCount(); i++)
			if (getDouTypeListBox().getItemText(i)==selectedDouType)
				getDouTypeListBox().setItemSelected(i, true);
	}

}
