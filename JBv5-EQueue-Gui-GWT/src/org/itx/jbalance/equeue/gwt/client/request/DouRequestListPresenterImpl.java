package org.itx.jbalance.equeue.gwt.client.request;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.register.RegisterEditPresenter;
import org.itx.jbalance.equeue.gwt.client.request.DouRequestListWidget.RecordColorWrapper;
import org.itx.jbalance.equeue.gwt.client.request.PrivilegeSearchItem.Type;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsSearchResultsWrapper;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsService;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsServiceAsync;
import org.itx.jbalance.equeue.gwt.shared.DouRequestDetailsDTO;
import org.itx.jbalance.equeue.gwt.shared.DouRequestListPrerequesedData;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.RegisterLine;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.IntegerFilter;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;


/**
 * Контроллер(пресентор) для формы просмотра списка заявок.
 * @author apv
 *
 */
public class DouRequestListPresenterImpl extends MyPresenter<DouRequestListWidget>
		implements DouRequestListPresenter {


	
	private final DouRequestsServiceAsync service = GWT.create(DouRequestsService.class);
//	private final ContragentsServiceAsync contragentsService= GWT.create(ContragentsService.class);
	
	final private State state;
	
	/**
	 * Коллекция выбранных(оммеченных) для протокола РН
	 * Используется в режиме формирование протокола
	 */
	private Set<Long>selectedNums=new HashSet<Long>();
	/**
	 * Коллекция выбранных(отмеченных) для протокола РН
	 * Используется в режиме формирование протокола
	 */
	private Set<Long>prewSelectedNums=new HashSet<Long>();
	
	
	DouRequestsSearchParams searchParams=new DouRequestsSearchParams();
	
	/**
	 * 
	 * @param display
	 * @param state - Режим работы формы
	 */
	public DouRequestListPresenterImpl(DouRequestListWidget display,State state) {
		super(display);
		this.state=state;
	}

	
	

	@Override
	public void delete(Long id) {
		
		if(state!=State.VIEW_REQUESTS)
			throw new RuntimeException();
		else{
			Log.debug("delete Request with id="+id);
			service.delete(id, new AsyncCallback<OperationResultWrapper<HDouRequest>>() {
				@Override
				public void onSuccess(OperationResultWrapper<HDouRequest> result) {
					HDouRequest request = result.getResult();
					switch (result.getStatus()) {
						case VALIDATION_ERROR:
						case SERVER_ERROR:
							getDisplay().showWarningNotification(result.getComments());
							break;

						case SUCCESS:
							getDisplay().showInfoNotification("Регистрационный номер " +request+" удален");
							search();
							break;
					}
				}
				@Override
				public void onFailure(Throwable caught) {
					handleException(caught);
				}
			});
		}
	}

	

	
	@Override
	public void initialize() {
		service.getDouRequestListPrerequesedData(new AsyncCallback<DouRequestListPrerequesedData>() {
			
			@Override
			public void onSuccess(DouRequestListPrerequesedData result) {
				getDisplay().setDouList(result.getDous());
				Collections.sort(result.getPrivileges(), new Comparator<Privilege>() {

					@Override
					public int compare(Privilege o1, Privilege o2) {
						return o1.getName().compareToIgnoreCase(o2.getName());
					}
				});

					
				getDisplay().setPrivileges(result.getPrivileges());
				fillSearchForm();
				initListeners();
				search();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}

	private void fillSearchForm() {
		getDisplay().getSearchField().setText(searchParams.pattern);
		if(searchParams.exact_privilege != null){
			getDisplay().getPrivilege().setValue(new PrivilegeSearchItem(Type.EXACT_PRIVELEGE, searchParams.exact_privilege, null));
		}else if(searchParams.privilege != null){
			if(searchParams.privilege){
				getDisplay().getPrivilege().setValue(PrivilegeSearchItem.YES_PRIVILEGE);
			} else {
				getDisplay().getPrivilege().setValue(PrivilegeSearchItem.NO_PRIVILEGE);
			}
		} else {
			getDisplay().getPrivilege().setValue(PrivilegeSearchItem.ANY);
		}
		if(searchParams.orderStatus!=null)
			getDisplay().getPermit().setValue(searchParams.orderStatus);
		if(searchParams.plainYear != null){
			getDisplay().getPlainYear().setValue(searchParams.plainYear.getFilterValue());
		}
		if(searchParams.searchedDous!=null)
			getDisplay().setSearchedDou(searchParams.searchedDous);

		if(searchParams.birthday != null){
			getDisplay().getBirthdayFromDateBox().setValue(searchParams.birthday.getFilterValues().get(0));
			getDisplay().getBirthdayToDateBox().setValue(searchParams.birthday.getFilterValues().get(1));
		}
		
		if(searchParams.regDate != null){
			getDisplay().getRegDateFrom().setValue(searchParams.regDate.getFilterValues().get(0));
			getDisplay().getRegDateTo().setValue(searchParams.regDate.getFilterValues().get(1));
		}
		
		if(searchParams.sverkDate != null){
			getDisplay().getSverkDateFrom().setValue(searchParams.sverkDate.getFilterValues().get(0));
			getDisplay().getSverkDateTo().setValue(searchParams.sverkDate.getFilterValues().get(1));
		}
		
//		TODO муть какая-то с цветами
//		RecordColorWrapper rcw = getDisplay().getRecordColor().getValue();
//		if(rcw == null){
//			searchParams.recordColor=null;
//			searchParams.withoutColor=null;
//		} else {
//			searchParams.withoutColor = rcw.withoutColor;
//			searchParams.recordColor  = rcw.recordColor;
//		}
		searchParams.phone=getDisplay().getPhone().getValue();
		getDisplay().getAgeFrom().setText(searchParams.ageFrom==null? null: searchParams.ageFrom +"");
		getDisplay().getAgeTo().setText(searchParams.ageTo==null? null: searchParams.ageTo +"");
	}

	private void initListeners() {}
	
	/**
	 * Подгрузка данных
	 */
	public void loadData(final int from,final  int to, List<SortedColumnInfo>sortInfo){
		searchParams.setFrom(from);
		searchParams.setTo(to);
		searchParams.setSortInfo(sortInfo);
		service.search(searchParams,new AsyncCallback< DouRequestsSearchResultsWrapper>() {
			@Override
			public void onSuccess( DouRequestsSearchResultsWrapper result) {
				getDisplay().updateRowData(from, result.getList());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}

	/**
	 * Запуск поиска
	 */
	public void search(){
		
		List <SortedColumnInfo>sortedColumns = null;
		if(State.SELECT_REQUESTS == getState()){
			sortedColumns= new ArrayList<SortedColumnInfo>();
			sortedColumns.add(new SortedColumnInfo("rn", true));
		}

		
		DouRequestsSearchParams params = getSearchParams();
		params.setFrom(0);
		params.setTo(PAGE_SIZE-1);
		params.setSortInfo(sortedColumns);
		service .search(params,/*0,PAGE_SIZE-1, sortedColumns ,*/new AsyncCallback<DouRequestsSearchResultsWrapper>() {
		
			final long startTime=System.currentTimeMillis();
			@Override
			public void onSuccess( DouRequestsSearchResultsWrapper result) {
				long searchEndedTime = System.currentTimeMillis();
				Log.debug("Search - "+(searchEndedTime-startTime)+" ms.");
				getDisplay().showDouRequests(result);
				Log.debug("Rendering - "+(System.currentTimeMillis()-searchEndedTime)+" ms.");
//				TODO coment this: 
//				for (HDouRequest r : result.getList()) {
//					Log.debug("get detail for " + r + "    UId: "+ r.getUId() + "    regDate: "+r.getRegDate() + "   DNumber: "+r.getDnumber());
//				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}
	
	@Override
	public void update(Privilege privilege) {}


	@Override
	public void populateDetails(DouRequestDTO selectedRequest) {
		if(selectedRequest == null)
			return;
		service.getDetails(selectedRequest.getUId(),new AsyncCallback<DouRequestDetailsDTO>() {
			
			@Override
			public void onSuccess(DouRequestDetailsDTO result) {
				getDisplay().populateDousList(result.getSpec());
				getDisplay().populateSRegTable(result.getMoves());
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}
	
	public State getState() {
		return state;
	}

	@Override
	public void select(Long uid){
		selectedNums.add(uid);
	}
	
	@Override
	public void deselect(Long uid){
		selectedNums.remove(uid);
	}
	
	@Override
	public boolean isSelected(Long uid){
		return selectedNums.contains(uid);
	}

	public Set<Long> getSelectedNums(){
		return selectedNums;
	}
	
	public Set<Long> getPrewSelectedNums(){
		return prewSelectedNums;
	}
	
	RegisterEditPresenter registerEditPresenter;
	public void setRegisterPresenter(RegisterEditPresenter registerEditPresenter){
		if(state!=State.SELECT_REQUESTS)
			throw new RuntimeException();
		this.registerEditPresenter=registerEditPresenter;
		
		
		for (RegisterLine registerLine : registerEditPresenter.getTableData()) {
			select(registerLine.getDouRequest().getUId());
			prewSelectedNums.add(registerLine.getDouRequest().getUId());
		}
	}
	
	public RegisterEditPresenter getRegisterPresenter( ){
		if(state!=State.SELECT_REQUESTS)
			throw new RuntimeException();
		return registerEditPresenter;
	}




	@Override
	public DouRequestsSearchParams getSearchParams() {
		searchParams.pattern= getDisplay().getSearchField().getText();
		 
		switch(getDisplay().getPrivilege().getValue().getType()){
			case ANY: searchParams.privilege = null; searchParams.exact_privilege = null; break;
			case NO_PRIVILEGE: searchParams.privilege = false; searchParams.exact_privilege = null;  break;
			case YES_PRIVILEGE: searchParams.privilege = true; searchParams.exact_privilege = null; break;
			case EXACT_PRIVELEGE: searchParams.privilege = null; searchParams.exact_privilege = getDisplay().getPrivilege().getValue().getExactPrivelageId();
		}
//		searchParams.privilege=getDisplay().getPrivilege().getValue();
		searchParams.orderStatus=getDisplay().getPermit().getValue();
		
		if(getDisplay().getPlainYear().getValue() != null){
			searchParams.plainYear = new IntegerFilter(getDisplay().getPlainYear().getValue());
		}
		searchParams.searchedDous=getDisplay().getSearchedDou();
		
		searchParams.birthday = DateFilter.build( 
				getDisplay().getBirthdayFromDateBox().getValue(),
				getDisplay().getBirthdayToDateBox().getValue());
		
		searchParams.regDate = DateFilter.build( 
				getDisplay().getRegDateFrom().getValue(),
				getDisplay().getRegDateTo().getValue());
		
		searchParams.sverkDate = DateFilter.build( 
				getDisplay().getSverkDateFrom().getValue(),
				getDisplay().getSverkDateTo().getValue());
		
		RecordColorWrapper rcw = getDisplay().getRecordColor().getValue();
		if(rcw == null){
			searchParams.recordColor=null;
			searchParams.withoutColor=null;
		} else {
			searchParams.withoutColor = rcw.withoutColor;
			searchParams.recordColor  = rcw.recordColor;
		}
		searchParams.phone=getDisplay().getPhone().getValue();
		try{
			searchParams.ageFrom=new Integer(getDisplay().getAgeFrom().getText());
		}catch(NumberFormatException e){searchParams.ageFrom=null;}
		
		try{
			searchParams.ageTo=new Integer(getDisplay().getAgeTo().getText());
		}catch(NumberFormatException e){searchParams.ageTo=null;}
		return searchParams;
	}




	public void setSearchParams(DouRequestsSearchParams sp) {
		searchParams = sp;
	}




	@Override
	public void updatePhone(Long uid, String phone) {
		service.updatePhoneNumber(uid, phone, new AsyncCallback<OperationResultWrapper<DouRequestDTO>>() {

			@Override
			public void onSuccess(OperationResultWrapper<DouRequestDTO> result) {
				if(result.getStatus() != OperationResultWrapper.Status.SUCCESS){
					getDisplay().showWarningNotification("Ошибка при обновлении телефонного номера\n"+result.getComments());
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}





}

