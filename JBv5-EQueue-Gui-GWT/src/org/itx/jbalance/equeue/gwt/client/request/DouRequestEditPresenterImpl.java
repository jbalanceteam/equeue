package org.itx.jbalance.equeue.gwt.client.request;


import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.equeue.gwt.client.Callback;
import org.itx.jbalance.equeue.gwt.client.EQueueNavigation;
import org.itx.jbalance.equeue.gwt.client.MyPresenter;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsService;
import org.itx.jbalance.equeue.gwt.client.services.DouRequestsServiceAsync;
import org.itx.jbalance.equeue.gwt.shared.AddDouRequestOperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.CreateNewDouRequestRequest;
import org.itx.jbalance.equeue.gwt.shared.DouRequestDetailsDTO;
import org.itx.jbalance.equeue.gwt.shared.DouRequestListPrerequesedData;
import org.itx.jbalance.equeue.gwt.shared.OperationResultWrapper;
import org.itx.jbalance.equeue.gwt.shared.UpdateDouRequestRequest;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HDouRequest.Status;
import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.filters.StringFilter;

import com.allen_sauer.gwt.log.client.Log;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.regexp.shared.MatchResult;
import com.google.gwt.regexp.shared.RegExp;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.DialogBox;

public class DouRequestEditPresenterImpl extends MyPresenter<DouRequestEditWidget>
		implements DouRequestEditPresenter {

	public enum State{
		NEW,EDIT
	}
	private State state=State.NEW;
	
	HDouRequest douRequest;
	Physical child;
	BirthSertificate birthSertificate;
	Physical declarant;
	private Map<Region, List<Dou>> dousByRegions;
	
	private final DouRequestsServiceAsync requestsService = GWT.create(DouRequestsService.class);
	
	private List<Privilege> privileges;
	
	public DouRequestEditPresenterImpl(DouRequestEditWidget display) {
		super(display);
	}

	@Override
	public void save() {
		if(state == State.NEW){
			douRequest = new HDouRequest();
			child = new Physical();
			birthSertificate = new BirthSertificate();
			declarant = new Physical();
		} else if(declarant == null) {
			declarant = new Physical();
		}
		
		child.setSurname(getDisplay().getLastName().getText());
		child.setName(getDisplay().getFirstName().getText());
		child.setRealName(getDisplay().getFirstName().getText());
		child.setPatronymic(getDisplay().getPatronymic().getText());
		child.setBirthday(getDisplay().getBirthDate().getValue());
		child.setPhone(getDisplay().getPhoneNumber().getText());
		child.setEMail(getDisplay().getEmail().getText());
		douRequest.setFoundation("foundation");
		
		Log.debug("Sex: "+getDisplay().getSex());
		Log.debug("BsDate: "+getDisplay().getBsDateBox().getValue());
		birthSertificate.setSex(getDisplay().getSex()==null?Sex.M:getDisplay().getSex());
		birthSertificate.setSeria(getDisplay().getBsSeriaBox().getText());
		birthSertificate.setNumber(getDisplay().getBsNumberBox().getText());
		birthSertificate.setDate(getDisplay().getBsDateBox().getValue());
		birthSertificate.setName("свидетельство о рождении");
//		#942
//		birthSertificate.setGuardian1(getDisplay().getGuardian1().getText());
//		birthSertificate.setGuardian2(getDisplay().getGuardian2().getText());
		
		if(state == State.NEW){
			douRequest.setRegDate(getDisplay().getRegistrationDate().getValue());
			douRequest.setStatus(Status.WAIT);
		}
		douRequest.setOldNumber(getDisplay().getOldNumber().getText());
		douRequest.setPrivilege(getDisplay().getPrivilege());
		
		douRequest.setYear(getDisplay().getYear().getValue());
		douRequest.setComments(getDisplay().getComments().getText());
		douRequest.setSverkDate(getDisplay().getSverkDate().getValue());
		douRequest.setRecordColor(getDisplay().getRecordColor().getValue());
		douRequest.setAddress(getDisplay().getAddress().getText());
		
		declarant.setSurname(getDisplay().getDeclarantLastname().getText());
		declarant.setName(getDisplay().getDeclarantFirstname().getText());
		declarant.setRealName(getDisplay().getDeclarantFirstname().getText());
		declarant.setPatronymic(getDisplay().getDeclarantPatronymic().getText());
//		declarant.setBirthday(getDisplay().getDecBirthDate().getValue());
		declarant.getPassport().setSeria(getDisplay().getDeclarantPassportSeria().getText());
		declarant.getPassport().setNumber(getDisplay().getDeclarantPassportNumber().getText());
		declarant.getPassport().setAuthority(getDisplay().getDeclarantPassportAuthority().getText());
		declarant.getPassport().setDateOfIssue(getDisplay().getDeclarantPassportIssueDate().getValue());
		
//		" без льгот "
		if(state == State.NEW){
			doCreateNew(false);
		}else{
			doUpdate();
		}
	}

	private void doUpdate() {
		requestsService.update(new UpdateDouRequestRequest(child, birthSertificate, douRequest, declarant, getDisplay().getSelectedDou()), new AsyncCallback< OperationResultWrapper<HDouRequest>>() {
		
		@Override
		public void onSuccess( OperationResultWrapper<HDouRequest> result) {
			HDouRequest request = result.getResult();
			switch (result.getStatus()) {
				case VALIDATION_ERROR:
				case SERVER_ERROR:
					getDisplay().showWarningNotification(result.getComments());
					break;

				case SUCCESS:
					getDisplay().showInfoNotification("Обновлено.\n Регистрационный номер "+request.getDnumber()+"\n UId="+request.getUId());
					douRequest =request;
					child = request.getChild();
					birthSertificate = request.getBirthSertificate();
					declarant = request.getDeclarant();
					break;
			}
		}
		
		@Override
		public void onFailure(Throwable caught) {
			handleException(caught);
		}
		});
	}

	/**
	 * 
	 * @param force - есди ИСТИНА, игнорировать дубликаты... Добавить в любом случае. Если ЛОЖЬ - не добвилять при наличии дубливатов
	 */
	private void doCreateNew(boolean force) {
		final Map<Dou, Integer> selectedDou = getDisplay().getSelectedDou();
		CreateNewDouRequestRequest request = new CreateNewDouRequestRequest(child, birthSertificate, douRequest, declarant, selectedDou, force);
		requestsService.createNew(request,  new AsyncCallback< AddDouRequestOperationResultWrapper>() {
			
			@Override
			public void onSuccess( AddDouRequestOperationResultWrapper result) {
				HDouRequest request = result.getResult();
				switch (result.getStatus()) {
					case VALIDATION_ERROR:
						if(result.getMatchFio() > 0 ){
							/* Если найдены совпадения, показываем предупреждение с вариантами действий:
							 "Все равно добавить" , "Просмотреть совпадения",  "Вернуться к редактированию"
							  */
							
							
							/* Обработчик для кнопки "Все равно добавить" */
							ClickHandler ignore = new ClickHandler() {
						          public void onClick(ClickEvent event) {
						        	  doCreateNew(true);
						          }
						        };
						        
					        /* Обработчик для кнопки "Вернуться к редактированию" */
					        ClickHandler backToEdit = new ClickHandler() {
						          public void onClick(ClickEvent event) {
						        	  
						          }
						        };
							
						    /* Обработчик для кнопки "Просмотреть совпадения" */
					        ClickHandler showMatches = new ClickHandler() {
						          public void onClick(ClickEvent event) {
						        	  DouRequestsSearchParams searchParams=new DouRequestsSearchParams();
						        	  searchParams.firstname  = new StringFilter(child.getRealName());
						        	  searchParams.lastname   = new StringFilter(child.getSurname());
						        	  searchParams.middlename = new StringFilter(child.getPatronymic());
						        	  Map<String,Serializable> hashMap = new HashMap<String,Serializable>();
						        	  hashMap.put("searchParams", searchParams);
									EQueueNavigation.instance().goToDouRequestsListPage(hashMap);
							  		  
						          }
						        };
							
						    /* Показываем модальную панель с предупреждением и кнопками*/
							final DialogBox dialogBox = getDisplay(). createDialogBox(result.getMatchFio(), 
									result.getMatchFioAndBirthday(),
									ignore, backToEdit, showMatches);
						    dialogBox.setGlassEnabled(true);
						    dialogBox.setAnimationEnabled(true);

						    dialogBox.center();
						    dialogBox.show();
						    break;
							
						}
					case SERVER_ERROR:
						getDisplay().showWarningNotification(result.getComments());
						break;
	
					case SUCCESS:
						getDisplay().showInfoNotification("Сохранено.\n Регистрационный номер "+request.toString()+"\n UId="+request.getUId());
						clearForm();
						break;
				}
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}
	
	private void clearForm(){
		getDisplay().getOldNumber().setText("");
		getDisplay().getRegistrationDate().setValue(new Date());
		getDisplay().getRegistrationDateLabel().setHTML("");
		
		getDisplay().getLastName().setText("");
		getDisplay().getFirstName().setText("");
		getDisplay().getFirstName().setText("");
		getDisplay().getPatronymic().setText("");
		getDisplay().getBirthDate().setValue(new Date());
		getDisplay().getPhoneNumber().setText("");
		getDisplay().getEmail().setText("");
		
		getDisplay().getBsSeriaBox().setText("");
		getDisplay().getBsNumberBox().setText("");
		getDisplay().getBsDateBox().setValue(new Date());
		
//		#942
//		getDisplay().getGuardian1().setText("");
//		getDisplay().getGuardian2().setText("");
		
		
		getDisplay().setPrivilege(null);
		getDisplay().getComments().setText("");
		getDisplay().getYear().setValue(null);
		getDisplay().getSverkDate().setValue(null);
		getDisplay().getRecordColor().setValue(null);
		getDisplay().getAddress().setText("");
		getDisplay().setSex(Sex.M);
		getDisplay().removeAllDou();
		
		getDisplay().getDeclarantFirstname().setText("");
		getDisplay().getDeclarantLastname().setText("");
		getDisplay().getDeclarantPatronymic().setText("");
		getDisplay().getDeclarantPassportSeria().setText("");
		getDisplay().getDeclarantPassportNumber().setText("");
		getDisplay().getDeclarantPassportIssueDate().setValue(new Date());
		getDisplay().getDeclarantPassportAuthority().setText("");
	}

	@Override
	public void initialize() {
		initialize(null);
	}
	
	@Override
	public void initialize(final Callback callback) {
		Log.debug("getting dous list...");
		requestsService.getDouRequestListPrerequesedData(new AsyncCallback<DouRequestListPrerequesedData>() {
			
			@Override
			public void onSuccess(DouRequestListPrerequesedData result) {
				dousByRegions = result.getDous();
				getDisplay().setAvailableDou(result.getDous());
				
				privileges=result.getPrivileges();
				getDisplay().setPrivileges(result.getPrivileges());
				Log.debug("       result = "+result);
				
				callback.callback();
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}


	@Override
	public List<Privilege> getAllPrivileges() {
		return privileges;
	}

	@Override
	public void setObjectUId(Long uid) {
		
		requestsService.getDetails(uid, new AsyncCallback<DouRequestDetailsDTO>() {
			@Override
			public void onSuccess(DouRequestDetailsDTO result) {
				state = State.EDIT;
				douRequest = result.getDoc();
				child = douRequest.getChild();
				birthSertificate = douRequest.getBirthSertificate();
				declarant = douRequest.getDeclarant();
				
				getDisplay().getOldNumber().setText(douRequest.getOldNumber());
				getDisplay().getRegistrationDate().setValue(douRequest.getRegDate());
				getDisplay().getRegistrationDateLabel().setHTML(DateTimeFormat.getFormat("dd.MM.yyyy").format(douRequest.getRegDate()));
				
				getDisplay().getLastName().setText(child.getSurname());
				getDisplay().getFirstName().setText(child.getName());
				getDisplay().getFirstName().setText(child.getRealName());
				getDisplay().getPatronymic().setText(child.getPatronymic());
				getDisplay().getBirthDate().setValue(child.getBirthday());
				getDisplay().getPhoneNumber().setText(child.getPhone());
				getDisplay().getEmail().setText(child.getEMail());
				
				getDisplay().getBsSeriaBox().setText(birthSertificate.getSeria());
				getDisplay().getBsNumberBox().setText(birthSertificate.getNumber());
				getDisplay().getBsDateBox().setValue(birthSertificate.getDate());
				getDisplay().setSex(birthSertificate.getSex());
				
//				#942
//				getDisplay().getGuardian1().setText(birthSertificate.getGuardian1());
//				getDisplay().getGuardian2().setText(birthSertificate.getGuardian2());
				
				
				getDisplay().setPrivilege(douRequest.getPrivilege());
				getDisplay().getComments().setText(douRequest.getComments());
				getDisplay().getYear().setValue(douRequest.getYear());
				getDisplay().getSverkDate().setValue(douRequest.getSverkDate());
				getDisplay().getRecordColor().setValue(douRequest.getRecordColor());
				getDisplay().getAddress().setText(douRequest.getAddress());
				
				if(douRequest.getDeclarant() != null){
					getDisplay().getDeclarantFirstname().setText(declarant.getRealName());
					getDisplay().getDeclarantLastname().setText(declarant.getSurname());
					getDisplay().getDeclarantPatronymic().setText(declarant.getPatronymic());
					
					getDisplay().getDeclarantPassportSeria().setText(declarant.getPassport().getSeria());
					getDisplay().getDeclarantPassportNumber().setText(declarant.getPassport().getNumber());
					getDisplay().getDeclarantPassportIssueDate().setValue(declarant.getPassport().getDateOfIssue());
					getDisplay().getDeclarantPassportAuthority().setText(declarant.getPassport().getAuthority());
				}
				
				Log.debug(
						"tz.getOffset(child.getBirthday().getTime()) "+ DateTimeFormat.getLongDateTimeFormat().format(child.getBirthday()) + " "
				);
				
				Log.debug("setObjectUId... child.getBirthday():" + child.getBirthday() + " "+ child.getBirthday().getTimezoneOffset());
				
				getDisplay().getFormTitle().setText("Редактирование РН "+douRequest.toString());
				
				for (SDouRequest sDouRequest : result.getSpec()) {
					getDisplay().chooseOneDou(sDouRequest.getArticleUnit(), sDouRequest.getRating());
				}
				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				handleException(caught);
			}
		});
	}


	private String getVal(String xml, String tag){
		final RegExp pattern = RegExp.compile("<"+tag+">([\\S\\s]*)<\\/"+tag+">", "i");
		MatchResult matcher = pattern.exec(xml);
		final String res;
		if(matcher != null){
			res = matcher.getGroup(1);
		} else {
			res = null;
		}
		Log.debug(tag + ": "+res);
		return res;
	}
	
	DateTimeFormat ISO_DATE_FORMAT =DateTimeFormat.getFormat("yyyy-MM-dd");
	private Date getValDate(String xml, String tag){
		try {
			String val = getVal(xml, tag);
			return val==null? null: ISO_DATE_FORMAT.parse(val);
		} catch (Exception e) {
			handleException(e);
			return null;
		}
	}
	
	private Integer getValInt(String xml, String tag){
		try {
			String val = getVal(xml, tag);
			return val==null? null: new Integer(val);
		} catch (Exception e) {
			handleException(e);
			return null;
		}
	}
	
	@Override
	public void populateFormUsingXml(String xml) {
		Log.debug("xml: " + xml);
		 clearForm();
//		getDisplay().getOldNumber().setText();
//		getDisplay().getRegistrationDate().setValue();
//		getDisplay().getRegistrationDateLabel().setHTML(DateTimeFormat.getFormat("dd.MM.yyyy").format(douRequest.getRegDate()));
		
		getDisplay().getLastName().setText(getVal(xml, "last_name"));
		getDisplay().getFirstName().setText(getVal(xml, "first_name"));
		getDisplay().getPatronymic().setText(getVal(xml, "patronimic"));
		getDisplay().getBirthDate().setValue(getValDate(xml, "birthday"));
		getDisplay().getPhoneNumber().setText(getVal(xml, "applicant_phones"));
		getDisplay().getEmail().setText(getVal(xml, "applicant_email"));
		
		getDisplay().getBsSeriaBox().setText(getVal(xml, "bs_seria"));
		getDisplay().getBsNumberBox().setText(getVal(xml, "bs_number"));
		getDisplay().getBsDateBox().setValue(getValDate(xml, "bs_date"));
		
		String sex = getVal(xml, "sex");
		if("M".equals(sex)){
			getDisplay().setSex(Sex.M);
		} else if("W".equals(sex)){
			getDisplay().setSex(Sex.W);
		}
		

		
		String region = getVal(xml, "region");
		String applicant_fio = getVal(xml, "applicant_fio");
		String comment = getVal(xml, "comments");
		if(region!= null && !region.isEmpty()){
			comment += ("\nРайон: "+region);
		}
		
		if(applicant_fio!= null && !applicant_fio.isEmpty()){
			comment += ("\nЗаявитель: "+applicant_fio);
		}
		
			
		getDisplay().getComments().setText(comment);
		
		getDisplay().getYear().setValue(getValInt(xml, "planYear"));
//		getDisplay().getSverkDate().setValue();
//		getDisplay().getRecordColor().setValue();
		getDisplay().getAddress().setText(getVal(xml, "address"));
		
		// Заявитель
		getDisplay().getDeclarantFirstname().setText(getVal(xml, "declarantFirstname"));
		getDisplay().getDeclarantLastname().setText(getVal(xml, "declarantLastname"));
		getDisplay().getDeclarantPatronymic().setText(getVal(xml, "declarantPatronymic"));
		getDisplay().getDeclarantPassportSeria().setText(getVal(xml, "declarantPassportSeria"));
		getDisplay().getDeclarantPassportNumber().setText(getVal(xml, "declarantPassportNumber"));
		getDisplay().getDeclarantPassportIssueDate().setValue(getValDate(xml, "declarantPassportIssueDate"));
		getDisplay().getDeclarantPassportAuthority().setText(getVal(xml, "declarantPassportAuthority"));
		
		getDisplay().getFormTitle().setText("Импорт РН ");
		
		
		
		
		if(dousByRegions!=null){
			Collection<List<Dou>> values = dousByRegions.values();
			for (List<Dou> list : values) {
				for (Dou dou : list) {
					String val = getVal(xml, "dou_"+dou.getNumber()+"_raiting");
					if(val!=null && !val.isEmpty()){
//						TODO тут надо что-то мутить с рейтингом !!!!
						getDisplay().chooseOneDou(dou, new Integer(val));
					}
				}
			}
		}
	}

}