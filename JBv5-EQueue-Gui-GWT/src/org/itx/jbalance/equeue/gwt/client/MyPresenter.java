package org.itx.jbalance.equeue.gwt.client;


public abstract class MyPresenter<D extends MyDisplay> {

	protected D display;

	public MyPresenter(final D display) {
		this.display = display;
	}
	
	public D getDisplay(){
		return display;
	};
	
	/**
	 * Пишет в лог стек-трейс
	 * Инфоримрует пользователя об ошибке.
	 * @param caught информация об исключении
	 */
	public void handleException(Throwable caught){
		display.showExceptionNotification(caught);
	}
	
	public abstract void initialize();
	
}
