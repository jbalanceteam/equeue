package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.Juridical;

public class ContractorsListDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private List<HStaffDivision> staffDivisions;
	private List<Juridical> juridicals;
	private Map<String,Integer> kg;
	private Map<String,Integer> km;
	
	public List<HStaffDivision> getStaffDivisions() {
		return staffDivisions;
	}
	public void setStaffDivisions(List<HStaffDivision> staffDivisions) {
		this.staffDivisions = staffDivisions;
	}
	public Map<String, Integer> getKg() {
		return kg;
	}
	public void setKg(Map<String, Integer> kg) {
		this.kg = kg;
	}
	public Map<String, Integer> getKm() {
		return km;
	}
	public void setKm(Map<String, Integer> km) {
		this.km = km;
	}
	public List<Juridical> getJuridicals() {
		return juridicals;
	}
	public void setJuridicals(List<Juridical> juridicals) {
		this.juridicals = juridicals;
	}
}
