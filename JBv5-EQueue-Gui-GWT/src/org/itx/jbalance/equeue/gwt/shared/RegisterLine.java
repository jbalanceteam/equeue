package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
/**
 * Класс для представления информации одной строки протокола
 * @author apv
 *
 */
public class RegisterLine implements Serializable{

	private static final long serialVersionUID = 1L;

	private HDouRequest douRequest;
	private List<SDouRequest>sDouRequests;
	private SRegister sRegister;
	/**
	 * #237: Очередник не может быть в двух предв. протоколах одновременно
	 * 
	 * Список протоколов(на самом деле строк спецификации) на этапе формирования < 2, 
	 * где присутствует этот же ребенок. 
	 */
	private List<SRegister> whereElseChildIs = new ArrayList<SRegister>();
	
	/**
	 * iss31: Когда выбрали детей из списка (отметили птицами) и нажали к "формированию протокола", 
	 * записи, которые попали в протокол подсвечивать цветом до следующей итерации. 
	 */
	private boolean newFlag = false;
	
	public HDouRequest getDouRequest() {
		return douRequest;
	}
	public void setDouRequest(HDouRequest douRequest) {
		this.douRequest = douRequest;
	}
	public List<SDouRequest> getSDouRequests() {
		return sDouRequests;
	}
	public void setSDouRequests(List<SDouRequest> sDouRequests) {
		this.sDouRequests = sDouRequests;
	}
	public SRegister getSRegister() {
		return sRegister;
	}
	public void setSRegister(SRegister sRegister) {
		this.sRegister = sRegister;
	}
	public boolean isNewFlag() {
		return newFlag;
	}
	public void setNewFlag(boolean newFlag) {
		this.newFlag = newFlag;
	}
	
	
	public String getDousByRating(Integer rating) {
		StringBuilder stringBuilder = new StringBuilder();
		if(sDouRequests != null && !sDouRequests.isEmpty()){
			for (SDouRequest sDouRequest: sDouRequests) {
				if(rating != null && rating != sDouRequest.getRating()){
					continue;
				}
				stringBuilder.append(sDouRequest.getArticleUnit().getNumber());
				stringBuilder.append(", ");
			}
			if(stringBuilder.length() > 2){
				stringBuilder.deleteCharAt(stringBuilder.length()-2);
			}
		}
		
		return stringBuilder.toString();
	}
	
	
	public List<SRegister> getWhereElseChildIs() {
		return whereElseChildIs;
	}
	public void setWhereElseChildIs(List<SRegister> whereElseChildIs) {
		this.whereElseChildIs = whereElseChildIs;
	}
}
