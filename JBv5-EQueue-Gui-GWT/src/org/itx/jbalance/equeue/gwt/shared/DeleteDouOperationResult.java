package org.itx.jbalance.equeue.gwt.shared;

import org.itx.jbalance.l0.o.Contractor;

public class DeleteDouOperationResult extends OperationResultWrapper<Contractor>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer associadedRequestsAmount;

	public Integer getAssociadedRequestsAmount() {
		return associadedRequestsAmount;
	}

	public void setAssociadedRequestsAmount(Integer associadedRequestsAmount) {
		this.associadedRequestsAmount = associadedRequestsAmount;
	}
	
	

}
