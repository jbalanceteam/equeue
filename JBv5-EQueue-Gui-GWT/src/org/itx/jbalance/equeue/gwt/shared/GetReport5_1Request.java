package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author apv
 *
 */
public class GetReport5_1Request implements Serializable {
	

	private static final long serialVersionUID = 1L;
	private Date effectiveDate;
	private List<Report5_1DTO> ages = new ArrayList<Report5_1DTO>();
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	public List<Report5_1DTO> getAges() {
		return ages;
	}
}
