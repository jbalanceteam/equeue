package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
/**
 * ДТО описывает одну строчку отчета "Распределение по возрасту"
 * @author apv
 *
 */
public class Report5_1DTO implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * Возраст в месяцах
	 * С
	 */
	private Integer from;
	/**
	 * Возраст в месяцах
	 * ПО
	 */
	private Integer to;
	/**
	 * Кол-во детей в данном возрастном интервале
	 */
	private Integer count;
	/**
	 * #123 Дополнить колонкой "Нехватило мест" Вычислить сколько детей с отказами по возрасту.
	 */
	private Integer rejectedCount;
	/**
	 * Issue #370
	 * Добавить в отчтет №1 Потребность в местах
	 */
	private Integer howMuchNeed;

	public Report5_1DTO() {}
	
	public Report5_1DTO(Integer from, Integer to) {
		super();
		this.from = from;
		this.to = to;
	}
	
	public Integer getFrom() {
		return from;
	}
	public void setFrom(Integer from) {
		this.from = from;
	}
	public Integer getTo() {
		return to;
	}
	public void setTo(Integer to) {
		this.to = to;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getRejectedCount() {
		return rejectedCount;
	}

	public void setRejectedCount(Integer rejectedCount) {
		this.rejectedCount = rejectedCount;
	}

	/**
	 * Потребность в местах
	 * @return
	 */
	public Integer getHowMuchNeed() {
		return howMuchNeed;
	}

	/**
	 * Потребность в местах
	 * @param howMuchNeed
	 */
	public void setHowMuchNeed(Integer howMuchNeed) {
		this.howMuchNeed = howMuchNeed;
	}


	
	
	
}
