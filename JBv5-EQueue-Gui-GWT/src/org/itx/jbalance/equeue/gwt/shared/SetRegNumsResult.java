package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l0.o.Dou;

public class SetRegNumsResult implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Dou> allDou;
	private List<DOUGroups> ageGroups;
	List<RegisterLine>registerLines = new ArrayList<RegisterLine>();
	public List<Dou> getAllDou() {
		return allDou;
	}
	public void setAllDou(List<Dou> allDou) {
		this.allDou = allDou;
	}
	public List<RegisterLine> getRegisterLines() {
		return registerLines;
	}
	public void setRegisterLines(List<RegisterLine> registerLines) {
		this.registerLines = registerLines;
	}
	public List<DOUGroups> getAgeGroups() {
		return ageGroups;
	}
	public void setAgeGroups(List<DOUGroups> ageGroups) {
		this.ageGroups = ageGroups;
	}
	
	
}
