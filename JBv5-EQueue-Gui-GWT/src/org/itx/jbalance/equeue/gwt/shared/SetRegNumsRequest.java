package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.Set;

import org.itx.jbalance.l0.h.HRegister;

public class SetRegNumsRequest implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * HDou Requsts uids
	 */
	Set<Long> Ids;
	HRegister register;

	public Set<Long> getIds() {
		return Ids;
	}

	public void setIds(Set<Long> ids) {
		Ids = ids;
	}

	public HRegister getRegister() {
		return register;
	}

	public void setRegister(HRegister register) {
		this.register = register;
	}
	
	
	
	
}
