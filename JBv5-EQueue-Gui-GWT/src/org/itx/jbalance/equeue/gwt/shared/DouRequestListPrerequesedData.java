package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.Region;

/**
 * Это ДТО для того, чтобы отрисовать DouRequestList форму
 * @author apv
 *
 */
public class DouRequestListPrerequesedData  implements Serializable{
	private static final long serialVersionUID = 1L;
	/**
	 * Список садиков, сгруппированный по регионам
	 */
	private Map <Region,List<Dou>> dous ;
	/**
	 * Список льгот
	 */
	private List<Privilege> privileges = new ArrayList<Privilege>();
	public Map<Region, List<Dou>> getDous() {
		return dous;
	}
	public void setDous(Map<Region, List<Dou>> dous) {
		this.dous = dous;
	}
	public List<Privilege> getPrivileges() {
		return privileges;
	}
}
