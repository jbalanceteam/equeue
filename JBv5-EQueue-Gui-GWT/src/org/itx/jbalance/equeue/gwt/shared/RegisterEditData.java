package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l2_api.dto.equeue.RegisterDTO;
/**
 * DTO содержащий данные для формы редактирования протокола
 * 
 * @author apv
 */
public class RegisterEditData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Собственно протокол
	 */
	private HRegister register;
	/**
	 * Список всех ДОУ
	 */
	private List<Dou> allDou;
	/**
	 * Строки протокола.
	 * Каждая строка - движение РН (изменения его статуса). НАпример "выдать петевку", "Убрать из очереди"
	 */
	private List<RegisterLine> registerLines = new ArrayList<RegisterLine>();
	/**
	 * Помимо HRegister зачем-то передаем DTO
	 */
	private RegisterDTO registerDTO;
	/**
	 * Список возростных групп
	 */
	private List<DOUGroups>ageGroups;
	
	public HRegister getRegister() {
		return register;
	}
	public void setRegister(HRegister register) {
		this.register = register;
	}
	public List<Dou> getAllDou() {
		return allDou;
	}
	public void setAllDou(List<Dou> allDou) {
		this.allDou = allDou;
	}
	public List<RegisterLine> getRegisterLines() {
		return registerLines;
	}
	public void setRegisterLines(List<RegisterLine> registerLines) {
		this.registerLines = registerLines;
	}
	public RegisterDTO getRegisterDTO() {
		return registerDTO;
	}
	public void setRegisterDTO(RegisterDTO registerDTO) {
		this.registerDTO = registerDTO;
	}
	public List<DOUGroups> getAgeGroups() {
		return ageGroups;
	}
	public void setAgeGroups(List<DOUGroups> ageGroups) {
		this.ageGroups = ageGroups;
	}
	
	
	
}
