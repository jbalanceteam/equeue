package org.itx.jbalance.equeue.gwt.shared;

import org.itx.jbalance.l0.h.HDouRequest;

public class AddDouRequestOperationResultWrapper extends OperationResultWrapper<HDouRequest> {

	private static final long serialVersionUID = 7355509135831439833L;
	
	
	
	public AddDouRequestOperationResultWrapper() {
		super();
		// TODO Auto-generated constructor stub
	}
	private int matchFio;
	private int matchFioAndBirthday;
	public int getMatchFio() {
		return matchFio;
	}
	public void setMatchFio(int matchFio) {
		this.matchFio = matchFio;
	}
	public int getMatchFioAndBirthday() {
		return matchFioAndBirthday;
	}
	public void setMatchFioAndBirthday(int matchFioAndBirthday) {
		this.matchFioAndBirthday = matchFioAndBirthday;
	}
	
	
	
	
}
