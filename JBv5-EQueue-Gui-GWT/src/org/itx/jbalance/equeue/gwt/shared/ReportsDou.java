package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.itx.jbalance.l0.o.Dou;

public class ReportsDou implements Serializable{

	private static final long serialVersionUID = 1L;
private Dou dou;
 private Map<ReportsAge,Integer>count=new HashMap<ReportsAge, Integer>();
public Dou getDou() {
	return dou;
}
public void setDou(Dou dou) {
	this.dou = dou;
}
public Map<ReportsAge, Integer> getCount() {
	return count;
}
public void setCount(Map<ReportsAge, Integer> count) {
	this.count = count;
}
 
 
}
