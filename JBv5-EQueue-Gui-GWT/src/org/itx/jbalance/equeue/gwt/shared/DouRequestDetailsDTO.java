package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.List;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;

public class DouRequestDetailsDTO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HDouRequest doc;
	private List<SDouRequest>spec;
	private List<SRegister>moves;
	public List<SDouRequest> getSpec() {
		return spec;
	}
	public void setSpec(List<SDouRequest> spec) {
		this.spec = spec;
	}
	public List<SRegister> getMoves() {
		return moves;
	}
	public void setMoves(List<SRegister> moves) {
		this.moves = moves;
	}
	public HDouRequest getDoc() {
		return doc;
	}
	public void setDoc(HDouRequest doc) {
		this.doc = doc;
	}
	

}
