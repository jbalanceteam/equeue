package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.l0.o.Privilege;

public class Report5_2DTO implements Serializable{

	private static final long serialVersionUID = 1L;

	private ArrayList<Privilege>privileges;
	
	public Report5_2DTO() {
		super();
		
	}
	
	public ArrayList<ReportAgeWithCount> ages=new ArrayList<ReportAgeWithCount>();
	
	
	
	
	
	
	
	
	
	

	public ArrayList<Privilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(ArrayList<Privilege> privileges) {
		this.privileges = privileges;
	}

	public List<ReportAgeWithCount> getAges() {
		return ages;
	}

	public void setAges(ArrayList<ReportAgeWithCount> ages) {
		this.ages = ages;
	}
	
	
	
}
