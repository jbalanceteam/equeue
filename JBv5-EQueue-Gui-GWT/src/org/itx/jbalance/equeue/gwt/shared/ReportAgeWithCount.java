package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ReportAgeWithCount extends ReportsAge implements Serializable{
	
	
	public ReportAgeWithCount(Integer from, Integer to) {
		super(from, to);
		// TODO Auto-generated constructor stub
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * ID льготы - количество
	 */
	private HashMap<Long,Integer> count=new HashMap<Long, Integer>();
	
	public ReportAgeWithCount(){}
	
	
	public Map<Long,Integer> getCount() {
		return count;
	}
	
	
}