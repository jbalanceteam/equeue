package org.itx.jbalance.equeue.gwt.shared;

import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.l0.h.HRegister;

public class SaveRegisterResult extends OperationResultWrapper<HRegister>{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	List<RegisterLine>registerLines = new ArrayList<RegisterLine>();

	public List<RegisterLine> getRegisterLines() {
		return registerLines;
	}
	public void setRegisterLines(List<RegisterLine> registerLines) {
		this.registerLines = registerLines;
	}
	
	
}
