package org.itx.jbalance.equeue.gwt.shared;

import java.io.Serializable;
import java.util.Map;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Physical;

public class CreateNewDouRequestRequest implements Serializable{

	private static final long serialVersionUID = 1L;

	private Physical child;
	private BirthSertificate birthSertificate;
	private HDouRequest p; 
	private Physical declarant;
	private Map<Dou, Integer> raiting;
	/**
	 * Игнорировать проверку на дубликаты по ФИО и дате рождения
	 */
	private boolean force;
	
	
	
	public CreateNewDouRequestRequest() {
		super();
	}
	/**
	 * 
	 * @param child - Данные о ребенке
	 * @param birthSertificate - Свидетельство о рождении
	 * @param p - заявка на постановку  в очередь
	 * @param raiting - список МДОУ с рейтингом желемости
	 * @param force - не проверять на дубликаты
	 */
	public CreateNewDouRequestRequest(Physical child,
			BirthSertificate birthSertificate, HDouRequest p,
			Physical declarant,
			Map<Dou, Integer> raiting,
			boolean force) {
		super();
		this.child = child;
		this.birthSertificate = birthSertificate;
		this.p = p;
		this.declarant = declarant;
		this.raiting = raiting;
		this.force = force;
	}
	public Physical getChild() {
		return child;
	}
	public void setChild(Physical child) {
		this.child = child;
	}
	public BirthSertificate getBirthSertificate() {
		return birthSertificate;
	}
	public void setBirthSertificate(BirthSertificate birthSertificate) {
		this.birthSertificate = birthSertificate;
	}
	public HDouRequest getP() {
		return p;
	}
	public void setP(HDouRequest p) {
		this.p = p;
	}
	public Map<Dou, Integer> getRaiting() {
		return raiting;
	}
	public void setRaiting(Map<Dou, Integer> raiting) {
		this.raiting = raiting;
	}
	public boolean isForce() {
		return force;
	}
	public void setForce(boolean force) {
		this.force = force;
	}
	public Physical getDeclarant() {
		return declarant;
	}
	public void setDeclarant(Physical declarant) {
		this.declarant = declarant;
	}
}
