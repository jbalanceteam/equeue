package org.itx.jbalance.equeue.queueImport;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.itx.jbalance.l0.h.RecordColor;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;

/**
 * Данные из одной строки Excel-файла
 * @author apv
 *
 */
public class Row implements Comparable<Row>{
	private Integer number;
	private List<String> dous;
	private String firstName;
	private String secondName;
	private String patronymicName;
	private Date birthday;
	private String address;
	private Date regDate;
	private String privelegy;
	private String comments;
	private String bsNumber;
	private String bsSeria;
	private Date bsDate;
	private Sex sex;
	private Date sverkDate;
	private String bsGuardian1;
	private String oldRN;
	private String phone;
	private RecordColor color;
	private Integer plainYear;
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	public List<String> getDous() {
		if(dous == null){
			dous = new ArrayList<String>();
		}
		return dous;
	}
	public void setDous(List<String> dous) {
		this.dous = dous;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Date getRegDate() {
		return regDate;
	}
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	public String getPrivelegy() {
		return privelegy;
	}
//	public void setPrivelegy(String privelegy) {
//		this.privelegy = privelegy;
//	}
	
	
	
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getPatronymicName() {
		return patronymicName;
	}
	public void setPatronymicName(String patronymicName) {
		this.patronymicName = patronymicName;
	}
	
	
	
	
	@Override
	public String toString() {
		return "Row [number=" + number + ", dous=" + dous + ", firstName="
				+ firstName + ", secondName=" + secondName
				+ ", patronymicName=" + patronymicName + ", birthday="
				+ birthday + ", address=" + address + ", regDate=" + regDate
				+ ", privelegy=" + privelegy + ", comments=" + comments
				+ ", bsNumber=" + bsNumber + ", bsSeria=" + bsSeria
				+ ", bsDate=" + bsDate + ", color=" + color + "]";
	}
	public Row(Integer number, List<String> dous, 
			String secondName,
			String firstName, 
			String patronymicName,
			Date birthday, String address, Date regDate,
			String privelegy) {
		super();
		this.number = number;
		this.dous = dous;
		this.firstName = firstName;
		this.secondName = secondName;
		this.patronymicName = patronymicName;
		this.birthday = birthday;
		this.address = address;
		this.regDate = regDate;
		this.privelegy = privelegy;
	}
	@Override
	public int compareTo(Row o) {
		return this.getNumber().compareTo(o.getNumber());
	}
	public RecordColor getColor() {
		return color;
	}
	public void setColor(RecordColor color) {
		this.color = color;
	}
	public String getBsNumber() {
		return bsNumber;
	}
	public void setBsNumber(String bsNumber) {
		this.bsNumber = bsNumber;
	}
	public String getBsSeria() {
		return bsSeria;
	}
	public void setBsSeria(String bsSeria) {
		this.bsSeria = bsSeria;
	}
	public Date getBsDate() {
		return bsDate;
	}
	public void setBsDate(Date bsDate) {
		this.bsDate = bsDate;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getBsGuardian1() {
		return bsGuardian1;
	}
	public void setBsGuardian1(String bsGuardian1) {
		this.bsGuardian1 = bsGuardian1;
	}
	public void setOldRn(String oldRN) {
		this.oldRN = oldRN;
		
	}
	public String getOldRN() {
		return oldRN;
	}
	public Date getSverkDate() {
		return sverkDate;
	}
	public void setSverkDate(Date sverkDate) {
		this.sverkDate = sverkDate;
	}
	public Sex getSex() {
		return sex;
	}
	public void setSex(Sex sex) {
		this.sex = sex;
	}
	public Integer getPlainYear() {
		return plainYear;
	}
	public void setPlainYear(Integer plainYear) {
		this.plainYear = plainYear;
	}
	
	
}
