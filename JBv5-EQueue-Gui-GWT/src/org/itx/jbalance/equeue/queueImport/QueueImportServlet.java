package org.itx.jbalance.equeue.queueImport;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.biff.EmptyCell;
import jxl.read.biff.BlankCell;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;


public class QueueImportServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public static final int BUFSIZE=256;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/plain");
        response.setHeader("Content-Type", "text/plain; charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
	    try {
	    	System.out.println("doPost()");
	        List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
	        String key = null;
	        for (FileItem item : items) {
	            if (item.isFormField()) {
	                // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
	                String fieldname = item.getFieldName();
	                String fieldvalue = item.getString();

	                System.out.println("fieldname: "+fieldname + " fieldvalue : "+fieldvalue );
	                
	                if("key".equals(fieldname)){
	                	key = fieldvalue;
	                }
	                
	            } else {
	                // Process form file field (input type="file").
	                String fieldname = item.getFieldName();
	                String filename = FilenameUtils.getName(item.getName());
	                InputStream filecontent = item.getInputStream();
	                final JBSession jbSession = new JBSession();
					try {
						List<Row> rows = getData(filecontent);
						jbSession.upLoad(rows);
		                PrintWriter writer = response.getWriter();
		    			final String log = jbSession.getLog();
		    			byte ptext[] = log.getBytes();
		    			String logUtf8 = new String(ptext, "UTF-8");
		    			
		    			writer.println(logUtf8);
						
		    			writer.flush();
		    			writer.close();
					} catch (Exception e) {
						
						e.printStackTrace();
						
						PrintWriter writer = response.getWriter();
		    			writer.println(e.getMessage());
		    			writer.flush();
		    			writer.close();
					}
	            }
	        }

	    } catch (FileUploadException e) {
	        throw new ServletException("Cannot parse multipart request.", e);
	    }
	}
	
	
	public List<Row> getData(InputStream filecontent) throws Exception {
//		try {
			List<Row> result = new ArrayList<Row>(1000);
			Workbook workbook = Workbook.getWorkbook(filecontent);
			Sheet sheet = workbook.getSheet(0);
			int rowsCount = sheet.getRows();
			System.err.println("There are "+(rowsCount-1)+" rows.");
			for (int i=20; i < rowsCount; i++ ) {
				 
				try {
					 Cell[] cells = sheet.getRow(i);
					 Row row = processRow(cells);
					 result.add(row);
				} catch(Exception e) {
					System.err.println("Error processing record "+i);
					e.printStackTrace();
					return null;
				}
			}
			return result;
//		} catch (BiffException e) {
//			e.printStackTrace();
//			return null;
//		} catch (IOException e) {
//			e.printStackTrace();
//			return null;
//		} 
	}

	private Row processRow(Cell[] cells) {
		String fio = getString(cells ,0);
		 StringBuffer sb = new StringBuffer();
		 for (int j = 0; j < fio.length(); j++) {
				char charAt = fio.charAt(j);
				if(j!=0 && Character.isUpperCase(charAt) && fio.charAt(j-1) != ' '){
					sb.append(' ');
				}
				sb.append(charAt);
		 }
		 String[] splitFio = sb.toString().split(" ");
		 String secondName = splitFio[0];	
		 String	firstName = splitFio[1];
		 String patronymicName = splitFio.length>2 ?  splitFio[2]: null;
		 
		 Date birthday = 		 getDate  (cells ,1);
		 StringBuffer comments  = new StringBuffer();
		 comments.append(" Возраст (на дату распределения): ");
		 comments.append(getString  (cells ,2));
		 comments.append(" Тип заявления ");
		 comments.append(getString  (cells ,4));	
		 comments.append(" Номер заявления ");
		 comments.append(getString  (cells ,5));
		 comments.append(" Зарегистрировано ");
		 comments.append(getString  (cells ,6));
		 Date regDate = getDate  (cells ,6);
		 comments.append(" Статус ");
		 comments.append(getString  (cells ,7));
		 comments.append(" ФИО заявителя ");
		 comments.append(getString  (cells ,9));
		 comments.append(" ФИО родителя ");
		 comments.append(getString  (cells ,10));
		 comments.append(" Тип уведомления ");
		 comments.append(getString  (cells ,12));
		String phone = getString  (cells ,13);
		String email = getString  (cells ,15);
		email= email.trim();
		if(email.equals("-")){
			email = null;
		}
		
		comments.append(" Тип привилегии ");
		comments.append(getString  (cells ,18));
		
		String dousS = getString(cells ,21);
		String[] split = dousS.split(",");
		List<String> dous=new ArrayList<String>(split.length);
		for (String s : split) {
			 s = s.trim();
			 if(!s.isEmpty())
				 dous.add(s);
		}
		Integer plainYear = getDate(cells ,24).getYear() + 1900;
		 
		String privelegyS = null;
		Row row = new Row(null, dous, secondName,  firstName, patronymicName,  birthday, "нет информации", regDate, privelegyS );
		row.setComments(comments.toString());
		row.setPhone(phone);
		row.setSex(Sex.M);
		row.setPlainYear(plainYear);
		return row;
	}
	
	
	protected Integer getInteger(Cell[] cells, int col) {
		if(cells.length<=col)
			return null;
		String string = getString(cells,col);
		if(string == null || string.isEmpty())
			return null;
		return new Integer(string.trim());
	}
	

	protected String getString(Cell[] cells, int col) {
		if(cells.length<=col)
			return null;
		Cell stringCell = cells[col];
		return stringCell.getContents();
	}

	protected Date getDate(Cell[] cells, int col) {
		if(cells.length<=col)
			return null;
		 Cell cell = cells[col];
		 if(cell instanceof EmptyCell)
			 return null;
		 if(cell instanceof BlankCell)
			 return null;
		 
		 if(CellType.EMPTY.equals(cell .getType() ))
			 return null;
		 
		 
		 
		 if(! (cell  instanceof DateCell)){
			 final String contents = cell.getContents();
			 return parseDate(contents);
			 
//			 throw new RuntimeException("Contents: "+ contents + "   Type: "+ cell.getType());
		 }
			 
		 DateCell dateCell= (DateCell) cell;
		 return dateCell.getDate();
	}
	
	DateFormat dfs[] =  {new SimpleDateFormat("dd.MM.yy"),
			new SimpleDateFormat("dd.MM.yyyy"),
			new SimpleDateFormat("yyyy-MM-dd"),
			new SimpleDateFormat("dd.MM.yy HH:MM")};
	
	Date parseDate(String str){
		for (DateFormat  df : dfs) {
			try {
				return df.parse(str);
			} catch(Exception e){}
		}
		throw new RuntimeException("Неверный формат даты  " + str + " попробуйте ДД.ММ.ГГГГ");
	}
}
