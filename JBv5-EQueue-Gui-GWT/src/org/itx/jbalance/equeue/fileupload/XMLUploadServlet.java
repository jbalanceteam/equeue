package org.itx.jbalance.equeue.fileupload;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

public class XMLUploadServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public static final int BUFSIZE=256;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    try {
	    	System.out.println("doPost()");
	        List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
	        String key = null;
	        String xml = null;
	        for (FileItem item : items) {
	            if (item.isFormField()) {
	                // Process regular form field (input type="text|radio|checkbox|etc", select, etc).
	                String fieldname = item.getFieldName();
	                String fieldvalue = item.getString();

	                System.out.println("fieldname: "+fieldname + " fieldvalue : "+fieldvalue );
	                
	                if("key".equals(fieldname)){
	                	key = fieldvalue;
	                }
	                
	            } else {
	                // Process form file field (input type="file").
	                String fieldname = item.getFieldName();
	                String filename = FilenameUtils.getName(item.getName());
	                InputStream filecontent = item.getInputStream();
	                System.out.println("fieldname: "+fieldname + " filename: "+filename);
	                
	                StringWriter writer = new StringWriter();
	                IOUtils.copy(filecontent, writer, "UTF-8");
	                xml = writer.toString();
	                System.out.println("xml1: "+xml);
	                
	            }
	        }
//	        if(key != null && xml != null)
//	        	SubmissionXMLCache.push(key, xml);
	        response.setContentType("text/html");
	        
	        response.setHeader("Content-Type", "text/html; charset=UTF-8");
	        response.setCharacterEncoding("UTF-8");
	        PrintWriter writer = response.getWriter();
	        
			writer.println(xml);
			writer.flush();
			writer.close();
	    } catch (FileUploadException e) {
	        throw new ServletException("Cannot parse multipart request.", e);
	    }
	}
}
