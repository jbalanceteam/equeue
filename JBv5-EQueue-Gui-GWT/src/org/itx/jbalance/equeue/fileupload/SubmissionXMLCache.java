package org.itx.jbalance.equeue.fileupload;

import java.util.Hashtable;
import java.util.Map;

public class SubmissionXMLCache {
	private static Map<String, String>cache = new Hashtable<String, String>();
	
	public static String pull(String key){
		System.out.println("pull("+key+")");
		return cache.remove(key);
	}
	
	public static String push(String key, String value){
		System.out.println("pull("+key+","+ value+")");
		return cache.put(key, value);
	}
}
