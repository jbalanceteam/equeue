package org.itx.jbalance.equeue.print;

import java.util.Calendar;
import java.util.Comparator;

import org.itx.jbalance.l0.s.SRegister;

/**
 * #334: В печатных формах протоколах сделать сортировку очередников
 */
public class SRegistrerComparator implements Comparator<SRegister>{
	@Override
	public int compare(SRegister o1, SRegister o2) {
//		p1 и p2 - флаги наличия льготы
		Boolean p1 = o1.getDouRequest().getPrivilege() != null;
		Boolean p2 = o2.getDouRequest().getPrivilege() != null;
		
		if(p2.compareTo(p1) != 0){
			return p2.compareTo(p1);
		}
			
		Calendar cal = Calendar.getInstance();
		cal.setTime(o1.getDouRequest().getRegDate());
		Integer y1 = cal.get(Calendar.YEAR);
		
		cal.setTime(o2.getDouRequest().getRegDate());
		Integer y2 = cal.get(Calendar.YEAR);
		
		if(y1.compareTo(y2) != 0){
			return y1.compareTo(y2);
		}
		
		return o1.getDouRequest().getDnumber().compareTo(o2.getDouRequest().getDnumber());
	}
}
