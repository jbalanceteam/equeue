package org.itx.jbalance.equeue.print;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class AbstractReport {

	
	private static final String PROP_NAME_TEMPLATES_DIR="templates.dir";
	
	InputStream getTemplate(String filename) throws FileNotFoundException {
			InputStream resourceAsStream = null;
			
			String templateDirectory = getTemplateDirectory();
			
			if(templateDirectory!=null) {
				File file = new File(templateDirectory+"/"+filename);
				if(file.exists()){
					resourceAsStream = new FileInputStream(file);
				}
			} 
			if(resourceAsStream==null)
			resourceAsStream = getClass().getClassLoader()
				.getResourceAsStream("./reportTemplates/"+filename);
			return resourceAsStream;
	}
	
	/**
	 * спользуется в печатны форма на основе шаблонов.
	 * Заменяет <code>param</code> в шаблоне на <code>val</code>
	 * @param doc
	 * @param param
	 * @param val
	 */
	protected void replace(Node doc,String param,String val){
		NodeList childNodes = doc.getChildNodes();
		//param="#{"+param+"}";
		for (int i=0;i<childNodes.getLength();i++) {
			Node item = childNodes.item(i);
			replace(item, param, val);
		}
		if(childNodes.getLength()>0)
			return;
		
		if(doc.getTextContent().contains(param)){
//			NPE fix
//			java.lang.NullPointerException
//			at java.util.regex.Matcher.appendReplacement(Matcher.java:699)
//			at java.util.regex.Matcher.replaceAll(Matcher.java:813)
//			at java.lang.String.replaceAll(String.java:2189)
//			at org.itx.jbalance.equeue.print.AbstractReport.replace(AbstractReport.java:51)
			if(val == null){
				val = "";
			}
			doc.setTextContent(doc.getTextContent().replaceAll(param, val));
		}
	}
	
	private String getTemplateDirectory(){
		return System.getProperty(PROP_NAME_TEMPLATES_DIR);
	}
}
