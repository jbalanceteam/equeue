
package org.itx.jbalance.equeue.print;

import java.io.File;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.PermitSearchParams;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.DateFilterType;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.d.HRegisterRemote;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.odftoolkit.odfdom.doc.table.OdfTableRow;



public final class ReportQueueByYear extends AbstractReport {

	HDouRequestsRemote ejb;
	HRegisterRemote registerEjb;
//	RegionsRemote regionsEJB;
//	DousRemote dousEJB;
	
	int from;
	int to;
	public ReportQueueByYear(HDouRequestsRemote ejb,HRegisterRemote registerEjb
			,int from,int to){
		this.ejb=ejb;
		this.registerEjb=registerEjb;
		this.from=from;
		this.to=to;
		
//		this.regionsEJB=regionsEJB;
//		this.dousEJB=dousEJB;
	}
	

    public File generateReport() {
    	try{
    		InputStream template = getTemplate("reportQueueByYear.ods");	
    		OdfDocument doc = OdfSpreadsheetDocument.loadDocument(template);
    	
		
		
		
		OdfTable table = doc. getTableByName("Распределение по годам");


		DouRequestsSearchParams searchParams=new DouRequestsSearchParams();
		Calendar cal=Calendar.getInstance();
		cal.set(Calendar.YEAR, from-1);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DATE, 31);
		cal.set(Calendar.MINUTE, 59);
		
		searchParams.regDate = new DateFilter(DateFilterType.LT , cal.getTime());
		searchParams.sex=Sex.M;
		Integer perehodInteredCntM = ejb.getSearchCount(searchParams);
		searchParams.sex=Sex.W;
		Integer perehodInteredCntW = ejb.getSearchCount(searchParams);
		
		
		
		PermitSearchParams params = new PermitSearchParams();
		params.action=Action.GIVE_PERMIT;
		params.dateFrom=null;
		params.dateTo=cal.getTime();
		params.sex=Sex.M;
		Integer perehodPermitCntM= registerEjb.getPermitCnt(params);
		params.sex=Sex.W;
		Integer perehodPermitCntW= registerEjb.getPermitCnt(params);
		
		
		
		
		params.action=Action.BACK_TO_QUEUE;
		params.sex=Sex.M;
		Integer perehodBackToQueueM= registerEjb.getPermitCnt(params);
		params.sex=Sex.W;
		Integer perehodBackToQueueW= registerEjb.getPermitCnt(params);
		
		Integer perehodM=perehodInteredCntM-perehodPermitCntM+perehodBackToQueueM;
		Integer perehodW=perehodInteredCntW-perehodPermitCntW+perehodBackToQueueW;
		
		
		
//		searchParams.sex=Sex.W;
//		Integer interedCntW = ejb.getSearchCount(searchParams);
//		
//		Integer perehodM=ejb.getPerehod(from, Sex.M);
//		Integer perehodW=ejb.getPerehod(from, Sex.W);
//		
		int rowInd=2;
		for(int year=from;year<=to;year++){
			OdfTableRow row = table.getRowByIndex(rowInd++);
			
			
			
			
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, cal.getMinimum(Calendar.MONTH));
			cal.set(Calendar.DATE, cal.getMinimum(Calendar.DATE));
			cal.set(Calendar.HOUR_OF_DAY, cal.getMinimum(Calendar.HOUR_OF_DAY));
			cal.set(Calendar.MINUTE, cal.getMinimum(Calendar.MINUTE));
			cal.set(Calendar.SECOND, cal.getMinimum(Calendar.SECOND));
			cal.set(Calendar.MILLISECOND, cal.getMinimum(Calendar.MILLISECOND));
			
			
			Date dateFrom = cal.getTime();
			
			
			cal.set(Calendar.YEAR, year);
			cal.set(Calendar.MONTH, cal.getMaximum(Calendar.MONTH));
			cal.set(Calendar.DATE, cal.getMaximum(Calendar.DATE));
			cal.set(Calendar.HOUR_OF_DAY, cal.getMaximum(Calendar.HOUR_OF_DAY));
			cal.set(Calendar.MINUTE, cal.getMaximum(Calendar.MINUTE));
			cal.set(Calendar.SECOND, cal.getMaximum(Calendar.SECOND));
			cal.set(Calendar.MILLISECOND, cal.getMaximum(Calendar.MILLISECOND));
			
			Date dateTo = cal.getTime();
			searchParams.regDate= DateFilter.build(dateFrom, dateTo);
			searchParams.sex=Sex.M;
			Integer interedCntM = ejb.getSearchCount(searchParams);
			searchParams.sex=Sex.W;
			Integer interedCntW = ejb.getSearchCount(searchParams);
			
			
			params.action=Action.GIVE_PERMIT;
			params.dateFrom=dateFrom;
			params.dateTo=dateTo;
			params.sex=Sex.M;
			Integer permitCntM= registerEjb.getPermitCnt(params);
			params.sex=Sex.W;
			Integer permitCntW= registerEjb.getPermitCnt(params);
			
			
			
			params.action=Action.DELETE_BY_PARRENTS_AGREE;
			params.sex=Sex.M;
			Integer byAgreeCntM= registerEjb.getPermitCnt(params);
			params.sex=Sex.W;
			Integer byAgreeCntW= registerEjb.getPermitCnt(params);
			
			params.action=Action.DELETE_BY_7_YEAR;
			params.sex=Sex.M;
			Integer byAgeCntM= registerEjb.getPermitCnt(params);
			params.sex=Sex.W;
			Integer byAgeCntW= registerEjb.getPermitCnt(params);
			
			
			int deltaM = interedCntM - permitCntM  - byAgreeCntM - byAgeCntM;
			int deltaW = interedCntW - permitCntW  - byAgreeCntW - byAgeCntW;
			
			
			row.getCellByIndex(0).setDoubleValue(new Double(year));
			
			row.getCellByIndex(1).setDoubleValue(new Double(perehodM+perehodW));
			row.getCellByIndex(2).setDoubleValue(new Double(perehodM));
			row.getCellByIndex(3).setDoubleValue(new Double(perehodW));
			
			row.getCellByIndex(4).setDoubleValue(new Double((interedCntM+interedCntW)));
			row.getCellByIndex(5).setDoubleValue(new Double((interedCntM)));
			row.getCellByIndex(6).setDoubleValue(new Double((interedCntW)));
			
			row.getCellByIndex(7).setDoubleValue(new Double((permitCntM+permitCntW)));
			row.getCellByIndex(8).setDoubleValue(new Double(permitCntM));
			row.getCellByIndex(9).setDoubleValue(new Double(permitCntW));
			
			row.getCellByIndex(10).setDoubleValue(new Double(byAgreeCntM+byAgreeCntW));
			row.getCellByIndex(11).setDoubleValue(new Double(byAgreeCntM));
			row.getCellByIndex(12).setDoubleValue(new Double(byAgreeCntW));
			
			row.getCellByIndex(13).setDoubleValue(new Double(byAgeCntM+byAgeCntW));
			row.getCellByIndex(14).setDoubleValue(new Double(byAgeCntM+byAgeCntW));
			row.getCellByIndex(15).setDoubleValue(new Double(byAgeCntM+byAgeCntW));
			
			
			
			row.getCellByIndex(16).setDoubleValue(new Double(deltaM+deltaW));
			row.getCellByIndex(17).setDoubleValue(new Double(deltaM));
			row.getCellByIndex(18).setDoubleValue(new Double(deltaW));
			
			
			
			perehodM=perehodM+ deltaM;
			perehodW=perehodW+ deltaW;
			
		}
		
		
		
    	
    	File tempFile = File.createTempFile("equeue", ".ods");
		doc.save(tempFile);
		doc.close();
    	return tempFile;
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e);
    	}
    	
    		
    		
    
    }


//	private String getProcent(Integer allCnt, Integer pivivegesCnt) {
//		if(allCnt==0)
//			return "-";
//		return (Math.round((float)pivivegesCnt/allCnt*100)) + "%";
//	}
//    
//    
//    private Integer getCount(Boolean privilege,String region,Long dou,Integer age){
//    	DouRequestsSearchParams searchParams=new DouRequestsSearchParams();
//		searchParams.permit=Boolean.FALSE;
//		searchParams.privilege=privilege;
//		searchParams.region=region;
//		if(dou!=null)
//			searchParams.searchedDous=Collections.singletonList(dou);
//		
//		if(age!=null){
//		Calendar cal=Calendar.getInstance();
//		cal.setTime(new Date());
//		cal.add(Calendar.MONTH, -1* age);
//		searchParams.birthdayTo=cal.getTime();
//		}
//		
//		return ejb.getSearchCount(searchParams);
//    }

    

//    private List<Dou>getDouByRegion(List<Dou>allDous,Region reg){
//    	List<Dou>res=new ArrayList<Dou>();
//    	for (Dou dou : allDous) {
//			if(dou.getRegion()!=null && dou.getRegion().equals(reg.getName()))
//				res.add(dou);
//		}
//    	return res;
//    }

}
