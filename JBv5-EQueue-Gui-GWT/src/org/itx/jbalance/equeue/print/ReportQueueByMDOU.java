
package org.itx.jbalance.equeue.print;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.DouRequestsSearchParams.OrderStatus;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.DateFilterType;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.RegionsRemote;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.doc.style.OdfStyle;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.odftoolkit.odfdom.doc.table.OdfTableRow;
import org.odftoolkit.odfdom.dom.style.OdfStyleFamily;
import org.odftoolkit.odfdom.dom.style.props.OdfTableCellProperties;
import org.odftoolkit.odfdom.dom.style.props.OdfTextProperties;



public final class ReportQueueByMDOU extends AbstractReport {

	HDouRequestsRemote ejb;
	RegionsRemote regionsEJB;
	DousRemote dousEJB;
	
	public ReportQueueByMDOU(HDouRequestsRemote ejb,RegionsRemote regionsEJB,DousRemote dousEJB){
		this.ejb=ejb;
		this.regionsEJB=regionsEJB;
		this.dousEJB=dousEJB;
	}
	

    public File generateReport() {
    	try{
    		InputStream template = getTemplate("reportQueueByMdou.ods");	
    		OdfDocument doc = OdfSpreadsheetDocument.loadDocument(template);
    	
		
//		BOLD
		OdfStyle boldStyle=doc.getOrCreateDocumentStyles()
		.newStyle("MyBold", OdfStyleFamily.TableCell);
		boldStyle.setProperty (OdfTextProperties.FontWeight, "bold"); 
		boldStyle.setProperty(OdfTableCellProperties.Border, "1pt solid black");
//		boldStyle.setProperty(OdfTableCellProperties.BorderLineWidth, "1");
		
		
//		OdfStyle defaulStyle=doc.getOrCreateDocumentStyles()
//		.newStyle("MyBold", OdfStyleFamily.TableCell);
//		boldStyle.setProperty (OdfTextProperties.FontWeight, "bold"); 
//		boldStyle.setProperty(OdfTableCellProperties.Border, "black");
//		boldStyle.setProperty(OdfTableCellProperties.BorderLineWidth, "1");
		
//		boldStyle.setProperty(OdfTableCellProperties.BorderLineWidth, "1");
//		boldStyle.setProperty (OdfTextProperties.FontWeight, "bold"); 
		
		
		
		OdfTable table = doc. getTableByName("Распределение по МДОУ");
//		OdfTable table =OdfTable.newTable(doc);
//		table.setTableName("Распределение по МДОУ");
		List dous = dousEJB.getAll(Dou.class);
		
		int rowInd=1;
	

		
		OdfTableRow sumRow = table.getRowByIndex(1);
//		sumRow.setDefaultCellStyle(boldStyle);
		Integer allCnt=getCount(null,null,null,null);
		Integer pivivegesCnt=getCount(true,null,null,null);
		Integer privilegesCnt18 = getCount(true,null,null,18);
		Integer allCnt18 = getCount(null,null,null,18);
		
		sumRow.getCellByIndex(2).setDoubleValue(new Double(allCnt));
		sumRow.getCellByIndex(3).setDoubleValue(new Double(pivivegesCnt));
		sumRow.getCellByIndex(4).setStringValue(getProcent(allCnt, pivivegesCnt));
		sumRow.getCellByIndex(5).setDoubleValue(new Double(allCnt18));
		sumRow.getCellByIndex(6).setDoubleValue(new Double(privilegesCnt18));
		
		
		sumRow.getCellByIndex(1).getOdfElement().setStyleName("MyBold");
//		table.getRowByIndex(1).setDefaultCellStyle(boldStyle);
		
		List<? extends Region> regions = regionsEJB.getAll(Region.class);
		for (Region region : regions) {
			OdfTableRow row = table.appendRow();//getRowByIndex(rowInd++);

//			row.setDefaultCellStyle(boldStyle);

			
			String regionName = region.getName();
			
			
			allCnt=				getCount(null,regionName,null,null);
			pivivegesCnt=		getCount(true,regionName,null,null);
			privilegesCnt18 = 	getCount(true,regionName,null,18);
			allCnt18 = 			getCount(null,regionName,null,18);
			
			row.getCellByIndex(1).setStringValue(regionName);
			row.getCellByIndex(2).setDoubleValue(new Double(allCnt));
			row.getCellByIndex(3).setDoubleValue(new Double(pivivegesCnt));
			row.getCellByIndex(4).setStringValue(getProcent(allCnt, pivivegesCnt));
			row.getCellByIndex(5).setDoubleValue(new Double(allCnt18));
			row.getCellByIndex(6).setDoubleValue(new Double(privilegesCnt18));
			
			
			row.getCellByIndex(1).getOdfElement().setStyleName("MyBold");
			
			
			
			
			
			List<Dou> douByRegion = getDouByRegion(dous,region);
			for (Dou dou : douByRegion) {
				OdfTableRow row1 = table.appendRow();//getRowByIndex(rowInd++);
				
				
				
				allCnt=				getCount(null,null,dou.getUId(),null);
				pivivegesCnt=		getCount(true,null,dou.getUId(),null);
				privilegesCnt18 = 	getCount(true,null,dou.getUId(),18);
				allCnt18 = 			getCount(null,null,dou.getUId(),18);
				
				row1.getCellByIndex(1).setStringValue(dou.getName());
				row1.getCellByIndex(1).getOdfElement().setStyleName("Базовый");
				row1.getCellByIndex(2).setDoubleValue(new Double(allCnt));
				row1.getCellByIndex(3).setDoubleValue(new Double(pivivegesCnt));
				row1.getCellByIndex(4).setStringValue(getProcent(allCnt, pivivegesCnt));
				row1.getCellByIndex(5).setDoubleValue(new Double(allCnt18));
				row1.getCellByIndex(6).setDoubleValue(new Double(privilegesCnt18));
				
				
			}
			
		}
		
		
    	
    	File tempFile = File.createTempFile("equeue", ".ods");
		doc.save(tempFile);
		doc.close();
    	return tempFile;
    	}catch(Exception e){
    		e.printStackTrace();
    		throw new RuntimeException(e);
    	}
    	
    		
    		
    
    }


	private String getProcent(Integer allCnt, Integer pivivegesCnt) {
		if(allCnt==0)
			return "-";
		return (Math.round((float)pivivegesCnt/allCnt*100)) + "%";
	}
    
    
    private Integer getCount(Boolean privilege, String region, Long dou, Integer age){
    	DouRequestsSearchParams searchParams=new DouRequestsSearchParams();
		searchParams.orderStatus = OrderStatus.ACTIVE_QUEUE;
		searchParams.privilege=privilege;
		searchParams.region=region;
		if(dou!=null)
			searchParams.searchedDous=Collections.singletonList(dou);
		
		if(age!=null){
		Calendar cal=Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.MONTH, -1* age);
		searchParams.birthday = new DateFilter(DateFilterType.LTE , cal.getTime());
		}
		
		return ejb.getSearchCount(searchParams);
    }

    

    private List<Dou>getDouByRegion(List<Dou>allDous,Region reg){
    	List<Dou>res=new ArrayList<Dou>();
    	for (Dou dou : allDous) {
			if(dou.getRegion()!=null && dou.getRegion().equals(reg.getName()))
				res.add(dou);
		}
    	return res;
    }

}
