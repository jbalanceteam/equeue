package org.itx.jbalance.equeue.print;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class Download extends HttpServlet {

	private static final long serialVersionUID = 1L;
	public static final int BUFSIZE=256;

	
	
	
@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		File generateReport = ReportsHelper.generateReport(req);
		doDownload(req, resp, generateReport,generateReport.getName());
	}




	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		File generateReport = ReportsHelper.generateReport(req);
		doDownload(req, resp, ReportsHelper.generateReport(null),generateReport.getName());
	}




	/**
	 *  Sends a file to the ServletResponse output stream.  Typically
	 *  you want the browser to receive a different name than the
	 *  name the file has been saved in your local database, since
	 *  your local names need to be unique.
	 *
	 *  @param req The request
	 *  @param resp The response
	 *  @param filename The name of the file you want to download.
	 *  @param original_filename The name the browser should receive.
	 */
	private void doDownload( HttpServletRequest req, HttpServletResponse resp,
			File file, String original_filename )
	    throws IOException
	{
		
	//    File                f        = new File(filename);
	    int                 length   = 0;
	    ServletOutputStream op       = resp.getOutputStream();
	    ServletContext      context  = getServletConfig().getServletContext();
	    String              mimetype = context.getMimeType( file.getAbsolutePath()); //.getAbsolutePath()
	
	    //
	    //  Set the response and go!
	    //
	    //
	    resp.setContentType( (mimetype != null) ? mimetype : "application/octet-stream" );
	    resp.setContentLength( (int)file.length() );
	    resp.setHeader( "Content-Disposition", "attachment; filename=\"" +original_filename + "\"" );// original_filename
	
	    //
	    //  Stream to the requester.
	    //
	    byte[] bbuf = new byte[BUFSIZE];
	    DataInputStream in = new DataInputStream(new FileInputStream(file));
	
	    while ((in != null) && ((length = in.read(bbuf)) != -1))
	    {
	        op.write(bbuf,0,length);
	    }
	
	    in.close();
	    op.flush();
	    op.close();
	}


}