package org.itx.jbalance.equeue.print;

//import OdfContentDom;
//import OdfTextSpan;
//import TextPElement;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.List;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.s.SDouRequest;

import org.odftoolkit.odfdom.OdfFileDom;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.doc.office.OdfOfficeDocument;
import org.odftoolkit.odfdom.doc.office.OdfOfficeSpreadsheet;
import org.w3c.dom.DOMException;

public final class ReportRegistrationNotification extends AbstractReport  {

	private static ReportRegistrationNotification instance = null;

	private ReportRegistrationNotification() {
	}

	public static synchronized ReportRegistrationNotification getInstance() {
		if (instance == null)
			instance = new ReportRegistrationNotification();
		return instance;
	}

	String outputFileName;
	OdfDocument outputDocument;
	OdfFileDom contentDom;
	OdfOfficeSpreadsheet officeSpreadsheet;
	OdfOfficeDocument officeDocument;

	void setupOutputDocument() {
		try {
			InputStream template = getTemplate("reportRegistrationNotification.odt");
			outputDocument = OdfTextDocument.loadDocument(template);
		} catch (Exception e) {
			System.err.println("Unable to create output file.");
			System.err.println(e.getMessage());
			outputDocument = null;
		}
	}

	private File saveOutputDocument() {
		File outputFile = null;
		try {
			outputFile = File.createTempFile("reportRegistrationNotificationTemp", ".odt");

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			outputDocument.save(outputFile);
			outputDocument.close();
		} catch (Exception e) {
			System.err.println("Unable to save document.");
			System.err.println(e.getMessage());
		}
		return outputFile;
	}

	/**
	 * @param rec_num
	 * @param odfTableRow
	 * @param douRequest
	 * @param specificationLines 
	 */
	public File printRegistrationNotification(HDouRequest douRequest, List<SDouRequest> specificationLines) {
		setupOutputDocument();
		if (outputDocument != null) {
			processRegistrationNotification(douRequest, specificationLines);
			return saveOutputDocument();
		}
		return null;
	}

	private void processRegistrationNotification(HDouRequest douRequest, List<SDouRequest> specificationLines) {
		try {
			OdfTextDocument doc=(OdfTextDocument) outputDocument;
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");      
			String fullName = douRequest.getChild().getSurname() +" "+
			                  douRequest.getChild().getName()+" "+
			                  douRequest.getChild().getPatronymic();
			String birthdayStr = sdf.format(douRequest.getChild().getBirthday());
			String registrationDateStr = sdf.format(douRequest.getRegDate());
			replace(doc.getContentDom(),"p1", fullName);
			replace(doc.getContentDom(),"p2", birthdayStr);
			replace(doc.getContentDom(),"p3", registrationDateStr);
			replace(doc.getContentDom(),"p4", douRequest.toString());
			
			StringBuilder p5 = new StringBuilder();
			StringBuilder p6 = new StringBuilder();
			
			for (SDouRequest sDouRequest : specificationLines) {
				if(sDouRequest!=null && sDouRequest.getArticleUnit() != null){
					p5.append(sDouRequest.getArticleUnit().getNumber());
					p5.append(", ");
				}
				
				if(sDouRequest!=null && sDouRequest.getArticleUnit() != null){
					p6.append(sDouRequest.getArticleUnit().getName());
					p6.append(", ");
				}
			}
			if(p5.length() > 0){
				p5.deleteCharAt(p5.length()-2);
			}
			
			if(p6.length() > 0){
				p6.deleteCharAt(p6.length()-2);
			}
			
			replace(doc.getContentDom(),"p5", p5.toString());
			replace(doc.getContentDom(),"p6", p6.toString());
			
			
		} catch (DOMException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}
	

	
	//OdfParagraph paragraph = new OdfParagraph(contentDom);
	//OdfFileDom contentDom = outputDocument.getContentDom();
	//XPath xpath = contentDom.g .getXPath();
	//TextPElement para = (TextPElement) xpath.evaluate("//text:p[1]", contentDom, XPathConstants.NODE);
	//OdfTextSpan spanElem = new OdfTextSpan(contentDom);
	//spanElem.setTextContent(TEST_SPAN_TEXT);
	//XPathFactory
	//para.appendChild(spanElem);
	//((OdfTextDocument)outputDocument).getContentRoot().setTextContent("ededweddddddddddddd");
	//((OdfTextDocument)outputDocument).
//	String str = ((OdfTextDocument)outputDocument).getContentRoot().getTextContent().replace("p1", douRequest.getChild().getName());
//	((OdfTextDocument)outputDocument).addText(str);
//	XPath xpath = ((OdfTextDocument)outputDocument).getXPath();
//	contentDom = ((OdfTextDocument)outputDocument).getContentDom();
//	TextPElement para = (TextPElement) xpath.evaluate("//text:T3", contentDom, XPathConstants.NODE);
//	OdfTextSpan spanElem = new OdfTextSpan(contentDom);
//	spanElem.setTextContent(douRequest.getChild().getName());
//	para.setNodeValue(douRequest.getChild().getName());
	// appendChild(spanElem);
	//<text:span text:style-name="T3">
	//OdfTextDocument.newTextTemplateDocument().getXPath();
	//((OdfTextDocument)outputDocument).getXPath().
	//OdfParagraph paragraph = new OdfParagraph(contentDom);
}
