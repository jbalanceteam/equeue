package org.itx.jbalance.equeue.print;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.itx.jbalance.l0.AgeCalculator;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.odftoolkit.odfdom.doc.table.OdfTableRow;

/**
 * Тот самый отчет который делает экспорт списка РН
 * @author apv
 *
 */
public final class ReportRN extends AbstractReport {

	private OdfDocument outputDocument;
	
	public File printRequestsList(List<DouRequestDTO> douRequests) {
		System.err.println("Ok... Let's see... we got list of "+douRequests.size()+" RN");
		setupOutputDocument();
		if (outputDocument != null) {
			processRequestsListDoc(douRequests);
			return saveOutputDocument(); 
		}
		return null;
	}

	private void setupOutputDocument() {
		try {
			System.err.println("setupOutputDocument()");
			InputStream template = getTemplate("reportRN.ods");
			outputDocument = OdfSpreadsheetDocument.loadDocument(template);
		} catch (Exception e) {
			System.err.println("Unable to create output file.");
			System.err.println(e.getMessage());
			outputDocument = null;
		}
	}

	private void processRequestsListDoc(List<DouRequestDTO>  requestsList) {
		System.err.println("processRequestsListDoc()");
		int rec_num = 0;
		OdfTable table = outputDocument.getTableByName("Список РН");
//		table.appendRow();
		for (DouRequestDTO douRequest : requestsList) {
			OdfTableRow odfTableRow = table.appendRow();
			rec_num += 1;
			setRow(rec_num, odfTableRow, douRequest);
			System.err.print("row "+rec_num+"  ");
		}
	}

	private File saveOutputDocument() {
		System.err.println("saveOutputDocument()");
		File outputFile = null;
		try {
			outputFile = File.createTempFile("reportRN", ".ods");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			outputDocument.save(outputFile);
			outputDocument.close();
		} catch (Exception e) {
			System.err.println("Unable to save document.");
			e.printStackTrace();
		}
		return outputFile;
	}

	/**
	 * @param rec_num
	 * @param odfTableRow
	 * @param douRequest
	 */
	private void setRow(int rec_num, OdfTableRow odfTableRow,
			DouRequestDTO douRequest) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		
		odfTableRow.getCellByIndex(0).setDisplayText(	Integer.valueOf(rec_num).toString());
		odfTableRow.getCellByIndex(1).setDisplayText(douRequest.getRegNumber());
		odfTableRow.getCellByIndex(2).setDisplayText(
				douRequest.getOldNumber() != null ? douRequest.getOldNumber()
						: "");
		odfTableRow.getCellByIndex(3).setDisplayText(
				douRequest.getRegDate() != null ? sdf.format(douRequest
						.getRegDate()) : "");
		odfTableRow.getCellByIndex(4).setDisplayText(
				douRequest.getSverkDate() != null ? sdf.format(douRequest.getSverkDate()) : "");
		odfTableRow.getCellByIndex(5).setDisplayText(douRequest.getSurname());
		odfTableRow.getCellByIndex(6).setDisplayText(douRequest.getRealName());
		odfTableRow.getCellByIndex(7).setDisplayText(douRequest.getPatronymic());
		odfTableRow.getCellByIndex(8).setDisplayText(douRequest.getDous());
		odfTableRow.getCellByIndex(9).setDisplayText(
				douRequest.getBirthday() != null ?  AgeCalculator.calculateAge(
						 douRequest.getBirthday(), new Date()).toStringRus() : "");
		odfTableRow.getCellByIndex(10).setDisplayText(
				douRequest.getSex() != null ? douRequest.getSex().toString() : "");
		
		odfTableRow.getCellByIndex(11).setDisplayText(
				douRequest.getBirthday() != null && douRequest.getBirthday() != null ? 
				sdf.format(douRequest.getBirthday()) : 
				"");
		
		odfTableRow.getCellByIndex(12).setDisplayText(douRequest.getBirthdaySertificateSeria());
		odfTableRow.getCellByIndex(13).setDisplayText(douRequest.getBirthdaySertificateNumber());

		
		String str13 = "";
		if(douRequest.getBirthdaySertificateDate() != null){
			str13 =sdf.format(douRequest.getBirthdaySertificateDate());
		}
		odfTableRow.getCellByIndex(14).setDisplayText(str13);
		
		
		odfTableRow.getCellByIndex(15).setDisplayText(douRequest.getAddress());
		odfTableRow.getCellByIndex(16).setDisplayText(douRequest.getPhone());
		odfTableRow.getCellByIndex(17).setDisplayText(douRequest.getPrivilege() != null ? douRequest.getPrivilege().getName() : "");
		odfTableRow.getCellByIndex(18).setDisplayText(
				douRequest.getYear() != null ? douRequest.getYear().toString()
						: "");
		
//		odfTableRow.getCellByIndex(17).setDisplayText(
//				douRequest..getYear() != null ? douRequest.getYear().toString()
//						: "");
		
		//дата путевки (если есть; если не одна, то последняя)
		//Номер путевки (если есть; если не одна, то последняя)
		//номер протокола (если есть; если не один, то последний)
	}		
}
