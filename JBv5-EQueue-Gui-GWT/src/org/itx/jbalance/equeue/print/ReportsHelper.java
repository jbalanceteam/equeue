package org.itx.jbalance.equeue.print;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.itx.jbalance.equeue.gwt.server.LookupHelper;
import org.itx.jbalance.equeue.gwt.server.SerializerHelper;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.d.HRegisterRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.RegionsRemote;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;
import org.itx.jbalance.l2_api.services.DouRequestService;

public class ReportsHelper {


	public static File generateReport (HttpServletRequest request){
		
		String reportType = request.getParameter("type");
		
		String sessionId = (String) request.getParameter("sessionId");
		if("requestsList".equals(reportType)){
//			HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(sessionId);
			DouRequestService douRequestsSession = LookupHelper.getInstance().getDouRequestsSession();
			DouRequestsSearchParams searchParams = (DouRequestsSearchParams) SerializerHelper.deserializeFromBase64(request.getParameter("searchParams"));

			int delta = 500;
			Integer from = 0;
			Integer to = delta-1;
			
			List<DouRequestDTO> douRequests = new LinkedList<DouRequestDTO>();
			List<DouRequestDTO> tmp;
			do {
				searchParams.setFrom(from);
				searchParams.setTo(to);
				searchParams.setSortInfo(null);
	
				tmp = douRequestsSession.getDouRequests(searchParams);
				douRequests.addAll(tmp);
				
				from = to + 1;
				to = to + delta;
			}while (tmp.size() >= delta);
			return new ReportRN().printRequestsList(douRequests);
		} else if("registrationNotification".equals(reportType)){
			HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(sessionId);
		    Long uid = new Long(request.getParameter("hDouRequestUidParam"));
		    HDouRequest hDouRequest = ejb.getByUId(uid,HDouRequest.class);
		    List<SDouRequest> specificationLines = ejb.getSpecificationLines(hDouRequest);
			return ReportRegistrationNotification.getInstance().printRegistrationNotification(hDouRequest, specificationLines);
		
			
//			#333 Разделяем protocol на 3 разных отчета: отказ, выдана, вернули
		} else if("protocol".equals(reportType)){
//			Тут мы печатаем всё(!) содержимое протокола 
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(sessionId);
		    Long uid = new Long(request.getParameter("hRegisterUidParam"));
		    HRegister hRegister = ejb.getByUId(uid,HRegister.class);
		    List<SRegister> sRegisters =ejb.getSpecificationLines(hRegister); 
			return ReportProtocol.getInstance().printProtocol(hRegister,sRegisters, "");
		
		} else if("protocolRefusal".equals(reportType)){  // отказ
//			Тут мы печатаем тех, кому отказали
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(sessionId);
		    Long uid = new Long(request.getParameter("hRegisterUidParam"));
		    HRegister hRegister = ejb.getByUId(uid,HRegister.class);
		    List<SRegister> all = ejb.getSpecificationLines(hRegister); 
			return ReportProtocol.getInstance().printProtocol(hRegister, filterSRegistersByAction(all, Action.REFUSAL), "Refusal");
		} else if("protocolGivePermit".equals(reportType)){ //выдана
//			Тут мы печатаем тех, кому ВЫДАЛИ путевку
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(sessionId);
		    Long uid = new Long(request.getParameter("hRegisterUidParam"));
		    HRegister hRegister = ejb.getByUId(uid,HRegister.class);
		    List<SRegister> all =ejb.getSpecificationLines(hRegister); 
			return ReportProtocol.getInstance().printProtocol(hRegister, filterSRegistersByAction(all, Action.GIVE_PERMIT), "GivePermit");
		} else if("protocolBackToQueue".equals(reportType)){ // вернуть
//			Тут мы печатаем только тех, кого вернули в очередь
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(sessionId);
		    Long uid = new Long(request.getParameter("hRegisterUidParam"));
		    HRegister hRegister = ejb.getByUId(uid,HRegister.class);
		    List<SRegister> all =ejb.getSpecificationLines(hRegister); 
			return ReportProtocol.getInstance().printProtocol(hRegister, filterSRegistersByAction(all, Action.BACK_TO_QUEUE), "BackToQueue");
			
			
//			#333 Разделяем tickets на 3 разных отчета: отказ, выдана, вернули	
		} else if("tickets".equals(reportType)){
//			Тут мы печатаем всё(!) содержимое протокола 
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(sessionId);
		    Long uid = new Long(request.getParameter("hRegisterUidParam"));
		    HRegister hRegister = ejb.getByUId(uid,HRegister.class);
		    List<SRegister> sRegisters =ejb.getSpecificationLines(hRegister);
		    return new ReportTickets().printTickets(hRegister, sRegisters, "");
		} else if("ticketsRefusal".equals(reportType)){
//			Тут мы печатаем тех, кому отказали
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(sessionId);
		    Long uid = new Long(request.getParameter("hRegisterUidParam"));
		    HRegister hRegister = ejb.getByUId(uid,HRegister.class);
		    List<SRegister> all = ejb.getSpecificationLines(hRegister); 
		    return new ReportTickets().printTickets(hRegister, filterSRegistersByAction(all, Action.REFUSAL), "Refusal");
		} else if("ticketsGivePermit".equals(reportType)){
//			Тут мы печатаем тех, кому ВЫДАЛИ путевку
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(sessionId);
		    Long uid = new Long(request.getParameter("hRegisterUidParam"));
		    HRegister hRegister = ejb.getByUId(uid,HRegister.class);
		    List<SRegister> all = ejb.getSpecificationLines(hRegister); 
		    return new ReportTickets().printTickets(hRegister, filterSRegistersByAction(all, Action.GIVE_PERMIT), "GivePermit");
		} else if("ticketsBackToQueue".equals(reportType)){
//			Тут мы печатаем только тех, кого вернули в очередь
			HRegisterRemote ejb = LookupHelper.getInstance().getRegister(sessionId);
		    Long uid = new Long(request.getParameter("hRegisterUidParam"));
		    HRegister hRegister = ejb.getByUId(uid,HRegister.class);
		    List<SRegister> all = ejb.getSpecificationLines(hRegister); 
		    return new ReportTickets().printTickets(hRegister, filterSRegistersByAction(all, Action.BACK_TO_QUEUE), "BackToQueue");
		
		
		} else if("reportPrognoz".equals(reportType)){
			HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(sessionId);
			ReportPrognoz reportPrognoz=new ReportPrognoz();
			return reportPrognoz.generateReport(ejb);
		
		} else if("reportQueueByMdou".equals(reportType)){
			HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(sessionId);
			DousRemote dousEJB = LookupHelper.getInstance().getDous(sessionId);
			RegionsRemote regionsEjb = LookupHelper.getInstance().getRegions(sessionId);
			ReportQueueByMDOU reportPrognoz=new ReportQueueByMDOU(ejb,regionsEjb,dousEJB);
			return reportPrognoz.generateReport();
		
		} else  if("queueByYear".equals(reportType)){
			HDouRequestsRemote ejb = LookupHelper.getInstance().getHDouRequests(sessionId);
			HRegisterRemote registerEjb = LookupHelper.getInstance().getRegister(sessionId);
			
			ReportQueueByYear reportPrognoz=new ReportQueueByYear(ejb,registerEjb,
					new Integer(request.getParameter("yearFrom")),
					new Integer(request.getParameter("yearTo")));
			return reportPrognoz.generateReport();
		} else {
			return null;
		}
	}

	private static List<SRegister> filterSRegistersByAction(List<SRegister> all, Action action) {
		List<SRegister> selected = new ArrayList<SRegister>();
		for (SRegister sRegister : all) {
			if(action.equals(sRegister.getRnAction())){
				selected . add(sRegister);
			}
		}
		return selected;
	}
	
	
}
