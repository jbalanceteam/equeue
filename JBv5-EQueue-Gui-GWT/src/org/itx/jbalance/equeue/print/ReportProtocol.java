package org.itx.jbalance.equeue.print;

//import OdfContentDom;
//import OdfTextSpan;
//import TextPElement;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.s.SRegister;
import org.odftoolkit.odfdom.OdfFileDom;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.doc.office.OdfOfficeDocument;
import org.odftoolkit.odfdom.doc.office.OdfOfficeSpreadsheet;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.odftoolkit.odfdom.doc.table.OdfTableRow;
import org.w3c.dom.DOMException;

//import org.odftoolkit.odfdom.doc.text.;;

public final class ReportProtocol extends AbstractReport {

	
	private static ReportProtocol instance = null;

	private ReportProtocol() {
	}

	public static synchronized ReportProtocol getInstance() {
		if (instance == null)
			instance = new ReportProtocol();
		return instance;
	}

	String outputFileName;
	OdfDocument outputDocument;
	OdfFileDom contentDom;
	OdfOfficeSpreadsheet officeSpreadsheet;
	OdfOfficeDocument officeDocument;

	/**
	 * 
	 * @param siffix Тут может быть 4 разных шаблона: без суффикса, Refusal, GivePermit, BackToQueue
	 * Это будет выглядеть, как 4 разных отчета
	 */
	void setupOutputDocument(String siffix) {
		try {
			InputStream template = getTemplate("reportProtocol" +(siffix==null?"":siffix)+  ".odt");
			outputDocument = OdfTextDocument.loadDocument(template);
		} catch (Exception e) {
			System.err.println("Unable to create output file.");
			System.err.println(e.getMessage());
			outputDocument = null;
		}
	}

	private File saveOutputDocument() {
		File outputFile = null;
		try {
			outputFile = File.createTempFile("reportProtocolTemp", ".odt");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			outputDocument.save(outputFile);
			outputDocument.close();
		} catch (Exception e) {
			System.err.println("Unable to save document.");
			System.err.println(e.getMessage());
		}
		return outputFile;
	}

	/**
	 * @param rec_num
	 * @param odfTableRow
	 * @param hRegister
	 * @param type Тут может быть 4 разных типа: null, Refusal, GivePermit, BackToQueue
	 * Это будет выглядеть, как 4 разных отчета
	 */
	public File printProtocol(HRegister  hRegister, List<SRegister> sRegisters, String type) {
		Collections.sort(sRegisters, new SRegistrerComparator());
		setupOutputDocument(type);
		if (outputDocument != null) {
			processProtocol(hRegister,sRegisters);
			return saveOutputDocument();
		}
		return null;
	}


	private void processProtocol(HRegister hRegister,List<SRegister> sRegisters) {
		try {
			Collections.sort(sRegisters, new SRegistrerComparator());
			
			OdfTextDocument doc=(OdfTextDocument) outputDocument;
			SimpleDateFormat sdfDD = new SimpleDateFormat("dd");      
			SimpleDateFormat sdfYYYY = new SimpleDateFormat("yyyy");      

			String protocolNumber = hRegister.getDnumber().toString();
			String dateStr = sdfDD.format(hRegister.getRegisterDate());
			String yearStr =sdfYYYY.format(hRegister.getRegisterDate());
			String monthStr = getMonth(hRegister);
			
			replace(doc.getContentDom(),"p1", protocolNumber);
			replace(doc.getContentDom(),"p2", dateStr);
			replace(doc.getContentDom(),"p3", monthStr);
			replace(doc.getContentDom(),"p4", yearStr);
					
			int rec_num = 0;
			OdfTable odfTable =(OdfTable)outputDocument.getTableByName("Таблица1");
			for(SRegister sRegister : sRegisters) {
			   rec_num += 1;
		       OdfTableRow odfTableRow = odfTable.appendRow();
		       
		       odfTableRow.getCellByIndex(0).setDisplayText(Integer.valueOf(rec_num).toString());
		       odfTableRow.getCellByIndex(1).setDisplayText(sRegister.getDouRequest()+"");
		       odfTableRow.getCellByIndex(2).setDisplayText("");
		       odfTableRow.getCellByIndex(3).setDisplayText("");
		       Physical child = sRegister.getDouRequest().getChild();
			   String fullName = child.getSurname()+" "+child.getName()+" "+child.getPatronymic();
		       odfTableRow.getCellByIndex(4).setDisplayText(fullName);
		       SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");  
		       odfTableRow.getCellByIndex(5).setDisplayText(sdf.format(child.getBirthday()));
		       String dous="";
		       if(sRegister.getDou()!=null){
			    dous = sRegister.getDou().getNumber()+""; //+"  ("+sRegister.getDou().getName()+")"
		       }
		       odfTableRow.getCellByIndex(6).setDisplayText(dous);
		       
		       
		       odfTableRow.getCellByIndex(7).setDisplayText(sRegister.getRnAction().getShortDescription()); 
		       
//		       поле "Примечание"
//		       #342 агружать в поле "примечание" конкретную льготу очередника, если она у него есть
		       StringBuffer comment = new StringBuffer();
		       if(sRegister.getFoundation()!=null){
		    	   comment.append("Основание: ");
		    	   comment.append(sRegister.getFoundation().getShortName());
		       }
		       
		       Privilege privilege = sRegister.getDouRequest().getPrivilege();
		       if(privilege !=null){
		    	   if(comment.length() > 0){
		    		   comment.append("\n");
		    	   }
		    	   comment.append("Льгота: ");
		    	   comment.append( privilege.getName());
		       }
		       odfTableRow.getCellByIndex(8).setDisplayText(comment.toString()); //sRegister.getDouRequest().getStatus().getDescription()
			}
			//newTable.getCellByPosition(1, 1).setStringValue("УРА УРА УРА");
			
			
			
//			XPath xpath = doc.getXPath();
//			Node p_s=(Node)xpath.evaluate("//office:text/text:p[14]/draw:frame/draw:text-box/table:table/table:table-row[2]", outputDocument.getContentDom(),XPathConstants.NODE);
//			//Node p = (Node)xpath.evaluate("//office:text/text:p[14]/draw:frame/draw:text-box/table:table", outputDocument.getContentDom(),XPathConstants.NODE);
//			String str = "<table:table-row table:style-name=\"Таблица1.2\">  <table:table-cell office:value-type=\"string\"      " + 
//			"table:style-name=\"Таблица1.A1\">         " + 
//			"<text:list text:style-name=\"WW8Num2\"    " + 
//				"xmlns:xml=\"http://www.w3.org/XML/1998/namespace\" xml:id=\"list28666306\"> " + 
//					"<text:list-item> " + 
//						"<text:p text:style-name=\"P13\" /> " + 
//					"</text:list-item> " + 
//				"</text:list>" + 
//			"</table:table-cell>" + 
//			"<table:table-cell office:value-type=\"string\" " + 
//			"table:style-name=\"Таблица1.A1\"> " + 
//				"<text:p text:style-name=\"P7\">2448</text:p> " + 
//			"</table:table-cell> " + 
//			"<table:table-cell office:value-type=\"string\" " + 
//				"table:style-name=\"Таблица1.A1\"> " + 
//				"<text:p text:style-name=\"P7\" /> " + 
//			"</table:table-cell> " + 
//			"<table:table-cell office:value-type=\"string\" " + 
//				"table:style-name=\"Таблица1.A1\"> " + 
//				"<text:p text:style-name=\"P7\" /> " + 
//			"</table:table-cell> " + 
//			"<table:table-cell office:value-type=\"string\" " + 
//				"table:style-name=\"Таблица1.A1\"> " + 
//				"<text:p text:style-name=\"P9\">Чернышев Алексей</text:p> " + 
//		     "</table:table-cell> " +
//			"<table:table-cell office:value-type=\"string\" " + 
//				"table:style-name=\"Таблица1.A1\"> " + 
//				"<text:p text:style-name=\"P9\">27.02.09</text:p> " + 
//			"</table:table-cell> " + 
//			"<table:table-cell office:value-type=\"string\" " + 
//				"table:style-name=\"Таблица1.A1\"> " + 
//				"<text:p text:style-name=\"P9\">149</text:p> " + 
//			"</table:table-cell> " + 
//			"<table:table-cell office:value-type=\"string\" " + 
//				"table:style-name=\"Таблица1.H1\"> " + 
//				"<text:p text:style-name=\"P10\">Заявление</text:p> " + 
//			"</table:table-cell>" + 
//	 "</table:table-row>";
//			DocumentBuilder docBuilder = DocumentBuilderFactory
//		    .newInstance()
//		    .newDocumentBuilder();
//			Node n = docBuilder.parse(
//			        new InputSource(new StringReader(str)))
//			        .getDocumentElement();
//			    n = outputDocument.getContentDom().importNode(n, true);//.importNode(n, true);
//			    //p.appendChild(n);
//			    outputDocument.getContentDom().replaceChild(p_s, n);
//			    //.insertBefore(n,p_s);
//			    //removeChild(p_s);// .replaceChild(p_s, p);
			
		} catch (DOMException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return;
	}

	/**
	 * @param hRegister
	 */
	private String getMonth(HRegister hRegister) {
		SimpleDateFormat sdfMM = new SimpleDateFormat("MM");      
		int monthStrNum = new Integer(sdfMM.format(hRegister.getRegisterDate()));
		switch (monthStrNum) {
		 case 1: return "января"; 
		 case 2: return "февраля"; 
		 case 3: return "марта"; 
		 case 4: return "апреля"; 
		 case 5: return "мая"; 
		 case 6: return "июня";  
		 case 7: return "июля"; 
		 case 8: return "августа"; 
		 case 9: return "сентября"; 
		 case 10: return "октября"; 
		 case 11: return "ноября"; 
		 case 12: return "декабря"; 
		}
		return "";
	}
	
	
	
	
	//OdfParagraph paragraph = new OdfParagraph(contentDom);
	//OdfFileDom contentDom = outputDocument.getContentDom();
	//XPath xpath = contentDom.g .getXPath();
	//TextPElement para = (TextPElement) xpath.evaluate("//text:p[1]", contentDom, XPathConstants.NODE);
	//OdfTextSpan spanElem = new OdfTextSpan(contentDom);
	//spanElem.setTextContent(TEST_SPAN_TEXT);
	//XPathFactory
	//para.appendChild(spanElem);
	//((OdfTextDocument)outputDocument).getContentRoot().setTextContent("ededweddddddddddddd");
	//((OdfTextDocument)outputDocument).
//	String str = ((OdfTextDocument)outputDocument).getContentRoot().getTextContent().replace("p1", douRequest.getChild().getName());
//	((OdfTextDocument)outputDocument).addText(str);
//	XPath xpath = ((OdfTextDocument)outputDocument).getXPath();
//	contentDom = ((OdfTextDocument)outputDocument).getContentDom();
//	TextPElement para = (TextPElement) xpath.evaluate("//text:T3", contentDom, XPathConstants.NODE);
//	OdfTextSpan spanElem = new OdfTextSpan(contentDom);
//	spanElem.setTextContent(douRequest.getChild().getName());
//	para.setNodeValue(douRequest.getChild().getName());
	// appendChild(spanElem);
	//<text:span text:style-name="T3">
	//OdfTextDocument.newTextTemplateDocument().getXPath();
	//((OdfTextDocument)outputDocument).getXPath().
	//OdfParagraph paragraph = new OdfParagraph(contentDom);
}
