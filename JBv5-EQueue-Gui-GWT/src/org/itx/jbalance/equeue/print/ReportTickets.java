package org.itx.jbalance.equeue.print;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.xpath.XPathExpressionException;

import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.s.SRegister;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfTextDocument;
import org.odftoolkit.odfdom.doc.text.OdfTextParagraph;
import org.odftoolkit.odfdom.dom.element.text.TextSoftPageBreakElement;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
/**
 * Путевки.
 * Этот отчет выполняется, при нажатии на иконку принтера в UI
 * 
 * @author apv
 *
 */
public final class ReportTickets extends AbstractReport {

	public File printTickets(HRegister hRegister,
			List<SRegister> sRegisters, String type) {
		try {
			Collections.sort(sRegisters, new SRegistrerComparator());
			
			InputStream template = getTemplate("reportTickets" +  (type==null?"":type) + ".odt");
			OdfDocument outputDocument = OdfTextDocument.loadDocument(template);
			OdfTextDocument doc = (OdfTextDocument) outputDocument;

			try {
				System.out.println("printTickets [1]");

				NodeList childNodes = doc.getContentRoot().getChildNodes();
				List<Node> list = new ArrayList<Node>(childNodes.getLength());

				for (int i = 1; i < childNodes.getLength(); i++) {
					Node node = childNodes.item(i);
					Node cloneNode = node.cloneNode(true);
					list.add(cloneNode);
				}
				
				System.out.println("printTickets [2]");
				fillOneSection(hRegister, sRegisters.get(0), doc.getContentDom());

				System.out.println("printTickets [3]");
				for (int i = 1; i < sRegisters.size(); i++) {
					SRegister s = sRegisters.get(i);
					System.out.println("printTickets [4][1] "+ i);
					for (Node node : list) {
						Node cloneNode = node.cloneNode(true);
						fillOneSection(hRegister, s, cloneNode);
						doc.getContentRoot().appendChild(cloneNode);
					}
					System.out.println("printTickets [4][2] "+ i);
					
					if ((i + 1) % 2 == 0) {
						TextSoftPageBreakElement textSoftPageBreakElement = new TextSoftPageBreakElement(
								doc.getContentDom());
						OdfTextParagraph odfParagraph = new OdfTextParagraph(
								doc.getContentDom());
						odfParagraph.appendChild(textSoftPageBreakElement);
						doc.getContentRoot().appendChild(odfParagraph);
					}

				}

			} catch (XPathExpressionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			File outputFile = File.createTempFile("reportTicketsTemp", ".odt");
			doc.save(outputFile);
			return outputFile;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	private void fillOneSection(HRegister hRegister,
			SRegister sRegister, Node node) throws Exception {	
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		replace(node, "p0", sRegister.getSeqNumber() + "/"+ hRegister.getDnumber());
		replace(node, "p2", sdf.format(hRegister.getRegisterDate()));
		replace(node, "p3", sRegister.getDou()!=null ? sRegister.getDou().getName() : "");
		replace(node, "p4", sRegister.getDou()!=null ? sRegister.getDou().getJAddress() : "");
		Physical child = sRegister.getDouRequest().getChild();
		String fio = "";
		if(child.getSurname() != null){
			fio = fio + child.getSurname();
		}
		
		if(child.getName() != null){
			fio = fio + " "+ child.getName();
		}
		
		if(child.getPatronymic() != null){
			fio = fio + " "+ child.getPatronymic();
		}
		
		replace(node, "p5", fio );
		replace(node, "p6", child.getBirthday()!=null ? sdf.format(child.getBirthday()): "");
		//replace(node, "p7", sRegister.getDouRequest().getChild().getPAddress()!=null ? sRegister.getDouRequest().getChild().getPAddress() : "");
		replace(node, "p7", sRegister.getDouRequest().getAddress()!=null ? sRegister.getDouRequest().getAddress() : "");
		replace(node, "p8", sRegister.getDouRequest().getChild().getPhone()!=null ? sRegister.getDouRequest().getChild().getPhone() : "");
		replace(node, "p9", sRegister.getDouRequest()!=null ? sRegister.getDouRequest().toString():"");
		replace(node, "p10",	sdf.format(sRegister.getDouRequest().getRegDate()));
		replace(node, "p11", hRegister.getDnumber()!=null ? hRegister.getDnumber().toString():"");
		replace(node, "p12",	sdf.format(hRegister.getRegisterDate()));
		replace(node, "p13", sRegister.getDouRequest().getPrivilege()!=null ? sRegister.getDouRequest().getPrivilege().getName() : "");
		replace(node, "p14", sRegister.getFoundation()!=null ? sRegister.getFoundation().getDescription(): "");
		
		replace(node, "p15", sRegister.getDou()!=null ? sRegister.getDou().getNumber()+"" : "");
		replace(node, "p16", sRegister.getDou()!=null ? sRegister.getDou().getPAddress() : "");
	}

}
