package org.itx.jbalance.equeue.print;

import java.io.File;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;

import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.DouRequestsSearchParams.OrderStatus;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.IntegerFilter;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.odftoolkit.odfdom.doc.OdfDocument;
import org.odftoolkit.odfdom.doc.OdfSpreadsheetDocument;
import org.odftoolkit.odfdom.doc.table.OdfTable;
import org.odftoolkit.odfdom.doc.table.OdfTableRow;

public final class ReportPrognoz extends AbstractReport {

	public File generateReport(HDouRequestsRemote ejb) {
		try {
			InputStream template = getTemplate("reportPrognoz.ods");
			OdfDocument doc = OdfSpreadsheetDocument.loadDocument(template);

			OdfTable table = doc.getTableByName("Прогноз по годам");

			Calendar cal = Calendar.getInstance();

			for (int yearDelta = 1; yearDelta < 4; yearDelta++) {
				DouRequestsSearchParams searchParams = new DouRequestsSearchParams();
				int year = cal.get(Calendar.YEAR) + yearDelta;
				searchParams.plainYear = new IntegerFilter(year);
				searchParams.sex = Sex.M;
				Integer cntM = ejb.getSearchCount(searchParams);
				searchParams.sex = Sex.W;
				Integer cntW = ejb.getSearchCount(searchParams);

				OdfTableRow row = table.getRowByIndex(1 + yearDelta);
				row.getCellByIndex(0).setDoubleValue(new Double(year));
				row.getCellByIndex(1).setDoubleValue(new Double(cntM + cntW));
				row.getCellByIndex(2).setDoubleValue(new Double(cntM));
				row.getCellByIndex(3).setDoubleValue(new Double(cntW));

				Calendar calFrom = Calendar.getInstance();
				calFrom.set(Calendar.MONTH, Calendar.SEPTEMBER);
				calFrom.set(Calendar.DAY_OF_MONTH, 1);
				calFrom.set(Calendar.YEAR, year);
				calFrom.add(Calendar.MONTH, -18);
				Date to = calFrom.getTime();

				calFrom.add(Calendar.MONTH, -18);
				Date from = calFrom.getTime();
				searchParams.birthday = DateFilter.build(from, to);

				//
				// System.out.println("birthdayTo: "+searchParams.birthdayTo);
				// System.out.println("birthdayFrom: "+searchParams.birthdayFrom);

				searchParams.orderStatus = OrderStatus.ACTIVE_QUEUE;
				searchParams.plainYear = new IntegerFilter(-1);
				searchParams.sex = Sex.M;
				Integer cntM18_36 = ejb.getSearchCount(searchParams);
				searchParams.sex = Sex.W;
				Integer cntW18_36 = ejb.getSearchCount(searchParams);

				/* 3,1 .. 4.0 */
				to = searchParams.birthday.getFilterValues().get(0);
				calFrom.add(Calendar.MONTH, -12);
				from = calFrom.getTime();
				searchParams.birthday = DateFilter.build(from, to);
				
				searchParams.sex = Sex.M;
				Integer cntM36_48 = ejb.getSearchCount(searchParams);
				searchParams.sex = Sex.W;
				Integer cntW36_48 = ejb.getSearchCount(searchParams);

				/* 4,1 .. 5.0 */
				to = searchParams.birthday.getFilterValues().get(0);
				calFrom.add(Calendar.MONTH, -12);
				from = calFrom.getTime();
				searchParams.birthday = DateFilter.build(from, to);
				
				searchParams.sex = Sex.M;
				Integer cntM48_60 = ejb.getSearchCount(searchParams);
				searchParams.sex = Sex.W;
				Integer cntW48_60 = ejb.getSearchCount(searchParams);

				/* 5,1 .. 6.0 */
				to = searchParams.birthday.getFilterValues().get(0);
				calFrom.add(Calendar.MONTH, -12);
				from = calFrom.getTime();
				searchParams.birthday = DateFilter.build(from, to);
				
				searchParams.sex = Sex.M;
				Integer cntM60_72 = ejb.getSearchCount(searchParams);
				searchParams.sex = Sex.W;
				Integer cntW60_72 = ejb.getSearchCount(searchParams);

				/* 6,1 .. 7.0 */
				to = searchParams.birthday.getFilterValues().get(0);
				calFrom.add(Calendar.MONTH, -12);
				from = calFrom.getTime();
				searchParams.birthday = DateFilter.build(from, to);
				
				searchParams.sex = Sex.M;
				Integer cntM72_84 = ejb.getSearchCount(searchParams);
				searchParams.sex = Sex.W;
				Integer cntW72_84 = ejb.getSearchCount(searchParams);

				row.getCellByIndex(4).setDoubleValue(
						new Double(cntM18_36 + cntW18_36 + cntM36_48
								+ cntW36_48 + cntM48_60 + cntW48_60 + cntM60_72
								+ cntW60_72 + cntM72_84 + cntW72_84));
				row.getCellByIndex(5).setDoubleValue(
						new Double(cntM18_36 + cntM36_48 + cntM48_60
								+ cntM60_72 + cntM72_84));
				row.getCellByIndex(6).setDoubleValue(
						new Double(cntW18_36 + cntW36_48 + cntW48_60
								+ cntW60_72 + cntW72_84));

				row.getCellByIndex(7).setDoubleValue(
						new Double(cntM18_36 + cntW18_36));
				row.getCellByIndex(8).setDoubleValue(new Double(cntM18_36));
				row.getCellByIndex(9).setDoubleValue(new Double(cntW18_36));

				row.getCellByIndex(10).setDoubleValue(
						new Double(cntM36_48 + cntW36_48));
				row.getCellByIndex(11).setDoubleValue(new Double(cntM36_48));
				row.getCellByIndex(12).setDoubleValue(new Double(cntW36_48));

				row.getCellByIndex(13).setDoubleValue(
						new Double(cntM48_60 + cntW48_60));
				row.getCellByIndex(14).setDoubleValue(new Double(cntM48_60));
				row.getCellByIndex(15).setDoubleValue(new Double(cntW48_60));

				row.getCellByIndex(16).setDoubleValue(
						new Double(cntM60_72 + cntW60_72));
				row.getCellByIndex(17).setDoubleValue(new Double(cntM60_72));
				row.getCellByIndex(18).setDoubleValue(new Double(cntW60_72));

				row.getCellByIndex(19).setDoubleValue(
						new Double(cntM72_84 + cntW72_84));
				row.getCellByIndex(20).setDoubleValue(new Double(cntM72_84));
				row.getCellByIndex(21).setDoubleValue(new Double(cntW72_84));
			}

			File tempFile = File.createTempFile("equeue", ".ods");
			doc.save(tempFile);
			doc.close();
			return tempFile;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

//	/**
//	 * @param table
//	 */
//	private void setColumnOptimalSize(OdfTable table) {
//		for (int i = 1; i < 14; i++)
//			table.getColumnByIndex(i).setUseOptimalWidth(true);
//	}

	
	
	// private static void copyfile(File src , File dst){
	// try{
	// InputStream in = new FileInputStream(src);
	//
	//
	// OutputStream out = new FileOutputStream(dst);
	//
	// byte[] buf = new byte[1024];
	// int len;
	// while ((len = in.read(buf)) > 0){
	// out.write(buf, 0, len);
	// }
	// in.close();
	// out.close();
	// System.out.println("File copied.");
	// }
	// catch(FileNotFoundException e){
	// e.printStackTrace();
	// }
	// catch(IOException e){
	// e.printStackTrace();
	// }
	// }
	//

}