package org.itx.sphinx.equeue.tuapse;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.sphinx.equeue.Row;
import org.itx.sphinx.equeue.XlsReaderBase;

import jxl.*; 
import jxl.read.biff.BiffException;

public class XlsReader extends XlsReaderBase {

//	private final DateFormat dateFormat= new SimpleDateFormat("dd.MM.yyyy");
	
	private String filePath;
	
	public XlsReader(String filePath){
		this.filePath = filePath;
	} 
	
	public List<Row> getData(){
		try {
			List<Row> result = new ArrayList<Row>(1000);
			Workbook workbook = Workbook.getWorkbook(new File(filePath));
			for (int sheetsIndex = 0; sheetsIndex < workbook.getNumberOfSheets(); sheetsIndex++) {
				Sheet sheet = workbook.getSheet(sheetsIndex);
				String sheetName = workbook.getSheetNames()[sheetsIndex];
				int rowsCount = sheet.getRows();
				System.err.println("There are "+(rowsCount-3)+" rows.");
				for (int i=3; i < rowsCount; i++ ) {
					 
					try{
					Cell[] cells = sheet.getRow(i);

					 Integer number = null;// getInteger(cells ,0);
					 Date regDate =     	 getDate  (cells ,0);
					 String secondName = 	 getString(cells ,2);
					 String firstName = 	 getString(cells ,3);
					 String patronymicName = getString(cells ,4);
					 
					 Integer birthdayDay = 		 	getInteger(cells ,5);
					 if(birthdayDay == null){
						 birthdayDay = 1;
					 }
					 Integer birthdayMonth = 		 getInteger(cells ,6);
					 if(birthdayMonth == null){
						 birthdayMonth = 1;
					 }
					 Integer birthdayYear = 		 getInteger(cells ,7);
					 if(birthdayYear == null){
						 birthdayYear = 1;
					 }
					 Calendar cal = Calendar.getInstance();
					 cal.set(Calendar.YEAR, birthdayYear);
					 cal.set(Calendar.MONTH, birthdayMonth);
					 cal.set(Calendar.DATE, birthdayDay);
					 Date birthday = cal.getTime();
					 
					 String addressCity = 		 getString(cells ,8);
					 String addressStreet = 		 getString(cells ,9);
					 String addressHome = 		 getString(cells ,10);
					 String addressApp = 		 getString(cells ,11);
					 
					 StringBuilder sb = new StringBuilder();
					 if(addressCity!= null && !addressCity.isEmpty() ){
						 sb.append(addressCity);
					 }
					 if(addressStreet!= null && !addressStreet.isEmpty() ){
						 sb.append(" ул. ");
						 sb.append(addressStreet);
					 }
					 if(addressHome!= null && !addressHome.isEmpty() ){
						 sb.append(" дом ");
						 sb.append(addressHome);
					 }
					 if(addressApp!= null && !addressApp.isEmpty() ){
						 sb.append(" кв. ");
						 sb.append(addressApp);
					 }
					 String address = sb.toString();

					 
					 
					 String privelegyS  = 	 getString(cells ,12);
					 if(privelegyS!=null){
						 privelegyS = privelegyS.trim();
						 if(privelegyS.isEmpty())
							 privelegyS = null;
					 }
					 
//					 Мд		 Оу
					 String mdOu  = 	 getString(cells ,13);
					 
//					 Решение О Приеме
					 String decisionAbout =     	 getString  (cells ,14);
					 
					
				
					 String dousS = getString(cells ,15 );
					 String[] split = dousS.split(",");
					 List<String> dous=new ArrayList<String>(split.length);
					 for (String s : split) {
						 s = s.trim();
						 if(!s.isEmpty())
							 dous.add(s);
					 }
//					 
//					 String sex = Sex.M;
//
//					 

					 Row row = new Row(number, dous, secondName,  firstName, patronymicName,  birthday, address, regDate, privelegyS);
					 StringBuilder sbComment = new StringBuilder();
					 if(addressApp!= null && !addressApp.isEmpty() ){
						 sbComment.append("Решение О Приеме: ");
						 sbComment.append( decisionAbout);
					 }
					 
					 if(addressApp!= null && !addressApp.isEmpty() ){
						 sbComment.append("Мд Оу: ");
						 sbComment.append( mdOu);
					 }
					 row.setComments(sbComment.toString());
//					 row.setPhone(phone);
//					 row.setBsDate(bsDate);
//					 row.setBsNumber(bsNumber);
//					 row.setBsSeria(bsSeria);
//					 row.setOldRn(oldRN);
//					 row.setSverkDate(sverkDate);
//					 row.setSex(sex);
//					 row.setPlainYear(plainYear);
					 result.add(row);
					}catch(Exception e){
						System.err.println("Error processing record "+(i+1) + " sheetsIndex: "+sheetsIndex + "  sheetName: " + sheetName);
						e.printStackTrace();
						return null;
					}
				}
			}
			return result;
		} catch (BiffException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} 
	}


	

	
	
	public static void main(String [] argv){
		List<Row> data = new XlsReader("tuapse1.xls").getData();
		int i=1;
		for (Row row : data) {
			System.err.println("=============== "+i++ +" ===============");
			System.err.println(row);
		}
		
	}
}
