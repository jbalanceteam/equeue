package org.itx.sphinx.equeue.labinsk;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.itx.sphinx.equeue.Row;
import org.itx.sphinx.equeue.XlsReaderBase;

import jxl.*; 
import jxl.read.biff.BiffException;

public class XlsReader extends XlsReaderBase {

	
	private String filePath;
	
	public XlsReader(String filePath){
		this.filePath = filePath;
	} 
	
	public List<Row> getData(){
		List<Row> result = new ArrayList<Row>(1000);
		Workbook workbook = null;
		try {
			
			workbook = Workbook.getWorkbook(new File(filePath));
			Sheet sheet = workbook.getSheet(0);
			int rowsCount = sheet.getRows();
			System.err.println("There are "+(rowsCount-1)+" rows.");
			for (int i=1; i < rowsCount; i++ ) {
				 
				try{
				Cell[] cells = sheet.getRow(i);

				 Integer number  = getInteger(cells ,0);
				 Date regDate =    getDate  (cells ,1);
				 
				 String fio = getString(cells ,2);
				 StringBuffer sb = new StringBuffer();
				 for (int j = 0; j < fio.length(); j++) {
						char charAt = fio.charAt(j);
//						System.out.println(j+" " + (j!=0));
						if(j!=0 && Character.isUpperCase(charAt) && fio.charAt(j-1) != ' '){
							sb.append(' ');
						}
						sb.append(charAt);
				 }
				 String[] splitFio = sb.toString().split(" ");
				 String secondName = splitFio[0];	
				 String	firstName = splitFio.length>1 ?  splitFio[1]: null;
				 String patronymicName = splitFio.length>2 ?  splitFio[2]: null;
				 
				 Date birthday = 		 getDate  (cells ,3);
				 
				 
				 
//				 тут гемор с садиками...
//				 начальный индекс колонок с садами
				 int startIndex = 4; 
//				 в такй последовательности идут сады в файле
				 int dousIds[] = {1, 3, 4, 5, 6, 7, 8, 10, 11, 15, 17, 18, 19, 20, 21, 22, 26, 300, 400, 600, 700, 800, 1000, 1200, 1300, 1400, 1900, 2100, 3400};
				 List<String> dous =new ArrayList<String>();
				 for (int j = 0; j < dousIds.length; j++) {
					 int id = dousIds[j];
					 Integer flag  = getInteger(cells ,startIndex + j);
					 if(flag != null){
						 dous.add(id + "");
					 }
				}
				 
				 int endIndex = startIndex + dousIds.length; 
				 String father = getString(cells, endIndex + 0);
				 String mother = getString(cells, endIndex + 1);
				 String address = getString(cells, endIndex + 2);
				 String phone = getString(cells, endIndex + 3);
				 String privelegyS = getString(cells, endIndex + 4);
				 
				
				 String comment = 		"ФИО отца, место работы: " + father + " \n ФИО матери, место работы: "+mother;

				 
				 Row row = new Row(number, dous, secondName,  firstName, patronymicName,  birthday, address, regDate, privelegyS);
				 row.setComments(comment);
				 row.setPhone(phone);
				 result.add(row);
				}catch(Exception e){
					System.err.println("Error processing record "+i);
					e.printStackTrace();
					return null;
				}
			}
			return result;
		} catch (BiffException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(workbook!=null)
				workbook.close();
		}
	}


	

	
	
	public static void main(String [] argv){
		List<Row> data = new XlsReader("labinsk1.xls").getData();
		int i=1;
		for (Row row : data) {
			System.err.println("=============== "+i++ +" ===============");
			System.err.println(row);
		}
		
	}
}
