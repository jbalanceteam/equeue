package org.itx.sphinx.equeue;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import jxl.Cell;
import jxl.CellType;
import jxl.DateCell;
import jxl.biff.EmptyCell;
import jxl.read.biff.BlankCell;

public class XlsReaderBase {
	protected Integer getInteger(Cell[] cells, int col) {
		if(cells.length<=col)
			return null;
		String string = getString(cells,col);
		if(string == null || string.isEmpty())
			return null;
		return new Integer(string.trim());
	}
	

	protected String getString(Cell[] cells, int col) {
		if(cells.length<=col)
			return null;
		Cell stringCell = cells[col];
		return stringCell.getContents();
	}

	protected Date getDate(Cell[] cells, int col) {
		if(cells.length<=col)
			return null;
		 Cell cell = cells[col];
		 if(cell instanceof EmptyCell)
			 return null;
		 if(cell instanceof BlankCell)
			 return null;
		 
		 if(CellType.EMPTY.equals(cell .getType() ))
			 return null;
		 
		 
		 
		 if(! (cell  instanceof DateCell)){
			 final String contents = cell.getContents();
			 return parseDate(contents);
			 
//			 throw new RuntimeException("Contents: "+ contents + "   Type: "+ cell.getType());
		 }
			 
		 DateCell dateCell= (DateCell) cell;
		 return dateCell.getDate();
	}
	
	DateFormat dfs[] =  {new SimpleDateFormat("dd.MM.yy"),
			new SimpleDateFormat("dd.MM.yyyy"),
			new SimpleDateFormat("yyyy-MM-dd"),
			new SimpleDateFormat("dd.MM.yy HH:MM")};
	
	Date parseDate(String str){
		for (DateFormat  df : dfs) {
			try {
				return df.parse(str);
			} catch(Exception e){}
		}
		throw new RuntimeException("Неверный формат даты  " + str + " попробуйте ДД.ММ.ГГГГ");
	}
	
}
