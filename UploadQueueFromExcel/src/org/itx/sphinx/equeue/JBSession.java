package org.itx.sphinx.equeue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.NamingException;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.d.HStaffDivisionsRemote;
import org.itx.jbalance.l1.o.BirthSertificatesRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.PhysicalsRemote;
import org.itx.jbalance.l1.o.PrivilegesRemote;
import org.itx.jbalance.l1.o.RegionsRemote;
/**
 * Класс который работает с уже распарсенными и провалидированными данными.
 * Просто заливает их в систему.
 * 
 * @author apv
 */
public class JBSession {
	
	Map<String, Dou> dous ;
	Map<String,Privilege> privilege;
	JBClient client = new JBClient();
	
	public void upLoad(List<Row> rows) {
		try {
			for (Row row : rows) {
				loadOne(row);
			}
		} catch (NamingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
		
	
	public Set<String>isThereMissedDou(Set<String> forCheck){
		Map<String, Dou> dousMap;
		try {
			dousMap = getDous();
		} catch (NamingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		Set<String> res=new HashSet<String>();
		for (String dId : forCheck) {
			if(!dousMap.containsKey(dId)){
				res.add(dId);
			}
		}
		return res;
	}
	
	
	/**
	 * Метод для валидаци. Ексель файла.
	 * Проверяет наличие ДОУ в системке по номеру
	 * @param forCheck
	 * @return
	 */
	public Set<String>isThereMissedPriveleges(Set<String> forCheck){
		try {
			Set<String> res=new HashSet<String>();
			for (String name : forCheck) {
				if(name!=null && !name.isEmpty() && getPrivelege(name) == null){
					res.add(name);
				}
			}
			return res;
		} catch (NamingException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	
	public void createPrivilege(String name, String description) throws NamingException{
		PrivilegesRemote privilegesRemote = client.getPrivilegesRemote();
		Privilege object = new Privilege();
		object.setName(name);
		object.setDescription(description);
		privilegesRemote.create(object);
		
	}
	
	public void createDou(String num) throws NamingException{
		RegionsRemote regionsRemote = client.getRegionsRemote();
		List<? extends Region> all = regionsRemote.getAll(Region.class);
		if(all.isEmpty()){
			throw new RuntimeException("Regions mustn't be empty");
		}
		
		DousRemote service = client.getDou();
		HStaffDivisionsRemote staffDivision = client.getStaffDivision();
		
		Dou p = new Dou();
		p.setName("Садик №"+num);
		p.setNumber(num);
		p.setDescription("Водятся чертята");
		p.setEMail("sad"+num+"@edu.ru");
		p.setOKPO(123l);
		p.setINN("123");
		p.setJAddress("Б.Садовая "+num);
		
		p.setRegion(all.get(0));
		p = service.create(p);
		
		
		HStaffDivision object = new HStaffDivision();
		object.setDou(p);
		object.setFoundation("");
		object.setInfo("");
		staffDivision.create(object);
		
	}
	
	
	/**
	 * Залить одну строку
	 * @param row
	 * @throws NamingException
	 */
	private void loadOne(Row row) throws NamingException{
		System.err.println("======== Create One ====== "+ row);
		HDouRequestsRemote doc_l1 = client.getHDouRequest();

		Physical child = new Physical();
		child.setName(row.getFirstName());
		child.setRealName(row.getFirstName());
		child.setSurname(row.getSecondName());
		child.setPatronymic(row.getPatronymicName());
		child.setBirthday(row.getBirthday());
		child.setPAddress(row.getAddress());
		child.setPhone(row.getPhone());

		PhysicalsRemote physicalsEjb = client.getPhysicals();
		child =physicalsEjb.create(child);

		
		HDouRequest douRequest=new HDouRequest();
		douRequest.setChild(child);
		douRequest.setAddress(row.getAddress());
		String comments = "Загружено из excel файла. ";
		comments = comments + (row.getComments()==null?"":row.getComments());
		System.out.println("comments: "+comments);
		douRequest.setComments(comments);
		douRequest.setFoundation("Основание");
		douRequest.setRegDate(row.getRegDate());
//		douRequest.setDnumber(row.getNumber().longValue());
		douRequest.setPrivilege(getPrivelege(row.getPrivelegy()));
		douRequest.setRecordColor(row.getColor());
		douRequest.setOldNumber(row.getOldRN());
		douRequest.setYear(row.getPlainYear());
		douRequest.setSverkDate(row.getSverkDate());
		
		BirthSertificate birthSertificate=new BirthSertificate();
		birthSertificate.setSex(row.getSex() == null ? Sex.M: row.getSex());
		birthSertificate.setSeria(row.getBsSeria());
		birthSertificate.setNumber(row.getBsNumber());
		birthSertificate.setDate(row.getBsDate());
		birthSertificate.setName("свидетельство о рождении");
		birthSertificate.setGuardian1(row.getBsGuardian1());
		BirthSertificatesRemote birthSertificatesRemote = client.getBirthSertificates();
		birthSertificate = birthSertificatesRemote.create(birthSertificate);
		
		douRequest.setBirthSertificate(birthSertificate);
		douRequest = doc_l1.create(douRequest);

		List<String> dousIds = row.getDous();
		for (String douNumber : dousIds) {
			SDouRequest sLine = new SDouRequest();
			Dou contractor = getDous().get(douNumber);
			if(contractor==null){
				System.err.println("Не найден сад с номером "+douNumber);
				continue;
			}
			sLine .setArticleUnit(contractor);
			sLine .setRating(SDouRequest.RATING_MIDDLE);
			doc_l1.createSpecificationLine(sLine,douRequest);
		}
	}


	private Privilege getPrivelege(String name) throws NamingException {
		if(name==null || name.isEmpty())
			return null;
		Map<String, Privilege> map = getPrivilege();
//		пробем найти точное совпадение
		if(map.containsKey(name))
			return map.get(name);
//		поиск без учета регистра
		for(String key: map.keySet()){
			if(key.equalsIgnoreCase(name)){
				return map.get(key);
			}
		}
		return null;
	}

	
	/**
	 * Возвращает все ДОУ системы
	 * Ключ - номер ДОУ, значение - ДОУ
	 * @return
	 * @throws NamingException
	 */
	public Map<String,Dou>getDous() throws NamingException{
		if(dous == null){
			DousRemote douEjb = client.getDou();
			List<? extends Dou> all = douEjb.getAll(Dou.class);
			dous =  new HashMap<String, Dou>(all.size()); 
			for (Dou dou : all) {
				dous.put(dou.getNumber(), dou);
			}
		}
		
		return dous;
	}
	
	/**
	 * Ключ - имя льготы ('Инвалид','Работник ДОУ', и т.д.)
	 * @return Все льготы системы
	 * @throws NamingException
	 */
	Map<String,Privilege>getPrivilege() throws NamingException{
		if(privilege == null){
			PrivilegesRemote privilegesEjb = client.getPrivilegesRemote();
			List<? extends Privilege> all = privilegesEjb.getAll(Privilege.class);
			privilege =  new HashMap<String, Privilege>(all.size()); 
			for (Privilege p : all) {
				privilege.put(p.getName(), p);
			}
		}
		
		return privilege;
	}
	
	
	
}
