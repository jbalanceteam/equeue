package org.itx.sphinx.equeue.krylovskaya;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.NamingException;

import org.itx.jbalance.l0.h.RecordColor;
import org.itx.sphinx.equeue.Loader;
import org.itx.sphinx.equeue.Row;
import org.itx.sphinx.equeue.JBSession;

public class LoaderBean implements Loader{
	
	@Override
	public  void load(String filename){
		System.err.println("Link your belts.... Go ahead!");
		List<Row> data = new XlsReader(filename).getData();
		JBSession loader = new JBSession();
		
		System.err.println("Check: are there any missed DOU?");
		
		
		
		Set<String> privelegesForCheck=new HashSet<String>();
		for (Row row : data) {
			if(row.getPrivelegy()==null ||row.getPrivelegy().isEmpty())
				continue;
			privelegesForCheck.add(row.getPrivelegy());
		}
		Set<String> privilegesCheckRes = loader.isThereMissedPriveleges(privelegesForCheck);
		if(!privilegesCheckRes.isEmpty()){
			System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.err.println("Following privilege's IDs are not exist: ");
			int j=0;
			for (String p : privilegesCheckRes) {
				System.err.println(++j + ":\t " + p + "\n lines: ");
				for (int i = 0 ; i<data.size();i++) {
					if(data.equals(data.get(i).getPrivelegy())){
						System.err.println(i+" ");
					}
				}
				try {
					loader.createPrivilege(p, "Cоздана автоматически при импорте из экселя");
				} catch (NamingException e) {
					throw new RuntimeException(e);
				}
				
			}
			System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
		
		
		
		System.err.println("Check: are there any missed DOU?");
		Set<String> forCheck=new HashSet<String>();
		for (Row row : data) {
			forCheck.addAll(row.getDous());
		}
		Set<String> checkRes = loader.isThereMissedDou(forCheck);
		if(!checkRes.isEmpty()){
			System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.err.println("Following dou's IDs are not exist: ");
			System.err.println(checkRes);
			for (String num : checkRes) {
				try {
					loader.createDou(num);
				} catch (NamingException e) {
					throw new RuntimeException(e);
				}
			}
			System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

		}
		
		
		Collections.sort(data, new Comparator<Row>() {

			@Override
			public int compare(Row o1, Row o2) {
				int res =  o1.getRegDate().compareTo(o2.getRegDate());
				if(res == 0){
					res = o1.getNumber().compareTo(o2.getNumber());
				}
				return res;
			}
			
		});
		
		
		for (Row row : data) {
			if(row.getDous() == null || row.getDous().isEmpty()){
				row.setColor(RecordColor.RED);
				String s = row.getComments()==null?"":row.getComments();
				row.setComments("ОШИБКА: Список ДОУ пуст! \n" +  s);
				System.err.println(row.getComments());
			}
			
			
			for (Row row1 : data) {
				if(row1 == row)
					continue;
				if(row1.getBirthday()!=null && row1.getBirthday().equals(row.getBirthday()) && 
						row1.getFirstName()!=null && row1.getFirstName().equals(row.getFirstName()) && 
						row1.getSecondName()!=null && row1.getSecondName().equals(row.getSecondName()) && 
						row1.getPatronymicName()!=null && row1.getPatronymicName().equals(row.getPatronymicName()) 
				){
					row.setColor(RecordColor.RED);
					String s = row.getComments()==null?"":row.getComments();

					row.setComments("ОШИБКА: ДУБЛИКАТ! \n" +  s);
					System.err.println( row.getComments());
				}
			}
			
//			if(row.getDous() == null || row.getDous().isEmpty()){
//				row.setColor(RecordColor.RED);
//				row.setComments("ОШИБКА: Список ДОУ пуст! \n" +  row.getComments()==null?"":row.getComments());
//			}
		}
		
//		for (Row r : data) {
//			System.err.println(r);
//		}
		loader = new JBSession();
		
		loader.upLoad(data);
	}
}
