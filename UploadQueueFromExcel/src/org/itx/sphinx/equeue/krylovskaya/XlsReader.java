package org.itx.sphinx.equeue.krylovskaya;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.itx.sphinx.equeue.Row;
import org.itx.sphinx.equeue.XlsReaderBase;

import jxl.*; 
import jxl.read.biff.BiffException;

public class XlsReader extends XlsReaderBase {

	
	private String filePath;
	
	public XlsReader(String filePath){
		this.filePath = filePath;
	} 
	
	public List<Row> getData(){
		List<Row> result = new ArrayList<Row>(1000);
		Workbook workbook = null;
		try {
			
			workbook = Workbook.getWorkbook(new File(filePath));
			Sheet sheet = workbook.getSheet(0);
			int rowsCount = sheet.getRows();
			System.err.println("There are "+(rowsCount-1)+" rows.");
			for (int i=1; i < rowsCount; i++ ) {
				 
				try{
				Cell[] cells = sheet.getRow(i);

				 Integer number  = getInteger(cells ,0);
				 Date regDate =     	 getDate  (cells ,1);
				 
				 String fio = getString(cells ,2);
				 StringBuffer sb = new StringBuffer();
				 for (int j = 0; j < fio.length(); j++) {
						char charAt = fio.charAt(j);
//						System.out.println(j+" " + (j!=0));
						if(j!=0 && Character.isUpperCase(charAt) && fio.charAt(j-1) != ' '){
							sb.append(' ');
						}
						sb.append(charAt);
				 }
				 String[] splitFio = sb.toString().split(" ");
				 String secondName = splitFio[0];	
				 String	firstName = splitFio[1];
				 String patronymicName = splitFio.length>2 ?  splitFio[2]: null;
				 
				 Date birthday = 		 getDate  (cells ,3);
				 
				 String dousS = getString(cells ,4);
				 List<String> dous = null;
				 if(!dousS.trim().isEmpty()){
					 String[] split = dousS.split(",");
					 dous=new ArrayList<String>(split.length);
					 for (String s : split) {
						 s = s.trim();
						 if(!s.isEmpty())
						 	dous.add(s);
					 }
				 }
				 
				 String privelegyS = 	 getString(cells ,5);
				 if(privelegyS!=null && privelegyS.trim().isEmpty()){
					 privelegyS = null;
				 }
				 
				 if(privelegyS!=null){
					 privelegyS = privelegyS.trim().toLowerCase();
				 }
					 
				 String address = 		 null;

				 
				 Row row = new Row(number, dous, secondName,  firstName, patronymicName,  birthday, address, regDate, privelegyS);
				 result.add(row);
				}catch(Exception e){
					System.err.println("Error processing record "+i);
					e.printStackTrace();
					return null;
				}
			}
			return result;
		} catch (BiffException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(workbook!=null)
				workbook.close();
		}
	}


	

	
	
	public static void main(String [] argv){
		List<Row> data = new XlsReader("krylovskaya.xls").getData();
		int i=1;
		for (Row row : data) {
			System.err.println("=============== "+i++ +" ===============");
			System.err.println(row);
		}
		
	}
}
