package org.itx.sphinx.equeue;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginContext;

import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.d.HRegisterRemote;
import org.itx.jbalance.l1.d.HStaffDivisionsRemote;
import org.itx.jbalance.l1.o.BirthSertificatesRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.PhysicalsRemote;
import org.itx.jbalance.l1.o.PrivilegesRemote;
import org.itx.jbalance.l1.o.RegionsRemote;
import org.itx.jbalance.l1.o.StaffDivisionsRemote;
//import org.itx.jbalance.l1.session.Sessionprofile;
//import org.itx.jbalance.l1.session.SessionprofileRemote;
import org.jboss.security.auth.callback.UsernamePasswordHandler;

public class JBClient {
	
	private DousRemote dousRemote;
	private PhysicalsRemote physicalsRemote;
	private HRegisterRemote hRegisterRemote;
	private HDouRequestsRemote douRequestsRemote;
	private BirthSertificatesRemote birthSertificatesRemote;
	private StaffDivisionsRemote staffDivisionsRemote;
	private PrivilegesRemote privilegesRemote;
	private RegionsRemote regionsRemote;
	
	protected final RegionsRemote getRegionsRemote() throws NamingException {
		if(regionsRemote!=null)
			return regionsRemote;
		Context context = getContext();
		regionsRemote = (RegionsRemote ) context.lookup(Constants.JNDI_REGIONS_REMOTE);
		return regionsRemote;
	}
	
	public final DousRemote getDou() throws NamingException {
		
		if(dousRemote!=null)
			return dousRemote;
		Context context = getContext();
		dousRemote = (DousRemote) context.lookup("Dous/remote");
		return dousRemote;
	}
	
	
	public final PhysicalsRemote getPhysicals() throws NamingException {
		if(physicalsRemote!=null)
			return physicalsRemote;
		Context context = getContext();
		physicalsRemote = (PhysicalsRemote) context.lookup("Physicals/remote");
		return physicalsRemote;
	}
	
	
	public final HRegisterRemote getHRegisterRemote() throws NamingException {
		if(hRegisterRemote!=null)
			return hRegisterRemote;
		Context context = getContext();
		hRegisterRemote = (HRegisterRemote) context.lookup(Constants.JNDI_HREGISTER_REMOTE);
		return hRegisterRemote;
	}
	
	public final HDouRequestsRemote getHDouRequest() throws NamingException {
		if(douRequestsRemote!=null)
			return douRequestsRemote;
		Context context = getContext();
		douRequestsRemote = (HDouRequestsRemote) context.lookup(Constants.JNDI_HDOUREQUESTS_REMOTE);
		return douRequestsRemote;
	}

	
	public final StaffDivisionsRemote getStaffDivisions() throws NamingException {
		if(staffDivisionsRemote!=null)
			return staffDivisionsRemote;
		Context context = getContext();
		staffDivisionsRemote = (StaffDivisionsRemote) context.lookup(Constants.JNDI_STAFFDIVISIONS_REMOTE);
		return staffDivisionsRemote;
	}

	
	public final  PrivilegesRemote getPrivilegesRemote() throws NamingException {
		if(privilegesRemote!=null)
			return privilegesRemote;
		Context context=getContext();
		privilegesRemote = (PrivilegesRemote) context.lookup(Constants.JNDI_PRIVILEGES_REMOTE);
		return privilegesRemote;
	}
	
	public final HStaffDivisionsRemote getStaffDivision() throws NamingException {
		Context context = getContext();
		HStaffDivisionsRemote HStaffDivisionss = (HStaffDivisionsRemote) context.lookup(Constants.JNDI_HSTAFFDIVISIONS_REMOTE);
		return HStaffDivisionss;
	}
	
	
	public final BirthSertificatesRemote getBirthSertificates() throws NamingException {
		if(birthSertificatesRemote!=null)
			return birthSertificatesRemote;
		Context context = getContext();
		birthSertificatesRemote = (BirthSertificatesRemote) context.lookup(Constants.JNDI_BIRTHSERTIFICATES_REMOTE);
		return birthSertificatesRemote;
	}

	private final Context getContext() throws NamingException {
		System.out.println("Try to lookup JB context");
		Hashtable<String,String> hashtable = new Hashtable<String,String>();

		
		UsernamePasswordHandler handler = new UsernamePasswordHandler("admin", "11".toCharArray());
		System.setProperty("java.security.auth.login.config", 
				"jaas.config");
		try {
			LoginContext lc = new LoginContext("client-login", handler);
			lc.login();
		} catch (Exception e) {
			// TODO: handle exception
		}
        hashtable.put("java.naming.factory.url.pkgs",
            "org.jboss.naming rg.jnp.interfaces");
 		hashtable.put("java.naming.factory.initial","org.jnp.interfaces.NamingContextFactory");
		hashtable.put("java.naming.provider.url","jnp://localhost:1099");
		hashtable.put("java.naming.factory.url.pkgs","org.jnp.interfaces");
		Context context=new InitialContext(hashtable);
		return context;
	}
}
