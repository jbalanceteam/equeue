package org.itx.sphinx.equeue.gelendjik;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.itx.sphinx.equeue.Row;
import org.itx.sphinx.equeue.XlsReaderBase;

import jxl.*; 
import jxl.read.biff.BiffException;

public class XlsReader extends XlsReaderBase {

	
	private String filePath;
	
	public XlsReader(String filePath){
		this.filePath = filePath;
	} 
	
	public List<Row> getData(){
		List<Row> result = new ArrayList<Row>(1000);
		Workbook workbook = null;
		try {
			
			workbook = Workbook.getWorkbook(new File(filePath));
			Sheet sheet = workbook.getSheet(0);
			int rowsCount = sheet.getRows();
			System.err.println("There are "+(rowsCount-1)+" rows.");
			for (int i=1; i < rowsCount; i++ ) {
				 
				try{
				Cell[] cells = sheet.getRow(i);

				 Integer number = null ; //getInteger(cells ,1);
//				 Все, что есть
				 List<String> dous= null;
				
				 
				 String secondName = 	 getString(cells ,1);
				 String firstName = 	 getString(cells ,2);
				 Date birthday = 		 getDate  (cells ,3);
				 if(birthday==null)
					 birthday =  new Date();
				 
				 String patronymicName =  null ;//getString(cells ,5);
				 
				 String address = 		 getString(cells ,4);
				 
				 Date regDate =     	 getDate  (cells ,5);
				 String privelegyS = 	 getString(cells ,6);

				 
				 Row row = new Row(number, dous, secondName,  firstName, patronymicName,  birthday, address, regDate, privelegyS);
				 result.add(row);
				}catch(Exception e){
					System.err.println("Error processing record "+i);
					e.printStackTrace();
					return null;
				}
			}
			return result;
		} catch (BiffException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			if(workbook!=null)
				workbook.close();
		}
	}


	

	
	
	public static void main(String [] argv){
		List<Row> data = new XlsReader("gelendjik.xls").getData();
		int i=1;
		for (Row row : data) {
			System.err.println("=============== "+i++ +" ===============");
			System.err.println(row);
		}
		
	}
}
