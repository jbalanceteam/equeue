package org.itx.sphinx.equeue.gelendjik;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.NamingException;

import org.itx.sphinx.equeue.Loader;
import org.itx.sphinx.equeue.Row;
import org.itx.sphinx.equeue.JBSession;

public class LoaderBean implements Loader{
	
	@Override
	public  void load(String filename){
		System.err.println("Link your belts.... Go ahead!");
		List<Row> data = new XlsReader(filename).getData();
		JBSession loader = new JBSession();
		
		System.err.println("Check: are there any missed DOU?");
		
		
		
		Set<String> privelegesForCheck=new HashSet<String>();
		for (Row row : data) {
			if(row.getPrivelegy()==null ||row.getPrivelegy().isEmpty())
				continue;
			privelegesForCheck.add(row.getPrivelegy());
		}
		Set<String> privilegesCheckRes = loader.isThereMissedPriveleges(privelegesForCheck);
		if(!privilegesCheckRes.isEmpty()){
			System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.err.println("Following privilege's IDs are not exist: ");
			int j=0;
			for (String p : privilegesCheckRes) {
				System.err.println(++j + ":\t " + p + "\n lines: ");
				for (int i = 0 ; i<data.size();i++) {
					if(data.equals(data.get(i).getPrivelegy())){
						System.err.println(i+" ");
					}
				}
			}
			System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.err.println("Exit");
			System.exit(1);
		}
		
		
		Set<String> keySet = null;
		try {
			keySet = loader.getDous().keySet();
			for (Row row : data) {
				row.getDous().addAll(keySet);
			}
		} catch (NamingException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		
		Collections.sort(data, new Comparator<Row>() {

			@Override
			public int compare(Row o1, Row o2) {
				return o1.getRegDate().compareTo(o2.getRegDate());
			}
			
		});
		
//		for (Row r : data) {
//			System.err.println(r);
//		}
		
		
		loader.upLoad(data);
	}
}
