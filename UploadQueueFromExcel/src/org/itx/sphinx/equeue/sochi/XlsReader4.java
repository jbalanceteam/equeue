package org.itx.sphinx.equeue.sochi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.itx.sphinx.equeue.Row;
import org.itx.sphinx.equeue.XlsReaderBase;

import jxl.*; 
import jxl.read.biff.BiffException;

public class XlsReader4 extends XlsReaderBase {

//	private final DateFormat dateFormat= new SimpleDateFormat("dd.MM.yyyy");
	
	private String filePath;
	
	public XlsReader4(String filePath){
		this.filePath = filePath;
	} 
	
	public List<Row> getData(){
		try {
			List<Row> result = new ArrayList<Row>(1000);
			Workbook workbook = Workbook.getWorkbook(new File(filePath));
			Sheet sheet = workbook.getSheet(0);
			int rowsCount = sheet.getRows();
			System.err.println("There are "+(rowsCount-1)+" rows.");
			for (int i=1; i < rowsCount; i++ ) {
				 
				try{
				Cell[] cells = sheet.getRow(i);

				 Integer number = getInteger(cells ,0);
				 Date regDate =     	 getDate  (cells ,1);
				 String secondName = 	 getString(cells ,2);
				 String firstName = 	 getString(cells ,3);
				 String patronymicName = getString(cells ,4);
				 
				 Date birthday = 		 getDate  (cells ,5);
				 String address = 		 getString(cells ,6);
				 String privelegyS = 	 getString(cells ,7);
				 
				 privelegyS = privelegyS.trim();
				 if(privelegyS.isEmpty())
					 privelegyS = null;
				 
				 String dousS = getString(cells ,8);
				 String[] split = dousS.split(",");
				 List<String> dous=new ArrayList<String>(split.length);
				 for (String s : split) {
					 s = s.trim();
					 dous.add(s);
				 }
				 
				 
				

				 String comments = getString(cells ,9);
				 
				 

				 
				 Row row = new Row(number, dous, secondName,  firstName, patronymicName,  birthday, address, regDate, privelegyS);
				 row.setComments(comments);
				 result.add(row);
				}catch(Exception e){
					System.err.println("Error processing record "+i);
					e.printStackTrace();
					return null;
				}
			}
			return result;
		} catch (BiffException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} 
	}


	

	
	
	public static void main(String [] argv){
		List<Row> data = new XlsReader4("sochi5.xls").getData();
		int i=1;
		for (Row row : data) {
			System.err.println("=============== "+i++ +" ===============");
			System.err.println(row);
		}
		
	}
}
