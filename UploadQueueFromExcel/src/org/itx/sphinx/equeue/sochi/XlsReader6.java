package org.itx.sphinx.equeue.sochi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.sphinx.equeue.Row;
import org.itx.sphinx.equeue.XlsReaderBase;

import jxl.*; 
import jxl.read.biff.BiffException;

public class XlsReader6 extends XlsReaderBase {

//	private final DateFormat dateFormat= new SimpleDateFormat("dd.MM.yyyy");
	
	private String filePath;
	
	public XlsReader6(String filePath){
		this.filePath = filePath;
	} 
	
	public List<Row> getData(){
		try {
			List<Row> result = new ArrayList<Row>(1000);
			Workbook workbook = Workbook.getWorkbook(new File(filePath));
			Sheet sheet = workbook.getSheet(0);
			int rowsCount = sheet.getRows();
			System.err.println("There are "+(rowsCount-1)+" rows.");
			for (int i=1; i < rowsCount; i++ ) {
				 
				try{
				Cell[] cells = sheet.getRow(i);

				 Integer number = getInteger(cells ,0);
				 String oldRN =     	 getString  (cells ,2);
				 Date regDate =     	 getDate  (cells ,3);
				 if(regDate == null){
					 throw new RuntimeException("В строке "+(i-1)+" не заполнена дата РН");
				 }
					 
				 
				 Date sverkDate = 	 getDate(cells ,4);
				 String secondName = 	 getString(cells ,5);
				 String firstName = 	 getString(cells ,6);
				 String patronymicName = getString(cells ,7);
				 
				 String dousS = getString(cells ,8);
				 String[] split = dousS.split(",");
				 List<String> dous=new ArrayList<String>(split.length);
				 for (String s : split) {
					 s = s.trim();
					 if(!s.isEmpty())
						 dous.add(s);
				 }
				 
				 String sexS = getString(cells ,10);
				 Sex sex = null;
				 if(sexS!=null && !sexS.isEmpty()){
					 if(sexS.equalsIgnoreCase("м")){
						 sexS = "M";
					 }
					 sex = Sex.valueOf(sexS);
				 }
				 
				 Date birthday = 		 getDate  (cells ,11);
				 String bsSeria = 	 getString(cells ,12);
				 String bsNumber = 	 getString(cells ,13);
				 Date bsDate = 	 getDate(cells ,14);
				 String address = 		 getString(cells ,15);
				 
				 String phone = 	 getString(cells ,16);
				 
				 String privelegyS  = 	 getString(cells ,17);
				 if(privelegyS!=null){
					 privelegyS = privelegyS.trim();
					 if(privelegyS.isEmpty())
						 privelegyS = null;
				 }
				 
				 
				 
				 
				

				 Integer plainYear = getInteger(cells ,18);
				 
				 

				 
				 Row row = new Row(number, dous, secondName,  firstName, patronymicName,  birthday, address, regDate, privelegyS);
//				 row.setComments(comments);
				 row.setPhone(phone);
				 row.setBsDate(bsDate);
				 row.setBsNumber(bsNumber);
				 row.setBsSeria(bsSeria);
				 row.setOldRn(oldRN);
				 row.setSverkDate(sverkDate);
				 row.setSex(sex);
				 row.setPlainYear(plainYear);
				 result.add(row);
				}catch(Exception e){
					System.err.println("Error processing record "+i);
					e.printStackTrace();
					return null;
				}
			}
			return result;
		} catch (BiffException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} 
	}


	

	
	
	public static void main(String [] argv){
		List<Row> data = new XlsReader6("sochi7.xls").getData();
		int i=1;
		for (Row row : data) {
			System.err.println("=============== "+i++ +" ===============");
			System.err.println(row);
		}
		
	}
}
