package org.itx.sphinx.equeue.sochi;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.itx.sphinx.equeue.Loader;
import org.itx.sphinx.equeue.Row;
import org.itx.sphinx.equeue.JBSession;

public class LoaderBean implements Loader{
	
	@Override
	public  void load(String filename){
		System.err.println("Link your belts.... Go ahead!");
		List<Row> data = new XlsReader6(filename).getData();
		JBSession loader = new JBSession();
		
		System.err.println("Check: are there any missed DOU?");
		Set<String> forCheck=new HashSet<String>();
		for (Row row : data) {
			forCheck.addAll(row.getDous());
		}
		Set<String> checkRes = loader.isThereMissedDou(forCheck);
		if(!checkRes.isEmpty()){
			System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.err.println("Following dou's IDs are not exist: ");
			System.err.println(checkRes);
//			int j=0;
//			for (Integer p : checkRes) {
//				System.err.print(++j + ":\t " + p + "\n lines:[");
//				for (int i = 0 ; i < data.size();i++) {
//					Row row = data.get(i);
//					List<Integer> dous = row.getDous();
//					if(dous.contains(p)){
//						System.err.print(i+" ");
//					}
//				}
//				System.err.println("]");
//			}
			System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.err.println("Exit");
			System.exit(1);
		}
		
		
		Set<String> privelegesForCheck=new HashSet<String>();
		for (Row row : data) {
			if(row.getPrivelegy()!=null && !row.getPrivelegy().isEmpty())
				privelegesForCheck.add(row.getPrivelegy());
		}
		Set<String> privilegesCheckRes = loader.isThereMissedPriveleges(privelegesForCheck);
		if(!privilegesCheckRes.isEmpty()){
			System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.err.println("Following privileges are not exist: ");
			int j=0;
			for (String p : privilegesCheckRes) {
				System.err.println(++j + ":\t '" + p + "'\n lines: ");
				for (int i = 0 ; i<data.size();i++) {
					if(p.equals(data.get(i).getPrivelegy())){
						System.err.print(i+" ");
					}
				}
			}
			System.err.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.err.println("Exit");
			System.exit(1);
		}
		
		
		Collections.sort(data, new Comparator<Row>() {
			@Override
			public int compare(Row o1, Row o2) {
				return o1.getNumber().compareTo(o2.getNumber());
			}
			
		});
		loader.upLoad(data);
	}
}
