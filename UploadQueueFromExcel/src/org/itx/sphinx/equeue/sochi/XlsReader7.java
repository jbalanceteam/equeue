package org.itx.sphinx.equeue.sochi;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.sphinx.equeue.Row;
import org.itx.sphinx.equeue.XlsReaderBase;

import jxl.*; 
import jxl.read.biff.BiffException;

public class XlsReader7 extends XlsReaderBase {

	private String filePath;
	
	public XlsReader7(String filePath){
		this.filePath = filePath;
	} 
	
	public List<Row> getData(){
		try {
			List<Row> result = new ArrayList<Row>(1000);
			Workbook workbook = Workbook.getWorkbook(new File(filePath));
			Sheet sheet = workbook.getSheet(0);
			int rowsCount = sheet.getRows();
			System.err.println("There are "+(rowsCount-1)+" rows.");
			for (int i=20; i < rowsCount; i++ ) {
				 
				try {
					 Cell[] cells = sheet.getRow(i);
					 Row row = processRow(cells);
					 result.add(row);
				} catch(Exception e) {
					System.err.println("Error processing record "+i);
					e.printStackTrace();
					return null;
				}
			}
			return result;
		} catch (BiffException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} 
	}

	private Row processRow(Cell[] cells) {
		String fio = getString(cells ,0);
		 StringBuffer sb = new StringBuffer();
		 for (int j = 0; j < fio.length(); j++) {
				char charAt = fio.charAt(j);
				if(j!=0 && Character.isUpperCase(charAt) && fio.charAt(j-1) != ' '){
					sb.append(' ');
				}
				sb.append(charAt);
		 }
		 String[] splitFio = sb.toString().split(" ");
		 String secondName = splitFio[0];	
		 String	firstName = splitFio[1];
		 String patronymicName = splitFio.length>2 ?  splitFio[2]: null;
		 
		 Date birthday = 		 getDate  (cells ,1);
		 StringBuffer comments  = new StringBuffer();
		 comments.append(" Возраст (на дату распределения): ");
		 comments.append(getString  (cells ,2));
		 comments.append(" Тип заявления ");
		 comments.append(getString  (cells ,4));	
		 comments.append(" Номер заявления ");
		 comments.append(getString  (cells ,5));
		 comments.append(" Зарегистрировано ");
		 comments.append(getString  (cells ,6));
		 Date regDate = getDate  (cells ,6);
		 comments.append(" Статус ");
		 comments.append(getString  (cells ,7));
		 comments.append(" ФИО заявителя ");
		 comments.append(getString  (cells ,9));
		 comments.append(" ФИО родителя ");
		 comments.append(getString  (cells ,10));
		 comments.append(" Тип уведомления ");
		 comments.append(getString  (cells ,12));
		String phone = getString  (cells ,13);
		String email = getString  (cells ,15);
		email= email.trim();
		if(email.equals("-")){
			email = null;
		}
		
		comments.append(" Тип привилегии ");
		comments.append(getString  (cells ,18));
		
		String dousS = getString(cells ,21);
		String[] split = dousS.split(",");
		List<String> dous=new ArrayList<String>(split.length);
		for (String s : split) {
			 s = s.trim();
			 if(!s.isEmpty())
				 dous.add(s);
		}
		Integer plainYear = getDate(cells ,24).getYear() + 1900;
		 
		String privelegyS = null;
		Row row = new Row(null, dous, secondName,  firstName, patronymicName,  birthday, "нет информации", regDate, privelegyS );
		row.setComments(comments.toString());
		row.setPhone(phone);
		row.setSex(Sex.M);
		row.setPlainYear(plainYear);
		return row;
	}
	
	public static void main(String [] argv){
		List<Row> data = new XlsReader7("PreschoolChildRegister-05072015-101333.xls").getData();
		int i=1;
		for (Row row : data) {
			System.err.println("=============== "+i++ +" ===============");
			System.err.println(row);
		}
		
	}
}
