package org.itx.sphinx.equeue;

public interface Loader {

	void load(String filename);

}
