package org.itx.sphinx.equeue;


public class Main {
	
	public final static String FILE_NAME_PROP= "filename";
	public final static String LOADER_NAME_PROP= "loader";
	
	public final static String LOADER_NAME_ANAPA = "anapa";
	public final static String LOADER_NAME_SOCHI = "sochi";
	public final static String LOADER_NAME_SOCHI6 = "sochi6";
	public final static String LOADER_NAME_SOCHI7 = "sochi7";
	public final static String LOADER_NAME_GEL = "gel";
	public final static String LOADER_NAME_ARMAVIR = "armavir";
	public final static String LOADER_NAME_USPENSKAYA = "uspenskaya";
	public final static String LOADER_NAME_PAVLOVSKAYA = "pavlovskaya";
	public final static String LOADER_NAME_TIHORECK = "tihoreck";
	public final static String LOADER_NAME_LABINSK = "labinsk";
	public final static String LOADER_NAME_TUAPSE = "tuapse";
	public final static String LOADER_NAME_KRYLOVSKAYA = "krylovskaya";
	
	
	public static void main(String [] argv){
		
		String filename = System.getProperty(FILE_NAME_PROP, "PreschoolChildRegister-05072015-101333.xls");
		String loaderName = System.getProperty(LOADER_NAME_PROP, LOADER_NAME_SOCHI7);
		
		
		if(loaderName==null || loaderName.isEmpty()){
			System.err.println("You should set '"+LOADER_NAME_PROP+"' system property");
			System.err.println("Possible values: "+LOADER_NAME_SOCHI+", " + LOADER_NAME_SOCHI6+", " + LOADER_NAME_SOCHI7 + ", "+LOADER_NAME_GEL + ", "+LOADER_NAME_ARMAVIR+", "+LOADER_NAME_USPENSKAYA +
					", "+LOADER_NAME_PAVLOVSKAYA + ", "+LOADER_NAME_TIHORECK + ", "+ LOADER_NAME_ANAPA+ 
					", "+ LOADER_NAME_LABINSK + ", " + LOADER_NAME_TUAPSE + ", " + LOADER_NAME_KRYLOVSKAYA);
			return;
		}
		
		System.err.println("filename: "+filename);
		System.err.println("loader: "+loaderName);
		
		Loader loader = getLoader(loaderName);
		loader.load(filename);
		
		System.err.println("Comple");
	}
	
	/**
	 * Может здесь уместне фабрика? Builder?
	 */
	private static Loader getLoader(String loader){
		if(LOADER_NAME_SOCHI.equals(loader)){
			return new org.itx.sphinx.equeue.sochi.LoaderBean();
		} else if(LOADER_NAME_SOCHI6.equals(loader)){
			return new org.itx.sphinx.equeue.sochi.LoaderBean6();
		} else if(LOADER_NAME_SOCHI7.equals(loader)){
			return new org.itx.sphinx.equeue.sochi.LoaderBean7();
		} else if(LOADER_NAME_GEL.equals(loader)){
			return new org.itx.sphinx.equeue.gelendjik.LoaderBean();
		} else if(LOADER_NAME_ARMAVIR.equals(loader)){
			return new org.itx.sphinx.equeue.armavir.LoaderBean();
		} else if(LOADER_NAME_USPENSKAYA.equals(loader)){
			return new org.itx.sphinx.equeue.uspenskaya.LoaderBean();
		} else if(LOADER_NAME_PAVLOVSKAYA.equals(loader)){
			return new org.itx.sphinx.equeue.pavlovskaya.LoaderBean();
		} else if(LOADER_NAME_TIHORECK.equals(loader)){
			return new org.itx.sphinx.equeue.tihoreck.LoaderBean();
		} else if(LOADER_NAME_ANAPA.equals(loader)){
			return new org.itx.sphinx.equeue.anapa.LoaderBean();
		} else if(LOADER_NAME_LABINSK.equals(loader)){
			return new org.itx.sphinx.equeue.labinsk.LoaderBean();
		} else if(LOADER_NAME_TUAPSE.equals(loader)){
			return new org.itx.sphinx.equeue.tuapse.LoaderBean();
		} else if(LOADER_NAME_KRYLOVSKAYA.equals(loader)){
			return new org.itx.sphinx.equeue.krylovskaya.LoaderBean();
		} else {
			return null;
		}
	}
	
	
	
}
