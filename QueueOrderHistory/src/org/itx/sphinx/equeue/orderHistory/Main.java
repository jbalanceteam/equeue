package org.itx.sphinx.equeue.orderHistory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class Main {
	
	
	public final static String PROP_HOST= "host";
	public final static String PROP_USERNAME= "username";
	public final static String PROP_PASSWORD= "password";
	public final static String PROP_DATABASE= "database";
	
	public final static String PROP_RN= "rn";
	public final static String PROP_DATE= "date";
	
	private static DateFormat dateFroemat = new SimpleDateFormat("yyyy-MM-dd");
	
	public static void main(String [] argv) throws SQLException, ClassNotFoundException{
		
		String host = System.getProperty(PROP_HOST, "localhost");
		String username = System.getProperty(PROP_USERNAME, "postgres");
		String password = System.getProperty(PROP_PASSWORD, "masterkey");
		String database = System.getProperty(PROP_DATABASE, "equeue");
		
		String rn = System.getProperty(PROP_RN);
		String date = System.getProperty(PROP_DATE);
		
		if(rn==null || rn.isEmpty()){
			System.err.println("Необходимо установить '"+PROP_RN+"' параметр");
			System.err.println("Например: -Drn=1424-2013");
			return;
		}
		
		if(date==null || date.isEmpty()){
			System.err.println("Необходимо установить '"+PROP_DATE+"' параметр");
			System.err.println("Например: -Ddate=2014-06-12");
			return;
		}
		
		
		if(rn==null || rn.isEmpty()){
			System.err.println("You must set '"+PROP_RN+"' system property");
			System.err.println("E.g.: 1424-2013");
			return;
		}
		
		System.err.println("============ Connection properties ============");
		System.err.println("host: "+host);
		System.err.println("username: "+username);
		System.err.println("password: "+password);
		System.err.println("database: "+database);
		System.err.println("");
		System.err.println("============ Query setings ============");
		System.err.println("rn: "+rn);
		System.err.println("date: "+date);
		
		
		final String[] split = rn.split("-");
		final String dnumber = split[0];
		final String year = split[1];
		
		final Connection connection = getConnection(host, username, password, database);
		final Statement statement = connection.createStatement();
		
		final StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(" select hdr.uid, surname, realname, patronymic, birthday, hdr.regdate, hdr.privilege_uid ");
		stringBuilder.append(" from Physical p inner join HDouRequest hdr on hdr.child_uid = p.uid" );
		stringBuilder.append(" where p.version is null and hdr.version is null and hdr.dnumber = " + dnumber);
		stringBuilder.append("  and date_part ('year', hdr.regdate) = " + year );

		final ResultSet executeQuery = statement.executeQuery(stringBuilder.toString());
		String formattedDate;
		try {
			final Date parse = dateFroemat.parse(date);
			formattedDate =  dateFroemat.format(parse);
		} catch (ParseException e) {
			System.err.println("Неверный формат даты: " + date);
			System.err.println("Используйте следующий формат: гггг-мм-дд");
			return ;
		}
		
		while(executeQuery.next()){
			final Long hdrUid = executeQuery.getLong("uid");
			final String surname= executeQuery.getString("surname");
			final String realname= executeQuery.getString("realname");
			final String patronymic= executeQuery.getString("patronymic");
			final Timestamp birthday = executeQuery.getTimestamp("birthday");
			final Timestamp regdate = executeQuery.getTimestamp("regdate");
			final Object privilegeUid = executeQuery.getObject("privilege_uid"); 
			
			System.err.println("");
			System.err.println("===============================");
			System.err.println("UID: " + hdrUid);
			System.err.println("Дата постановка в очередь: " + dateFroemat.format(regdate));
			System.err.println(surname + " " + realname + " " + patronymic);
			System.err.println("Дата рождения: " + dateFroemat.format(birthday));
			System.err.println("Льгота: " + (privilegeUid==null? "нет": "есть"));
			executeQuery.close();
			
			final StringBuilder sb = new StringBuilder();
			sb.append(" select count(*) from HDouRequest r1 inner join Physical c on r1.child_UId = c.uid  ");
			sb.append("  left join     ");
			sb.append(" (    ");
					
			sb.append(" 	 select sr.*     ");
			sb.append(" 	 from SRegister sr inner join HRegister hr on sr.huid=hr.uid    ");
				
			sb.append(" 	 where    ");
			sb.append(" 	 sr.version is null     ");
			sb.append(" 	 and hr.version is null     ");
			sb.append(" 	 and hr.status='THIRD'    ");
			sb.append(" 	 and (hr.closeDate is null or hr.closeDate > '"+ formattedDate +"')     ");
			sb.append(" 	 and hr.registerDate <= '"+ formattedDate +"'    ");
				
			sb.append(" 	 and not exists (    ");
			sb.append(" 			 select sr2.uid from SRegister  sr2 inner join HRegister hr2 on sr2.huid=hr2.uid    "); 
			sb.append(" 		 where (hr2.closeDate is null or hr2.closeDate > '"+ formattedDate +"')     ");
			sb.append(" 		 and (sr2.closeDate is null or sr2.closeDate > '"+ formattedDate +"')     ");
			sb.append(" 		 and hr2.registerDate <= '"+ formattedDate +"'    ");
			sb.append(" 		 and hr2.version is null     ");
			sb.append(" 		 and sr2.version is null     ");
			sb.append(" 		 and sr2.DouRequest_uid = sr.DouRequest_uid    ");
			sb.append(" 		 and hr2.status='THIRD'    ");
			sb.append(" 		 and hr2.dnumber > hr.dnumber    ");
			sb.append(" 		 )    ");

			sb.append(" 	 )register     ");
			sb.append(" on register.DouRequest_uid = r1.uid     ");
			sb.append(" where (r1.closeDate is null or r1.closeDate > '"+ formattedDate +"')     ");
			sb.append(" and r1.regDate <= '"+ formattedDate +"'    ");
			sb.append(" and r1.version is null   ");
			
			ResultSet resultSet = statement.executeQuery(sb.toString());
			resultSet.next();
			long count = resultSet.getLong(1);
			System.err.println("Всего детей на  дату отчета: " + count);
			resultSet.close();
			
			final Calendar c = Calendar.getInstance();
			c.setTime(regdate);
			c.set(Calendar.HOUR_OF_DAY, c.getMinimum(Calendar.HOUR_OF_DAY));
			c.set(Calendar.MINUTE, c.getMinimum(Calendar.MINUTE));
			c.set(Calendar.SECOND, c.getMinimum(Calendar.SECOND));
			c.set(Calendar.MILLISECOND, c.getMinimum(Calendar.MILLISECOND));
			c.set(Calendar.DAY_OF_MONTH, c.getMinimum(Calendar.DAY_OF_MONTH));
			c.set(Calendar.MONTH, c.getMinimum(Calendar.MONTH));
			final Date regdateStartOfYear = c.getTime();
			
			c.set(Calendar.HOUR_OF_DAY, c.getMaximum(Calendar.HOUR_OF_DAY));
			c.set(Calendar.MINUTE, c.getMaximum(Calendar.MINUTE));
			c.set(Calendar.SECOND, c.getMaximum(Calendar.SECOND));
			c.set(Calendar.MILLISECOND, c.getMaximum(Calendar.MILLISECOND));
			c.set(Calendar.DAY_OF_MONTH, c.getMaximum(Calendar.DAY_OF_MONTH));
			c.set(Calendar.MONTH, c.getMaximum(Calendar.MONTH));
			final Date regdateEndOfYear = c.getTime();
			
			sb.append("  and  ((r1.regDate < '"+ dateFroemat.format(regdateStartOfYear) +"') or (r1.regDate >= '"+ dateFroemat.format(regdateStartOfYear) +"' and r1.regDate <= '"+ dateFroemat.format(regdateEndOfYear) +"' and r1.dnumber <= " + dnumber +")   ");
			sb.append("  or r1.privilege_uid is not null) ");
			sb.append("  and (register.rnaction is null or register.rnaction <> 'GIVE_PERMIT' )  " );
			
//			System.err.println(sb.toString());
			resultSet = statement.executeQuery(sb.toString());
			resultSet.next();
			count = resultSet.getLong(1);
			System.err.println("Детей впереди: " + count);
			resultSet.close();
			
			
			c.setTime(birthday);
			c.set(Calendar.HOUR_OF_DAY, c.getMinimum(Calendar.HOUR_OF_DAY));
			c.set(Calendar.MINUTE, c.getMinimum(Calendar.MINUTE));
			c.set(Calendar.SECOND, c.getMinimum(Calendar.SECOND));
			c.set(Calendar.MILLISECOND, c.getMinimum(Calendar.MILLISECOND));
			c.set(Calendar.DAY_OF_MONTH, c.getMinimum(Calendar.DAY_OF_MONTH));
			c.set(Calendar.MONTH, c.getMinimum(Calendar.MONTH));
			final Date birthdayStartOfYear = c.getTime();
			
			c.set(Calendar.HOUR_OF_DAY, c.getMaximum(Calendar.HOUR_OF_DAY));
			c.set(Calendar.MINUTE, c.getMaximum(Calendar.MINUTE));
			c.set(Calendar.SECOND, c.getMaximum(Calendar.SECOND));
			c.set(Calendar.MILLISECOND, c.getMaximum(Calendar.MILLISECOND));
			c.set(Calendar.DAY_OF_MONTH, c.getMaximum(Calendar.DAY_OF_MONTH));
			c.set(Calendar.MONTH, c.getMaximum(Calendar.MONTH));
			final Date birthdayEndOfYear = c.getTime();
			
			
			sb.append("  and c.birthday between '"+ dateFroemat.format(birthdayStartOfYear) +"' and '"+ dateFroemat.format(birthdayEndOfYear) +"' ");
			
			
//			System.err.println(sb.toString());
			resultSet = statement.executeQuery(sb.toString());
			resultSet.next();
			count = resultSet.getLong(1);
			System.err.println("С фильтром по году рождения: " + count);
			resultSet.close();
			  
			
			
			
			final List<DouInfo> dous = getDous(statement, formattedDate, hdrUid);
			StringBuilder douGroupFilter = new StringBuilder();
			douGroupFilter.append("   and exists ( ");
			douGroupFilter.append(" select sdr.uid from SDouRequest sdr ");
			douGroupFilter.append(" inner join Dou d on d.uid = sdr.contractor ");
			douGroupFilter.append(" where sdr.version is null"); 
			douGroupFilter.append(" and (sdr.closeDate is null or sdr.closeDate > '"+ formattedDate +"') ");   
			douGroupFilter.append(" and sdr.openDate <= '"+ formattedDate +"' ");  
			douGroupFilter.append(" and d.uid in   ( ??? ) ");
			douGroupFilter.append(" and r1.uid = sdr.huid "); 
			douGroupFilter.append(" and d.version is null ");
			douGroupFilter.append(" ) ");
			
			StringBuilder douList = new StringBuilder();
			for (DouInfo douInfo : dous) {
				douList.append(douInfo.uid);
				douList.append(",");
				
				final String douFilterStr = douGroupFilter.toString().replace("???", douInfo.uid.toString());
				
				resultSet = statement.executeQuery(sb.toString() + douFilterStr);
				resultSet.next();
				count = resultSet.getLong(1);
				
			//	System.err.println(sb.toString() + douFilterStr);
				
				System.err.println("ДОУ " + douInfo.number + ": " + count);
				resultSet.close();

				
				
			}
			douList.delete(douList.length() - 1,	 douList.length());
			
			
			
			final String douGroupFilterStr = douGroupFilter.toString().replace("???", douList.toString());
			
			resultSet = statement.executeQuery(sb.toString() + douGroupFilterStr);
			resultSet.next();
			count = resultSet.getLong(1);
			System.err.println("С фильтром по группе ДОУ: " + count);
			resultSet.close();
			
			break;
		}
		
	}

	static class DouInfo{
		private Long uid;
		private String number;
	}
	
	private static List<DouInfo> getDous(final Statement statement,
			String formattedDate, final Long hdrUid) throws SQLException {
		ResultSet resultSet;
		final StringBuilder dousQuery = new StringBuilder();
		dousQuery.append("select sdr.uid suid, d.uid douUid, d.number from SDouRequest sdr inner join Dou d on d.uid = sdr.contractor ");
		dousQuery.append("where sdr.version is null  ");
		dousQuery.append("and (sdr.closeDate is null or sdr.closeDate > '"+ formattedDate +"')    ");
		dousQuery.append(" and sdr.openDate <= '"+ formattedDate +"'   ");
		dousQuery.append(" and sdr.huid = " + hdrUid);
		
		resultSet = statement.executeQuery(dousQuery.toString());
		
		List<DouInfo> result = new ArrayList<Main.DouInfo>();
		while(resultSet.next()){
			final long suid = resultSet.getLong("suid");
			final long douUid = resultSet.getLong("douUid");
			final String number = resultSet.getString("number");
			
			final DouInfo e = new DouInfo();
			e.number = number;
			e.uid = douUid;
			result.add(e);
		}
		resultSet.close();
		return result;
	}
	
	
	public static Connection getConnection(String host, String username, String password, String database) throws ClassNotFoundException, SQLException{
	System.err.println("-------- PostgreSQL "
			+ "JDBC Connection Testing ------------");

		Class.forName("org.postgresql.Driver");

	System.err.println("PostgreSQL JDBC Driver Registered!");

	Connection connection = null;

		connection = DriverManager.getConnection(
				"jdbc:postgresql://"+ host + ":5432/" + database, username,
				password);

	if (connection != null) {
		System.err.println("You made it, take control your database now!");
	} else {
		System.err.println("Failed to make connection!");
	}
	
	return connection;
}
	
}
