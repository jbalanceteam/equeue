package org.itx.jbalance.l0;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.itx.jbalance.l0.Age;
import org.junit.Assert;
import org.junit.Test;

public class TestAgeCalculator {

	DateFormat df=new SimpleDateFormat("yyyy-MM-dd");
	@Test
	public void testCalculateAge(){
		try {
			Age age=AgeCalculator.calculateAge(df.parse("2001-01-01"), df.parse("2001-01-02"));
			Assert.assertEquals(age.years, 0);
			Assert.assertEquals(age.months, 0);
			Assert.assertEquals(age.days, 1);
			
			age=AgeCalculator.calculateAge(df.parse("2001-01-01"), df.parse("2001-02-02"));
			Assert.assertEquals(age.years, 0);
			Assert.assertEquals(age.months, 1);
			Assert.assertEquals(age.days, 1);
			
			age=AgeCalculator.calculateAge(df.parse("2001-01-01"), df.parse("2002-02-02"));
			Assert.assertEquals(age.years, 1);
			Assert.assertEquals(age.months, 1);
			Assert.assertEquals(age.days, 1);
			
			
			age=AgeCalculator.calculateAge(df.parse("2001-01-05"), df.parse("2001-02-02"));
			Assert.assertEquals(age.years, 0);
			Assert.assertEquals(age.months, 0);
			Assert.assertEquals(age.days, 28);
			
			
			age=AgeCalculator.calculateAge(df.parse("2001-02-05"), df.parse("2002-01-06"));
			Assert.assertEquals(age.years, 0);
			Assert.assertEquals(age.months, 11);
			Assert.assertEquals(age.days, 1);
			
			age=AgeCalculator.calculateAge(df.parse("2001-02-02"), df.parse("2002-01-02"));
			Assert.assertEquals(age.years, 0);
			Assert.assertEquals(age.months, 11);
			Assert.assertEquals(age.days, 0);
			
			age=AgeCalculator.calculateAge(df.parse("2001-02-03"), df.parse("2002-01-02"));
			Assert.assertEquals(age.years, 0);
			Assert.assertEquals(age.months, 10);
			Assert.assertEquals(age.days, 30);
			

			age=AgeCalculator.calculateAge(df.parse("2001-01-03"), df.parse("2002-01-02"));
			Assert.assertEquals(age.years, 0);
			Assert.assertEquals(age.months, 11);
			Assert.assertEquals(age.days, 30);
			
			age=AgeCalculator.calculateAge(df.parse("2001-01-03"), df.parse("2001-01-03"));
			Assert.assertEquals(age.years, 0);
			Assert.assertEquals(age.months, 0);
			Assert.assertEquals(age.days, 0);
			
			age=AgeCalculator.calculateAge(df.parse("2001-01-04"), df.parse("2001-01-03"));
//			System.out.println(age);
			Assert.assertEquals(age.years, 0);
			Assert.assertEquals(age.months, 0);
			Assert.assertEquals(age.days, 0);

			age=AgeCalculator.calculateAge(null, df.parse("2001-01-03"));
			Assert.assertNull(age);

			age=AgeCalculator.calculateAge(df.parse("2001-01-03"), null);
			Assert.assertNull(age);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testDaysInMonth(){
		Calendar c=Calendar.getInstance();
		c.setTime(new Date());
		c.add(Calendar.MONTH,	-1000);
		for(int i=0;i<1000;i++){
			c.add(Calendar.MONTH,	1);
			Assert.assertEquals(AgeCalculator.daysInMonth(c.get(Calendar.YEAR), c.get(Calendar.MONTH)),  c.getMaximum(Calendar.DAY_OF_MONTH));
		}
	}
}
