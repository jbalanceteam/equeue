package org.itx.jbalance.l1.session;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

import org.itx.jbalance.l0.h.Hmenu;
import org.itx.jbalance.l0.o.Action;
import org.itx.jbalance.l0.s.SdivisionJuridical;
import org.itx.jbalance.l0.s.Sprofile;
import org.itx.jbalance.l1.d.Profiles;

@Local

//	TODO: проблема с наследованием интерфейсом и резолвингом в @EJB

public interface Sessionprofile extends Profiles{
	
	public Date getDate();
	
	public Config getConfig();

	public void setUser();
	
	public Sprofile getSpecificationLine();
	
	public List<Hmenu> getMenus();

	public void addProfiles();

	public Action getActionByViewId(String VId);
	
	public void setDivisionJuridical(SdivisionJuridical dj);
	
	public SdivisionJuridical getDivisionJuridical();
	
	public void setConfig(String key, Abstractconfig obj);

	public <T extends Abstractconfig> T getConfig(String key);
	
	
}
