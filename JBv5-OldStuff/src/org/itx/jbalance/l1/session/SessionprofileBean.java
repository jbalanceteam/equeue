package org.itx.jbalance.l1.session;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateful;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.h.Hactionright;
import org.itx.jbalance.l0.h.Hcategorygroup;
import org.itx.jbalance.l0.h.Hcontractorright;
import org.itx.jbalance.l0.h.Hcurrencyright;
import org.itx.jbalance.l0.h.Hkindofpriceright;
import org.itx.jbalance.l0.h.Hmenu;
import org.itx.jbalance.l0.h.Hprofile;
import org.itx.jbalance.l0.h.Hsystem;
import org.itx.jbalance.l0.o.Action;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.o.Currency;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.o.Kindofprice;
import org.itx.jbalance.l0.o.Oper;
import org.itx.jbalance.l0.s.Scontractorright;
import org.itx.jbalance.l0.s.SdivisionJuridical;
import org.itx.jbalance.l0.s.Smenu;
import org.itx.jbalance.l0.s.Sprofile;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.d.Actionrights;
import org.itx.jbalance.l1.d.Contractorrights;
import org.itx.jbalance.l1.d.Currencyrights;
import org.itx.jbalance.l1.d.Kindofpricerights;
import org.itx.jbalance.l1.d.Menus;
import org.itx.jbalance.l1.d.Profiles;
import org.itx.jbalance.l1.d.ProfilesBean;
import org.itx.jbalance.l1.o.Actions;
import org.itx.jbalance.l1.o.Juridicals;
import org.itx.jbalance.l1.o.Opers;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
import org.jboss.security.SecurityAssociation;

@Remote(SessionprofileRemote.class)
@Stateful(name = "Sessionprofile")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 900, removalTimeoutSeconds = 3600)
@LocalBinding(jndiBinding = "Sessionprofile/local")
@RemoteBinding(jndiBinding = "Sessionprofile/remote")
public class SessionprofileBean extends ProfilesBean implements Sessionprofile,SessionprofileRemote {

	private static final long serialVersionUID = 901001508194964134L;

	private Config config;
	
	private HashMap<String, Abstractconfig> mconfig;

	@EJB(name = "Opers/local")
	private Opers opers;

	@EJB(name = "Profiles/local", beanName = "Profiles")
	private Profiles profiles;

	@EJB(name = "Menus/local")
	private Menus menus;

	@EJB(name = "Actions/local")
	private Actions actions;

	@EJB(name = "Actionrights/local")
	private Actionrights actionrights;

	@EJB(name = "Contractorrights/local")
	private Contractorrights contractorrights;

	@EJB(name = "Kindofpricerights/local")
	private Kindofpricerights kindofpricerights;
	
//	@EJB(name = "Rsssubscriptions/local")
//	private Rsssubscriptions rsssubscriptions;

	@EJB(name = "Currencyrights/local")
	private Currencyrights currencyrights;

	@EJB(name = "Juridicals/local")
	private Juridicals juridicals;
	
	private List<Sprofile> list_profile = null;

	
	public Date getDate(){return new Date();}
	 

	public void setUser() {
		
		Principal principal = SecurityAssociation.getPrincipal();
		final String UserLogin ;
		if(principal!=null)
			UserLogin = principal.toString();
		else
			UserLogin = "admin";
		System.out.println("-------------AutorisationUserName="+UserLogin);
		
		Oper oper = null;
		Hprofile hprofile = null;
		Sprofile sprofile = null;
		Hmenu hmenu = null;
		Hmenu hmenu1 = null;
		Hmenu hmenu2 = null;
		Smenu smenu = null;
		Action action = null;
		try {
			oper = opers.getByLogin(UserLogin).get(0);
		} catch (Exception e) {
		}
		if ((oper == null) && (UserLogin.equals("admin"))){
			try {
				

				
				oper = new Oper();
				oper.setName("Administrator");
				oper.setLogin("admin");
				oper.setPassphrase("");
				oper.setVersionRoot(oper);
				opers.setRights(this);
				opers.setObject(oper);
				opers.create();

				Juridical j = new Juridical();
				j.setName("ООО Сфинкс");
				juridicals.setRights(this);
				juridicals.setObject(j);
				juridicals.create();
				
				hprofile = new Hprofile();
				hprofile.setFoundation("Autogeneraterd profile");
				hprofile.setContractorOwner(juridicals.getObject());
				hprofile.setOperReceiver(oper);
				//!!!!!!!!!!!!!!!!!
				config.setDivisionjuridical(null);
				profiles.setRights(this);
				profiles.setObject(hprofile);
				profiles.create();

				
				Hcontractorright hcontright = new Hcontractorright();
				hcontright.setContractorOwner(juridicals.getObject());
				hcontright.setFoundation("");
				contractorrights.setRights(this);
				contractorrights.setObject(hcontright);
				contractorrights.create();
				
				Scontractorright scontright = new Scontractorright();
				scontright.setEnabledContractor(juridicals.getObject());
				contractorrights.setSpecificationLine(scontright);
				contractorrights.createSpecificationLine();
				
				sprofile = new Sprofile();
				sprofile.setEnabler(contractorrights.getObject());
				profiles.setSpecificationLine(sprofile);
				profiles.createSpecificationLine();
				
				
				
				hmenu1 = new Hmenu();
				hmenu1.setName("AdministrativeMenu");
				hmenu1.setFoundation("Autogenerated menu");
				menus.setRights(this);
				menus.setObject(hmenu1);
				menus.create();

				sprofile = new Sprofile();
				sprofile.setEnabler(hmenu1);
				profiles.setSpecificationLine(sprofile);
				profiles.createSpecificationLine();

				hmenu = new Hmenu();
				hmenu.setHigherDocument(hmenu1);
				hmenu.setName("SystemServiceMenu");
				hmenu.setFoundation("Autogenerated menu");
				menus.setObject(hmenu);
				menus.create();

				// ------------------------- system

				action = new Action();
				action.setUrl("/pages/system/systems.seam");
				action.setName("ListSystem");
				action.setImage("listsystem.png");
				actions.setRights(this);
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- exchange

				action = new Action();
				action.setUrl("/pages/exchange/exchanges.seam");
				action.setName("ListExchange");
				action.setImage("listexchange.png");
				actions.setRights(this);
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/exchange/exchange.seam");
				action.setName("CardExchange");
				action.setImage("cardexchange.png");
				actions.setRights(this);
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- oper

				action = new Action();
				action.setUrl("/pages/oper/opers.seam");
				action.setName("ListOper");
				action.setImage("listoper.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/oper/oper.seam");
				action.setImage("cardoper.png");
				action.setName("CardOper");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- action

				action = new Action();
				action.setUrl("/pages/action/actions.seam");
				action.setName("ListAction");
				action.setImage("listaction.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/action/action.seam");
				action.setName("CardAction");
				action.setImage("cardaction.png");
				actions.setObject(action);
				actions.create();
				action = actions.getObject();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- menu

				action = new Action();
				action.setUrl("/pages/menu/menus.seam");
				action.setName("ListMenu");
				action.setImage("listmenu.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/menu/menunew.seam");
				action.setName("CardMenu");
				action.setImage("cardmenu.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/menu/menuview.seam");
				action.setName("TreeMenu");
				action.setImage("treemenu.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/menu/richmenu.seam");
				action.setName("RichMenu");
				action.setImage("richmenu.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- profile

				action = new Action();
				action.setUrl("/pages/profile/profiles.seam");
				action.setName("ListProfile");
				action.setImage("listprofile.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/profile/profile.seam");
				action.setName("CardProfile");
				action.setImage("cardprofile.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- actionright

				action = new Action();
				action.setUrl("/pages/actionright/actionrights.seam");
				action.setName("ListActionright");
				action.setImage("listactionright.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/actionright/actionright.seam");
				action.setName("CardActionright");
				action.setImage("cardactionright.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// =========================

				hmenu2 = new Hmenu();
				hmenu2.setHigherDocument(hmenu1);
				hmenu2.setName("BusinesServiceMenu");
				hmenu2.setFoundation("Autogenerated menu");
				menus.setObject(hmenu2);
				menus.create();
				// ------------------------- contractorright

				action = new Action();
				action.setUrl("/pages/contractorright/contractorrights.seam");
				action.setName("ListContractorright");
				action.setImage("listcontractorright.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/contractorright/contractorright.seam");
				action.setName("CardContractorright");
				action.setImage("cardcontractorright.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- warehouse

				action = new Action();
				action.setUrl("/pages/warehouse/warehouses.seam");
				action.setName("ListWarehouse");
				action.setImage("listwarehouse.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/warehouse/warehouse.seam");
				action.setName("CardWarehouse");
				action.setImage("cardwarehouse.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- contractorwarehouse

				action = new Action();
				action
						.setUrl("/pages/contractorwarehouse/contractorwarehouses.seam");
				action.setName("ListContractorwarehouse");
				action.setImage("listcontractorwarehouse.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action
						.setUrl("/pages/contractorwarehouse/contractorwarehouse.seam");
				action.setName("CardContractorwarehouse");
				action.setImage("cardcontractorwarehouse.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- kindofprice

				action = new Action();
				action.setUrl("/pages/kindofprice/kindofprices.seam");
				action.setName("ListKindofprice");
				action.setImage("listkindofprice.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/kindofprice/kindofprice.seam");
				action.setName("CardKindofprice");
				action.setImage("cardkindofprice.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- kindofpriceright

				action = new Action();
				action.setUrl("/pages/kindofpriceright/kindofpricerights.seam");
				action.setName("ListKindofpriceright");
				action.setImage("listkindofpriceright.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/kindofpriceright/kindofpriceright.seam");
				action.setName("CardKindofpriceright");
				action.setImage("cardkindofpriceright.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- currency
				action = new Action();
				action.setUrl("/pages/currency/currencys.seam");
				action.setName("ListCurrency");
				action.setImage("listcurrency.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/currency/currency.seam");
				action.setName("CardCurrency");
				action.setImage("cardcurrency.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- currencyright

				action = new Action();
				action.setUrl("/pages/currencyright/currencyrights.seam");
				action.setName("ListCurrencyright");
				action.setImage("listcurrencyright.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/currencyright/currencyright.seam");
				action.setName("CardCurrencyright");
				action.setImage("cardcurrencyright.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				// ------------------------- employee

				action = new Action();
				action.setUrl("/pages/employee/employees.seam");
				action.setName("ListEmployee");
				action.setImage("listemployee.png");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();

				action = new Action();
				action.setUrl("/pages/employee/employee.seam");
				action.setImage("cardemployee.png");
				action.setName("CardEmployee");
				actions.setObject(action);
				actions.create();

				smenu = new Smenu();
				smenu.setMenuAction(action);
				menus.setSpecificationLine(smenu);
				menus.createSpecificationLine();
			} catch (Exception e) {
				getLog().error("",e);
				// TODO: handle exception
			}
		}
		if (oper == null) {
			// TODO пользователь в источнике авторизации есть но профиля нет.
			// Здесь обработка нужна!
		}
		Date currentDate = new  Date();
		config.setOper(oper);
		config.setLoginDate(currentDate);
		config.setPeriodFrom(currentDate);
		config.setPeriodTo(currentDate);
		try {
			setObject(getByOperReceiver(oper));
		} catch (Exception e) {
			// User loged in but not have profile.
			// newObject();
			return;
		}
		
		list_profile = (List<Sprofile>)getSpecificationLines();
		System.out.println(">>> Get spec profile user ");
		
		List<Action> lea = getEnabledActions();
		config.setEnabledActions(lea);
		List<Juridical> lcr = getContractorright();
		config.setContractorsOwner(lcr);
		config.setSelectedContractorOwner(lcr);
		List<Kindofprice> lekp = getEnabledKindofprices();
		config.setEnabledKindofprices(lekp);
		List<Currency> lec = getEnabledCurrencys();
		config.setEnabledCurrencys(lec);
//		List<Srsssubscription> lrss = getEnabledRssSubscription();
//		config.setEnabledSrssSubscription(lrss);
		
		List<Hcategorygroup> hcg = getEnabledHcategorygroup();
		config.setEnabledHcategorygroup(hcg);
		config.setSkin(getObject().getSkin()==null?"DEFAULT":getObject().getSkin());

//TODO Стоит ли убрать поля config
		
		mconfig = new HashMap<String, Abstractconfig>();
		setConfig(Config.NAME_MODULE, config);
		
	}

	public Config getConfig() {
		return config;
	}
	
	public void setConfig(String key, Abstractconfig obj){
		mconfig.put(key, obj);
	}

	@SuppressWarnings("unchecked")
	public <T extends Abstractconfig> T getConfig(String key){
		return (T)mconfig.get(key);
	}
	
	public SessionprofileBean() {
		super();
		config = new Config();
	}

	public Hprofile getObject() {
		return (Hprofile) super.getObject();
	}

	public Sprofile getSpecificationLine() {
		return (Sprofile) super.getSpecificationLine();
	}

	// Not work there!
	@Override
	public synchronized void create() {
	}

	// Not work there!
	@Override
	public synchronized void createSpecificationLine() {
	}

	// Work there! для сохранения пользователем личых параметров
	@Override
	public synchronized void update() throws JBException {
		// TODO Auto-generated method stub
		getObject().setSkin(config.getSkin());
		super.update();
	}
	// Not work there!
	@Override
	public synchronized void updateSpecificationLine() {
	}

	// Not work there!
	@Override
	public synchronized void delete() {
	}

	// Not work there!
	@Override
	public synchronized void deleteSpecificationLine() {
	}
//Checked
	@SuppressWarnings("unchecked")
	public List<Hmenu> getMenus() {
		List<Hmenu> rez = new ArrayList<Hmenu>();
		try {
			for (Sprofile o : list_profile)
				if (o.getEnabler() instanceof Hmenu)
					rez.add((Hmenu) o.getEnabler());
		} catch (Exception e) {
		}
		return rez;
	}
//Checked
	@SuppressWarnings("unchecked")
	private List<Action> getEnabledActions() {
		List<Action> l = new ArrayList<Action>();
		List<Hactionright> heads = new ArrayList<Hactionright>();
		actionrights.setRights(this);
		for (Sprofile o : list_profile)
			if (o.getEnabler() instanceof Hactionright) {
				heads.add((Hactionright)o.getEnabler());
//				actionrights.setObject(o.getEnabler());
//				for (Sactionright s : (List<Sactionright>) actionrights
//						.getSpecificationLines())
//					l.add((Action) s.getEnabledAction());
			}
		
		return heads.size() > 0 ? actionrights.getAction(heads): new ArrayList<Action>();
	}

	@SuppressWarnings("unchecked")
	private List<Juridical> getContractorright(){
		List<Juridical> l = new ArrayList<Juridical>();
		List<Hcontractorright> heads = new ArrayList<Hcontractorright>();
		contractorrights.setRights(this);
		for (Sprofile o : list_profile)
			if (o.getEnabler() instanceof Hcontractorright) {
				heads.add((Hcontractorright)o.getEnabler());
//				contractorrights.setObject(o.getEnabler());
//				for (Scontractorright s : (List<Scontractorright>) contractorrights.getSpecificationLines())
//					l.add((Juridical) s.getEnabledContractor());
			}
		for (Contractor  s : contractorrights.getContractorByHeads(heads))
			l.add((Juridical) s);
		return l;
	}
	
	

	@SuppressWarnings("unchecked")
	private List<Kindofprice> getEnabledKindofprices() {
		List<Kindofprice> l = new ArrayList<Kindofprice>();
		List<Hkindofpriceright> heads = new ArrayList<Hkindofpriceright>();
		kindofpricerights.setRights(this);
		for (Sprofile o : list_profile)
			if (o.getEnabler() instanceof Hkindofpriceright) {
				heads.add((Hkindofpriceright)o.getEnabler());
//				kindofpricerights.setObject(o.getEnabler());
//				for (Skindofpriceright s : (List<Skindofpriceright>) kindofpricerights
//						.getSpecificationLines())
//					l.add((Kindofprice) s.getEnabledKindofprice());
				
				
			}
		return heads.size() > 0 ? kindofpricerights.getEnabledKindofprice(heads) : new ArrayList<Kindofprice>();
	}

	
//	@SuppressWarnings({ "unchecked", "unused" })
//	private List<Srsssubscription> getEnabledRssSubscription(){
//		List<Srsssubscription> l = new ArrayList<Srsssubscription>();
//		rsssubscriptions.setRights(this);		
//		
//		for (Sprofile o : (List<Sprofile>) getSpecificationLines())
//			if (o.getEnabler() instanceof Hrsssubscription){
//				System.out.println("-------------------------------------------");
//				rsssubscriptions.setObject(o.getEnabler());
//				l.addAll((List<Srsssubscription>)rsssubscriptions.getSpecificationLines());
//				
//			}
//		return l;
//	}
	//Checked
	@SuppressWarnings({ "unchecked", "unused" })
	private List<Hcategorygroup> getEnabledHcategorygroup(){
		List<Hcategorygroup> l = new ArrayList<Hcategorygroup>();
		for (Sprofile o : list_profile)
			if (o.getEnabler() instanceof Hcategorygroup)
				l.add((Hcategorygroup)o.getEnabler());
		return l;
	}
	
	@SuppressWarnings("unchecked")
	private List<Currency> getEnabledCurrencys() {
		List<Currency> l = new ArrayList<Currency>();
		List<Hcurrencyright> heads = new ArrayList<Hcurrencyright>();
		currencyrights.setRights(this);
		for (Sprofile o : list_profile)
			if (o.getEnabler() instanceof Hcurrencyright) {
				heads.add((Hcurrencyright)o.getEnabler());
//				currencyrights.setObject(o.getEnabler());
//				for (Scurrencyright s : (List<Scurrencyright>) currencyrights
//						.getSpecificationLines())
//					l.add((Currency) s.getEnabledCurrency());
				
			}
		return heads.size() > 0 ? currencyrights.getCurrency(heads) : new ArrayList<Currency>();
	}

	@Override
	public List getAll() {
		List<Hprofile> l = new ArrayList<Hprofile>();
		l.add(getObject());
		return l;
	}

	public void addProfiles() {
		Hprofile hprofile;
		Sprofile sprofile;
		Long SprofileConsts[] = { 114L, 163L, 363L, 365L, 185L };
		if (!getConfig().isAdmin())
			return;
		for (Ubiq o : opers.getAll()) {
			Oper oper = (Oper) o;

			if (getByOperReceiver(oper) == null) {
				hprofile = new Hprofile();
				hprofile.setFoundation("Autogeneraterd profile");
				hprofile.setOperReceiver(oper);
				
//				TODO Cannot cast from Hprofile to Juridica
//				hprofile
//						.setContractorOwner((Juridical) profiles.getByUId(161L));
				profiles.setRights(this);
				profiles.setObject(hprofile);
				profiles.create();

				for (Long UIDElem : SprofileConsts){
					sprofile = new Sprofile();
					sprofile.setEnabler((Hsystem) profiles.getByUId(UIDElem));
					profiles.setSpecificationLine(sprofile);
					try {
						profiles.createSpecificationLine();
					} catch (JBException e) {
						// TODO Auto-generated catch block
						getLog().error("",e);
					}
				}
			}
		}
	}

	public Action getActionByViewId(String VId) {
		return actions.getByUrl(VId.replaceAll(".xhtml", ".seam"));
	}
	
	public void setDivisionJuridical(SdivisionJuridical dj){
		config.setDivisionjuridical(dj);
	}
	
	public SdivisionJuridical getDivisionJuridical(){
		return config.getDivisionjuridical();
	}

}
