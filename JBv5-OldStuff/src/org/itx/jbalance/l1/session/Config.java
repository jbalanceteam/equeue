package org.itx.jbalance.l1.session;

import java.util.Date;
import java.util.List;

import org.itx.jbalance.l0.h.Hcategorygroup;
import org.itx.jbalance.l0.o.Action;
import org.itx.jbalance.l0.o.Currency;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.o.Kindofprice;
import org.itx.jbalance.l0.o.Oper;
import org.itx.jbalance.l0.s.SdivisionJuridical;
//import org.itx.jbalance.l0.s.Srsssubscription;


public class Config extends Abstractconfig{
	
	public final static String NAME_MODULE = "CORE";
	
	private Oper oper;

	private Date LoginDate;

	private Date PeriodFrom;

	private Date PeriodTo;
	
	private SdivisionJuridical divisionjuridical; 
	
	private List<Action> EnabledActions;

	private List<Juridical> ContractorsOwner;

	private List<Juridical> SelectedContractorOwner;

	private List<Kindofprice> EnabledKindofprices;
	
	private List<Currency> EnabledCurrencys;
	
//	private List<Srsssubscription> EnabledSrssSubscription;
	
	private List<Hcategorygroup> EnabledHcategorygroup;

	private boolean ShowImages;
	
	private boolean ShowDescriptions;
	
	private Long RowsOnPage=20L;
	
	private Long VisualsOnPage=15L;
	
	private String Skin;
	
	private String editewiki;
	
	private String viewwiki;
	
	private String hostwiki;
	
	private String htmlwiki;
	
	

	public String getHtmlwiki() {
		return htmlwiki;
	}

	public void setHtmlwiki(String htmlwiki) {
		this.htmlwiki = htmlwiki;
	}

	public String getSkin() {
		return Skin;
	}

	public void setSkin(String skin) {
		Skin = skin;
	}

	public Date getLoginDate() {
		return LoginDate;
	}

	public void setLoginDate(Date loginDate) {
		LoginDate = loginDate;
	}

	public Oper getOper() {
		return oper;
	}

	public void setOper(Oper oper) {
		this.oper = oper;
	}

	public Date getPeriodFrom() {
		return PeriodFrom;
	}

	public void setPeriodFrom(Date periodFrom) {
		PeriodFrom = periodFrom;
	}

	public Date getPeriodTo() {
		return PeriodTo;
	}

	public void setPeriodTo(Date periodTo) {
		PeriodTo = periodTo;
	}

	public boolean isAdmin() {
// TODO: Реализовать группу администраторов
		//return true;
		return oper.getVersionRoot().getUId()==1;
	}

	public boolean isLogin() {
		return oper!=null;
	}

	public boolean isPeriodExpired(Date D) {
		return D.before(new Date(System.currentTimeMillis() - 4 * 86400000));
	}

	public List<Juridical> getContractorsOwner() {
		return ContractorsOwner;
	}

	public void setContractorsOwner(List<Juridical> contractorsOwner) {
		ContractorsOwner = contractorsOwner;
	}

	public List<Juridical> getSelectedContractorOwner() {
		return SelectedContractorOwner;
	}

	public void setSelectedContractorOwner(List<Juridical> selectedContractorOwner) {
		SelectedContractorOwner = selectedContractorOwner;
	}

	public boolean isShowDescriptions() {
		return ShowDescriptions;
	}

	public void setShowDescriptions(boolean showDescriptions) {
		ShowDescriptions = showDescriptions;
	}

	public boolean isShowImages() {
		return ShowImages;
	}

	public void setShowImages(boolean showImages) {
		ShowImages = showImages;
	}

	public Long getRowsOnPage() {
		return RowsOnPage;
	}

	public void setRowsOnPage(Long rowsOnPage) {
		RowsOnPage = rowsOnPage;
	}

	public Long getVisualsOnPage() {
		return VisualsOnPage;
	}

	public void setVisualsOnPage(Long visualsOnPage) {
		VisualsOnPage = visualsOnPage;
	}

	public List<Action> getEnabledActions() {
		return EnabledActions;
	}

	public void setEnabledActions(List<Action> actions) {
		EnabledActions = actions;
	}

	public List<Currency> getEnabledCurrencys() {
		return EnabledCurrencys;
	}

	public void setEnabledCurrencys(List<Currency> enabledCurrencys) {
		EnabledCurrencys = enabledCurrencys;
	}

	public List<Kindofprice> getEnabledKindofprices() {
		return EnabledKindofprices;
	}

	public void setEnabledKindofprices(List<Kindofprice> enabledKindofprices) {
		EnabledKindofprices = enabledKindofprices;
	}

	public Date getDateTime(){
		return new Date(System.currentTimeMillis());
	}

	public SdivisionJuridical getDivisionjuridical() {
		return divisionjuridical;
	}

	public void setDivisionjuridical(SdivisionJuridical divisionjuridical) {
		this.divisionjuridical = divisionjuridical;
	}

//	public List<Srsssubscription> getEnabledSrssSubscription() {
//		return EnabledSrssSubscription;
//	}
//
//	public void setEnabledSrssSubscription(
//			List<Srsssubscription> enabledSrssSubscription) {
//		EnabledSrssSubscription = enabledSrssSubscription;
//	}

	public List<Hcategorygroup> getEnabledHcategorygroup() {
		return EnabledHcategorygroup;
	}

	public void setEnabledHcategorygroup(List<Hcategorygroup> enabledHcategorygroup) {
		EnabledHcategorygroup = enabledHcategorygroup;
	}

	public String getHostwiki() {
		return hostwiki;
	}

	public void setHostwiki(String hostwiki) {
		this.hostwiki = hostwiki;
	}

	public String getEditewiki() {
		return editewiki;
	}

	public void setEditewiki(String editewiki) {
		this.editewiki = editewiki;
	}

	public String getViewwiki() {
		return viewwiki;
	}

	public void setViewwiki(String viewwiki) {
		this.viewwiki = viewwiki;
	}


}
