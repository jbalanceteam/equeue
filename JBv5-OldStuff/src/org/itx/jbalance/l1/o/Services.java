package org.itx.jbalance.l1.o;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Service;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Services extends CommonObject {

	public Service getObject();
}
