package org.itx.jbalance.l1.o;

import javax.ejb.Stateful;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Physicals/local")
@RemoteBinding(jndiBinding = "Physicals/remote")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class PhysicalsBean extends CommonObjectBean<Physical> implements Physicals ,PhysicalsRemote{

	private static final long serialVersionUID = -9035535178949572016L;

	public PhysicalsBean() {
		super(new Physical());
	}


	public Physical getObject(){
	   	return (Physical) super.getObject();
	}
	
	public void AddPhysical(Physical p){
		super.setObject(p);
		super.create();
	}
	
	
	public void UpdatePhysical(Physical p){
		Physical value = manager.find(Physical.class, p.getUId());
		value.setName(p.getName());
		value.setSurname(p.getSurname());
		value.setPatronymic(p.getPatronymic());
		value.setEMail(p.getEMail());
		value.setPhone(p.getPhone());
		value.setFax(p.getFax());
		value.setBirthday(p.getBirthday());
		setObject(value);
		try{update();}
		catch (JBException e){
			getLog().error("",e);
		}
		
	}
	
	
	
}
