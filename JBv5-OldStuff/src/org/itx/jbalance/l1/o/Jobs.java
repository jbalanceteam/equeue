package org.itx.jbalance.l1.o;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Job;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Jobs extends CommonObject
{
//	public List<Job> getAll();
	public Job getObject();
	public void add(Job job);
	public void update(Job job);
	public void del(Job job);
}
