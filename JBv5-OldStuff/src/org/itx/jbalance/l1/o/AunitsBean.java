package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.o.Warehouse;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
//@LocalBinding(jndiBinding = "Aunits/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class AunitsBean extends CommonObjectBean implements Aunits {

	private static final long serialVersionUID = -5015917301267038613L;

	public AunitsBean() {
		super(new Aunit());
	}


	public Aunit getObject() {
		return (Aunit) super.getObject();
	}

	@SuppressWarnings("unchecked")
	public List<Aunit> getAll() {
		Long naus=null;
		String sdoc=getObject().getSearchString();
		if (sdoc == null || sdoc.length() == 0)
			return (List<Aunit>) manager.createNamedQuery("Aunit.All")
					.getResultList();
		else {
			try {
				naus=Long.valueOf(sdoc);
 			} catch (Exception e) {
			}
			return (List<Aunit>) manager.createNamedQuery(
					"Aunit.Search").setParameter("Pref",
					naus).setParameter("Pref1",
							"%" + sdoc + "%").getResultList();
		}
	}
	

}
