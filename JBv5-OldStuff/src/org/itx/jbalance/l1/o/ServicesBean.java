package org.itx.jbalance.l1.o;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Service;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
//@LocalBinding(jndiBinding = "Services/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class ServicesBean extends CommonObjectBean implements Services {

	private static final long serialVersionUID = -2506608275469651921L;

	public ServicesBean() {
		super(new Service());
	}


	public Service getObject() {
		return (Service) super.getObject();
	}

}
