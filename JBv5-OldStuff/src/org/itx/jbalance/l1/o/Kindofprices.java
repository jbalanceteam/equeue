package org.itx.jbalance.l1.o;


import javax.ejb.Local;

import org.itx.jbalance.l0.o.Kindofprice;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Kindofprices extends CommonObject {

	public Kindofprice getObject();

}
