package org.itx.jbalance.l1.o;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Action;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Actions/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class ActionsBean extends CommonObjectBean <Action> implements Actions {

	@Override
	public void newObject() {
		super.newObject();
		getObject().setImage("blank.png");
	}

	private static final long serialVersionUID = 797832421329471932L;

	public ActionsBean() {
		super(new Action());
	}


	public Action getObject() {
		return (Action) super.getObject();
	}

	public Action getByUrl(String Url) {
		try{
		return (Action)manager.createNamedQuery("Action.ByUrl")
		.setParameter("Pref",Url)
		.getResultList().get(0);
		} catch(Exception e){
			return null;
		}
	}
}