package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Packet;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class PacketsBean extends CommonObjectBean implements Packets {

	private static final long serialVersionUID = -5015917301267038613L;

	public PacketsBean() {
		super(new Packet());
	}


	public Packet getObject() {
		return (Packet) super.getObject();
	}

	@SuppressWarnings("unchecked")
	public List<Packet> getAll() {
		Long naus=null;
		String sdoc=getObject().getSearchString();
		if (sdoc == null || sdoc.length() == 0)
			return (List<Packet>) manager.createNamedQuery("Packet.All")
					.getResultList();
		else {
			try {
				naus=Long.valueOf(sdoc);
 			} catch (Exception e) {
			}
			return (List<Packet>) manager.createNamedQuery(
					"Packet.Search").setParameter("Pref",
					naus).setParameter("Pref1",
							"%" + sdoc + "%").getResultList();
		}
	}
	

}
