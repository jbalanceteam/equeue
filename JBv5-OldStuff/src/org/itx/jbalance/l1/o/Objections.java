package org.itx.jbalance.l1.o;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Objection;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Objections extends CommonObject {

		Objection getObject();
}
