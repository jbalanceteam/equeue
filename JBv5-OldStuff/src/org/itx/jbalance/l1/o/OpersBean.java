package org.itx.jbalance.l1.o;

import java.security.MessageDigest;
import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Oper;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
import org.jboss.security.Util;


@Stateful
@LocalBinding(jndiBinding = "Opers/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class OpersBean extends CommonObjectBean<Oper> implements Opers {

	private static final long serialVersionUID = 266450029800037219L;
	
	public OpersBean() {
		super(new Oper());
	}




	@SuppressWarnings("unchecked")
	public List<Oper> getByLogin(String Value) {
		return (List<Oper>) manager.createNamedQuery("Oper.ByLogin")
				.setParameter("Login", Value).getResultList();
	}

	public Oper getObject() {
		return (Oper)super.getObject();
	}
	
	@Override
	public void create() {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (Exception e) {
			getLog().error("",e);
		}
		byte[] passwordBytes = getObject().getPassphrase().getBytes();
		byte[] hash = md.digest(passwordBytes);
		getObject().setPassphrase(Util.encodeBase64(hash));
		getObject().setRoles("JBalance");
		super.create();
	}

	@Override
	public void update() throws JBException {
//		System.out.println("____________________________________"+getObject().getPassphrase());
		if(getObject().getPassphrase().length()!=0){
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (Exception e) {
			getLog().error("",e);
		}
		byte[] passwordBytes = getObject().getPassphrase().getBytes();
		byte[] hash = md.digest(passwordBytes);
		getObject().setPassphrase(Util.encodeBase64(hash));
		getObject().setRoles("JBalance");
		}
//		System.out.println("____________________________________"+getObject().getPassphrase());
		super.update();
	}

}
