package org.itx.jbalance.l1.o;

import java.util.List;
import javax.ejb.Local;

import org.itx.jbalance.l0.o.Imago;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Imagos extends CommonObject{
	public Imago getObject();
	public List<Imago> getAll();
}
