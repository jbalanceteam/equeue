package org.itx.jbalance.l1.o;

import java.util.List;
import javax.ejb.Local;

import org.itx.jbalance.l0.o.Oper;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Opers extends CommonObject <Oper>{

	public List<Oper> getByLogin(String Value);
	
	public Oper getObject();
}
