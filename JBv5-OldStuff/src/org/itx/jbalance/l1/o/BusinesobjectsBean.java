package org.itx.jbalance.l1.o;

import javax.ejb.Stateful;

import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
//@LocalBinding(jndiBinding = "Businesobjects/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class BusinesobjectsBean extends CommonObjectBean implements
		Businesobjects {

	private static final long serialVersionUID = 797832421329471932L;

	public BusinesobjectsBean() {
		super(new org.itx.jbalance.l0.o.Businesobject());
	}


	public org.itx.jbalance.l0.o.Businesobject getObject() {
		
		return (org.itx.jbalance.l0.o.Businesobject) super.getObject();
	}
}