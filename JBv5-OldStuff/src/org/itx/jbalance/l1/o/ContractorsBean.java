package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.itx.jbalance.l1.o.Contractors;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Contractors/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class ContractorsBean extends CommonObjectBean implements Contractors {

	private static final long serialVersionUID = 6530056866449195444L;

	public ContractorsBean() {
		super(new Contractor());
	}

	public Contractor getObject() {
		return (Contractor) super.getObject();
	}

	@SuppressWarnings("unchecked")
	public List<Contractor> getRangeWSearch(Long first, Long last, Contractor contractor)
	{
		setObject(contractor);
		return (List<Contractor>)getRange(first, last);
	}
}
