package org.itx.jbalance.l1.o;


import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Kindofprice;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Kindofprices/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class KindofpricesBean extends CommonObjectBean implements Kindofprices {

	private static final long serialVersionUID = 6225343036574032259L;


	public KindofpricesBean() {
		super(new Kindofprice());
	}

	public Kindofprice getObject() {
		return (Kindofprice) super.getObject();
	}

}
