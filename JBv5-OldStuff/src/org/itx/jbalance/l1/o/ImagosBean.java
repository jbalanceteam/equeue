package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Imago;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Imagos/local")
public class ImagosBean extends CommonObjectBean implements Imagos {

	private static final long serialVersionUID = -5308380345049039854L;
	
	public ImagosBean() {
		super(new Imago());
	}
	
	public Imago getObject(){
		return (Imago)super.getObject();
	}
	
	@SuppressWarnings("unchecked")
	public List<Imago> getAll(){
		Long naus=null;
		String sdoc=getObject().getSearchString();
		if (sdoc == null || sdoc.length() == 0)
			return (List<Imago>) manager.createNamedQuery("Imago.All").getResultList();
		else {
			try {
				naus=Long.valueOf(sdoc);
 			} catch (Exception e) {
			}
			return (List<Imago>) manager.createNamedQuery(
					"Imago.Search").setParameter("Pref",
					naus).getResultList();
		}
	}

}
