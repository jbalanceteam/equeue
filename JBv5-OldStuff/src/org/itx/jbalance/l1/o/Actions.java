package org.itx.jbalance.l1.o;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Action;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Actions extends CommonObject<Action> {
	
	
	public Action getObject();

	public Action getByUrl(String Url);
}
