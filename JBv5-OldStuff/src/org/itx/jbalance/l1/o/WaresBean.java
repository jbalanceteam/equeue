package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Ware;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
//@LocalBinding(jndiBinding = "Wares/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class WaresBean extends CommonObjectBean implements Wares {

	private static final long serialVersionUID = 6225343036574032259L;

	public WaresBean() {
		super(new Ware());
	}

	public Ware getObject() {
		return (Ware) super.getObject();
	}
	
	@SuppressWarnings("unchecked")
	public List<Ware> getAll() {
		Long naus=null;
		String sdoc=getObject().getSearchString();
		if (sdoc == null || sdoc.length() == 0)
			return (List<Ware>) manager.createNamedQuery("Ware.All")
					.getResultList();
		else {
			try {
				naus=Long.valueOf(sdoc);
 			} catch (Exception e) {
			}
			return (List<Ware>) manager.createNamedQuery(
					"Ware.Search").setParameter("Pref",
					naus).setParameter("Pref1",
							"%" + sdoc + "%").getResultList();
		}
	}

}
