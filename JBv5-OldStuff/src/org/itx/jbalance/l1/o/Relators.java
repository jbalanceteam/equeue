package org.itx.jbalance.l1.o;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Relator;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Relators extends CommonObject {
	
	public Relator getObject();
	
}
