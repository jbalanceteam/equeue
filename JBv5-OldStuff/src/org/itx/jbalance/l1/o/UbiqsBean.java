package org.itx.jbalance.l1.o;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;


@Stateful
//@LocalBinding(jndiBinding = "Ubiqs/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class UbiqsBean extends CommonObjectBean implements Ubiqs {

	private static final long serialVersionUID = -5015917301267038613L;

	public UbiqsBean() {
		super(new Ubiq());
	}


	public Ubiq getObject() {
		return (Ubiq) super.getObject();
	}

	public void search() {

	}

}
