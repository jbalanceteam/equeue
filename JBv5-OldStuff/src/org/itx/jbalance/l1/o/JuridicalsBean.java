package org.itx.jbalance.l1.o;

import java.util.List;


import javax.ejb.Remote;
import javax.ejb.Stateful;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.itx.jbalance.l1.session.SessionprofileRemote;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;


//@Remote(SessionprofileRemote.class)
//@Stateful(name = "Sessionprofile")
//@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 900, removalTimeoutSeconds = 3600)
//@LocalBinding(jndiBinding = "Sessionprofile/local")
//@RemoteBinding(jndiBinding = "Sessionprofile/remote")

@Remote(JuridicalsRemote.class)
@Stateful(name = "Juridicals")
@RemoteBinding(jndiBinding = "Juridicals/remote")
@LocalBinding(jndiBinding = "Juridicals/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class JuridicalsBean extends CommonObjectBean<Juridical> implements Juridicals {

	private static final long serialVersionUID = 5177964569087956836L;

	public JuridicalsBean() {
		super(new Juridical());
	}

	@SuppressWarnings("unchecked")
	public List<Juridical> getByName(String Value) {
		return (List<Juridical>)manager.createNamedQuery("Juridical.ByName").setParameter("Name", Value).getResultList();
	}
	
	public Juridical getObject(){
		return (Juridical)super.getObject();
	}

	@SuppressWarnings("unchecked")
	public List<Juridical> getBySameName(String value) {
		return (List<Juridical>)manager.createNamedQuery("Juridical.SearchSameName").setParameter("Name", value).getResultList();
	}

	public Juridical getJuridical(Juridical j) {
		return (Juridical)manager.find(Juridical.class, j.getUId());
	}
}
