package org.itx.jbalance.l1.o;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Currency;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Currencys extends CommonObject {
	public Currency getObject();

}
