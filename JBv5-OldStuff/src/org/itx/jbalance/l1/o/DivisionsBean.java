package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Stateful;
import org.itx.jbalance.l0.o.Division;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Divisions/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class DivisionsBean extends CommonObjectBean implements Divisions
{
	private static final long serialVersionUID = -3174607204964617259L;

	public DivisionsBean(){
		super(new Division());
	}
	
	public Division getObject(){
		return (Division)super.getObject();
	}
	
	@SuppressWarnings("unchecked")
	public List<Division> getAll(){
		Long naus=null;
		String sdoc=getObject().getSearchString();
		if (sdoc == null || sdoc.length() == 0)
			return (List<Division>) manager.createNamedQuery("Division.All").getResultList();
		else {
			try {
				naus=Long.valueOf(sdoc);
 			} catch (Exception e) {
			}
			return (List<Division>) manager.createNamedQuery(
					"Division.Search").setParameter("Pref",
					naus).getResultList();
		}
	}
}
