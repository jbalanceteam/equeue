package org.itx.jbalance.l1.o;

import java.util.List;
import javax.ejb.Stateful;
import org.itx.jbalance.l0.o.Group_;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;


@Stateful
@LocalBinding(jndiBinding = "Groups/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class GroupsBean extends CommonObjectBean implements Groups
{
	private static final long serialVersionUID = -3174607204964617259L;

	@SuppressWarnings("unchecked")
	public List<Group_> getAll()
	{
		String sdoc=getObject().getSearchString();
		if (sdoc == null || sdoc.length() == 0)
			return (List<Group_>) manager.createNamedQuery("Group_.All")
					.getResultList();
		else
			return (List<Group_>) manager.createNamedQuery("Group_.Search")
					.setParameter("Pref","%" + sdoc + "%")
					.getResultList();
	}

	public GroupsBean(){
		super(new Group_());
	}

	public Group_ getObject(){
		return (Group_)super.getObject();
	}

	public void add(Group_ group){
		setObject(group);
		create();
	}

	public void del(Group_ group){
		group = manager.find(Group_.class, group.getUId());
		setObject(group);
		try{
			delete();
		}
		catch (JBException e){
			getLog().error("",e);
		}
	}

	public void update(Group_ group){
		Group_ value = manager.find(Group_.class, group.getUId());
		value.setName(group.getName());
		setObject(value);
		try{
			update();
		}
		catch (JBException e){
			getLog().error("",e);
		}
	}
}
