package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Measurements/local")
public class MeasurementsBean extends CommonObjectBean implements Measurements {

	private static final long serialVersionUID = -5308380345049039854L;
	
	public MeasurementsBean() {
		super(new Measurement());
	}
	
	public Measurement getObject(){
		return (Measurement)super.getObject();
	}
	
	@SuppressWarnings("unchecked")
	public List<Measurement> getAll(){
		Long naus=null;
		String sdoc=getObject().getSearchString();
		if (sdoc == null || sdoc.length() == 0)
			return (List<Measurement>) manager.createNamedQuery("Measurement.All").getResultList();
		else {
			try {
				naus=Long.valueOf(sdoc);
 			} catch (Exception e) {
			}
			return (List<Measurement>) manager.createNamedQuery(
					"Measurement.Search").setParameter("Pref",
					naus).getResultList();
		}
	}

}
