package org.itx.jbalance.l1.o;



import javax.ejb.Local;

import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Physicals extends CommonObject<Physical> {
	public void AddPhysical(Physical p);
	public void UpdatePhysical(Physical p);

}
