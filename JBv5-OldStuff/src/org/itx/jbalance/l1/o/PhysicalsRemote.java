package org.itx.jbalance.l1.o;



import javax.ejb.Remote;

import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l1.common.CommonObject;

@Remote
public interface PhysicalsRemote extends CommonObject<Physical> {
	public void AddPhysical(Physical p);
	public Physical getObject();
	public void UpdatePhysical(Physical p);

}
