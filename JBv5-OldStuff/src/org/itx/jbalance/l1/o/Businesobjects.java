package org.itx.jbalance.l1.o;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Businesobject;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Businesobjects extends CommonObject {
	
	
	public Businesobject getObject();
}
