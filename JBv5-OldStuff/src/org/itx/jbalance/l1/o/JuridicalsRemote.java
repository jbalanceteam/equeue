package org.itx.jbalance.l1.o;

import java.util.List;
import javax.ejb.Remote;

import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l1.common.CommonObject;

@Remote
public interface JuridicalsRemote extends CommonObject {
	public List<Juridical> getByName(String Value);
	public Juridical getObject();
		
	public List<Juridical> getBySameName(String value);
	
	public Juridical getJuridical(Juridical j);
}
