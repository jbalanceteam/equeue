package org.itx.jbalance.l1.o;

import java.util.List;
import javax.ejb.Local;

import org.itx.jbalance.l0.o.Employee;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Employees extends CommonObject {
	//public List<Employee> getAll();

	public Employee getObject();
	
	
}
