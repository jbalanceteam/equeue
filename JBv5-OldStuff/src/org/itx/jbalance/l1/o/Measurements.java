package org.itx.jbalance.l1.o;

import java.util.List;
import javax.ejb.Local;

import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Measurements extends CommonObject{
	public Measurement getObject();
	public List<Measurement> getAll();
}
