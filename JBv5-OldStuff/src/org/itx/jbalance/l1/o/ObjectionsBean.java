package org.itx.jbalance.l1.o;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Objection;
import org.itx.jbalance.l1.common.CommonObjectBean;

import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Objections/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class ObjectionsBean extends CommonObjectBean implements Objections{

	private static final long serialVersionUID = 1L;

	public ObjectionsBean() {
		super(new Objection());
	}
	
	public Objection getObject() {
		return (Objection)super.getObject();
	}
}
