package org.itx.jbalance.l1.o;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Currency;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Currencys/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class CurrencysBean extends CommonObjectBean implements Currencys {

	private static final long serialVersionUID = 6225343036574032259L;


	public CurrencysBean() {
		super(new Currency());
	}

	public Currency getObject() {
		return (Currency) super.getObject();
	}

}
