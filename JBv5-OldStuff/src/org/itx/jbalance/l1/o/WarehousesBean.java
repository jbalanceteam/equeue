package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Warehouse;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;


@Stateful
//@LocalBinding(jndiBinding = "Warehouses/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class WarehousesBean extends CommonObjectBean implements Warehouses {

	public WarehousesBean() {
		super(new Warehouse());
	}

	private static final long serialVersionUID = -7598036366719951243L;

	@SuppressWarnings("unchecked")
	public List<Warehouse> getAll() {
		Long nwhs=null;
		String sdoc=getObject().getSearchString();
		if (sdoc == null || sdoc.length() == 0)
			return (List<Warehouse>) manager.createNamedQuery("Warehouse.All")
					.getResultList();
		else {
			try {
				nwhs=Long.valueOf(sdoc);
 			} catch (Exception e) {
			}
			return (List<Warehouse>) manager.createNamedQuery(
					"Warehouse.Search").setParameter("Pref",
					nwhs).setParameter("Pref1",
							"%" + sdoc + "%").getResultList();
		}
	}

	@SuppressWarnings("unchecked")
	public Warehouse getZero(){
		return ((List<Warehouse>)manager.createNamedQuery("Warehouse.getByZero").getResultList()).get(0);
	}

	public Warehouse getObject() {
		return (Warehouse) super.getObject();
	}

}
