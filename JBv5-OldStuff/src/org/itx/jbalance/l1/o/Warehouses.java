package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Warehouse;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Warehouses extends CommonObject {
	public List<Warehouse> getAll();
	public Warehouse getObject();
	public Warehouse getZero();
}
