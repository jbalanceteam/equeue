package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Aunits extends CommonObject {
	
	public Aunit getObject();
	public List<Aunit> getAll();

}
