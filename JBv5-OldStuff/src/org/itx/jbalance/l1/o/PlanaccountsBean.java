package org.itx.jbalance.l1.o;

import java.util.List;
import javax.ejb.Stateful;
import org.itx.jbalance.l0.o.Planaccount;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Planaccounts/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class PlanaccountsBean extends CommonObjectBean implements Planaccounts {

	private static final long serialVersionUID = 797832421329471932L;

	@Override
	public void newObject() {
		super.newObject();
	}
	
	public enum TypeAccounts {NA, SUB_NA, SUB_CONTRA};
	
	
	@SuppressWarnings("unchecked")
	public List<Planaccount> getAllAccounts(){
		return (List<Planaccount>)manager.createNamedQuery("Planaccount.AllAccounts").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private Planaccount getByNumberAccounts(Integer number){
		List<Planaccount> list = (List<Planaccount>)manager.createNamedQuery("Planaccount.getByNumberAccounts")
													.setParameter("NUMBER", number)
													.getResultList();
		if(list.size() > 0) return list.get(0);
		return null;
	}
	
	@SuppressWarnings("unchecked")
	private Planaccount getByNumberSubAccounts(Integer number, Integer sub_number){
		List<Planaccount> list = (List<Planaccount>)manager.createNamedQuery("Planaccount.getByNumberSubAccounts")
													.setParameter("NUMBER", number)
													.setParameter("SUB_NUMBER", sub_number)
													.getResultList();
		if(list.size() > 0) return list.get(0);
		return null;
	}
	

	@SuppressWarnings("unchecked")
	private Planaccount getByNumberSubContra(Integer number, Integer sub_number, Integer sub_contra){
		List<Planaccount> list = (List<Planaccount>)manager.createNamedQuery("Planaccount.getByNumberSubAccounts")
													.setParameter("NUMBER", number)
													.setParameter("SUB_NUMBER", sub_number)
													.setParameter("SUB_CONTRA", sub_contra)
													.getResultList();
		if(list.size() > 0) return list.get(0);
		return null;
	}

	@Override
	public synchronized void create(){
		super.create();
	}
	
	public Planaccount getNA(Planaccount na){
		TypeAccounts type = null;
		if(na.getNumber_accounts() != null && na.getNumber_subaccounts() == null && na.getNumber_subcontra() == null)
			type = TypeAccounts.NA;
		else if(na.getNumber_accounts() != null && na.getNumber_subaccounts() != null && na.getNumber_subcontra() == null)
			type = TypeAccounts.SUB_NA;
		else if(na.getNumber_accounts() != null && na.getNumber_subaccounts() != null && na.getNumber_subcontra() != null)
			type = TypeAccounts.SUB_CONTRA;
		switch (type) {
			case NA:
				na = getByNumberAccounts(na.getNumber_accounts());
				break;
			case SUB_NA:
				na = getByNumberSubAccounts(na.getNumber_accounts(), na.getNumber_subaccounts());
				break;
			case SUB_CONTRA:
				na = getByNumberSubContra(na.getNumber_accounts(), na.getNumber_subaccounts(), na.getNumber_subcontra());
				break;				
			default:
				break;
		}
		return na;
	}
	
	
}