package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Packet;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Packets extends CommonObject {
	
	public Packet getObject();
	public List<Packet> getAll();

}
