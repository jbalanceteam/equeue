package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.o.Division;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Divisions extends CommonObject{
	public Division getObject();
	public List<Division> getAll();
}
