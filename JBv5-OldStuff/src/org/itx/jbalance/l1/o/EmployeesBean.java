package org.itx.jbalance.l1.o;

import javax.ejb.Stateful;
import org.itx.jbalance.l0.o.Employee;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Employees/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class EmployeesBean extends CommonObjectBean implements Employees {

	private static final long serialVersionUID = 266450029800037219L;

	public EmployeesBean() {
		super(new Employee());
//		new NoClassDefFoundError();
	}

//	@SuppressWarnings("unchecked")
//	public List<Employee> getAll() {
//		String sdoc=getObject().getSearchString();
//		if (sdoc == null || sdoc.length() == 0)
//			return (List<Employee>) manager.createNamedQuery("Employee.All")
//					.getResultList();
//		else
//			return (List<Employee>) manager.createNamedQuery("Employee.Search")
//					.setParameter("Pref","%" + sdoc + "%")
//					.getResultList();
//	}

	public Employee getObject() {
		return (Employee) super.getObject();
	}

}
