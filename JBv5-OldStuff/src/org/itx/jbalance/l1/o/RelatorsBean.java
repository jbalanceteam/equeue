package org.itx.jbalance.l1.o;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Relator;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
//@LocalBinding(jndiBinding = "Relators/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class RelatorsBean extends CommonObjectBean implements Relators {

	private static final long serialVersionUID = 7703307604825670954L;
	
	public RelatorsBean() {
		super(new Relator());
	}
	

	public Relator getObject() {
		return (Relator) super.getObject();
	}

}
