package org.itx.jbalance.l1.o;

import java.util.List;
import javax.ejb.Local;
import org.itx.jbalance.l0.o.Group_;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface Groups extends CommonObject{
	public List<Group_> getAll();
	public Group_ getObject();
	public void add(Group_ group);
	public void del(Group_ group);
	public void update(Group_ group);
}
