package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.o.Job;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Jobs/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class JobsBean extends CommonObjectBean implements Jobs
{
	private static final long serialVersionUID = -3174607204964617259L;

	@SuppressWarnings("unchecked")
	public List<Job> getAll()
	{
		String sdoc=getObject().getSearchString();
		if (sdoc == null || sdoc.length() == 0)
			return (List<Job>) manager.createNamedQuery("Job.All")
					.getResultList();
		else
			return (List<Job>) manager.createNamedQuery("Job.Search")
					.setParameter("Pref","%" + sdoc + "%")
					.getResultList();
	}

	public JobsBean()
	{
		super(new Job());
	}

	public Job getObject()
	{
		return (Job)super.getObject();
	}

	public void add(Job job){
		setObject(job);
		create();
	}

	public void del(Job job)
	{
		Job value = manager.find(Job.class, job.getUId());
		setObject(value);
		try
		{
			delete();
		}
		catch (JBException e)
		{
			getLog().error("",e);
		}
	}

	public void update(Job job)
	{
		try
		{
		Job value = manager.find(Job.class, job.getUId());
		value.setName(job.getName());
		value.setTypeuser(job.getTypeuser());
		setObject(value);
		
			update();
		}
		catch (JBException e)
		{
			getLog().error("",e);
		}
	}
}
