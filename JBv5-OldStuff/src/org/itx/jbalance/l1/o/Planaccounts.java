package org.itx.jbalance.l1.o;

import java.util.List;
import javax.ejb.Local;
import org.itx.jbalance.l0.o.Planaccount;
import org.itx.jbalance.l1.common.CommonObject;
import org.itx.jbalance.l1.o.PlanaccountsBean.TypeAccounts;

@Local
public interface Planaccounts extends CommonObject {
	public List<Planaccount> getAllAccounts();
	public Planaccount getNA(Planaccount na);
	public void create();
}
