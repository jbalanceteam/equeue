package org.itx.jbalance.l1.o;

import javax.ejb.Stateful;
import org.itx.jbalance.l0.o.Typecontract;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Typecontracts/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class TypecontractsBean extends CommonObjectBean implements Typecontracts
{
	private static final long serialVersionUID = -3174607204964617259L;

	public TypecontractsBean(){
		super(new Typecontract());
	}
}
