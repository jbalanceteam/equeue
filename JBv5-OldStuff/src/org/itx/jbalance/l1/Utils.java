package org.itx.jbalance.l1;

import java.lang.reflect.Method;
import java.util.HashMap;
import org.itx.jbalance.l0.Ubiq;

public class Utils {

	static public void copy(Object src, Object dst) {
		HashMap<String, Method> dstHM = new HashMap<String, Method>();
		

		Method md[] = dst.getClass().getMethods();
		for (Method m : md) {
			if ((m.getModifiers() == 1) && (m.getName().startsWith("set")))
				dstHM.put(m.getName().replaceFirst("set", "get"), m);
		}
		Method ms[] = src.getClass().getMethods();
		for (Method m : ms) {
			if ((m.getModifiers() == 1) && (m.getName().startsWith("get"))
					&& dstHM.containsKey(m.getName()))
				try {
					Object o[] = { m.invoke(src, (Object[]) null) };
					dstHM.get(m.getName()).invoke(dst, o);
				} catch (Exception e) {
					System.err
							.println("Busines object reflection error in method: "
									+ m.getName());
				}
		}
		dstHM = null;
	}

	

}


