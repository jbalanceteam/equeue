package org.itx.jbalance.l1.common;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.h.HdivisionJuridical;
import org.itx.jbalance.l0.h.Hdocument;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.s.Sdocument;
import org.itx.jbalance.l1.JBException;

public class CommonDocumentBean <D extends Hdocument,S extends Sdocument>  extends CommonObjectBean<D> implements
		CommonDocument<D,S> {

	private static final long serialVersionUID = 6705628333560203373L;

	private boolean specificationLineDirty = false;

	private boolean specificationDirty = false;

	private S specificationLine;

	public CommonDocumentBean() {
	}

	public CommonDocumentBean(D object, S specificationLine) {
		super(object);
		this.specificationLine = specificationLine;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public synchronized void create(){
		// getLog().debug("Create called");
		// Calculate and set new sequental number
		setupDNumber();
		// save object
		setSpecificationDirty(true);
		setSpecificationLineDirty(true);
		super.create();
	}

	
	protected void setupDNumber() {
		Long i = getMaxNumber();
		i = i == null ? i = 1l : i+1;
		getObject().setDnumber(i);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public synchronized void createSpecificationLine() throws JBException {
		// Calculate and set new sequental number
		Integer i = getMaxSeqNumber();
		i = i == null ? i = new Integer(1) : ++i;
		specificationLine.setSeqNumber(i);
		// save object
		setObjectDirty(true);
		setObjectListDirty(true);
		setSpecificationDirty(true);
		setSpecificationLineDirty(true);
		specificationLine.setHUId(getObject());
		create(specificationLine);
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public synchronized void delete() throws JBException {
		setSpecificationDirty(true);
		setSpecificationLineDirty(true);
		for (S s : (List<S>) getSpecificationLines()) {
			setSpecificationLine(s);
			deleteSpecificationLine();
		}
		super.delete();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public synchronized void deleteSpecificationLine() throws JBException {
		setObjectDirty(true);
		setObjectListDirty(true);
		setSpecificationDirty(true);
		setSpecificationLineDirty(true);
		delete(specificationLine);
		newSpecificationLine();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public synchronized void update() throws JBException {
		setSpecificationDirty(true);
		setSpecificationLineDirty(true);
		super.update();
		specificationLine.setHUId(getObject());
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public synchronized void updateSpecificationLine() throws JBException {
		setObjectDirty(true);
		setObjectListDirty(true);
		setSpecificationDirty(true);
		setSpecificationLineDirty(true);
		specificationLine = (S) update(specificationLine); // 7.
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<S> getSpecificationLines() {
		getLog().debug("getSpecificationLines()  for "+getObject());
		if (getObject()==null || getObject().getUId() == null) {
			return null;
		}
		
		try{
//			return manager.createNamedQuery(getObject().getClass().getSimpleName()+".Specification")
//			.setParameter("Header", getObject()).getResultList();
			
			return manager.createQuery(
			"from S"+getObject().getClass().getSimpleName().substring(1)+" o where o.version is null and o.closeDate is null and o.HUId = :Header order by o.seqNumber"
			).setParameter("Header", getObject()).getResultList();
		
		}catch(Exception Ex){
			getLog().error("",Ex);
			throw new RuntimeException( Ex);
		}
	}
	
	
	/**
	 * This method is universal,
	 * but too slow
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	protected List<S> getSpecificationLinesCommon() {
//		Пиздец шо за запрос сейчас сгенерится :(((
		try{
		return manager.createNamedQuery("Document.Specification")
				.setParameter("Header", getObject()).getResultList();
		} catch (IllegalStateException Ex) {
			getLog().error("", Ex);
			return null;
		}
	}
	

	@Override
	public void newObject() {
		super.newObject();
		newSpecificationLine();
	}

	protected void newSpecificationLine() {
		try {
			specificationLine = (S) specificationLine.getClass().newInstance();
		} catch (InstantiationException e) {
			getLog().error("",e);
		} catch (IllegalAccessException e) {
			getLog().error("",e);
		}
		specificationLine.setSysUser(sessionprofile.getConfig().getOper());
	}

	public D getObject() {
		return (D) super.getObject();
	}

	public boolean isSpecificationDirty() {
		return specificationDirty;
	}

	public void setSpecificationDirty(boolean specificationDirty) {
		this.specificationDirty = specificationDirty;
	}

	public S getSpecificationLine() {
		return specificationLine;
	}

	public void setSpecificationLine(S specificationLine) {
		this.specificationLine = specificationLine;
	}

	public boolean isSpecificationLineDirty() {
		return specificationLineDirty;
	}

	public void setSpecificationLineDirty(boolean specificationLineDirty) {
		this.specificationLineDirty = specificationLineDirty;
	}

	@Override
	public synchronized void processMessage(String MsgAction, Ubiq MsgObject) {
		super.processMessage(MsgAction, MsgObject);
		if (specificationLine == null) {
			setSpecificationDirty(true);
			setSpecificationLineDirty(true);
			return;
		}
		if (specificationLine.getClass().isInstance(MsgObject))
			if (((Sdocument) MsgObject).getHUId().equals(getObject()))
				setSpecificationDirty(true);
		if (MsgObject.equals(specificationLine))
			setSpecificationLineDirty(true);
	}

	@Override
	public void washAllDirty() {
		super.washAllDirty();
		setSpecificationLineDirty(false);
		setSpecificationDirty(false);
	}

	protected Long getMaxNumber() {
		if(getObject().getContractorOwner() != null)
			return (Long) manager.createNamedQuery(
				getObject().getClass().getSimpleName() + ".MaxNumber")
				.setParameter("ContractorOwner",
						getObject().getContractorOwner()).getResultList()
				.get(0);
		else {
			//!!!!!!!!!!!!!!!!!!!
			if(sessionprofile.getDivisionJuridical()==null)return 0l;
			HdivisionJuridical h =  (HdivisionJuridical)sessionprofile.getDivisionJuridical().getHUId();
			return (Long) manager.createNamedQuery(
					getObject().getClass().getSimpleName() + ".MaxNumber")
					.setParameter("ContractorOwner", (Juridical)h.getJuridical()
							).getResultList()
					.get(0);
		}	
	}

	private Integer getMaxSeqNumber() {
		return (Integer) manager.createNamedQuery("Document.MaxSeqNumber")
				.setParameter("Header", getObject()).getResultList().get(0);
	}

	@Override
	public void sync() {
		// getLog().debug("=======================CommonDocument.sync"+getSpecificationLine());
		if (getSpecificationLine() != null)
			if (isSpecificationLineDirty()
					&& getSpecificationLine().getUId() != null)
				manager.find(getSpecificationLine().getClass(),
						getSpecificationLine().getUId());
		super.sync();
	}

	@Override
	public void search() {
		if (getSpecificationLine().getSearchString() != null)
			setSpecificationDirty(true);
		if (getObject().getSearchString() != null)
			super.search();
	}
}
