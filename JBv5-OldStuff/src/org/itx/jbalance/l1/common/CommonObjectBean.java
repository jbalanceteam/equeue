package org.itx.jbalance.l1.common;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l1.JBException;

public class CommonObjectBean<O extends Ubiq> extends CommonBean implements CommonObject<O> {
	protected final static int MAX_SEARCH_STRING_SIZE = 10;

	private boolean objectDirty = false;

	private boolean objectListDirty = false;

	private static final long serialVersionUID = -3166304145281052141L;

	private O object;

	public CommonObjectBean(O object) {

		this.object = object;
	}

	public CommonObjectBean() {

	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public synchronized void create() {
		setObjectDirty(true);
		setObjectListDirty(true);
		create(object);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public synchronized void delete() throws JBException {
		setObjectDirty(true);
		setObjectListDirty(true);
		delete(object);
		newObject();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public synchronized void update() throws JBException {
		getLog().debug("update()");
		setObjectDirty(true);
		setObjectListDirty(true);
		object = (O) update(object);
	}

	public O getObject() {
		return object;
	}

	public void setObject(Ubiq object) {
		this.object = (O) object;
	}

	public synchronized void processMessage(String MsgAction, Ubiq MsgObject) {
		// System.out.println("processMessage MsgAction = "+MsgAction );
		// System.out.println("processMessage MsgObject = "+MsgObject );
		// System.out.println("processMessage object = "+object );
		// System.out.println("processMessage Class =
		// "+this.getClass().getSimpleName() );
		if (object == null) {
			setObjectListDirty(true);
			setObjectDirty(true);
			return;
		}
		if (object.getClass().isInstance(MsgObject)) {
			setObjectListDirty(true);
			// System.out.println("processMessage Instance of
			// "+object.getClass()+" Dirty");
		}
		if (MsgObject.equals(getObject())) {
			setObjectDirty(true);
		}
	}

	public boolean isObjectListDirty() {
		return objectListDirty;
	}

	public void setObjectListDirty(boolean objectListDirty) {
		this.objectListDirty = objectListDirty;
	}

	public boolean isObjectDirty() {
		return objectDirty;
	}

	public void setObjectDirty(boolean objectDirty) {
		this.objectDirty = objectDirty;
	}

	public void newObject() {
		try {
			// System.out.println("CommonObjectBean::newObject create object
			// with class " + object.getClass());
			object = (O) object.getClass().newInstance();
		} catch (InstantiationException e) {
			getLog().error("",e);
		} catch (IllegalAccessException e) {
			getLog().error("",e);
		}
		object.setSysUser(sessionprofile.getConfig().getOper());
	}

	public void washAllDirty() {
		setObjectDirty(false);
		setObjectListDirty(false);
	}

	private Query constructQuery() {
		Query query;
		String sdoc = getObject().getSearchString();

		boolean isSearch = !(sdoc == null || sdoc.length() == 0);
		// При слишком большой поисковой строке получаем ошибку !
		// Caused by: java.sql.DataTruncation: Data truncation
		// at
		// org.firebirdsql.jdbc.field.FBWorkaroundStringField.setString(FBWorkaroundStringField.java:97)
		// at
		// org.firebirdsql.jdbc.AbstractPreparedStatement.setString(AbstractPreparedStatement.java:384)
		// at
		// org.jboss.resource.adapter.jdbc.WrappedPreparedStatement.setString(WrappedPreparedStatement.java:316)
		// at org.hibernate.type.StringType.set(StringType.java:26)
		// at org.hibernate.type.NullableType.nullSafeSet(NullableType.java:136)
		// at org.hibernate.type.NullableType.nullSafeSet(NullableType.java:116)
		// at
		// org.hibernate.param.NamedParameterSpecification.bind(NamedParameterSpecification.java:38)
		// at
		// org.hibernate.loader.hql.QueryLoader.bindParameterValues(QueryLoader.java:491)
		// at
		// org.hibernate.loader.Loader.prepareQueryStatement(Loader.java:1563)
		// at org.hibernate.loader.Loader.doQuery(Loader.java:673)
		// at
		// org.hibernate.loader.Loader.doQueryAndInitializeNonLazyCollections(Loader.java:236)
		// at org.hibernate.loader.Loader.doList(Loader.java:2220)
		if (isSearch && sdoc.length() > MAX_SEARCH_STRING_SIZE)
			sdoc = sdoc.substring(0, MAX_SEARCH_STRING_SIZE);
		if (isSearch)
			query = manager.createNamedQuery(
					getObject().getClass().getSimpleName() + ".Search")
					.setParameter("Pref", "%" + sdoc + "%");
		else
			query = manager.createNamedQuery(getObject().getClass()
					.getSimpleName()
					+ ".All");
		return query;
	}

	public List<? extends O> getAll() {
		List<O> tmp = constructQuery().getResultList();
		return tmp;
	}

	public List<? extends Ubiq> getRange(Long firstRow, Long maxRow) {
		getLog().debug("getRange("+firstRow+","+maxRow+")");
		List tmp = constructQuery().setFirstResult(firstRow.intValue())
				.setMaxResults(maxRow.intValue()).getResultList();
		return tmp;
	}

	public Long getCount() {
		getLog().debug("getCount()");
		String sdoc = getObject().getSearchString();
		boolean isSearch = !(sdoc == null || sdoc.length() == 0);
		Long tmp;
		if (isSearch && sdoc.length() > MAX_SEARCH_STRING_SIZE)
			sdoc = sdoc.substring(0, MAX_SEARCH_STRING_SIZE);
		if (isSearch)
			// !!!!
			tmp = (Long) manager.createNamedQuery(
					getObject().getClass().getSimpleName() + ".SearchCount")
					.setParameter("Pref", "%" + sdoc + "%")
					.getSingleResult();
		else
			tmp = (Long) manager.createNamedQuery(
					getObject().getClass().getSimpleName() + ".AllCount")
					.getSingleResult();
		return tmp;
	}

	public O getByUId(Long UId) {
		getLog().debug("getByUId("+UId+")");
		return (O) manager.find(getObject().getClass(), UId);
//		createNamedQuery("Ubiq.ByUId").setParameter(
//				"UId", UId).getSingleResult();
	}

	public void sync() {
		if (getObject() == null) {
			// System.out.println("Sync called and object is null");
			return;
		}
		if (isObjectDirty() && getObject().getUId() != null)
			manager.find(getObject().getClass(), getObject().getUId());
	}

	public void search() {
		if (getObject().getSearchString() != null)
			setObjectListDirty(true);
	}

}
