package org.itx.jbalance.l1.common;

import java.util.List;

import org.itx.jbalance.l0.h.Hdocument;
import org.itx.jbalance.l0.s.Sdocument;
import org.itx.jbalance.l1.JBException;

public interface CommonDocument <D extends Hdocument,S extends Sdocument>  extends CommonObject<D> {
	
	public D getObject();
	public S getSpecificationLine();
	public void setSpecificationLine(S specificationLine);
	public List<S> getSpecificationLines();

	public void createSpecificationLine() throws JBException;
	public void deleteSpecificationLine() throws JBException;
	public void updateSpecificationLine() throws JBException;

	public boolean isSpecificationDirty();
	public void setSpecificationDirty(boolean specificationDirty);
	public boolean isSpecificationLineDirty();
	public void setSpecificationLineDirty(boolean specificationLineDirty);	
}
