package org.itx.jbalance.l1.common;

import java.sql.Date;
import java.sql.Timestamp;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.PostActivate;
import javax.ejb.PrePassivate;
import javax.ejb.Remove;
import javax.jms.JMSException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.s.Swhsdocument;
import org.itx.jbalance.l1.Utils;
import org.itx.jbalance.l1.session.Sessionprofile;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
import org.jboss.logging.Logger;

@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 900, removalTimeoutSeconds = 1000)
public class CommonBean implements Common {

	private static final long serialVersionUID = 5439577352372150207L;

//	@Resource(mappedName = "ConnectionFactory")
//	transient private TopicConnectionFactory factory;

//	@Resource(mappedName = "topic/JBalanceTopic")
//	transient private Topic topic;

//	transient private TopicSession topicSession = null;
//
//	transient private TopicConnection topicConnection = null;
//
//	transient private TopicPublisher topicPublisher = null;
//
//	transient private TopicSubscriber topicSubscriber = null;

	protected int mMessageCount = 0;

//	@SuppressWarnings("unused")
	protected Sessionprofile sessionprofile;

	@PersistenceContext(unitName = "JBv4_2")
	protected EntityManager manager;

	private boolean passivated = false;

	private boolean predestroyed= false;

	protected synchronized void processMessage(String MsgAction, Ubiq MsgObject) {
		getLog().debug("It`s CommonBean(" + this
				+ ")::processMessage, I get the message " + MsgAction
				+ " with object " + MsgObject);
	}

	private void TopicSet() {
//		try {
//			topicConnection = factory.createTopicConnection("guest", "guest");
//			topicSession = topicConnection.createTopicSession(false,
//					TopicSession.AUTO_ACKNOWLEDGE);
//			topicPublisher = topicSession.createPublisher(topic);
//			topicSubscriber = topicSession.createSubscriber(topic);
//			topicConnection.setExceptionListener(new ExceptionListener() {
//				public void onException(JMSException arg0) {
//					System.out
//							.println("JMSException exception hooked. System shutdown ?");
//					try {
//						topicSubscriber.setMessageListener(null);
//						topicSubscriber.close();
//						topicPublisher.close();
//						topicSession.close();
//						topicConnection.stop();
//						topicConnection.close();
//					} catch (JMSException e) {
//					}
//				}
//			});
//			topicSubscriber.setMessageListener(new MessageListener() {
//				private ObjectMessage theMessage;
//
//				public synchronized void onMessage(Message Msg) {
//					Object o=null;
//					String s=null;
//					if (Msg instanceof ObjectMessage) {
//						theMessage = (ObjectMessage) Msg;
//						// getLog().debug("CommonBean2-------------------------------"+this+"======"+Msg);
//						try {
//							s=theMessage.getStringProperty("Action");
//							o=theMessage.getObject();
//							processMessage(s,(Ubiq)o );
//							notifyAll();
//						} catch (JMSException e) {
//							getLog().error("",e);
//						} catch (Exception e){
//							getLog().debug("CommonBean::OnMessage"+e.getMessage()+"\n"+
//									s+" -> "+o);
//
//						}
//						
//					}
//				}
//
//
//			});
//			topicConnection.start();
//		} catch (JMSException e) {
//			// TODO Auto-generated catch block
//			getLog().error("",e);
//		}
	}

	private void TopicUnset() {
//		try {
//			topicSubscriber.setMessageListener(null);
//			topicSubscriber.close();
//			topicPublisher.close();
//			// В примерах только topicSession.close() И topicConnection.close();
//			topicSession.close();
//			topicConnection.stop();
//			topicConnection.setExceptionListener(null);
//			topicConnection.close();
//		} catch (JMSException e) {
//			// TODO Auto-generated catch block
//			// getLog().error("",e);
//		}
	}

	protected void sendMessage(String S, Ubiq U) throws JMSException {
//		ObjectMessage msg = topicSession.createObjectMessage(U);
//		msg.setStringProperty("Action", S);
//		topicPublisher.send(msg);
	}

	protected synchronized void create(Ubiq O) {
		O.setUId(null);
		O.setSysUser(sessionprofile.getConfig().getOper());
		O.setClassName(O.getClass().getName());
		O.setVersionRoot(O);
		O.setVersion(null);
		O.setOpenDate(O.getOpenDate() == null ? new Date(System.currentTimeMillis()): O.getOpenDate());
		O.setTS(new Timestamp(System.currentTimeMillis()));
		manager.persist(O);

		try {
			sendMessage("create", O);
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			getLog().error("",e);
		}
	}

	/**
	 * 1) N - new instance 2) find Oo 3) reflect Oo to N 4) persist O 5)
	 * N.version = O 6) find Oi (Предыдущая версия объекта) 7) Oi.version = N 8)
	 * persist N 9) persist Oi
	 * 
	 */
	protected synchronized Ubiq update(Ubiq O) {
		Ubiq N = null, Oo = null, Oi = null;
		try {
			N = O.getClass().newInstance(); // 1
		} catch (InstantiationException e) {
			getLog().error("",e);
		} catch (IllegalAccessException e) {
			getLog().error("",e);
		}

		Oo = manager.find(O.getClass(), O.getUId());
		if(Oo instanceof Swhsdocument){
			getLog().debug(">>>>>>>>>>>>>>>>>>>>>> " + ((Swhsdocument) Oo).getRest() + "<<<<<<<<<<<<<<<<<<<<<<<<");
			getLog().debug(">>>>>>>>>>>>>>>>>>>>>> " + ((Swhsdocument) O).getRest() + "<<<<<<<<<<<<<<<<<<<<<<<<<<");
		}
		Utils.copy(Oo, N);
		N.setVersion(O);

		if (!(this instanceof Sessionprofile)) O.setSysUser(sessionprofile.getConfig().getOper());
		try {
//			Oi = (Ubiq) manager.createNamedQuery("Ubiq.PrevVersion")
//					.setParameter("version", O).getSingleResult();
			
			Oi=getPrevVersion(O);
		} catch (NoResultException e) {
			Oi = null;
		}

		if (Oi != null) {
			Oi.setVersion(N);
			manager.persist(Oi);
		}
		manager.merge(O);
		O = manager.find(O.getClass(), O.getUId());
		N.setUId(null);
		manager.persist(N);
		try {
			sendMessage("update", O);
		} catch (JMSException e) {
			getLog().error("",e);
		}
		return O;
	}

	protected synchronized Ubiq delete(Ubiq O) {
		Ubiq N = null, Oo = null, Oi = null;
		try {
			N = O.getClass().newInstance(); // 1
		} catch (InstantiationException e) {
			getLog().error("",e);
		} catch (IllegalAccessException e) {
			getLog().error("",e);
		}

		Oo = manager.find(O.getClass(), O.getUId());
		Utils.copy(Oo, N);
		N.setVersion(O);

		O.setSysUser(sessionprofile.getConfig().getOper());
		O.setCloseDate(new Date(System.currentTimeMillis()));
		try {
//			Oi = (Ubiq) manager.createNamedQuery("Ubiq.PrevVersion")
//					.setParameter("version", O).getSingleResult();
			
			Oi = getPrevVersion(O);
			
		} catch (NoResultException e) {
			Oi = null;
		}

		if (Oi != null) {
			Oi.setVersion(N);
			manager.persist(Oi);
		}
		manager.merge(O);
		N.setUId(null);
		manager.persist(N);
		try {
			sendMessage("delete", O);
		} catch (JMSException e) {
			getLog().error("",e);
		}
		return O;

	}

	protected Ubiq getPrevVersion(Ubiq O) {
		return (Ubiq) manager.createQuery("from "+O.getClass().getSimpleName()+" o where o.version = :version")
		.setParameter("version", O).getSingleResult();
	}

	protected Ubiq getByUId(Ubiq O) {
		return manager.find(O.getClass(), O.getUId());
	}

	public void setRights(Sessionprofile profile) {
		this.sessionprofile = profile;
	}

	@Remove
	public void destroy() {
		getLog().debug("---Remove------" + this + "---");

	}

	public EntityManager getManager() {
		return manager;
	}

	@PrePassivate
	public void prepassivate() {
		getLog().debug("---PrePassivate------" + this + "---");
		TopicUnset();
		// topicSession = null;
		// topicConnection = null;
		// topicPublisher = null;
		// topicSubscriber = null;
		passivated = true;
		//System.out.println("-------------------------------------------------------------------------");
	}

	@PostActivate
	public void postactivate() {
		getLog().debug("---PostActivate------" + this + "---");
		TopicSet();
		passivated = false;
//		System.out
//				.println("-------------------------------------------------------------------------");
	}

	@PostConstruct
	public void postconstruct() {
		getLog().debug("---PostConstruct------" + this + "---");
		TopicSet();
//		System.out
//				.println("-------------------------------------------------------------------------");
	}

	@PreDestroy
	public void predestroy() {
		getLog().debug("---PreDestroy------" + this + "---");
		if (passivated || predestroyed)
			return;
		TopicUnset();
//		System.out
//				.println("-------------------------------------------------------------------------");
		predestroyed=true;
	}

	
	public Logger getLog(){
		return Logger.getLogger(getClass());
	}
	
}
