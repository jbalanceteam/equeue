package org.itx.jbalance.l1.common;

import java.util.List;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.h.Hdocument;
import org.itx.jbalance.l0.h.HierarchicalHeader;
import org.itx.jbalance.l0.s.Sdocument;
import org.itx.jbalance.l1.JBException;

public class CommonHierarchicalDocumentBean extends CommonDocumentBean
		implements CommonHierarchicalDocument {

	private static final long serialVersionUID = -1718641829572138922L;

	private boolean childsDirty = false;

	public CommonHierarchicalDocumentBean(Hdocument hdocument,
			Sdocument sdocument) {
		super(hdocument, sdocument);
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public synchronized void delete() throws JBException {
		for (HierarchicalHeader h : (List<HierarchicalHeader>) getChildsDocument()) {
			h.setHigherDocument(((HierarchicalHeader) getObject())
					.getHigherDocument());
			update((Hdocument) h);
		}
		super.delete();
	}

	@SuppressWarnings("unchecked")
	public List getChildsDocument() {
		if (!sessionprofile.getConfig().isAdmin()) {
			if (getObject().getUId() != null) {
				return manager.createNamedQuery(
						getObject().getClass().getSimpleName()
								+ ".ByHigherDocumentByOwners").setParameter(
						"HigherDocument", getObject()).setParameter(
						"ContractorOwner",
						sessionprofile.getConfig().getContractorsOwner())
						.getResultList();
			}
			return manager.createNamedQuery(
					getObject().getClass().getSimpleName()
							+ ".RootDocumentByOwners").setParameter(
					"ContractorOwner",
					sessionprofile.getConfig().getContractorsOwner())
					.getResultList();
		} else {
			if (getObject().getUId() != null) {
				return manager.createNamedQuery(
						getObject().getClass().getSimpleName()
								+ ".ByHigherDocument").setParameter(
						"HigherDocument", getObject()).getResultList();
			}
			return manager.createNamedQuery(
					getObject().getClass().getSimpleName()
							+ ".RootDocument").getResultList();
		}
	}

	public boolean isChildsDirty() {
		return childsDirty;
	}

	public void setChildsDirty(boolean childsDirty) {
		this.childsDirty = childsDirty;
	}

	@Override
	public void washAllDirty() {
		super.washAllDirty();
		setChildsDirty(false);
	}

	@Override
	public synchronized void processMessage(String MsgAction, Ubiq MsgObject) {
		super.processMessage(MsgAction, MsgObject);
		if (getObject() == null) {
			setChildsDirty(true);
			return;
		}
		if (getObject().getClass().isInstance(MsgObject))
			if (((HierarchicalHeader) MsgObject).getHigherDocument() == ((HierarchicalHeader) getObject())
					.getHigherDocument())
				setChildsDirty(true);
			else if (((HierarchicalHeader) MsgObject).getHigherDocument()
					.equals(
							((HierarchicalHeader) getObject())
									.getHigherDocument()))
				setChildsDirty(true);
	}

}
