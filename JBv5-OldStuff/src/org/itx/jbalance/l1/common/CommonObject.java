package org.itx.jbalance.l1.common;

import java.util.List;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l1.JBException;

public interface  CommonObject <D extends Ubiq> extends Common {
	
	public void create();

	public void delete() throws JBException;

	public void update() throws JBException;

	public D getObject();

	public void setObject(Ubiq object);
	
	public boolean isObjectListDirty();

	public void setObjectListDirty(boolean listObjectDirty);

	public boolean isObjectDirty();

	public void setObjectDirty(boolean objectDirty);
	
	public void newObject();
	
	public void washAllDirty();
	
	public List<? extends D> getAll();
	
	public D getByUId(Long UId);
	
	public void sync();
	
	public void search();

	public Long getCount();
	
	public List<? extends Ubiq> getRange(Long firstRow,Long maxRow);
}
