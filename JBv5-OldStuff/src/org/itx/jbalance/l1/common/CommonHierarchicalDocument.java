package org.itx.jbalance.l1.common;

import java.util.List;

public interface CommonHierarchicalDocument extends CommonDocument {

	public List getChildsDocument();
	
	public boolean isChildsDirty();

	public void setChildsDirty(boolean childsDirty);

	
}
