package org.itx.jbalance.l1.common;

import java.io.Serializable;

import javax.persistence.EntityManager;
import org.itx.jbalance.l1.session.Sessionprofile;

public interface Common extends Serializable {
	public void setRights(Sessionprofile profile);
	public EntityManager getManager();
	public void destroy();
	public void prepassivate();
	public void postactivate();
	public void postconstruct();
	public void predestroy();
}
