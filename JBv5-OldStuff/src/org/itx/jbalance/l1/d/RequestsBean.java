package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.Hrequest;
import org.itx.jbalance.l0.s.Sprice;
import org.itx.jbalance.l0.s.Srequest;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
//@LocalBinding(jndiBinding = "Requests/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class RequestsBean extends CommonDocumentBean implements Requests {

	private static final long serialVersionUID = 4331024638735062585L;

	public RequestsBean() {
		super(new Hrequest(),new Srequest());
	}

	@Override
	public void createSpecificationLine() throws JBException {
		importSource();
		getObject().setSumm(getObject().getSumm() 
				+ getSpecificationLine().getPrice() 
				* getSpecificationLine().getQuantity());
		update();
		getSpecificationLine().setSumm(getSpecificationLine().getPrice() * getSpecificationLine().getQuantity());
		// save object
		super.createSpecificationLine();
		
	}

	public void deleteSpecificationLine() throws JBException {
		getObject().setSumm(getObject().getSumm() - getSpecificationLine().getSumm());
		update();
		super.deleteSpecificationLine();
	}
	
	@SuppressWarnings("unchecked")
	public List<Hrequest> getByOwnersByPeriod() {
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
		if (!sessionprofile.getConfig().isAdmin())
		return (List<Hrequest>) manager.createNamedQuery(
				"Hrequest.ByOwnersByPeriod").setParameter("periodFrom",
				sessionprofile.getConfig().getPeriodFrom()).setParameter("periodTo",
				sessionprofile.getConfig().getPeriodTo()).setParameter(
				"ContractorOwner",
				sessionprofile.getConfig().getSelectedContractorOwner())
				.getResultList();
		else
			return (List<Hrequest>) manager.createNamedQuery(
			"Hrequest.ByPeriod").setParameter("periodFrom",
			sessionprofile.getConfig().getPeriodFrom()).setParameter("periodTo",
			sessionprofile.getConfig().getPeriodTo()).getResultList();
		}else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hrequest>) manager.createNamedQuery(
						"Hrequest.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hrequest>) manager.createNamedQuery(
						"Hrequest.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}

	public Hrequest getObject() {
		return (Hrequest)super.getObject();
	}

	public Srequest getSpecificationLine() {
		return (Srequest)super.getSpecificationLine();
	}

	@Override
	public void newObject() {
		super.newObject();
		getObject().setSumm(0F);
	}

	/**
	 * Алгоритм измененния записи спецификации с пересчетом заголовка:
	 * 1. Пересчитываем header
	 * 2. Сохраняем его в базу
	 * 3. Получаем список спецификации
	 * 4. Удаляем изменяемую запись спецификации из полученного списка
	 * 5. Привязываем измененную запись спецификации к новому header
	 * 6. Пересчитываем строку спецификации
	 * 7. Записываем ее в базу
	 * 8. Обновляем все оставшиеся записи спецификации в базе
	 * 9. Устанавливаем header как текущий 
	 * @throws JBException 
	 */
	public void updateSpecificationLine() throws JBException {
		getObject().setSumm(getObject().getSumm() 
				+ getSpecificationLine().getPrice()  // 1. 
				* getSpecificationLine().getQuantity() 
				- getSpecificationLine().getSumm());
		update();	// 2.
		getSpecificationLine().setSumm(getSpecificationLine().getPrice() * 
				getSpecificationLine().getQuantity()); 
		super.updateSpecificationLine(); // 7.
	}

	public void importSource() {
//		System.out.println("RequestsBean.importSource()1"+sw);
		if(getSpecificationLine().getSource() instanceof Sprice){
//			System.out.println("RequestsBean.importSource()2"+sw);
			if(getSpecificationLine().getPrice()==null)getSpecificationLine().setPrice(((Sprice)getSpecificationLine().getSource()).getPrice());
			if(getSpecificationLine().getCurrency()==null)getSpecificationLine().setCurrency(((Sprice)getSpecificationLine().getSource()).getCurrency());
			getSpecificationLine().setArticleUnit(((Sprice)getSpecificationLine().getSource()).getArticleUnit());
		}
	}
}
