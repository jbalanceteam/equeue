package org.itx.jbalance.l1.d;


import java.sql.Date;
import java.util.List;
import java.util.Vector;
import javax.ejb.Stateful;
import org.itx.jbalance.l0.h.Hmoveaunit;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.o.Packet;
import org.itx.jbalance.l0.s.Smoveaunit;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class MoveaunitsBean extends CommonDocumentBean implements Moveaunits {

	private static final long serialVersionUID = 4506454648365081531L;
	
	private List<Smoveaunit> genirate_list_moveaunit = null;

	public MoveaunitsBean() {
		super(new Hmoveaunit(), new Smoveaunit());
	}
	
	private void getSmoveaunit(Smoveaunit spec){
		 spec.setArticleUnit(manager.find(Aunit.class, spec.getArticleUnit().getUId()));
		 spec.setPacket(manager.find(Packet.class, spec.getPacket().getUId()));
		 spec.setMeasurement(manager.find(Measurement.class, spec.getMeasurement().getUId()));
	}
	
	@SuppressWarnings("unchecked")
	public void setMoveAunitBeginAndEndPer(Date date){
		boolean is_begin = date == getObject().getBegin_period() ? true : false;
		List<Smoveaunit> list_rest_begin_per = !is_begin ?
		
			(List<Smoveaunit>)manager.createNamedQuery("Smoveaunit.getMoveAunitByPeriod")
				.setParameter("OWNER", getObject().getContractorOwner())
				.setParameter("WHSOWNER", getObject().getWhs())
				.setParameter("OPEN_DATE",date)
				.getResultList()
		:
			(List<Smoveaunit>)manager.createNamedQuery("Smoveaunit.getMoveAunitByPeriodNotIncludeDate")
			.setParameter("OWNER", getObject().getContractorOwner())
			.setParameter("WHSOWNER", getObject().getWhs())
			.setParameter("OPEN_DATE",date)
			.getResultList();
		
		List<Smoveaunit> list_recoil = (List<Smoveaunit>)manager.createNamedQuery("Smoveaunit.getRestIncomeByConsumption")
			.setParameter("OWNER", getObject().getContractorOwner())
			.setParameter("WHSOWNER", getObject().getWhs())
			.setParameter("OPEN_DATE", date)
			.getResultList();
		
		for(Smoveaunit spec : list_rest_begin_per){
			getSmoveaunit(spec);
			int index_rest_recoil = list_recoil.indexOf(spec);
			spec.setStartrest(index_rest_recoil >= 0 
						? spec.getStartrest() + list_recoil.get(index_rest_recoil).getStartrest() 
						: spec.getStartrest());
			int index_ = genirate_list_moveaunit.indexOf(spec);
			if(index_ >= 0 ){
				if(is_begin)
					genirate_list_moveaunit.get(index_).setStartrest(spec.getStartrest());
				else
					genirate_list_moveaunit.get(index_).setEndrest(spec.getStartrest());
			}else{
				genirate_list_moveaunit.add(spec);
				if(is_begin)
					spec.setStartrest(spec.getStartrest());
				else{
					spec.setEndrest(spec.getStartrest());
					spec.setStartrest(null);
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public void GenirateSpec(){
		try{
			genirate_list_moveaunit = new Vector<Smoveaunit>();
			System.out.println("Start date = " + getObject().getBegin_period());
			System.out.println("Start end = " + getObject().getEnd_period());
			System.out.println("whs uid = " + getObject().getWhs().getUId());
			setMoveAunitBeginAndEndPer(getObject().getBegin_period());
			setMoveAunitBeginAndEndPer(getObject().getEnd_period());
			
			List<Smoveaunit> list_rest_income_per = (List<Smoveaunit>)manager.createNamedQuery("Smoveaunit.getIncomeByPeriod")
				.setParameter("WHSOWNER", getObject().getWhs())
				.setParameter("BEGIN_PER",getObject().getBegin_period())
				.setParameter("END_PER",getObject().getEnd_period())
				.getResultList();
			System.out.println("size list_rest_income_per = " + list_rest_income_per.size());
			for(Smoveaunit spec : list_rest_income_per){
				int index_ = genirate_list_moveaunit.indexOf(spec);
				if(index_ >= 0)
					genirate_list_moveaunit.get(index_).setIncomerest(spec.getStartrest());
				else{
					genirate_list_moveaunit.add(spec);
					spec.setIncomerest(spec.getStartrest());
					spec.setStartrest(null);
				}
			}

			List<Smoveaunit> list_rest_consumption_per = (List<Smoveaunit>)manager.createNamedQuery("Smoveaunit.getConsumptionByPeriod")
				.setParameter("WHSOWNER", getObject().getWhs())
				.setParameter("BEGIN_PER",getObject().getBegin_period())
				.setParameter("END_PER",getObject().getEnd_period())
				.getResultList();
			System.out.println("size list_rest_consumption_per = " + list_rest_consumption_per.size());
			for(Smoveaunit spec : list_rest_consumption_per){
				int index_ = genirate_list_moveaunit.indexOf(spec);
				if(index_ >= 0)
					genirate_list_moveaunit.get(index_).setConsumptionrest(spec.getStartrest());
				else{
					genirate_list_moveaunit.add(spec);
					spec.setConsumptionrest(spec.getStartrest());
					spec.setStartrest(null);
				}
			}
			
		for(Smoveaunit spec : genirate_list_moveaunit){
			setSpecificationLine(spec);
			createSpecificationLine();
		}
			
		}catch (Exception e) {
			getLog().error("",e);
		
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Smoveaunit> getGenirateSpecificationLines(){
		List<Smoveaunit> list = (List<Smoveaunit>)super.getSpecificationLines();
		System.out.println("Size spec move aunit = " + list.size());
		if(list.size() > 0)	
			return list;
		GenirateSpec();
		return genirate_list_moveaunit;
	}
	
	public Hmoveaunit getObject(){
		return (Hmoveaunit)super.getObject();
	}
	
	@SuppressWarnings("unchecked")
	public List<Hmoveaunit> getRangeHead(int first, int n, Object arg){
		return manager.createNamedQuery("Hmoveaunit.All")
					.setFirstResult(first)
					.setMaxResults(n)
					.getResultList();
	}
	
}
