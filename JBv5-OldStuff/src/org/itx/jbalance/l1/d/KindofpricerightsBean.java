package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.Hkindofpriceright;
import org.itx.jbalance.l0.o.Kindofprice;
import org.itx.jbalance.l0.s.Skindofpriceright;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
@SuppressWarnings("unchecked")
@Stateful
@LocalBinding(jndiBinding = "Kindofpricerights/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class KindofpricerightsBean extends CommonDocumentBean implements
		Kindofpricerights {

	private static final long serialVersionUID = -8430635966944491477L;
	
	public KindofpricerightsBean() {
		super(new Hkindofpriceright(), new Skindofpriceright());
	}

	public Hkindofpriceright getObject() {
		return (Hkindofpriceright)super.getObject();
	}

	public Skindofpriceright getSpecificationLine() {
		return (Skindofpriceright)super.getSpecificationLine();
	}
	
	public List<Kindofprice> getEnabledKindofprice(List<Hkindofpriceright> heads) {
		return (List<Kindofprice>)manager.createNamedQuery("Skindofpriceright.getEnabledByHeads")
				.setParameter("HEADS", heads)
				.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Hkindofpriceright> getByOwnersByPeriod() {
		System.out.println(sessionprofile.getConfig().getPeriodFrom());
		System.out.println(sessionprofile.getConfig().getPeriodTo());
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hkindofpriceright>) manager.createNamedQuery(
						"Hkindofpriceright.ByOwnersByPeriod").setParameter(
						"periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.getResultList();
			else
				return (List<Hkindofpriceright>) manager.createNamedQuery(
						"Hkindofpriceright.ByPeriod").setParameter("periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.getResultList();
		} else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hkindofpriceright>) manager.createNamedQuery(
						"Hkindofpriceright.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hkindofpriceright>) manager.createNamedQuery(
						"Hkindofpriceright.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}
	
}
