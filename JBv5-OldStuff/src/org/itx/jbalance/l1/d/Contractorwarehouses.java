package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hcontractorwarehouse;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.s.Scontractorwarehouse;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Contractorwarehouses extends CommonDocument {

	public Hcontractorwarehouse getObject();
	public Scontractorwarehouse getSpecificationLine();
	public List<Hcontractorwarehouse> getByOwnersByPeriod();
	public Hcontractorwarehouse getByOwner(Contractor c);
	public List<Scontractorwarehouse> getWhsByOwner();

}
