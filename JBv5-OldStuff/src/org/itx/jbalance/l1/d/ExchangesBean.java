package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.h.Hexchange;
import org.itx.jbalance.l0.s.Sexchange;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.itx.jbalance.l1.o.Actions;
import org.itx.jbalance.l1.o.Juridicals;
import org.itx.jbalance.l1.o.Opers;
import org.itx.jbalance.l1.o.Physicals;
import org.itx.jbalance.l1.o.Ubiqs;
import org.itx.jbalance.l1.session.Sessionprofile;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
//@LocalBinding(jndiBinding = "Exchanges/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class ExchangesBean extends CommonDocumentBean<Hexchange,Sexchange>   implements  Exchanges {

	private static final long serialVersionUID = 797832421329471932L;

	@EJB(name = "Ubiqs/local")
	private Ubiqs UbiqVO;

	@EJB(name = "Opers/local")
	private Opers OperVO;

	@EJB(name = "Actions/local")
	private Actions ActionVO;

	@EJB(name = "Billfacts/local")
	private Billfacts BillfactsVO;

	@EJB(name = "Actionrights/local")
	private Actionrights ActionrightVO;

	@EJB(name = "Physicals/local")
	private Physicals PhysicalVO;

	@EJB(name = "Juridicals/local")
	private Juridicals JuridicalVO;

	@EJB(name = "Contractorrights/local")
	private Contractorrights ContractorrightVO;

	@EJB(name = "Menus/local")
	private Menus MenuVO;

	public ExchangesBean() {
		super(new Hexchange(), new Sexchange());
	}

	public void insertOpers() {
		for (Ubiq ubiq : OperVO.getAll()) {
			newSpecificationLine();
			getSpecificationLine().setExchangedUbiq(ubiq);
			try {
				createSpecificationLine();
			} catch (JBException e) {
				getLog().error("",e);
			}
		}
	}

	@TransactionTimeout(900)
	public void insertBillfact() {
		for (Ubiq ubiq : BillfactsVO.getAll()) {
			newSpecificationLine();
			getSpecificationLine().setExchangedUbiq(ubiq);
			try {
				createSpecificationLine();
			} catch (JBException e) {
				getLog().error("",e);
			}
		}
	}

	public void insertActions() {
		for (Ubiq ubiq : ActionVO.getAll()) {
			newSpecificationLine();
			getSpecificationLine().setExchangedUbiq(ubiq);
			try {
				createSpecificationLine();
			} catch (JBException e) {
				getLog().error("",e);
			}
		}
	}

	public void insertActionrights() {
		for (Ubiq ubiq : ActionrightVO.getAll()) {
			newSpecificationLine();
			getSpecificationLine().setExchangedUbiq(ubiq);
			try {
				createSpecificationLine();
			} catch (JBException e) {
				getLog().error("",e);
			}
		}
	}

	public void insertPhysicals() {
		for (Ubiq ubiq : PhysicalVO.getAll()) {
			newSpecificationLine();
			getSpecificationLine().setExchangedUbiq(ubiq);
			try {
				createSpecificationLine();
			} catch (JBException e) {
				getLog().error("",e);
			}
		}
	}

	public void insertJuridicals() {
		for (Ubiq ubiq : JuridicalVO.getAll()) {
			newSpecificationLine();
			getSpecificationLine().setExchangedUbiq(ubiq);
			try {
				createSpecificationLine();
			} catch (JBException e) {
				getLog().error("",e);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void insertContractorrights() {
		List tmp = ContractorrightVO.getAll();
		for (Object o : tmp) {
			Ubiq ubiq =(Ubiq)o;
			newSpecificationLine();
			getSpecificationLine().setExchangedUbiq(ubiq);
			try {
				createSpecificationLine();
			} catch (JBException e) {
				getLog().error("",e);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void insertMenus() {
		for (Ubiq hubiq : (List<Ubiq>) MenuVO.getAll()) { // Выгребаем шапки
			newSpecificationLine();
			getSpecificationLine().setExchangedUbiq(hubiq);
			try {
				createSpecificationLine();
			} catch (JBException e) {
				getLog().error("",e);
			}
			MenuVO.setObject(hubiq); // Выгребаем спецификацию
			for (Ubiq subiq : (List<Ubiq>) MenuVO.getSpecificationLines()) {
				newSpecificationLine();
				getSpecificationLine().setExchangedUbiq(subiq);
				try {
					createSpecificationLine();
				} catch (JBException e) {
					getLog().error("",e);
				}
			}
		}
	}

	public Hexchange getObject() {
		return (Hexchange) super.getObject();
	}

	public Sexchange getSpecificationLine() {
		return (Sexchange) super.getSpecificationLine();
	}

	// поиск строки спецификации по хранимым в ней убикам
	// установка ее как текущей строки спец.
	public void setUSpecificationLine(Ubiq sr) {
		// TODO: проверить!
		// System.out.println("=================setUSpecificationLine
		// "+sr.getClass()+" "+sr);
		setSpecificationLine((Sexchange) manager.createNamedQuery(
				"Sexchange.USpecificationByUbiq").setParameter("Header",
				getObject()).setParameter("Spec", sr).getResultList().get(0));
	}

	@SuppressWarnings("unchecked")
	public List<Ubiq> getUSpecificationLines() {
		return manager.createNamedQuery("Sexchange.USpecification")
				.setParameter("Header", getObject()).getResultList();
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean importSpecificationLine(Ubiq ubiq) {
		try {
			UbiqVO.setObject(ubiq);
			UbiqVO.create();
			newSpecificationLine();
			getSpecificationLine().setExchangedUbiq(ubiq);
			createSpecificationLine();
		} catch (Exception e) {
			// System.out.println(e.getMessage());
			return false;
		}
		// manager.flush();
		return true;
	}

	@Override
	public void setRights(Sessionprofile profile) {
		super.setRights(profile);
		UbiqVO.setRights(profile);
		OperVO.setRights(profile);
		ActionVO.setRights(profile);
		ActionrightVO.setRights(profile);
		JuridicalVO.setRights(profile);
		ContractorrightVO.setRights(profile);
		MenuVO.setRights(profile);
	}

	@SuppressWarnings("unchecked")
	public List<Hexchange> getByOwnersByPeriod() {
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hexchange>) manager.createNamedQuery(
						"Hexchange.ByOwnersByPeriod").setParameter(
						"periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.getResultList();
			else
				return (List<Hexchange>) manager.createNamedQuery(
						"Hexchange.ByPeriod").setParameter("periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.getResultList();
		} else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hexchange>) manager.createNamedQuery(
						"Hexchange.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hexchange>) manager.createNamedQuery(
						"Hexchange.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}

}
