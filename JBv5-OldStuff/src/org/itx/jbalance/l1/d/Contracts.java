package org.itx.jbalance.l1.d;

import java.util.List;
import javax.ejb.Local;
import org.itx.jbalance.l0.h.Hcontract;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Contracts extends CommonDocument{
	public List<Hcontract> getAll();
	public Long getAllCount();
}
