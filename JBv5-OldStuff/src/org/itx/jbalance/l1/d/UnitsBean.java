/**
 * 
 */
package org.itx.jbalance.l1.d;

import java.util.List;
import java.util.Vector;
import org.itx.jbalance.l0.h.Hunit;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.s.Sunit;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

import javax.ejb.Stateful;

/**
 * @author galina
 *
 */
@Stateful
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class UnitsBean extends CommonDocumentBean implements	Units {
	
	private static final long serialVersionUID = -1495425223764336537L;
	
	
	
	public UnitsBean() {
		super(new Hunit(), new Sunit());
	}

	public Hunit getObject() {
		return (Hunit)super.getObject();
	}
	
	public Sunit getSpecificationLine(){
		return (Sunit)super.getSpecificationLine();
	}


	@SuppressWarnings("unchecked")
	public List<Hunit> searchHunit(Aunit aunit, Measurement measurement)
	{
		return (List<Hunit>)manager.createNamedQuery("Hunit.Head")
		.setParameter("AUNIT", aunit).setParameter("MEASUREMENT", measurement)
		.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Hunit> getHeadByAunit(Aunit aunit)
	{
		return (List<Hunit>)manager.createNamedQuery("Hunit.getHeadByAunit")
		.setParameter("AUNIT", aunit).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Sunit> getSpecByHeads(List<Hunit> list){
			if(list.size() > 0)
				return manager.createNamedQuery("Sunit.getSpecByHeads")
						.setParameter("HEADS", list)
						.getResultList();
			return new Vector<Sunit>();
	}
	
	@SuppressWarnings("unchecked")
	public List<Sunit> getAllSpec(){
		return (List<Sunit>)manager.createNamedQuery("Sunit.All").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Sunit> getAllSpecRange(int first, int n){
		return (List<Sunit>)manager.createNamedQuery("Sunit.All")
			.setFirstResult(first)
			.setMaxResults(n)
			.getResultList();
	}
	
	public Long getAllSpecCount(){
		return (Long)manager.createNamedQuery("Sunit.AllCount").getSingleResult();
	}
	
}
