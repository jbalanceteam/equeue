package org.itx.jbalance.l1.d;
import java.util.List;
import javax.ejb.Local;
import org.itx.jbalance.l0.h.Hbill;
import org.itx.jbalance.l0.h.Hdocument;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.s.Sbill;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Bills extends CommonDocument {
	public Hbill getObject();
	public List<Hbill> getByOwnersByPeriod();
	public List<Hbill> getBillByContractorReceiver(Contractor c, int s, int e);
	public Long getBillByContractorReceiverCount(Contractor c);
	public Sbill getSpecificationLine();
	
	public List<Hbill> getAllBill(int s, int e);
	public Long getAllBillCount();
	
		
	public List<Hbill> getAllBillBySource(Hdocument d, int s, int e);
	public Long getAllBillBySourceCount(Hdocument d);
	
}
