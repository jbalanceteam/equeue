/**
 * 
 */
package org.itx.jbalance.l1.d;

import java.util.List;

import org.itx.jbalance.l0.h.Himago;
import org.itx.jbalance.l0.h.Hunit;
import org.itx.jbalance.l0.h.Hwhsdocument;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Imago;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.o.Packet;
import org.itx.jbalance.l0.s.Simago;
import org.itx.jbalance.l0.s.Sunit;
import org.itx.jbalance.l0.s.Swhsdocument;
import org.itx.jbalance.l1.common.CommonDocument;
import javax.ejb.Local;

/**
 * @author galina
 *
 */
@Local
public interface Imagosdocuments extends CommonDocument {
	
	public Himago getObject();
	
	public Simago getSpecificationLine();
	
	public List<Himago> getHeadByImago(Imago imago);
	
	public List<Simago> getSpecByHeads(List<Himago> list);

	public List<Simago> getSpecByHeadsRange(List<Himago> list, Long from, Long n);
	
	public Long getSpecByHeadsCount(List<Himago> list);
}
