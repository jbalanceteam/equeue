package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hactionright;
import org.itx.jbalance.l0.o.Action;
import org.itx.jbalance.l0.s.Sactionright;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Actionrights extends CommonDocument<Hactionright,Sactionright>{

	
	
	public Sactionright getSpecificationLine();
	
	public List<Hactionright> getByOwnersByPeriod();
	
	public List<Action> getAction(List<Hactionright> heads);
}
