package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.Hmenu;
import org.itx.jbalance.l0.s.Smenu;
import org.itx.jbalance.l1.common.CommonHierarchicalDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
//@LocalBinding(jndiBinding = "Menus/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class MenusBean extends CommonHierarchicalDocumentBean implements Menus {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5192209916762779641L;

	private List<Hmenu> Objects;
	
	public MenusBean() {
		super(new Hmenu(),new Smenu());
	}


	@SuppressWarnings("unchecked")
	public List<Hmenu> getNotEmptyDocumentsByOwners() {
		return (List<Hmenu>) manager.createNamedQuery(
				"Menu.NotEmptyDocumentsByOwners").setParameter("ContractorOwner",
				sessionprofile.getConfig().getContractorsOwner()).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Hmenu> getByOwnersByPeriod() {
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hmenu>) manager.createNamedQuery(
						"Hmenu.ByOwnersByPeriod").setParameter(
						"periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.getResultList();
			else
				return (List<Hmenu>) manager.createNamedQuery(
						"Hmenu.ByPeriod").setParameter("periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.getResultList();
		} else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hmenu>) manager.createNamedQuery(
						"Hmenu.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hmenu>) manager.createNamedQuery(
						"Hmenu.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}

	public Hmenu getObject() {
		return (Hmenu)super.getObject();
	}


	public Smenu getSpecificationLine() {
		return (Smenu) super.getSpecificationLine();
	}


	public List<Hmenu> getObjects() {
		return Objects;
	}


	public void setObjects(List<Hmenu> objects) {
		Objects = objects;
	}
	
	@Override
	public boolean isObjectDirty() {
// TODO переделать с учетом списка объектов
		return super.isObjectDirty();
	}
	
	@Override
	public List getChildsDocument() {
		List rez=null;
		// объект установлен но uid отсутсвует - предполагаем что задан не 
		// узел дерева а список меню в профиле
		if(getObject().getUId()==null) rez = (List)getObjects();
		if(rez!=null) return rez;
		// иначе даем поддерево. если узел null - то от корня
		return super.getChildsDocument();
	}
	
}
