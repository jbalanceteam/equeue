package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hmenu;
import org.itx.jbalance.l0.s.Smenu;
import org.itx.jbalance.l1.common.CommonHierarchicalDocument;

@Local
public interface Menus extends CommonHierarchicalDocument {

	public List<Hmenu> getNotEmptyDocumentsByOwners();

	public List<Hmenu> getByOwnersByPeriod();

	public Hmenu getObject();

	public Smenu getSpecificationLine();
	
	public List<Hmenu> getObjects();

	public void setObjects(List<Hmenu> objects);

}
