package org.itx.jbalance.l1.d;

import java.util.List;
import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hcategorygroup;
import org.itx.jbalance.l0.h.Hgroupcontractor;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.o.Group_;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.s.Sgroupcontractor;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Groupcontractors extends CommonDocument{
	public Hgroupcontractor getObject();
	public boolean add(boolean f, Hgroupcontractor groupcontractor);
	public void addSpec(Sgroupcontractor s);
    public Sgroupcontractor getSpecLine();
	public List<Sgroupcontractor> getSpec(Hgroupcontractor s);
	public boolean delSpec(Sgroupcontractor s);
	public boolean updateSpec(Sgroupcontractor s);
	
	public List<Sgroupcontractor> getSpecByContractor(Sgroupcontractor s);
	public List<Sgroupcontractor> getSpecByCategory(Sgroupcontractor s, Hcategorygroup c);
	public List<Hgroupcontractor> getHeadByGroup(Hgroupcontractor h);
	

	public List<Contractor> getContractors(Hgroupcontractor h, Long first, Long last);
	public Long getContractorsCount(Hgroupcontractor h);
	
	public List<Juridical> getJuridical(Hgroupcontractor h, Long first, Long last);
	public Long getJuridicalCount(Hgroupcontractor h);
	
	public Object getContractors(Hgroupcontractor h, Hgroupcontractor hSecond, Long first, Long last);
	//public List<Contractor> getContractors(Hgroupcontractor h, Hgroupcontractor hSecond, Long first, Long last);
	public boolean checkDuplication(Sgroupcontractor s, Hgroupcontractor h);
	
	public boolean updateSpecBal(Sgroupcontractor h);
	
	public List<Sgroupcontractor> getSpecByContractorGroups(Sgroupcontractor s, List<Group_> listgroup);
}
