package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.Hbillfact;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.s.Sbillfact;
import org.itx.jbalance.l0.s.Sprice;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

//@SuppressWarnings("unchecked")
@Stateful
@LocalBinding(jndiBinding = "Billfacts/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class BillfactsBean extends CommonDocumentBean<Hbillfact,Sbillfact> implements Billfacts {

	private static final long serialVersionUID = 5364695926228069986L;

	public BillfactsBean() {
		super(new Hbillfact(), new Sbillfact());
	}

	@Override
	public Sbillfact getSpecificationLine() {
		return (Sbillfact) super.getSpecificationLine();
	}

	public void createSpecificationLine() throws JBException {
		if (!isUpdateable())
			throw new JBException("restricted");
		importSource();
		getObject().setSumm(
				getObject().getSumm() + getSpecificationLine().getPrice()
						* getSpecificationLine().getQuantity());
		update();
		getSpecificationLine().setSumm(
				getSpecificationLine().getPrice()
						* getSpecificationLine().getQuantity());
		// save object
		super.createSpecificationLine();

	}

	public void deleteSpecificationLine() throws JBException {
		if (!isUpdateable())
			throw new JBException("restricted");
		getObject().setSumm(
				getObject().getSumm() - getSpecificationLine().getSumm());
		update();
		super.deleteSpecificationLine();
	}

	@Override
	public void update() throws JBException {
		if (!isUpdateable())
			throw new JBException("restricted");
		super.update();
	}

	@Override
	public synchronized void delete() throws JBException {
		if (!sessionprofile.getConfig().isAdmin())
			throw new JBException("restricted");
		super.delete();
	}

	private boolean isUpdateable() {
		boolean admin = sessionprofile.getConfig().isAdmin();
		boolean period = !sessionprofile.getConfig().isPeriodExpired(
				getObject().getOpenDate());
		boolean oper = sessionprofile.getConfig().getOper().equals(
				getObject().getSysUser());

		return admin || (period && oper);
	}

	@SuppressWarnings("unchecked")
	public List<Hbillfact> getByOwnersByPeriod() {
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		// без фильтра
		if ((sdoc == null) || (sdoc.length() == 0)) {
			// я админ
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hbillfact>) manager.createNamedQuery(
						"Hbillfact.ByOwnersByPeriod").setParameter(
						"periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.getResultList();
			// я не админ
			else
				return (List<Hbillfact>) manager.createNamedQuery(
						"Hbillfact.ByPeriod").setParameter("periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.getResultList();
		}
		// с фильтром
		else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				System.out.println(sdoc);
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
				System.out.println(ddoc);
			} catch (Exception e) {
			}
			// Я админ
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hbillfact>) manager.createNamedQuery(
						"Hbillfact.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			// я не админ
			else
				return (List<Hbillfact>) manager.createNamedQuery(
						"Hbillfact.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}

	public Hbillfact getObject() {
		return (Hbillfact) super.getObject();
	}

	@Override
	public void newObject() {
		super.newObject();
		getObject().setSumm(0F);
	}

	public void updateSpecificationLine() throws JBException {
		if (!isUpdateable())
			throw new JBException("restricted");
		getObject().setSumm(
				getObject().getSumm()
						+ getSpecificationLine().getPrice() // 1.
						* getSpecificationLine().getQuantity()
						- getSpecificationLine().getSumm());
		update(); // 2.
		getSpecificationLine().setSumm(
				getSpecificationLine().getPrice()
						* getSpecificationLine().getQuantity());
		super.updateSpecificationLine(); // 7.
	}

	private void importSource() {
		// System.out.println("RequestsBean.importSource()1"+sw);
		if (getSpecificationLine().getSource() instanceof Sprice) {
			// System.out.println("RequestsBean.importSource()2"+sw);
			if (getSpecificationLine().getPrice() == null)
				getSpecificationLine().setPrice(
						((Sprice) getSpecificationLine().getSource())
								.getPrice());
			if (getSpecificationLine().getCurrency() == null)
				getSpecificationLine().setCurrency(
						((Sprice) getSpecificationLine().getSource())
								.getCurrency());
			getSpecificationLine().setArticleUnit(
					((Sprice) getSpecificationLine().getSource())
							.getArticleUnit());
		}
	}

	
	@SuppressWarnings("unchecked")	
	public List<Hbillfact> getBillByContractorReceiver(Contractor c, int s, int e){
		return manager.createNamedQuery("Hbillfact.getBillByContractorReceiver")
						.setParameter("CONTRACTOR", c)
						.setFirstResult(s)
						.setMaxResults(e)
						.getResultList();
    }
	
	@SuppressWarnings("unchecked")	
	public Long getBillByContractorReceiverCount(Contractor c){
		return (Long)manager.createNamedQuery("Hbillfact.getBillByContractorReceiverCount")
						.setParameter("CONTRACTOR", c)
						.getSingleResult();
    }

	@SuppressWarnings("unchecked")
	public List<Hbillfact> getByContractorsByPeriod(List<Contractor> contractors, Date startDate, Date endDate) {
		return (List<Hbillfact>)manager.createNamedQuery("Hbillfact.getBillByContractorReceiversByPeriod").setParameter("Contractors", contractors).setParameter("startDate", startDate).setParameter("endDate", endDate).getResultList();
	}

	public Float getSummByContarctorsByPeriod(List<Contractor> contractors, Date startDate, Date endDate) {
		List<Hbillfact> list = getByContractorsByPeriod(contractors, startDate, endDate);
		
		Float summ = Float.valueOf(0);
		for(Hbillfact hbf : list)
		{
			if(hbf.getSumm() != null)
				summ += hbf.getSumm();
		}
		
		return summ;
	}
}
