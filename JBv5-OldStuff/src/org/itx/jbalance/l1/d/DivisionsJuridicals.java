package org.itx.jbalance.l1.d;


import java.util.List;
import javax.ejb.Local;
import org.itx.jbalance.l0.h.HdivisionJuridical;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.s.SdivisionJuridical;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface DivisionsJuridicals extends CommonDocument{
	public List<HdivisionJuridical>getDivisionInJuridical(Juridical j);
	public List<SdivisionJuridical> getSpec(HdivisionJuridical h);
	public List<SdivisionJuridical> getSpecByEmpl(SdivisionJuridical sd);
	
	public HdivisionJuridical getDivisionJuridical(HdivisionJuridical hdj);
}
