package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hprice;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Currency;
import org.itx.jbalance.l0.o.Kindofprice;
import org.itx.jbalance.l0.s.Sprice;
import org.itx.jbalance.l1.common.CommonHierarchicalDocument;

@Local
public interface Prices extends CommonHierarchicalDocument {
	public List<Hprice> getNotEmptyPriceByOwners();

	public List<Hprice> getByOwnersByPeriod();

	public Hprice getObject();
	
	public Sprice getSpecificationLine();

	public List<Sprice> getSpec(Hprice hprice, Kindofprice kindofprice, Currency currency);
	
	public List<Sprice> getSpec(Hprice p, Aunit aunit);
}
