/**
 * 
 */
package org.itx.jbalance.l1.d;

import java.util.List;

import org.itx.jbalance.l0.h.Htechnologymap;
import org.itx.jbalance.l0.h.Hunit;
import org.itx.jbalance.l0.h.Hwhsdocument;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Division;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.o.Packet;
import org.itx.jbalance.l0.s.Stechnologymap;
import org.itx.jbalance.l0.s.Sunit;
import org.itx.jbalance.l0.s.Swhsdocument;
import org.itx.jbalance.l1.common.CommonDocument;
import javax.ejb.Local;

/**
 * @author galina
 *
 */
@Local
public interface Technologymaps extends CommonDocument {
	
	public Htechnologymap getObject();
	
	public Stechnologymap getSpecificationLine();
	
	public List<Htechnologymap> searchHtechnologymap(Aunit aunit, Division division);
	
	public List<Htechnologymap> getHeadByAunit(Aunit aunit);
	
	public List<Stechnologymap> getSpecByHeads(List<Htechnologymap> list);

	public List<Stechnologymap> getBasicImagosByHead(Htechnologymap head);
	
	public List<Stechnologymap> getSubImagosByHead(Htechnologymap head, Stechnologymap spec);
}
