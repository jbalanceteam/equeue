package org.itx.jbalance.l1.d;

import java.util.List;
import javax.ejb.Local;
import org.itx.jbalance.l0.Rest;
import org.itx.jbalance.l0.h.Hwhsdocument;
import org.itx.jbalance.l0.s.Swhsdocument;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Whsdocuments extends CommonDocument {

	public List<Hwhsdocument> getByOwnersByPeriod();

	public Hwhsdocument getObject();
	
	public Swhsdocument getSpecificationLine();
	
	public List<Rest> getRestByOwner();
	
	public List<Hwhsdocument> getHeadByReciver(Long first, Long n);
	
	public List<Hwhsdocument> getHeadByOwner(Long first, Long n);
	
	public boolean isModifySpecificationLine(Swhsdocument modify_spec);
		
	public void recoilTransaction(Swhsdocument mod_spec);
	
	public List<Swhsdocument> getDependence_spec();
	
	public void updateSpecificationLine(Swhsdocument spec_mod)	throws JBException;
	
	public void rmSpec(List<Swhsdocument> spec_list) throws JBException;
	
	public List<Rest> getRestByWhs();
	
	public List<Object> getMoveAunit();
	
	public List<Hwhsdocument> getHeadByOwnerBetweenWhs(Long first, Long n);

	public List<Hwhsdocument> getHeadByOwnerIncomActWhs(Long first, Long n);
	
	public List<Hwhsdocument> getHeadByOwnerConsumptionActWhs(Long first, Long n);
	
	public Long getCountHeadByOwnerBetweenWhs();

	public Long getCountHeadByOwnerIncomActWhs();
	
	public Long getCountHeadByOwnerConsumptionActWhs();
	
	public Long getCountHeadByReciver();
	
	public Long getCountHeadByOwner();
}
