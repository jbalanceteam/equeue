package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.Hprice;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Currency;
import org.itx.jbalance.l0.o.Kindofprice;
import org.itx.jbalance.l0.s.Sdocument;
import org.itx.jbalance.l0.s.Sprice;
import org.itx.jbalance.l1.common.CommonHierarchicalDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Prices/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class PricesBean extends CommonHierarchicalDocumentBean implements Prices {

	private static final long serialVersionUID = -463835679513704685L;
	
	
	public PricesBean() {
		super(new Hprice(),new Sprice());
	}

	@SuppressWarnings("unchecked")
	public List<Hprice> getNotEmptyPriceByOwners(){
		return (List<Hprice>) manager.createNamedQuery(
				"Hprice.NotEmptyDocumentsByOwners").setParameter("ContractorOwner",
				sessionprofile.getConfig().getContractorsOwner()).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Hprice> getByOwnersByPeriod() {
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
		if (!sessionprofile.getConfig().isAdmin())
		return (List<Hprice>) manager.createNamedQuery("Hprice.ByOwnersByPeriod")
			.setParameter("periodFrom",sessionprofile.getConfig().getPeriodFrom())
			.setParameter("periodTo",sessionprofile.getConfig().getPeriodTo())
			.setParameter("ContractorOwner",sessionprofile.getConfig().getSelectedContractorOwner())
			.getResultList();
		else
			return (List<Hprice>) manager.createNamedQuery("Hprice.ByPeriod")
			.setParameter("periodFrom",sessionprofile.getConfig().getPeriodFrom())
			.setParameter("periodTo",sessionprofile.getConfig().getPeriodTo())
			.getResultList();
		}else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hprice>) manager.createNamedQuery(
						"Hprice.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hprice>) manager.createNamedQuery(
						"Hprice.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}

	public Hprice getObject() {
		return (Hprice)super.getObject();
	}


	public Sprice getSpecificationLine() {
		return (Sprice)super.getSpecificationLine();
	}
	
	@Override
	public void newObject() {
		super.newObject();
		getObject().setSumm(0F);
	}
	
	@Override
	public List getSpecificationLines() {
		if (getObject().getUId() == null) {
			return null;
		}
		try {
			System.out.println(getSpecificationLine().getSearchKindOfPrice());
			return manager.createNamedQuery("Price.Specification")
			.setParameter("Header", getObject())
			.setParameter("KOP", getSpecificationLine().getSearchKindOfPrice())
			.setParameter("IC", getSpecificationLine().getSearchCurrency())
			.getResultList();
		} catch (IllegalStateException E) {
			// System.out.println(E);
			return null;
		}
	}
	
	@Override
	public void search() {
			setSpecificationDirty(true);
			super.search();
	}
	
	@Override
	public void setSpecificationLine(Sdocument specificationLine) {
		super.setSpecificationLine(specificationLine);
		getSpecificationLine().setSearchKindOfPrice(getSpecificationLine().getKindOfPrice());
		getSpecificationLine().setSearchCurrency(getSpecificationLine().getCurrency());
	}

	@SuppressWarnings("unchecked")
	public List<Sprice> getSpec(Hprice hprice, Kindofprice kindofprice, Currency currency)
	{
		setObject(manager.find(Hprice.class, hprice.getUId()));
		getSpecificationLine().setSearchKindOfPrice(manager.find(Kindofprice.class, kindofprice.getUId()));
		getSpecificationLine().setSearchCurrency(manager.find(Currency.class, currency.getUId()));
		return getSpecificationLines();
	}
	
	/**
	 * to do вынести метод в l2json
	 * @param p
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Sprice> getSpec(Hprice p, Aunit aunit){
		p = manager.find(Hprice.class, p.getUId());
		aunit = manager.find(Aunit.class, aunit.getUId());
		return manager.createNamedQuery("Price.SpecificationAll")
		.setParameter("Header", p)
		.setParameter("AUNIT", aunit)
		.getResultList();
	}
}
