package org.itx.jbalance.l1.d;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateful;
import org.itx.jbalance.l0.h.Hcontract;
import org.itx.jbalance.l0.s.Scontract;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Contrats/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class ContratsBean extends CommonDocumentBean implements Contracts
{
	private static final long serialVersionUID = 6299528713915120823L;

	public ContratsBean(){
		super(new Hcontract(), new Scontract());
	}
	
	//@Override
	protected Long getMaxNumber(){
		Hcontract h = (Hcontract)getObject();
		Long n = (Long)manager.createNamedQuery(
				getObject().getClass().getSimpleName() + ".MaxNumber")
				.setParameter("ContractorOwner",getObject().getContractorOwner())
				.getResultList().get(0);
		n = n == null ? new Long(1) : n++;
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());
		h.setDisdnumber(Integer.toString(c.get(Calendar.YEAR))+"-"+n+h.getTypecontract().getShortname());
		setObject(h);
	    return n;
	}

	@SuppressWarnings("unchecked")
	public List<Hcontract> getAll(){
			return manager.createNamedQuery("Hcontract.All").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Long getAllCount(){
			return (Long)manager.createNamedQuery("Hcontract.AllCount").getSingleResult();
	}
		
	
}
