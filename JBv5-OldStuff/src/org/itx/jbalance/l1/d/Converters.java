package org.itx.jbalance.l1.d;

import java.util.List;
import javax.ejb.Local;
import org.itx.jbalance.l0.s.Sdocument;
import org.itx.jbalance.l0.s.Sware;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Converters extends CommonDocument{
	public List<Sdocument> getDocumentByBill(Sware s);
}
