
/**
 * 
 */
package org.itx.jbalance.l1.d;

import java.util.List;
import java.util.Vector;

import org.itx.jbalance.l0.h.Hcategorygroup;
import org.itx.jbalance.l0.h.Himago;
import org.itx.jbalance.l0.h.Hunit;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Group_;
import org.itx.jbalance.l0.o.Imago;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.o.Packet;
import org.itx.jbalance.l0.s.Simago;
import org.itx.jbalance.l0.s.Sunit;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

import javax.ejb.Stateful;

/**
 * @author galina
 *
 */
@Stateful
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class ImagosdocumentsBean extends CommonDocumentBean implements	Imagosdocuments {
	
	private static final long serialVersionUID = -1495425223764336537L;
	
	
	
	public ImagosdocumentsBean() {
		super(new Himago(), new Simago());
	}

	public Himago getObject() {
		return (Himago)super.getObject();
	}
	
	public Simago getSpecificationLine(){
		return (Simago)super.getSpecificationLine();
	}


//	@SuppressWarnings("unchecked")
//	public List<Himago> searchHimago(Aunit aunit, Measurement measurement)
//	{
//		return (List<Himago>)manager.createNamedQuery("Himago.Head")
//		.setParameter("AUNIT", aunit).setParameter("MEASUREMENT", measurement)
//		.getResultList();
//	}
	
	@SuppressWarnings("unchecked")
	public List<Himago> getHeadByImago(Imago imago)
	{
		return (List<Himago>)manager.createNamedQuery("Himago.getHeadByImago")
		.setParameter("IMAGO", imago).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Simago> getSpecByHeads(List<Himago> list){
			if(list.size() > 0)
				return manager.createNamedQuery("Simago.getSpecByHeads")
						.setParameter("HEADS", list)
						.getResultList();
			return new Vector<Simago>();
	}
	
	@SuppressWarnings("unchecked")
	public List<Simago> getSpecByHeadsRange(List<Himago> list, Long from, Long n){
			if(list.size() > 0)
				return manager.createNamedQuery("Simago.getSpecByHeads")
						.setParameter("HEADS", list)
						.setFirstResult(from.intValue()).setMaxResults(n.intValue())
						.getResultList();
			return new Vector<Simago>();
	}
	
	@SuppressWarnings("unchecked")
	public Long getSpecByHeadsCount(List<Himago> list){
			if(list.size() > 0)
				return (Long)manager.createNamedQuery("Simago.getSpecByHeadsCount")
						.setParameter("HEADS", list)
						.getSingleResult();
			return new Long(0);
	}
}
