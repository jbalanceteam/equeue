package org.itx.jbalance.l1.d;

import java.util.List;
import javax.ejb.Local;
import org.itx.jbalance.l0.h.Hdictionary;
import org.itx.jbalance.l0.s.Sdictionary;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Dictionarys extends CommonDocument{
	public void add(Hdictionary h);
	public List<Hdictionary> getAll();
	public Hdictionary update(Hdictionary h);
	public boolean del(Hdictionary h);
	
	
	public Sdictionary getSpec();
	public void addSpec(Sdictionary s);
	public List<Sdictionary> getSpec(Hdictionary h);
	public Sdictionary updateSpec(Sdictionary s);
	public boolean delSpec(Sdictionary s);
	
	public List<Sdictionary> getSpecByTypeDictionary(Hdictionary h);
}
