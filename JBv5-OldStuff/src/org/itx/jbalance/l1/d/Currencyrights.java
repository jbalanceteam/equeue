package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hcurrencyright;
import org.itx.jbalance.l0.o.Currency;
import org.itx.jbalance.l0.s.Scurrencyright;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Currencyrights extends CommonDocument{

	
	public Scurrencyright getSpecificationLine();
	
	public List<Hcurrencyright> getByOwnersByPeriod();
	
	public List<Currency> getCurrency(List<Hcurrencyright> heads);
}
