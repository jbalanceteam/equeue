/**

 * 
 */
package org.itx.jbalance.l1.d;

import java.util.List;
import org.itx.jbalance.l0.h.Hunit;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.s.Sunit;
import org.itx.jbalance.l1.common.CommonDocument;
import javax.ejb.Local;

/**
 * @author galina
 *
 */
@Local
public interface Units extends CommonDocument {
	
	public Hunit getObject();
	
	public Sunit getSpecificationLine();
	
	public List<Hunit> searchHunit(Aunit aunit, Measurement measurement);
	
	public List<Hunit> getHeadByAunit(Aunit aunit);
	
	public List<Sunit> getSpecByHeads(List<Hunit> list);
	
	public List<Sunit> getAllSpec();
	
	public List<Sunit> getAllSpecRange(int first, int n);
	
	public Long getAllSpecCount();

}
