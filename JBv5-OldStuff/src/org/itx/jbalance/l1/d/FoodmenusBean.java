package org.itx.jbalance.l1.d;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.Hfoodmenu;
import org.itx.jbalance.l0.s.Sfoodmenu;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class FoodmenusBean extends CommonDocumentBean implements Foodmenus {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -7395174424707682274L;

	public FoodmenusBean() {
		super(new Hfoodmenu(),new Sfoodmenu());
	}

	public Hfoodmenu getObject() {
		return (Hfoodmenu)super.getObject();
	}


	public Sfoodmenu getSpecificationLine() {
		return (Sfoodmenu)super.getSpecificationLine();
	}
	
//	@Override
//	public void newObject() {
//		super.newObject();
//		getObject().setSumm(0F);
//	}
	
//	public List getSpecificationLines() {
//		if (getObject().getUId() == null) {
//			return null;
//		}
//		try {
//			System.out.println(getSpecificationLine().getSearchKindOfPrice());
//			return manager.createNamedQuery("Price.Specification")
//			.setParameter("Header", getObject())
//			.setParameter("KOP", getSpecificationLine().getSearchKindOfPrice())
//			.setParameter("IC", getSpecificationLine().getSearchCurrency())
//			.getResultList();
//		} catch (IllegalStateException E) {
//			// System.out.println(E);
//			return null;
//		}
//	}
	
//	@Override
//	public void search() {
//			setSpecificationDirty(true);
//			super.search();
//	}
	
//	@Override
//	public void setSpecificationLine(Sdocument specificationLine) {
//		super.setSpecificationLine(specificationLine);
//		getSpecificationLine().setSearchKindOfPrice(getSpecificationLine().getKindOfPrice());
//		getSpecificationLine().setSearchCurrency(getSpecificationLine().getCurrency());
//	}

//	@SuppressWarnings("unchecked")
//	public List<Sprice> getSpec(Hprice hprice, Kindofprice kindofprice, Currency currency)
//	{
//		setObject(manager.find(Hprice.class, hprice.getUId()));
//		getSpecificationLine().setSearchKindOfPrice(manager.find(Kindofprice.class, kindofprice.getUId()));
//		getSpecificationLine().setSearchCurrency(manager.find(Currency.class, currency.getUId()));
//		return getSpecificationLines();
//	}
	
	/**
	 * to do вынести метод в l2json
	 * @param p
	 * @return
	 */
//	@SuppressWarnings("unchecked")
//	public List<Sprice> getSpec(Hprice p, Aunit aunit){
//		p = manager.find(Hprice.class, p.getUId());
//		aunit = manager.find(Aunit.class, aunit.getUId());
//		return manager.createNamedQuery("Price.SpecificationAll")
//		.setParameter("Header", p)
//		.setParameter("AUNIT", aunit)
//		.getResultList();
//	}
}
