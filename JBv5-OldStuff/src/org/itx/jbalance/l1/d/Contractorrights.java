package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hcontractorright;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.s.Scontractorright;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Contractorrights extends CommonDocument{
	public Hcontractorright getObject();

	public Scontractorright getSpecificationLine();
	
	public List<Hcontractorright> getByOwnersByPeriod();
	public List<Contractor> getContractorByHeads(List<Hcontractorright> heads);
}
