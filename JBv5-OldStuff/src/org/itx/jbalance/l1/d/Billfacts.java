package org.itx.jbalance.l1.d;

import java.util.Date;
import java.util.List;
import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hbillfact;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.s.Sbillfact;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Billfacts extends CommonDocument<Hbillfact,Sbillfact> {
	public Hbillfact getObject();
	public List<Hbillfact> getByOwnersByPeriod();
	public List<Hbillfact> getBillByContractorReceiver(Contractor c, int s, int e);
	public Long getBillByContractorReceiverCount(Contractor c);
	public Sbillfact getSpecificationLine();
	
	public List<Hbillfact> getByContractorsByPeriod(List<Contractor> contractors, Date dateFrom, Date dateTo);
	
	public Float getSummByContarctorsByPeriod(List<Contractor> contractors, Date dateFrom, Date dateTo);
}
