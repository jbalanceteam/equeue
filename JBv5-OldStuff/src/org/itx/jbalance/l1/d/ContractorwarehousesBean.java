package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.Hcontractorwarehouse;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.s.Scontractorwarehouse;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
//@LocalBinding(jndiBinding = "Contractorwarehouses/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 900, removalTimeoutSeconds = 3600)
public class ContractorwarehousesBean extends CommonDocumentBean implements
		Contractorwarehouses {

	private static final long serialVersionUID = -631134669506095675L;
	public ContractorwarehousesBean() {
		super(new Hcontractorwarehouse(),new Scontractorwarehouse());
	}
	
	public Hcontractorwarehouse getObject() {
		return (Hcontractorwarehouse)super.getObject();
	}

	public Scontractorwarehouse getSpecificationLine() {
		return (Scontractorwarehouse)super.getSpecificationLine();
	}
	
	@SuppressWarnings("unchecked")
	public List<Hcontractorwarehouse> getByOwnersByPeriod() {
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hcontractorwarehouse>) manager.createNamedQuery(
						"Hcontractorwarehouse.ByOwnersByPeriod").setParameter(
						"periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.getResultList();
			else
				return (List<Hcontractorwarehouse>) manager.createNamedQuery(
						"Hcontractorwarehouse.ByPeriod").setParameter("periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.getResultList();
		} else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hcontractorwarehouse>) manager.createNamedQuery(
						"Hcontractorwarehouse.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hcontractorwarehouse>) manager.createNamedQuery(
						"Hcontractorwarehouse.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}

	@SuppressWarnings("unchecked")
	public Hcontractorwarehouse getByOwner(Contractor c) {
		List<Hcontractorwarehouse> list = (List<Hcontractorwarehouse>)manager.createNamedQuery(
		"Hcontractorwarehouse.ByOwner").setParameter("ContractorOwner",c)
		.getResultList();
		if(list.size() == 0) return null;
		return list.get(0);
   }
	
   @SuppressWarnings("unchecked")
   public List<Scontractorwarehouse> getWhsByOwner(){
	   return manager.createNamedQuery("Scontractorwarehouse.AllByOwner")
	   			.setParameter("OWNER", sessionprofile.getConfig().getContractorsOwner())
	   			.getResultList();
   }

}
