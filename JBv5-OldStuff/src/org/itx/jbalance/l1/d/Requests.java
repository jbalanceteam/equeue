package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hrequest;
import org.itx.jbalance.l0.s.Srequest;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Requests extends CommonDocument {


	public List<Hrequest> getByOwnersByPeriod();

	public Hrequest getObject();
	
	public Srequest getSpecificationLine();


//	public void importSource();

}
