package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.Hcontractorright;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.s.Scontractorright;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
@Stateful
//@LocalBinding(jndiBinding = "Contractorrights/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 900, removalTimeoutSeconds = 3600)
public class ContractorrightsBean extends CommonDocumentBean implements Contractorrights {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5614980360195552026L;
	
	

	public ContractorrightsBean() {
		super(new Hcontractorright(),new Scontractorright());
	}

	public Hcontractorright getObject() {
		return (Hcontractorright)super.getObject();
	}

	public Scontractorright getSpecificationLine() {
		return (Scontractorright)super.getSpecificationLine();
	}
	
	@SuppressWarnings("unchecked")
	public List<Contractor> getContractorByHeads(List<Hcontractorright> heads){
		return (List<Contractor>)manager.createNamedQuery("Scontractorright.getContractorByHeads").setParameter("HEADS", heads).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Hcontractorright> getByOwnersByPeriod() {
		System.out.println(sessionprofile.getConfig().getPeriodFrom());
		System.out.println(sessionprofile.getConfig().getPeriodTo());
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hcontractorright>) manager.createNamedQuery(
						"Hcontractorright.ByOwnersByPeriod").setParameter(
						"periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.getResultList();
			else
				return (List<Hcontractorright>) manager.createNamedQuery(
						"Hcontractorright.ByPeriod").setParameter("periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.getResultList();
		} else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				System.out.println(sdoc);
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
				System.out.println(ddoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hcontractorright>) manager.createNamedQuery(
						"Hcontractorright.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hcontractorright>) manager.createNamedQuery(
						"Hcontractorright.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}
}
