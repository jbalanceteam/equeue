package org.itx.jbalance.l1.d;

import java.util.List;
import javax.ejb.Stateful;
import org.itx.jbalance.l0.h.Hcategorygroup;
import org.itx.jbalance.l0.o.Group_;
import org.itx.jbalance.l0.s.Scategorygroup;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Categorygroups/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class CategorygroupsBean extends CommonDocumentBean implements Categorygroups
{
	private static final long serialVersionUID = 6299528713915120823L;

	public CategorygroupsBean(){
		super(new Hcategorygroup(), new Scategorygroup());
	}

	public Hcategorygroup getObject(){
		return (Hcategorygroup)super.getObject();
	}

	@SuppressWarnings("unchecked")
	public List<Hcategorygroup> getAll(){
		return manager.createNamedQuery("Hcategorygroup.All").getResultList();
	}

	public void add(Hcategorygroup groupcontractor){
		setObject(groupcontractor);
		create();
	}

	public void update(Hcategorygroup groupcontractor){
		try{
			Hcategorygroup value = manager.find(Hcategorygroup.class, groupcontractor.getUId());
			value.setName(groupcontractor.getName());
			value.setRepetition(groupcontractor.isRepetition());
			setObject(value);
			update();
		}catch (JBException e){
			getLog().error("",e);
		}
	}

	public void del(Hcategorygroup groupcontractor){
		Hcategorygroup value = manager.find(Hcategorygroup.class, groupcontractor.getUId());
		setObject(value);
		try{
			delete();
		}
		catch (JBException e){
			getLog().error("",e);
		}
	}

	
	public void addSpec(Scategorygroup s){
		Hcategorygroup h = manager.find(Hcategorygroup.class, s.getHUId().getUId());
		s.setGroup(manager.find(Group_.class, s.getGroup().getUId()));
		setObject(h);
		setSpecificationLine(s);
		try{
			createSpecificationLine();
		}
		catch (JBException e){
			getLog().error("",e);
		}
	}
	
	public Scategorygroup getSpecLine(){
		return (Scategorygroup)getSpecificationLine();
	}
	
	@SuppressWarnings("unchecked")
	public List<Scategorygroup> getSpec(Hcategorygroup s){
		setObject(manager.find(Hcategorygroup.class, s.getUId()));
		return (List<Scategorygroup>)getSpecificationLines();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Scategorygroup> getAllSpec(){
		return manager.createNamedQuery("Scategorygroup.getAllSpec").getResultList();
		
	}
	
	public boolean delSpec(Scategorygroup s){
		s = manager.find(Scategorygroup.class, s.getUId());
		setSpecificationLine(s);
		try	{
			deleteSpecificationLine();
			return true;
		}
		catch (JBException e){
			getLog().error("",e);
			return false;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Group_> getSpecByHeads(List<Hcategorygroup> list){
		return manager.createNamedQuery("Scategorygroup.getSpecByHeads")
						.setParameter("HEADS", list)
						.getResultList();
	}
	
	public boolean updateSpec(Scategorygroup s){
		Scategorygroup ns = manager.find(Scategorygroup.class, s.getUId());
		ns.setGroup(manager.find(Group_.class, s.getGroup().getUId()));
		setSpecificationLine(ns);
		try{
			updateSpecificationLine();
			return true;
		}
		catch (JBException e){
			getLog().error("",e);
			return false;
		}
	}
	
	public List<Hcategorygroup> getHead(){
		return sessionprofile.getConfig().getEnabledHcategorygroup();
	}
}
