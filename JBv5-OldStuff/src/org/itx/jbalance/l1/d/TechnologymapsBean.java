/**
 * 
 */
package org.itx.jbalance.l1.d;

import java.util.List;
import java.util.Vector;

import org.itx.jbalance.l0.h.Hcategorygroup;
import org.itx.jbalance.l0.h.Htechnologymap;
import org.itx.jbalance.l0.h.Hunit;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Division;
import org.itx.jbalance.l0.o.Group_;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.o.Packet;
import org.itx.jbalance.l0.s.Stechnologymap;
import org.itx.jbalance.l0.s.Sunit;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

import javax.ejb.Stateful;

/**
 * @author galina
 *
 */
@Stateful
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class TechnologymapsBean extends CommonDocumentBean implements	Technologymaps {
	
	private static final long serialVersionUID = -1495425223764336537L;
	
	public TechnologymapsBean() {
		super(new Htechnologymap(), new Stechnologymap());
	}

	public Htechnologymap getObject() {
		return (Htechnologymap)super.getObject();
	}
	
	public Stechnologymap getSpecificationLine(){
		return (Stechnologymap)super.getSpecificationLine();
	}


	@SuppressWarnings("unchecked")
	public List<Htechnologymap> searchHtechnologymap(Aunit aunit, Division division)
	{
		return (List<Htechnologymap>)manager.createNamedQuery("Htechnologymap.Head")
		.setParameter("AUNIT", aunit).setParameter("DIVISION", division)
		.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Htechnologymap> getHeadByAunit(Aunit aunit)
	{
		return (List<Htechnologymap>)manager.createNamedQuery("Htechnologymap.getHeadByAunit")
		.setParameter("AUNIT", aunit).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Stechnologymap> getSpecByHeads(List<Htechnologymap> list){
			if(list.size() > 0)
				return manager.createNamedQuery("Stechnologymap.getSpecByHeads")
						.setParameter("HEADS", list)
						.getResultList();
			return new Vector<Stechnologymap>();
	}
	
    @SuppressWarnings("unchecked")
	public List<Stechnologymap> getBasicImagosByHead(Htechnologymap head)
    {
    	if(head != null)
			return manager.createNamedQuery("Stechnologymap.getBasicImagoByHead")
					.setParameter("HEAD", head)
					.getResultList();
		return new Vector<Stechnologymap>();
    }
	
	@SuppressWarnings("unchecked")
	public List<Stechnologymap> getSubImagosByHead(Htechnologymap head, Stechnologymap spec)
	{
		if(head != null)
			return manager.createNamedQuery("Stechnologymap.getSubImagoByHead")
					.setParameter("HEAD", head).setParameter("SOURCE", spec)
					.getResultList();
		return new Vector<Stechnologymap>();
	}
}
