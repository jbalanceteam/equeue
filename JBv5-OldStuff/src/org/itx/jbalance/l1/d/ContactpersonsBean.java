package org.itx.jbalance.l1.d;

import java.util.ArrayList;
import java.util.Date;

import java.util.List;

import javax.ejb.Stateful;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
import org.itx.jbalance.l0.h.Hcontactpersons;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.s.Scontactpersons;

@Stateful
@LocalBinding(jndiBinding = "Contactpersons/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class ContactpersonsBean extends CommonDocumentBean implements Contactpersons {

	private static final long serialVersionUID = 5177964569087956836L;

	public ContactpersonsBean() {
		super(new Hcontactpersons(), new Scontactpersons());
	}
	
	@SuppressWarnings("unchecked")
	public void AddHead(Hcontactpersons h){
		List<Hcontactpersons> l = (List<Hcontactpersons>)manager.
		createQuery("from Hcontactpersons where juridicalName = "+h.getJuridicalName().getUId())
		.getResultList();
		if(l.size() == 0){
			h.setJuridicalName((Juridical)manager.find(Juridical.class, h.getJuridicalName().getUId()));
			super.setObject(h);
			super.create();
		}else{super.setObject(l.get(0));}
    }
	
	@SuppressWarnings("unchecked")
	public boolean AddContactPersons(Scontactpersons s){
		List<Scontactpersons> l = manager.createNamedQuery("Contactpersons.SearchContactpersons")
		.setParameter("Header", getObject())
		.setParameter("realName", s.getPhysical().getRealName())
		.setParameter("surname", s.getPhysical().getSurname())
		.setParameter("patronymic", s.getPhysical().getPatronymic())
		.getResultList();
		if(l.size() != 0){
			s.setPhysical(l.get(0).getPhysical());
			s.setCanMakeDecision(false);
            return true;
		}else{
			return false;
		}	
   }
	
	@SuppressWarnings("unchecked")
	public Hcontactpersons getHead(Juridical j){
      List<Hcontactpersons> l = (List<Hcontactpersons>)manager.
			createQuery("from Hcontactpersons where juridicalName = "+j.getUId())
			.getResultList();
	  if(l.size() == 0)
		  return null; 
	  else{ 
		    super.setObject(l.get(0));
		  	return (Hcontactpersons)super.getObject();
		  }
	}
	
	@SuppressWarnings("unchecked")
	public List<Scontactpersons> getSpecification(){
		return manager.createNamedQuery("Contactpersons.Specification")
		.setParameter("Header", getObject())
		.getResultList();
	}
	
		
	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<Scontactpersons> getContactByJuridicalAndBirthDate(List<Contractor> list, Date dateStart, Date dateEnd){
		
		//get list contact persons in jur
		List<Scontactpersons> contacts = (List<Scontactpersons>)manager.createNamedQuery("Contactpersons.getContatcsByJuridicals")
						.setParameter("LIST", list)
						.getResultList();
		List<Scontactpersons> results = new ArrayList<Scontactpersons>();
		
		// find contacts per. by between date. Not using year
		for (Scontactpersons contact: contacts){
			Date birthDay = contact.getPhysical().getBirthday();
			if (birthDay != null) {
				// set currency year in everyone contact from list (contacts)
				dateStart.setYear(birthDay.getYear());
				dateEnd.setYear(birthDay.getYear());
				if (dateStart.getTime() <= birthDay.getTime() && birthDay.getTime() <= dateEnd.getTime()) {
					results.add(contact);
				}
			}
		}
		return results;
		
	}

}
