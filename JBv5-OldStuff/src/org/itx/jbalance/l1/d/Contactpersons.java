package org.itx.jbalance.l1.d;


import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import org.itx.jbalance.l1.common.CommonDocument;
import org.itx.jbalance.l0.h.Hcontactpersons;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.s.Scontactpersons;

@Local
public interface Contactpersons extends CommonDocument {
	public void AddHead(Hcontactpersons h);
	public Hcontactpersons getHead(Juridical j);
	public List<Scontactpersons> getSpecification();
	public boolean AddContactPersons(Scontactpersons s);
	public List<Scontactpersons> getContactByJuridicalAndBirthDate(List<Contractor> list, Date dateStart, Date dateEnd);
//	public List<Scontactpersons> getDecisionMakers();
}
