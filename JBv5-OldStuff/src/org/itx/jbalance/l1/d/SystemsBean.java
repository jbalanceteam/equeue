package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.Hsystem;
import org.itx.jbalance.l0.s.Ssystem;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
@SuppressWarnings("unchecked")
@Stateful
//@LocalBinding(jndiBinding = "Systems/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class SystemsBean extends CommonDocumentBean implements Systems {

	private static final long serialVersionUID = -8430635966944491477L;
	
	public SystemsBean() {
		super(new Hsystem(), new Ssystem());
	}

	public Hsystem getObject() {
		return (Hsystem)super.getObject();
	}

	public Ssystem getSpecificationLine() {
		return (Ssystem)super.getSpecificationLine();
	}


	@SuppressWarnings("unchecked")
	public List<Hsystem> getByOwnersByPeriod() {
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hsystem>) manager.createNamedQuery(
						"Hsystem.ByOwnersByPeriod").setParameter(
						"periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.getResultList();
			else
				return (List<Hsystem>) manager.createNamedQuery(
						"Hsystem.ByPeriod").setParameter("periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.getResultList();
		} else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				System.out.println(sdoc);
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
				System.out.println(ddoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hsystem>) manager.createNamedQuery(
						"Hsystem.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hsystem>) manager.createNamedQuery(
						"Hsystem.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}


}
