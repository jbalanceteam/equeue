package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.List;
import javax.ejb.Stateful;
import org.itx.jbalance.l0.h.Hactionright;
import org.itx.jbalance.l0.o.Action;
import org.itx.jbalance.l0.s.Sactionright;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
@SuppressWarnings("unchecked")
@Stateful
//@LocalBinding(jndiBinding = "Actionrights/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 900, removalTimeoutSeconds = 3600)
public class ActionrightsBean extends CommonDocumentBean<Hactionright,Sactionright> implements Actionrights {

	
	private static final long serialVersionUID = -8430635966944491477L;
	
	public ActionrightsBean() {
		super(new Hactionright(), new Sactionright());
	}

	public Hactionright getObject() {
		return (Hactionright)super.getObject();
	}

	public Sactionright getSpecificationLine() {
		return (Sactionright)super.getSpecificationLine();
	}
	
	public List<Action> getAction(List<Hactionright> heads){
		return (List<Action>)manager.createNamedQuery("Sactionright.getEnabledByHeads").setParameter("HEADS", heads).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Hactionright> getByOwnersByPeriod() {
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hactionright>) manager.createNamedQuery(
						"Hactionright.ByOwnersByPeriod").setParameter(
						"periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.getResultList();
			else
				return (List<Hactionright>) manager.createNamedQuery(
						"Hactionright.ByPeriod").setParameter("periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.getResultList();
		} else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				System.out.println(sdoc);
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
				System.out.println(ddoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hactionright>) manager.createNamedQuery(
						"Hactionright.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hactionright>) manager.createNamedQuery(
						"Hactionright.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}

}
