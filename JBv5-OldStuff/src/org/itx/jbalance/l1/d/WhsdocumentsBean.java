package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.ejb.Stateful;
import org.itx.jbalance.l0.Rest;
import org.itx.jbalance.l0.h.Hwhsdocument;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.s.Sprice;
import org.itx.jbalance.l0.s.Swhsdocument;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.Utils;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class WhsdocumentsBean extends CommonDocumentBean implements Whsdocuments {

	private static final long serialVersionUID = 151901703348257702L;
	
	public List<Swhsdocument> whsdocument_owner_rest = null;
	
	public List<Swhsdocument> dependence_spec = null;
	
	public List<Swhsdocument> getDependence_spec() {
		return dependence_spec;
	}

	public void setDependence_spec(List<Swhsdocument> dependenceSpec) {
		dependence_spec = dependenceSpec;
	}

	public WhsdocumentsBean() {
		super(new Hwhsdocument(),new Swhsdocument());
	}
	
	@SuppressWarnings("unchecked")
	public List<Hwhsdocument> getByOwnersByPeriod(){
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
		if (!sessionprofile.getConfig().isAdmin())
		return (List<Hwhsdocument>) manager.createNamedQuery(
				"Hwhsdocument.ByOwnersByPeriod").setParameter("periodFrom",
				sessionprofile.getConfig().getPeriodFrom()).setParameter("periodTo",
				sessionprofile.getConfig().getPeriodTo()).setParameter(
				"ContractorOwner",
				sessionprofile.getConfig().getSelectedContractorOwner())
				.getResultList();
		else
			return (List<Hwhsdocument>) manager.createNamedQuery(
			"Hwhsdocument.ByPeriod").setParameter("periodFrom",
			sessionprofile.getConfig().getPeriodFrom()).setParameter("periodTo",
			sessionprofile.getConfig().getPeriodTo()).getResultList();
		}else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hwhsdocument>) manager.createNamedQuery(
						"Hwhsdocument.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hwhsdocument>) manager.createNamedQuery(
						"Hwhsdocument.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}
	
	@Override
	public Hwhsdocument getObject() {
		return (Hwhsdocument)super.getObject();
	}

	@Override
	public Swhsdocument getSpecificationLine() {
		return (Swhsdocument)super.getSpecificationLine();
	}
	
	@Override
	public void createSpecificationLine() throws JBException {
		importSource();
		getObject().setSumm(getObject().getSumm() 
				+ getSpecificationLine().getPrice() 
				* getSpecificationLine().getQuantity()
				- getSpecificationLine().getSumm());
		update();
		if(isCreateSpecificationLine()){
			List<Swhsdocument> list_spec_receiver = getTransaction();
			for(Swhsdocument s : list_spec_receiver){
				setSpecificationLine(s.getInOutLink());
				super.updateSpecificationLine();
				s.setInOutLink(getSpecificationLine());
				setSpecificationLine(s);
				getSpecificationLine().setOpenDate(getObject().getOpenDate());
				getSpecificationLine().setSumm(getSpecificationLine().getPrice() 
						* getSpecificationLine().getQuantity());
				getSpecificationLine().setOpenDate(getObject().getOpenDate());
				super.createSpecificationLine();
			}
			if(list_spec_receiver.size() == 0){
				getSpecificationLine().setSumm(getSpecificationLine().getPrice() 
						* getSpecificationLine().getQuantity());
				getSpecificationLine().setOpenDate(getObject().getOpenDate());
				super.createSpecificationLine();
			}
		} else throw new JBException();
	}
	
	public List<Swhsdocument> getTransaction(){
		List<Swhsdocument> list_spec_receiver = new Vector<Swhsdocument>();
		for(Swhsdocument spec_rest : whsdocument_owner_rest){
			System.out.println(">>>>>>>>>>>>> spec rest "  + spec_rest.getRest() + " <<<<<<<<<<<<< " + spec_rest.getUId());
			Swhsdocument spec = new Swhsdocument();
			Utils.copy(getSpecificationLine(), spec);
			spec.setUId(null);
			list_spec_receiver.add(spec);
			if(spec_rest.getRest() <= getSpecificationLine().getQuantity()){
				spec.setRest(spec_rest.getRest());
				spec_rest.setRest(new Float(0));
				getSpecificationLine().setQuantity(getSpecificationLine().getQuantity() - spec.getRest());
			}else{
				manager.clear();
				spec_rest.setRest(spec_rest.getRest() - getSpecificationLine().getQuantity());
				spec.setRest(getSpecificationLine().getQuantity());
				getSpecificationLine().setQuantity(new Float(0));
			}
			spec.setInOutLink(spec_rest);
			spec.setQuantity(spec.getRest());
			System.out.println(">>>>>>>>>>>>> spec rest "  + spec_rest.getRest() + " <<<<<<<<<<<<< " + spec_rest.getUId() + " >>> rest " +
					"  " + manager.find(Swhsdocument.class, spec_rest.getUId()).getRest());
			if(getSpecificationLine().getQuantity() <= 0) break;
		}
		
		return list_spec_receiver;
	}
	
	@SuppressWarnings("unchecked")
	public List<Rest> getRestByOwner(){
		try{
			List<Juridical> list = new Vector<Juridical>();
			list.add(getObject().getContractorOwner());
			Date date = getObject() != null ? getObject().getOpenDate() : new Date(System.currentTimeMillis());
			System.out.println("OWNER = " + getObject().getContractorOwner().getUId());
			System.out.println("WHS = " + getObject().getWhsTo().getUId());
			System.out.println("OPEN_DATE = " + date);
			List<Rest> list_rest = (List<Rest>)manager.createNamedQuery("Swhsdocument.getRestByOwner")
				.setParameter("OWNER", getObject().getContractorOwner())
				.setParameter("WHSOWNER", getObject().getWhsTo())
				.setParameter("OPEN_DATE", date)
				.getResultList();
			System.out.println(">>>>>> Size list rest " + list_rest.size());
			List<Rest> list_recoil = (List<Rest>)manager.createNamedQuery("Swhsdocument.getRestByWhsIsNotInOutLink")
				.setParameter("OWNER", getObject().getContractorOwner())
				.setParameter("WHSOWNER", getObject().getWhsTo())
				.setParameter("OPEN_DATE", date)
				.getResultList();
			System.out.println(">>>>>> Size list recoil rest " + list_recoil.size());
			for(Rest rest : list_rest){
				int index_rest_recoil = list_recoil.indexOf(rest);
				System.out.println("Index >> " + index_rest_recoil);
				rest.setRest(index_rest_recoil >= 0 ? rest.getRest() + list_recoil.get(index_rest_recoil).getRest() : rest.getRest());
			}
			return list_rest;
		}catch (Exception e) {
			getLog().error("",e);
			return null;
		}	
		 
	}
	
	@SuppressWarnings("unchecked")
	public List<Rest> getRestByWhs(){
		Date date = getObject().getOpenDate() != null ? getObject().getOpenDate() : new Date(System.currentTimeMillis());
		return (List<Rest>)manager.createNamedQuery("Swhsdocument.getRestByWhs")
		.setParameter("WHSOWNER", getObject().getWhsTo())
		.setParameter("OPEN_DATE", date)
		.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Hwhsdocument> getHeadByReciver(Long first, Long n){
		return (List<Hwhsdocument>)manager.createNamedQuery("Hwhsdocument.AllByReciver")
						.setParameter("OWNER", sessionprofile.getConfig().getContractorsOwner())
						.setFirstResult(first.intValue()).setMaxResults(n.intValue())
						.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Hwhsdocument> getHeadByOwner(Long first, Long n){
		return (List<Hwhsdocument>)manager.createNamedQuery("Hwhsdocument.AllByOwner")
						.setParameter("OWNER", sessionprofile.getConfig().getContractorsOwner())
						.setFirstResult(first.intValue()).setMaxResults(n.intValue())
						.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Hwhsdocument> getHeadByOwnerBetweenWhs(Long first, Long n){
		return (List<Hwhsdocument>)manager.createNamedQuery("Hwhsdocument.getHeadByOwnerBetweenWhs")
			.setParameter("OWNER", sessionprofile.getConfig().getContractorsOwner())
			.setFirstResult(first.intValue()).setMaxResults(n.intValue())
			.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Hwhsdocument> getHeadByOwnerIncomActWhs(Long first, Long n){
		return (List<Hwhsdocument>)manager.createNamedQuery("Hwhsdocument.getHeadByOwnerIncomActWhs")
			.setParameter("OWNER", sessionprofile.getConfig().getContractorsOwner())
			.setFirstResult(first.intValue()).setMaxResults(n.intValue())
			.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Hwhsdocument> getHeadByOwnerConsumptionActWhs(Long first, Long n){
		return (List<Hwhsdocument>)manager.createNamedQuery("Hwhsdocument.getHeadByOwnerConsumptionActWhs")
			.setParameter("OWNER", sessionprofile.getConfig().getContractorsOwner())
			.setFirstResult(first.intValue()).setMaxResults(n.intValue())
			.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	public Long getCountHeadByReciver(){
		return (Long)manager.createNamedQuery("Hwhsdocument.CountAllByReciver")
						.setParameter("OWNER", sessionprofile.getConfig().getContractorsOwner())
						.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public Long getCountHeadByOwner(){
		return (Long)manager.createNamedQuery("Hwhsdocument.CountAllByOwner")
						.setParameter("OWNER", sessionprofile.getConfig().getContractorsOwner())
						.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public Long getCountHeadByOwnerBetweenWhs(){
		return (Long)manager.createNamedQuery("Hwhsdocument.getCountHeadByOwnerBetweenWhs")
			.setParameter("OWNER", sessionprofile.getConfig().getContractorsOwner())
			.getSingleResult();
		
	}
	
	@SuppressWarnings("unchecked")
	public Long getCountHeadByOwnerIncomActWhs(){
		return (Long)manager.createNamedQuery("Hwhsdocument.getCountHeadByOwnerIncomActWhs")
			.setParameter("OWNER", sessionprofile.getConfig().getContractorsOwner())
			.getSingleResult();
		
	}
	
	@SuppressWarnings("unchecked")
	public Long getCountHeadByOwnerConsumptionActWhs(){
		return (Long)manager.createNamedQuery("Hwhsdocument.getCountHeadByOwnerConsumptionActWhs")
			.setParameter("OWNER", sessionprofile.getConfig().getContractorsOwner())
			.getSingleResult();
		
	}
	
	@SuppressWarnings("unchecked")
	public boolean isCreateSpecificationLine(){
		boolean result = true; 
		if(getObject().getWhsTo().getZero() != null && getObject().getWhsTo().getZero()){
			whsdocument_owner_rest = new Vector<Swhsdocument>();
			return result;
		}
		whsdocument_owner_rest = (List<Swhsdocument>) manager.createNamedQuery("Swhsdocument.getSpecByOwnerAunit")
						.setParameter("OWNER", getObject().getContractorOwner())
						.setParameter("AUNIT", getSpecificationLine().getArticleUnit())
						.setParameter("MEASURMENT", getSpecificationLine().getMeasurement())
						.setParameter("WHSOWNER", getObject().getWhsTo())
						.setParameter("PACKET", getSpecificationLine().getPacket())
						.setParameter("OPEN_DATE", getObject().getOpenDate())
						.getResultList();
		System.out.println("Owner = " + getObject().getContractorOwner().getUId());
		System.out.println("Aunit = " + getSpecificationLine().getArticleUnit().getUId());
		System.out.println("OpenDate = " + getObject().getOpenDate());
		System.out.println("MEASURMENT = " + getSpecificationLine().getMeasurement().getUId());
		System.out.println("PACKET = " + getSpecificationLine().getPacket().getUId());
		System.out.println("WHSOWNER = " + getObject().getWhsTo().getUId());
		System.out.println(">>>>>>>>>>>>>>>>>>>>> " + whsdocument_owner_rest.size());
		float sum = 0;
		for(Swhsdocument spec : whsdocument_owner_rest)
			sum += spec.getRest();
		result = sum >= getSpecificationLine().getQuantity() ? true : false;
		System.out.println("Sum = " + sum);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Swhsdocument> getDependence(Swhsdocument spec){
		return (List<Swhsdocument>)manager.createNamedQuery("Swhsdocument.getRestByInOutLink")
		.setParameter("SPEC", spec)
		.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Swhsdocument> getDependence(List<Swhsdocument> spec){
		return (List<Swhsdocument>)manager.createNamedQuery("Swhsdocument.getRestsByInOutLink")
		.setParameter("SPEC", spec)
		.getResultList();
	}

	
	
	public boolean isModifySpecificationLine(Swhsdocument modify_spec){
		float difference_quantity = modify_spec.getQuantity() - getSpecificationLine().getQuantity();
		if(getSpecificationLine().getRest() + difference_quantity >= 0){
			getSpecificationLine().setQuantity(modify_spec.getQuantity());
			getSpecificationLine().setRest(getSpecificationLine().getRest() + difference_quantity);
			return true;
		}
		dependence_spec = getDependence(getSpecificationLine());
		return false;
	}
	
	
	public void recoilTransaction(Swhsdocument mod_spec){
		float difference_quantity = getSpecificationLine().getQuantity() - mod_spec.getQuantity();
		getSpecificationLine().setRest(getSpecificationLine().getRest() - difference_quantity);
		getSpecificationLine().setQuantity(mod_spec.getQuantity());
		try{
			updateSpecificationLine();
		}catch (Exception e) {
			getLog().error("",e);
		}
		setObject(getSpecificationLine().getInOutLink().getHUId());
		setSpecificationLine(getSpecificationLine().getInOutLink());
		getSpecificationLine().setRest(getSpecificationLine().getRest() + difference_quantity);
		try{
			updateSpecificationLine();
		}catch (Exception e) {
			getLog().error("",e);
		}
	}
	
	
	/**
	 * Если остаток взялся не откуда, то просто обновляем спеку, если это возможно. 
	 * Если увеличиваем количество то создаём еще одну спека на разницу количества и проводим ее;
	 * Если уменьшаем количество то откатываем остаток назад в приходную накладную
	 * @param spec_mod
	 * @throws JBException
	 */
	public void updateSpecificationLine(Swhsdocument spec_mod) 
												throws JBException {

		float difference_quantity = getSpecificationLine().getQuantity() - spec_mod.getQuantity();
		if(spec_mod.getQuantity() > getSpecificationLine().getQuantity()){
			if(getSpecificationLine().getInOutLink() == null && isModifySpecificationLine(spec_mod)){
				updateSpecificationLine();
				return;
			}
			Swhsdocument spec = new Swhsdocument();
			Utils.copy(getSpecificationLine(), spec);
			spec.setUId(null);
			spec.setQuantity(spec_mod.getQuantity() - getSpecificationLine().getQuantity());
			setSpecificationLine(spec);
			createSpecificationLine();
		}else if(spec_mod.getQuantity() < getSpecificationLine().getQuantity()){
			if(isModifySpecificationLine(spec_mod)){
				updateSpecificationLine();
			if(getSpecificationLine().getInOutLink() != null){
				Swhsdocument tmp = getSpecificationLine();
				setSpecificationLine(getSpecificationLine().getInOutLink());
				setObject(getSpecificationLine().getHUId());
				getSpecificationLine().setRest(getSpecificationLine().getRest() + difference_quantity);
				updateSpecificationLine();
				setObject(tmp.getHUId());
				setSpecificationLine(tmp);
			}
		  }else	throw new JBException();
		}
	}	
	
	@Override
	public void updateSpecificationLine() throws JBException {
		System.out.println(">>>> Summm 1 = " + getSpecificationLine().getSumm());
		getObject().setSumm(getObject().getSumm() - getSpecificationLine().getSumm());
		getSpecificationLine().setSumm(getSpecificationLine().getPrice() * getSpecificationLine().getQuantity());
		System.out.println(">>>> Summm 2 =  " + getSpecificationLine().getSumm());
		getObject().setSumm(getObject().getSumm() + getSpecificationLine().getSumm());
		update();
		super.updateSpecificationLine();
	}

	@Override
	public void newObject() {
		super.newObject();
		getObject().setSumm(0F);
	}

	public void importSource() {
		if(getSpecificationLine().getSource() instanceof Sprice){
			if(getSpecificationLine().getPrice()==null)getSpecificationLine().setPrice(((Sprice)getSpecificationLine().getSource()).getPrice());
			if(getSpecificationLine().getCurrency()==null)getSpecificationLine().setCurrency(((Sprice)getSpecificationLine().getSource()).getCurrency());
			getSpecificationLine().setArticleUnit(((Sprice)getSpecificationLine().getSource()).getArticleUnit());
		}
	}
	
	public void rmSpec(List<Swhsdocument> spec_list) throws JBException{
		//TODO
		dependence_spec = getDependence(spec_list);
		if(dependence_spec.size() > 0) throw new JBException();
		for(Swhsdocument spec : spec_list){
			if(spec.getInOutLink() != null){
				setSpecificationLine(spec.getInOutLink());
				getSpecificationLine().setRest(getSpecificationLine().getRest() + spec.getRest());
				setObject(getSpecificationLine().getHUId());
				updateSpecificationLine();
			}
			setObject(spec.getHUId());
			setSpecificationLine(spec);
			getObject().setSumm(
					getObject().getSumm() - getSpecificationLine().getSumm());
			update();
			deleteSpecificationLine();
		}
	}
	
	public List<Object> getMoveAunit(){
		return manager.createNamedQuery("Swhsdocument.getMoveAunitByPeriod").getResultList();
	}

}
