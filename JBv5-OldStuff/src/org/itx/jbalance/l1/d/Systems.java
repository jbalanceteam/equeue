package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hsystem;
import org.itx.jbalance.l0.s.Ssystem;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Systems extends CommonDocument {


	public Ssystem getSpecificationLine();
	
	public Hsystem getObject();
	
	public List<Hsystem> getByOwnersByPeriod();
}
