package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.HJobCategory;
import org.itx.jbalance.l0.o.Job;
import org.itx.jbalance.l0.s.SJobCategory;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "JobCategories/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class JobCategoriesBean extends CommonDocumentBean implements JobCategories
{
	private static final long serialVersionUID = 6299528713915120823L;

	public JobCategoriesBean()
	{
		super(new HJobCategory(), new SJobCategory());
	}

	public HJobCategory getObject()
	{
		return (HJobCategory)super.getObject();
	}

	@SuppressWarnings("unchecked")
	public List<HJobCategory> getJobCategories()
	{
		return manager.createNamedQuery("HJobCategory.All").getResultList();
	}

	public void add(HJobCategory jobCategory)
	{
		setObject(jobCategory);
		create();
	}

	public void update(HJobCategory jobCategory)
	{
		HJobCategory value = manager.find(HJobCategory.class, jobCategory.getUId());
		value.setName(jobCategory.getName());
		setObject(value);
		try
		{
			update();
		}
		catch (JBException e)
		{
			getLog().error("",e);
		}
	}

	public void del(HJobCategory jobCategory)
	{
		HJobCategory value = manager.find(HJobCategory.class, jobCategory.getUId());
		setObject(value);
		try
		{
			delete();
		}
		catch (JBException e)
		{
			getLog().error("",e);
		}
	}

	public void addSpec(HJobCategory jobCategory, Job job)
	{
		HJobCategory header = manager.find(HJobCategory.class, jobCategory.getUId());
		setObject(header);

		SJobCategory spec = new SJobCategory();
		spec.setJob(job);
		setSpecificationLine(spec);
		try
		{
			createSpecificationLine();
		}
		catch (JBException e)
		{
			getLog().error("",e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<SJobCategory> getSpec(HJobCategory jobCategory)
	{
		setObject(manager.find(HJobCategory.class, jobCategory.getUId()));
		return (List<SJobCategory>)getSpecificationLines();
	}

	public void delSpec(SJobCategory jobCategory)
	{
		SJobCategory spec = manager.find(SJobCategory.class, jobCategory.getUId());
		setSpecificationLine(spec);
		try
		{
			deleteSpecificationLine();
		}
		catch (JBException e)
		{
			getLog().error("",e);
		}
	}

	public void updateSpec(SJobCategory jobCategory, Job job)
	{
		Job f = manager.find(Job.class, job.getUId());
		SJobCategory spec = manager.find(SJobCategory.class, jobCategory.getUId());
		spec.setJob(f);

		setSpecificationLine(spec);

		try
		{
			updateSpecificationLine();
		}
		catch (JBException e)
		{
			getLog().error("",e);
		}
	}
}
