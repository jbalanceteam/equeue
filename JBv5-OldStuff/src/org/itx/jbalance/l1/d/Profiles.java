package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hprofile;
import org.itx.jbalance.l0.o.Oper;
import org.itx.jbalance.l0.s.Sprofile;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Profiles extends CommonDocument <Hprofile,Sprofile>  {


	public Sprofile getSpecificationLine();

	public Hprofile getByOperReceiver(Oper oper);
	
	public List<Hprofile> getByOwnersByPeriod();
}
