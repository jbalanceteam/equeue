package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.HdivisionJuridical;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.s.SdivisionJuridical;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "DivisionsJuridicals/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class DivisionsJuridicalsBean extends CommonDocumentBean implements DivisionsJuridicals
{
	private static final long serialVersionUID = 6299528713915120823L;

	public DivisionsJuridicalsBean(){
		super(new HdivisionJuridical(), new SdivisionJuridical());
	}
	
	@SuppressWarnings("unchecked")
	public List<HdivisionJuridical>getDivisionInJuridical(Juridical j){
		return (List<HdivisionJuridical>)super.manager.createNamedQuery("HdivisionJuridical.HeadJuridical")
		.setParameter("juridical", j)
		.getResultList(); 
	}
	
	@SuppressWarnings("unchecked")
	public List<SdivisionJuridical> getSpec(HdivisionJuridical h){
		return (List<SdivisionJuridical>)super.manager.createNamedQuery("SdivisionJuridical.Specification")
		.setParameter("Header", h)
		.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<SdivisionJuridical> getSpecByEmpl(SdivisionJuridical sd){
		return (List<SdivisionJuridical>)super.manager.createNamedQuery("SdivisionJuridical.getSpecByEmpl")
		.setParameter("EMPL", sd.getEmployee())
		.getResultList();
	
	}

	public HdivisionJuridical getDivisionJuridical(HdivisionJuridical hdj) {
		return (HdivisionJuridical)manager.find(HdivisionJuridical.class, hdj);
	}
}
