/**
 * 
 */
package org.itx.jbalance.l1.d;

import java.util.List;
import java.util.Vector;

import org.itx.jbalance.l0.h.Hcategorygroup;
import org.itx.jbalance.l0.h.Hseason;
import org.itx.jbalance.l0.h.Htechnologymap;
import org.itx.jbalance.l0.h.Hunit;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Division;
import org.itx.jbalance.l0.o.Group_;
import org.itx.jbalance.l0.o.Imago;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.o.Packet;
import org.itx.jbalance.l0.s.Sseason;
import org.itx.jbalance.l0.s.Stechnologymap;
import org.itx.jbalance.l0.s.Sunit;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

import javax.ejb.Stateful;

/**
 * @author galina
 *
 */
@Stateful
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class SeasonsBean extends CommonDocumentBean implements	Seasons {
	
	private static final long serialVersionUID = -1495425223764336537L;
	
	public SeasonsBean() {
		super(new Hseason(), new Sseason());
	}

	public Hseason getObject() {
		return (Hseason)super.getObject();
	}
	
	public Sseason getSpecificationLine(){
		return (Sseason)super.getSpecificationLine();
	}

	@SuppressWarnings("unchecked")
	public List<Hseason> getHeadByImago(Imago imago)
	{
		return (List<Hseason>)manager.createNamedQuery("Hseason.getHeadByImago")
		.setParameter("IMAGO", imago).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Sseason> getSpecByHead(Hseason head) {
		return (List<Sseason>)manager.createNamedQuery("Sseason.getSpecByHead")
		.setParameter("HEAD", head).getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Sseason> getSpecByHeads(List<Hseason> head) {
		return (List<Sseason>)manager.createNamedQuery("Sseason.getSpecByHeads")
		.setParameter("HEADS", head).getResultList();
	}
}
