package org.itx.jbalance.l1.d;

import java.util.List;
import javax.ejb.Stateful;
import org.itx.jbalance.l0.h.Hconverter;
import org.itx.jbalance.l0.s.Sconverter;
import org.itx.jbalance.l0.s.Sdocument;
import org.itx.jbalance.l0.s.Sware;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Converters/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class ConvertersBean extends CommonDocumentBean implements Converters{
	
	private static final long serialVersionUID = 6299528713915120823L;

	public ConvertersBean(){
		super(new Hconverter(), new Sconverter());
	}
	
	@SuppressWarnings("unchecked")
	public List<Sdocument> getDocumentByBill(Sware s){
		return (List<Sdocument>)manager.createNamedQuery("Sconverter.getDocumentBySware")
				.setParameter("WARE", s)
				.getResultList();
	}
	
	
}
