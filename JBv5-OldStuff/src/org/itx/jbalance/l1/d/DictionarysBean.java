package org.itx.jbalance.l1.d;

import java.util.List;
import javax.ejb.Stateful;
import org.itx.jbalance.l0.h.Hdictionary;
import org.itx.jbalance.l0.s.Sdictionary;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Dictionarys/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class DictionarysBean extends CommonDocumentBean implements Dictionarys
{
	private static final long serialVersionUID = 6299528713915120823L;

	public DictionarysBean(){
		super(new Hdictionary(), new Sdictionary());
	}
	
	public void add(Hdictionary h){
		setObject(h);
		create();
	}
	
	@SuppressWarnings("unchecked")
	public List<Hdictionary> getAll(){
		return manager.createNamedQuery("Hdictionary.All").getResultList();
	}
	
	public Hdictionary update(Hdictionary h){
		Hdictionary head = manager.find(Hdictionary.class, h.getUId());
		head.setName(h.getName());
		head.setTypeDictionary(h.getTypeDictionary());
		setObject(head);
		try{
			update();
			return (Hdictionary)getObject();
		}catch (Exception e) {
			// TODO: handle exception
			getLog().error("",e);
			return null;
		}	
		
	}
	
	public boolean del(Hdictionary h){
		h = manager.find(Hdictionary.class, h.getUId());
		setObject(h);
		try{
			delete();
			return true;
		}catch (Exception e) {
			getLog().error("",e);
			return false;
			// TODO: handle exception
		}
	}
	
		
	public void addSpec(Sdictionary s){
		s.setHUId(manager.find(Hdictionary.class, s.getHUId().getUId()));
		setSpecificationLine(s);
		try{
			createSpecificationLine();
		}catch (Exception e) {
			getLog().error("",e);
			// TODO: handle exception
		}	
		
	}
	
	public Sdictionary getSpec(){
		return (Sdictionary)super.getSpecificationLine();
	}
	
	@SuppressWarnings("unchecked")
	public List<Sdictionary> getSpec(Hdictionary h){
		h = manager.find(Hdictionary.class, h.getUId());
		setObject(h);
		return (List<Sdictionary>)getSpecificationLines();
	}
	
	public Sdictionary updateSpec(Sdictionary s){
		Sdictionary spec = manager.find(Sdictionary.class, s.getUId());
		spec.setValue_(s.getValue_());
		spec.setBal(s.getBal());
		try{
			setSpecificationLine(spec);
			updateSpecificationLine();
			return (Sdictionary)getSpecificationLine();
		}catch (Exception e) {
			getLog().error("",e);
			return null;
			// TODO: handle exception
		}
	}
	
	public boolean delSpec(Sdictionary s){
		s = manager.find(Sdictionary.class, s.getUId());
		setSpecificationLine(s);
		try{
			deleteSpecificationLine();
			return true;
		}catch (Exception e) {
			getLog().error("",e);
			return false;
			// TODO: handle exception
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Sdictionary> getSpecByTypeDictionary(Hdictionary h){
		return manager.createNamedQuery("Sdictionary.getSpecByTypeDictionary")
				.setParameter("TD", h.getTypeDictionary())
				.getResultList();
	}
	
	
	
	
}
