package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.Hprofile;
import org.itx.jbalance.l0.o.Oper;
import org.itx.jbalance.l0.s.Sprofile;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@SuppressWarnings("unchecked")
@Stateful(name="Profiles")
@LocalBinding(jndiBinding = "Profiles/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class ProfilesBean extends CommonDocumentBean<Hprofile,Sprofile>  implements Profiles{

	private static final long serialVersionUID = -8430635966944491477L;

	public ProfilesBean() {
		super(new Hprofile(), new Sprofile());
	}

	public Hprofile getObject(){
		return (Hprofile) super.getObject();
	}

	public Sprofile getSpecificationLine() {
		return (Sprofile) super.getSpecificationLine();
	}

	public Hprofile getByOperReceiver(Oper oper){
		List<Hprofile> l= manager.createNamedQuery("Hprofile.ByOperReceiver").setParameter("OperReceiver", oper).getResultList();
		if(l.size()==0) return null;
		return l.get(0);
	}

	
	@SuppressWarnings("unchecked")
	public List<Hprofile> getByOwnersByPeriod() {
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hprofile>) manager.createNamedQuery("Hprofile.ByOwnersByPeriod")
						.setParameter("periodFrom",	sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",sessionprofile.getConfig().getPeriodTo())
						.setParameter("ContractorOwner",sessionprofile.getConfig().getSelectedContractorOwner()).getResultList();
			else
				return (List<Hprofile>) manager.createNamedQuery(
						"Hprofile.ByPeriod").setParameter("periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.getResultList();
		} else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				System.out.println(sdoc);
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
				System.out.println(ddoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hprofile>) manager.createNamedQuery(
						"Hprofile.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hprofile>) manager.createNamedQuery(
						"Hprofile.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}
	
	


}
