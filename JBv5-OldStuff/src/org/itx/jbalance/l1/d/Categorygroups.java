package org.itx.jbalance.l1.d;

import java.util.List;
import javax.ejb.Local;
import org.itx.jbalance.l0.h.Hcategorygroup;
import org.itx.jbalance.l0.o.Group_;
import org.itx.jbalance.l0.s.Scategorygroup;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Categorygroups extends CommonDocument{
	public Hcategorygroup getObject();
	public List<Hcategorygroup> getAll();
	public void add(Hcategorygroup groupcontractor);
	public void update(Hcategorygroup groupcontractor);
	public void del(Hcategorygroup groupcontractor);
	public void addSpec(Scategorygroup s);
	public Scategorygroup getSpecLine();
	public List<Scategorygroup> getSpec(Hcategorygroup s);
	public boolean delSpec(Scategorygroup s);
	public boolean updateSpec(Scategorygroup s);
	public List<Scategorygroup> getAllSpec();
	public List<Hcategorygroup> getHead();
	
	public List<Group_> getSpecByHeads(List<Hcategorygroup> list);
}
