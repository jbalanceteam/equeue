package org.itx.jbalance.l1.d;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hfoodmenu;
import org.itx.jbalance.l0.s.Sfoodmenu;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Foodmenus extends CommonDocument {
	//public List<Hfoodmenu> getNotEmptyPriceByOwners();

	//public List<Hfoodmenu> getByOwnersByPeriod();

	public Hfoodmenu getObject();
	
	public Sfoodmenu getSpecificationLine();

	//public List<Sfoodmenu> getSpec(Hfoodmenu Hfoodmenu, Kindofprice kindofprice, Currency currency);
	
	//public List<Sfoodmenu> getSpec(Hfoodmenu p, Aunit aunit);
}
