package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.h.Hexchange;
import org.itx.jbalance.l0.s.Sexchange;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Exchanges extends CommonDocument<Hexchange,Sexchange> {
	public void insertOpers();

	public void insertActions();

	public void insertActionrights();

	public void insertJuridicals();
	
	public void insertBillfact();
	
	public void insertContractorrights();
	
	public void insertMenus();
	
	public Sexchange getSpecificationLine();
	
	public void setUSpecificationLine(Ubiq sr);
	
	public List<Ubiq> getUSpecificationLines();
	
	public boolean importSpecificationLine(Ubiq ubiq);
	
//	public Hexchange getObject();
	
	public List<Hexchange> getByOwnersByPeriod();

	public void insertPhysicals();

}
