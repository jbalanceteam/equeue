/**
 * 
 */
package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hseason;
import org.itx.jbalance.l0.o.Imago;
import org.itx.jbalance.l0.s.Sseason;
import org.itx.jbalance.l1.common.CommonDocument;

/**
 * @author galina
 *
 */
@Local
public interface Seasons extends CommonDocument {
	
	public Hseason getObject();
	
	public Sseason getSpecificationLine();
	
	public List<Hseason> getHeadByImago(Imago imago);
	
	public List<Sseason> getSpecByHead(Hseason head);
	
	public List<Sseason> getSpecByHeads(List<Hseason> head);
}
