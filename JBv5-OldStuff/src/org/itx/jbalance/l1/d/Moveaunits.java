package org.itx.jbalance.l1.d;

import java.util.List;
import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hmoveaunit;
import org.itx.jbalance.l0.s.Smoveaunit;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Moveaunits extends CommonDocument {
	public void GenirateSpec();
	public List<Hmoveaunit> getRangeHead(int first, int n, Object arg);
	public Hmoveaunit getObject();
	public List<Smoveaunit> getGenirateSpecificationLines();
}
