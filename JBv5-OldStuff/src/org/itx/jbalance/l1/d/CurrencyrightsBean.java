package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.ejb.Stateful;

import org.itx.jbalance.l0.h.Hcurrencyright;
import org.itx.jbalance.l0.o.Currency;
import org.itx.jbalance.l0.s.Scurrencyright;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
@SuppressWarnings("unchecked")
@Stateful
//@LocalBinding(jndiBinding = "Currencyrights/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class CurrencyrightsBean extends CommonDocumentBean implements
		Currencyrights {

	private static final long serialVersionUID = -8430635966944491477L;
	
	public CurrencyrightsBean() {
		super(new Hcurrencyright(), new Scurrencyright());
	}

	public Hcurrencyright getObject() {
		return (Hcurrencyright)super.getObject();
	}

	public Scurrencyright getSpecificationLine() {
		return (Scurrencyright)super.getSpecificationLine();
	}
	
	public List<Currency> getCurrency(List<Hcurrencyright> heads){
		return manager.createNamedQuery("Scurrencyright.getEnabledByHeads").setParameter("HEADS", heads).getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Hcurrencyright> getByOwnersByPeriod() {
		Long ndoc = null;
		java.util.Date ddoc = null;
		String sdoc = getObject().getSearchString();
		if ((sdoc == null) || (sdoc.length() == 0)) {
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hcurrencyright>) manager.createNamedQuery(
						"Hcurrencyright.ByOwnersByPeriod").setParameter(
						"periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.getResultList();
			else
				return (List<Hcurrencyright>) manager.createNamedQuery(
						"Hcurrencyright.ByPeriod").setParameter("periodFrom",
						sessionprofile.getConfig().getPeriodFrom())
						.setParameter("periodTo",
								sessionprofile.getConfig().getPeriodTo())
						.getResultList();
		} else {
			try {
				ndoc = Long.valueOf(sdoc);
			} catch (Exception e) {
			}
			try {
				System.out.println(sdoc);
				ddoc = new SimpleDateFormat("dd.mm.yyyy").parse(sdoc);
				System.out.println(ddoc);
			} catch (Exception e) {
			}
			if (!sessionprofile.getConfig().isAdmin())
				return (List<Hcurrencyright>) manager.createNamedQuery(
						"Hcurrencyright.SearchForOwners")
						.setParameter(
								"ContractorOwner",
								sessionprofile.getConfig()
										.getSelectedContractorOwner())
						.setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
			else
				return (List<Hcurrencyright>) manager.createNamedQuery(
						"Hcurrencyright.Search").setParameter("PrefD", ddoc)
						.setParameter("PrefN", ndoc).setParameter("PrefS",
								"%" + sdoc + "%").getResultList();
		}
	}


}
