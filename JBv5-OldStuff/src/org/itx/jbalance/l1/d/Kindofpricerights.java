package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.Hkindofpriceright;
import org.itx.jbalance.l0.o.Kindofprice;
import org.itx.jbalance.l0.s.Skindofpriceright;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface Kindofpricerights extends CommonDocument{

	
	public Skindofpriceright getSpecificationLine();
	
	public List<Hkindofpriceright> getByOwnersByPeriod();
	
	public List<Kindofprice> getEnabledKindofprice(List<Hkindofpriceright> heads);
	
}
