package org.itx.jbalance.l1.d;

import java.util.List;
import javax.ejb.Stateful;
import javax.persistence.Query;

import org.itx.jbalance.l0.h.Hcategorygroup;
import org.itx.jbalance.l0.h.Hgroupcontractor;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.o.Group_;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.s.Sgroupcontractor;
import org.itx.jbalance.l1.JBException;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;

@Stateful
@LocalBinding(jndiBinding = "Groupcontractors/local")
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class GroupcontractorsBean extends CommonDocumentBean implements Groupcontractors{
	private static final long serialVersionUID = 6299528713915120823L;

	public GroupcontractorsBean(){
		super(new Hgroupcontractor(), new Sgroupcontractor());
	}
	
	public Hgroupcontractor getObject(){
		return (Hgroupcontractor)super.getObject();
	}

	@SuppressWarnings("unchecked")
	public boolean add(boolean f, Hgroupcontractor groupcontractor){
	 try{	
		groupcontractor.setGroup(manager.find(Group_.class, groupcontractor.getGroup().getUId()));
		if(f){
			List<Hgroupcontractor> l = manager.createNamedQuery("Hgroupcontractor.GroupContractorByGroup")
					.setParameter("GROUP", groupcontractor.getGroup())
					.getResultList();
			if(l.size() > 0){
				setObject(l.get(0));
				return false;
			}	
		}
		setObject(groupcontractor);
		create();
		return true;
	 }catch (Exception e) {
		 getLog().error("",e);
		 return false;
	  }	
	}
	
	@SuppressWarnings("unchecked")
	public List<Hgroupcontractor> getHeadByGroup(Hgroupcontractor h){
		List<Hgroupcontractor> l = manager.createNamedQuery("Hgroupcontractor.GroupContractorByGroup")
		.setParameter("GROUP", (Group_)manager.find(Group_.class, h.getGroup().getUId()))
		.getResultList();
		return l;
	} 
	
	@SuppressWarnings("unchecked")
	public List<Sgroupcontractor> getSpecByContractor(Sgroupcontractor s){
		s.setArticleUnit(manager.find(Contractor.class, s.getArticleUnit().getUId()));
		List<Sgroupcontractor> l = manager.createNamedQuery("Sgroupcontractor.getSpecByContractor")
		.setParameter("CONTRACTOR", s.getArticleUnit())
		.getResultList();
		return l;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<Sgroupcontractor> getSpecByContractorGroups(Sgroupcontractor s, List<Group_> listgroup){
		s.setArticleUnit(manager.find(Contractor.class, s.getArticleUnit().getUId()));
		List<Sgroupcontractor> l = manager.createNamedQuery("Sgroupcontractor.getSpecByContractorGroups")
			.setParameter("CONTRACTOR", s.getArticleUnit())
			.setParameter("LISTGROUPS", listgroup)
			.getResultList();
		return l;
		
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<Sgroupcontractor> getSpecByCategory(Sgroupcontractor s, Hcategorygroup c){
		c = manager.find(Hcategorygroup.class, c.getUId());
		s.setArticleUnit(manager.find(Contractor.class, s.getArticleUnit().getUId()));
		List<Sgroupcontractor> l = manager.createNamedQuery("Sgroupcontractor.getSpecByCategory")
		 .setParameter("CONTRACTOR", s.getArticleUnit())
		 .setParameter("CATEGORY", c)
		 .getResultList();
		return l;
	}
	
	
	public boolean updateSpec(Sgroupcontractor s){
		this.add(true, (Hgroupcontractor)s.getHUId());
		s = manager.find(Sgroupcontractor.class, s.getUId());
		s.setHUId(getObject());
		setSpecificationLine(s);
		try{
			updateSpecificationLine();
			return true;
		}
		catch (JBException e){
			getLog().error("",e);
			return false;
		}
	}
	
	
	
	public void addSpec(Sgroupcontractor s){
		Hgroupcontractor h = manager.find(Hgroupcontractor.class, s.getHUId().getUId());
		s.setArticleUnit(manager.find(Contractor.class, s.getArticleUnit().getUId()));
		setObject(h);
		setSpecificationLine(s);
		try{
			createSpecificationLine();
		}
		catch (JBException e){
			getLog().error("",e);
		}
	}
	
	
	public Sgroupcontractor getSpecLine(){
		return (Sgroupcontractor)getSpecificationLine();
	}
	
	@SuppressWarnings("unchecked")
	public List<Sgroupcontractor> getSpec(Hgroupcontractor s){
		setObject(manager.find(Hgroupcontractor.class, s.getUId()));
		return (List<Sgroupcontractor>)getSpecificationLines();
		
	}
	
	public boolean delSpec(Sgroupcontractor s){
		s = manager.find(Sgroupcontractor.class, s.getUId());
		setSpecificationLine(s);
		try	{
			deleteSpecificationLine();
			return true;
		}
		catch (JBException e){
			getLog().error("",e);
			return false;
		}
	}
	
	
	/**
	 * Получаем контрагентов по группе 
	 * если  поле SearchStringзаполнина ищем еще по имени городу и т.д.
	 */
	@SuppressWarnings("unchecked")
	public List<Contractor> getContractors(Hgroupcontractor h, Long first, Long last){
		List<Contractor> l;
		if(h.getSearchString().length() == 0)
			l = (List<Contractor>)manager.createNamedQuery("Sgroupcontractor.Contractors")
				.setParameter("HEAD", manager.find(Hgroupcontractor.class, h.getUId()))
				.setFirstResult(first.intValue())
				.setMaxResults(last.intValue())
				.getResultList();
		else 
			l = (List<Contractor>)manager.createNamedQuery("Sgroupcontractor.SearchContractors")
				.setParameter("HEAD", manager.find(Hgroupcontractor.class, h.getUId()))
				.setParameter("Pref", "%"+h.getSearchString()+"%")
				.setFirstResult(first.intValue())
				.setMaxResults(last.intValue())
				.getResultList();
			
		return l;
	}

	public Long getContractorsCount(Hgroupcontractor h){
		if(h.getSearchString().length() == 0)
			return (Long)manager.createNamedQuery("Sgroupcontractor.ContractorsCount").setParameter("HEAD", manager.find(Hgroupcontractor.class, h.getUId())).getSingleResult();
		else 
			return (Long)manager.createNamedQuery("Sgroupcontractor.SearchContractorsCount").setParameter("HEAD", manager.find(Hgroupcontractor.class, h.getUId())).setParameter("Pref", "%"+h.getSearchString()+"%").getSingleResult();
	}

	
	
	
	
 		
		
	/**
	 * Получаем контрагентов по двум  группам  
	 * если  поле SearchString заполнина ищем еще по имени городу и т.д.
	 * Если в пермом параметре стоит closeDate != null то ищем по фильтрам среди удаленных контрагентов
	 */
	@SuppressWarnings("unchecked")
	public Object getContractors(Hgroupcontractor h, Hgroupcontractor hSecond, Long first, Long last){
	 try{
		 String selectb = " ";
		 if(first == -1)
			 selectb = "select count(a)";
		 else 
			 selectb = "select a ";
		 boolean isSearch = !(h.getSearchString() == null || h.getSearchString().length() == 0);
		 boolean isSubSelect = !(hSecond == null);
		 
		 String query = selectb + " from Sgroupcontractor s join s.articleUnit a  where  a.version is null and a.closeDate is null and s.version is null and s.closeDate is null and s.HUId = :HEAD ";
		 String querycon = " and (a.name like :Pref or a.INN like :Pref or a.barCode like :Pref or a.JAddress like :Pref or a.PAddress like :Pref) ";
		 
		 // Если closeDate == null то ищем не удаленных контрагентов
		 // в противном случае - удаленных
		 String querysub = " select a_s  from Sgroupcontractor ss join ss.articleUnit a_s  where a_s.version is null and  ss.version is null and ss.closeDate is null and ss.HUId = :HEAD_ ";
		 
		 if(h.getCloseDate() == null)
		 {
			 querysub += " and a_s.closeDate is null ";
		 }
		 else
		 {
			 querysub += " and a_s.closeDate is not null ";
		 }

		 String queryconsub = " and (a_s.name like :Pref or a_s.INN like :Pref or a_s.barCode like :Pref or a_s.JAddress like :Pref or a_s.PAddress like :Pref) ";

		 if(isSearch){
			 query += querycon;
			 querysub += queryconsub;
		 }
		 if(isSubSelect)
			 query += " and a in ( "+querysub+" ) "; 
		 
		 Query q = manager.createQuery(query).setParameter("HEAD", manager.find(Hgroupcontractor.class, h.getUId()));
		 if(isSubSelect)
			 q.setParameter("HEAD_", manager.find(Hgroupcontractor.class, hSecond.getUId()));
		 if(isSearch)
			 q.setParameter("Pref", "%"+h.getSearchString()+"%");
		 if(first == -1) return q.getSingleResult();
		 else return q.setFirstResult(first.intValue()).setMaxResults(last.intValue()).getResultList();
		 
	 }catch (Exception e) {
		 getLog().error("",e);
		 return null;
	 }
	}
		
	@SuppressWarnings("unchecked")
	public List<Juridical> getJuridical(Hgroupcontractor h, Long first, Long last){
		List<Juridical> l;
		if(h.getSearchString() == null)
			h.setSearchString("");
		
			if(h.getSearchString().length() == 0)
				l = (List<Juridical>)manager.createNamedQuery("Sgroupcontractor.Contractors")
				.setParameter("HEAD", manager.find(Hgroupcontractor.class, h.getUId()))
				.setFirstResult(first.intValue())
				.setMaxResults(last.intValue())
				.getResultList();
			else 
				l = (List<Juridical>)manager.createNamedQuery("Sgroupcontractor.SearchContractors")
					.setParameter("HEAD", manager.find(Hgroupcontractor.class, h.getUId()))
					.setParameter("Pref", h.getSearchString())
					.setFirstResult(first.intValue())
					.setMaxResults(last.intValue())
					.getResultList();
		return l;
	}

	public Long getJuridicalCount(Hgroupcontractor h){
		if(h.getSearchString() == null)
			h.setSearchString("");
		if(h.getSearchString().length() == 0)
			return (Long)manager.createNamedQuery("Sgroupcontractor.ContractorsCount").setParameter("HEAD", manager.find(Hgroupcontractor.class, h.getUId())).getSingleResult();
		else 
			return (Long)manager.createNamedQuery("Sgroupcontractor.ContractorsCount")
					.setParameter("HEAD", manager.find(Hgroupcontractor.class, h.getUId()))
					.setParameter("Pref", h.getSearchString())
					.getSingleResult();
	}

	@SuppressWarnings("unchecked")
	public boolean checkDuplication(Sgroupcontractor s, Hgroupcontractor h){
	  try{	
		h.setGroup(manager.find(Group_.class, h.getGroup().getUId()));
		List<Hgroupcontractor> l = manager.createNamedQuery("Hgroupcontractor.GroupContractorByGroup")
		 .setParameter("GROUP", h.getGroup())
		 .getResultList();
		System.out.println("==============================="+l.size());
		if(l.size()==0)
			return false;
		
		s.setArticleUnit(manager.find(Contractor.class, s.getArticleUnit().getUId()));
		List<Sgroupcontractor> Sp = manager.createNamedQuery("Sgroupcontractor.getSpecByContractorAndHead")
		 .setParameter("CONTRACTOR", s.getArticleUnit())
		 .setParameter("HEAD", l.get(0))
		 .getResultList();
		System.out.println("==============================="+Sp.size());
		if(Sp.size() == 0)
			return false;
		return true;
	  }catch (Exception e) {
		  getLog().error("",e);
		  return true;
	  }	
	}
	
	public boolean updateSpecBal(Sgroupcontractor h){
		Sgroupcontractor head = manager.find(Sgroupcontractor.class, h.getUId());
		head.setBal(h.getBal());
		head.setDescription(h.getDescription());
		setObject(head);
		try{
			update();
			return true;
		}catch (Exception e) {
			getLog().error("",e);
			return false;
		}
	}
	
	
}
