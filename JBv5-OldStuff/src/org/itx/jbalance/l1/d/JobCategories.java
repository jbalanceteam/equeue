package org.itx.jbalance.l1.d;

import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.HJobCategory;
import org.itx.jbalance.l0.o.Job;
import org.itx.jbalance.l0.s.SJobCategory;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface JobCategories extends CommonDocument
{
	public List<HJobCategory> getJobCategories();
	public HJobCategory getObject();

	public void add(HJobCategory jobCategory);
	public void update(HJobCategory jobCategory);
	public void del(HJobCategory jobCategory);

	public void addSpec(HJobCategory jobCategory, Job job);
	public void delSpec(SJobCategory jobCategory);
	public void updateSpec(SJobCategory jobCategory, Job job);
	public List<SJobCategory> getSpec(HJobCategory jobCategory);
}
