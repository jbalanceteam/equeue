package org.itx.jbalance.l1;

public class JBException extends Exception {
	public JBException(String message) {
		super(message);
	}

	public JBException() {
		super();
	}
}
