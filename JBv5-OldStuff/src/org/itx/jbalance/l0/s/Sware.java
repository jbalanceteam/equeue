package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Currency;

@Entity
@PrimaryKeyJoinColumn(name = "UId")
public class Sware extends Sdocument implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1913872132276081727L;

	private Aunit ArticleUnit;
	
	private Sware Source;
	
	private Float Price;
	
	private Currency currency;


	public Float getPrice() {
		return Price;
	}

	public void setPrice(Float price) {
		Price = price;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Aunit", nullable = true)
	public Aunit getArticleUnit() {
		return ArticleUnit;
	}

	public void setArticleUnit(Aunit articleUnit) {
//	TODO убрать при реализации нормального suggestionbox
		// добавлено потому что, a:support на onselect дает ДВА вызова сеттерра
		// и один из них почему то NULL
		if(articleUnit!=null)
		ArticleUnit = articleUnit;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Sware", nullable = true)
	public Sware getSource() {
		return Source;
	}

	public void setSource(Sware source) {
		Source = source;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Currency", nullable = true)
	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
}
