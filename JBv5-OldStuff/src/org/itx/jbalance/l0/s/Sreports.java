package org.itx.jbalance.l0.s;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.h.Hreports;

@Entity
@NamedQueries({
	@NamedQuery(name="Sreports.Specification",query="from Sdocument o where o.version is null and o.closeDate is null and o.HUId = :Header order by o.seqNumber"),
	@NamedQuery(name="Sreports.MaxSeqNumber",query="select max(o.seqNumber) from Sdocument o where CloseDate is null and Version is null and HUId = :Header")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Sreports extends Sdocument implements Serializable {
	
	private static final long serialVersionUID = -1079135099710366724L;

}