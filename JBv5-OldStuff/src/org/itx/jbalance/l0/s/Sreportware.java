package org.itx.jbalance.l0.s;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.o.Packet;

@Entity
@NamedQueries({
	@NamedQuery(name="Sreportware.Specification",query="from Sdocument o where o.version is null and o.closeDate is null and o.HUId = :Header order by o.seqNumber"),
	@NamedQuery(name="Sreportware.MaxSeqNumber",query="select max(o.seqNumber) from Sdocument o where CloseDate is null and Version is null and HUId = :Header")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Sreportware extends Sreports implements Serializable {
	
	private static final long serialVersionUID = -1079135099710366724L;
	
	private Aunit ArticleUnit;

	private Measurement measurement;
	
	private Packet packet;

	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Packet", nullable = true)
	public Packet getPacket() {
		return packet;
	}

	public void setPacket(Packet packet) {
		this.packet = packet;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Measurement", nullable = true)
	public Measurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Aunit", nullable = true)
	public Aunit getArticleUnit() {
		return ArticleUnit;
	}

	public void setArticleUnit(Aunit articleUnit) {
		ArticleUnit = articleUnit;
	}

}