package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Job;

@Entity
@PrimaryKeyJoinColumn(name = "UId")
public class SJobCategory extends Sdocument implements Serializable
{
	private static final long serialVersionUID = -6509683322025071470L;

	private Job job;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Job", nullable = true)
	public Job getJob()
	{
		return job;
	}
	public void setJob(Job job)
	{
		this.job = job;
	}

	public String toString()
	{
		if (job != null)
			return job.toString();
		else
			return null;
	}
}
