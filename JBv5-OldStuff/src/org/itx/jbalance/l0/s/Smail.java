package org.itx.jbalance.l0.s;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
@PrimaryKeyJoinColumn(name = "UId")
public class Smail extends Scontractor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8298590750403235320L;
	
	private Timestamp SendTime;
	
	private Boolean Sended;

	public Boolean getSended() {
		return Sended;
	}

	public void setSended(Boolean sended) {
		Sended = sended;
	}

	public Timestamp getSendTime() {
		return SendTime;
	}

	public void setSendTime(Timestamp sendTime) {
		SendTime = sendTime;
	}
	
}
