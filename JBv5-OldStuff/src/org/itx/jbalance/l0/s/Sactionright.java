package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Action;


@Entity
@NamedQueries({
	@NamedQuery(name="Sactionright.getEnabledByHeads",query="select o.enabledAction from Sactionright o where o.HUId in (:HEADS) and o.version is null and o.closeDate is null order by o.openDate")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Sactionright extends Ssystem implements Serializable {

	private static final long serialVersionUID = -7889036222500942894L;
	
	private Action EnabledAction;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Action", nullable = true)
	public Action getEnabledAction() {
		return EnabledAction;
	}
	public void setEnabledAction(Action enabledAction) {
		EnabledAction = enabledAction;
	}
	

}
