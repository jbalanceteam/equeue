package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
@PrimaryKeyJoinColumn(name = "UId")
public class Srequest extends Sware implements Serializable {

	private static final long serialVersionUID = -1228432832819705481L;
	
	private Float Quantity;
	
	private Float Summ;

	public Float getQuantity() {
		return Quantity;
	}

	public void setQuantity(Float quantity) {
		Quantity = quantity;
	}
	

	public Float getSumm() {
		return Summ;
	}

	public void setSumm(Float summ) {
		Summ = summ;
	}

}
