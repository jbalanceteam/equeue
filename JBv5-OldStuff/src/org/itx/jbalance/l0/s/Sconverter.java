package org.itx.jbalance.l0.s;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@NamedQueries({
	@NamedQuery(name="Sconverter.getDocumentBySware",query=" " +
			" select " +
				" doc " +
			" from " +
				" Sconverter con_ join con_.sdocument doc, Hconverter hcon_" +
		   " where " +
		   		" con_.HUId = hcon_.UId " +
		   		" and hcon_.ware = :WARE " +
		   		" and con_.version is null " +
		   		" and con_.closeDate is null")
//	@NamedQuery(name="Sreport.ReportCountByContractor",query="select count(o) from Sreport o join o.stask s where o.version is null and o.closeDate is null and s.HUId = :header and s.articleUnit = :contractor")
					
})
@PrimaryKeyJoinColumn(name = "UId")
public class Sconverter extends Sdocument implements Serializable{
	private static final long serialVersionUID = 5123224054993362783L;
	
	private Sdocument sdocument;
		
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "sdocument", nullable = false)
	public Sdocument getSdocument() {
		return sdocument;
	}
	public void setSdocument(Sdocument sdocument) {
		this.sdocument = sdocument;
	}
	

}
