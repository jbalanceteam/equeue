package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Contractor;


@Entity
@NamedQueries({
	@NamedQuery(name="Scontractorright.getContractorByHeads",query="select o.enabledContractor from Scontractorright o where o.HUId in (:HEADS) and o.version is null and o.closeDate is null")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Scontractorright extends Ssystem implements Serializable {

	private static final long serialVersionUID = -7889036222500942894L;
	private Contractor EnabledContractor;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Contractor", nullable = true)
	public Contractor getEnabledContractor() {
		return EnabledContractor;
	}
	public void setEnabledContractor(Contractor enabledContractor) {
		EnabledContractor = enabledContractor;
	}
	

}
