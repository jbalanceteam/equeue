/**
 * 
 */
package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.h.Hunit;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.o.Packet;

/**
 * @author galina
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Sunit.getSpecByHeads", query="select o from Sunit o where o.version is null and o.closeDate is null and o.HUId in (:HEADS)"),
	@NamedQuery(name="Sunit.All", query="select o from Sunit o where o.version is null and o.closeDate is null "),
	@NamedQuery(name="Sunit.AllCount", query="select count(o) from Sunit o where o.version is null and o.closeDate is null ")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Sunit extends Sdocument implements Serializable {

	private static final long serialVersionUID = -8324401605148437655L;
	
	private Packet packet;
	
	private Float coef;
	
	private Measurement measurement;
	
	private String articul;
	
	private String code;
	
	private String format;
	
	
	
	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_packet", nullable = true)
	public Packet getPacket() {
		return packet;
	}

	public void setPacket(Packet packet) {
		this.packet = packet;
	}

	public String getArticul() {
		return articul;
	}

	public void setArticul(String articul) {
		this.articul = articul;
	}

	public Float getCoef() {
		return coef;
	}

	public void setCoef(Float coef) {
		this.coef = coef;
	}

	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_measurement", nullable = true)
	public Measurement getMeasurement() {
		return measurement;
	}
	
	public String toString(){
		return ((Hunit)getHUId()).getAunit() != null && getPacket() != null ? ((Hunit)getHUId()).getAunit().getName() + " " + getPacket().getName() : "";  
	}
}
