package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Job;
import org.itx.jbalance.l0.o.Physical;

@Entity
@NamedQueries({
	@NamedQuery(name="Contactpersons.Specification",
			query="from " +
					"Scontactpersons o " +
				  "where " +
				  	"o.version is null and " +
				  	"o.closeDate is null and " +
				  	"o.HUId = :Header"),
				  	
     @NamedQuery(name="Contactpersons.SearchContactpersons",
	  		 query="from " +
			 		 "Scontactpersons o " +
			 		 "join o.physical p " +
					"where " +
					  	"o.version is null and " +
					  	"o.closeDate is null and " +
					  	"o.HUId = :Header and " +
					  	"p.realName = :realName and " +
					  	"p.surname = :surname and " +
					  	"p.surname = :patronymic"),
					  	
		@NamedQuery(name="Contactpersons.DecisionMakers",
			query="select o from " +
					"Scontactpersons o join o.HUId head join head.juridicalName juridical " +
				  "where " +
				  	"o.version is null and " +
				  	"o.closeDate is null and " +
				  	"juridical = :Header and " +
				  	"o.canMakeDecision = true"),
		@NamedQuery(name="Contactpersons.getContatcsByJuridicals",
				query="select o from " +
					"Scontactpersons o join o.HUId head join head.juridicalName juridical " +
				"where " +
			  		"o.version is null " +
			  		"and o.closeDate is null " +
			  		"and juridical in (:LIST) ")

				  	
})
@PrimaryKeyJoinColumn(name = "UId")
public class Scontactpersons extends Scontractor implements Serializable {
	
	private static final long serialVersionUID = -1913872132276081727L;
	
	private Physical physical;
	
	private Job job;
	
	private boolean canMakeDecision;
	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Physical", nullable = true)
	public Physical getPhysical() {
		return physical;
		
	}

	public void setPhysical(Physical physical) {
		this.physical = physical;
	}

	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "job", nullable = true)
	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	@Column(nullable = false)
	public boolean getCanMakeDecision() {
		return canMakeDecision;
	}

	public void setCanMakeDecision(boolean canMakeDecision) {
		this.canMakeDecision = canMakeDecision;
	}
}
