package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Action;

@Entity
@PrimaryKeyJoinColumn(name = "UId")
public class Smenu extends Ssystem implements Serializable {

	private static final long serialVersionUID = -7889036222500942894L;
	private Action MenuAction;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Action", nullable = true)
	public Action getMenuAction() {
		return MenuAction;
	}
	public void setMenuAction(Action menuAction) {
		MenuAction = menuAction;
	}
	

}
