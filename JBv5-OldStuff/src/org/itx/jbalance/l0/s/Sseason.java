/**
 * 
 */
package org.itx.jbalance.l0.s;

import java.io.Serializable;
import java.sql.Date;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import javax.persistence.Transient;
/**
 * @author galina
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Sseason.getSpecByHead", query="select o from Sseason o where o.version is null and o.closeDate is null and o.HUId = :HEAD"),
	@NamedQuery(name="Sseason.getSpecByHeads", query="select o from Sseason o where o.version is null and o.closeDate is null and o.HUId in (:HEADS)")
				  	   
})
@PrimaryKeyJoinColumn(name = "UId")
public class Sseason extends Sdocument implements Serializable {

	private static final long serialVersionUID = 2323600351192353325L;

	private Date datefrom;
	
	private Date dateto; 
	
	private Float percent;
	
	private String from;
	
	private String to;

	public Date getDatefrom() {
		return datefrom;
	}
	
//	@Transient
//	public String getFrom() {
//		DateFormat df=new SimpleDateFormat("dd-MMM");
//		return df.format(datefrom);
//	}
//	
//	@Transient
//	public String getTo() {
//		DateFormat df=new SimpleDateFormat("dd-MMM");
//		return df.format(dateto);
//	}


	public void setDatefrom(Date datefrom) {
		this.datefrom = datefrom;
	}

	public Date getDateto() {
		return dateto;
	}

	public void setDateto(Date dateto) {
		this.dateto = dateto;
	}

	public Float getPercent() {
		return percent;
	}

	public void setPercent(Float percent) {
		this.percent = percent;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public void setTo(String to) {
		this.to = to;
	}
	
	
}
