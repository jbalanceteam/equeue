package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Currency;


@Entity
@NamedQueries({
	@NamedQuery(name="Scurrencyright.getEnabledByHeads",query="select o.enabledCurrency from Scurrencyright o where o.HUId in (:HEADS) and o.version is null and o.closeDate is null order by o.openDate")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Scurrencyright extends Ssystem implements Serializable {

	private static final long serialVersionUID = -7889036222500942894L;
	private Currency EnabledCurrency;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Currency", nullable = true)
	public Currency getEnabledCurrency() {
		return EnabledCurrency;
	}
	public void setEnabledCurrency(Currency enabledCurrency) {
		EnabledCurrency = enabledCurrency;
	}
	

}
