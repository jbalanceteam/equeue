package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import org.itx.jbalance.l0.o.Currency;
import org.itx.jbalance.l0.o.Kindofprice;


@Entity
@NamedQueries({
	@NamedQuery(name="Price.Specification",query="from Sprice o where o.version is null and o.closeDate is null and o.HUId = :Header and o.kindOfPrice = :KOP and o.currency = :IC order by o.seqNumber"),
	@NamedQuery(name="Price.SpecificationAll",query="from Sprice o where o.version is null and o.closeDate is null and o.HUId = :Header and o.articleUnit = :AUNIT order by o.seqNumber")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Sprice extends Sware implements Serializable {

	private static final long serialVersionUID = -6870624185618127309L;
	private Kindofprice KindOfPrice;
	private Kindofprice searchKindOfPrice;
	private Currency searchCurrency;
	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Kindofprice", nullable = true)
	public Kindofprice getKindOfPrice() {
		return KindOfPrice;
	}

	public void setKindOfPrice(Kindofprice kindOfPrice) {
		KindOfPrice = kindOfPrice;
	}

	@Transient
	public Currency getSearchCurrency() {
		return searchCurrency;
	}

	public void setSearchCurrency(Currency searchisoCurrency) {
		this.searchCurrency = searchisoCurrency;
	}

	@Transient
	public Kindofprice getSearchKindOfPrice() {
		return searchKindOfPrice;
	}

	public void setSearchKindOfPrice(Kindofprice searchKindOfPrice) {
		this.searchKindOfPrice = searchKindOfPrice;
	}


}
