/**
 * 
 */
package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Ware;

/**
 * @author galina
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Simago.getSpecByHeads", query="select o from Simago o where o.version is null and o.closeDate is null and o.HUId in (:HEADS)"),
	@NamedQuery(name="Simago.getSpecByHeadsCount", query="select count(*) from Simago o where o.version is null and o.closeDate is null and o.HUId in (:HEADS)")
				  	   
})
@PrimaryKeyJoinColumn(name = "UId")
public class Simago extends Sdocument implements Serializable {

	private static final long serialVersionUID = -8324401605148437655L;
	
	private Ware ware;
	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_ware", nullable = true)
	public Ware getWare() {
		return ware;
	}

	public void setWare(Ware ware_) {
		this.ware = ware_;
	}
}
