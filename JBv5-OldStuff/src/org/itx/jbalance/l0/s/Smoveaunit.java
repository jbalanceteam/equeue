package org.itx.jbalance.l0.s;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.o.Packet;


@Entity
@NamedQueries({
	@NamedQuery(name="Smoveaunit.Specification",query="from Smoveaunit o where o.version is null and o.closeDate is null and o.HUId = :Header order by o.seqNumber"),
	@NamedQuery(name="Smoveaunit.MaxSeqNumber",query="select max(o.seqNumber) from Smoveaunit o where CloseDate is null and Version is null and HUId = :Header"),
	
	@NamedQuery(name="Smoveaunit.getMoveAunitByPeriod", query= "" +
	"	select " +
			"new org.itx.jbalance.l0.s.Smoveaunit( " +
			" o.articleUnit.UId as uid, " +
			" sum(o.rest) as rest, " +
			" o.packet.UId as uid_packet," +
			" o.measurement.UId as uid_measurement) " +
	  " from " +
			" Swhsdocument o, Hwhsdocument h " +
	"   where " +
			" o.version is null " +
			" and o.closeDate is null " +
			" and o.rest > 0  " +
			" and h = o.HUId " +
			" and h.whsFrom = :WHSOWNER " +							
			" and h.openDate <= :OPEN_DATE " +
			" and h.contractorReceiver =:OWNER " +
		"group by " +
				" o.articleUnit.UId, " +
				" o.packet.UId," +
				" o.measurement.UId "),
				
		@NamedQuery(name="Smoveaunit.getMoveAunitByPeriodNotIncludeDate", query= "" +
			"	select " +
					"new org.itx.jbalance.l0.s.Smoveaunit( " +
							" o.articleUnit.UId as uid, " +
								" sum(o.rest) as rest, " +
								" o.packet.UId as uid_packet," +
								" o.measurement.UId as uid_measurement) " +
						  " from " +
								" Swhsdocument o, Hwhsdocument h " +
						"   where " +
								" o.version is null " +
								" and o.closeDate is null " +
								" and o.rest > 0  " +
								" and h = o.HUId " +
								" and h.whsFrom = :WHSOWNER " +							
								" and h.openDate < :OPEN_DATE " +
								" and h.contractorReceiver =:OWNER " +
							"group by " +
									" o.articleUnit.UId, " +
									" o.packet.UId," +
									" o.measurement.UId "),


	@NamedQuery(name="Smoveaunit.getRestIncomeByConsumption", query= "" +
	"	select " +
			"new org.itx.jbalance.l0.s.Smoveaunit( " +
				" o.articleUnit.UId as uid, " +
				" sum(o.quantity) as rest, " +
				" o.packet.UId as uid_packet," +
				" o.measurement.UId as uid_measurement) " +
	  " from " +
	  		" Swhsdocument o, Hwhsdocument h " +
	"   where " +
			" o.version is null " +
			" and o.closeDate is null " +
			" and o.inOutLink.openDate <= :OPEN_DATE " +			
			" and o.rest > 0  " +
			" and h = o.HUId " +
			" and h.whsTo = :WHSOWNER " +							
			" and o.openDate > :OPEN_DATE " +
			" and h.contractorOwner =:OWNER " +
		"group by " +
			" o.articleUnit.UId, " +
			" o.packet.UId," +
			" o.measurement.UId "),

				
	@NamedQuery(name="Smoveaunit.getIncomeByPeriod", query= "" +
	"	select " +
			"new org.itx.jbalance.l0.s.Smoveaunit( " +
				" o.articleUnit.UId as uid, " +
				" sum(o.quantity) as rest, " +
				" o.packet.UId as uid_packet," +
				" o.measurement.UId as uid_measurement) " +
		" from " +
			" Swhsdocument o, Hwhsdocument h " +
	  "   where " +
			" o.version is null " +
			" and o.closeDate is null " +
			" and o.rest > 0  " +
			" and h = o.HUId " +
			" and h.whsFrom = :WHSOWNER " +							
			" and h.openDate between (:BEGIN_PER) and (:END_PER) " +
		 "group by " +
			" o.articleUnit.UId, " +
			" o.packet.UId," +
			" o.measurement.UId "),
			
	@NamedQuery(name="Smoveaunit.getConsumptionByPeriod", query= "" +
	   "  select " +
			"new org.itx.jbalance.l0.s.Smoveaunit( " +
				" o.articleUnit.UId as uid, " +
				" sum(o.quantity) as rest, " +
				" o.packet.UId as uid_packet," +
				" o.measurement.UId as uid_measurement) " +
	     " from " +
		    	" Swhsdocument o, Hwhsdocument h " +
         " where " +
				" o.version is null " +
				" and o.closeDate is null " +
				" and o.rest > 0  " +
				" and h = o.HUId " +
				" and h.whsTo = :WHSOWNER " +							
				" and h.openDate between (:BEGIN_PER) and (:END_PER) " +
		"group by " +
				" o.articleUnit.UId, " +
				" o.packet.UId," +
				" o.measurement.UId ")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Smoveaunit extends Sreportware implements Serializable {

	private static final long serialVersionUID = 2323600351192353325L;

	private Double startrest;
	
	private Double endrest;
	
	private Double incomerest;
	
	private Double consumptionrest;
	
	public Smoveaunit(){}
 	
	public Smoveaunit(Long uid_aunit, Double start_rest, Long uid_packet, Long uid_measurement){
		Aunit a = new Aunit();
		a.setUId(uid_aunit);
		setArticleUnit(a);
		
		Packet packet_ = new Packet();
		packet_.setUId(uid_packet);
		setPacket(packet_);
		
		Measurement measurement_ = new Measurement();
		measurement_.setUId(uid_measurement);
		setMeasurement(measurement_);
	
		setStartrest(start_rest);
	}

	public Double getEndrest() {
		return endrest;
	}

	public void setEndrest(Double endrest) {
		this.endrest = endrest;
	}

	
	public Double getStartrest() {
		return startrest;
	}

	public void setStartrest(Double startrest) {
		this.startrest = startrest;
	}
	
	public Double getIncomerest() {
		return incomerest;
	}

	public void setIncomerest(Double incomerest) {
		this.incomerest = incomerest;
	}
	
	public Double getConsumptionrest() {
		return consumptionrest;
	}

	public void setConsumptionrest(Double consumptionrest) {
		this.consumptionrest = consumptionrest;
	}

	
	public boolean equals(Object arg){
		return ((Smoveaunit)arg).getArticleUnit().getUId().equals(getArticleUnit().getUId()) 
			& ((Smoveaunit)arg).getMeasurement().getUId().equals(getMeasurement().getUId())
			& ((Smoveaunit)arg).getPacket().getUId().equals(getPacket().getUId()); 
	}
	
	public String toString(){
		return getArticleUnit().getName() + "  " 
				+ getPacket().getName() + " (" + getMeasurement().getName() + ")";
	}

}
