package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import org.itx.jbalance.l0.o.Currency;
import org.itx.jbalance.l0.o.Group_;
import org.itx.jbalance.l0.o.Kindofprice;


@Entity
@NamedQueries({
	@NamedQuery(name="Sfoodmenu.Specification",query="from Sfoodmenu o where o.version is null and o.closeDate is null and o.HUId = :Header and o.quantity = :QUANTITY and o.type = :TYPE order by o.seqNumber"),
	@NamedQuery(name="Sfoodmenu.SpecificationAll",query="from Sfoodmenu o where o.version is null and o.closeDate is null and o.HUId = :Header and o.articleUnit = :AUNIT order by o.seqNumber")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Sfoodmenu extends Sware implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2547818602357049087L;

	private Integer quantity;
	
	private Group_ type;
	
	private Float Summ;

	private Double output;

	public Double getOutput() {
		return output;
	}

	public void setOutput(Double output) {
		this.output = output;
	}

	@Transient
	public Float getSumm() {
		return quantity*getPrice();
	}

	public void setSumm(Float sum) {
		this.Summ = sum;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Group_", nullable = true)
	public Group_ getType() {
		return type;
	}

	public void setType(Group_ type) {
		this.type = type;
	}
	
}
