package org.itx.jbalance.l0.s;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.o.Packet;

@NamedQueries({
	@NamedQuery(name="Swhsdocument.getSpecByOwnerAunit", query="	" +
				"	select " +
						" o " +
				"   from " +
					" Swhsdocument o, Hwhsdocument h " +
				" where " +
					" o.version is null " +
					" and o.closeDate is null " +
					" and o.rest > 0" +
					" and o.measurement = :MEASURMENT " +
					" and o.packet = :PACKET " +
					" and h = o.HUId " +
					" and h.openDate <= :OPEN_DATE " +
					" and h.whsFrom = :WHSOWNER " +					
					" and h.contractorReceiver = :OWNER " +
					" and o.articleUnit = :AUNIT"),

	@NamedQuery(name="Swhsdocument.getRestByOwner", query="	" +
					"	select " +
							"new org.itx.jbalance.l0.Rest( " +
							" o.articleUnit.UId as uid, " +
							" o.articleUnit.name as name, " +
							" sum(o.rest) as rest, " +
							" o.packet.UId as uid_packet," +
							" o.measurement.UId as name_measurement, " +
							" o.packet.name as uid_packet," +
							" o.measurement.name as name_measurement) " +
							
					"   from " +
							" Swhsdocument o, Hwhsdocument h " +
					  " where " +
							" o.version is null " +
							" and o.closeDate is null " +
							" and o.rest > 0  " +
							" and h = o.HUId " +
							" and h.whsFrom = :WHSOWNER " +							
							" and h.openDate <= :OPEN_DATE " +
							" and h.contractorReceiver = (:OWNER) " +
						"group by " +
							" 1,2,4,5,6,7 " +
						" "),
@NamedQuery(name="Swhsdocument.getRestByWhs", query="	" +
					"	select " +
							"new org.itx.jbalance.l0.Rest( " +
							" o.articleUnit.UId as uid, " +
							" o.articleUnit.name as name, " +
							" sum(o.rest) as rest, " +
							" o.packet.UId as uid_packet," +
							" o.measurement.UId as name_measurement, " +
							" o.packet.name as uid_packet," +
							" o.measurement.name as name_measurement) " +
										
					"   from " +
							" Swhsdocument o, Hwhsdocument h " +
					" where " +
							" o.version is null " +
							" and o.closeDate is null " +
							" and o.rest > 0  " +
							" and h = o.HUId " +
							" and h.whsFrom = :WHSOWNER " +							
							" and h.openDate <= :OPEN_DATE " +
							
						"group by " +
								" 1,2,4,5,6,7 " +
					" "),												
/**
 * Запрос возвращает сумма количества по накладным, 
 * которые имеют указатель (InOutLink) на накладные в период < InOutLink.OPENDATE и > InOutLink.OPENDATE									
 */
	@NamedQuery(name="Swhsdocument.getRestByWhsIsNotInOutLink", query="	" +
					"	select " +
							"new org.itx.jbalance.l0.Rest( " +
							" o.articleUnit.UId as uid, " +
							" o.articleUnit.name as name, " +
							" sum(o.quantity) as rest, " +
							" o.packet.UId as uid_packet," +
							" o.measurement.UId as name_measurement, " +
							" o.packet.name as uid_packet," +
							" o.measurement.name as name_measurement) " +
										
					"   from " +
							" Swhsdocument o, Hwhsdocument h " +
					" where " +
							" o.version is null " +
							" and o.closeDate is null " +
							" and o.rest > 0 " +
							" and o.inOutLink.openDate <= :OPEN_DATE " +
							" and h = o.HUId " +
							" and h.whsTo = :WHSOWNER " +							
							" and o.openDate > :OPEN_DATE " +
							" and h.contractorOwner = (:OWNER) " +
						"group by " +
								" o.articleUnit.UId," +
								" o.articleUnit.name," +
								" o.packet.UId," +
								" o.measurement.UId," +
								" o.packet.name, " +
								" o.measurement.name " +
					" "),						
	@NamedQuery(name="Swhsdocument.getRestByInOutLink", query=" " +
			" select " +
				" o " +
			" from " +
				" Swhsdocument o " +
			" where " +
				" o.version is null " +
				" and o.closeDate is null " +
				" and o.inOutLink = (:SPEC)"),
				
	@NamedQuery(name="Swhsdocument.getRestsByInOutLink", query=" " +
						" select " +
							" o " +
						" from " +
							" Swhsdocument o " +
						" where " +
							" o.version is null " +
							" and o.closeDate is null " +
							" and o.inOutLink in (:SPEC)")
})
@Entity
@PrimaryKeyJoinColumn(name = "UId")
public class Swhsdocument extends Sware implements Serializable {

	private static final long serialVersionUID = 8608528910353856924L;

	private Float Quantity;

	private Float Summ;

	private Float Rest;

	private Swhsdocument InOutLink;
	
	private Measurement measurement;
	
	private Packet packet;

	public Float getQuantity() {
		return Quantity;
	}

	public void setQuantity(Float quantity) {
		Quantity = quantity;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "InOutLink", nullable = true)
	public Swhsdocument getInOutLink() {
		return InOutLink;
	}

	public void setInOutLink(Swhsdocument inOutLink) {
		InOutLink = inOutLink;
	}

	public Float getRest() {
		return Rest;
	}

	public void setRest(Float rest) {
		Rest = rest;
	}

	public Float getSumm() {
		return Summ;
	}

	public void setSumm(Float summ) {
		Summ = summ;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "uidmeasurement", nullable = true)
	public Measurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "uidpacket", nullable = true)
	public Packet getPacket() {
		return packet;
	}

	public void setPacket(Packet packet) {
		this.packet = packet;
	}
	
	public String toString(){
		return getArticleUnit().getName() + "  " + getPacket().getName();
	}
	

}
