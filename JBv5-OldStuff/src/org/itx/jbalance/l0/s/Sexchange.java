package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.Ubiq;



@Entity
@NamedQueries({
	@NamedQuery(name="Sexchange.USpecification",query="select o.exchangedUbiq from Sexchange as o where o.version is null and o.closeDate is null and o.HUId = :Header order by o.seqNumber"),
	@NamedQuery(name="Sexchange.USpecificationByUbiq",query="select distinct o from Sexchange as o where o.version is null and o.closeDate is null and o.HUId = :Header and o.exchangedUbiq = :Spec  order by o.seqNumber")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Sexchange extends Ssystem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7889036222500942894L;
	private Ubiq ExchangedUbiq;
	
//	private Ubiq ExchangedUbiq_;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "ExchangedUbiq", nullable = true)
	public Ubiq getExchangedUbiq() {
		return ExchangedUbiq;
	}
	
	public void setExchangedUbiq(Ubiq ubiq) {
		ExchangedUbiq = ubiq;
	}


}
