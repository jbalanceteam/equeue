package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Contractor;

@Entity
@PrimaryKeyJoinColumn(name = "UId")
public class Scontractor extends Sdocument implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1913872132276081727L;

	private Contractor contractor;
	
	private Scontractor Source;
	

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Contractor", nullable = true)
	public Contractor getArticleUnit() {
		return contractor;
	}

	public void setArticleUnit(Contractor contractor) {
		this.contractor = contractor;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Scontractor", nullable = true)
	public Scontractor getSource() {
		return Source;
	}

	public void setSource(Scontractor source) {
		Source = source;
	}
}
