package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Warehouse;

@Entity
@NamedQueries({
	@NamedQuery(name="Scontractorwarehouse.AllByOwner",query="from Scontractorwarehouse o where o.version is null and o.closeDate is null and o.HUId.contractorOwner in (:OWNER) order by o.openDate")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Scontractorwarehouse extends Ssystem implements Serializable {

	private static final long serialVersionUID = 4650596257786477928L;
	
	private Warehouse Whs;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Warehouse", nullable = true)
	public Warehouse getWhs() {
		return Whs;
	}

	public void setWhs(Warehouse whs) {
		Whs = whs;
	}

}
