package org.itx.jbalance.l0.s;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@NamedQueries({
	@NamedQuery(name="Sgroupcontractor.getSpecByContractor",query="select s from Sgroupcontractor s where s.version is null and s.articleUnit =:CONTRACTOR and s.closeDate is null "),
	@NamedQuery(name="Sgroupcontractor.getSpecByContractorGroups",query="select s from Sgroupcontractor s join s.HUId h where s.version is null and s.articleUnit =:CONTRACTOR and h.group in (:LISTGROUPS) and s.closeDate is null "),
	
	@NamedQuery(name="Sgroupcontractor.getSpecByContractorAndHead",query="select s from Sgroupcontractor s where s.version is null and s.articleUnit =:CONTRACTOR and s.HUId = :HEAD and s.closeDate is null "),
	@NamedQuery(name="Sgroupcontractor.getSpecByCategory",
			query=" select " +
						" s " +
				   " from " +
				   		" Sgroupcontractor s join s.HUId h join h.group g, " +
				   		" Scategorygroup cg " +
				   	" where " +
				   		" h.version is null and " +
				   		" h.closeDate is null and " +
				   		" s.version is null and " +
				   		" s.closeDate is null and " +
				   		" s.articleUnit =:CONTRACTOR and " +
				   		" g.UId = cg.group and " +
				   		" cg.HUId =:CATEGORY"),
				   		@NamedQuery(name="Sgroupcontractor.Contractors",query=" " +
				   				" select " +
				   					" a " +
				   				" from " +
				   					" Sgroupcontractor s join s.articleUnit a " +
				   				" where " +
				   					" s.version is null " +
				   					" and s.closeDate is null " +
				   					" and s.HUId = :HEAD  " +
				   					" order by a.name"),
				   		@NamedQuery(name="Sgroupcontractor.ContractorsCount",query=" " +
				   				" select " +
				   					" count(a) " +
				   				" from " +
				   					" Sgroupcontractor s join s.articleUnit a " +
				   				" where " +
				   					" s.version is null " +
				   					" and s.closeDate is null " +
				   					" and s.HUId = :HEAD"),

				   					
		   	  @NamedQuery(name="Sgroupcontractor.SearchContractors",query=" " +
							   				" select " +
							   					" a " +
							   				" from " +
							   					" Sgroupcontractor s join s.articleUnit a " +
							   				" where " +
							   					" s.version is null " +
							   					" and s.closeDate is null " +
							   					" and s.HUId = :HEAD and " +
							   					" (a.name like :Pref or a.INN like :Pref or a.barCode like :Pref or a.JAddress like :Pref or a.PAddress like :Pref) " +
							   					" order by a.name"),
							   					
				
							   					
	   		   @NamedQuery(name="Sgroupcontractor.SearchContractorsCount",query=" " +
							   				" select " +
							   					" count(a) " +
							   				" from " +
							   					" Sgroupcontractor s join s.articleUnit a " +
							   				" where " +
							   					" s.version is null " +
							   					" and s.closeDate is null and " +
							   					" (a.name like :Pref or a.INN like :Pref or a.barCode like :Pref or a.JAddress like :Pref or a.PAddress like :Pref) " +
							   					" and s.HUId = :HEAD"),
								   					
		
//	   			@NamedQuery(name="Sgroupcontractor.ContractorsSecond",query=" " +
//							   				" select " +
//							   					" a " +
//							   				" from " +
//							   					" Sgroupcontractor s join s.articleUnit a " +
//							   				" where " +
//							   					" s.version is null " +
//							   					" and s.closeDate is null " +
//							   					" and s.HUId = :HEAD " +
//							   					" and a in (:CA) " +
//							   					" order by a.name")				
				   					
							  		   			@NamedQuery(name="Sgroupcontractor.ContractorsSecond",query=" " +
										   				" select " +
											   					" a " +
										   				" from " +
										   					" Sgroupcontractor s join s.articleUnit a " +
										   				" where " +
										   					" s.version is null " +
										   					" and s.closeDate is null " +
										   					" and s.HUId = :HEAD " +
										   					" and a in (select a_s from Sgroupcontractor ss join ss.articleUnit a_s where ss.version is null and ss.closeDate is null and ss.HUId = :HEAD_) " +
										   					" order by a.name")							   					
						   					
		   							
					
										   					
													   		
})
@PrimaryKeyJoinColumn(name = "UId")
public class Sgroupcontractor extends Scontractor implements Serializable{
	private static final long serialVersionUID = -6509683322025071470L;
	
	private String description;
	
	private Byte bal;

	public Byte getBal() {
		return bal;
	}

	public void setBal(Byte bal) {
		this.bal = bal;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
