package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Kindofprice;


@Entity
@NamedQueries({
	@NamedQuery(name="Skindofpriceright.getEnabledByHeads",query="select o.enabledKindofprice from Skindofpriceright o where o.HUId in (:HEADS) and o.version is null and o.closeDate is null order by o.openDate")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Skindofpriceright extends Ssystem implements Serializable {

	private static final long serialVersionUID = -7889036222500942894L;
	private Kindofprice EnabledKindofprice;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Kindofprice", nullable = true)
	public Kindofprice getEnabledKindofprice() {
		return EnabledKindofprice;
	}
	public void setEnabledKindofprice(Kindofprice enabledKindofprice) {
		EnabledKindofprice = enabledKindofprice;
	}
	

}
