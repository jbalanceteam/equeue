package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.h.Hsystem;


@Entity
@PrimaryKeyJoinColumn(name = "UId")
public class Sprofile extends Ssystem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7889036222500942894L;
	private Hsystem Enabler;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "HsystemEnabler", nullable = true)
	public Hsystem getEnabler() {
		return Enabler;
	}
	public void setEnabler(Hsystem enabler) {
		Enabler = enabler;
	}
	

}
