/**
 * 
 */
package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.h.Hunit;
import org.itx.jbalance.l0.o.Imago;
import org.itx.jbalance.l0.o.Measurement;
import org.itx.jbalance.l0.o.Packet;

/**
 * @author galina
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Stechnologymap.getSpecByHeads", query="select o from Stechnologymap o where o.version is null and o.closeDate is null and o.HUId in (:HEADS)"),
	@NamedQuery(name="Stechnologymap.getBasicImagoByHead", query="select o from Stechnologymap o where o.version is null and o.closeDate is null and o.HUId = :HEAD and o.source is null"),
	@NamedQuery(name="Stechnologymap.getSubImagoByHead", query="select o from Stechnologymap o where o.version is null and o.closeDate is null and o.HUId = :HEAD and o.source = :SOURCE")
				  	   
})
@PrimaryKeyJoinColumn(name = "UId")
public class Stechnologymap extends Sdocument implements Serializable {

	private static final long serialVersionUID = -8324401605148437655L;
	
	private Imago imago;
	
	private Stechnologymap source;
	
	private Double netto;
	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_imago", nullable = true)
	public Imago getImago() {
		return imago;
	}

	public void setImago(Imago imago) {
		this.imago = imago;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_source", nullable = true)
	public Stechnologymap getSource() {
		return source;
	}

	public void setSource(Stechnologymap source) {
		this.source = source;
	}

	public Double getNetto() {
		return netto;
	}

	public void setNetto(Double netto) {
		this.netto = netto;
	}

}
