package org.itx.jbalance.l0.s;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@NamedQueries({
	@NamedQuery(name="Sdictionary.getSpecByTypeDictionary",
		query="" +
			" select " +
				" s " +
			" from " +
			   " Sdictionary s join s.HUId h " +
		    " where " +
		    	" s.version is null " +
		    	" and s.closeDate is null " +
		    	" and h.typeDictionary = :TD " +
		    	" and h.version is null " +
		    	" and h.closeDate is null " +
		    	" order by s.openDate")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Sdictionary extends Sdocument implements Serializable {
	private static final long serialVersionUID = -1079135099710366724L;
	
	private String value_;
	
	private Byte bal;

	public Byte getBal() {
		return bal;
	}

	public void setBal(Byte bal) {
		this.bal = bal;
	}

	public String getValue_() {
		return value_;
	}

	public void setValue_(String value) {
		this.value_ = value;
	}
	
	

}