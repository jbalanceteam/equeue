package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.h.HdivisionJuridical;
import org.itx.jbalance.l0.o.Employee;
import org.itx.jbalance.l0.o.Job;


@Entity
@NamedQueries({
	@NamedQuery(name="SdivisionJuridical.Specification",
			query="from " +
					"SdivisionJuridical o " +
				  "where " +
				  	"o.version is null and " +
				  	"o.closeDate is null and " +
				  	"o.HUId = :Header"),
				  	
	@NamedQuery(name="SdivisionJuridical.getSpecByEmpl",
			query="select o " +
				" from " +
					"SdivisionJuridical o join o.HUId ho " +
				  "where " +
				  	"ho.version is null and " +
				  	"ho.closeDate is null and " +
				  	
				  	"o.version is null and " +
				  	"o.closeDate is null and " +
				  	"o.employee = :EMPL")				  	

				  	
})
@PrimaryKeyJoinColumn(name = "UId")
public class SdivisionJuridical extends Scontractor implements Serializable {
	
	private static final long serialVersionUID = -1913872132276081727L;
	
	private Employee employee;
	
	private Job job;
	
	public SdivisionJuridical(){}
	
	public SdivisionJuridical(SdivisionJuridical s){
		setEmployee(s.getEmployee());
		setJob(s.getJob());
		setHUId((HdivisionJuridical)s.getHUId());
	}
	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_employee", nullable = true)
	public Employee getEmployee(){
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_job", nullable = true)
	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}
}
