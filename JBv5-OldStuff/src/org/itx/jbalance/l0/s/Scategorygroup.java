package org.itx.jbalance.l0.s;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.itx.jbalance.l0.o.Group_;

@Table(name="Scategorygroup_")
@Entity
@NamedQueries({
	@NamedQuery(name="Scategorygroup.getAllSpec",
			query="from " +
					"Scategorygroup o " +
				  "where " +
				  	"o.version is null and " +
				  	"o.closeDate is null"),
	@NamedQuery(name="Scategorygroup.getSpecByHeads", query="select o.group from Scategorygroup o where o.version is null and o.closeDate is null and o.HUId in (:HEADS)")			  	
				  	   
})
@PrimaryKeyJoinColumn(name = "UId")
public class Scategorygroup extends Ssystem implements Serializable{
	private static final long serialVersionUID = -6509683322025071470L;
	
	private Group_ group;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_group", nullable = true)
	public Group_ getGroup() {
		return group;
	}

	public void setGroup(Group_ group) {
		this.group = group;
	} 
	

}
