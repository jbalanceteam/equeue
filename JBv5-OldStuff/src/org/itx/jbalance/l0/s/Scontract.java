package org.itx.jbalance.l0.s;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
@PrimaryKeyJoinColumn(name = "UId")
public class Scontract extends Sware implements Serializable {
	private static final long serialVersionUID = -1079135099710366724L;
	
	private Float summ;
	

	public Float getSumm() {
		return summ;
	}

	public void setSumm(Float summ) {
		this.summ = summ;
	}
}