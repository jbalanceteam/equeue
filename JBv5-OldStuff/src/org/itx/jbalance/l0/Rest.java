package org.itx.jbalance.l0;

import java.io.Serializable;
/**
 * Это ДТО - остаток
 * Используется для склада
 * 
 * @author apv
 *
 */
public class Rest implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public Long uid;
	
	public String name;
	
	public Double rest;
	
	public Long uid_packet;
	
	public Long uid_measurment;
	
	public String name_packet;
	
	public String name_measurment;
	
	
	public Long getUid(){
		return uid;
	}

	public void setUid(Long uid){
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getRest() {
		return rest;
	}

	public void setRest(Double rest) {
		this.rest = rest;
	}

	public String getName_packet() {
		return name_packet;
	}

	public void setName_packet(String namePacket) {
		name_packet = namePacket;
	}

	public String getName_measurment() {
		return name_measurment;
	}

	public void setName_measurment(String nameMeasurment) {
		name_measurment = nameMeasurment;
	}

	public Long getUid_packet() {
		return uid_packet;
	}

	public Rest(Long uid, String name, Double rest, Long uidPacket,
			Long uidMeasurment, String namePacket, String nameMeasurment) {
		super();
		this.uid = uid;
		this.name = name;
		this.rest = rest;
		uid_packet = uidPacket;
		uid_measurment = uidMeasurment;
		name_packet = namePacket;
		name_measurment = nameMeasurment;
	}

	public void setUid_packet(Long uidPacket) {
		uid_packet = uidPacket;
	}

	public Long getUid_measurment() {
		return uid_measurment;
	}

	public void setUid_measurment(Long uidMeasurment) {
		uid_measurment = uidMeasurment;
	}


	public String toString(){
		return name + "  " + name_packet;
	}
	
	public boolean equals(Object arg){
		Rest rest = (Rest)arg;
		return getUid().equals(rest.getUid()) 
				& getUid_measurment().equals(rest.getUid_measurment()) 
				& getUid_packet().equals(rest.getUid_packet());
	}

	
	
	
	
}
