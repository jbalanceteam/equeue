package org.itx.jbalance.l0;

import java.io.Serializable;

public class Age implements Serializable{
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		int years;
		int months;
		int days;
		
		
		
		
		public Age(int years, int months, int days) {
			super();
			this.years = years;
			this.months = months;
			this.days = days;
		}
		
		public Age() {
			super();
		}
	
		public int getYears() {
			return years;
		}
		public int getMonths() {
			return months;
		}
		public int getDays() {
			return days;
		}
		

		@Override
		public String toString() {
//			return "" + 
//					(years  > 0 ? years  + " y. " : "") + 
//					(months > 0 ? months + " m."  : "") + 
//					(days   > 0 ? days   + " m."  : "");
			
			return years + "." + months;
			
		}
		
		public String toStringRus() {
			return "" + 
					(years  > 0 ? years  + " г. " : "") + 
					(months > 0 ? months + " м."  : "") + 
					(days   > 0 ? days   + " д."  : "");
		}

		public void setYears(int years) {
			this.years = years;
		}

		public void setMonths(int months) {
			this.months = months;
		}

		public void setDays(int days) {
			this.days = days;
		}
		
		

	}

	
	