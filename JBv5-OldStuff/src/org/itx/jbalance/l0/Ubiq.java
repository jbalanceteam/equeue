package org.itx.jbalance.l0;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.itx.jbalance.l0.o.Oper;

/**
 * @author dima
 */
@Entity
//@Name("ubiq")
@Inheritance(strategy = InheritanceType.JOINED)
// @PersistenceContext(unitName="DefaultDS")
@NamedQueries({
	@NamedQuery(name="Ubiq.All",query="from Ubiq o where o.version is null and o.closeDate is null order by o.className"),
	@NamedQuery(name="Ubiq.ByUId",query="from Ubiq o where o.version is null and o.closeDate is null and o.UId = :UId"),
	@NamedQuery(name="Ubiq.PrevVersion",query="from Ubiq o where o.version = :version"),
	@NamedQuery(name="Ubiq.Search",query="from Ubiq o where o.version is null and o.closeDate is null and o.barCode like :Pref")
})
public class Ubiq implements Serializable {


	private static final long serialVersionUID = 7191409438438573891L;

	/**
	 * @param UId -
	 *            global uniq identifier
	 */
	private Long UId;

	private Long CheckingHashCode;

	private Timestamp TS;

	private Date OpenDate;

	private Date CloseDate;

	private String ClassName;

	private Oper SysUser;

	private Ubiq Version;

	private Ubiq VersionRoot;

	private Long BarCode;
	
	private String SearchString;



	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((UId == null) ? 0 : UId.hashCode());
		return result;
	}

	public boolean equals(Object other) {
		if (other == null || !(other instanceof Ubiq))
			return false;
		Ubiq that = (Ubiq) other;
		try {
			return this==that || this.UId.equals(that.UId);
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @return Returns the closeDate.
	 * @uml.property name="closeDate"
	 */
	public Date getCloseDate() {
		return CloseDate;
	}

	/**
	 * @param closeDate
	 *            The closeDate to set.
	 * @uml.property name="closeDate"
	 */
	public void setCloseDate(Date closeDate) {
		CloseDate = closeDate;
	}

	/**
	 * @return Returns the checkingHashCode.
	 * @uml.property name="checkingHashCode"
	 */
	public Long getCheckingHashCode() {
		return CheckingHashCode;
	}

	/**
	 * @param checkingHashCode
	 *            The checkingHashCode to set.
	 * @uml.property name="checkingHashCode"
	 */
	public void setCheckingHashCode(Long chc) {
		CheckingHashCode = chc;
	}

	/**
	 * @return Returns the openDate.
	 * @uml.property name="openDate"
	 */
	public Date getOpenDate() {
		return OpenDate;
	}

	/**
	 * @param openDate
	 *            The openDate to set.
	 * @uml.property name="openDate"
	 */
	public void setOpenDate(Date openDate) {
		OpenDate = openDate;
	}

	/**
	 * @return Returns the tS.
	 * @uml.property name="tS"
	 */
	@javax.persistence.Version
	public Timestamp getTS() {
		return TS;
	}

	/**
	 * @param tS
	 *            The tS to set.
	 * @uml.property name="tS"
	 */
	public void setTS(Timestamp ts) {
		TS = ts;
	}

	/**
	 * @return Returns the uId.
	 * @uml.property name="uId"
	 */
	// JBoss 4.0.3 RC1
	// @Id(generate = GeneratorType.AUTO)
	// JBoss 4.0.4 RC1
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getUId() {
		return UId;
	}

	/**
	 * @param uId
	 *            The uId to set.
	 * @uml.property name="uId"
	 */
	public void setUId(Long id) {
		UId = id;
	}

	/**
	 * @return Returns the className.
	 * @uml.property name="className"
	 */
	public String getClassName() {
		return ClassName;
	}

	/**
	 * @param className
	 *            The className to set.
	 * @uml.property name="className"
	 */
	public void setClassName(String className) {
		ClassName = className;
	}

	/**
	 * @return Returns the sysUser.
	 * @uml.property name="sysUser"
	 */
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "SysUser", nullable = true)
	public Oper getSysUser() {
		return SysUser;
	}

	/**
	 * @param sysUser
	 *            The sysUser to set.
	 * @uml.property name="sysUser"
	 */
	public void setSysUser(Oper sysUser) {
		SysUser = sysUser;
	}

	/**
	 * @return Returns the version.
	 * @uml.property name="version"
	 */
	@OneToOne(optional = true,fetch=FetchType.LAZY)
	@JoinColumn(name = "Version", nullable = true)
	public Ubiq getVersion() {
		return Version;
	}

	/**
	 * @param version
	 *            The version to set.
	 * @uml.property name="version"
	 */
	public void setVersion(Ubiq version) {
		Version = version;
	}

	/**
	 * @return Returns the barCode.
	 * @uml.property name="barCode"
	 */
	@Column(nullable = true)
	public Long getBarCode() {
		return BarCode;
	}

	/**
	 * @param barCode
	 *            The barCode to set.
	 * @uml.property name="barCode"
	 */
	public void setBarCode(Long barCode) {
		BarCode = barCode;
	}

	@ManyToOne(optional = true, fetch=FetchType.LAZY)
	@JoinColumn(name = "VersionRoot", nullable = true)
	public Ubiq getVersionRoot() {
		return VersionRoot;
	}

	public void setVersionRoot(Ubiq versionRoot) {
		VersionRoot = versionRoot;
	}

	@Transient
	public String getSearchString() {
		return SearchString;
	}

	public void setSearchString(String searchString) {
		SearchString = searchString;
	}

	
	
}
