package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@NamedQueries({
	@NamedQuery(name="HJobCategory.All",query="from HJobCategory j where j.version is null and j.closeDate is null order by j.openDate,j.dnumber"),
	@NamedQuery(name="HJobCategory.MaxNumber",query="select max(o.dnumber) from HJobCategory o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})
@PrimaryKeyJoinColumn(name = "UId")
public class HJobCategory extends Hdocument implements Serializable
{
	private static final long serialVersionUID = 3326254582947311408L;

	private String name;

	@Column(nullable=false)
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}

	public String toString()
	{
		return name;
	}
}
