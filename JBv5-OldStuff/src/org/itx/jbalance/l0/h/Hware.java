package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Currency;


@Entity
@PrimaryKeyJoinColumn(name = "UId")
public class Hware extends Hdocument implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6131475090397734966L;

	private Float Summ;
	
	private Boolean AccChk;
	
	private Currency currency;

	public Boolean getAccChk() {
		return AccChk;
	}

	public void setAccChk(Boolean accChk) {
		AccChk = accChk;
	}

	@Column(nullable=false)
	public Float getSumm() {
		return Summ;
	}

	public void setSumm(Float summ) {
		Summ = summ;
	}
	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Currency", nullable = true)
	public Currency getCurrency() {
		return currency;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

}
