package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Contractor;

@Entity
@NamedQueries({
	@NamedQuery(name="Hfoodmenu.All",query="from Hfoodmenu o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hfoodmenu.ByPeriod",query="from Hfoodmenu o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hfoodmenu.ByOwnersByPeriod",query="from Hfoodmenu o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	//@NamedQuery(name="Hfoodmenu.Search",query="from Hfoodmenu o where o.version is null and o.closeDate is null and " +
	//		"( o.openDate = :PrefD or o.dnumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.name like :PrefS or o.contractorReceiver.name like :PrefS ) "+
	//		"order by o.openDate,o.dnumber"),
	//@NamedQuery(name="Hfoodmenu.SearchForOwners",query="from Hfoodmenu o where CloseDate is null and Version is null and contractorOwner in (:ContractorOwner) and" +
	//		"( o.openDate = :PrefD or o.dnumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.name like :PrefS or o.contractorReceiver.name like :PrefS ) "+
	//		"order by o.openDate,o.dnumber"),
	//@NamedQuery(name="Hfoodmenu.ByHigherDocumentByOwners",query="from Hfoodmenu o where o.version is null and o.closeDate is null and o.higherDocument = :HigherDocument and o.contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	//@NamedQuery(name="Hfoodmenu.ByHigherDocument",query="from Hfoodmenu o where o.version is null and o.closeDate is null and o.higherDocument = :HigherDocument order by o.openDate,o.dnumber"),
	//@NamedQuery(name="Hfoodmenu.RootDocumentByOwners",query="from Hfoodmenu o where o.version is null and o.closeDate is null and o.higherDocument is null and o.contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	//@NamedQuery(name="Hfoodmenu.RootDocument",query="from Hfoodmenu o where o.version is null and o.closeDate is null and o.higherDocument is null order by o.openDate,o.dnumber"),
	//@NamedQuery(name="Hfoodmenu.NotEmptyDocumentsByOwners",query="select distinct o from Sfoodmenu as s join s.HUId as o where o = s.HUId and s.version is null and s.closeDate is null and o.version is null and o.closeDate is null and o.higherDocument is null and o.contractorOwner in (:ContractorOwner)"),
	@NamedQuery(name="Hfoodmenu.MaxNumber",query="select max(o.dnumber) from Hfoodmenu o where CloseDate is null and Version is null and contractorOwner = :ContractorOwner")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hfoodmenu extends Hware implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 831542580040807161L;
	

	
}
