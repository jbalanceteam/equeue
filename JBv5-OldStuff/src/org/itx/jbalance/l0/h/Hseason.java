/**
 * 
 */
package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Imago;

/**
 * @author galina
 * Надо будет пилить!!
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Hseason.All",query="from Hseason j where j.version is null and j.closeDate is null order by j.dnumber desc"),
	@NamedQuery(name="Hseason.AllCount",query="select count(j) from Hseason j where j.version is null and j.closeDate is null "),
	@NamedQuery(name="Hseason.MaxNumber",query="select max(o.dnumber) from Hseason o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner"),
    @NamedQuery(name="Hseason.getHeadByImago",query="from Hseason o " +
														"where " +
														"o.imago = :IMAGO and " +
														"o.version is null and o.closeDate is null")														
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hseason extends Hdocument implements Serializable{

	private static final long serialVersionUID = 658352625945258630L;

	private Imago imago;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_imago", nullable = true)
	public Imago getImago() {
		return imago;
	}

	public void setImago(Imago imago) {
		this.imago = imago;
	}

}
