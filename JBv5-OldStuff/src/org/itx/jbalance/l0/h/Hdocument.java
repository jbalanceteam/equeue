package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.o.Juridical;

/**
 * @author  dima
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Hdocument.All",query="from Hdocument o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hdocument.ByPeriod",query="from Hdocument o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hdocument.ByOwnersByPeriod",query="from Hdocument o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hdocument.Search",query="from Hdocument o where o.version is null and o.closeDate is null and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hdocument.SearchForOwners",query="from Hdocument o where CloseDate is null and Version is null and contractorOwner in (:ContractorOwner) and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hdocument extends Ubiq implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 399067768066188006L;

	/**
	 * 
	 */

	private String Info;

	private String Foundation;  // Основание

	private Long Dnumber;

	private Juridical ContractorOwner;

	/**
	 * @return  Returns the foundation.
	 * @uml.property  name="foundation"
	 */
	@Column(nullable=false)
	public String getFoundation() {
		return Foundation;
	}

	/**
	 * @param foundation  The foundation to set.
	 * @uml.property  name="foundation"
	 */
	public void setFoundation(String foundation) {
		Foundation = foundation;
	}

	/**
	 * @return  Returns the info.
	 * @uml.property  name="info"
	 */
	@Column(nullable = true, length=5000)
	public String getInfo() {
		return Info;
	}

	/**
	 * @param info  The info to set.
	 * @uml.property  name="info"
	 */
	public void setInfo(String info) {
		Info = info;
	}

	/**
	 * @return  Returns the number.
	 * @uml.property  name="number"
	 */
	public Long getDnumber() {
		return Dnumber;
	}

	/**
	 * @param number  The number to set.
	 * @uml.property  name="number"
	 */
	public void setDnumber(Long dnumber) {
		Dnumber = dnumber;
	}

	/**
	 * @return  Returns the contractorOwner.
	 * @uml.property  name="contractorOwner"
	 */
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "ContractorOwner", nullable = true)
	public Juridical getContractorOwner() {
		return ContractorOwner;
	}

	/**
	 * @param contractorOwner  The contractorOwner to set.
	 * @uml.property  name="contractorOwner"
	 */
	public void setContractorOwner(Juridical contractorOwner) {
		ContractorOwner = contractorOwner;
	}

	// private Hdocument Chain;
	
	@Override
	public String toString() {
		if(Dnumber==null) return null;
		return Dnumber.toString();
	}

}
