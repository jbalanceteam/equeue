package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.o.Warehouse;

@Entity
@NamedQueries({
	@NamedQuery(name="Hwhsdocument.All",query="from Hwhsdocument o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	
	@NamedQuery(name="Hwhsdocument.AllByReciver",query="from Hwhsdocument o where o.version is null and o.closeDate is null and o.contractorReceiver in (:OWNER) and o.contractorReceiver <> o.contractorOwner  order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hwhsdocument.AllByOwner",query="from Hwhsdocument o where o.version is null and o.closeDate is null and o.contractorOwner in (:OWNER) and o.contractorReceiver <> o.contractorOwner order by o.openDate,o.dnumber"),
	
	@NamedQuery(name="Hwhsdocument.getHeadByOwnerBetweenWhs",query="from Hwhsdocument o where o.version is null and o.closeDate is null and o.contractorOwner in (:OWNER) and o.contractorReceiver = o.contractorOwner order by o.openDate,o.dnumber"),
	
	@NamedQuery(name="Hwhsdocument.getHeadByOwnerIncomActWhs",query="from Hwhsdocument o where o.version is null and o.closeDate is null and o.contractorOwner in (:OWNER) and o.contractorReceiver = o.contractorOwner and o.whsTo.zero=true order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hwhsdocument.getHeadByOwnerConsumptionActWhs",query="from Hwhsdocument o where o.version is null and o.closeDate is null and o.contractorOwner in (:OWNER) and o.contractorReceiver = o.contractorOwner and o.whsFrom.zero=true order by o.openDate,o.dnumber"),
	

	@NamedQuery(name="Hwhsdocument.CountAllByReciver",query="select count(*) from Hwhsdocument o where o.version is null and o.closeDate is null and o.contractorReceiver in (:OWNER) and o.contractorReceiver <> o.contractorOwner"),
	@NamedQuery(name="Hwhsdocument.CountAllByOwner",query="select count(*) from Hwhsdocument o where o.version is null and o.closeDate is null and o.contractorOwner in (:OWNER) and o.contractorReceiver <> o.contractorOwner"),
	
	@NamedQuery(name="Hwhsdocument.getCountHeadByOwnerBetweenWhs",query="select count(*) from Hwhsdocument o where o.version is null and o.closeDate is null and o.contractorOwner in (:OWNER) and o.contractorReceiver = o.contractorOwner"),
	
	@NamedQuery(name="Hwhsdocument.getCountHeadByOwnerIncomActWhs",query="select count(*) from Hwhsdocument o where o.version is null and o.closeDate is null and o.contractorOwner in (:OWNER) and o.contractorReceiver = o.contractorOwner and o.whsTo.zero=true"),
	@NamedQuery(name="Hwhsdocument.getCountHeadByOwnerConsumptionActWhs",query="select count(*) from Hwhsdocument o where o.version is null and o.closeDate is null and o.contractorOwner in (:OWNER) and o.contractorReceiver = o.contractorOwner and o.whsFrom.zero=true"),
	
	
	@NamedQuery(name="Hwhsdocument.ByPeriod",query="from Hwhsdocument o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hwhsdocument.ByOwnersByPeriod",query="from Hwhsdocument o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hwhsdocument.Search",query="from Hwhsdocument o where o.version is null and o.closeDate is null and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.whsFrom.warehouseNumber = :PrefN or o.whsTo.warehouseNumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.contractorReceiver.name like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hwhsdocument.SearchForOwners",query="from Hwhsdocument o where CloseDate is null and Version is null and contractorOwner in (:ContractorOwner) and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.whsFrom.warehouseNumber = :PrefN or o.whsTo.warehouseNumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.contractorReceiver.name like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hwhsdocument.MaxNumber",query="select max(o.dnumber) from Hwhsdocument o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
	
	
})
	@PrimaryKeyJoinColumn(name = "UId")
public class Hwhsdocument extends Hware implements Serializable {

	private static final long serialVersionUID = -2594219931504012347L;

	private Contractor ContractorReceiver;

	private Warehouse WhsFrom;
	
	private Warehouse WhsTo;

	
	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "ContractorReceiver", nullable = true)
	public Contractor getContractorReceiver() {
		return ContractorReceiver;
	}
	public void setContractorReceiver(Contractor contractorReceiver) {
		ContractorReceiver = contractorReceiver;
	}	

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "WhsFrom", nullable = true)
	public Warehouse getWhsFrom(){
		return WhsFrom;
	}

	public void setWhsFrom(Warehouse whsFrom) {
		WhsFrom = whsFrom;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "WhsTo", nullable = true)
	public Warehouse getWhsTo() {
		return WhsTo;
	}

	public void setWhsTo(Warehouse whsTo) {
		WhsTo = whsTo;
	}

}
