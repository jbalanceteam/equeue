package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@NamedQueries({
	@NamedQuery(name="Hcontractorright.All",query="from Hcontractorright o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hcontractorright.ByPeriod",query="from Hcontractorright o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hcontractorright.ByOwnersByPeriod",query="from Hcontractorright o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hcontractorright.Search",query="from Hcontractorright o where o.version is null and o.closeDate is null and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hcontractorright.SearchForOwners",query="from Hcontractorright o where CloseDate is null and Version is null and contractorOwner in (:ContractorOwner) and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hcontractorright.MaxNumber",query="select max(o.dnumber) from Hcontractorright o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hcontractorright extends Hsystem implements Serializable {
	private static final long serialVersionUID = 550170721765567250L;
	
}
