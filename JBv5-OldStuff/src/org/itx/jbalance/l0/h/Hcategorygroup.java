package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;


@Table(name="Hcategorygroup_")
@Entity
@NamedQueries({
	@NamedQuery(name="Hcategorygroup.All",query="from Hcategorygroup j where j.version is null and j.closeDate is null order by j.openDate,j.dnumber"),
	@NamedQuery(name="Hcategorygroup.MaxNumber",query="select max(o.dnumber) from Hcategorygroup o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hcategorygroup extends Hsystem implements Serializable{
	private static final long serialVersionUID = 3326254582947311408L;

	private String name;
	
	private boolean repetition; 	
	
	@Column(nullable=false)
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}

	public String toString(){
		return name;
	}
	
	public boolean isRepetition() {
		return repetition;
	}
	public void setRepetition(boolean repetition) {
		this.repetition = repetition;
	}
}
