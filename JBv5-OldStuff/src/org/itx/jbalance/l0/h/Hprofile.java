package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Oper;

@Entity
@NamedQueries( {
		@NamedQuery(name = "Hprofile.All", query = "from Hprofile o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
		@NamedQuery(name = "Hprofile.ByPeriod",query="from Hprofile o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
		@NamedQuery(name = "Hprofile.ByOwnersByPeriod",query="from Hprofile o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
		@NamedQuery(name = "Hprofile.Search",query="from Hprofile o where o.version is null and o.closeDate is null and " +
				"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.operReceiver.name like :PrefS ) "+
				"order by o.openDate,o.dnumber"),
		@NamedQuery(name = "Hprofile.SearchForOwners",query="from Hprofile o where CloseDate is null and Version is null and contractorOwner in (:ContractorOwner) and " +
				"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.operReceiver.name like :PrefS ) "+
				"order by o.openDate,o.dnumber"),
				
		@NamedQuery(name = "Hprofile.ByOperReceiver", query = "from Hprofile o where o.version is null and o.closeDate is null and OperReceiver = :OperReceiver"),
		@NamedQuery(name = "Hprofile.HeadByOperReceiver", query = "select o.UId from Hprofile o where o.version is null and o.closeDate is null and OperReceiver = :OperReceiver"),
		@NamedQuery(name = "Hprofile.MaxNumber", query = "select max(o.dnumber) from Hprofile o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner") 
		})
@PrimaryKeyJoinColumn(name = "UId")
public class Hprofile extends Hsystem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 550170721765567250L;

	private Oper OperReceiver;
	
	private String Skin;

	public String getSkin() {
		return Skin;
	}

	public void setSkin(String skin) {
		Skin = skin;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "OperReceiver", nullable = true)
	public Oper getOperReceiver() {
		return OperReceiver;
	}

	public void setOperReceiver(Oper contractorReceiver) {
		OperReceiver = contractorReceiver;
	}

}
