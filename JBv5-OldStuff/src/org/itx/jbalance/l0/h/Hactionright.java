package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@NamedQueries({
	@NamedQuery(name="Hactionright.All",query="from Hactionright o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hactionright.ByPeriod",query="from Hactionright o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hactionright.ByOwnersByPeriod",query="from Hactionright o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hactionright.Search",query="from Hactionright o where o.version is null and o.closeDate is null and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hactionright.SearchForOwners",query="from Hactionright o where CloseDate is null and Version is null and contractorOwner in (:ContractorOwner) and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hactionright.MaxNumber",query="select max(o.dnumber) from Hactionright o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hactionright extends Hsystem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 550170721765567250L;
	

}
