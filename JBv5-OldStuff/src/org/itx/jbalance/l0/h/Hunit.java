/**
 * 
 */
package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Measurement;

/**
 * @author galina
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Hunit.All",query="from Hunit j where j.version is null and j.closeDate is null order by j.dnumber desc"),
	@NamedQuery(name="Hunit.AllCount",query="select count(j) from Hunit j where j.version is null and j.closeDate is null "),
	@NamedQuery(name="Hunit.MaxNumber",query="select max(o.dnumber) from Hunit o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner"),
	@NamedQuery(name="Hunit.Head",query="from Hunit o " +
														"where " +
														"o.aunit = :AUNIT and " +
														"o.measurement = :MEASUREMENT and " +
														"o.version is null and o.closeDate is null"),
    @NamedQuery(name="Hunit.getHeadByAunit",query="from Hunit o " +
														"where " +
														"o.aunit = :AUNIT and " +
														"o.version is null and o.closeDate is null")														
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hunit extends Hdocument implements Serializable{

	private static final long serialVersionUID = 4821290936120669984L;
	
	private Aunit aunit;
	
	private Measurement measurement;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_aunit", nullable = true)
	public Aunit getAunit() {
		return aunit;
	}

	public void setAunit(Aunit aunit) {
		this.aunit = aunit;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_measurement", nullable = true)
	public Measurement getMeasurement() {
		return measurement;
	}

	public void setMeasurement(Measurement measurement) {
		this.measurement = measurement;
	}

}
