package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;


	@Entity
	@NamedQueries({
		@NamedQuery(name="Hmenu.All",query="from Hmenu o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
		@NamedQuery(name="Hmenu.ByPeriod",query="from Hmenu o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
		@NamedQuery(name="Hmenu.ByOwnersByPeriod",query="from Hmenu o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
		@NamedQuery(name="Hmenu.Search",query="from Hmenu o where o.version is null and o.closeDate is null and " +
				"( o.openDate = :PrefD or o.dnumber = :PrefN or o.name like :PrefS or o.foundation like :PrefS or o.info like :PrefS ) "+
				"order by o.openDate,o.dnumber"),
		@NamedQuery(name="Hmenu.SearchForOwners",query="from Hmenu o where CloseDate is null and Version is null and contractorOwner in (:ContractorOwner) and " +
				"( o.openDate = :PrefD or o.dnumber = :PrefN or o.name like :PrefS or o.foundation like :PrefS or o.info like :PrefS ) "+
				"order by o.openDate,o.dnumber"),
		@NamedQuery(name="Hmenu.ByHigherDocumentByOwners",query="from Hmenu o where o.version is null and o.closeDate is null and o.higherDocument = :HigherDocument and o.contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
		
		@NamedQuery(name="Hmenu.ByHigherDocument",query="from Hmenu o where o.version is null and o.closeDate is null and o.higherDocument = :HigherDocument order by o.openDate,o.dnumber"),
		@NamedQuery(name="Hmenu.RootDocumentByOwners",query="from Hmenu o where o.version is null and o.closeDate is null and o.higherDocument is null and o.contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
		@NamedQuery(name="Hmenu.RootDocument",query="from Hmenu o where o.version is null and o.closeDate is null and o.higherDocument is null order by o.openDate,o.dnumber"),
		@NamedQuery(name="Hmenu.NotEmptyDocumentsByOwners",query="select distinct o from Smenu as s join s.HUId as o where o = s.HUId and s.version is null and s.closeDate is null and o.version is null and o.closeDate is null and o.higherDocument is null and o.contractorOwner in (:ContractorOwner)"),
		@NamedQuery(name="Hmenu.MaxNumber",query="select max(o.dnumber) from Hmenu o where CloseDate is null and Version is null and contractorOwner = :ContractorOwner")
	})
	@PrimaryKeyJoinColumn(name = "UId")
//
	public class Hmenu extends Hsystem implements HierarchicalHeader,Serializable {
		private static final long serialVersionUID = 550170721765567250L;
		
		private String Name;

		private Hmenu HigherDocument;
		
		@OneToOne(optional = true)
		@JoinColumn(name = "HigherDocument", nullable = true)
		public Hmenu getHigherDocument() {
			return HigherDocument;
		}

		public void setHigherDocument(Hdocument higherDocument) {
			HigherDocument = (Hmenu)higherDocument;
		}

		public String getName() {
			return Name;
		}

		public void setName(String name) {
			Name = name;
		}

	}

