package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Contractor;

@Entity
@NamedQueries({
	@NamedQuery(name="Hprice.All",query="from Hprice o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hprice.ByPeriod",query="from Hprice o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hprice.ByOwnersByPeriod",query="from Hprice o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hprice.Search",query="from Hprice o where o.version is null and o.closeDate is null and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.name like :PrefS or o.contractorReceiver.name like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hprice.SearchForOwners",query="from Hprice o where CloseDate is null and Version is null and contractorOwner in (:ContractorOwner) and" +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.name like :PrefS or o.contractorReceiver.name like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hprice.ByHigherDocumentByOwners",query="from Hprice o where o.version is null and o.closeDate is null and o.higherDocument = :HigherDocument and o.contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hprice.ByHigherDocument",query="from Hprice o where o.version is null and o.closeDate is null and o.higherDocument = :HigherDocument order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hprice.RootDocumentByOwners",query="from Hprice o where o.version is null and o.closeDate is null and o.higherDocument is null and o.contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hprice.RootDocument",query="from Hprice o where o.version is null and o.closeDate is null and o.higherDocument is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hprice.NotEmptyDocumentsByOwners",query="select distinct o from Sprice as s join s.HUId as o where o = s.HUId and s.version is null and s.closeDate is null and o.version is null and o.closeDate is null and o.higherDocument is null and o.contractorOwner in (:ContractorOwner)"),
	@NamedQuery(name="Hprice.MaxNumber",query="select max(o.dnumber) from Hprice o where CloseDate is null and Version is null and contractorOwner = :ContractorOwner")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hprice extends Hware implements HierarchicalHeader,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5983804880260918098L;

	private String Name;

	private Hprice HigherDocument;
	
	private Contractor ContractorReceiver;
	
	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	@OneToOne(optional = true)
	@JoinColumn(name = "HigherDocument", nullable = true)
	public Hprice getHigherDocument() {
		return HigherDocument;
	}

	public void setHigherDocument(Hdocument higherPrice) {
		HigherDocument = (Hprice)higherPrice;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "ContractorReceiver", nullable = true)
	public Contractor getContractorReceiver() {
		return ContractorReceiver;
	}
	public void setContractorReceiver(Contractor contractorReceiver) {
		ContractorReceiver = contractorReceiver;
	}



}
