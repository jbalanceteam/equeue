/**
 * 
 */
package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Imago;
import org.itx.jbalance.l0.o.Measurement;

/**
 * @author galina
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Himago.All",query="from Himago j where j.version is null and j.closeDate is null order by j.dnumber desc"),
	@NamedQuery(name="Himago.AllCount",query="select count(j) from Himago j where j.version is null and j.closeDate is null "),
	@NamedQuery(name="Himago.MaxNumber",query="select max(o.dnumber) from Himago o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner"),
    @NamedQuery(name="Himago.getHeadByImago",query="from Himago o " +
														"where " +
														"o.imago = :IMAGO and " +
														"o.version is null and o.closeDate is null")														
})
@PrimaryKeyJoinColumn(name = "UId")
public class Himago extends Hdocument implements Serializable{

	private static final long serialVersionUID = 4821290936120669984L;
	
	private Imago imago;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_imago", nullable = true)
	public Imago getImago() {
		return imago;
	}

	public void setImago(Imago imago_) {
		this.imago = imago_;
	}

}
