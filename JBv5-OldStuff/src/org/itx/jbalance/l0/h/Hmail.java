package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries({
	@NamedQuery(name="Hmail.All",query="from Hmail o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hmail.ByOwnersByPeriod",query="from Hmail o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hmail.MaxNumber",query="select max(o.dnumber) from Hmail o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})
public class Hmail extends Hcontractor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7079479002907107711L;


}
