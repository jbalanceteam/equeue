package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@NamedQueries({
	@NamedQuery(name="Hcontractorwarehouse.All",query="from Hcontractorwarehouse o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hcontractorwarehouse.ByPeriod",query="from Hcontractorwarehouse o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hcontractorwarehouse.ByOwner",query="from Hcontractorwarehouse o where CloseDate is null and Version is null and contractorOwner = :ContractorOwner order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hcontractorwarehouse.ByOwnersByPeriod",query="from Hcontractorwarehouse o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hcontractorwarehouse.Search",query="from Hcontractorwarehouse o where o.version is null and o.closeDate is null and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hcontractorwarehouse.SearchForOwners",query="from Hcontractorwarehouse o where CloseDate is null and Version is null and contractorOwner in (:ContractorOwner) and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hcontractorwarehouse.MaxNumber",query="select max(o.dnumber) from Hcontractorwarehouse o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hcontractorwarehouse extends Hsystem implements Serializable {

	private static final long serialVersionUID = -5151574549879070089L;

}
