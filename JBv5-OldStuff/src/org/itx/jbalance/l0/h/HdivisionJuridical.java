package org.itx.jbalance.l0.h;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.Parameter;
import org.itx.jbalance.l0.o.Division;
import org.itx.jbalance.l0.o.Juridical;

@Entity
@NamedQueries({
	@NamedQuery(name="HdivisionJuridical.MaxNumber",query="select max(o.dnumber) from HdivisionJuridical o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner"),
	@NamedQuery(name="HdivisionJuridical.Head",query="from HdivisionJuridical o " +
														"where " +
														"o.juridical = :juridical and " +
														"division = :division and " +
														"o.version is null and o.closeDate is null"),
    @NamedQuery(name="HdivisionJuridical.HeadJuridical",query="from HdivisionJuridical o " +
														"where " +
															"o.juridical = :juridical and " +
															"o.version is null and o.closeDate is null "),
															
	@NamedQuery(name="HdivisionJuridical.HdivisionJuridicalByDivision",query="" +
															" from HdivisionJuridical o " +
																"where " +
																	"o.division = :DIVISION and " +
																	"o.version is null and o.closeDate is null ")															
})
//@FilterDef(name="HdivisionJuridical.probafilternowork",  parameters = @ParamDef(name="proba", type="Long"))
//@Filter(name="HdivisionJuridical.probafilternowork", condition=":proba = UId")

//@FilterDef(name="minLength1",parameters={@ParamDef(name="minLength1", type="long")})
//@Filters( {
//    @Filter(name="minLength1",condition=":minLength1 = UId")
//} )

@PrimaryKeyJoinColumn(name = "UId")
public class HdivisionJuridical extends Hcontractor implements Serializable {

	private static final long serialVersionUID = 6131475090397734966L;
	
	private Juridical juridical;
	
	private Division division;

	
	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_contractor", nullable = true)
	public Juridical getJuridical() {
		return juridical;
	}


	public void setJuridical(Juridical juridical) {
		this.juridical = juridical;
	}

	
	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_division", nullable = true)
	public Division getDivision() {
		return division;
	}


	public void setDivision(Division division) {
		this.division = division;
	}


	}
