package org.itx.jbalance.l0.h;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;



@Entity
@NamedQueries({
	@NamedQuery(name="Hdictionary.All",query="from Hdictionary j where j.version is null and j.closeDate is null order by j.openDate,j.dnumber"),
	@NamedQuery(name="Hdictionary.MaxNumber",query="select max(o.dnumber) from Hdictionary o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hdictionary extends Hdocument implements Serializable {
	private static final long serialVersionUID = 399067768066188006L;
	
	private String name;

	/**
	 * 0 - Задача
	 */
	private byte typeDictionary;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public byte getTypeDictionary() {
		return typeDictionary;
	}

	public void setTypeDictionary(byte typeDictionary) {
		this.typeDictionary = typeDictionary;
	}
		

}
