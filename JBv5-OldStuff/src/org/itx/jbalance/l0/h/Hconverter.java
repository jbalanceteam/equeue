package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import org.itx.jbalance.l0.s.Sware;


@Entity
@NamedQueries({
//	@NamedQuery(name="Hreport.All",query="from Hreport j where j.version is null and j.closeDate is null order by j.openDate,j.dnumber"),
//	@NamedQuery(name="Hreport.ByTask",query="from Hreport j where j.version is null and j.closeDate is null and source = :htask order by j.openDate,j.dnumber"),
	@NamedQuery(name="Hconverter.MaxNumber",query="select max(o.dnumber) from Hconverter o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")

//	@NamedQuery(name="Hreport.ByWeek",query="from  Hreport  j  where  j.version is null and  j.closeDate is null and  j.source = :htask and  j.openDate >= :startd and  j.openDate <= :endd order by j.openDate")
})
  
@PrimaryKeyJoinColumn(name = "UId")
public class Hconverter extends Hdocument implements Serializable  {
	
	private static final long serialVersionUID = 7142358372656740776L;

	private Sware ware;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Sware", nullable = false)
	public Sware getWare() {
		return ware;
	}
	public void setWare(Sware ware) {
		this.ware = ware;
	}
	
	
	
}
