package org.itx.jbalance.l0.h;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import org.itx.jbalance.l0.o.Contractor;

@Entity
@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries({
	@NamedQuery(name="Hbillfact.All",query="from Hbillfact o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hbillfact.ByPeriod",query="from Hbillfact o where o.version is null and o.closeDate is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hbillfact.ByOwnersByPeriod",query="from Hbillfact o where o.version is null and o.closeDate is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hbillfact.Search",query="from Hbillfact o where o.version is null and o.closeDate is null and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.contractorReceiver.name like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hbillfact.SearchForOwners",query="from Hbillfact o where o.version is null and o.closeDate is null and contractorOwner in (:ContractorOwner) and" +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.contractorReceiver.name like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
			@NamedQuery(name="Hbillfact.MaxNumber",query="select max(o.dnumber) from Hbillfact o where o.version is null and o.closeDate is null and ContractorOwner = :ContractorOwner"),
			@NamedQuery(name="Hbillfact.getBillByContractorReceiver",query="select o from Hbillfact o where o.version is null and o.closeDate is null and ContractorReceiver = :CONTRACTOR"),
			@NamedQuery(name="Hbillfact.getBillByContractorReceiverCount",query="select count(o) from Hbillfact o where o.version is null and o.closeDate is null and ContractorReceiver = :CONTRACTOR"),
			
	@NamedQuery(name="Hbillfact.getBillByContractorReceiversByPeriod", query=
		"from Hbillfact o where o.version is null " +
			"and o.closeDate is null " +
			"and o.contractorReceiver in (:Contractors) " +
			"and (o.openDate between :startDate and :endDate)")
})
public class Hbillfact extends Hware implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7079479002907107711L;
	private Boolean BookChk;
	private Contractor ContractorReceiver;


	public Boolean getBookChk() {
		return BookChk;
	}
	public void setBookChk(Boolean bookChk) {
		BookChk = bookChk;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "ContractorReceiver", nullable = true)
	public Contractor getContractorReceiver() {
		return ContractorReceiver;
	}
	public void setContractorReceiver(Contractor contractorReceiver) {
		ContractorReceiver = contractorReceiver;
	}
}
