package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
@NamedQueries({
	@NamedQuery(name="Hrequest.All",query="from Hrequest o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hrequest.ByPeriod",query="from Hrequest o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hrequest.ByOwnersByPeriod",query="from Hrequest o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hrequest.Search",query="from Hrequest o where o.version is null and o.closeDate is null and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hrequest.SearchForOwners",query="from Hrequest o where CloseDate is null and Version is null and contractorOwner in (:ContractorOwner) and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hrequest.MaxNumber",query="select max(o.dnumber) from Hrequest o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})

@PrimaryKeyJoinColumn(name = "UId")
public class Hrequest extends Hware implements Serializable {

	private static final long serialVersionUID = -4810124345496997946L;
	

}
