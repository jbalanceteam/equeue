package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;


/**
 * Часть администрирования
 * Документ, который определяет поведение системы
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Hsystem.All",query="from Hsystem o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hsystem.ByPeriod",query="from Hsystem o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hsystem.ByOwnersByPeriod",query="from Hsystem o where CloseDate is null and Version is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hsystem.Search",query="from Hsystem o where o.version is null and o.closeDate is null and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hsystem.SearchForOwners",query="from Hsystem o where CloseDate is null and Version is null and contractorOwner in (:ContractorOwner) and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.foundation like :PrefS or o.info like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hsystem.MaxNumber",query="select max(o.dnumber) from Hsystem o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hsystem extends Hdocument implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = 2230964533043202757L;
	
	private Boolean SysChk;

	public Boolean getSysChk() {
		return SysChk;
	}

	public void setSysChk(Boolean sysChk) {
		SysChk = sysChk;
	}


}
