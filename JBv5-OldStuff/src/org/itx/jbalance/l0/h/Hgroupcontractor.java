package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Group_;



@Entity
@NamedQueries({
	@NamedQuery(name="Hgroupcontractor.All",query="from Hgroupcontractor j where j.version is null and j.closeDate is null order by j.openDate,j.dnumber"),
	@NamedQuery(name="Hgroupcontractor.MaxNumber",query="select max(o.dnumber) from Hgroupcontractor o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner"),
	@NamedQuery(name="Hgroupcontractor.GroupContractorByGroup",query="from Hgroupcontractor j where j.version is null and j.group =:GROUP and j.closeDate is null order by j.openDate,j.dnumber")
	
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hgroupcontractor extends Hcontractor implements Serializable{
	private static final long serialVersionUID = 3326254582947311408L;
		
	private Group_ group;


	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_group", nullable = true)
	public Group_ getGroup() {
		return group;
	}

	public void setGroup(Group_ group) {
		this.group = group;
	}
}
