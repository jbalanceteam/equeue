package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;


import org.itx.jbalance.l0.o.Juridical;


@Entity
@NamedQueries({
	@NamedQuery(name="Hcontactpersons.MaxNumber",query="select max(o.dnumber) from Hcontactpersons o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})

@PrimaryKeyJoinColumn(name = "UId")
public class Hcontactpersons extends Hcontractor implements Serializable {

	private static final long serialVersionUID = 6131475090397734966L;
	
	
	private Juridical juridicalName;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "JuridicalName", nullable = true)
	public Juridical getJuridicalName() {
		return juridicalName;
	}


	public void setJuridicalName(Juridical juridicalName) {
		this.juridicalName = juridicalName;
	}



}
