package org.itx.jbalance.l0.h;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Warehouse;

@Entity
@NamedQueries({
	@NamedQuery(name="Hmoveaunit.All",query="from Hmoveaunit o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hmoveaunit.MaxNumber",query="select max(o.dnumber) from Hmoveaunit o where CloseDate is null and Version is null and contractorOwner = :ContractorOwner")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hmoveaunit extends Hreportware implements Serializable{

	private static final long serialVersionUID = 658352625945258630L;
	
	public Date begin_period;
	
	public Date end_period;
	
	public Warehouse whs;

	@OneToOne(optional = true)
	@JoinColumn(name = "whs", nullable = true)
	public Warehouse getWhs() {
		return whs;
	}

	public void setWhs(Warehouse whs) {
		this.whs = whs;
	}

	public Date getBegin_period() {
		return begin_period;
	}

	public void setBegin_period(Date beginPeriod) {
		begin_period = beginPeriod;
	}

	public Date getEnd_period() {
		return end_period;
	}

	public void setEnd_period(Date endPeriod) {
		end_period = endPeriod;
	}
	
	
	
	
}
