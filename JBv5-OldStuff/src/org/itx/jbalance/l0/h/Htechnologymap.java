/**
 * 
 */
package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Aunit;
import org.itx.jbalance.l0.o.Division;
import org.itx.jbalance.l0.o.Measurement;

/**
 * @author galina
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Htechnologymap.All",query="from Htechnologymap j where j.version is null and j.closeDate is null order by j.dnumber desc"),
	@NamedQuery(name="Htechnologymap.AllCount",query="select count(j) from Htechnologymap j where j.version is null and j.closeDate is null "),
	@NamedQuery(name="Htechnologymap.MaxNumber",query="select max(o.dnumber) from Htechnologymap o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner"),
	@NamedQuery(name="Htechnologymap.Head",query="from Htechnologymap o " +
														"where " +
														"o.aunit = :AUNIT and " +
														"o.division = :DIVISION and " +
														"o.version is null and o.closeDate is null"),
    @NamedQuery(name="Htechnologymap.getHeadByAunit",query="from Htechnologymap o " +
														"where " +
														"o.aunit = :AUNIT and " +
														"o.version is null and o.closeDate is null")														
})
@PrimaryKeyJoinColumn(name = "UId")
public class Htechnologymap extends Hdocument implements Serializable{

	private static final long serialVersionUID = 4821290936120669984L;
	
	private Aunit aunit;
	
	private Division division;

	private Integer category_product;
	
	private Double output;
	
	private String collectionRecipes;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_aunit", nullable = true)
	public Aunit getAunit() {
		return aunit;
	}

	public void setAunit(Aunit aunit) {
		this.aunit = aunit;
	}
	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_division", nullable = true)
	public Division getDivision() {
		return division;
	}

	public void setDivision(Division division) {
		this.division = division;
	}

	public Integer getCategory_product() {
		return category_product;
	}

	public void setCategory_product(Integer categoryProduct) {
		category_product = categoryProduct;
	}

	public Double getOutput() {
		return output;
	}

	public void setOutput(Double output) {
		this.output = output;
	}

	public String getCollectionRecipes() {
		return collectionRecipes;
	}

	public void setCollectionRecipes(String collectionRecipes) {
		this.collectionRecipes = collectionRecipes;
	}

	@Override
	public String toString() {
		if(aunit==null) return null;
		return aunit.toString()+"/"+output;
	}

}
