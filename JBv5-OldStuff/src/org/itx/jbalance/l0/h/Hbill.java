package org.itx.jbalance.l0.h;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.itx.jbalance.l0.o.Contractor;


@Entity
@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries({
	@NamedQuery(name="Hbill.All",query="from Hbill o where o.version is null and o.closeDate is null order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hbill.AllCount",query="select count(o) from Hbill o where o.version is null and o.closeDate is null"),
	
	@NamedQuery(name="Hbill.AllBySource",query="from Hbill o where o.source =:SOURCE and o.version is null and o.closeDate is null order by o.dnumber desc"),
	@NamedQuery(name="Hbill.AllBySourceCount",query="select count(o) from Hbill o where o.source =:SOURCE and o.version is null and o.closeDate is null"),
	
	@NamedQuery(name="Hbill.ByPeriod",query="from Hbill o where o.version is null and o.closeDate is null and OpenDate >= :periodFrom and OpenDate <= :periodTo order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hbill.ByOwnersByPeriod",query="from Hbill o where o.version is null and o.closeDate is null and OpenDate >= :periodFrom and OpenDate <= :periodTo and contractorOwner in (:ContractorOwner) order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hbill.Search",query="from Hbill o where o.version is null and o.closeDate is null and " +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.contractorReceiver.name like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
	@NamedQuery(name="Hbill.SearchForOwners",query="from Hbill o where o.version is null and o.closeDate is null and contractorOwner in (:ContractorOwner) and" +
			"( o.openDate = :PrefD or o.dnumber = :PrefN or o.summ = :PrefN or o.foundation like :PrefS or o.info like :PrefS or o.contractorReceiver.name like :PrefS ) "+
			"order by o.openDate,o.dnumber"),
			@NamedQuery(name="Hbill.MaxNumber",query="select max(o.dnumber) from Hbill o where o.version is null and o.closeDate is null and ContractorOwner = :ContractorOwner"),
			@NamedQuery(name="Hbill.getBillByContractorReceiver",query="select o from Hbill o where o.version is null and o.closeDate is null and ContractorReceiver = :CONTRACTOR"),
			@NamedQuery(name="Hbill.getBillByContractorReceiverCount",query="select count(o) from Hbill o where o.version is null and o.closeDate is null and ContractorReceiver = :CONTRACTOR")
})
public class Hbill extends Hware implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2891317456857716705L;
		
	private Boolean BookChk;
	private Contractor ContractorReceiver;
	private Hdocument source;

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Source", nullable = true)
	public Hdocument getSource() {
		return source;
	}
	public void setSource(Hdocument source) {
		this.source = source;
	}
	public Boolean getBookChk() {
		return BookChk;
	}
	public void setBookChk(Boolean bookChk) {
		BookChk = bookChk;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "ContractorReceiver", nullable = true)
	public Contractor getContractorReceiver() {
		return ContractorReceiver;
	}
	public void setContractorReceiver(Contractor contractorReceiver) {
		ContractorReceiver = contractorReceiver;
	}

}
