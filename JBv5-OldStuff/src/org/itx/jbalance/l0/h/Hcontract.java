package org.itx.jbalance.l0.h;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import org.itx.jbalance.l0.o.Contractor;
import org.itx.jbalance.l0.o.Typecontract;

@Entity
@NamedQueries({
	@NamedQuery(name="Hcontract.All",query="from Hcontract j where j.version is null and j.closeDate is null order by j.dnumber desc"),
	@NamedQuery(name="Hcontract.AllCount",query="select count(j) from Hcontract j where j.version is null and j.closeDate is null "),
	@NamedQuery(name="Hcontract.MaxNumber",query="select max(o.dnumber) from Hcontract o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner"),
	@NamedQuery(name="Hcontract.MaxNumberD",query="select max(o.dnumber) from Hcontract o where o.typecontract =:TypeContract and CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Hcontract extends Hware implements Serializable {
	
	private static final long serialVersionUID = 399067768066188006L;
	
	public String disdnumber;
	
	public Date startdate;
	
	public Date enddate;
	
	public Typecontract typecontract;
	
	private Contractor contractorreceiver;
	
	private boolean createfile = false;
	
	private byte[] filed;

	public byte[] getFiled() {
		return filed;
	}

	public void setFiled(byte[] filed) {
		this.filed = filed;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_contractorreceiver", nullable = true)
	public Contractor getContractorreceiver(){
		return contractorreceiver;
	}

	public void setContractorreceiver(Contractor contractorreceiver) {
		this.contractorreceiver = contractorreceiver;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "id_typecontract", nullable = true)
	public Typecontract getTypecontract() {
		return typecontract;
	}

	public void setTypecontract(Typecontract typecontract) {
		this.typecontract = typecontract;
	}

	public String getDisdnumber() {
		return disdnumber;
	}

	public void setDisdnumber(String disdnumber) {
		this.disdnumber = disdnumber;
	}

	public boolean isCreatefile() {
		return createfile;
	}

	public void setCreatefile(boolean createfile) {
		this.createfile = createfile;
	}
			

}
