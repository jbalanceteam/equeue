package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@NamedQueries( { 
	@NamedQuery(name = "Currency.All", query = "from Currency o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name = "Currency.AllCount", query = "select count(o) from Currency o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Currency.Search",query="from Currency o where o.version is null and o.closeDate is null and (o.ISO4217AlphabeticCode like :Pref or o.ISO4217NumericCode like :Pref or o.name like :Pref) order by o.name"),
	@NamedQuery(name="Currency.SearchCount",query="select count(o) from Currency o where o.version is null and o.closeDate is null and (o.ISO4217AlphabeticCode like :Pref or o.ISO4217NumericCode like :Pref or o.name like :Pref) order by o.name")
	})
@PrimaryKeyJoinColumn(name = "UId")
public class Currency extends Businesobject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2454916018411637328L;

	private String ShortName;

	private String ISO4217AlphabeticCode;

	private String ISO4217NumericCode;

	private String Description;

	public String getShortName() {
		return ShortName;
	}

	public void setShortName(String shortName) {
		ShortName = shortName;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getISO4217AlphabeticCode() {
		return ISO4217AlphabeticCode;
	}

	public void setISO4217AlphabeticCode(String alphabeticCode) {
		ISO4217AlphabeticCode = alphabeticCode;
	}

	public String getISO4217NumericCode() {
		return ISO4217NumericCode;
	}

	public void setISO4217NumericCode(String numericCode) {
		ISO4217NumericCode = numericCode;
	}

}
