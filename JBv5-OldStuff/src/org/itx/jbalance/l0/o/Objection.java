package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@NamedQueries( { 
	@NamedQuery(name = "Objection.All", query = "from Objection o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name = "Objection.AllCount", query = "select count(o) from Objection o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="Objection.Search",query="from Objection o where o.version is null and o.closeDate is null and o.name like :Pref order by o.name"),
	@NamedQuery(name="Objection.SearchCount",query="select count(o) from Objection o where o.version is null and o.closeDate is null and o.name like :Pref")
	})
@PrimaryKeyJoinColumn(name = "UId")
public class Objection extends Businesobject implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
