/**
 * 
 */
package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * @author galina
 *
 */
@Entity
@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries({
	@NamedQuery(name="Imago.All",query="from Imago m where m.version is null and m.closeDate is null order by m.name"),
	@NamedQuery(name="Imago.AllCount",query="select count(m) from Imago m where m.version is null and m.closeDate is null"),
	@NamedQuery(name="Imago.Search",query="from Imago o where o.version is null and o.closeDate is null and (o.name like :Pref) order by o.name"),
	@NamedQuery(name="Imago.SearchCount",query="select count(o) from Imago o where o.version is null and o.closeDate is null and (o.name like :Pref)")
})
public class Imago extends Relator implements Serializable{

	private static final long serialVersionUID = 4097008221436065082L;

}
