package org.itx.jbalance.l0.o;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries({
	@NamedQuery(name="Aunit.All",query="from Aunit o where o.version is null and o.closeDate is null order by o.code"),
	@NamedQuery(name="Aunit.AllCount",query="select count(o) from Aunit o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="Aunit.Search",query="from Aunit o where o.version is null and o.closeDate is null and (o.name like :Pref or o.code like :Pref or o.barCode like :Pref) order by o.code"),
	@NamedQuery(name="Aunit.SearchCount",query="select count(o) from Aunit o where o.version is null and o.closeDate is null and (o.name like :Pref or o.code like :Pref or o.barCode like :Pref) order by o.code")
})
public class Aunit extends Relator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8954519206462767324L;

	private String Props;
	
	private String Code;

	private Boolean NewOffer;
	
	public String getProps() {
		return Props;
	}

	public void setProps(String props) {
		Props = props;
	}

	public Boolean getNewOffer() {
		return NewOffer;
	}

	public void setNewOffer(Boolean newOffer) {
		NewOffer = newOffer;
	}

	public String getCode() {
		return Code;
	}

	public void setCode(String code) {
		Code = code;
	}
}
