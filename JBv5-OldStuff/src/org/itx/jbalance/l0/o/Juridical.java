package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;



/**
 * @author  dima
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Juridical.All",query="from Juridical o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Juridical.AllCount",query="select count(o) from Juridical o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="Juridical.Search",query="from Juridical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.JAddress like :Pref or o.PAddress like :Pref) order by o.name"),
	//@NamedQuery(name="Juridical.SearchCount",query="select count(o) from Juridical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref) order by o.name")
	@NamedQuery(name="Juridical.SearchCount",query="select count(o) from Juridical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.JAddress like :Pref or o.PAddress like :Pref)"),
	
	@NamedQuery(name="Juridical.SearchSameName", query="from Juridical o where o.version is null and o.closeDate is null and (lower(o.name) like lower(:Name)) order by o.name"),
	
	@NamedQuery(name="Juridical.AllDeleted",query="from Juridical o where o.version is null and o.closeDate is not null order by o.name"),
	@NamedQuery(name="Juridical.SearchDeleted",query="from Juridical o where o.version is null and o.closeDate is not null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.JAddress like :Pref or o.PAddress like :Pref) order by o.name"),
	
	@NamedQuery(name="Juridical.AllDeletedCount",query="select count(o) from Juridical o where o.version is null and o.closeDate is not null"),
	@NamedQuery(name="Juridical.SearchDeletedCount",query="select count(o) from Juridical o where o.version is null and o.closeDate is not null and (o.name like :Pref or o.INN like :Pref or o.barCode like :Pref or o.JAddress like :Pref or o.PAddress like :Pref)")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Juridical extends Contractor implements Serializable {
	/**
	 * 
	 */ 
	private static final long serialVersionUID = -1722455974258219396L;

	/**
	 * 
	 */

	private String JAddress;
	
	//private String region;


	private Long OKONH;

	private Long OKPO;

	//private int regionId;
	/**
	 * @return  Returns the jAddress.
	 * @uml.property  name="jAddress"
	 */
	public String getJAddress() {
		return JAddress;
	}

	/**
	 * @param jAddress  The jAddress to set.
	 * @uml.property  name="jAddress"
	 */
	public void setJAddress(String address) {
		JAddress = address;
	}

	/**
	 * @return  Returns the region.
	 * @uml.property  name="region"
	 */
//	public String getRegion() {
//		return region;
//	}
	
	/**
	 * @param region  The region of a city to set.
	 * @uml.property  name="region"
	 */	
//	public void setRegion(String region) {
//		this.region = region;
//	}
	
	/**
	 * @return  Returns the oKONH.
	 * @uml.property  name="oKONH"
	 */
	public Long getOKONH() {
		return OKONH;
	}

	/**
	 * @param oKONH  The oKONH to set.
	 * @uml.property  name="oKONH"
	 */
	public void setOKONH(Long okonh) {
		OKONH = okonh;
	}

	/**
	 * @return  Returns the oKPO.
	 * @uml.property  name="oKPO"
	 */
	public Long getOKPO() {
		return OKPO;
	}

	/**
	 * @param oKPO  The oKPO to set.
	 * @uml.property  name="oKPO"
	 */
	public void setOKPO(Long okpo) {
		OKPO = okpo;
	}

//	public void setRegionId(int regionId) {
//		this.regionId = regionId;
//	}
//
//	public int getRegionId() {
//		return regionId;
//	}

}
