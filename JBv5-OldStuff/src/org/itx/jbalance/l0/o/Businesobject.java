package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;




/**
 * Это то, что не укладывается в рамки текущей бизнес модели!
 * Этот класс нужен, чтобы ПОТОМ сюда что-то вынести!
 * не правильно!
 * Это не красиво!
 * 
 * @author apv
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Businesobject.All",query="from Businesobject o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Businesobject.AllCount",query="select count(o) from Businesobject o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Businesobject.Search",query="from Businesobject o where o.version is null and o.closeDate is null and (o.name like :Pref or o.barCode like :Pref) order by o.name"),
	@NamedQuery(name="Businesobject.SearchCount",query="select count(o) from Businesobject o where o.version is null and o.closeDate is null and (o.name like :Pref or o.barCode like :Pref) order by o.name")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Businesobject extends Relator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9093754579724645465L;

}
