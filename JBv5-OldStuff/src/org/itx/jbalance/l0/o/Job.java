package org.itx.jbalance.l0.o;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
@NamedQueries( { 
	@NamedQuery(name = "Job.All", query = "from Job j where j.version is null and j.closeDate is null order by j.name"),
	@NamedQuery(name = "Job.Search",query="from Job j where j.version is null and j.closeDate is null and j.name like :Pref order by j.name")
	})
@PrimaryKeyJoinColumn(name = "UId")
public class Job extends Businesobject implements Serializable{
	
	private static final long serialVersionUID = -2648925017856086777L;
	
	/**
	 * 0 - superuser
	 * 1 - privelegeduser
	 * 2 - user
	 */
	private Byte typeuser;

	public Byte getTypeuser() {
		return typeuser;
	}

	public void setTypeuser(Byte typeuser) {
		this.typeuser = typeuser;
	}
	
	

}
