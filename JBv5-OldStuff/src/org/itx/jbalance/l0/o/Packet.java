/**
 * 
 */
package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * @author galina
 *
 */
@Entity
@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries({
	@NamedQuery(name="Packet.All",query="from Packet o where o.version is null and o.closeDate is null order by o.code"),
	@NamedQuery(name="Packet.AllCount",query="select count(o) from Packet o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="Packet.Search",query="from Packet o where o.version is null and o.closeDate is null and (o.name like :Pref or o.code like :Pref or o.barCode like :Pref) order by o.code"),
	@NamedQuery(name="Packet.SearchCount",query="select count(o) from Packet o where o.version is null and o.closeDate is null and (o.name like :Pref or o.code like :Pref or o.barCode like :Pref)")
})
public class Packet extends Aunit implements Serializable{

	private static final long serialVersionUID = 9151413216530872231L;

}
