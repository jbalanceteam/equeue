package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@NamedQueries( { 
	@NamedQuery(name = "Group_.All", query = "from Group_ j where j.version is null and j.closeDate is null order by j.name"),
	@NamedQuery(name = "Group_.Search",query="from Group_ j where j.version is null and j.closeDate is null and j.name like :Pref order by j.name")
	})
@PrimaryKeyJoinColumn(name = "UId")
public class Group_ extends Businesobject implements Serializable{
	private static final long serialVersionUID = -2648925017856086777L;
}
