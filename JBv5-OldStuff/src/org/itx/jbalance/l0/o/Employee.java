package org.itx.jbalance.l0.o;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;


/**
 * @author  dima
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Employee.All",query="from Employee o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Employee.AllCount",query="select count(o) from Employee o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="Employee.Search",query="from Employee o where o.version is null and o.closeDate is null and (o.name like :Pref or o.workTab like :Pref or o.barCode like :Pref) order by o.name"),
	@NamedQuery(name="Employee.SearchCount",query="select count(o) from Employee o where o.version is null and o.closeDate is null and (o.name like :Pref or o.workTab like :Pref or o.barCode like :Pref) order by o.name")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Employee extends Physical implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7623046226927752468L;
	/**
	 * 
	 */

	private String WorkTab;

	/**
	 * @return  Returns the workTab.
	 * @uml.property  name="workTab"
	 */
	public String getWorkTab() {
		return WorkTab;
	}

	/**
	 * @param workTab  The workTab to set.
	 * @uml.property  name="workTab"
	 */
	public void setWorkTab(String workTab) {
		WorkTab = workTab;
	}

}
