/**
 * 
 */
package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * @author galina
 *
 */
@Entity
@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries({
	@NamedQuery(name="Measurement.All",query="from Measurement m where m.version is null and m.closeDate is null order by m.name"),
	@NamedQuery(name="Measurement.AllCount",query="select count(m) from Measurement m where m.version is null and m.closeDate is null"),
	@NamedQuery(name="Measurement.Search",query="from Measurement o where o.version is null and o.closeDate is null and (o.name like :Pref) order by o.name"),
	@NamedQuery(name="Measurement.SearchCount",query="select count(o) from Measurement o where o.version is null and o.closeDate is null and (o.name like :Pref)")
})
public class Measurement extends Relator implements Serializable{

	private static final long serialVersionUID = 4097008221436065082L;

}
