package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@NamedQueries( { 
	@NamedQuery(name = "Kindofprice.All", query = "from Kindofprice o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name = "Kindofprice.AllCount", query = "select count(o) from Kindofprice o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Kindofprice.Search",query="from Kindofprice o where o.version is null and o.closeDate is null and (o.description like :Pref or o.shortName like :Pref or o.name like :Pref) order by o.name"),
	@NamedQuery(name="Kindofprice.SearchCount",query="select count(o) from Kindofprice o where o.version is null and o.closeDate is null and (o.description like :Pref or o.shortName like :Pref or o.name like :Pref) order by o.name")
	})
@PrimaryKeyJoinColumn(name = "UId")
public class Kindofprice extends Businesobject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2454916018411637328L;

	private String ShortName;

	private String Description;

	public String getShortName() {
		return ShortName;
	}

	public void setShortName(String shortName) {
		ShortName = shortName;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

}
