package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.validator.NotNull;
import org.itx.jbalance.l0.Ubiq;

/**
 * @author  dima
 */
@Entity
@NamedQueries({
	@NamedQuery(name="Relator.All",query="from Relator o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Relator.AllCount",query="select count(o) from Relator o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="Relator.Search",query="from Relator o where o.version is null and o.closeDate is null and (o.name like :Pref or o.barCode like :Pref) order by o.name"),
	@NamedQuery(name="Relator.SearchCount",query="select count(o) from Relator o where o.version is null and o.closeDate is null and (o.name like :Pref or o.barCode like :Pref) order by o.name")
})
@PrimaryKeyJoinColumn(name = "UId")
public class Relator extends Ubiq implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4171292306687715913L;
	/**
	 * 
	 */

	private String Name;

	
	private String description;

	/**
	 * @return  Returns the name.
	 * @uml.property  name="name"
	 */
	@NotNull
	public String getName() {
		return Name;
	}

	/**
	 * @param name  The name to set.
	 * @uml.property  name="name"
	 */
	public void setName(String name) {
		Name = name;
	}
	
	@Override
	public String toString() {
		return Name;
	}
	
	
	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
