package org.itx.jbalance.l0.o;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;

import org.itx.jbalance.l0.Age;
import org.itx.jbalance.l0.AgeCalculator;


/**
 * @author  dima
 */
@Entity
@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries({
	@NamedQuery(name="Physical.All",query="from Physical o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Physical.AllCount",query="select count(o) from Physical o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="Physical.Search",query="from Physical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.surname like :Pref or o.barCode like :Pref) order by o.name"),
	@NamedQuery(name="Physical.SearchCount",query="select count(o) from Physical o where o.version is null and o.closeDate is null and (o.name like :Pref or o.surname like :Pref or o.barCode like :Pref) order by o.name")
})
public class Physical extends Contractor implements Serializable {

	private static final long serialVersionUID = -4426847307209004511L;

	private String RealName;

	private String Surname; 

	private String Patronymic;
	
	private String Passport;
	
//	private String phone; 
	 
	private String fax;  
	
	private Date birthday;
	
	@Transient
	private Age age;
	
	
	public String getPassport() {
		return Passport;
	}
	public void setPassport(String passport) {
		Passport = passport;
	}
	public String getPatronymic() {
		return Patronymic;
	}
	public void setPatronymic(String patronymic) {
		Patronymic = patronymic;
	}

	public String getRealName() {
		return RealName;
	}

	public void setRealName(String realName) {
		RealName = realName;
	}

	public String getSurname() {
		return Surname;
	}

	public void setSurname(String surname) {
		Surname = surname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		age = AgeCalculator.calculateAge(birthday, new Date());
		this.birthday = birthday;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Transient
	public Age getAge(){
		return age;
	} 
	
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	
	
	
	
	
	
}
