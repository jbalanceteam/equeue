package org.itx.jbalance.l0.o;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
@NamedQueries( { 
	@NamedQuery(name = "Planaccount.All", query = "from Planaccount j where j.version is null and j.closeDate is null order by j.number_accounts,j.number_subaccounts,number_subcontra"),
	@NamedQuery(name = "Planaccount.Search",query="from Planaccount j where j.version is null and j.closeDate is null and j.name like :Pref order by j.name"),
	@NamedQuery(name = "Planaccount.AllAccounts", query = "from Planaccount j where j.number_subaccounts is null and j.number_subcontra is null and j.version is null and j.closeDate is null order by j.number_accounts,j.number_subaccounts,number_subcontra"),
	
	@NamedQuery(name = "Planaccount.getByNumberAccounts", query = "from Planaccount j where j.number_accounts = (:NUMBER) and j.number_subaccounts is null and j.number_subcontra is null and j.version is null and j.closeDate is null order by j.number_accounts,j.number_subaccounts,number_subcontra"),
	@NamedQuery(name = "Planaccount.getByNumberSubAccounts", query = "from Planaccount j where j.number_accounts = (:NUMBER) and j.number_subaccounts = (:SUB_NUMBER) and j.number_subcontra is null and j.version is null and j.closeDate is null order by j.number_accounts,j.number_subaccounts,number_subcontra"),
	@NamedQuery(name = "Planaccount.getByNumberSubContra", query = "from Planaccount j where j.number_accounts = (:NUMBER) and j.number_subaccounts = (:SUB_NUMBER) and j.number_subcontra = (:SUB_CONTRA) and j.version is null and j.closeDate is null order by j.number_accounts,j.number_subaccounts,number_subcontra")
	})
@PrimaryKeyJoinColumn(name = "UId")
public class Planaccount extends Businesobject implements Serializable{
	
	private static final long serialVersionUID = -2648925017856086777L;
	
	private Integer number_accounts;
	
	private Integer number_subaccounts;
	
	private Integer number_subcontra;
	
	private String description;

	public Integer getNumber_accounts() {
		return number_accounts;
	}

	public void setNumber_accounts(Integer numberAccounts) {
		number_accounts = numberAccounts;
	}

	public Integer getNumber_subaccounts() {
		return number_subaccounts;
	}

	public void setNumber_subaccounts(Integer numberSubaccounts) {
		number_subaccounts = numberSubaccounts;
	}

	public Integer getNumber_subcontra() {
		return number_subcontra;
	}

	public void setNumber_subcontra(Integer numberSubcontra) {
		number_subcontra = numberSubcontra;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public String toString(){
		String str = "";
		if(getNumber_accounts() != null)
			str += getNumber_accounts().toString();
		if(getNumber_subaccounts() != null)
			str += getNumber_subaccounts().toString();
		if(getNumber_subcontra() != null)
			str += getNumber_subcontra().toString();
		return str;
	}

	
	

}
