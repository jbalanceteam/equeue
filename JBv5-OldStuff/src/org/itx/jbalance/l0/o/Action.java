package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

/**
 * Грубо говоря, это пункт меню
 */
@Entity
@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries( { 
	@NamedQuery(name = "Action.All", query = "from Action o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name = "Action.AllCount", query = "select count(o) from Action o where o.version is null and o.closeDate is null order by o.name"),
	@NamedQuery(name="Action.Search",query="from Action o where o.version is null and o.closeDate is null and (o.name like :Pref or o.url like :Pref or o.description like :Pref) order by o.name"),
	@NamedQuery(name="Action.SearchCount",query="select count(o) from Action o where o.version is null and o.closeDate is null and (o.name like :Pref or o.url like :Pref or o.description like :Pref) order by o.name"),
	@NamedQuery(name="Action.ByUrl",query="from Action o where o.version is null and o.closeDate is null and o.url like :Pref")
	})
public class Action extends Businesobject implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8517372335814816535L;

	private String Image;

	private String Url;

	private String Description;

	private String Help;

	public String getImage() {
		return Image;
	}

	public void setImage(String image) {
		Image = image;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getHelp() {
		return Help;
	}

	public void setHelp(String help) {
		Help = help;
	}

	public String getUrl() {
		return Url;
	}

	public void setUrl(String url) {
		Url = url;
	}

}
