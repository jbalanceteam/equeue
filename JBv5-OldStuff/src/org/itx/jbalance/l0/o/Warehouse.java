package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries({
	@NamedQuery(name="Warehouse.All",query="from Warehouse w where w.version is null and w.closeDate is null order by w.warehouseNumber"),
	@NamedQuery(name="Warehouse.getByZero",query="from Warehouse w where w.version is null and w.closeDate is null and w.zero = true order by w.warehouseNumber"),
	@NamedQuery(name="Warehouse.AllCount",query="select count(o) from Warehouse w where w.version is null and w.closeDate is null order by w.warehouseNumber"),
	@NamedQuery(name="Warehouse.Search",query="from Warehouse o where o.version is null and o.closeDate is null and  ( o.warehouseNumber = :Pref or o.name like :Pref1 ) order by o.name"),
	@NamedQuery(name="Warehouse.SearchCount",query="select count(o) from Warehouse o where o.version is null and o.closeDate is null and  ( o.warehouseNumber = :Pref or o.name like :Pref1 ) order by o.name")
})
public class Warehouse extends Businesobject implements Serializable {

	private static final long serialVersionUID = 9147378451328430865L;
	
	private Long WarehouseNumber;
	
	private Boolean zero;


	public Boolean getZero() {
		return zero;
	}

	public void setZero(Boolean zero) {
		this.zero = zero;
	}

	public Long getWarehouseNumber() {
		return WarehouseNumber;
	}

	public void setWarehouseNumber(Long number) {
		WarehouseNumber = number;
	}

}
