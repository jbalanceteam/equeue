package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
@NamedQueries({ 
	@NamedQuery(name = "Ware.All", query = "from Ware o where o.version is null and o.closeDate is null order by o.code"), 
	@NamedQuery(name = "Ware.AllCount", query = "select count(o) from Ware o where o.version is null and o.closeDate is null"), 
	@NamedQuery(name = "Ware.Search",query="from Ware o where o.version is null and o.closeDate is null and (o.name like :Pref or o.code like :Pref or o.barCode like :Pref) order by o.code"),
	@NamedQuery(name = "Ware.SearchCount",query="select count(o) from Ware o where o.version is null and o.closeDate is null and (o.name like :Pref or o.code like :Pref or o.barCode like :Pref) ")
		})
@PrimaryKeyJoinColumn(name = "UId")
public class Ware extends Aunit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2454916018411637328L;
	
	private byte[] Image;
	
	private String ImageContentType;
	
	private String Description;

	@Lob
	public byte[] getImage() {
		return Image;
	}

	public void setImage(byte[] image) {
		Image = image;
	}

	public String getImageContentType() {
		return ImageContentType;
	}

	public void setImageContentType(String imageContentType) {
		ImageContentType = imageContentType;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}
	

}
