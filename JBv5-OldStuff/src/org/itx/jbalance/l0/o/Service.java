package org.itx.jbalance.l0.o;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;


@Entity
@NamedQueries( { 
	@NamedQuery(name = "Service.All", query = "from Service o where o.version is null and o.closeDate is null order by o.code"),
	@NamedQuery(name = "Service.AllCount", query = "select count(o) from Service o where o.version is null and o.closeDate is null order by o.code"),
	@NamedQuery(name="Service.Search",query="from Service o where o.version is null and o.closeDate is null and (o.name like :Pref or o.code like :Pref or o.barCode like :Pref) order by o.code"),
	@NamedQuery(name="Service.SearchCount",query="select count(o) from Service o where o.version is null and o.closeDate is null and (o.name like :Pref or o.code like :Pref or o.barCode like :Pref) order by o.code")
	})
@PrimaryKeyJoinColumn(name = "UId")
public class Service extends Aunit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2454916018411637328L;

}
