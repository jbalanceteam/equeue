package org.itx.sphinx.equeue;

import java.util.List;

import javax.naming.NamingException;

import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.StaffDivision;
import org.itx.jbalance.l0.s.SStaffDivision;
import org.itx.jbalance.l1.d.HStaffDivisionsRemote;
import org.itx.jbalance.l1.o.DousRemote;

public class CRUDIntegrationTestHStaffDivision  extends  CRUDIntegrationTest {
	@org.junit.Test
	  public void testAuthenticate(){
		try{
			printAll();
			createOne();
			printAll();
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}



	private void createOne() throws NamingException {
		System.out.println("======== Create One ======");
		HStaffDivisionsRemote doc_l1 = getStaffDivision();
		HStaffDivision p = new HStaffDivision();
	
		DousRemote dou_l1 = getDou();
		dou_l1.getAll(Dou.class).get(0);
		Dou dou=dou_l1.getAll(Dou.class).get(0);
		p.setDou(dou);
		p.setContractorOwner(dou);
		p.setFoundation("Основание");
		
	
		p = doc_l1.create(p);
		
		SStaffDivision sLine = new SStaffDivision();
		StaffDivision division = getStaffDivisions().getAll(StaffDivision.class).get(0);
		sLine .setDivision(division);
		
		
		doc_l1.createSpecificationLine(sLine, p);
		doc_l1.createSpecificationLine(sLine, p);
		doc_l1.delete(p);
	}




	private void printAll() throws NamingException {
		System.out.println("======== printAll ======");
		HStaffDivisionsRemote l1 = getStaffDivision();
		
		List<HStaffDivision> all = (List<HStaffDivision>)l1.getAll(HStaffDivision.class);
		for (HStaffDivision doc : all) {
			System.out.println(doc);
			List specificationLines = l1.getSpecificationLines(doc);
			for (Object object : specificationLines) {
				System.out.println("   ===>"+object);
			}
		}
	}



	
}
