package org.itx.sphinx.equeue;

import java.util.List;

import javax.naming.NamingException;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.PhysicalsRemote;

public class CRUDIntegrationTestHDouRequest extends CRUDIntegrationTest{
	@org.junit.Test
	  public void testAuthenticate(){
		try{
//			printAll();
//			createOne();
//			printAll();
			search();
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}



	private void search() throws NamingException {
		HDouRequestsRemote doc_l1 = getHDouRequest();
		List<HDouRequest> search = doc_l1.search(new DouRequestsSearchParams());
		for (HDouRequest hDouRequest : search) {
			System.out.println(hDouRequest);
		}
	}



	private void createOne() throws NamingException {
		System.out.println("======== Create One ======");
		HDouRequestsRemote doc_l1 = getHDouRequest();

		HDouRequest p=new HDouRequest();

		Physical child = new Physical();
		child.setName("Вася");
		
		PhysicalsRemote physicalsEjb = getPhysicals();
		child = physicalsEjb.create(child);

		
		p.setChild(child);
		p.setComments("тест");
		p.setFoundation("Основание");

		p = doc_l1.create(p);

		DousRemote dou = getDou();
		List<? extends Dou> all = dou.getAll(Dou.class);
		Dou d = all.get(0);

		SDouRequest sLine = new SDouRequest();
		sLine .setArticleUnit(d);


		sLine = doc_l1.createSpecificationLine(sLine , p);
		sLine = doc_l1.createSpecificationLine(sLine , p);

	}




	private void printAll() throws NamingException {
		System.out.println("======== printAll ======");
		HDouRequestsRemote l1 = getHDouRequest();
		
		List<HDouRequest> all = (List<HDouRequest>)l1.getAll(HDouRequest.class);
		for (HDouRequest doc : all) {
			System.out.println(doc);
			List specificationLines = l1.getSpecificationLines(doc);
			for (Object object : specificationLines) {
				System.out.println("   ===>"+object);
			}
		}
	}

}
