package org.itx.sphinx.equeue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.d.HRegisterRemote;

public class CRUDIntegrationTestHRegister extends  CRUDIntegrationTest {
	@org.junit.Test
	  public void testAuthenticate(){
		try{
			printAll();
			
			for(int i=1; i< 200;i++)
			createOne();
			printAll();
//			search();
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}

	
	private void printAll() throws NamingException {
		System.out.println("======== printAll ======");
		HRegisterRemote doc_l1 = getHRegisterRemote();
		List<HRegister> all =(List<HRegister>) doc_l1.getAll(HRegister.class);
		for (HRegister doc : all) {
			System.out.println(doc);
			List specificationLines = doc_l1.getSpecificationLines(doc);
			for (Object object : specificationLines) {
				System.out.println("   ===>"+object);
			}
		}
	}





	private void createOne() throws NamingException {
		System.out.println("======== Create One ======");
		HRegisterRemote doc_l1 = getHRegisterRemote();

		HRegister p=new HRegister();
		p.setFoundation(RandomDataGenerator.randomSentance());
//		Physical child = new Physical();
//		child.setName("Вася");
		
		p.setMember1(RandomDataGenerator.randomName());
		p.setMember2(RandomDataGenerator.randomName());
		p.setMember3(RandomDataGenerator.randomName());
		
		try {
			p.setRegisterDate(RandomDataGenerator.randomDateBetween(new SimpleDateFormat("yyyy-MM-dd").parse("2011-01-01"), new Date()));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		p= doc_l1.create(p);
		
		Dou dou = getDou().getAll(Dou.class).get(0);
		
		HDouRequestsRemote hDouRequest = getHDouRequest();
		List<? extends HDouRequest> allReq = hDouRequest.getAll(HDouRequest.class);
		
		
		
		
		for (HDouRequest r : allReq) {
			
			if(Math.random() > 0.6)
				continue;
			
			SRegister s=new SRegister();
			
			double random = Math.random();
			if(random<0.25)
				s.setRnAction(Action.GIVE_PERMIT);
			else if(random>=0.25 && random < 0.5)
				s.setRnAction(Action.BACK_TO_QUEUE);
			else if(random>=0.5 && random < 0.75)
				s.setRnAction(Action.DELETE_BY_7_YEAR);
			else if(random>=0.75 && random < 0.8)
				s.setRnAction(Action.REFUSAL);
			else 
				s.setRnAction(Action.DELETE_BY_PARRENTS_AGREE);
			s.setDou(dou);
			s.setDouRequest(r);
			s= doc_l1.createSpecificationLine(s, p);

			
			if(Math.random() > 0.9)
				break;
			
		}
		
	}



}
