package org.itx.sphinx.equeue;

import java.util.List;

import javax.naming.NamingException;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l1.o.PrivilegesRemote;

public class CRUDIntegrationTestPrivileges extends CRUDIntegrationTest {
	@org.junit.Test
	  public void testAuthenticate(){
		try{
//			System.out.println(">>>Before creation<<<");
//			printAll();
//			Privilege p = createOne();
//			System.out.println("\n\n>>>After creation<<<");
//			printAll();
//			p.setName("new Name");
//			p=update(p);
//			System.out.println("\n\n>>>After update<<<");
//			printAll();
//			delete(p);
			createList();
//			System.out.println("\n\n>>>After delete<<<");
			printAll();
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}



	/**
	 * @throws NamingException
	 */
	private void createList() throws NamingException {
		String list[]={"Инвалиды и их дети",
		"Судьи",
		"Чернобыль",
		"Прокуратура и след",
		"Военнослуж",
		"Полиция",
		"Нарко",
		"Многодетные"};
		System.out.println("======== Create One ======");
		PrivilegesRemote privilages = getPrivilegesRemote();
		for (String string : list) {
			Privilege p = new Privilege();
			p.setName(string);
			p.setDescription(string);
			p = privilages.create(p);
		}

	}



	private void delete(Ubiq createOne) throws NamingException {
		PrivilegesRemote privilages = getPrivilegesRemote();
		privilages.delete(createOne.getUId());
	}
	
	
	private Privilege update(Privilege p) throws NamingException {
		PrivilegesRemote privilages = getPrivilegesRemote();
		return privilages.update(p);
	}



	private Privilege createOne() throws NamingException {
		System.out.println("======== Create One ======");
		PrivilegesRemote privilages = getPrivilegesRemote();
		Privilege p = new Privilege();
		p.setName("Депутат");
		p.setDescription("Избранник народа");
		return privilages.create(p);
	}




	private void printAll() throws NamingException {
		System.out.println("======== printAll ======");
		PrivilegesRemote privilages = getPrivilegesRemote();
		List<Privilege> all = (List<Privilege>)privilages.getAll(Privilege.class);
		for (Privilege privilage : all) {
			System.out.println(privilage);
		}
	}

	
	
	
	
}
