package org.itx.sphinx.equeue;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class RandomDataGenerator {
 private RandomDataGenerator(){
	 
 }
 
 static String article[] = { "the", "a", "one", "some", "any" };
 // define noun, verb, preposition in same way here
 static String noun[]={"boy","dog","car","bicycle"};
 static String verb[]={"ran","jumped","sang","moves"};
 static String preposition[]={"away","towards","around","near"};
 
 public static String randomSentance(){
	 Random generator = new Random();
	 

	 int article1 = generator.nextInt( article.length );

	 int noun1 = generator.nextInt( noun.length );
	 int verb1 = generator.nextInt( verb.length );
	 int preposition1 =generator.nextInt(preposition.length);
	 int article2=generator.nextInt(article.length);
	 int noun2=generator.nextInt(noun.length);
	 StringBuilder buffer = new StringBuilder();
	 // concatenate words and add period
	 buffer
	 	.append(article[article1]).append(" ")
	 	.append(noun[noun1]).append(" ")
	 	.append	 (verb[verb1]).append(" ")
	 	.append(preposition[preposition1]).append(" ")
	 	.append(article[article2]).append(" ")
	 	.append(noun[noun2]);

	 buffer.setCharAt( 0, Character.toUpperCase( buffer.charAt( 0 ) ) );

	 return buffer.toString();
	 
 }
 
 
 
 static String firstnames[]={"Иван","Тимур","Николай","Семен","Кирилл","Дмитрий","Федор"};
 static String lastnames[]={"Васютин","Синичкин","Путин","Комаров","Пчеловодов","Романов","Сергеев"};
 static String middlenames[]={"Петрович","Авдеевич","Викторович","Павлович","Артемович","Леонидович"};
 
 public static String randomName(){
Random generator = new Random();
	 

	 int fn = generator.nextInt( firstnames.length );

	 int ln = generator.nextInt( lastnames.length );
	 int mn = generator.nextInt( middlenames.length );
	
	 StringBuilder buffer = new StringBuilder();
	 // concatenate words and add period
	 buffer
	 	.append(lastnames[fn]).append(" ")
	 	.append(firstnames[ln]).append(" ")
	 	.append	 (middlenames[mn]);

	 return buffer.toString();
 }
 
 
 public static Date randomDateBetween(Date from, Date to) {
	 	        Calendar cal = Calendar.getInstance();
	 	 
	 	        cal.setTime(from);
	 	        BigDecimal decFrom = new BigDecimal(cal.getTimeInMillis());
	 	 
	 	        cal.setTime(to);
	 	        BigDecimal decTo = new BigDecimal(cal.getTimeInMillis());
	 	 
	 	        BigDecimal selisih = decTo.subtract(decFrom);
	 	        BigDecimal factor = selisih.multiply(new BigDecimal(Math.random()));
	 	 
	 	        return new Date((factor.add(decFrom)).longValue());
	 	    }
 
}
