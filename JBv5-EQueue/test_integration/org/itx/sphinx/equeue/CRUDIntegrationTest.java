package org.itx.sphinx.equeue;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.security.auth.login.LoginContext;

import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.d.HRegisterRemote;
import org.itx.jbalance.l1.d.HStaffDivisionsRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.PhysicalsRemote;
import org.itx.jbalance.l1.o.PrivilegesRemote;
import org.itx.jbalance.l1.o.RegionsRemote;
import org.itx.jbalance.l1.o.StaffDivisionsRemote;
import org.jboss.security.auth.callback.UsernamePasswordHandler;

public abstract class CRUDIntegrationTest {
	
	private DousRemote dousRemote;
	private PhysicalsRemote physicalsRemote;
	private HRegisterRemote hRegisterRemote;
	private HDouRequestsRemote douRequestsRemote;
	private StaffDivisionsRemote staffDivisionsRemote;
	private PrivilegesRemote privilegesRemote;
	private HStaffDivisionsRemote hStaffDivisionsRemote;
	private RegionsRemote regionsRemote;
		
	protected final DousRemote getDou() throws NamingException {
		
		if(dousRemote!=null)
			return dousRemote;
		Context context = getContext();
		dousRemote = (DousRemote) context.lookup(Constants.JNDI_DOUS_REMOTE);
		return dousRemote;
	}
	
	
	protected final HStaffDivisionsRemote getHStaffDivisionsRemote() throws NamingException {
		if(hStaffDivisionsRemote!=null)
			return hStaffDivisionsRemote;
		Context context = getContext();
		hStaffDivisionsRemote = (HStaffDivisionsRemote ) context.lookup(Constants.JNDI_HSTAFFDIVISIONS_REMOTE);
		return hStaffDivisionsRemote;
	}
	
	
	protected final RegionsRemote getRegionsRemote() throws NamingException {
		if(regionsRemote!=null)
			return regionsRemote;
		Context context = getContext();
		regionsRemote = (RegionsRemote ) context.lookup(Constants.JNDI_REGIONS_REMOTE);
		return regionsRemote;
	}

	
	
	protected final PhysicalsRemote getPhysicals() throws NamingException {
		if(physicalsRemote!=null)
			return physicalsRemote;
		Context context = getContext();
		physicalsRemote = (PhysicalsRemote) context.lookup("Physicals/remote");
		return physicalsRemote;
	}
	protected final HRegisterRemote getHRegisterRemote() throws NamingException {
		if(hRegisterRemote!=null)
			return hRegisterRemote;
		Context context = getContext();
		hRegisterRemote = (HRegisterRemote) context.lookup(Constants.JNDI_HREGISTER_REMOTE);
		return hRegisterRemote;
	}
	
	protected final HDouRequestsRemote getHDouRequest() throws NamingException {
		if(douRequestsRemote!=null)
			return douRequestsRemote;
		Context context = getContext();
		douRequestsRemote = (HDouRequestsRemote) context.lookup(Constants.JNDI_HDOUREQUESTS_REMOTE);
		return douRequestsRemote;
	}

	
	protected final StaffDivisionsRemote getStaffDivisions() throws NamingException {
		if(staffDivisionsRemote!=null)
			return staffDivisionsRemote;
		Context context = getContext();
		staffDivisionsRemote = (StaffDivisionsRemote) context.lookup(Constants.JNDI_STAFFDIVISIONS_REMOTE);
		return staffDivisionsRemote;
	}

	
	protected final  PrivilegesRemote getPrivilegesRemote() throws NamingException {
		if(privilegesRemote!=null)
			return privilegesRemote;
		Context context=getContext();
		privilegesRemote = (PrivilegesRemote) context.lookup(Constants.JNDI_PRIVILEGES_REMOTE);
		return privilegesRemote;
	}
	
	protected final HStaffDivisionsRemote getStaffDivision() throws NamingException {
		Context context = getContext();
		HStaffDivisionsRemote HStaffDivisionss = (HStaffDivisionsRemote) context.lookup(Constants.JNDI_HSTAFFDIVISIONS_REMOTE);
		return HStaffDivisionss;
	}
	
	protected final RegionsRemote getRegions() throws NamingException {
		Context context = getContext();
		RegionsRemote regionsRemote  = (RegionsRemote) context.lookup(Constants.JNDI_REGIONS_REMOTE);
		return regionsRemote;
	}
	
	

	private final Context getContext() throws NamingException {
		System.out.println("Try lookup");
		Hashtable<String,String> hashtable = new Hashtable<String,String>();

		
		UsernamePasswordHandler handler = new UsernamePasswordHandler("admin", "11".toCharArray());
		System.setProperty("java.security.auth.login.config", 
				"/home/apv/projects/sphinx/Queue/JBv5-EQueue/resources/jaas.config");
		try {
			LoginContext lc = new LoginContext("client-login", handler);
			lc.login();
		} catch (Exception e) {
			// TODO: handle exception
		}

		
        hashtable.put("java.naming.factory.url.pkgs",
            "org.jboss.naming rg.jnp.interfaces");
 		hashtable.put("java.naming.factory.initial","org.jnp.interfaces.NamingContextFactory");
		hashtable.put("java.naming.provider.url","jnp://localhost:1099");
		hashtable.put("java.naming.factory.url.pkgs","org.jnp.interfaces");
		Context context=new InitialContext(hashtable);
		return context;
	}
	
	
	
}
