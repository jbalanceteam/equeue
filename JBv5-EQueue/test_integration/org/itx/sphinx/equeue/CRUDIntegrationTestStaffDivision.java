package org.itx.sphinx.equeue;

import java.util.List;

import javax.naming.NamingException;

import org.itx.jbalance.l0.o.StaffDivision;
import org.itx.jbalance.l1.o.StaffDivisionsRemote;

public class CRUDIntegrationTestStaffDivision extends CRUDIntegrationTest{
	@org.junit.Test
	  public void testAuthenticate(){
		try{
			printAll();
			createOne();
			printAll();
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}



	private void createOne() throws NamingException {
		System.out.println("======== Create One ======");
		StaffDivisionsRemote privilages = getStaffDivisions();
		StaffDivision p = new StaffDivision();
		p.setName("Ясли");
		p.setDescription("Совсем малыши");
		p = privilages.create(p);
	}




	private void printAll() throws NamingException {
		System.out.println("======== printAll ======");
		StaffDivisionsRemote privilages = getStaffDivisions();
		List<StaffDivision> all = (List<StaffDivision>)privilages.getAll(StaffDivision.class);
		for (StaffDivision privilage : all) {
			System.out.println(privilage);
		}
	}

	
	
	
	
}
