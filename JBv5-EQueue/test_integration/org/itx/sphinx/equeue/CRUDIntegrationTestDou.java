package org.itx.sphinx.equeue;

import java.util.List;

import javax.naming.NamingException;

import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l1.d.HStaffDivisionsRemote;
import org.itx.jbalance.l1.o.DousRemote;

public class CRUDIntegrationTestDou extends  CRUDIntegrationTest {
	@org.junit.Test
	  public void testAuthenticate(){
		try{
			printAll();
			createOne();
			printAll();
			
//			DousRemote service = getDou();
//			HStaffDivisionsRemote staffDivision = getStaffDivision();
//			List<Dou> all = (List<Dou>)service.getAll();
//			for (Dou d : all) {
//				System.out.println(d);
//				HStaffDivision object = new HStaffDivision();
//				object.setDou(d);
//				object.setFoundation(RandomDataGenerator.randomSentance());
//
//				staffDivision.setObject(object);
//				SStaffDivision specificationLine = new SStaffDivision();
//				specificationLine.setDivision(new StaffDivision());
//				staffDivision.setSpecificationLine(specificationLine);
//				staffDivision.create();
//				
//			}
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
	}



	private void createOne() throws NamingException {
		System.out.println("======== Create One ======");
		DousRemote service = getDou();
		
		List<? extends Region> allRegions = getRegions().getAll(Region.class);
		
//	Integer ids[]=	{1, 136, 139, 4, 5, 6, 7, 129, 9, 128, 11, 132, 14, 134, 17, 19, 23, 27, 28, 34, 35, 32, 33, 39, 36, 42, 40, 41, 46, 47, 166, 44, 45, 51, 50, 49, 55, 53, 52, 58, 57, 56, 63, 69, 67, 76, 78, 79, 72, 74, 85, 84, 86, 81, 80, 83, 82, 93, 92, 98, 97, 110, 111, 109, 107, 104, 105, 118, 117, 115, 114, 113, 126, 125, 124, 123, 122, 121, 120};
		Integer ids[]=	{1234};
		
		for(int i=0;i<ids.length;i++){
			int testNum = ids[i];
			Region region = allRegions.get(testNum % allRegions.size());
			Dou p = new Dou();
			p.setName("Садик №"+testNum);
			p.setNumber(testNum + "");
			p.setDescription("Водятся чертята");
			p.setEMail("sad"+testNum+"@edu.ru");
			p.setOKPO(123l);
			p.setINN("123");
			p.setJAddress("Б.Садовая "+testNum);
			
			p.setRegion(region);
			p = service.create(p);
			
			
			HStaffDivisionsRemote hStaffDivisions = getHStaffDivisionsRemote();
			HStaffDivision object = new HStaffDivision();
			object.setDou(p);
			object.setFoundation("");
			object.setInfo("");
			hStaffDivisions.create(object);
		}
	}




	private void printAll() throws NamingException {
		System.out.println("======== printAll ======");
		DousRemote privilages = getDou();
		List<Dou> all = (List<Dou>)privilages.getAll(Dou.class);
		for (Dou privilage : all) {
			System.out.println(privilage);
		}
	}

	
	

}
