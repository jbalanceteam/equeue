package org.itx.jbalance;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.d.HAutoRegisterRemote;
import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l1.d.HRegisterRemote;
import org.itx.jbalance.l1.o.BirthSertificatesRemote;
import org.itx.jbalance.l1.o.ContractorsRemote;
import org.itx.jbalance.l1.o.DOUGroupsRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.PermitsRemote;
import org.itx.jbalance.l1.o.PhysicalsRemote;
import org.itx.jbalance.l1.o.PrivilegesRemote;
import org.itx.jbalance.l1.o.RegionsRemote;
import org.itx.jbalance.l2_api.services.AttendanceService;
import org.itx.jbalance.l2_api.services.RegisterService;
import org.itx.jbalance.l2_api.ws.ExternalWS;
import org.itx.jbalance.l2_api.ws.MfcWS;
import org.junit.Before;

public class AbstractEqueueTestEJB extends AbstractTestEJB{
	
	protected HDouRequestsRemote hDouRequests;
	protected Juridical juridical;
	protected HRegisterRemote hRegisters;
	protected HAutoRegisterRemote hAutoRegisters;
	protected DOUGroupsRemote douGroupsRemote;
	protected MfcWS mfcWS;
	protected ExternalWS externalWS;
	protected DousRemote dousRemote;
	protected RegionsRemote regionsRemote;
	protected PrivilegesRemote privilegesRemote;
	protected PhysicalsRemote physicalsRemote;
	protected BirthSertificatesRemote birthSertificatesRemote;
	protected AttendanceService attendanceService;
	protected RegisterService registerService;
	protected PermitsRemote permits;
	
	@Override
	@Before
	public void before() throws Exception {
		Constants.JNDI_JMS_CONNECTIONFACTORY = "openejb:Resource/ConnectionFactory";
		Constants.JNDI_JMS_DOUCATCHEQUEUE = "openejb:Resource/queue/dousCache";
		super.before();
        contractors		   = (ContractorsRemote)context.lookup("Contractors/remote");
        hDouRequests    = (HDouRequestsRemote)context.lookup(Constants.JNDI_HDOUREQUESTS_REMOTE);
        hRegisters = (HRegisterRemote)context.lookup(Constants.JNDI_HREGISTER_REMOTE);
        hAutoRegisters = (HAutoRegisterRemote)context.lookup(Constants.JNDI_HAUTOREGISTER_REMOTE);
        douGroupsRemote = (DOUGroupsRemote)context.lookup(Constants.JNDI_DOUGROUPS_REMOTE);
        dousRemote = (DousRemote)context.lookup(Constants.JNDI_DOUS_REMOTE);
        regionsRemote = (RegionsRemote)context.lookup(Constants.JNDI_REGIONS_REMOTE);
        privilegesRemote = (PrivilegesRemote)context.lookup(Constants.JNDI_PRIVILEGES_REMOTE);
        birthSertificatesRemote = (BirthSertificatesRemote)context.lookup(Constants.JNDI_BIRTHSERTIFICATES_REMOTE);
        physicalsRemote =  (PhysicalsRemote)context.lookup("Physicals/remote");
        mfcWS = (MfcWS)context.lookup(MfcWS.JNDI_NAME);
        externalWS = (ExternalWS)context.lookup(ExternalWS.JNDI_NAME);
        attendanceService = (AttendanceService)context.lookup(AttendanceService.JNDI_NAME);
        registerService = (RegisterService)context.lookup(RegisterService.JNDI_NAME);
        permits = (PermitsRemote)context.lookup(Constants.JNDI_PERMITS_REMOTE);
	}
	
	public Physical generateChild(String birthday) {
		return physicalsRemote.create(GenerateDataHelper.generateChild(birthday));
	}
	
	public BirthSertificate createBS() {
		return birthSertificatesRemote.create(GenerateDataHelper.generateBS());
	}
	
	public List<SDouRequest> createReqS(HDouRequest douRequest, String ... dous) {
		List<SDouRequest> res = new ArrayList<SDouRequest>(dous.length);
		for (String douId : dous) {
			res.add(createReqS(douRequest, douId));
		}
		return res;
	}
	
	public SDouRequest createReqS(HDouRequest douRequest, String  douId) {
		return createReqSWithRating(douRequest, douId, SDouRequest.RATING_MIDDLE);
	}
	
	public SDouRequest createReqSWithRating(HDouRequest douRequest, String  douId, int rating) {
		SDouRequest spec = new SDouRequest();
		Dou contractor = dousRemote.getByNumber(douId);
		if(contractor == null){
			contractor = createDou(douId);
		}
		spec.setArticleUnit(contractor);
		spec.setRating(rating);
		return hDouRequests.createSpecificationLine(spec, douRequest);
	}

	public Dou createDou(Integer douId) {
		return createDou(douId + "");
	}
	
	public Dou createDou(String douId) {
		Dou contractor = new Dou();
		contractor.setNumber(douId);
		Region region = new Region(""+douId,""+douId);
		region = regionsRemote.create(region);
		contractor.setRegion(region);
		contractor = dousRemote.create(contractor);
		return contractor;
	}
	
	
	
	
	
	public HDouRequest createReq(String birthday, String regDate) {
		HDouRequest douRequest = hDouRequests.create(GenerateDataHelper.generateReq(birthday, regDate));
		return douRequest;
	}
	
	public HRegister createRegister() {
		HRegister hRegister = hRegisters.create(GenerateDataHelper.generateRegister());
		return hRegister;
	}
	
	public SRegister createRegisterS(HRegister hRegister, HDouRequest hDouRequest, Action action) {
		SRegister spec = new SRegister();
		spec.setDou(createDou(1));
		spec.setDouRequest(hDouRequest);
		if(action == null){
			action = Action.GIVE_PERMIT;
		}
		spec.setRnAction(action);
		
		return hRegisters.createSpecificationLine(spec, hRegister);
	}
	
	
	public Privilege createPrivelege(String name) {
		Privilege object = new Privilege();
		object.setName(name);
		object.setDescription(name);
		return privilegesRemote.create(object);
	}
	
	
	protected Date parseISODate(String str){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return dateFormat.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}
}
