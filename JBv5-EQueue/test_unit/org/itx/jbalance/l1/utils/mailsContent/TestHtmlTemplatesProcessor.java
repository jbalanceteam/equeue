package org.itx.jbalance.l1.utils.mailsContent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;
import org.junit.Assert;
import org.junit.Test;

public class TestHtmlTemplatesProcessor {
	protected Log log = new Log4JLogger(getClass().getName());


	@Test
	public void testReplace() {
		StringBuilder stringBuilder = new StringBuilder("<h1>#{test}</h1>");
		HtmlTemplatesProcessor htmlTemplatesProcessor = new HtmlTemplatesProcessor(stringBuilder);
		htmlTemplatesProcessor.setVariable("test", "yo");
		Assert.assertEquals(htmlTemplatesProcessor.toString(), "<h1>yo</h1>");
	}
	
	@Test
	public void testGetTemplateAsString() {
		HtmlTemplatesProcessor htmlTemplatesProcessor = new HtmlTemplatesProcessor("createRn.html");
//		Assert.assertNotNull(htmlTemplatesProcessor.toString());
	}
}
