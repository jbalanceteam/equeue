package org.itx.jbalance.l1.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;
import org.junit.Assert;
import org.junit.Test;

//import org.itx.jbalance.l2_api.dto.DouRequestDTO;

public class TestDateUtils {
	protected Log log = new Log4JLogger(getClass().getName());

	DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	@Test
	public void testGetEndOfYear() throws ParseException {
		DateUtils dateUtils = new DateUtils();
		Date endOfYear = dateUtils.getEndOfYear(df.parse("2008-02-02 00:00"));
		Assert.assertEquals( "2008-12-31 23:59", df.format(endOfYear));
	}
	
	@Test
	public void testGetEndOfYear2() throws ParseException {
		DateUtils dateUtils = new DateUtils();
		Date endOfYear = dateUtils.getEndOfYear(2008);
		Assert.assertEquals( "2008-12-31 23:59", df.format(endOfYear));
	}
	
	@Test
	public void testGetStartOfYear() throws ParseException {
		DateUtils dateUtils = new DateUtils();
		Date endOfYear = dateUtils.getStartOfYear(df.parse("2008-02-02 00:00"));
		Assert.assertEquals( "2008-01-01 00:00", df.format(endOfYear));
	}
	
	@Test
	public void testGetStartOfYear2() throws ParseException {
		DateUtils dateUtils = new DateUtils();
		Date endOfYear = dateUtils.getStartOfYear(2008);
		Assert.assertEquals( "2008-01-01 00:00", df.format(endOfYear));
	}

}
