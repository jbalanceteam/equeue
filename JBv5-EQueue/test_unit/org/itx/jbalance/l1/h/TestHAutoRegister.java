package org.itx.jbalance.l1.h;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.itx.jbalance.AbstractEqueueTestEJB;
import org.itx.jbalance.l0.h.HAutoRegister.Method;
import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.junit.Assert;
import org.junit.Test;

public class TestHAutoRegister extends AbstractEqueueTestEJB {
	@Test
	public void testCalculateDouRatingWishesMethod(){
		String douNumber1 = "10";
		Dou dou1 = createDou(douNumber1);
		
		String douNumber2 = "20";
		Dou dou2 = createDou(douNumber2);
		
		HDouRequest rn1 = createReq("2014-01-01", null);
		createReqSWithRating(rn1, douNumber1, SDouRequest.RATING_MIN);
		createReqSWithRating(rn1, douNumber2, SDouRequest.RATING_MIN);
		
		HDouRequest rn2 = createReq("2014-01-01", null);
		createReqSWithRating(rn2, douNumber1, SDouRequest.RATING_MIDDLE);
		createReqSWithRating(rn2, douNumber2, SDouRequest.RATING_MIN);
		
		HDouRequest rn3 = createReq("2014-01-01", null);
		createReqSWithRating(rn3, douNumber1, SDouRequest.RATING_MAX);
		createReqSWithRating(rn3, douNumber2, SDouRequest.RATING_MIN);
		
		DOUGroups ageGroup = douGroupsRemote.getById(1);
		
		Map<Dou, Double> douRating1 = hAutoRegisters.calculateDouRating(Arrays.asList(dou1), ageGroup, parseISODate("2016-02-01"), Method.WISHES);
		Assert.assertNotNull(douRating1);
		System.out.println("douRating1: " + douRating1);
		Assert.assertTrue(douRating1.containsKey(dou1));
		double expected1 = (double) SDouRequest.RATING_MIN + SDouRequest.RATING_MIDDLE + SDouRequest.RATING_MAX;
		Assert.assertTrue(Math.abs(expected1 - (double) douRating1.get(dou1)) < 0.001);
		
		
		Map<Dou, Double> douRating2 = hAutoRegisters.calculateDouRating(Arrays.asList(dou2), ageGroup, parseISODate("2016-02-01"), Method.WISHES);
		Assert.assertNotNull(douRating2);
		System.out.println("douRating1: " + douRating1);
		Assert.assertTrue(douRating2.containsKey(dou2));
		double expected2 = (double) SDouRequest.RATING_MIN + SDouRequest.RATING_MIN + SDouRequest.RATING_MIN;
		Assert.assertTrue(Math.abs(expected2 - (double) douRating2.get(dou2)) < 0.001);
		
		Map<Dou, Double> douRating = hAutoRegisters.calculateDouRating(Arrays.asList(dou1, dou2), ageGroup, parseISODate("2016-02-01"), Method.WISHES);
		Assert.assertEquals(douRating1.get(dou1), douRating.get(dou1));
		Assert.assertEquals(douRating2.get(dou2), douRating.get(dou2));
	}
	
	@Test
	public void testCalculateDouRatingDouFillMethod(){
		String douNumber1 = "10";
		Dou dou1 = createDou(douNumber1);
		
		String douNumber2 = "20";
		Dou dou2 = createDou(douNumber2);
		
		HDouRequest rn1 = createReq("2014-01-01", null);
		createReqSWithRating(rn1, douNumber1, SDouRequest.RATING_MIN);
		createReqSWithRating(rn1, douNumber2, SDouRequest.RATING_MIN);
		
		HDouRequest rn2 = createReq("2014-01-01", null);
		createReqSWithRating(rn2, douNumber1, SDouRequest.RATING_MIDDLE);
		createReqSWithRating(rn2, douNumber2, SDouRequest.RATING_MIN);
		
		HDouRequest rn3 = createReq("2014-01-01", null);
		createReqSWithRating(rn3, douNumber1, SDouRequest.RATING_MAX);
		createReqSWithRating(rn3, douNumber2, SDouRequest.RATING_MIN);
		
		DOUGroups ageGroup = douGroupsRemote.getById(1);
		
		Map<Dou, Double> douRating1 = hAutoRegisters.calculateDouRating(Arrays.asList(dou1), ageGroup, parseISODate("2016-02-01"), Method.DOU_FILL);
		Assert.assertNotNull(douRating1);
		Assert.assertTrue(douRating1.containsKey(dou1));
		double expected1 = (double) 3;
		Assert.assertTrue(Math.abs(expected1 - (double) douRating1.get(dou1)) < 0.001);
		
		
		Map<Dou, Double> douRating2 = hAutoRegisters.calculateDouRating(Arrays.asList(dou2), ageGroup, parseISODate("2016-02-01"), Method.DOU_FILL);
		Assert.assertNotNull(douRating2);
		Assert.assertTrue(douRating2.containsKey(dou2));
		double expected2 = (double) 3;
		Assert.assertTrue(Math.abs(expected2 - (double) douRating2.get(dou2)) < 0.001);
		
		Map<Dou, Double> douRating = hAutoRegisters.calculateDouRating(Arrays.asList(dou1, dou2), ageGroup, parseISODate("2016-02-01"), Method.DOU_FILL);
		Assert.assertEquals(douRating1.get(dou1), douRating.get(dou1));
		Assert.assertEquals(douRating2.get(dou2), douRating.get(dou2));
	}
	
	
	
	@Test
	public void testGetSuitableDouRequests(){
		Privilege privilege = createPrivelege("test");
		String douNumber1 = "10";
		String douNumber2 = "20";
		
//		Первый не подходит по возрасту ... Больно млад
		HDouRequest rn1 = createReq("2015-01-01", null);
		createReqSWithRating(rn1, douNumber1, SDouRequest.RATING_MIN);
		createReqSWithRating(rn1, douNumber2, SDouRequest.RATING_MIN);
		
//		Второй должен попасть в выборку
		HDouRequest rn2 = createReq("2014-01-01", null);
		createReqSWithRating(rn2, douNumber1, SDouRequest.RATING_MIDDLE);
		createReqSWithRating(rn2, douNumber2, SDouRequest.RATING_MIN);
		
//		Третий уже в протоколе
		HDouRequest rn3 = createReq("2014-01-01", null);
		createReqSWithRating(rn3, douNumber1, SDouRequest.RATING_MAX);
		createReqSWithRating(rn3, douNumber2, SDouRequest.RATING_MIN);
		HRegister register = createRegister();
		createRegisterS(register, rn3, null);
		
		DOUGroups ageGroup = douGroupsRemote.getById(1);
		
		Dou dou1 = dousRemote.getByNumber(douNumber1);
		Dou dou2 = dousRemote.getByNumber(douNumber2);
		List<Long> asList = Arrays.asList(dou1.getUId(), dou2.getUId());
//		Этот запрос ничего не должен вернуть, т.к. у нас нелт льготников 
		List<HDouRequest> suitableDouRequests = hAutoRegisters.getSuitableDouRequests(ageGroup, parseISODate("2016-02-01"), false, asList, null);
		Assert.assertNotNull(suitableDouRequests);
		Assert.assertEquals(1, suitableDouRequests.size());
		Assert.assertEquals(rn2, suitableDouRequests.get(0));
		
		suitableDouRequests = hAutoRegisters.getSuitableDouRequests(ageGroup, parseISODate("2016-02-01"), true, Arrays.asList(dou1.getUId(), dou2.getUId()), null);
		Assert.assertNotNull(suitableDouRequests);
		Assert.assertEquals(0, suitableDouRequests.size());

//		Проставляем всем льготу
		rn1.setPrivilege(privilege);
		rn2.setPrivilege(privilege);
		rn3.setPrivilege(privilege);
		rn1 = hDouRequests.update(rn1);
		rn2 = hDouRequests.update(rn2);
		rn3 = hDouRequests.update(rn3);
		
//		Запрос возвращает 2-й РН
		suitableDouRequests = hAutoRegisters.getSuitableDouRequests(ageGroup, parseISODate("2016-02-01"), true, asList, null);
		Assert.assertNotNull(suitableDouRequests);
		Assert.assertEquals(1, suitableDouRequests.size());
		Assert.assertEquals(rn2, suitableDouRequests.get(0));
	}
	
	
	
	
	@Test
	public void testAutoRegisterCreation(){
		HAutoRegister autoRegister = new HAutoRegister();
		Dou dou = createDou(1);
		autoRegister.addSAutoRegister(dou, 1, 10);
		autoRegister.setCalculationDate(new Date());
		autoRegister.setPrivelegePart(HAutoRegister.DAFAULT_PRIVELEGE_PART);
		autoRegister.setMethod(Method.WISHES);
		hAutoRegisters.create(autoRegister);
		
		List<? extends HAutoRegister> all = hAutoRegisters.getAll(HAutoRegister.class);
		Assert.assertNotNull(all);
		Assert.assertEquals(1, all.size());
		Assert.assertNotNull(all.get(0).getUId());
		Assert.assertEquals(1, all.get(0).getSAutoRegisters().size());
		Assert.assertNotNull(all.get(0).getSAutoRegister(dou, 1));
	}
	
	
	
	/**
	 * Тестируем пересечение возрастов
	 * В очереди 2 номера
	 * Путевка должна быть выдана 1-у.
	 * У 2-го должен быть отказ
	 */
	@Test
	public void testAutoRegisterCrossAges(){
		String douNumber = "10";
		Dou dou = createDou(douNumber);
		HDouRequest rn1 = createReq("2014-01-01", null);
		createReqS(rn1, douNumber);
		
		HDouRequest rn2 = createReq("2014-01-01", null);
		createReqS(rn2, douNumber);
		
		HDouRequest rn3 = createReq("2014-01-01", null);
		createReqS(rn3, douNumber);
		
		
		HAutoRegister autoRegister = new HAutoRegister();
		autoRegister.setCalculationDate(parseISODate("2015-07-01"));
		autoRegister.setPrivelegePart(HAutoRegister.DAFAULT_PRIVELEGE_PART);
		autoRegister.setMethod(Method.WISHES);
		autoRegister.setIncludeRefuses(true);
		autoRegister.addSAutoRegister(dou, 1, 1);
		autoRegister.addSAutoRegister(dou, 6, 1);
		
		autoRegister = hAutoRegisters.autoRegister(autoRegister);
		
		Assert.assertNotNull(autoRegister);
		Assert.assertNotNull(autoRegister.getSAutoRegisters());
		Assert.assertNotNull(autoRegister.getHRegister());
		
		HRegister register = autoRegister.getHRegister();
		List<SRegister> specificationLines = hRegisters.getSpecificationLines(register);
		Assert.assertNotNull(specificationLines);
		
		Assert.assertEquals(3, specificationLines.size());
		
		Assert.assertEquals(rn1, specificationLines.get(0).getDouRequest());
		Assert.assertEquals(dou, specificationLines.get(0).getDou());
		Assert.assertEquals(Action.GIVE_PERMIT, specificationLines.get(0).getRnAction());
		
		Assert.assertEquals(rn2, specificationLines.get(1).getDouRequest());
		Assert.assertEquals(dou, specificationLines.get(1).getDou());
		Assert.assertEquals(Action.GIVE_PERMIT, specificationLines.get(1).getRnAction());
		
		Assert.assertEquals(rn3, specificationLines.get(2).getDouRequest());
		Assert.assertNull(specificationLines.get(2).getDou());
		Assert.assertEquals(Action.REFUSAL, specificationLines.get(2).getRnAction());
		
	}
	
	
	
	
	
	
	/**
	 * В очереди 2 номера
	 * Путевка должна быть выдана 1-у.
	 * У 2-го должен быть отказ
	 */
	@Test
	public void testAutoRegister(){
		String douNumber = "10";
		Dou dou = createDou(douNumber);
		HDouRequest rn1 = createReq("2014-01-01", null);
		createReqS(rn1, douNumber);
		
		HDouRequest rn2 = createReq("2014-01-01", null);
		createReqS(rn2, douNumber);
		
		Integer ageGroupID = 1;
		
		HAutoRegister autoRegister = new HAutoRegister();
		autoRegister.setCalculationDate(parseISODate("2015-07-01"));
		autoRegister.setPrivelegePart(HAutoRegister.DAFAULT_PRIVELEGE_PART);
		autoRegister.setMethod(Method.WISHES);
		autoRegister.setIncludeRefuses(true);
		autoRegister.addSAutoRegister(dou, ageGroupID, 1);
		
		autoRegister = hAutoRegisters.autoRegister(autoRegister);
		
		Assert.assertNotNull(autoRegister);
		Assert.assertNotNull(autoRegister.getSAutoRegisters());
		Assert.assertNotNull(autoRegister.getHRegister());
		
		HRegister register = autoRegister.getHRegister();
		List<SRegister> specificationLines = hRegisters.getSpecificationLines(register);
		Assert.assertNotNull(specificationLines);
		
//		System.out.println("specificationLines: " + specificationLines);
//		for (SRegister sRegister : specificationLines) {
//			System.out.println(">>> " + sRegister.getUId() + "   " + sRegister.getDouRequest() + "   " + sRegister.getRnAction() + "   " + sRegister.getDouRequest().getUId());
//		}
		
		Assert.assertEquals(2, specificationLines.size());
		Assert.assertEquals(rn1, specificationLines.get(0).getDouRequest());
		Assert.assertEquals(dou, specificationLines.get(0).getDou());
		Assert.assertEquals(ageGroupID, specificationLines.get(0).getAgeGroupId());
		Assert.assertEquals(Action.GIVE_PERMIT, specificationLines.get(0).getRnAction());
		
		Assert.assertEquals(rn2, specificationLines.get(1).getDouRequest());
		Assert.assertNull(specificationLines.get(1).getDou());
		Assert.assertNull(specificationLines.get(1).getAgeGroupId());
		Assert.assertEquals(Action.REFUSAL, specificationLines.get(1).getRnAction());
	}
	
	
	
	
	/**
	 * В очереди 2 номера
	 * Путевка должна быть выдана 1-у.
	 * У 2-го должен быть отказ
	 */
	@Test
	public void testAutoRegisterDouFill(){
		String douNumber = "10";
		Dou dou = createDou(douNumber);
		HDouRequest rn1 = createReq("2014-01-01", null);
		createReqS(rn1, douNumber);
		
		HDouRequest rn2 = createReq("2014-01-01", null);
		createReqS(rn2, douNumber);
		
		
		HAutoRegister autoRegister = new HAutoRegister();
		autoRegister.setCalculationDate(parseISODate("2015-07-01"));
		autoRegister.setPrivelegePart(HAutoRegister.DAFAULT_PRIVELEGE_PART);
		autoRegister.setMethod(Method.DOU_FILL);
		autoRegister.setIncludeRefuses(true);
		autoRegister.addSAutoRegister(dou, 1, 1);
		
		autoRegister = hAutoRegisters.autoRegister(autoRegister);
		
		Assert.assertNotNull(autoRegister);
		Assert.assertNotNull(autoRegister.getSAutoRegisters());
		Assert.assertNotNull(autoRegister.getHRegister());
		
		HRegister register = autoRegister.getHRegister();
		List<SRegister> specificationLines = hRegisters.getSpecificationLines(register);
		Assert.assertNotNull(specificationLines);
		
//		System.out.println("specificationLines: " + specificationLines);
//		for (SRegister sRegister : specificationLines) {
//			System.out.println(">>> " + sRegister.getUId() + "   " + sRegister.getDouRequest() + "   " + sRegister.getRnAction() + "   " + sRegister.getDouRequest().getUId());
//		}
		
		Assert.assertEquals(2, specificationLines.size());
		Assert.assertEquals(rn1, specificationLines.get(0).getDouRequest());
		Assert.assertEquals(dou, specificationLines.get(0).getDou());
		Assert.assertEquals(Action.GIVE_PERMIT, specificationLines.get(0).getRnAction());
		
		Assert.assertEquals(rn2, specificationLines.get(1).getDouRequest());
		Assert.assertNull(specificationLines.get(1).getDou());
		Assert.assertEquals(Action.REFUSAL, specificationLines.get(1).getRnAction());
		
		
		Assert.assertEquals(1, (int)autoRegister.getSAutoRegister(dou, 1).getSeatsUsed());
		Assert.assertEquals(0, (int)autoRegister.getSAutoRegister(dou, 1).getSeatsPrivilegeUsed());
	}
	
	
	
	/**
	 * В очереди 2 номера
	 * Путевка должна быть выдана льготнику
	 * У 2-го должен быть отказ
	 */
	@Test
	public void testAutoRegisterWithPrivelage(){
		String douNumber = "10";
		Dou dou = createDou(douNumber);
		HDouRequest rn1 = createReq("2014-01-01", null);
		createReqS(rn1, douNumber);
		
		HDouRequest rn2 = createReq("2014-01-01", null);
		Privilege privelege = createPrivelege("test");
		rn2.setPrivilege(privelege);
		rn2 = hDouRequests.update(rn2);
		createReqS(rn2, douNumber);
		
		
		HAutoRegister autoRegister = new HAutoRegister();
		autoRegister.setMethod(Method.WISHES);
		autoRegister.setPrivelegePart(100);
		autoRegister.setCalculationDate(parseISODate("2015-07-01"));
		autoRegister.setIncludeRefuses(true);
		autoRegister.addSAutoRegister(dou, 1, 1);
		
		autoRegister = hAutoRegisters.autoRegister(autoRegister);
		
		Assert.assertNotNull(autoRegister);
		Assert.assertNotNull(autoRegister.getSAutoRegisters());
		Assert.assertNotNull(autoRegister.getHRegister());
		
		HRegister register = autoRegister.getHRegister();
		List<SRegister> specificationLines = hRegisters.getSpecificationLines(register);
		Assert.assertNotNull(specificationLines);
		Assert.assertEquals(2, specificationLines.size());
		
		Assert.assertEquals(rn2, specificationLines.get(0).getDouRequest());
		Assert.assertEquals(dou, specificationLines.get(0).getDou());
		Assert.assertEquals(Action.GIVE_PERMIT, specificationLines.get(0).getRnAction());
		
		Assert.assertEquals(rn1, specificationLines.get(1).getDouRequest());
		Assert.assertNull(specificationLines.get(1).getDou());
		Assert.assertEquals(Action.REFUSAL, specificationLines.get(1).getRnAction());
		
		Assert.assertEquals(1, (int)autoRegister.getSAutoRegister(dou, 1).getSeatsUsed());
		Assert.assertEquals(1, (int)autoRegister.getSAutoRegister(dou, 1).getSeatsPrivilegeUsed());
	}
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * Тестируем лишние отказы...
	 * Если в автопротоклле имеется ДОУ и возр группа, но нет мест в ЭТОЙ группедля ЭТОГО ДОУ
	 * то отказы выдаваться не должны
	 */
	@Test
	public void testAutoRegisterOddRefuce(){
		String douNumber1 = "10";
		String douNumber2 = "11";
		Dou dou1 = createDou(douNumber1);
		Dou dou2 = createDou(douNumber2);
		
		HDouRequest rn1 = createReq("2013-01-01", null);
		createReqS(rn1, douNumber1);
		
		HDouRequest rn2 = createReq("2013-01-01", null);
		createReqS(rn2, douNumber2);
		
		HDouRequest rn3 = createReq("2014-06-01", null);
		createReqS(rn3, douNumber2);
		
		
		HAutoRegister autoRegister = new HAutoRegister();
		autoRegister.setMethod(Method.WISHES);
		autoRegister.setPrivelegePart(40);
		autoRegister.setCalculationDate(parseISODate("2016-07-01"));
		autoRegister.setIncludeRefuses(true);
		autoRegister.addSAutoRegister(dou1, 2, 1);
		autoRegister.addSAutoRegister(dou2, 1, 1);
		
		autoRegister = hAutoRegisters.autoRegister(autoRegister);
		
		Assert.assertNotNull(autoRegister);
		Assert.assertNotNull(autoRegister.getSAutoRegisters());
		Assert.assertNotNull(autoRegister.getHRegister());
		
		HRegister register = autoRegister.getHRegister();
		List<SRegister> specificationLines = hRegisters.getSpecificationLines(register);
		Assert.assertNotNull(specificationLines);
		
//		System.out.println("specificationLines: " + specificationLines);
//		for (SRegister sRegister : specificationLines) {
//			System.out.println(">>> " + "DOU: " + sRegister.getDou().getIdDou()  + "  REQ: "  +  sRegister.getDouRequest() + "   " + sRegister.getRnAction());
//		}
		
		Assert.assertEquals(2, specificationLines.size());
		
		SRegister s1 = null;
		for (SRegister sRegister : specificationLines) {
			if(sRegister.getDouRequest().equals(rn1)){
				s1 = sRegister;
			}
		}
		
		SRegister s2 = null;
		for (SRegister sRegister : specificationLines) {
			if(sRegister.getDouRequest().equals(rn3)){
				s2 = sRegister;
			}
		}
		
		Assert.assertNotNull(s1);
		Assert.assertNotNull(s2);
		
		Assert.assertEquals(rn1, s1.getDouRequest());
		Assert.assertEquals(dou1, s1.getDou());
		Assert.assertEquals(Action.GIVE_PERMIT, s1.getRnAction());
		
		Assert.assertEquals(rn3, s2.getDouRequest());
		Assert.assertEquals(dou2, s2.getDou());
		Assert.assertEquals(Action.GIVE_PERMIT, s2.getRnAction());
		
	}
	
	
	
	
	
}
