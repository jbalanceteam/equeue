package org.itx.jbalance.l1.h;

import java.util.Date;

import org.itx.jbalance.AbstractEqueueTestEJB;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HDouRequest.Status;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l1.api.RegisterSearchParams;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.LongFilter;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class TestHRegister extends AbstractEqueueTestEJB {
	@Test
	public void testCreateGet(){
		Juridical jur = new Juridical();
		jur.setJAddress("asd");
		jur.setName("123");
		jur = juridicals.create(jur);
		
		HRegister object = new HRegister();
		object.setFoundation("123");
		object.setRegisterDate(new Date());
		object = hRegisters.create(object);
	}
	
	
	private Physical createChild() {
		Physical physical = new Physical();
		physical.setName("123");
		physical.setRealName("123");
		physical.setSurname("123");
		physical.setBirthday(new Date());
		physical.setPhone("123");
		return physicalsRemote.create(physical);
	}
	

	/**
	 * #133 тест для проверки изменения статусов РН при испротзовании его в протоколе.
	 * статус РН должен меняться в зависимости от выбранного действия:
	 * Отказ -> Ожидание
	 * Убрать по заявлению -> Убрали по заявлению
	 * Убрать по достижению 7-и лет -> Убрали Убрать по достижению 7-и лет
	 * Выдать путевку -> Выдали путевку
	 * Вернуть в очередь -> Ожидание
	 */
	@Test
	public void testStatusChanging(){
		HDouRequest douRequest = new HDouRequest();
		douRequest.setFoundation("Основание");
		douRequest.setRegDate(new Date());
		douRequest.setBirthSertificate(createBS());
		douRequest.setChild(createChild());
		douRequest = hDouRequests.create(douRequest);
		
		SDouRequest spec = new SDouRequest();
		Dou contractor = new Dou();
		contractor.setNumber("x");
		Region region = new Region("Западный","Западный микрорайон");
		region = regionsRemote.create(region);
		contractor.setRegion(region);
		contractor = dousRemote.create(contractor);
		spec.setArticleUnit(contractor);
		spec = hDouRequests.createSpecificationLine(spec, douRequest);
		
		/* Создаем протокол с отказом */
		HRegister hRegister = new HRegister();
		hRegister.setFoundation("123");
		hRegister.setRegisterDate(new Date());
		hRegister = hRegisters.create(hRegister);
		
		SRegister sRegister = new SRegister();
		sRegister.setDou(contractor);
		sRegister.setRnAction(Action.REFUSAL);
		sRegister.setDouRequest(douRequest);
		sRegister = hRegisters.createSpecificationLine(sRegister, hRegister);
		
		/* Проверяем, что после отказа статус = WAIT*/ 
		douRequest = hDouRequests.getByUId(douRequest.getUId(),HDouRequest.class);
		Assert.assertEquals(Status.WAIT, douRequest.getStatus());
		
		
		/* Создаем протокол с путеккой */
		hRegister = new HRegister();
		hRegister.setFoundation("123");
		hRegister.setRegisterDate(new Date());
		hRegister.setReceivePermitDueDate(new Date(System.currentTimeMillis() + 1000l * 60 * 60 * 24 *11));
		hRegister = hRegisters.create(hRegister);
		
		sRegister = new SRegister();
		sRegister.setDou(contractor);
		sRegister.setRnAction(Action.GIVE_PERMIT);
		sRegister.setDouRequest(douRequest);
		sRegister = hRegisters.createSpecificationLine(sRegister, hRegister);
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.SECOND);
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.THIRD);
		
		/* Проверяем, что после выдачи путевки статус = PERMIT*/ 
		douRequest = hDouRequests.getByUId(douRequest.getUId(),HDouRequest.class);
		Assert.assertEquals(Status.PERMIT, douRequest.getStatus());
		
		
		
		
		/* Создаем протокол с путеккой */
		/* Меняем в протоколе действие на "Убрать по заявлению" */
		hRegister = new HRegister();
		hRegister.setFoundation("123");
		hRegister.setRegisterDate(new Date());
		hRegister.setReceivePermitDueDate(new Date(System.currentTimeMillis() + 1000l * 60 * 60 * 24 *11));
		hRegister = hRegisters.create(hRegister);
		
		sRegister = new SRegister();
		sRegister.setDou(contractor);
		sRegister.setRnAction(Action.DELETE_BY_PARRENTS_AGREE);
		sRegister.setDouRequest(douRequest);
		sRegister = hRegisters.createSpecificationLine(sRegister, hRegister);
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.SECOND);
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.THIRD);
		douRequest = hDouRequests.getByUId(douRequest.getUId(),HDouRequest.class);
		Assert.assertEquals(Status.DELETE_BY_PARRENTS_AGREE, douRequest.getStatus());
		
		
		/* Меняем в протоколе действие на "Убрать по достижению 7 лет" */
		hRegister = new HRegister();
		hRegister.setFoundation("123");
		hRegister.setRegisterDate(new Date());
		hRegister.setReceivePermitDueDate(new Date(System.currentTimeMillis() + 1000l * 60 * 60 * 24 *11));
		hRegister = hRegisters.create(hRegister);
		
		sRegister = new SRegister();
		sRegister.setDou(contractor);
		sRegister.setRnAction(Action.DELETE_BY_7_YEAR);
		sRegister.setDouRequest(douRequest);
		sRegister = hRegisters.createSpecificationLine(sRegister, hRegister);
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.SECOND);
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.THIRD);
		douRequest = hDouRequests.getByUId(douRequest.getUId(),HDouRequest.class);
		Assert.assertEquals(Status.DELETE_BY_7_YEAR, douRequest.getStatus());
		
	}
	
	
	@Test
	public void testGKPStatusChanging(){
		HDouRequest douRequest = new HDouRequest();
		douRequest.setFoundation("Основание");
		douRequest.setRegDate(new Date());
		douRequest.setBirthSertificate(createBS());
		douRequest.setChild(createChild());
		douRequest = hDouRequests.create(douRequest);
		
		SDouRequest spec = new SDouRequest();
		Dou contractor = new Dou();
		Region region = new Region("Западный","Западный микрорайон");
		region = regionsRemote.create(region);
		contractor.setRegion(region);
		contractor.setNumber("x");
		contractor = dousRemote.create(contractor);
		spec.setArticleUnit(contractor);
		spec = hDouRequests.createSpecificationLine(spec, douRequest);
		
		/* Создаем протокол с отказом */
		HRegister hRegister = new HRegister();
		hRegister.setFoundation("123");
		hRegister.setRegisterDate(new Date());
		hRegister.setReceivePermitDueDate(new Date(System.currentTimeMillis() + 1000l * 60 * 60 * 24 *11));
		hRegister = hRegisters.create(hRegister);
		
		SRegister sRegister = new SRegister();
		sRegister.setDou(contractor);
		sRegister.setRnAction(Action.GKP);
		sRegister.setDouRequest(douRequest);
		sRegister = hRegisters.createSpecificationLine(sRegister, hRegister);
		
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.SECOND);
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.THIRD);
		
		/* Проверяем, что после отказа статус = WAIT*/ 
		douRequest = hDouRequests.getByUId(douRequest.getUId(),HDouRequest.class);
		Assert.assertEquals(Status.WAIT, douRequest.getStatus());
	}
	
	
	
	@Test
	public void testSearch(){
		/**
		 * Этот поиск ничего не вернет...Просто проверка генерируемого HQL
		 */

		RegisterSearchParams params = new RegisterSearchParams();
		params.custom = "abc";
		params.douRequestUid = new LongFilter(123l); 
		params.registerDate = DateFilter.build(new Date(),new Date());
		hRegisters.search(params);
	}
	
	
	@Test
	public void testGetSpecCount(){
		HRegister register = createRegister();
		SRegister sRegister = createRegisterS(register, createReq(null, null), Action.GIVE_PERMIT);
		
		Integer specCount = hRegisters.getSpecCount(register.getUId());
		Assert.assertTrue(specCount.equals(1));
		
		createRegisterS(register, createReq(null, null), Action.GIVE_PERMIT);
		
		specCount = hRegisters.getSpecCount(register.getUId());
		Assert.assertTrue(specCount.equals(2));
		
		hRegisters.deleteSpecificationLine(sRegister);
		
		specCount = hRegisters.getSpecCount(register.getUId());
		Assert.assertTrue(specCount.equals(1));
		
	}
	
	
	@Test
	public void testRecivePermitDueDate(){
		Juridical jur = new Juridical();
		jur.setJAddress("asd");
		jur.setName("123");
		jur = juridicals.create(jur);
		
		HRegister object = new HRegister();
		object.setFoundation("123");
		object.setRegisterDate(new Date());
		Date recivePermitDueDate1 = new Date(System.currentTimeMillis()+ 1000l * 60 * 60 *24);
		object.setReceivePermitDueDate(recivePermitDueDate1);
		object = hRegisters.create(object);
		
		Assert.assertNotNull(object);
		Assert.assertNotNull(object.getUId());
		Assert.assertEquals(recivePermitDueDate1, object.getReceivePermitDueDate());
		
		Date recivePermitDueDate2 = new Date(System.currentTimeMillis()+ 2000l * 60 * 60 *24);
		object.setReceivePermitDueDate(recivePermitDueDate2);
		
		object = hRegisters.update(object);
		Assert.assertEquals(recivePermitDueDate2, object.getReceivePermitDueDate());
	}
	
	
//	TODO
	@Test
	@Ignore
	public void testThreeStates(){
		
	}
	
	@Test
	public void testSeqNumber(){
		HRegister hRegister = createRegister();
		SRegister createRegisterS1 = createRegisterS(hRegister, createReq(null,null), null);
		Assert.assertEquals(1, createRegisterS1.getSeqNumber());
		SRegister createRegisterS2 = createRegisterS(hRegister, createReq(null,null), null);
		Assert.assertEquals(2, createRegisterS2.getSeqNumber());
		SRegister createRegisterS3 = createRegisterS(hRegister, createReq(null,null), null);
		Assert.assertEquals(3, createRegisterS3.getSeqNumber());
	}

}
