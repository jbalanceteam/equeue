package org.itx.jbalance.l1.h;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


import org.itx.jbalance.AbstractEqueueTestEJB;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.h.RecordColor;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.DouRequestsSearchParams.OrderStatus;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.DateFilterType;
import org.itx.jbalance.l1.api.filters.IntegerFilter;
import org.itx.jbalance.l1.api.filters.StringFilter;
import org.itx.jbalance.l1.api.filters.StringFilterType;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;


public class TestHDouRequests extends AbstractEqueueTestEJB {
	@Test
	public void testCreateGet(){
		HDouRequest douRequest = createReq(null, null);
		Assert.assertNotNull(douRequest);
		Assert.assertNotNull(douRequest.getUId());
	}

	@Test
	public void testDelete(){
		HDouRequest douRequest = createReq(null, null);
		createReqS(douRequest, "1");
		
		hDouRequests.delete(douRequest);
		
		HDouRequest byUId = hDouRequests.getByUId(douRequest.getUId(), HDouRequest.class);
		Assert.assertNotNull(byUId);
		Assert.assertNotNull(byUId.getCloseDate());
		
		List<SDouRequest> specificationLines = hDouRequests.getSpecificationLines(byUId);
		for (SDouRequest sDouRequest : specificationLines) {
			Assert.assertNotNull(sDouRequest.getCloseDate());
		}

	}
	
	/**
	 * Тестирование для 
	 * #163 Не позволять удалять РН, которые включены в протокол
	 */
	@Test (expected=RuntimeException.class)
	public void testDeletePresentedInProtocol(){
		log.debug("1");
		HDouRequest douRequest = createReq(null, null);
		SDouRequest spec  = createReqS(douRequest, "1");
		
		Juridical jur = new Juridical();
		jur.setJAddress("asd");
		jur.setName("123");
		jur = juridicals.create(jur);
		log.debug("3");
		HRegister register = new HRegister();
		register.setFoundation("123");
		register.setRegisterDate(new Date());
		register = hRegisters.create(register);
		log.debug("4");
		SRegister sRegister = new SRegister();
		sRegister.setDouRequest(douRequest);
		sRegister.setDou(spec.getArticleUnit());
		sRegister.setRnAction(Action.GIVE_PERMIT);
		SRegister createSpecificationLine = hRegisters.createSpecificationLine(sRegister, register);
		log.debug("5");
		douRequest = createSpecificationLine.getDouRequest();
		hDouRequests.delete(douRequest);
		log.debug("6");
	}
	
	
	@Test
	public void testValidateRegDate() throws ParseException{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String regDate = "2013-01-02 00:00";
		HDouRequest douRequest = createReq(null, regDate);

		log.debug(">>>>>>>>>>> "+douRequest.getRegDate());
		Date validate1 = hDouRequests.validate(simpleDateFormat.parse(regDate));
		Assert.assertNull(validate1);
		
		Date validate2 = hDouRequests.validate(simpleDateFormat.parse("2013-01-03 00:00"));
		Assert.assertNull(validate2);
		
		Date validate3 = hDouRequests.validate(simpleDateFormat.parse("2013-01-01 00:00"));
		Assert.assertEquals(validate3,simpleDateFormat.parse(regDate));
	}
	
	
	@Test
	public void testSearchCountWithEffectiveDate() throws ParseException{
		HDouRequest rn1 = createReq(null, "2013-01-03 00:00");
		HDouRequest rn2 = createReq(null, "2013-01-04 00:00");
		HDouRequest rn3 = createReq(null, "2013-01-05 00:00");
		
		
		DouRequestsSearchParams params = new DouRequestsSearchParams();
		
		Integer searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)3, (long)searchCount);
		
		params.setEffectiveDate(rn1.getRegDate());
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		params.setEffectiveDate(rn2.getRegDate());
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)2, (long)searchCount);
		
		params.setEffectiveDate(rn3.getRegDate());
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)3, (long)searchCount);
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date closeDate3 = simpleDateFormat.parse("2013-01-06 00:00");
		rn3.setCloseDate(closeDate3);
		rn3 = hDouRequests.update(rn3);
		
		
		params.setEffectiveDate(closeDate3);
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)2, (long)searchCount);
		
		params.setEffectiveDate(simpleDateFormat.parse("2013-01-05 23:50"));
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)3, (long)searchCount);
		
	}
	
	
	@Test
	public void testGetSearchCountWithEffectiveDateByAge() throws ParseException{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		HDouRequest rn1 = createReq("2013-01-01 00:00", "2013-01-03 00:00");
		
		DouRequestsSearchParams params = new DouRequestsSearchParams();
		params.setEffectiveDate(simpleDateFormat.parse("2013-01-03 00:00"));
		Integer searchCount ;
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		params.ageFrom = 0;
		params.ageTo = 2;
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		params.ageFrom = 2;
		params.ageTo = 3;
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)0, (long)searchCount);
		
	}
	
	@Test
	public void testGetSearchCountWithEffectiveDateByStatus() throws ParseException{
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		HDouRequest rn1 = createReq(null, "2013-01-03 00:00");
		
		DouRequestsSearchParams params = new DouRequestsSearchParams();
		params.setEffectiveDate(simpleDateFormat.parse("2013-01-03 00:00"));
		params.orderStatus = OrderStatus.ACTIVE_QUEUE;
		Integer searchCount ;
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)1, (long)searchCount);
		

		HRegister register = createRegister();
		createRegisterS(register, rn1, Action.GIVE_PERMIT);
		register.setRegisterDate(simpleDateFormat.parse("2013-01-05 00:00"));
		register.setReceivePermitDueDate(simpleDateFormat.parse("2013-02-05 00:00"));
		register = hRegisters.update(register);
		hRegisters.changeStatus(register.getUId(), RegisterStatus.SECOND);
		register = hRegisters.changeStatus(register.getUId(), RegisterStatus.THIRD);
		
		params.setEffectiveDate(simpleDateFormat.parse("2013-01-06 00:00"));
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)0, (long)searchCount);
		
		
		
		params.setEffectiveDate(simpleDateFormat.parse("2013-01-04 00:00"));
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		
		
		
		HRegister register2 = createRegister();
		createRegisterS(register2, rn1, Action.BACK_TO_QUEUE);
		register2.setRegisterDate(simpleDateFormat.parse("2013-01-07 00:00"));
		register2.setReceivePermitDueDate(simpleDateFormat.parse("2013-02-05 00:00"));
		register2 = hRegisters.update(register2);
		hRegisters.changeStatus(register2.getUId(), RegisterStatus.SECOND);
		register2 = hRegisters.changeStatus(register2.getUId(), RegisterStatus.THIRD);
		
		params.setEffectiveDate(simpleDateFormat.parse("2013-01-07 00:00"));
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		params.setEffectiveDate(simpleDateFormat.parse("2013-01-06 23:00"));
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)0, (long)searchCount);
		
		
		HRegister register3 = createRegister();
		createRegisterS(register3, rn1, Action.REFUSAL);
		register3.setRegisterDate(simpleDateFormat.parse("2013-01-09 00:00"));
		register3.setReceivePermitDueDate(simpleDateFormat.parse("2013-02-05 00:00"));
		register3 = hRegisters.update(register3);
		hRegisters.changeStatus(register3.getUId(), RegisterStatus.SECOND);
		register3 = hRegisters.changeStatus(register3.getUId(), RegisterStatus.THIRD);
		
		params.setEffectiveDate(simpleDateFormat.parse("2013-01-09 00:00"));
		params.orderStatus = OrderStatus.REJECT;
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
	}
	
	
	
	@Test
	public void testGetSearchCountWithEffectiveDate() throws ParseException{
		HDouRequest rn1 = createReq(null, "2013-01-03 00:00");
		HDouRequest rn2 = createReq(null, "2013-01-04 00:00");
		HDouRequest rn3 = createReq(null, "2013-01-05 00:00");
		
		
		DouRequestsSearchParams params = new DouRequestsSearchParams();
		
		Integer searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)3, (long)searchCount);
		
		params.setEffectiveDate(rn1.getRegDate());
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		params.setEffectiveDate(rn2.getRegDate());
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)2, (long)searchCount);
		
		params.setEffectiveDate(rn3.getRegDate());
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)3, (long)searchCount);
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date closeDate3 = simpleDateFormat.parse("2013-01-06 00:00");
		rn3.setCloseDate(closeDate3);
		rn3 = hDouRequests.update(rn3);
		
		
		params.setEffectiveDate(closeDate3);
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)2, (long)searchCount);
		
		params.setEffectiveDate(simpleDateFormat.parse("2013-01-05 23:50"));
		searchCount = hDouRequests.getSearchCountWithEffectiveDate(params);
		Assert.assertEquals((long)3, (long)searchCount);
	}
	
	
	@Test
	public void testSearch(){
		/**
		 * Этот поиск ничего не вернет...Просто проверка генерируемого HQL
		 */
		DouRequestsSearchParams params = new DouRequestsSearchParams();
	
		params.ageFrom=1;
		params.ageTo=2;
		params.birthday = DateFilter.build(new Date(), new Date());
		params.exact_privilege = 123l;
		params.firstname =  new StringFilter("123");
		params.lastname = new StringFilter("234");
		params.middlename = new StringFilter("345");
		params.minRn= "2012-2012";
		params.orderStatus = OrderStatus.ACTIVE_QUEUE;
		params.pattern = "456";
		params.phone = true;
		params.plainYear= new IntegerFilter(2013);
		params.recordColor = RecordColor.GRAY;
		params.regDate = new DateFilter(DateFilterType.EQUALS, new Date());
		params.region = "123";
		params.sex = Sex.M;
		params.searchedDous = Arrays.asList(12l,23l);
		hDouRequests.search(params);
	}
	
	
	@Test
	public void testIfFilteringIgnoreCase(){
		HDouRequest rn3 = createReq(null, null);
		String surname = "АлЕкСаНдРоВ";
		rn3.getChild().setSurname(surname);
		physicalsRemote.update(rn3.getChild());
		
		DouRequestsSearchParams params = new DouRequestsSearchParams();
		params.lastname = new StringFilter(StringFilterType.EQUALS_IC, surname);
		Integer searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		params.lastname = new StringFilter(StringFilterType.EQUALS_IC, surname.toLowerCase());		
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		params.lastname = new StringFilter(StringFilterType.EQUALS_IC, surname.toUpperCase());		
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		
		params.lastname = new StringFilter(surname);
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		params.lastname = new StringFilter(surname.toLowerCase());		
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)0, (long)searchCount);
		
		params.lastname = new StringFilter( surname.toUpperCase());		
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)0, (long)searchCount);
		
	}
	
	
	@Test
	public void testStringFilter(){
		HDouRequest rn3 = createReq(null, null);
		String surname = "АлЕкСаНдРоВ";
		rn3.getChild().setSurname(surname);
		physicalsRemote.update(rn3.getChild());
		
		DouRequestsSearchParams params = new DouRequestsSearchParams();
		params.lastname = new StringFilter("кСаНдР");
		Integer searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)0, (long)searchCount);
		
		params.lastname = new StringFilter(StringFilterType.CONTAINS, "кСаНдР");		
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		params.lastname = new StringFilter(StringFilterType.STARTS_WITH, "кСаНдР");		
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)0, (long)searchCount);
		
		
		params.lastname = new StringFilter(StringFilterType.STARTS_WITH, "АлЕк");
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)1, (long)searchCount);
		
		params.lastname = new StringFilter(StringFilterType.ENDS_WITH, "АлЕк");
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)0, (long)searchCount);
		
		
		params.lastname = new StringFilter(StringFilterType.ENDS_WITH, "НдРоВ");
		searchCount = hDouRequests.getSearchCount(params);
		Assert.assertEquals((long)1, (long)searchCount);
	}

	
	@Ignore
	@Test
	public void testVersioningForStatus(){
		HRegister register = createRegister();
		register.setAgereprocessdate(new Date());
		HDouRequest req = createReq(null, null);
		Assert.assertEquals(org.itx.jbalance.l0.h.HDouRequest.Status.WAIT, req.getStatus());
		createRegisterS(register, req, Action.GIVE_PERMIT);
		hRegisters.changeStatus(register.getUId(), RegisterStatus.SECOND);
		
		List<HDouRequest> history = hDouRequests.getHistory(req);
		Assert.assertEquals(1, history.size());
		System.out.println("?????????????????????????????????????????????????");
		
		req = hDouRequests.getByUId(req.getUId(), HDouRequest.class);
		Assert.assertEquals(org.itx.jbalance.l0.h.HDouRequest.Status.WAIT, req.getStatus());
		hRegisters.changeStatus(register.getUId(), RegisterStatus.THIRD);
		
		
		req = hDouRequests.getByUId(req.getUId(), HDouRequest.class);
		Assert.assertEquals(org.itx.jbalance.l0.h.HDouRequest.Status.PERMIT, req.getStatus());
		List<HDouRequest> history2 = hDouRequests.getHistory(req);
		Assert.assertEquals(2, history2.size());
		
	}
}
