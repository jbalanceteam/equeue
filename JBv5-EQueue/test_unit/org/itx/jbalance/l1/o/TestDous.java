package org.itx.jbalance.l1.o;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;
import org.itx.jbalance.AbstractEqueueTestEJB;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Region;
import org.junit.Test;
/**
 * JUnit тесты для DousBean
 * @author apv
 *
 */
public class TestDous extends AbstractEqueueTestEJB {
	protected Log log= new Log4JLogger(getClass().getName());
	

	@Test(expected=RuntimeException.class)
	public void testFailCreate(){
		Dou dou = new Dou();
		dou = dousRemote.create(dou);
	}
	
	@Test
	public void testCreste(){
		Region region = new Region();
		region = regionsRemote.create(region);
		
		Dou dou = new Dou();
		dou.setRegion(region);
		dou.setNumber("x");
		dou = dousRemote.create(dou);
	}
	
	
	@Test
	public void testGetByIdDou(){
		String douNumber = "1";
		Assert.assertNull(dousRemote.getByNumber(douNumber));
		createDou(douNumber);
		Dou byIdDou = dousRemote.getByNumber(douNumber);
		Assert.assertNotNull(byIdDou);
		Assert.assertEquals(douNumber, byIdDou.getNumber());
	}

}
