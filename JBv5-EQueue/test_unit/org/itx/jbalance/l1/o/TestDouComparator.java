package org.itx.jbalance.l1.o;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;
import org.itx.jbalance.l0.o.Dou;
import org.junit.Test;
/**
 * @author apv
 *
 */
public class TestDouComparator {
	protected Log log= new Log4JLogger(getClass().getName());
	


	@Test
	public void testDouComparator(){
		Assert.assertTrue(Dou.douNumbersComparator.compare("", "") == 0);
		Assert.assertTrue(Dou.douNumbersComparator.compare("12", "12") == 0);
		Assert.assertTrue(Dou.douNumbersComparator.compare("12", "1/2") > 0);
		Assert.assertTrue(Dou.douNumbersComparator.compare("-23", "-23") == 0);
		Assert.assertTrue(Dou.douNumbersComparator.compare("abc", "abc") == 0);
//		Assert.assertTrue(Dou.douNumbersComparator.compare("04", "4") == 0);
		Assert.assertTrue(Dou.douNumbersComparator.compare("40", "04") != 0);
		Assert.assertTrue(Dou.douNumbersComparator.compare("12", "23") < 0);
		Assert.assertTrue(Dou.douNumbersComparator.compare("12", "12/3") < 0);
		
		Assert.assertTrue(Dou.douNumbersComparator.compare("1", "1-1") < 0);
		Assert.assertTrue(Dou.douNumbersComparator.compare("2", "1-1") > 0);
		
	}
	
	@Test
	public void testToNumber(){
		Assert.assertEquals((Integer)12, (Integer)Dou.toNumber("12/1") );
		Assert.assertEquals((Integer)1, (Integer)Dou.toNumber("1") );
		Assert.assertEquals((Integer)2, (Integer)Dou.toNumber("2/2") );
		Assert.assertEquals((Integer)123, (Integer)Dou.toNumber("123") );
		Assert.assertEquals((Integer)312, (Integer)Dou.toNumber("312/23") );

		
	}
	
}
