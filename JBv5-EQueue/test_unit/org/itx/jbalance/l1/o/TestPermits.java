package org.itx.jbalance.l1.o;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.itx.jbalance.AbstractEqueueTestEJB;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HDouRequest.Status;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Juridical;
import org.itx.jbalance.l0.o.Permit;
import org.itx.jbalance.l0.o.Permit.PermitStatus;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l1.api.RegisterSearchParams;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.DateFilterType;
import org.itx.jbalance.l1.api.filters.IntegerFilter;
import org.itx.jbalance.l1.api.filters.LongFilter;
import org.itx.jbalance.l1.utils.Str;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestPermits extends AbstractEqueueTestEJB {
	
	Permit permit;
	
	@Before
	public void prepare(){
		HDouRequest hDouRequest = createReq(null, null);
		HRegister hRegister = createRegister();
		SRegister sRegister = createRegisterS(hRegister, hDouRequest, Action.GIVE_PERMIT);
		Privilege privelage = null;
		Date permitDate = new Date();;
		PermitStatus permitStatus = PermitStatus.ISSUED;
		permit = permits.create(new Permit(hDouRequest.getChild(), privelage , hDouRequest, hRegister.getDnumber() + "-" + sRegister.getNumber() , 
				permitDate , permitStatus , sRegister, sRegister.getDou()));
	}
	
	/**
	 * Просто проверяет создание
	 */
	@Test
	public void testCreate(){
		Assert.assertNotNull(permit.getAge());
	}
	
	/**
	 * Путевка должна автоматически создаваться при перевод протокола на 3-й этап
	 */
	@Test
	public void testAutoCreation(){
		Integer allCount0 = permits.getAllCount();
		HDouRequest hDouRequest = createReq(null, null);
		hDouRequest.getChild().setSurname("Петров");
		physicalsRemote.update(hDouRequest.getChild());
		Privilege privelege = createPrivelege("123");
		hDouRequest.setPrivilege(privelege);
		hDouRequest = hDouRequests.update(hDouRequest);
		HRegister hRegister = createRegister();
		SRegister sRegister = createRegisterS(hRegister, hDouRequest, Action.GIVE_PERMIT);
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.SECOND);
		Integer allCount1 = permits.getAllCount();
		Assert.assertEquals(allCount0 , allCount1);
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.THIRD);
		Integer allCount2 = permits.getAllCount();
		Assert.assertEquals((long)allCount1 + 1, (long)allCount2);
		
		GetPermitsRequest getPermitsRequest = new GetPermitsRequest();
		getPermitsRequest.setSearchByAllTextFields(hDouRequest.getChild().getSurname());
		List<Permit> search = permits.search(getPermitsRequest);
		Assert.assertEquals(1, search.size());
		
		Permit createdPermit = search.get(0);
		Assert.assertFalse(Str.empty(createdPermit.getNumber()));
		Assert.assertNotNull(createdPermit.getAge());
		Assert.assertNotNull(createdPermit.getDou());
		Assert.assertNotNull(createdPermit.getPermitDate());
		Assert.assertEquals(PermitStatus.ISSUED, createdPermit.getPermitStatus());
		Assert.assertNotNull(createdPermit.getSRegister());
		Assert.assertNotNull(createdPermit.getPrivilege());
		Assert.assertEquals(privelege, createdPermit.getPrivilege());
	}
	
	
	
	
	/**
	 * Все путевки выданные ребенку должны помечаться, как нереализованные
	 */
	@Test
	public void testStatusChanging(){
		
		Integer allCount0 = permits.getAllCount();
		
		HDouRequest hDouRequest = permit.getHDouRequest();
		hDouRequest.getChild().setSurname("Петров");
		physicalsRemote.update(hDouRequest.getChild());
		hDouRequest = hDouRequests.update(hDouRequest);
		HRegister hRegister = createRegister();
		SRegister sRegister = createRegisterS(hRegister, hDouRequest, Action.GIVE_PERMIT);
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.SECOND);
		Integer allCount1 = permits.getAllCount();
		Assert.assertEquals(allCount0 , allCount1);
		hRegisters.changeStatus(hRegister.getUId(), RegisterStatus.THIRD);
		Integer allCount2 = permits.getAllCount();
		Assert.assertEquals((long)allCount1 + 1, (long)allCount2);
		
		GetPermitsRequest getPermitsRequest = new GetPermitsRequest();
		getPermitsRequest.setSearchByAllTextFields(hDouRequest.getChild().getSurname());
		List<Permit> search = permits.search(getPermitsRequest);
		Assert.assertEquals(2, search.size());
		
		Permit createdPermit = search.get(0);
		Assert.assertFalse(Str.empty(createdPermit.getNumber()));
		Assert.assertNotNull(createdPermit.getAge());
		Assert.assertNotNull(createdPermit.getDou());
		Assert.assertNotNull(createdPermit.getPermitDate());
		Assert.assertEquals(PermitStatus.ISSUED, createdPermit.getPermitStatus());
		Assert.assertNotNull(createdPermit.getSRegister());
		
		createdPermit = search.get(1);
		Assert.assertFalse(Str.empty(createdPermit.getNumber()));
		Assert.assertNotNull(createdPermit.getAge());
		Assert.assertNotNull(createdPermit.getDou());
		Assert.assertNotNull(createdPermit.getPermitDate());
		Assert.assertEquals(PermitStatus.NOT_IMPLEMENTED, createdPermit.getPermitStatus());
		Assert.assertNotNull(createdPermit.getSRegister());
		
	}
	
	
	@Test
	public void testGetAllCount(){
		int allCount = permits.getAllCount();
		Assert.assertEquals(1, allCount);
	}
	
	/**
	 * Этот поиск ничего не вернет...Просто проверка генерируемого HQL
	 */
	@Test
	public void testSearch(){
		permits.search(new GetPermitsRequest());
		
		GetPermitsRequest getPermitsRequest = new GetPermitsRequest();
		getPermitsRequest.setAge(IntegerFilter.build(1,12));
		getPermitsRequest.setBirthday(new DateFilter(DateFilterType.LT, new Date()));
		getPermitsRequest.setEffectiveDate(new Date());
		getPermitsRequest.setExact_privilege(new LongFilter(123l));
		getPermitsRequest.setFrom(1);
		getPermitsRequest.setTo(2);
		getPermitsRequest.setPermitDate(new DateFilter(DateFilterType.LT, new Date()));
//		getPermitsRequest.setPrivilege(true);
		getPermitsRequest.setSearchByAllTextFields("string");
		getPermitsRequest.setDous(new LongFilter(Arrays.asList(12l,23l,34l)));
		List<Permit> search = permits.search(getPermitsRequest);
		
		Integer searchCount = permits.getSearchCount(getPermitsRequest);
		Assert.assertEquals((long)search.size(), (long)searchCount);
	}
	
	
	@Test
	public void testSearchByAge(){
		permit.setAge(24);
		permit = permits.update(permit);
		GetPermitsRequest getPermitsRequest = new GetPermitsRequest();
		getPermitsRequest.setAge(new IntegerFilter(1));
		List<Permit> search = permits.search(getPermitsRequest);
		Assert.assertTrue(search.isEmpty());
		
		getPermitsRequest.setAge(IntegerFilter.build(24,24));
		search = permits.search(getPermitsRequest);
		Assert.assertTrue(! search.isEmpty());
		
		Assert.assertEquals(permit, search.get(0));
		
		Integer searchCount = permits.getSearchCount(getPermitsRequest);
		Assert.assertEquals((long)search.size(), (long)searchCount);
	}
	
	
	
	
	@Test
	public void testSearchByName(){
		GetPermitsRequest getPermitsRequest = new GetPermitsRequest();
		getPermitsRequest.setSearchByAllTextFields("qwertyuiop");
		List<Permit> search = permits.search(getPermitsRequest);
		Assert.assertTrue(search.isEmpty());
		
		getPermitsRequest.setSearchByAllTextFields(permit.getChild().getSurname());
		search = permits.search(getPermitsRequest);
		Assert.assertTrue(! search.isEmpty());
		
		Assert.assertEquals(permit, search.get(0));
		
		Integer searchCount = permits.getSearchCount(getPermitsRequest);
		Assert.assertEquals((long)search.size(), (long)searchCount);
	}
	
	
	@Test
	public void testSearchByBirthday(){
		GetPermitsRequest getPermitsRequest = new GetPermitsRequest();
		getPermitsRequest.setBirthday(new DateFilter(DateFilterType.NULL));
		List<Permit> search = permits.search(getPermitsRequest);
		Assert.assertTrue(search.isEmpty());
		
		Date birthday = permit.getChild().getBirthday();
		getPermitsRequest.setBirthday(DateFilter.build(birthday, permit.getChild().getBirthday()));
		search = permits.search(getPermitsRequest);
		Assert.assertTrue(! search.isEmpty());
		
		Assert.assertEquals(permit, search.get(0));
		
		Integer searchCount = permits.getSearchCount(getPermitsRequest);
		Assert.assertEquals((long)search.size(), (long)searchCount);
	}
	
	
	
	@Test
	public void testSearchByPermitDate(){
		GetPermitsRequest getPermitsRequest = new GetPermitsRequest();
		getPermitsRequest.setPermitDate(new DateFilter(DateFilterType.NULL));
		List<Permit> search = permits.search(getPermitsRequest);
		Assert.assertTrue(search.isEmpty());
		
		getPermitsRequest.setPermitDate(DateFilter.build(permit.getPermitDate(), permit.getPermitDate()));
		search = permits.search(getPermitsRequest);
		Assert.assertTrue(! search.isEmpty());
		
		Assert.assertEquals(permit, search.get(0));
		
		Integer searchCount = permits.getSearchCount(getPermitsRequest);
		Assert.assertEquals((long)search.size(), (long)searchCount);
	}
	
	@Test
	public void testSearchByDou(){
		GetPermitsRequest getPermitsRequest = new GetPermitsRequest();
		getPermitsRequest.setDous(new LongFilter(-123l));
		List<Permit> search = permits.search(getPermitsRequest);
		Assert.assertTrue(search.isEmpty());
		
		getPermitsRequest.setDous(new LongFilter(permit.getDou().getUId()));
		search = permits.search(getPermitsRequest);
		Assert.assertTrue(! search.isEmpty());
		
		Assert.assertEquals(permit, search.get(0));
		
		Integer searchCount = permits.getSearchCount(getPermitsRequest);
		Assert.assertEquals((long)search.size(), (long)searchCount);
	}
	
	
	
	
	@Test
	public void testSearchByChild(){
		GetPermitsRequest getPermitsRequest = new GetPermitsRequest();
		getPermitsRequest.setChildUid(new LongFilter(-123l));
		List<Permit> search = permits.search(getPermitsRequest);
		Assert.assertTrue(search.isEmpty());
		
		getPermitsRequest.setChildUid(new LongFilter(permit.getChild().getUId()));
		search = permits.search(getPermitsRequest);
		Assert.assertTrue(! search.isEmpty());
		
		Assert.assertEquals(permit, search.get(0));
		
		Integer searchCount = permits.getSearchCount(getPermitsRequest);
		Assert.assertEquals((long)search.size(), (long)searchCount);
	}
	
	
	@Test
	public void testSearchByPrivelege(){
		Privilege createPrivelege = createPrivelege("Hey");
		permit.setPrivilege(createPrivelege);
		permit = permits.update(permit);
		
		GetPermitsRequest getPermitsRequest = new GetPermitsRequest();
		List<Permit> search;
		
		getPermitsRequest.setPrivilege(false);
		search = permits.search(getPermitsRequest);
		Assert.assertTrue(search.isEmpty());
		
		getPermitsRequest.setPrivilege(true);
		search = permits.search(getPermitsRequest);
		Assert.assertTrue(! search.isEmpty());
		
		Assert.assertEquals(permit, search.get(0));
		
		
		getPermitsRequest.setPrivilege(null);
		getPermitsRequest.setExact_privilege(new LongFilter(createPrivelege.getUId() - 1));
		search = permits.search(getPermitsRequest);
		Assert.assertTrue(search.isEmpty());
		
		getPermitsRequest.setExact_privilege(new LongFilter(createPrivelege.getUId()));
		search = permits.search(getPermitsRequest);
		Assert.assertTrue(! search.isEmpty());
		
		Assert.assertEquals(permit, search.get(0));
		
		Integer searchCount = permits.getSearchCount(getPermitsRequest);
		Assert.assertEquals((long)search.size(), (long)searchCount);
		
	}
	
	

}
