package org.itx.jbalance.l1.o;

import java.util.List;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;
import org.itx.jbalance.AbstractEqueueTestEJB;
import org.itx.jbalance.l0.o.DOUGroups;
import org.junit.Test;
/**
 * 
 * @author apv
 */
public class TestDOUGroups extends AbstractEqueueTestEJB {
	protected Log log= new Log4JLogger(getClass().getName());

	
	@Test
	public void testGetAll(){
		List<DOUGroups> all = douGroupsRemote.getAll();
		Assert.assertNotNull(all);
		Assert.assertTrue(all.size() > 0);
		
		DOUGroups byId1 = douGroupsRemote.getById(1);
		Assert.assertNotNull(byId1);
		Assert.assertEquals(1, byId1.getId());
	}

	
	@Test
	public void testGetById(){
		DOUGroups byId1 = douGroupsRemote.getById(1);
		Assert.assertNotNull(byId1);
		Assert.assertEquals(1, byId1.getId());
	}
}
