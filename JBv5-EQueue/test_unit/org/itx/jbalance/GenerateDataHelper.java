package org.itx.jbalance;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;

public class GenerateDataHelper {
	
	public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	public static SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	public static Physical generateChild(String birthday) {
		Physical physical = new Physical();
		physical.setName("123");
		physical.setRealName("123");
		physical.setSurname("123");
		if(birthday!=null) {
			physical.setBirthday(parseDateStr(birthday));
		} else {
			physical.setBirthday(new Date());
		}
		physical.setPhone("123");
		return physical;
	}
	
	private static Date parseDateStr(String str){
		try {
			return simpleDateFormat.parse(str);
		} catch (ParseException e) {
			try {
				return simpleDateFormat2.parse(str);
			} catch (ParseException e1) {
				throw new RuntimeException(e);
			}
		}
	}
	
	
	public static BirthSertificate generateBS() {
		BirthSertificate birthSertificate = new BirthSertificate();
		birthSertificate.setSex(Sex.M);
		birthSertificate.setSeria("1");
		birthSertificate.setNumber("1");
		return birthSertificate;
	}
	
	public static HDouRequest generateReq(String birthday, String regDate) {
		HDouRequest douRequest = new HDouRequest();
		douRequest.setFoundation("Основание");
		if(regDate!=null) {
			douRequest.setRegDate(parseDateStr(regDate));
		} else {
			douRequest.setRegDate(new Date());
		}
		
		douRequest.setChild(generateChild(birthday));
		douRequest.setBirthSertificate(generateBS());
		return douRequest;
	}

	public static HRegister generateRegister() {
		HRegister hRegister = new HRegister();
		hRegister.setFoundation("qwe");
		hRegister.setMember1("Member1");
		hRegister.setRegisterDate(new Date());
		Calendar instance = Calendar.getInstance();
		instance.add(Calendar.DATE, 11);
		hRegister.setReceivePermitDueDate(instance.getTime());
		return hRegister;
	}
}
