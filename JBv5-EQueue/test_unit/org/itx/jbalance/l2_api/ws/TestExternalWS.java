package org.itx.jbalance.l2_api.ws;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;
import org.itx.jbalance.AbstractEqueueTestEJB;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l2_api.dto.RequestInfo;
import org.itx.jbalance.l2_api.dto.RequestOrderInfo;
import org.itx.jbalance.l2_api.dto.submission.SubmissionPrerequisiteInfo;
import org.junit.Test;

public class TestExternalWS extends AbstractEqueueTestEJB {
	protected Log log = new Log4JLogger(getClass().getName());

	
	@Test
	public void getSubmissionPrerequisite() {
		Dou dou = createDou(123);
		SubmissionPrerequisiteInfo submissionPrerequisite = externalWS.getSubmissionPrerequisite();
		Assert.assertNotNull(submissionPrerequisite);
		Assert.assertNotNull(submissionPrerequisite.getDous());
		Assert.assertEquals(1, submissionPrerequisite.getDous().size());
		Assert.assertEquals(dou.getUId(), submissionPrerequisite.getDous().get(0).getUId());
	}
	
	/**
	 * Тестирует возвращение данных протокола из метода getRequestInfo
	 * #381: Сервис проверки очереди ВСЕГДА показывает дату и номер последнего протокола
	 */
	@Test
	public void testGetRegisterInfo() {
		HDouRequest req1 = createReq("2013-01-01", "2013-01-01");
		HRegister hRegister1 = createRegister();
		SRegister sRegister1 = createRegisterS(hRegister1, req1, Action.GIVE_PERMIT);

		hRegisters.changeStatus(hRegister1.getUId(), RegisterStatus.SECOND);
		hRegisters.changeStatus(hRegister1.getUId(), RegisterStatus.THIRD);
		
		RequestOrderInfo requestInfo = externalWS.getRequestInfo(req1.toString(), req1.getChild().getBirthday());
		Assert.assertEquals(requestInfo.getRn(), req1.toString());
		Assert.assertEquals(requestInfo.getProtokolNum(), hRegister1.getDnumber()+"");
		DateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Assert.assertEquals(dateFormat.format(requestInfo.getProtokolDate()), dateFormat.format(hRegister1.getRegisterDate()));
		
		
		HDouRequest req2 = createReq("2013-01-01", "2013-01-02");
		HRegister hRegister2 = createRegister();
		SRegister sRegister2 = createRegisterS(hRegister2, req2, Action.GIVE_PERMIT);

		hRegisters.changeStatus(hRegister2.getUId(), RegisterStatus.SECOND);
		hRegisters.changeStatus(hRegister2.getUId(), RegisterStatus.THIRD);
		
		requestInfo = externalWS.getRequestInfo(req1.toString(), req1.getChild().getBirthday());
		Assert.assertEquals(requestInfo.getRn(), req1.toString());
		Assert.assertEquals(hRegister1.getDnumber()+"", requestInfo.getProtokolNum());
		Assert.assertEquals(dateFormat.format(hRegister1.getRegisterDate()), dateFormat.format(requestInfo.getProtokolDate()));
		
		
		
		RequestOrderInfo requestInfo2 = externalWS.getRequestInfo(req2.toString(), req2.getChild().getBirthday());
		Assert.assertEquals(requestInfo2.getRn(), req2.toString());
		Assert.assertEquals(hRegister2.getDnumber()+"", requestInfo2.getProtokolNum());
		Assert.assertEquals(dateFormat.format(hRegister2.getRegisterDate()), dateFormat.format(requestInfo2.getProtokolDate()));
	}
	
	
	@Test
	public void testGetRequestInfo() {
		HDouRequest req1 = createReq("2013-01-01", "2013-01-01");
		HDouRequest req2 = createReq("2013-01-01", "2013-01-02");
		HDouRequest req3 = createReq("2013-01-01", "2013-01-03");
		
		RequestOrderInfo requestInfo3 = externalWS.getRequestInfo(req3.toString(),req3.getChild().getBirthday());
		Assert.assertEquals(3, (int)requestInfo3.getOrder());
		Assert.assertEquals(3, (int)requestInfo3.getContinuousOrder());
		
		/**
		 * а вот тут появляется льготник
		 */
		HDouRequest req4 = createReq("2013-01-01", "2013-02-01");
		req4.setPrivilege(createPrivelege("инв"));
		req4 = hDouRequests.update(req4);
		
		RequestOrderInfo requestInfo4 = externalWS.getRequestInfo(req4.toString(),req4.getChild().getBirthday());
		List<RequestInfo> privius10 = requestInfo4.getPrivius10();
		for (RequestInfo requestInfo : privius10) {
			log.debug(">>>>>>>"+requestInfo.getRn());
		}
		Assert.assertEquals(1, (int)requestInfo4.getOrder());
		Assert.assertEquals(4, (int)requestInfo4.getContinuousOrder());
		
		/**
		 * Пришел 1-й, а в очереди 2-й, т.к. впереди льготник
		 */
		RequestOrderInfo requestInfo1 = externalWS.getRequestInfo(req1.toString(),req1.getChild().getBirthday());
		Assert.assertEquals(2, (int)requestInfo1.getOrder());
		Assert.assertEquals(1, (int)requestInfo1.getContinuousOrder());
		
		/**
		 * 2-й льготник
		 */
		HDouRequest req5 = createReq("2013-01-01", "2013-02-02");
		req5.setPrivilege(createPrivelege("инв 2"));
		req5 = hDouRequests.update(req5);
		
		RequestOrderInfo requestInfo5 = externalWS.getRequestInfo(req5.toString(),req5.getChild().getBirthday());
		for (RequestInfo requestInfo : requestInfo5.getPrivius10()) {
			log.debug(">>>>>>>"+requestInfo.getRn());
		}
		Assert.assertEquals(2, (int)requestInfo5.getOrder());
	}
	
	
	
	/**
	 * Без уче
	 */
	@Test
	public void testGetRequestInfoIgnorePrivileges() {
		HDouRequest req1 = createReq("2013-01-01", "2013-01-01");
		HDouRequest req2 = createReq("2013-01-01", "2013-01-02");
		HDouRequest req3 = createReq("2013-01-01", "2013-01-03");
		
		RequestOrderInfo requestInfo3 = externalWS.getRequestInfoIgnorePrivileges(req3.toString(),req3.getChild().getBirthday());
		Assert.assertEquals(3, (int)requestInfo3.getOrder());
		
		/**
		 * а вот тут появляется льготник
		 */
		HDouRequest req4 = createReq("2013-01-01", "2013-02-01");
		req4.setPrivilege(createPrivelege("инв"));
		req4 = hDouRequests.update(req4);
		
		RequestOrderInfo requestInfo4 = externalWS.getRequestInfoIgnorePrivileges(req4.toString(),req4.getChild().getBirthday());
		List<RequestInfo> privius10 = requestInfo4.getPrivius10();
		for (RequestInfo requestInfo : privius10) {
			log.debug(">>>>>>>"+requestInfo.getRn());
		}
		Assert.assertEquals(4, (int)requestInfo4.getOrder());
		

		RequestOrderInfo requestInfo1 = externalWS.getRequestInfoIgnorePrivileges(req1.toString(),req1.getChild().getBirthday());
		Assert.assertEquals(1, (int)requestInfo1.getOrder());
		
		
		/**
		 * 2-й льготник
		 */
		HDouRequest req5 = createReq("2013-01-01", "2013-02-02");
		req5.setPrivilege(createPrivelege("инв 2"));
		req5 = hDouRequests.update(req5);
		
		RequestOrderInfo requestInfo5 = externalWS.getRequestInfoIgnorePrivileges(req5.toString(),req5.getChild().getBirthday());
		for (RequestInfo requestInfo : requestInfo5.getPrivius10()) {
			log.debug(">>>>>>>"+requestInfo.getRn());
		}
		Assert.assertEquals(5, (int)requestInfo5.getOrder());
	}
	
}
