package org.itx.jbalance.l2_api.ws;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;
import org.itx.jbalance.AbstractEqueueTestEJB;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.DouRequestPrerequisiteDTO;
import org.itx.jbalance.l2_api.dto.PrivilegeDTO;
import org.itx.jbalance.l2_api.dto.RegionDTO;
import org.itx.jbalance.l2_api.dto.ResultDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

//import org.itx.jbalance.l2_api.dto.DouRequestDTO;

public class TestMfcWS extends AbstractEqueueTestEJB {
	protected Log log = new Log4JLogger(getClass().getName());

	Region region;
	Dou dou;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
	Physical child;
	BirthSertificate birthSertificate;
	@Before
	public void setUp() {
		log.debug("SetUp....");
		region = new Region();
		region.setName("Region1");
		region = regionsRemote.create(region);
		dou = new Dou();
		dou.setRegion(region);
		dou.setNumber("5");
		dou = dousRemote.create(dou);

		Privilege p1 = new Privilege();
		p1.setName("p1");
		Privilege p2 = new Privilege();
		p2.setName("p2");
		privilegesRemote.create(p1);
		privilegesRemote.create(p2);

		// создаем тестового очередника и сохраняем в очередь: 
		//с ним будем сравнивать всех остальных
		HDouRequest douRequest = new HDouRequest();
		//douRequest.setFoundation("основание");
		birthSertificate = new BirthSertificate();
		birthSertificate.setSex(Sex.M);
		birthSertificate.setSeria("XIX-АВС");
		birthSertificate.setNumber("123456");
		birthSertificate = birthSertificatesRemote.create(birthSertificate);
		
		douRequest.setBirthSertificate(birthSertificate);
		child = new Physical();
		child.setSurname("Budnikov");
		child.setRealName("Alexandr");
		child.setName("Alexandr");
		child.setPatronymic("Victorovich");
		child.setBirthday(parseDate("2011-01-01"));
		child = physicalsRemote.create(child);
		
		douRequest.setChild(child);
		douRequest = hDouRequests.create(douRequest);
	
	}

	private Date parseDate(String str) {
		try {
			return simpleDateFormat.parse(str);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}	
	}

		
	@Test
	public void testDouRequestPrerequisite() {
		DouRequestPrerequisiteDTO douRequestPrerequisite = mfcWS
				.getDouRequestPrerequisite();
		List<RegionDTO> regionsDTOs = douRequestPrerequisite.getRegionsDTOs();
		Assert.assertNotNull(regionsDTOs);
		Assert.assertTrue(!regionsDTOs.isEmpty());
		Assert.assertEquals(regionsDTOs.size(), 1);
		Assert.assertEquals(regionsDTOs.get(0).getUId(), region.getUId());

		List<DouDTO> dousDTOs = douRequestPrerequisite.getDousDTOs();
		Assert.assertNotNull(dousDTOs);
		Assert.assertTrue(!dousDTOs.isEmpty());
		Assert.assertEquals(dousDTOs.size(), 1);
		Assert.assertEquals(dousDTOs.get(0).getUId(), dou.getUId());

		List<PrivilegeDTO> privilegeDtos = douRequestPrerequisite
				.getPrivilegeDtos();
		Assert.assertNotNull(privilegeDtos);
		Assert.assertTrue(!privilegeDtos.isEmpty());
		Assert.assertEquals(2, privilegeDtos.size());

	}
	
	/**
	 * Должен выбросить RuntimeException если свидетельство дублируется
	 */
	@Test(expected=RuntimeException.class)
	public void testMakeDousRequestValidationError() {
		DouRequestDTO request = new DouRequestDTO();
		request.setLastName("Ыапро");
		request.setFirstName("Ыапро");
		request.setPatronymic("Ыапро");
		request.setBirthDate(child.getBirthday());
		request.setBsNumber(birthSertificate.getNumber());
		request.setBsSeria(birthSertificate.getSeria());
		request.setSex('m');
		mfcWS.makeDousRequest(request);
	}

	@Test
	public void testMakeDousRequest() {
		// TODO описать правильные параметра, проверить результат
		//
		// DouRequestDTO dousRequestDTO = new DouRequestDTO();
		// mfcWS.makeDousRequest(dousRequestDTO);
	}

	@Test
	public void testCheckDousRequest() {
		//
		ResultDTO resultDTO;
		
		DouRequestDTO requestDTOCorrect = new DouRequestDTO();
		requestDTOCorrect.setLastName(child.getSurname());
		requestDTOCorrect.setFirstName(child.getRealName());
		requestDTOCorrect.setPatronymic(child.getPatronymic());
		requestDTOCorrect.setBirthDate(child.getBirthday());
		requestDTOCorrect.setSex('m');
		requestDTOCorrect.setBirthDate(parseDate("2011-01-01"));
		resultDTO= mfcWS.checkDousRequest(requestDTOCorrect);
		Assert.assertNotNull(resultDTO);
		Assert.assertNotNull(resultDTO.getStatus());
		Assert.assertEquals(ResultDTO.Status.FAIL, resultDTO.getStatus());
		
//		#347 Добавить проверку на уникальность полей свидетельства о рождении в МФЦ
		requestDTOCorrect = new DouRequestDTO();
		requestDTOCorrect.setLastName("1");
		requestDTOCorrect.setFirstName("2");
		requestDTOCorrect.setPatronymic("3");
		requestDTOCorrect.setBirthDate(child.getBirthday());
		requestDTOCorrect.setBsSeria(birthSertificate.getSeria());
		requestDTOCorrect.setBsNumber(birthSertificate.getNumber());
		requestDTOCorrect.setBirthDate(parseDate("2011-01-01"));
		requestDTOCorrect.setSex('m');
		resultDTO= mfcWS.checkDousRequest(requestDTOCorrect);
		Assert.assertNotNull(resultDTO);
		Assert.assertNotNull(resultDTO.getStatus());
		Assert.assertEquals(ResultDTO.Status.FAIL, resultDTO.getStatus());

		DouRequestDTO requestDTOIncorrectLastName = new DouRequestDTO();
		requestDTOIncorrectLastName.setLastName("Gudnikov");
		requestDTOIncorrectLastName.setFirstName("Alexandr");
		requestDTOIncorrectLastName.setPatronymic("Victorovich");
		requestDTOIncorrectLastName.setBirthDate(parseDate("2011-01-01"));
		requestDTOIncorrectLastName.setBsDate(parseDate("2011-01-01"));
		requestDTOIncorrectLastName.setSex('m');
		resultDTO= mfcWS.checkDousRequest(requestDTOIncorrectLastName);
		Assert.assertEquals(ResultDTO.Status.SUCCESS,resultDTO.getStatus());
		
		DouRequestDTO requestDTOIncorrectFirstName = new DouRequestDTO();
		requestDTOIncorrectFirstName.setLastName("Budnikov");
		requestDTOIncorrectFirstName.setFirstName("Alekandr");
		requestDTOIncorrectFirstName.setPatronymic("Victorovich");
		requestDTOIncorrectFirstName.setBirthDate(parseDate("2011-01-01"));
		requestDTOIncorrectFirstName.setBsDate(parseDate("2011-01-01"));
		requestDTOIncorrectFirstName.setSex('m');
		resultDTO= mfcWS.checkDousRequest(requestDTOIncorrectFirstName);
		Assert.assertEquals(ResultDTO.Status.SUCCESS,resultDTO.getStatus());

		DouRequestDTO requestDTOIncorrectPatronymic = new DouRequestDTO();
		requestDTOIncorrectPatronymic.setLastName("Budnikov");
		requestDTOIncorrectPatronymic.setFirstName("Alexandr");
		requestDTOIncorrectPatronymic.setPatronymic("Mihailvich");
		requestDTOIncorrectPatronymic.setBirthDate(parseDate("2011-01-01"));
		requestDTOIncorrectPatronymic.setBsDate(parseDate("2011-01-01"));
		requestDTOIncorrectPatronymic.setSex('m');
		resultDTO= mfcWS.checkDousRequest(requestDTOIncorrectPatronymic);
		Assert.assertEquals(ResultDTO.Status.SUCCESS,resultDTO.getStatus());

		DouRequestDTO requestDTOIncorrectBirthDate = new DouRequestDTO();
		requestDTOIncorrectBirthDate.setLastName("Budnikov");
		requestDTOIncorrectBirthDate.setFirstName("Alexandr");
		requestDTOIncorrectBirthDate.setPatronymic("Victorovich");
		requestDTOIncorrectBirthDate.setBirthDate(parseDate("2011-01-02"));
		requestDTOIncorrectBirthDate.setBsDate(parseDate("2011-01-02"));
		requestDTOIncorrectBirthDate.setSex('m');
		resultDTO= mfcWS.checkDousRequest(requestDTOIncorrectBirthDate);
		Assert.assertEquals(ResultDTO.Status.SUCCESS,resultDTO.getStatus());

		DouRequestDTO requestDTOIncorrectSex = new DouRequestDTO();
		requestDTOIncorrectSex.setLastName("Budnikov");
		requestDTOIncorrectSex.setFirstName("Alexandr");
		requestDTOIncorrectSex.setPatronymic("Victorovich");
		requestDTOIncorrectSex.setBirthDate(parseDate("2011-01-01"));
		requestDTOIncorrectSex.setBsDate(parseDate("2011-01-02"));
		requestDTOIncorrectSex.setSex('f');
		resultDTO= mfcWS.checkDousRequest(requestDTOIncorrectSex);
		Assert.assertEquals(ResultDTO.Status.SUCCESS,resultDTO.getStatus());

	}
	
	/**
	 * #403 Ошибка при работе в mfc
	 * разрешено сохранять запись, когда дата рождения, обльше даты выдачи, давай сделаем меньше или равно см скрин в почте
	 */
	@Test
	public void testWrorgDates() {
		ResultDTO resultDTO;
		
		DouRequestDTO requestDTOCorrect = new DouRequestDTO();
		requestDTOCorrect.setLastName("Vasiliy");
		requestDTOCorrect.setFirstName("Pupkin");
		requestDTOCorrect.setPatronymic("Qwe");
		requestDTOCorrect.setBirthDate(parseDate("2011-01-02"));
		requestDTOCorrect.setBsDate(   parseDate("2011-01-01"));
		requestDTOCorrect.setSex('f');
		resultDTO= mfcWS.checkDousRequest(requestDTOCorrect);
		Assert.assertNotNull(resultDTO);
		Assert.assertNotNull(resultDTO.getStatus());
		Assert.assertEquals(ResultDTO.Status.FAIL, resultDTO.getStatus());
		
		requestDTOCorrect.setBirthDate(parseDate("2011-01-01"));
		requestDTOCorrect.setBsDate(   parseDate("2011-01-01"));
		resultDTO= mfcWS.checkDousRequest(requestDTOCorrect);
		Assert.assertNotNull(resultDTO);
		Assert.assertNotNull(resultDTO.getStatus());
		Assert.assertEquals(ResultDTO.Status.SUCCESS, resultDTO.getStatus());
	}

}
