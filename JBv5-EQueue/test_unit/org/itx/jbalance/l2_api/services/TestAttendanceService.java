package org.itx.jbalance.l2_api.services;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;
import org.itx.jbalance.AbstractEqueueTestEJB;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l0.s.SRegister.Foundation;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsResult;
import org.itx.jbalance.l2_api.dto.attendance.PermitDTO;
import org.junit.Before;
import org.junit.Test;


public class TestAttendanceService extends AbstractEqueueTestEJB {
	protected Log log = new Log4JLogger(getClass().getName());


	@Before
	public void setUp() {
		log.debug("SetUp....");
		HRegister hRegister = createRegister();
		SRegister spec = new SRegister();
		spec.setDouRequest(createReq(null, null));
		spec.setFoundation(Foundation.DEFAULT);
		spec.setRnAction(Action.GIVE_PERMIT);
		spec.setDou(createDou(123));
		hRegisters.createSpecificationLine(spec, hRegister);
	
	}

	
	@Test
	public void testDouRequestPrerequisite() {
		GetPermitsResult result = attendanceService.getPermits(new GetPermitsRequest());
		List<PermitDTO> permits = result.getPermits();
		for (PermitDTO permitDTO : permits) {
			log.debug(permitDTO);
		}
	}
}
