package org.itx.jbalance.l2_api.services;

import java.util.List;

import junit.framework.Assert;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.impl.Log4JLogger;
import org.itx.jbalance.AbstractEqueueTestEJB;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l0.s.SRegister.Foundation;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsResult;
import org.itx.jbalance.l2_api.dto.attendance.PermitDTO;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterPrerequisiteInfo;
import org.junit.Before;
import org.junit.Test;


public class TestRegisterService extends AbstractEqueueTestEJB {
	protected Log log = new Log4JLogger(getClass().getName());


	@Before
	public void setUp() {
		createDou(1);
		createDou(2);
		
	}

	
	@Test
	public void testDouRequestPrerequisite() {
		AutoRegisterPrerequisiteInfo autoRegisterPrerequisiteInfo = registerService.getAutoRegisterPrerequisiteInfo();
		Assert.assertNotNull(autoRegisterPrerequisiteInfo);
		Assert.assertNotNull(autoRegisterPrerequisiteInfo.getDous());
		Assert.assertEquals(2, autoRegisterPrerequisiteInfo.getDous().size());
		
		Assert.assertNotNull(autoRegisterPrerequisiteInfo.getRegions());
		Assert.assertEquals(2, autoRegisterPrerequisiteInfo.getRegions().size());
		
		Assert.assertNotNull(autoRegisterPrerequisiteInfo.getAgeGroups());
		Assert.assertTrue(autoRegisterPrerequisiteInfo.getAgeGroups().size() > 1);
	}
}
