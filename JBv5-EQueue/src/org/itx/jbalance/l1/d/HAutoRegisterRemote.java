package org.itx.jbalance.l1.d;


import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l0.h.HAutoRegister.Method;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l1.common.CommonObject;

@Remote
public interface HAutoRegisterRemote extends CommonObject<HAutoRegister>{

	HAutoRegister autoRegister(HAutoRegister autoRegister);

	/**
	 * Расчет востребованности (желанности) ДОУ
	 * Расчет ведется для заданной возрастной группы
	 * @param dous
	 * @param ageGroup
	 * @param method влияет на рейтинг
	 * @return
	 */
	Map<Dou, Double> calculateDouRating(List<Dou> dous, DOUGroups ageGroup, Date calculationDate, Method method);
	/**
	 * Возвращает рег. номера, которые подходят для протокола 
	 * @param ageGroup
	 * @param autoRegister
	 * @param privelege возможность выбрать только льготников
	 * @return
	 */
	List<HDouRequest> getSuitableDouRequests(DOUGroups ageGroup, Date calculationDate, boolean privelege, List<Long>douIds, Long currentRegister);
}
