package org.itx.jbalance.l1.d;


import javax.ejb.Remote;

import org.itx.jbalance.l0.h.HFreeDivision;
import org.itx.jbalance.l0.s.SFreeDivision;
import org.itx.jbalance.l1.common.CommonDocument;

@Remote
public interface HFreeDivisionsRemote extends CommonDocument<HFreeDivision , SFreeDivision>{
	
}
