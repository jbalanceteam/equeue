package org.itx.jbalance.l1.d;


import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Remote;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.common.CommonDocument;

@Remote
public interface HDouRequestsRemote extends CommonDocument<HDouRequest,SDouRequest>{
	List<HDouRequest>search(DouRequestsSearchParams params);
	Integer getSearchCount(DouRequestsSearchParams params);
	Integer getAllCount();
//	void setId(Long uid);
	
	
	
	
	public void synchronizeSpecification(HDouRequest doc, Map<Dou, Integer> map);
	/**
	 * Дурацкая проверка!
	 * Дата регистрации не должна быть мньше(раньше), максимальной за год 
	 * @param regDate
	 * @return
	 */
	public Date validate(Date regDate);
	/**
	 * Запрос количества на дату в прошлом
	 * @param params
	 * @return
	 */
	Integer getSearchCountWithEffectiveDate(DouRequestsSearchParams params);
	
	/**
	 * Метод для отчета
	 * @param year
	 * @return
	 */
//	public Integer getPerehod(int year,Sex sex);
}
