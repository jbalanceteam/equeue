package org.itx.jbalance.l1.d;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.h.HFreeDivision;
import org.itx.jbalance.l0.s.SFreeDivision;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.RemoteBinding;

@Stateless(name ="HFreeDivisions")
@RemoteBinding(jndiBinding = Constants.JNDI_HFREEDIVISIONS_REMOTE)
public class HFreeDivisionsBean extends CommonDocumentBean<HFreeDivision , SFreeDivision> implements HFreeDivisionsRemote{

	private static final long serialVersionUID = -8430635966944491477L;

	
}
