package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Query;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HDouRequest.Status;
import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.EmailMessage;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.EmailMessage.ContentType;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.DouRequestsSearchParams.OrderStatus;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l1.api.filters.FilterParams;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.itx.jbalance.l1.o.BirthSertificates;
import org.itx.jbalance.l1.o.Emails;
import org.itx.jbalance.l1.o.Physicals;
import org.itx.jbalance.l1.utils.DateUtils;
import org.itx.jbalance.l1.utils.Str;
import org.itx.jbalance.l1.utils.mailsContent.HtmlTemplatesProcessor;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import static org.itx.jbalance.l1.api.filters.Filter.appendFilter;

@Stateless(name = "HDouRequests")
@RemoteBinding(jndiBinding = Constants.JNDI_HDOUREQUESTS_REMOTE)
@LocalBinding(jndiBinding = Constants.JNDI_HDOUREQUESTS_LOCAL)
public class HDouRequestsBean extends CommonDocumentBean<HDouRequest,SDouRequest> implements HDouRequestsRemote ,HDouRequestsLocal
{

	@EJB Physicals physicals;
	@EJB Emails emails;
	@EJB BirthSertificates birthSertificates;
	
	private static final long serialVersionUID = -8430635966944491477L;

//	TODO убить этот метод, когда(если) появится настройка системы
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public HDouRequest create(HDouRequest doc) {
//		Так делать нельзя ;)
//		Juridical juridical = (Juridical) getManager().createQuery("from Juridical")
//			.setMaxResults(1)
//			.getSingleResult();
//		getObject().setContractorOwner(juridical);
		if(doc.getBirthSertificate()==null){
			throw new RuntimeException("BirthSertificate is required");
		}
		
		if(doc.getChild()==null){
			throw new RuntimeException("Child is required");
		}
		
//		is BirthSertificate not persisted yet
//		TODO write isPersisted method in Ubiq
		if(doc.getBirthSertificate().getTS()==null){
			BirthSertificate birthSertificate = birthSertificates.create(doc.getBirthSertificate());
			doc.setBirthSertificate(birthSertificate);
		}
		
		
//		is Physical not persisted yet
//		TODO write isPersisted method in Ubiq
		if(doc.getChild().getTS()==null){
			Physical physical = physicals.create(doc.getChild());
			doc.setChild(physical);
		}
		
		
		if(doc.getFoundation()==null) 
			doc.setFoundation("основание");
		if (doc.getRegDate()==null)
			doc.setRegDate(new Date());
		
		doc.setContractorOwner(null);
		doc.setStatus(Status.WAIT);
		doc = super.create(doc);
		
		
//		Отправляем письмо
		if(!Str.empty(doc.getChild().getEMail())){
			
			HtmlTemplatesProcessor htmlTemplatesProcessor = new HtmlTemplatesProcessor(HtmlTemplatesProcessor.REPORT_CREATE_RN);
			htmlTemplatesProcessor.setVariable(HtmlTemplatesProcessor.PARAM_RN, doc.toString());
			EmailMessage email = new EmailMessage(doc.getChild(), "EQueue@jbalance.org", 
					doc.getChild().getEMail(), 
					null, 
					"apv@jbalance.org,d.rodionov@jbalance.org", 
					"Уведомление о постановке в очередь в ДОУ",
					htmlTemplatesProcessor.toString(), ContentType.HTML
					);
			emails.create(email);
		}
		return doc;
	}
	
	
	/**
	 * Дурацкая проверка!
	 * Дата регистрации не должна быть мньше(раньше), максимальной за все года 
	 * @param regDate
	 * @return
	 */
	public Date validate(Date regDate){
		getLog().info("validate("+regDate+")");
		
		Calendar cal=Calendar.getInstance();
		cal.setTime(regDate);
		
		regDate=new DateUtils().getStartOfDay(regDate);
		
		List<?> resultList = getManager().createQuery("select max(r.regDate) " +
				"from HDouRequest r " +
//				"where year(r.regDate) = :year and r.version is null and r.closeDate is null ")
				"where r.version is null and r.closeDate is null ")
//		.setParameter("year", cal.get(Calendar.YEAR))
		.getResultList();
		
		if(!resultList.isEmpty() && resultList.get(0) !=null){
			
			Date maxDate = (Date) resultList.get(0);
			
			maxDate=new DateUtils().getStartOfDay(maxDate);
			
			
			if(maxDate.after(regDate)){
				getLog().warn(maxDate +" after "+regDate);
				return maxDate;
			}
		}
		return null;
	}

	
	@Override
	protected void setupDNumber(HDouRequest doc) {
//		super.setupDNumber();
		
//		Date currentDate=new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(doc.getRegDate() == null?new Date():doc.getRegDate());
		int currentYear = c.get(Calendar.YEAR);
		
		@SuppressWarnings("unchecked")
		List<HDouRequest> resultList = getManager().createQuery("select r from HDouRequest r " +
				"where year(r.regDate) = "+currentYear+" and r.dnumber=(select max(r2.dnumber) from HDouRequest r2 where year(r2.regDate) = "+currentYear+" )")
		.getResultList();
		Long dnumber ;
		if(resultList.size() > 0){
//			dnumber=resultList.get(0).getDnumber()+1;
			dnumber=resultList.get(0).getDnumber()+1;
		}else{
			dnumber=1l;
		}
		
		doc.setDnumber(dnumber);
//		getObject().setDouRequestNumber(dnumber+"-"+currentYear);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<HDouRequest> search(DouRequestsSearchParams params) {
		if(params==null)
			params=new DouRequestsSearchParams();
		if(params.getFrom()==null)
			params.setFrom(0);
		if(params.getTo()==null)
			params.setTo(100000);
		
		
		StringBuilder sql =new StringBuilder( "select o " +
				"from HDouRequest o join o.child c join o.birthSertificate bs left join o.privilege pr where " +
				"o.version is null and o.closeDate is null ");
	
		FilterParams filterParams = new FilterParams();
		sql.append(produceWhere(params, filterParams));
				
				if(params.getSortInfo()!=null && !params.getSortInfo().isEmpty()){
					sql.append(" order by ");
					boolean ifFirst = true;
					for (SortedColumnInfo col : params.getSortInfo()) {
						if(!ifFirst)
							sql.append(" , ");
						
						if(col.getName().equals("rn")){
							
							
							if(col.isAsc())
								sql.append(" year(o.regDate) asc, o.dnumber asc");
							else
								sql.append(" year(o.regDate) desc, o.dnumber desc");
						}else if(col.getName().equals("privilege")){
							
//							TODO тут бешенная заморочь с NULL LAST/FIRST
//							if(col.isAsc())
//								sql.append(" o.privilege ASC NULLS LAST ");
//							else
//								sql.append(" o.privilege DESC NULLS FIRST ");

							if(col.isAsc())
								sql.append(" o.privilege ASC");
							else
								sql.append(" o.privilege DESC ");
	
							
						}
						ifFirst = false;
					}
				}else{
				 sql.append(" order by year(o.regDate) desc, o.dnumber desc");
				}
				
		String sql2s = sql.toString();
		getLog().debug(sql2s);
		Query query = getManager().createQuery(sql2s);
		query=fillParams(params, query, filterParams);
		
		List<HDouRequest> resultList = query.setMaxResults(params.getTo()-params.getFrom()+1).setFirstResult(params.getFrom()).getResultList();
//		Following code is moved to l2
//		for (HDouRequest r : resultList) {
//			setObject(r);
//			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZ");
//			getLog().debug("get detail for " + r + "    UId: "+ r.getUId() + "    regDate: "+simpleDateFormat.format(r.getRegDate()) + "   DNumber: "+r.getDnumber());
//			List<SDouRequest> spec = getSpecificationLines();
//			StringBuffer dous=new StringBuffer();
//			for (SDouRequest s : spec) {
//				if(s.getArticleUnit()!=null){
//					dous.append(((Dou)s.getArticleUnit()).getNumber());
//					dous.append(", ");
//				}
//			}
//			if(dous.length()>1)
//			dous.deleteCharAt(dous.length()-2);
//			r.setDous(dous.toString());
//		}
		
		return resultList;
	}

	

	
	


	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer getSearchCount(DouRequestsSearchParams params) {
		getLog().debug("Search Count METRICS [0]   params: "+params);
		StringBuilder sql =new StringBuilder( "select count(o) " +
				"from HDouRequest o join o.child c join o.birthSertificate bs left join o.privilege pr where " +
				"o.version is null ");
				
				
		FilterParams filterParams = new FilterParams();
		sql.append(produceWhere(params, filterParams));
		Query query = getManager().createQuery(sql.toString());
		query = fillParams(params,query, filterParams);
		getLog().debug("Search Count METRICS [1]");
		Date start = new Date();
		Number res = (Number)query.getSingleResult();
		getLog().debug("Search Count METRICS [2] query executed in " + (System.currentTimeMillis() - start.getTime()) +" ms");
		return res.intValue();
	}
	
	/**
	 * #348: Добавить в отчете о состоянии очереди поле "дата"
	 * 
	 * Продовал переделать getSearchCount, чтобы он полноценно учитывал EffectiveDate
	 * но не получилось. Сделал новый метод
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer getSearchCountWithEffectiveDate(DouRequestsSearchParams params) {
		getLog().debug("getSearchCountWithEffectiveDate METRICS [0]   params: "+params);
		
		Date effectiveDate =   params.getEffectiveDate();
		if(effectiveDate == null){
			effectiveDate = new Date();
		}
		
		StringBuilder sql =new StringBuilder( 
"select count(*) from HDouRequest r1 inner join Physical c on r1.child_UId = c.uid " + 

" left join  " + 
" ( " + 
		
	" select sr.* " +  
	" from SRegister sr inner join HRegister hr on sr.huid=hr.uid " + 
	
	" where " + 
	" sr.version is null " +  
	" and hr.version is null " +  
	" and hr.status='THIRD' " + 
	" and (hr.closeDate is null or hr.closeDate > :effectiveDate) " +  
	" and hr.registerDate <= :effectiveDate " + 
	
	" and not exists ( " + 
			" select sr2.uid from SRegister  sr2 inner join HRegister hr2 on sr2.huid=hr2.uid " +  
		" where (hr2.closeDate is null or hr2.closeDate > :effectiveDate) " +  
		" and (sr2.closeDate is null or sr2.closeDate > :effectiveDate)  " + 
		" and hr2.registerDate <= :effectiveDate " + 
		" and hr2.version is null " +  
		" and sr2.version is null  " + 
		" and sr2.DouRequest_uid = sr.DouRequest_uid " + 
		" and hr2.status='THIRD' " + 
		" and hr2.dnumber > hr.dnumber " + 
		" ) " + 

	" )register " +  
" on register.DouRequest_uid = r1.uid " +  



" where (r1.closeDate is null or r1.closeDate > :effectiveDate) " +  
" and r1.regDate <= :effectiveDate " + 
" and r1.version is null " +  
 "");
				
		
		
		if(params.orderStatus != null){
			switch(params.orderStatus){
			case ACTIVE_QUEUE:
				sql.append(" and (register.uid is null or register.rnAction = 'REFUSAL' or register.rnAction = 'GKP' or register.rnAction = 'BACK_TO_QUEUE') ");
				break;
			case OUT_OF_QUEUE:
				sql.append(" and (register.rnAction = 'DELETE_BY_PARRENTS_AGREE' or register.rnAction = 'GIVE_PERMIT' or register.rnAction = 'DELETE_BY_7_YEAR') ");
				break;
			case REJECT:
				sql.append(" and (register.rnAction = 'REFUSAL' ) ");
				break;
			}
		}
		
		if(params.ageFrom !=null){
			sql.append(" and c.birthday <= :birthdayTo");
		}
		
		if(params.ageTo !=null){
			sql.append(" and c.birthday >= :birthdayFrom");
		}
		
		FilterParams filterParams = new FilterParams();
		
		appendFilter(params.plainYear, filterParams, sql, "r1.year");
		
		
		getLog().debug("!!!!!!!!\n"+sql+"\n");
				
		Query query = getManager().createNativeQuery(sql.toString());
		
		
		for (Entry<String, Object> e : filterParams.getParamMap().entrySet()) {
			query=query.setParameter(e.getKey()	, e.getValue());
		}
		
		query.setParameter("effectiveDate", effectiveDate)
			.setParameter("effectiveDate", effectiveDate)
			.setParameter("effectiveDate", effectiveDate)
			.setParameter("effectiveDate", effectiveDate)
			.setParameter("effectiveDate", effectiveDate)
			.setParameter("effectiveDate", effectiveDate)
			.setParameter("effectiveDate", effectiveDate);
		
		
		if(params.ageFrom !=null){
			Calendar cal = Calendar.getInstance();
			cal.setTime(effectiveDate);
			cal.add(Calendar.MONTH, -1*params.ageFrom);
//			cal.set(Calendar.DAY_OF_MONTH, 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			query=query.setParameter("birthdayTo", cal.getTime());
			getLog().debug("ageFrom: "+params.ageFrom+"\tbirthdayTo:"+ new SimpleDateFormat("yyyy-MM-dd HH:mm").format(cal.getTime()));
			
		}
		
		if(params.ageTo !=null){
			Calendar cal = Calendar.getInstance();
			cal.setTime(effectiveDate);
			
			cal.add(Calendar.MONTH, -1*params.ageTo-1);
//			cal.add(Calendar.DAY_OF_MONTH, -2);
//			cal.set(Calendar.DAY_OF_MONTH, 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			query=query.setParameter("birthdayFrom", cal.getTime());
			getLog().debug("ageTo: "+params.ageTo+"\tbirthdayFrom:"+ new SimpleDateFormat("yyyy-MM-dd HH:mm").format(cal.getTime()));
		}
		
		
		
		getLog().debug("Search Count METRICS [1]");
		Date start = new Date();
		Number res = (Number)query.getSingleResult();
		getLog().debug("Search Count METRICS [2] query executed in " + (System.currentTimeMillis() - start.getTime()) +" ms");
		
		
		
		return res.intValue();

	}
	
	
	
	

	private Query fillParams(DouRequestsSearchParams params,
			Query query, FilterParams filterParams) {
		
		if(params.getEffectiveDate() != null){
			query=query.setParameter("effectiveDate", params.getEffectiveDate());
			query=query.setParameter("effectiveDate", params.getEffectiveDate());
		}
		
		for (Entry<String, Object> e : filterParams.getParamMap().entrySet()) {
			query=query.setParameter(e.getKey()	, e.getValue());
		}
		
		
//		if(params.uid != null){
//			query=query.setParameter("uid", params.uid.getValue());
//		}
		
		
		if(params.pattern!=null && ! params.pattern.isEmpty()){
			query=query.setParameter("surname", params.pattern)
				.setParameter("realName", params.pattern)
				.setParameter("patronymic", params.pattern)
				.setParameter("email", params.pattern)
				.setParameter("bsNumber", params.pattern)
				.setParameter("comments", params.pattern)
				.setParameter("oldNumber", params.pattern)
				.setParameter("phone", 		params.pattern)
				.setParameter("address", 		params.pattern)
				.setParameter("seria", params.pattern)
				.setParameter("reqNumber", params.pattern);
		}
		
//		if(params.rn!=null){
//			query=query.setParameter("exactRN", params.rn.getValue());
//		}
//		
//		if(params.lastname!=null){
//			query=query.setParameter("surname", params.lastname.getValue());
//		}
//		if(params.firstname!=null){
//			query=query.setParameter("realName", params.firstname.getValue());
//		}
//		if(params.middlename!=null){
//			query=query.setParameter("patronymic", params.middlename.getValue());
//		}
//		
		if(params.exact_privilege !=null){
			query=query.setParameter("exact_privilege", params.exact_privilege);
		}
		
		
		if(params.searchedDous!=null  && !params.searchedDous.isEmpty()){
			query=query.setParameter("searchedDous", params.searchedDous);
		}
//		
//		
//		if(params.bsNumber != null){
//			query=query.setParameter("bsNumber2", params.bsNumber.getValue());
//		}
//		if(params.bsSeria != null){
//			query=query.setParameter("bsSeria2", params.bsSeria.getValue());
//		}
		
		if(params.region!=null){
			query=query.setParameter("region", params.region);
		}
		
		
//		if(params.birthday != null){
//			if(params.birthday.getFrom() != null){
//				getLog().debug("params.birthdayFrom: "+params.birthday.getFrom());
//				query=query.setParameter("birthdayFrom", params.birthday.getFrom());
//			}
//			
//			if(params.birthday.getTo() != null){
//				getLog().debug("params.birthdayTo: " + params.birthday.getTo());
//				query=query.setParameter("birthdayTo", params.birthday.getTo());
//			}
//		}
//		
//		if(params.regDate!=null && params.regDate.getFrom()!=null){
//			query=query.setParameter("regDateFrom", params.regDate.getFrom());
//		}
//		
//		if(params.regDate!=null && params.regDate.getTo()!=null){
//			query=query.setParameter("regDateTo", params.regDate.getTo());
//		}
//		
//		if(params.sverkDate!=null && params.sverkDate.getFrom()!=null){
//			query=query.setParameter("sverkDateFrom", params.sverkDate.getFrom());
//		}
//		
//		if(params.sverkDate!=null && params.sverkDate.getTo()!=null){
//			query=query.setParameter("sverkDateTo", params.sverkDate.getTo());
//		}
		
		
		if(params.withoutColor!=null && params.withoutColor){
			
		}

		
		if(params.recordColor!=null){
			query=query.setParameter("recordColor", params.recordColor);
		}
		
		if(params.ageFrom !=null){
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			cal.add(Calendar.MONTH, -1*params.ageFrom);
//			cal.set(Calendar.DAY_OF_MONTH, 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			query=query.setParameter("birthdayTo", cal.getTime());
			getLog().debug("birthdayTo:"+ new SimpleDateFormat("yyyy-MM-dd HH:mm").format(cal.getTime()));
			
		}
		
		if(params.ageTo !=null){
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			
			cal.add(Calendar.MONTH, -1*params.ageTo-1);
//			cal.add(Calendar.DAY_OF_MONTH, -2);
//			cal.set(Calendar.DAY_OF_MONTH, 0);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			query=query.setParameter("birthdayFrom", cal.getTime());
			getLog().debug("birthdayFrom:"+ new SimpleDateFormat("yyyy-MM-dd HH:mm").format(cal.getTime()));
		}
		
		
//		if(params.plainYear!=null){
//			if(params.plainYear == -1 ){
//				
//			}else{
//				query=query.setParameter("year", params.plainYear);
//			}
//		}
		
		

//				Наличие путевки
		if(params.orderStatus!=null && params.orderStatus != OrderStatus.ALL){
			query=query.setParameter("statusWait", Status.WAIT);
			if(params.orderStatus == OrderStatus.REJECT)
				query=query.setParameter("rnAction", SRegister.Action.REFUSAL);
		}
		
		if(params.sex !=null){
			query=query.setParameter("sex", params.sex);
		}
		
		
		if(params.minRn!=null){
			String[] split = params.minRn.split("-");
			Long dnumber = new Long(split[0]);
			Integer year = new Integer(split[1]);
			Date startOfMinYear = new DateUtils().getStartOfYear(year);
			Date endOfMinYear = new DateUtils().getEndOfYear(year);
			query=query.setParameter("minDnumber", dnumber)
					   .setParameter("startOfMinYear",startOfMinYear)
					   .setParameter("endOfMinYear",endOfMinYear);
		}
		
		return query;
	}

	
	
	private StringBuilder produceWhere(	DouRequestsSearchParams  params, FilterParams filterParams) {
		StringBuilder res=new StringBuilder();
		
		if(params.getEffectiveDate() == null){
			res.append(" and o.closeDate is null ");
		} else {
			res.append(" and (o.closeDate is null or o.closeDate > :effectiveDate)  ");
			res.append(" and o.regDate <= :effectiveDate ");
		}
		
		appendFilter(params.uid, filterParams, res, "o.UId");
		
//		поиск по всем полям
		if(params.pattern!=null && !params.pattern.isEmpty()){
			res.append(" and " +
					"(lower(c.surname)  like lower(concat(:surname,'%')) or " +
					" lower(c.realName) like lower(concat(:realName,'%')) or " +
					" lower(c.patronymic) like lower(concat(:patronymic,'%')) or " +
					" lower(c.EMail) like lower(concat(:email,'%')) or " +
					" lower(bs.number) like lower(concat(:bsNumber,'%')) or "+
					" lower(o.comments) like lower(concat('%',concat(:comments,'%'))) or "+
					" lower(o.oldNumber) like lower(concat(:oldNumber,'%')) or "+
					" lower(c.phone) like lower(concat(:phone,'%')) or "+
					" lower(o.address) like lower(concat(:address,'%')) or "+
					" lower(concat(concat(o.dnumber,'-'),year(o.regDate))) like lower(concat(:reqNumber,'%')) or "+
					" lower(bs.seria) like lower(concat(:seria,'%')) ) ");
		}
		
		appendFilter(params.rn, 		filterParams, res, "concat(concat(o.dnumber,'-'),year(o.regDate))");
		appendFilter(params.lastname, 	filterParams, res, "c.surname");
		appendFilter(params.firstname, 	filterParams, res, "c.realName");
		appendFilter(params.middlename, filterParams, res, "c.patronymic");
		
		
//		res.append(generateWhereByStringFilter(params.rn, "concat(concat(o.dnumber,'-'),year(o.regDate))" , ":exactRN"));
//		res.append(generateWhereByStringFilter(params.lastname, "c.surname" , ":surname"));
//		res.append(generateWhereByStringFilter(params.firstname,"c.realName" , ":realName"));
//		res.append(generateWhereByStringFilter(params.middlename,"c.patronymic" , ":patronymic"));
		
						if(params.exact_privilege !=null){
							res.append(" and pr.UId = :exact_privilege");
						}
		
		//				Наличие льготы
						if(Boolean.TRUE.equals(params.privilege)){
							res.append(" and pr is not null ");
						}else if(Boolean.FALSE.equals(params.privilege)){
							res.append(" and pr is null ");
						}
						
		//				Наличие телеона
						if(Boolean.TRUE.equals(params.phone)){
							res.append(" and c.phone is not null and length(trim(c.phone))>0 ");
						}else if(Boolean.FALSE.equals(params.phone)){
							res.append(" and (c.phone is null or length(trim(c.phone))=0) ");
						}				
						
						
		//				Наличие путевки
						if(params.orderStatus!=null && params.orderStatus != OrderStatus.ALL){
							if(params.orderStatus .equals(OrderStatus.ACTIVE_QUEUE)) {
								res.append(" and o.status is :statusWait ");
							}else if(params.orderStatus .equals(OrderStatus.OUT_OF_QUEUE)){
								res.append(" and o.status is not :statusWait ");
							}else if(params.orderStatus .equals(OrderStatus.REJECT)) {
								res.append(" and o.status is :statusWait and exists (select sreg from " +SRegister.class.getSimpleName()+ " sreg "+
										" where sreg.version is null and sreg.closeDate is null and sreg.rnAction = :rnAction and  sreg.douRequest = o ) ");
							}
						}
						
						
						if(params.searchedDous!=null && !params.searchedDous.isEmpty()){
							res.append(" and exists (select sdr from SDouRequest sdr  where sdr.articleUnit.UId in (:searchedDous) and sdr.HUId=o and sdr.version is null and sdr.closeDate is null)");
						}
						
						if(params.region!=null){
							res.append(" and exists (select sdr from SDouRequest sdr inner join sdr.articleUnit au inner join au.region rg where rg.name = :region and sdr.HUId=o)");
						}
						
						appendFilter(params.birthday, filterParams, res, "c.birthday");
//							if(params.birthday.getFrom() != null){
//								res.append(" and c.birthday >= :birthdayFrom");
//							}
//							
//							if(params.birthday.getTo() != null){
//								res.append(" and c.birthday <= :birthdayTo");
//							}
//						}
						
						appendFilter(params.regDate, filterParams, res, "o.regDate");
						
//						if(params.regDate!=null && params.regDate.getFrom()!=null){
//							res.append(" and o.regDate >= :regDateFrom");
//						}
//						
//						if(params.regDate!=null && params.regDate.getTo()!=null){
//							res.append(" and o.regDate <= :regDateTo");
//						}
						
						appendFilter(params.sverkDate, filterParams, res, "o.sverkDate");
//						if(params.sverkDate!=null && params.sverkDate.getFrom()!=null){
//							res.append(" and o.sverkDate >= :sverkDateFrom");
//						}
//						
//						if(params.sverkDate!=null && params.sverkDate.getTo()!=null){
//							res.append(" and o.sverkDate <= :sverkDateTo");
//						}
						

						if(params.withoutColor!=null && params.withoutColor){
							res.append(" and o.recordColor is null ");
						}else if(params.recordColor!=null){
							res.append(" and o.recordColor = :recordColor");
						}
						
						if(params.ageFrom !=null){
							res.append(" and c.birthday <= :birthdayTo");
						}
						
						if(params.ageTo !=null){
							res.append(" and c.birthday >= :birthdayFrom");
						}
						
						
						appendFilter(params.plainYear, filterParams, res, "o.year");
//						if(params.plainYear !=null){
//							if(params.plainYear == -1 ){
//								res.append(" and o.year is null ");
//							}else{
//								res.append(" and o.year = :year");
//							}
//						}
						
						if(params.sex !=null){
							res.append(" and bs.sex = :sex");
						}
						
						appendFilter(params.bsNumber, filterParams, res, "bs.number");
						appendFilter(params.bsSeria, filterParams, res, "bs.seria");
//						if(params.bsNumber != null){
//							res.append(" and bs.number = :bsNumber2");
//						}
//						if(params.bsSeria != null){
//							res.append(" and bs.seria = :bsSeria2");
//						}
						
						if(params.minRn!=null){
//							res.append(" and ((year(o.regDate) < :minYear) or (year(o.regDate) = :minYear and o.dnumber <= :minDnumber)) ");
							res.append(" and (");
							res.append(	"(o.regDate < :startOfMinYear) or (o.regDate >= :startOfMinYear and o.regDate <= :endOfMinYear and o.dnumber <= :minDnumber) ");
							if(params.privilegesToBegin!=null && params.privilegesToBegin)
								res.append(		"or  pr is not null ");
							
							res.append(") ");
						}
						
						return res;
	}

	
	
//	following code is moved to LongFilter
//	String generateWhereByLongFilter(LongFilter longFilter, String fieldName, String paramName) {
//		String res = "";
//		if(longFilter !=null){
//			switch(longFilter.getType()){
//				case EQ: 
//					res = " and ("+fieldName+" = "+paramName+") ";  
//					break;
//				default:
//					throw new RuntimeException("Not implemented yet");
//			}
//			
//		}
//		return res;
//	}


//	following code is moved to StringFilter
//	String generateWhereByStringFilter(StringFilter stringFilter, String fieldName, String paramName){
//		String res = "";
//		if(stringFilter !=null){
//			switch(stringFilter.getType()){
//				case EQ: 
//					res = " and lower("+fieldName+") = lower("+paramName+")";  
//					break;
//				case ANYWHERE: 
//					res = " and lower("+fieldName+") like lower(concat(concat('%',"+paramName+" ) ,'%'))";   
//					break;
//				case END_WITH: 
//					res = " and lower("+fieldName+") like lower(concat('%' , "+paramName+" ))";  
//					break;
//				case START_WITH: 
//					res = " and lower("+fieldName+") like lower(concat("+paramName+" , '%' ))";  
//					break;
//			}
//			
//		}
//		return res;
//	}
	
	
//
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer getAllCount() {
		Number res = (Number) getManager()
		.createQuery("select count(*) from HDouRequest o where o.version is null and o.closeDate is null  ")
		.getSingleResult();
		return res.intValue();
	}


	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public HDouRequest update(HDouRequest object) {
		
		object = super.update(object);
		
////		Отправляем письмо
//		if(object.getChild().getEMail() != null && !doc.getChild().getEMail().isEmpty()){
//			
//			HtmlTemplatesProcessor htmlTemplatesProcessor = new HtmlTemplatesProcessor(HtmlTemplatesProcessor.REPORT_CREATE_RN);
//			htmlTemplatesProcessor.setVariable(HtmlTemplatesProcessor.PARAM_RN, object.toString());
//			EmailMessage email = new EmailMessage(object.getChild(), "EQueue@jbalance.org", 
//					object.getChild().getEMail(), 
//					null, 
//					"apv@jbalance.org,d.rodionov@jbalance.org", 
//					"Уведомление о внесении изменения в регистрационный номер в ДОУ",
//					htmlTemplatesProcessor.toString(), ContentType.HTML
//					);
//			emails.create(email);
//		}
		return object;
	}


	@Override
	public void synchronizeSpecification(HDouRequest doc, Map<Dou, Integer> dous) {
		try {
			List<SDouRequest> specificationLines = getSpecificationLines(doc);
			for (SDouRequest sDouRequest : specificationLines) {
	//			Если в списке садиков нет проверяемого - убить SpecificationLine
				if(! dous.containsKey(sDouRequest.getArticleUnit())){
					deleteSpecificationLine(sDouRequest);
				}else {
					Integer rating = dous.get(sDouRequest.getArticleUnit());
					if(rating== null || rating < 1){
						deleteSpecificationLine(sDouRequest);
						continue;
					}
					
					if(!rating.equals(sDouRequest.getRating())){
						sDouRequest.setRating(rating);
						updateSpecificationLine(sDouRequest);
					}
					
					dous.remove(sDouRequest.getArticleUnit());
				}
			}
	//		Теперь в dous остались только садики, для которых нет SpecificationLine
			for (Entry<Dou, Integer> d : dous.entrySet()) {
				SDouRequest s = new SDouRequest();
				s.setArticleUnit(d.getKey());
				s.setRating(d.getValue());
				s.setHUId(doc);
				createSpecificationLine(s,doc);
			}
			
		} finally {
			try {
				sendMessage(doc.getUId());
			} catch (NamingException e) {
				getLog().error(e);
			} catch (JMSException e) {
				getLog().error(e);
			}
		}
	}
	
	
	@Override
	public SDouRequest createSpecificationLine(SDouRequest spec, HDouRequest doc) {
		try {
			sendMessage(doc.getUId());
		} catch (NamingException e) {
			getLog().error(e);
		} catch (JMSException e) {
			getLog().error(e);
		}
		return super.createSpecificationLine(spec, doc);
	}


	@Override
	public SDouRequest deleteSpecificationLine(SDouRequest specificationLine) {
		try {
			sendMessage(specificationLine.getHUId().getUId());
		} catch (NamingException e) {
			getLog().error(e);
		} catch (JMSException e) {
			getLog().error(e);
		}
		return super.deleteSpecificationLine(specificationLine);
	}


	@Override
	public SDouRequest updateSpecificationLine(SDouRequest specificationLine) {
		try {
			sendMessage(specificationLine.getHUId().getUId());
		} catch (NamingException e) {
			getLog().error(e);
		} catch (JMSException e) {
			getLog().error(e);
		}
		return super.updateSpecificationLine(specificationLine);
	}
	
	private void sendMessage(Long uid) throws NamingException, JMSException{
		Context context = new InitialContext();
		QueueConnectionFactory queueFactory = (QueueConnectionFactory)context.lookup(Constants.JNDI_JMS_CONNECTIONFACTORY);
		QueueConnection queueConnection = null;
		QueueSender queueSender = null;
		try {
			queueConnection = queueFactory.createQueueConnection();
			QueueSession queueSession = queueConnection.createQueueSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
			Queue queue = (Queue)context.lookup(Constants.JNDI_JMS_DOUCATCHEQUEUE);
			queueSender = queueSession.createSender(queue);
			TextMessage message = queueSession.createTextMessage();
			message.setLongProperty("uid",uid);	
			queueSender.send(queue, message);
			
		} catch (JMSException e) {
			getLog().error(e);
		}finally{
			if(queueSender!=null)
				queueSender.close();
			if(queueConnection!=null)
				queueConnection.close();
		}
	}


	@Override
	public HDouRequest delete(HDouRequest object) {
		// #163 Не позволять удалять РН, которые включены в протокол
		List<SRegister> specByDouRequest = getHRegisters().getSpecByDouRequest(object.getUId());
		if(!specByDouRequest.isEmpty()){
			StringBuffer message = new StringBuffer();
			message.append("РН ");
			message.append(object);
			message.append(" включен в протокол");
			if(specByDouRequest.size()>1){
				message.append("ы");
			}
			message.append(" ");
			
			for (SRegister sRegister : specByDouRequest) {
				message.append(sRegister.getHUId().getDnumber());
				message.append(", ");
			}
			message.delete(message.length()-2, message.length());
			message.append(". Удаление невозможно!");
			throw new RuntimeException(message.toString());
		}
		
		return super.delete(object);
	}
	
	private HRegisterLocal getHRegisters(){
		try {
			Context context = new InitialContext();
			return (HRegisterLocal) context.lookup(Constants.JNDI_HREGISTER_LOCAL);
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
		 
	
}
