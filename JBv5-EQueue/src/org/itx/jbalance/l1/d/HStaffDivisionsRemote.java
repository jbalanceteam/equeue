package org.itx.jbalance.l1.d;


import javax.ejb.Remote;

import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.s.SStaffDivision;
import org.itx.jbalance.l1.common.CommonDocument;

@Remote
public interface HStaffDivisionsRemote extends CommonDocument<HStaffDivision , SStaffDivision>{
	
}
