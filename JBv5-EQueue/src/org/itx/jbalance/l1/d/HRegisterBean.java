package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Query;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HDouRequest.Status;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.EmailMessage;
import org.itx.jbalance.l0.o.Permit;
import org.itx.jbalance.l0.o.EmailMessage.ContentType;
import org.itx.jbalance.l0.o.Permit.PermitStatus;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l0.s.SRegister.Foundation;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.api.PermitSearchParams;
import org.itx.jbalance.l1.api.RegisterSearchParams;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l1.api.filters.FilterParams;
import org.itx.jbalance.l1.api.filters.LongFilter;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.itx.jbalance.l1.o.Emails;
import org.itx.jbalance.l1.o.Permits;
import org.itx.jbalance.l1.utils.DateUtils;
import org.itx.jbalance.l1.utils.Str;
import org.itx.jbalance.l1.utils.mailsContent.HtmlTemplatesProcessor;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.itx.jbalance.l2_api.utils.CacheHandlerMDB;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
import org.jboss.logging.Logger;

import static org.itx.jbalance.l1.api.filters.Filter.appendFilter;

@Stateless(name="HRegister")
@RemoteBinding(jndiBinding = Constants.JNDI_HREGISTER_REMOTE)
@LocalBinding(jndiBinding = Constants.JNDI_HREGISTER_LOCAL)
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class HRegisterBean extends CommonDocumentBean<HRegister,SRegister> implements HRegisterRemote, HRegisterLocal{

	private static final long serialVersionUID = -8430635966944491477L;

	
	@EJB(name=Constants.JNDI_HDOUREQUESTS_LOCAL)
	HDouRequestsLocal douRequestsBean;
	
	@EJB Emails emails;
	
	@EJB Permits permits;
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public HRegister create(HRegister hRegister) {
//		Так делать нельзя ;)
//		Juridical juridical = (Juridical) getManager().createQuery("from Juridical")
//			.setMaxResults(1)
//			.getSingleResult();
//		hRegister.setContractorOwner(juridical);
		
//		iss331 
//		if(hRegister.getReceivePermitDueDate() != null && hRegister.getRegisterDate().after(hRegister.getReceivePermitDueDate())){
//			throw new RuntimeException("Дата протокола должна быть меньше даты получения путевки");
//		}
		
		if(Str.empty(hRegister.getFoundation())){
			hRegister.setFoundation("wtf");
		}
		hRegister.setStatus(RegisterStatus.FIRST);
		hRegister.setContractorOwner(null);
		return super.create(hRegister);
	}

	@Override
	protected Long getMaxNumber(HRegister hRegister) {
		List<?> resultList = getManager().createQuery("select max (r.dnumber) from HRegister r").getResultList();
		getLog().debug("getMaxNumber("+hRegister+") resultList: "+resultList);
		if(resultList.isEmpty() || resultList.get(0)==null) {
			return null;
		} else {
			return ((Number)resultList.get(0)).longValue();
		}
	}
	

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer getAllCount() {
		Number res = (Number) getManager()
		.createQuery("select count(*) from HRegister o where o.version is null and o.closeDate is null  ")
		.getSingleResult();
		return res.intValue();
	}



	@Override
	public SRegister createSpecificationLine(SRegister sRegister, HRegister hRegister)  {
		/* Проверки */
		if(sRegister==null){
			throw new RuntimeException("sRegister mustn't be null");
		}
		
		if(sRegister.getDouRequest()==null){
			throw new RuntimeException("sRegister.douRequest mustn't be null");
		}
		
		if(hRegister==null){
			throw new RuntimeException("hRegister mustn't be null");
		}
		
		if( sRegister.getRnAction() == null){
			throw new RuntimeException("RnAction mustn't be null");
		}
		
		switch( sRegister.getRnAction()){
		case GIVE_PERMIT:
				if(sRegister.getDou()==null){
					throw new RuntimeException("При выдаче путевки необходимо заполнить поле ДОУ");
				}
				break;
			case BACK_TO_QUEUE:
			case DELETE_BY_7_YEAR:
			case DELETE_BY_PARRENTS_AGREE:
			case REFUSAL:
				sRegister.setDou(null);
		}
//		HDouRequest douRequest = sRegister.getDouRequest();
//		douRequest = updateRequestStatus(sRegister, douRequest);
//		sRegister.setDouRequest(douRequest);
		
		try {
			sendMessage(sRegister.getDouRequest().getUId());
		} catch (NamingException e) {
			getLog().error("",e);
		} catch (JMSException e) {
			getLog().error("",e);
		}
		
		return super.createSpecificationLine(sRegister, hRegister);
	}
	


	
	
	@Override
	public HRegister update(HRegister hRegister) {
		if(hRegister == null){
			throw new RuntimeException("hRegister can not be null");
		}
		
		if(hRegister.getUId() == null){
			throw new RuntimeException("You can not update non-persisted object");
		}
		
//		iss331 
//		if(hRegister.getReceivePermitDueDate() != null && hRegister.getRegisterDate().after(hRegister.getReceivePermitDueDate())){
//			throw new RuntimeException("Дата протокола должна быть меньше даты получения путевки");
//		}
		
		HRegister currentVersion = manager.find(HRegister.class, hRegister.getUId());
		
		if(currentVersion.getStatus() != hRegister.getStatus()){
			throw new RuntimeException("You can not update status directly. Use special changeStatus method");
		}
		
		return super.update(hRegister);
	}
	
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	@Override
	public HRegister changeStatus(Long regUid, RegisterStatus newStatus) {
		if(regUid == null){
			throw new RuntimeException("regUid can not be null");
		}
		
		if(newStatus == null){
			throw new RuntimeException("newStatus can not be null");
		}

		HRegister hRegister = manager.find(HRegister.class, regUid);
		RegisterStatus oldStatus = hRegister.getStatus();
		
		switch(oldStatus){
			case FIRST:
				switch(newStatus){
					case FIRST:
//						nothing to update
						getLog().debug("nothing to update");
						return hRegister;
					case SECOND:
						List<SRegister> specificationLines = getSpecificationLines(hRegister);
						for (SRegister sRegister : specificationLines) {
							HDouRequest douRequest = sRegister.getDouRequest();
							List<SRegister> specByDouRequest = getSpecByDouRequest(douRequest.getUId());
							for (SRegister sReg : specByDouRequest) {
//								этот же протокол
								if(sReg.getHUId().getUId().equals(hRegister.getUId())){
									continue;
								}
								
								if(sReg.getHUId().getStatus().getNumber() == 2){
									throw new RuntimeException("Очередник не может быть в двух предв. протоколах одновременно. \n РН "+douRequest+" присутствует в протоколе "+sReg.getHUId().getDnumber());
								}
							}
						}
						
						
						
						break;
					case THIRD:
						throw new RuntimeException("Второй статус перепрыгивать нельзя.");
				}
				break;
			case SECOND:
				switch(newStatus){
					case FIRST:
						break;
					case SECOND:
	//					nothing to update
						getLog().debug("nothing to update");
						return hRegister;
					case THIRD:
						
						/*     */
						Date registerDate = hRegister.getRegisterDate();
						if(registerDate == null){
							throw new RuntimeException("Заполните дату протокола");
						}

//						Query query = getManager().createQuery("select r from "+HRegister.class.getSimpleName()+" r "
//								+" where r.version is null and r.closeDate is null and r.status = :third and r.registerDate > :registerDate ")
//						.setParameter("third", RegisterStatus.THIRD).setParameter("registerDate", registerDate);
//						List<HRegister> resultList = query.getResultList();
//						if(!resultList.isEmpty()){
//							Long maxDate = 0l;
//							for (HRegister hr : resultList) {
//								maxDate = Math.max(maxDate, hr.getRegisterDate().getTime());
//							} 
//							throw new RuntimeException("Дата протокола не может быть меньше "+new SimpleDateFormat("dd.MM.yyyy").format(new Date(maxDate)));
//						}
						
						Date receivePermitDueDate = hRegister.getReceivePermitDueDate();
						if(receivePermitDueDate == null){
							throw new RuntimeException("Заполните дату уведомления");
						}
//						при переводе на 3 статус проверять что бы дата уведомления была больше текущей даты перевода на 3 статус на 10 календарных дней. 
//						Если меньше выводить сообщение о невозможности перевести и предлагать исправить поле.
						DateUtils dateUtils = new DateUtils();
						Calendar registerDateCal = Calendar.getInstance();
						registerDateCal.setTime(registerDate);
						registerDateCal.add(Calendar.DATE, +10);
						Date registerDatePlus10 = dateUtils.getEndOfDay(registerDateCal.getTime());
						if(registerDatePlus10.getTime() > receivePermitDueDate.getTime()){
							registerDateCal.add(Calendar.DATE, +1);
							registerDatePlus10 = registerDateCal.getTime();
							throw new RuntimeException("Дата уведомления должна быть больше чем дата протокола + 10 дней("+new SimpleDateFormat("dd.MM.yyyy").format(registerDatePlus10)+").");
						}
					    /*     */
						
						List<SRegister> specificationLines = getSpecificationLines(hRegister);
						
//						Проверки
						for (SRegister sRegister : specificationLines) {
							switch( sRegister.getRnAction()){
								case GIVE_PERMIT:
									if(sRegister.getDou()==null){
										throw new RuntimeException("При выдаче путевки необходимо заполнить поле ДОУ ["+sRegister.getDouRequest()+"]");
									}
									
//									Следующий свитч проверяет корректность смены статусов
									switch(sRegister.getDouRequest().getStatus()){
										case WAIT: 
											break;
										case PERMIT:
											throw new RuntimeException("РН "+sRegister.getDouRequest() +": невозможно выдать повторно");
											
									}
									break;
								case BACK_TO_QUEUE:
								case DELETE_BY_7_YEAR:
								case DELETE_BY_PARRENTS_AGREE:
								case REFUSAL:
									sRegister.setDou(null);
							}
						}
						
						
						for (SRegister sRegister : specificationLines) {
							HDouRequest douRequest = sRegister.getDouRequest();
							douRequest = updateRequestStatus(sRegister, douRequest);
							sRegister.setDouRequest(douRequest);
							
							if(sRegister.getRnAction().equals(Action.GIVE_PERMIT) || 
									sRegister.getRnAction().equals(Action.GKP)){
								createPermit(douRequest, sRegister);
							}
						}
						break;
						
				}
				break;
			case THIRD:
				throw new RuntimeException("Запрещено менять статус \"окончательного\" протокола.");
		}
		
		
		hRegister.setStatus(newStatus);
		return super.update(hRegister);
	}
	

	private HDouRequest updateRequestStatus(SRegister sRegister, HDouRequest douRequest) {
		switch( sRegister.getRnAction()){
			case GKP: 
				douRequest.setStatus(Status.WAIT);
				douRequest.setGkp(true);  
				break;
				
			case GIVE_PERMIT: 
				douRequest.setStatus(Status.PERMIT); 
				douRequest.setGkp(false); 
				break;
				
			case BACK_TO_QUEUE: 
				douRequest.setStatus(Status.WAIT); 
				douRequest.setGkp(false); 
				break;
				
			case DELETE_BY_7_YEAR: 
				douRequest.setStatus(Status.DELETE_BY_7_YEAR); 
				douRequest.setGkp(false);
				break;
				
			case DELETE_BY_PARRENTS_AGREE: 
				douRequest.setStatus(Status.DELETE_BY_PARRENTS_AGREE); 
				douRequest.setGkp(false);
				break;
		}
		

		
		douRequest = douRequestsBean.update(douRequest);
		try {
			sendMessage(douRequest.getUId());
		} catch (NamingException e) {
			getLog().error("",e);
		} catch (JMSException e) {
			getLog().error("",e);
		}
		
//		Отправляем письмо
		if(douRequest.getChild().getEMail() != null && !douRequest.getChild().getEMail().isEmpty()){
			HtmlTemplatesProcessor htmlTemplatesProcessor = new HtmlTemplatesProcessor(HtmlTemplatesProcessor.REPORT_PERMIT);
			htmlTemplatesProcessor.setVariable(HtmlTemplatesProcessor.PARAM_RN, douRequest.toString());
			htmlTemplatesProcessor.setVariable(HtmlTemplatesProcessor.PARAM_STATUS, douRequest.getStatus().getDescription());
			
			EmailMessage email = new EmailMessage(	douRequest.getChild(), 
					"EQueue@jbalance.org", 
					douRequest.getChild().getEMail(), 
					null, 
					"apv@jbalance.org,d.rodionov@jbalance.org", 
					"Уведомление об изменении статуса РН",
					htmlTemplatesProcessor.toString(), ContentType.HTML);	
			

			emails.create(email);
		}
		
		return douRequest;
	}
	
	
	/**
	 * Путевка должна автоматически создаваться при перевод протокола на 3-й этап
	 * 
	 * При этом все путевки выданные ребенку должны помечаться, как нереализованные
	 * @param douRequest
	 * @param sRegister
	 */
	private void createPermit(HDouRequest hDouRequest, SRegister sRegister) {
		Physical child = hDouRequest.getChild();
		Privilege privilege = hDouRequest.getPrivilege() ;
		PermitStatus permitStatus = PermitStatus.ISSUED;
		Dou dou = sRegister.getDou();
		
		GetPermitsRequest getPermitsRequest = new GetPermitsRequest();
		getPermitsRequest.setChildUid(new LongFilter(hDouRequest.getChild().getUId()));
		List<Permit> search = permits.search(getPermitsRequest);
		for (Permit permit : search) {
			if(permit.getPermitStatus() == null ||  permit.getPermitStatus() == PermitStatus.ISSUED){
				permit.setPermitStatus(PermitStatus.NOT_IMPLEMENTED);
				permit = permits.update(permit);
				getLog().debug("Путевка помечена, как NOT_IMPLEMENTED: " + permit);
			}
		}
		
		Permit permit = permits.create(new Permit(child, privilege, hDouRequest, sRegister.getHUId().getDnumber() + "-" +  sRegister.getNumber(), new Date(), permitStatus, sRegister, dou));
		getLog().debug("Создана путевка " + permit);
	}

//	@Override
//	public void setSpecificationLine(SRegister specificationLine) {
//		specificationLine.setDouRequest(
//			getManager().find(HDouRequest.class, specificationLine.getDouRequest().getUId()));
//		super.setSpecificationLine(specificationLine);
//	}
	
	
	@Override
	public SRegister updateSpecificationLine(SRegister sRegister) {
		HDouRequest douRequest = sRegister.getDouRequest();
//		douRequest = updateRequestStatus(sRegister, douRequest);
		sRegister.setDouRequest(douRequest);
		
//		if(sRegister.getHUId().getStatus().getNumber()>1){
//			if()
//		}
//		
		
		try {
			sendMessage(douRequest.getUId());
		} catch (NamingException e) {
			getLog().error("",e);
		} catch (JMSException e) {
			getLog().error("",e);
		}
		return super.updateSpecificationLine(sRegister);
	}
	
	
	private void sendMessage(Long uid) throws NamingException, JMSException{
		Context context = new InitialContext();
		QueueConnectionFactory queueFactory = (QueueConnectionFactory)context.lookup(Constants.JNDI_JMS_CONNECTIONFACTORY);
		QueueConnection queueConnection = null;
		QueueSender queueSender = null;
		try {
			queueConnection = queueFactory.createQueueConnection();
			QueueSession queueSession = queueConnection.createQueueSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
			Queue queue = (Queue)context.lookup(Constants.JNDI_JMS_DOUCATCHEQUEUE);
			queueSender = queueSession.createSender(queue);
			TextMessage message = queueSession.createTextMessage();
			message.setLongProperty("uid",uid);	
			queueSender.send(queue, message);
			
		} catch (JMSException e) {
			getLog().error(e);
		}finally{
			if(queueSender!=null)
				queueSender.close();
			if(queueConnection!=null)
				queueConnection.close();
		}
	}
	

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer getPermitCnt(PermitSearchParams params) {
		StringBuilder sql =new StringBuilder( "select count(o) " +
				"from  SRegister o join o.douRequest , HRegister h	") ;
				
		
		if(params.sex!=null){
			sql.append("join o.douRequest.birthSertificate ");
		}
			
		
		sql.append("where o.HUId=h and " +
				"o.version is null and o.closeDate is null ");
				
				
		if(params.dateFrom!=null){
			sql.append(" and h.registerDate >= :dateFrom");
		}
		
		if(params.dateTo!=null){
			sql.append(" and h.registerDate <= :dateTo");
		}
		
		if(params.action!=null){
			sql.append(" and o.rnAction = :action");
		}
		
		if(params.sex!=null){
			sql.append(" and o.douRequest.birthSertificate.sex = :sex");
		}
				
				
		Query query = getManager().createQuery(sql.toString());
		
		if(params.dateFrom!=null){
			query.setParameter("dateFrom", params.dateFrom);
		}
		
		if(params.dateTo!=null){
			query.setParameter("dateTo", params.dateTo);
		}
		
		if(params.action!=null){
			query.setParameter("action", params.action);
		}
		
		if(params.sex!=null){
			query.setParameter("sex", params.sex);
		}
		
		Number res = (Number)query.getSingleResult();
		return res.intValue();
	}
	
//	public List<H>
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<HRegister> search(RegisterSearchParams params) {

		
		Integer from = params.getFrom();
		Integer to = params.getTo();
		 List<SortedColumnInfo> sortInfo = params.getSortInfo();
		if(from==null)
			from=0;
		if(to==null)
			to=100000;
		
		System.out.println("!!!!!!!!!!!!: " + params);
		
		StringBuilder sql =new StringBuilder( "select o " +
				"from HRegister o where " +
				"o.version is null and o.closeDate is null ");
				
				
		FilterParams filterParams = new FilterParams();
		sql.append(produceWhere(params, filterParams));
				
				if(sortInfo!=null && !sortInfo.isEmpty()){
					sql.append(" order by ");
					for (SortedColumnInfo col : sortInfo) {
						if(col.getName().equals("openDate")){
							if(col.isAsc())
								sql.append(" o.dnumber asc, ");
							else
								sql.append(" o.dnumber desc");
						}
					}
				}else{
				 sql.append(" order by o.dnumber desc");
				}
				
		Query query = getManager().createQuery(sql.toString());
		query=fillParams(params, query, filterParams);
		
		List<HRegister> resultList = query.setMaxResults(to-from+1).setFirstResult(from).getResultList();

		Logger.getLogger(getClass()).debug("search returned "+resultList.size()+ " records");
		
		return resultList;
	}

	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer getSearchCount(RegisterSearchParams params) {
		
		StringBuilder sql =new StringBuilder( "select count(o) " +
				"from HRegister o where " +
				"o.version is null and o.closeDate is null ");
				
		FilterParams filterParams = new FilterParams();
		sql.append(produceWhere(params, filterParams));
		Query query = getManager().createQuery(sql.toString());
		query = fillParams(params,query, filterParams);
				
		Number res = (Number)query.getSingleResult();
		return res.intValue();

	}

	private Query fillParams(RegisterSearchParams params,
			Query query, FilterParams filterParams) {

		
		
//		if(params.registerDate!=null){
//			switch(params.registerDate.getType()){
//				case BETWEEN: query.setParameter("registerDateFrom", params.registerDate.getFrom()) ;
//							  query.setParameter("registerDateTo", params.registerDate.getTo()) ;
//							  break;
//					
//				case EQ:	
//				case GREAT:   
//				case LESS:	  
//					query.setParameter("registerDate", params.registerDate.getFrom() == null? params.registerDate.getTo():params.registerDate.getFrom()) ;	 
//					break;
//			}
//		}
		
		
		for (Entry<String, Object> e : filterParams.getParamMap().entrySet()) {
			query=query.setParameter(e.getKey()	, e.getValue());
		}
		
		if(params.douRequestUid !=null){
				query.setParameter("douRequestUid", params.douRequestUid.getFilterValue()) ;
		}
		
		return query;
	}
	
	
	@Override
	public void synchronizeSpecification(HRegister hRegister, List<SRegister> newSpec) {
		List<SRegister> specificationLines = getSpecificationLines(hRegister);
		for (SRegister sRegister : specificationLines) {
			sRegister.setDouRequest(douRequestsBean.getByUId(sRegister.getDouRequest().getUId(),HDouRequest.class));
			if(! newSpec.contains(sRegister)){
				deleteSpecificationLine(sRegister);
			}
		}
//		Теперь в dous остались только те, для которых нет SpecificationLine
		for (SRegister sRegister : newSpec) {
			if(sRegister.getUId() != null){
				updateSpecificationLine(sRegister);
			} else {
				createSpecificationLine(sRegister, hRegister);
			}

		}
	}

	
	
	private StringBuilder produceWhere(	RegisterSearchParams  params, FilterParams filterParams) {
		StringBuilder query = new StringBuilder();
		
		appendFilter(params.registerDate, filterParams, query, "o.registerDate");
//		if(params.registerDate!=null){
//			switch(params.registerDate.getType()){
//				case BETWEEN: res.append(" and o.registerDate between :registerDateFrom and :registerDateTo "); break;
//				case EQ:	  res.append(" and o.registerDate = :registerDate "); break;
//				case GREAT:   res.append(" and o.registerDate > :registerDate"); break;
//				case LESS:	  res.append(" and o.registerDate < :registerDate"); break;
//			}
//		}
//		
		if(params.douRequestUid !=null){
			switch(params.douRequestUid.getFilterType()){
				case EQUALS:	  query.append(" and exists(select spec from "+SRegister.class.getSimpleName()+" spec " +
						"where spec.douRequest.UId = :douRequestUid and spec.HUId = o) "); break;
				default: throw new RuntimeException("Not implemented yet.");
			}
		}
		
		
		
		return query;
	}
	
	@SuppressWarnings("unchecked")
	public List<SRegister>getSpecByDouRequest(Long uid){
		return getManager().createQuery("select o from SRegister o " +
				"where o.version is null and o.closeDate is null and o.HUId.version is null and o.HUId.closeDate is null " +
				"and o.douRequest.UId=:uid " +
				"order by o.openDate")
				.setParameter("uid", uid)
				.getResultList();
	}
	
	public List<Foundation>getFoundations(){
		return Arrays.asList( Foundation.values());
	}

	@Override
	public Integer getSpecCount(Long huid) {
		Query createQuery = getManager().createQuery("select count(s) from "+SRegister.class.getSimpleName()+" s where s.HUId.UId=:huid and s.version is null and s.closeDate is null");
		createQuery.setParameter("huid", huid);
		Number number = (Number) createQuery.getSingleResult();
		return number.intValue();
	}
	
}
