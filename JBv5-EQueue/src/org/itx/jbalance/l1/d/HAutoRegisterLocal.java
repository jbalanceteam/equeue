package org.itx.jbalance.l1.d;


import javax.ejb.Local;

import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface HAutoRegisterLocal extends CommonObject<HAutoRegister>{

	HAutoRegister autoRegister(HAutoRegister autoRegister);
	
}
