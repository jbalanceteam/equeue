package org.itx.jbalance.l1.d;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.h.HAutoRegister.Method;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HDouRequest.Status;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.s.SAutoRegister;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Action;
import org.itx.jbalance.l0.s.SRegister.Foundation;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.DateFilterType;
import org.itx.jbalance.l1.api.filters.FilterParams;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.itx.jbalance.l1.o.DOUGroupsRemote;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.annotation.ejb.cache.simple.CacheConfig;
import static org.itx.jbalance.l1.api.filters.Filter.appendFilter;

@Stateless(name="HAutoRegister")
@RemoteBinding(jndiBinding = Constants.JNDI_HAUTOREGISTER_REMOTE)
@LocalBinding(jndiBinding = Constants.JNDI_HAUTOREGISTER_LOCAL)
@CacheConfig(maxSize = 1000, idleTimeoutSeconds = 240, removalTimeoutSeconds = 3600)
public class HAutoRegisterBean extends CommonObjectBean<HAutoRegister> implements HAutoRegisterRemote, HAutoRegisterLocal{

	private static final long serialVersionUID = -8430635966944491477L;

	
	@EJB(name=Constants.JNDI_HDOUREQUESTS_LOCAL)
	HDouRequestsLocal hDouRequestsLocal;

	@EJB(name=Constants.JNDI_HREGISTER_LOCAL)
	HRegisterLocal hRegisterLocal;
	
	@EJB(name=Constants.JNDI_DOUS_REMOTE)
	DOUGroupsRemote douGroupsRemote;

	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	/**
	 * ВОТ ОН!
	 * Тот самый метод, который делает ПРОТОКОЛ
	 * 
	 * 
	 * 1. 
	 */
	@Override
	@TransactionTimeout(3600)
	@org.jboss.ejb3.annotation.TransactionTimeout(3600)
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public HAutoRegister autoRegister(HAutoRegister autoRegister) {
		
		HRegister register = new HRegister();
		register.setAgereprocessdate(autoRegister.getCalculationDate());
		register.setRegisterDate(new Date());
		register = hRegisterLocal.create(register);
		autoRegister = create(autoRegister);
		Set<SAutoRegister> sAutoRegisters = autoRegister.getSAutoRegisters();
		
		List<DOUGroups> ageGroups = getAgeGroups(sAutoRegisters);
		
		
		for (SAutoRegister sAutoRegister : sAutoRegisters) {
			sAutoRegister.setDou(getManager().find(Dou.class, sAutoRegister.getDou().getUId()));
		}
		
		autoRegister.setHRegister(register);
		for (DOUGroups ageGroup : ageGroups) {
			forOneAgeGroup(ageGroup, autoRegister, register);
		}
		
		if(autoRegister.isIncludeRefuses()){
			for (DOUGroups ageGroup : ageGroups) {
				doRegused(ageGroup, autoRegister, register);
			}
		}
		
		for (SAutoRegister sAutoRegister : sAutoRegisters) {
			sAutoRegister.setDou(getManager().find(Dou.class, sAutoRegister.getDou().getUId()));
		}
		
		
		return autoRegister;
	}


	/**
	 * Получение списка возрастных групп
	 * @param sAutoRegisters
	 * @return
	 */
	private List<DOUGroups> getAgeGroups(Set<SAutoRegister> sAutoRegisters) {
//		
		List<DOUGroups> ageGroupsAll = douGroupsRemote.getAll();
//		отсеем группы, для которых НЕТ мест
		Set<DOUGroups> ageGroupsSet = new HashSet<DOUGroups>();
		for (SAutoRegister s : sAutoRegisters) {
			for (DOUGroups douGroup : ageGroupsAll) {
				if(s.getDouGroup().equals(douGroup.getId())){
					ageGroupsSet.add(douGroup);
				}
			}
		}
		List<DOUGroups> ageGroups = new ArrayList<DOUGroups>();
		ageGroups.addAll(ageGroupsSet);
		Collections.sort(ageGroups, new Comparator<DOUGroups>() {
			@Override
			public int compare(DOUGroups o1, DOUGroups o2) {
				return new Integer(o1.getId()).compareTo(o2.getId());
			}
		});
		return ageGroups;
	}
	

	private void doRegused(DOUGroups ageGroup, HAutoRegister autoRegister,
			HRegister register) {
		List<Long> douIds = new ArrayList<Long> ();
		for (SAutoRegister sAutoRegister : autoRegister.getSAutoRegisters()) {
			if(sAutoRegister.getSeatsRquested() > 0 && sAutoRegister.getDouGroup().equals(ageGroup.getId())){
				douIds.add(sAutoRegister.getDou().getUId());
			}
		}
		
		List<HDouRequest> suitableDouRequests = getSuitableDouRequests(ageGroup, autoRegister.getCalculationDate(), false, douIds, register.getUId());
		for (HDouRequest hDouRequest : suitableDouRequests) {
//			ОТКАЗ
			createSRegister(register, hDouRequest, null , Action.REFUSAL, null);
		}
		
		getManager().flush();
	}


	/**
	 * Распределяет места для заданной возрастной группы
	 * @param ageGroup
	 * @param autoRegister
	 */
	private void forOneAgeGroup(DOUGroups ageGroup, HAutoRegister autoRegister, HRegister register){
//		Получаем список детских садов в которых есть места 
		List<SAutoRegister> sAutoRegisters = autoRegister.getSAutoRegisters(ageGroup.getId());
		List<DouData> dousData =  new ArrayList<DouData>();
		for (SAutoRegister sAutoRegister : sAutoRegisters) {
			if(sAutoRegister.getSeatsRquested() > 0){
				dousData.add(new DouData(sAutoRegister.getDou(), sAutoRegister.getSeatsRquested(), autoRegister.getPrivelegePart()));
			}
		}
//		нету мест в ДОУ на данную возрастную группу
		if(! dousData.isEmpty()){
//			Считаем рейтинги для ДОУ
			fillDouRatings(dousData, autoRegister, ageGroup);
//			Сортируем по рейтингу
			Collections.sort(dousData);
		}
		

		
		switch(autoRegister.getMethod()){
		case DOU_FILL:
			doDouFill(ageGroup, autoRegister, register, dousData);
			break;
		case WISHES:
			doWishes(ageGroup, autoRegister, register, dousData);
			break;
		}
		
		
		for (DouData douData : dousData) {
			SAutoRegister sAutoRegister = autoRegister.getSAutoRegister(douData.dou, ageGroup.getId());
			sAutoRegister.setSeatsPrivilegeUsed(douData.seatsUsedPrivelege);
			sAutoRegister.setSeatsUsed(douData.seatsUsedPrivelege + douData.seatsUsedNoPrivelege);
			getManager().merge(sAutoRegister);
		}
		
		
	}


	private void doDouFill(DOUGroups ageGroup, HAutoRegister autoRegister,
			HRegister register, List<DouData> dousData) {
		if(! isThereFreeNoPrivilgeSeat(dousData) &&  !isThereFreePrivilegeSeat(dousData)){
			return;
		}
//			Получаем подходящие РН
		List<HDouRequest> suitableDouRequests = getSuitableDouRequests(ageGroup, autoRegister.getCalculationDate(), true , getDouIds(dousData), register.getUId());
		getLog().debug("DouFill suitableDouRequests with privilege " + suitableDouRequests.size());
		for (HDouRequest hDouRequest : suitableDouRequests) {
			List<SDouRequest> specificationLines = hDouRequestsLocal.getSpecificationLines(hDouRequest);
			DouData suitableDou = getSuitableDouForDouFillMethod(specificationLines, dousData, true);
			if(suitableDou != null) {
				createSRegister(register, hDouRequest, suitableDou.dou, Action.GIVE_PERMIT, ageGroup.getId());
				suitableDou.seatsUsedPrivelege ++ ;
			}
			if(!isThereFreePrivilegeSeat(dousData)){
				break;
			}
		}
		
//			Все неизрасходованные льготные места сгорают :)
		for (DouData douDate : dousData) {
			douDate.movePrivelegePlaces();
		}
		
		/***************/
//			Получаем подходящие РН
		suitableDouRequests = getSuitableDouRequests(ageGroup, autoRegister.getCalculationDate(), false , getDouIds(dousData), register.getUId());
		getLog().debug("DouFill suitableDouRequests without privilege " + suitableDouRequests.size());
		for (HDouRequest hDouRequest : suitableDouRequests) {
			List<SDouRequest> specificationLines = hDouRequestsLocal.getSpecificationLines(hDouRequest);
			DouData suitableDou = getSuitableDouForDouFillMethod(specificationLines, dousData, false);
			if(suitableDou != null) {
				createSRegister(register, hDouRequest, suitableDou.dou, Action.GIVE_PERMIT, ageGroup.getId());
				suitableDou.seatsUsedNoPrivelege ++ ;
			} else {
//				В этой возрастной группе мест не нашлось. Но отказ выдавать рано. М.б. будут места в других возр. группах
				if(!isThereFreeNoPrivilgeSeat(dousData)){
					break;
				}
			}
		}
	}


	private void doWishes(DOUGroups ageGroup, HAutoRegister autoRegister,
			HRegister register, List<DouData> dousData) {
//		if(! isThereFreeNoPrivilgeSeat(dousData) &&  !isThereFreePrivilegeSeat(dousData)){
//			return;
//		}
//			Получаем подходящие РН
		List<HDouRequest> suitableDouRequests = getSuitableDouRequests(ageGroup, autoRegister.getCalculationDate(), true , getDouIds(dousData), register.getUId());
		getLog().debug("WISHES suitableDouRequests with privilege " + suitableDouRequests.size()+ "\nageGroup: " +ageGroup );
		getLog().debug(suitableDouRequests+"");
		for (HDouRequest hDouRequest : suitableDouRequests) {
			List<SDouRequest> specificationLines = hDouRequestsLocal.getSpecificationLines(hDouRequest);
			DouData suitableDou = getSuitableDouForWishesMethod(specificationLines, dousData, true);
			if(suitableDou != null) {
				createSRegister(register, hDouRequest, suitableDou.dou, Action.GIVE_PERMIT , ageGroup.getId());
				suitableDou.seatsUsedPrivelege ++ ;
			}
			if(!isThereFreePrivilegeSeat(dousData)){
				break;
			}
		}
		
		getManager().flush();
		
//			Все неизрасходованные льготные места сгорают :)
		for (DouData douDate : dousData) {
			douDate.movePrivelegePlaces();
		}
		
		/***************/
//			Получаем подходящие РН
		suitableDouRequests = getSuitableDouRequests(ageGroup, autoRegister.getCalculationDate(), false, getDouIds(dousData), register.getUId());
		getLog().debug("WISHES suitableDouRequests without privilege " + suitableDouRequests.size()+ "\nageGroup: " +ageGroup );
		getLog().debug(suitableDouRequests+"");
		for (HDouRequest hDouRequest : suitableDouRequests) {
			List<SDouRequest> specificationLines = hDouRequestsLocal.getSpecificationLines(hDouRequest);
			DouData suitableDou = getSuitableDouForWishesMethod(specificationLines, dousData, false);
			if(suitableDou != null) {
//					ПУТЕВКА
				createSRegister(register, hDouRequest, suitableDou.dou, Action.GIVE_PERMIT , ageGroup.getId());
				suitableDou.seatsUsedNoPrivelege ++ ;
			} else {
				if(!isThereFreeNoPrivilgeSeat(dousData)){
					break;
				}
			}
		}
		
		getManager().flush();
	}


	private SRegister createSRegister(HRegister register, HDouRequest hDouRequest,
			Dou dou, Action action, Integer ageGroupId) {
		SRegister sRegister = new SRegister();
		sRegister.setDouRequest(hDouRequest);
		sRegister.setFoundation(Foundation.DEFAULT);
		sRegister.setHUId(register);
		sRegister.setRnAction(action);
		sRegister.setDou(dou);
		sRegister.setAgeGroupId(ageGroupId);
		return hRegisterLocal.createSpecificationLine(sRegister, register);
	}
	
	
	
	
	private DouData getSuitableDouForDouFillMethod(List<SDouRequest> specificationLines,
			List<DouData> dousData, boolean privilege) {
		for (DouData douData : dousData) {
			if(!douData.isThereFreeNoPrivilgeSeat() && !douData.isThereFreePrivilegeSeat()){
				return null;
			}
			
			for(int rating = 3; rating>0; rating --){
				for (SDouRequest sDouRequest : specificationLines) {
					if(sDouRequest.getRating() == rating){
						if(douData.dou .equals(sDouRequest.getArticleUnit())){
							if(privilege && douData.isThereFreePrivilegeSeat()){
								return douData;
							} else if(!privilege && douData.isThereFreeNoPrivilgeSeat()){
								return douData;
							}
						}
					}
				}
			}
		}
		return null;
	}
	
	private List<Long> getDouIds(List<DouData> dousData){
		List<Long> res = new ArrayList<Long>();
		for (DouData douData : dousData) {
			res.add(douData.dou.getUId());
		}
		return res;
	}
	
	/**
	 * Получение подходящего очереднику ДОУ 
	 * Выбирается ДОУ с минимальным рейтингом. Пожелания очередника второстепенны
	 * @param specificationLines
	 * @param dousData
	 * @param privilege
	 * @return
	 */
	private DouData getSuitableDouForWishesMethod(List<SDouRequest> specificationLines,
			List<DouData> dousData, boolean privilege) {
		for(int rating = 3; rating>0; rating --){
			for (SDouRequest sDouRequest : specificationLines) {
				if(sDouRequest.getRating() == rating){
					for (DouData douData : dousData) {
						if(douData.dou .equals(sDouRequest.getArticleUnit())){
							if(privilege && douData.isThereFreePrivilegeSeat()){
								return douData;
							} else if(!privilege && douData.isThereFreeNoPrivilgeSeat()){
								return douData;
							}
						}
					}
				}
			}
		}
		return null;
	}


	private boolean isThereFreePrivilegeSeat(List<DouData> dousData){
		for (DouData douData : dousData) {
			if(douData.isThereFreePrivilegeSeat()){
				return true;
			}
		}
		return false;
	}
	
	private boolean isThereFreeNoPrivilgeSeat(List<DouData> dousData){
		for (DouData douData : dousData) {
			if(douData.isThereFreeNoPrivilgeSeat()){
				return true;
			}
		}
		return false;
	} 

	/**
	 * Получение ВСЕХ РН в заданной возрастной категории, претендующих на путевку
	 */
	@Override
	public List<HDouRequest> getSuitableDouRequests(DOUGroups ageGroup, Date  calculationDate, boolean privelege, 
			List<Long>douIds, Long currentRegister) {
		if(douIds.isEmpty()){
			return Collections.emptyList();
		}
		FilterParams filterParams = new FilterParams();
		StringBuilder hql = new StringBuilder();
		hql.append("select r from ").append(HDouRequest.class.getSimpleName()).append(" r ")
		.append("join r.child c  where r.closeDate is null and r.version is null ");
		DateFilter birthdayFilter = getBirthdayDateFilterByAgeGroup(calculationDate, ageGroup.getAgeFrom(), ageGroup.getAgeTo());
		appendFilter(birthdayFilter, filterParams, hql, "c.birthday");

//		Распределяем путевки только если планируемый год поступления не больше заданного или NULL
		hql.append(" and (r.year is null or r.year <= :year )");
		Calendar cal = Calendar.getInstance();
		cal.setTime(calculationDate);
		filterParams.addParam("year", cal.get(Calendar.YEAR));

//		Только если в активной очереди
		hql.append(" and r.status is :statusWait ");
		filterParams.addParam("statusWait", Status.WAIT);
		
//		не включен в протоколы на 1-м или 2-м шаге
		hql.append("  and not exists (select sreg from " +SRegister.class.getSimpleName()+ " sreg " +
				"join sreg.HUId hreg "+
		" where sreg.version is null and sreg.closeDate is null and sreg.rnAction = :givePermit and  sreg.douRequest = r " +
		"and (hreg.status = :status1 or hreg.status = :status2) and hreg.version is null and hreg.closeDate is null) ");
		
		
//		не включен в текущий протокол
		if(currentRegister != null){
			hql.append("  and not exists (select sreg from " +SRegister.class.getSimpleName()+ " sreg " +
					"join sreg.HUId hreg "+
			" where sreg.douRequest = r and hreg.UId = :currentRegister) ");
			filterParams.addParam("currentRegister", currentRegister);
		}
		
		hql.append(" and exists (select sdr from SDouRequest sdr  where sdr.articleUnit.UId in (:searchedDous) and sdr.HUId=r and sdr.version is null and sdr.closeDate is null)");
		filterParams.addParam("searchedDous", douIds);
		
		if(privelege){
			hql.append(" and r.privilege is not null ");
		}
		
		hql.append(" order by year(r.regDate) asc, r.dnumber asc ");
		
		filterParams.addParam("givePermit", Action.GIVE_PERMIT);
		filterParams.addParam("status1", RegisterStatus.FIRST);
		filterParams.addParam("status2", RegisterStatus.SECOND);
		
		
		
		String sqlStr = hql.toString();
		getLog().debug("!!! " + sqlStr);
		Query query = getManager().createQuery(sqlStr);
		for (Entry<String, Object> e : filterParams.getParamMap().entrySet()) {
			query=query.setParameter(e.getKey()	, e.getValue());
		}
		List<HDouRequest> douRequests = query.getResultList();
		return douRequests;
	}
	
	
	/**
	 * Заполняет поле rating у эллементов коллекции dousData
	 * @param dousData
	 * @param autoRegister
	 * @param ageGroup
	 */
	private void fillDouRatings(List<DouData> dousData,
			HAutoRegister autoRegister, DOUGroups ageGroup) {
		List<Dou> dous = new ArrayList<Dou>();
		for (DouData douData : dousData) {
			dous.add(douData.dou);
		}
		Map<Dou, Double> calculateDouRating = calculateDouRating(dous, ageGroup, autoRegister.getCalculationDate() , autoRegister.getMethod());
		for (DouData douData : dousData) {
			Double r = calculateDouRating.get(douData.dou);
			douData.rating = r==null? 0: r;
		}
	}


	class DouData implements Comparable<DouData>{
		Dou dou;
		Double rating;
		/**
		 * Сколько всего изначально выделено обыкновенных  мест 
		 * Не льготных
		 */
		int seatsRequestedNoPrivelege;
		/**
		 * Сколько всего изначально выделено мест льготникам
		 */
		int seatsRequestedPrivelege;
		
		/**
		 * Испольщовано мест
		 */
		int seatsUsedNoPrivelege = 0;
		/**
		 * Испольщовано мест
		 */
		int seatsUsedPrivelege  = 0;
		/**
		 * 
		 * @param dou
		 * @param seatsRequested
		 * @param privelegePart - процент льгтной категории
		 */
		private DouData(Dou dou, int seatsRequested,  Integer privelegePart) {
			super();
			this.dou = dou;
			seatsRequestedPrivelege = (seatsRequested * privelegePart) / 100;
			seatsRequestedNoPrivelege = seatsRequested - seatsRequestedPrivelege;
		}
		
		/**
		 * Когда льготники закончились, а места остались, мы перемещаем места льготников 
		 */
		void movePrivelegePlaces(){
			if(seatsRequestedPrivelege > seatsUsedPrivelege){
				seatsRequestedNoPrivelege+=(seatsRequestedPrivelege - seatsUsedPrivelege);
				seatsRequestedPrivelege = seatsUsedPrivelege;
			}
		}
		
		boolean isThereFreePrivilegeSeat(){
			return seatsRequestedPrivelege > seatsUsedPrivelege;
		}
		
		boolean isThereFreeNoPrivilgeSeat(){
			return seatsRequestedNoPrivelege > seatsUsedNoPrivelege;
		}

		int getAllRequested(){
			return seatsRequestedNoPrivelege + seatsRequestedPrivelege;
		}
		
		int getAllUsed(){
			return seatsUsedNoPrivelege + seatsUsedPrivelege;
		}


		@Override
		public int compareTo(DouData o) {
			return rating.compareTo(o.rating);
		}
	}
	
	
	/**
	 * Расчитывает рейтинг ДОУ. 
	 * Рейтинг используется при распределении мест. В первую очередь заполняются ДОУ с минимальным рейтингом.
	 * В теории это обеспечит максимальную эффективность.
	 */
	@Override
	public Map<Dou, Double> calculateDouRating(List<Dou> dous, DOUGroups ageGroup,Date calculationDate,  Method method){
//		getManager().createQuery("select d from " + Dou.class.getSimpleName() + " d where d.closeDate is null and d.version is null");
		
		StringBuilder hql = new StringBuilder();
		if(method == Method.DOU_FILL){
			hql.append("select count(s), s.articleUnit.UId from ");
		} else {
			hql.append("select sum(s.rating), s.articleUnit.UId from ");
		}
		hql.append(SDouRequest.class.getSimpleName());
		hql.append(" s join s.HUId h " );
		hql.append(" join h.child c ");
		hql.append( "where s.version is null and s.closeDate is null and s.articleUnit in (:dous) ");
		
		DateFilter birthdayDateFilter = getBirthdayDateFilterByAgeGroup(calculationDate, ageGroup.getAgeFrom(), ageGroup.getAgeTo());
		FilterParams filterParams = new FilterParams();
		filterParams.addParam("dous", dous);
		appendFilter(birthdayDateFilter, filterParams , hql, "c.birthday");
		
		hql.append(" group by s.articleUnit.UId ");
		
		Query query = getManager().createQuery(hql.toString());
		
		for (Entry<String, Object> e : filterParams.getParamMap().entrySet()) {
			query=query.setParameter(e.getKey()	, e.getValue());
		}
		
		List<?> resultList = query.getResultList();
		Map<Dou, Double> result = new HashMap<Dou, Double>();
		
		for (Dou dou: dous) {
			Double rating = 0d;
			for (Object o : resultList) {
				Object obj[] = (Object[]) o;
				Number douUid = (Number) obj[1];
				if(douUid.equals(dou.getUId())){
					rating = ((Number) obj[0]).doubleValue();
					break;
				}
			}
			result.put(dou, rating);
		}
		return result;
	}
	
	
	/**
	 * Для возрастной группы, расчитываем диапазон дат рождения
	 * @param calculateDate - дата на которую производим расчет
	 * @param fromMonth возраст ребенка в мес "С"
	 * @param toMonth возраст ребенка в мес "ПО"
	 * @return готовый фильтр для поля birthday
	 */
	private DateFilter getBirthdayDateFilterByAgeGroup(Date calculateDate, int fromMonth, int toMonth){
			Calendar cal = Calendar.getInstance();
			cal.setTime(calculateDate);
			cal.add(Calendar.MONTH, -1* fromMonth);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			Date to = cal.getTime();
			getLog().debug("birthdayTo:"+ simpleDateFormat.format(to));
			
		
			cal = Calendar.getInstance();
			cal.setTime(calculateDate);
			
			cal.add(Calendar.MONTH, -1* toMonth -1);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			Date from = cal.getTime(); 
			getLog().debug("birthdayFrom:"+ simpleDateFormat.format(from));
			
			return new DateFilter(DateFilterType.BETWEEN, Arrays.asList(from, to));
	}
	
	
}
