package org.itx.jbalance.l1.d;


import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l0.s.SRegister.Foundation;
import org.itx.jbalance.l1.api.PermitSearchParams;
import org.itx.jbalance.l1.api.RegisterSearchParams;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface HRegisterLocal extends CommonDocument<HRegister,SRegister>{
	
	Integer getAllCount();
	List<HRegister> search(RegisterSearchParams params);
	Integer getSearchCount(RegisterSearchParams params);
	Integer getSpecCount(Long huid);
	List<SRegister>getSpecByDouRequest(Long uid);
	List<Foundation>getFoundations();
	
	Integer getPermitCnt(PermitSearchParams params) ;
	HRegister changeStatus(Long regUid, RegisterStatus newStatus);
	
}
