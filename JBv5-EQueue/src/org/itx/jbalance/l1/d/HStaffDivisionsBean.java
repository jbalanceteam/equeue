package org.itx.jbalance.l1.d;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.s.SStaffDivision;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.common.CommonDocumentBean;
import org.jboss.annotation.ejb.RemoteBinding;

@Stateless(name = "HStaffDivisions")
@RemoteBinding(jndiBinding = Constants.JNDI_HSTAFFDIVISIONS_REMOTE)
public class HStaffDivisionsBean extends CommonDocumentBean<HStaffDivision , SStaffDivision> implements HStaffDivisionsRemote{

	private static final long serialVersionUID = 1L;

}
