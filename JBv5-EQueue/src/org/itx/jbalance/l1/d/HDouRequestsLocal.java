package org.itx.jbalance.l1.d;


import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.common.CommonDocument;

@Local
public interface HDouRequestsLocal extends CommonDocument<HDouRequest,SDouRequest>{
	/**
	 * 
	 * @param pattern
	 * @param privilege 
	 * true - privilege presents 
	 * false - privilege don't presents
	 * null - both cases
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	List<HDouRequest>search(DouRequestsSearchParams params);
	Integer getSearchCount(DouRequestsSearchParams params);
	Integer getAllCount();
//	void setId(Long uid);
	
	
	
	
//	public void synchronizeSpecification(HDouRequest doc, List<Dou>dous);
	/**
	 * Дурацкая проверка!
	 * Дата регистрации не должна быть мньше(раньше), максимальной за год 
	 * @param regDate
	 * @return
	 */
	public Date validate(Date regDate);
	
	/**
	 * Метод для отчета
	 * @param year
	 * @return
	 */
//	public Integer getPerehod(int year,Sex sex);
}
