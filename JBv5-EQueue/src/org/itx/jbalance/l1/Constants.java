package org.itx.jbalance.l1;

public class Constants {
	
	public static final String JNDI_HDOUREQUESTS_LOCAL = "HDouRequests/local";
	public static final String JNDI_HDOUREQUESTS_REMOTE = "HDouRequests/remote";
	
	public static final String JNDI_HFREEDIVISIONS_REMOTE = "HFreeDivisions/remote";
	
	
	public static final String JNDI_HREGISTER_REMOTE =  "HRegister/remote";
	public static final String JNDI_HREGISTER_LOCAL =  "HRegister/local";
	
	public static final String JNDI_HAUTOREGISTER_REMOTE =  "HAutoRegister/remote";
	public static final String JNDI_HAUTOREGISTER_LOCAL =  "HAutoRegister/local";
	
	public static final String JNDI_HSTAFFDIVISIONS_REMOTE =  "HStaffDivisions/remote";
	
	public static final String JNDI_BIRTHSERTIFICATES_REMOTE =  "BirthSertificates/remote";
	public static final String JNDI_BIRTHSERTIFICATES_LOCAL =   "BirthSertificates/local";
	
	public static final String JNDI_PERMITS_REMOTE =  "Permits/remote";
	public static final String JNDI_PERMITS_LOCAL =   "Permits/local";
	
	public static final String JNDI_DOUGROUPS_REMOTE =  "DOUGroups/remote";
	
	public static final String JNDI_DOUS_REMOTE =   "Dous/remote";
	
	public static final String JNDI_EMAILS_LOCAL =   "Emails/local";
	
	public static final String JNDI_JURIDICALTYPES_REMOTE =    "JuridicalTypes/remote";
	
	public static final String JNDI_PRIVILEGES_REMOTE =  "Privileges/remote";
	
	public static final String JNDI_REGIONS_REMOTE =  "Regions/remote";
	
	public static final String JNDI_STAFFDIVISIONS_REMOTE =  "StaffDivisions/remote";
	
	
	public static String JNDI_JMS_CONNECTIONFACTORY = "ConnectionFactory";
	
	public static String JNDI_JMS_DOUCATCHEQUEUE = "queue/dousCache";
	
}
