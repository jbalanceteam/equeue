package org.itx.jbalance.l1.o;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l1.Constants;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.logging.Logger;

@Stateless(name="DOUGroups")
@RemoteBinding(jndiBinding = Constants.JNDI_DOUGROUPS_REMOTE)
public class DOUGroupsBean  implements DOUGroupsRemote {

	private static final long serialVersionUID = 1L;

	private final Logger log = Logger.getLogger(getClass());

	List<DOUGroups> all ;
	
	public DOUGroupsBean(){
		all = new ArrayList<DOUGroups>();
		all.add(new DOUGroups(1, "Группа раннего возраста",18,36));
		all.add(new DOUGroups(8, "Первая младшая", 24, 36));
		all.add(new DOUGroups(2, "Вторая младшая группа",37,48));
		all.add(new DOUGroups(3, "Средняя группа",49,60));
		all.add(new DOUGroups(4, "Старшая группа",61,72));
		all.add(new DOUGroups(5, "Подготовительная группа",73,84));
		all.add(new DOUGroups(6, "Разновозрастная гр. младшего дошкольного возраста",18,48));
		all.add(new DOUGroups(7, "Разновозрастная гр. старшего дошкольного возраста",48,84));
	}
	
	@Override
	public List<DOUGroups> getAll() {
		log.debug("getAll() => "+all);
		return all;
	}

	@Override
	public DOUGroups getById(int id) {
		for (DOUGroups g : all) {
			if(g.getId() == id){
				return g;
			}
		}
		return null;
	}
}
