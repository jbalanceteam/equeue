package org.itx.jbalance.l1.o;

import java.util.Date;
import java.util.List;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.itx.jbalance.l0.Age;
import org.itx.jbalance.l0.AgeCalculator;
import org.itx.jbalance.l0.o.Permit;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.api.filters.FilterParams;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;

import static org.itx.jbalance.l1.api.filters.Filter.appendFilter;

@Stateless(name = "Permits")
@RemoteBinding(jndiBinding = Constants.JNDI_PERMITS_REMOTE)
@LocalBinding(jndiBinding = Constants.JNDI_PERMITS_LOCAL)
public class PermitsBean extends CommonObjectBean<Permit> implements PermitsRemote, Permits{

	private static final long serialVersionUID = -8430635966944491477L;

	@Override
	public Permit create(Permit object) {
		object.setPermitDate(new Date());
		Age age = AgeCalculator.calculateAge(object.getChild().getBirthday(), object.getPermitDate());
		object.setAge(age.getYears() * 12 + age.getMonths());
		return super.create(object);
	}


	@Override
	public List<Permit> search(GetPermitsRequest getPermitsRequest) {
		if(getPermitsRequest==null){
			throw new RuntimeException("getPermitsRequest is required");
		}
		if(getPermitsRequest.getFrom()==null)
			getPermitsRequest.setFrom(0);
		if(getPermitsRequest.getTo()==null)
			getPermitsRequest.setTo(100000);
		
		
		StringBuilder sql =new StringBuilder( "select p " +  produceFrom());
		FilterParams filterParams = new FilterParams();
	
		sql.append(produceWhere(getPermitsRequest, filterParams));
				
		sql.append(" order by hreg.dnumber desc, privilege.UId, year(douRequest.regDate) , douRequest.dnumber");
		
		
		String sql2s = sql.toString();
		getLog().debug(sql2s);
		Query query = getManager().createQuery(sql2s);
		query=fillParams(getPermitsRequest, query, filterParams);
		
		@SuppressWarnings("unchecked")
		List<Permit> resultList = query.setMaxResults(getPermitsRequest.getTo()-getPermitsRequest.getFrom()+1).setFirstResult(getPermitsRequest.getFrom()).getResultList();

		
		return resultList;
	}


	private String produceFrom() {
		return "from " + Permit.class.getSimpleName() + " p " +
								"join p.child c " +
								"join p.dou dou  " +
								"join p.SRegister sreg " +
								"join sreg.HUId hreg " +
								"join p.HDouRequest douRequest " +
								"left join p.privilege privilege ";
	}

	
	/**
	 * 
	 * @param params
	 * @param query
	 * @param filterParams
	 * @return
	 */
	private Query fillParams(GetPermitsRequest params,
			Query query, FilterParams filterParams) {
		
		if(params.getEffectiveDate() != null){
			query=query.setParameter("effectiveDate", params.getEffectiveDate());
			query=query.setParameter("effectiveDate", params.getEffectiveDate());
		}
		
		for (Entry<String, Object> e : filterParams.getParamMap().entrySet()) {
			query=query.setParameter(e.getKey()	, e.getValue());
		}
		return query;
	}
	
	
	private StringBuilder produceWhere(	GetPermitsRequest  params, FilterParams filterParams) {
		getLog().debug("" + params);
		
		StringBuilder query = new StringBuilder();
		
		query.append("where p.version is null ");
		
		appendFilter(params.getPermitDate(), filterParams, query, "p.permitDate");
		appendFilter(params.getAge(), filterParams, query, "p.age");
		appendFilter(params.getChildUid(), filterParams, query, "c.UId");
		appendFilter(params.getBirthday(), filterParams, query, "c.birthday");
		
		appendFilter(params.getDous(), filterParams, query, "dou.UId");
		appendFilter(params.getExact_privilege(), filterParams, query, "privilege.UId");
		
		
		//				Наличие льготы
		if(Boolean.TRUE.equals(params.getPrivilege())){
			query.append(" and privilege is not null ");
		}else if(Boolean.FALSE.equals(params.getPrivilege())){
			query.append(" and privilege is null ");
		}
		
		if(params.getEffectiveDate() == null){
			query.append(" and p.closeDate is null ");
		} else {
			query.append(" and (p.closeDate is null or p.closeDate > :effectiveDate)  ");
			query.append(" and p.permitDate <= :effectiveDate ");
		}


		//		поиск по всем полям
		if(params.getSearchByAllTextFields()!=null && !params.getSearchByAllTextFields().isEmpty()){
			query.append(" and " +
					"(lower(c.surname)  like lower(concat(:surname,'%')) or " +
					" lower(c.realName) like lower(concat(:realName,'%')) or " +
					" lower(c.patronymic) like lower(concat(:patronymic,'%')) or " +
					" lower(c.passport) like lower(concat(:passport,'%')) or " +
					" lower(c.EMail) like lower(concat(:email,'%')) or " +
					" lower(c.phone) like lower(concat(:phone,'%')) or "+
					" lower(douRequest.address) like lower(concat(:address,'%')) or " +
					" lower(concat(concat(douRequest.dnumber,'-'), year(douRequest.regDate))) like lower(concat(:reqNumber,'%')) ) ") ;
			
			filterParams.addParam("surname", params.getSearchByAllTextFields());
			filterParams.addParam("realName", params.getSearchByAllTextFields());
			filterParams.addParam("patronymic", params.getSearchByAllTextFields());
			filterParams.addParam("passport", params.getSearchByAllTextFields());
			filterParams.addParam("email", params.getSearchByAllTextFields());
			filterParams.addParam("phone", params.getSearchByAllTextFields());
			filterParams.addParam("address", params.getSearchByAllTextFields());
			filterParams.addParam("reqNumber", params.getSearchByAllTextFields());
		}
		return query;
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer getSearchCount(GetPermitsRequest params) {
		getLog().debug("Search Count METRICS [0]   params: "+params);
		StringBuilder sql =new StringBuilder( "select count(p) " +
				produceFrom());
				
	
				
		FilterParams filterParams = new FilterParams();
		sql.append(produceWhere(params, filterParams));
		Query query = getManager().createQuery(sql.toString());
		query = fillParams(params,query, filterParams);
		getLog().debug("Search Count METRICS [1]");
		Date start = new Date();
		Number res = (Number)query.getSingleResult();
		getLog().debug("Search Count METRICS [2] query executed in " + (System.currentTimeMillis() - start.getTime()) +" ms");
		return res.intValue();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Integer getAllCount() {
		Number res = (Number) getManager()
		.createQuery("select count(*) from Permit p where p.version is null and p.closeDate is null  ")
		.getSingleResult();
		return res.intValue();
	}

}
