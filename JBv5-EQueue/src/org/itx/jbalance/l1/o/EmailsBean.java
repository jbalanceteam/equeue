package org.itx.jbalance.l1.o;

import javax.ejb.Stateless;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.itx.jbalance.l0.o.EmailMessage;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.itx.jbalance.l2_api.utils.EmailHandlerMDB;
import org.jboss.annotation.ejb.LocalBinding;

@Stateless(name = "Emails")
@LocalBinding(jndiBinding = Constants.JNDI_EMAILS_LOCAL)
public class EmailsBean extends CommonObjectBean<EmailMessage> implements Emails{

	private static final long serialVersionUID = -8430635966944491477L;

	/**
	 * Сохраняет Email в таблице.
	 * Отправка производится позже - асинхронно
	 */
	@Override
	public EmailMessage create(EmailMessage object) {
		EmailMessage email = super.create(object);
		Long uid = email.getUId();
		
		try {
			notifyHandler(uid);
			return email;
		} catch (NamingException e) {
			getLog().error(e);
			throw new RuntimeException(e);
		} catch (JMSException e) {
			getLog().error(e);
			throw new RuntimeException(e);
		}
		
	}

	
	/**
	 * Отправляет JMS сообщение
	 * @param uid
	 * @throws NamingException
	 * @throws JMSException
	 */
	private void notifyHandler(Long uid) throws NamingException, JMSException{
		Context context = new InitialContext();
		QueueConnectionFactory queueFactory = (QueueConnectionFactory)context.lookup("ConnectionFactory");
		QueueConnection queueConnection = null;
		QueueSender queueSender = null;
		try {
			queueConnection = queueFactory.createQueueConnection();
			QueueSession queueSession = queueConnection.createQueueSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
			Queue queue = (Queue)context.lookup(EmailHandlerMDB.DESTINITION);
			queueSender = queueSession.createSender(queue);
			TextMessage message = queueSession.createTextMessage();
			message.setLongProperty("uid",uid);	
			queueSender.send(queue, message);
			
		} catch (JMSException e) {
			getLog().error(e);
		}finally{
			if(queueSender!=null)
				queueSender.close();
			if(queueConnection!=null)
				queueConnection.close();
		}
	}
	
}
