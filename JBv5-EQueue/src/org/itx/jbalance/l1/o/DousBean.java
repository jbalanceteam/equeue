package org.itx.jbalance.l1.o;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.DouRequestsSearchParams.OrderStatus;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.itx.jbalance.l1.d.HDouRequestsLocal;
import org.jboss.annotation.ejb.RemoteBinding;

@Stateless(name = "Dous")
@RemoteBinding(jndiBinding = Constants.JNDI_DOUS_REMOTE)
public class DousBean extends CommonObjectBean<Dou> implements DousRemote{

	private static final long serialVersionUID = -8430635966944491477L;

	@EJB HDouRequestsLocal douRequestsLocal;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Dou create(Dou object) {
		if(object.getRegion() == null){
			throw new RuntimeException("object.region must not be null");
		}
		return super.create(object);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public Dou delete(Dou dou) {
		if(dou == null){
			throw new RuntimeException("dou must not be null");
		}
		if(dou.getUId() == null){
			throw new RuntimeException("dou.uid must not be null");
		}
		
		DouRequestsSearchParams params = new DouRequestsSearchParams();
		params.searchedDous = new ArrayList<Long>();
		params.searchedDous.add(dou.getUId());
		params.orderStatus = OrderStatus.ACTIVE_QUEUE;
		List<HDouRequest> search = douRequestsLocal.search(params);
		for (HDouRequest hDouRequest : search) {
			List<SDouRequest> specificationLines = douRequestsLocal.getSpecificationLines(hDouRequest);
			for (SDouRequest sDouRequest : specificationLines) {
				if(sDouRequest.getArticleUnit().getUId().equals(dou.getUId())){
					douRequestsLocal.deleteSpecificationLine(sDouRequest);
				}
			} 
		}
		
		return super.delete(dou);
	}


	@Override
	public Dou getByNumber(String douNumber) {
		List<?> resultList = getManager()
			.createNamedQuery(Dou.QUERY_BY_NUMBER)
			.setParameter(Dou.QUERY_PARAM_NUMBER, douNumber)
			.getResultList();
		if(resultList.isEmpty()){
			return null;
		} else {
			return (Dou) resultList.get(0);
		}
	}
}
