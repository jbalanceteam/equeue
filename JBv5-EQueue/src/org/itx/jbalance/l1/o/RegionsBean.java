package org.itx.jbalance.l1.o;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.RemoteBinding;

@Stateless(name = "Regions")
@RemoteBinding(jndiBinding = Constants.JNDI_REGIONS_REMOTE)
public class RegionsBean extends CommonObjectBean<Region> implements RegionsRemote{

	private static final long serialVersionUID = -8430635966944491488L;



	@Override
	public Region delete(Long uid) {
		return super.delete(getManager().find(Region.class, uid));
	} 
	
	
	@Override
	public Region update(Region p) {;
		Region region = getManager().find(Region.class, p.getUId());
		region.setName(p.getName());
		region.setDescription(p.getDescription());
		return super.update(region);
	} 
	
}
