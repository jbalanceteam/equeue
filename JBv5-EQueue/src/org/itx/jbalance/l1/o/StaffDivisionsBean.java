package org.itx.jbalance.l1.o;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.o.StaffDivision;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.RemoteBinding;

@Stateless(name = "StaffDivisions")
@RemoteBinding(jndiBinding = Constants.JNDI_STAFFDIVISIONS_REMOTE)
public class StaffDivisionsBean extends CommonObjectBean<StaffDivision> implements StaffDivisionsRemote{

	private static final long serialVersionUID = -8430635966944491477L;

}
