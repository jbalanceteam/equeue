package org.itx.jbalance.l1.o;

import java.util.ArrayList;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.o.JuridicalType;
import org.itx.jbalance.l1.Constants;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.logging.Logger;
/**
 * HARDCODE BOOK
 * @author apv
 *
 */
@Stateless
@RemoteBinding(jndiBinding = Constants.JNDI_JURIDICALTYPES_REMOTE)
public class JuridicalTypesBean implements JuridicalTypesRemote {

	private static final long serialVersionUID = -8430635966944491477L;

	private final Logger log = Logger.getLogger(getClass());

	@Override
	public ArrayList<JuridicalType> getAll() {
		ArrayList<JuridicalType> result = new ArrayList<JuridicalType>();
		long uid = 0;
		result.add(new JuridicalType(++uid ,"Общеразвивающего вида"));
		result.add(new JuridicalType(++uid ,"Комбинированного вида"));
		result.add(new JuridicalType(++uid ,"Центр развития ребенка"));
		result.add(new JuridicalType(++uid ,"НШ - ДС"));
		result.add(new JuridicalType(++uid ,"Прогимназия"));
		result.add(new JuridicalType(++uid ,"Детский сад"));
		result.add(new JuridicalType(++uid ," Филиал детского сада"));
		result.add(new JuridicalType(++uid ,"Компенсирующий вид"));
		result.add(new JuridicalType(++uid ,"Для детей раннего возраста"));

		log.trace("getAll() => "+result);
		return result;
	}
	
}
