package org.itx.jbalance.l1.o;


import javax.ejb.Local;

import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l1.common.CommonObject;

@Local
public interface BirthSertificates extends CommonObject<BirthSertificate>{
	BirthSertificate delete(Long uid);
	BirthSertificate update(BirthSertificate p);
}
