package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Remote;

import org.itx.jbalance.l0.o.DOUGroups;

@Remote
//extends CommonObject
public interface DOUGroupsRemote  {
	public List<DOUGroups> getAll();
	DOUGroups getById(int id);
}
