package org.itx.jbalance.l1.o;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.LocalBinding;
import org.jboss.annotation.ejb.RemoteBinding;

@Stateless(name = "BirthSertificates")
@RemoteBinding(jndiBinding = Constants.JNDI_BIRTHSERTIFICATES_REMOTE)
@LocalBinding(jndiBinding = Constants.JNDI_BIRTHSERTIFICATES_LOCAL)
public class BirthSertificatesBean extends CommonObjectBean<BirthSertificate> implements BirthSertificatesRemote, BirthSertificates{

	private static final long serialVersionUID = -8430635966944491477L;


	@Override
	public BirthSertificate delete(Long uid) {
		return super.delete(getByUId(uid, BirthSertificate.class));
	} 
	
	
	@Override
	public BirthSertificate update(BirthSertificate p) {
		BirthSertificate birthSertificate = getManager().find(BirthSertificate.class, p.getUId());
		birthSertificate.setSex(p.getSex());
		birthSertificate.setName(p.getName());
		birthSertificate.setDescription(p.getDescription());
		birthSertificate.setSeria(p.getSeria());
		birthSertificate.setNumber(p.getNumber());
		birthSertificate.setDate(p.getDate());
		birthSertificate.setGuardian1(p.getGuardian1());
		birthSertificate.setGuardian2(p.getGuardian2());
		return super.update(birthSertificate);

	} 
}
