package org.itx.jbalance.l1.o;

import javax.ejb.Stateless;

import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.common.CommonObjectBean;
import org.jboss.annotation.ejb.RemoteBinding;

@Stateless(name = "Privileges")
@RemoteBinding(jndiBinding = Constants.JNDI_PRIVILEGES_REMOTE)
public class PrivilegesBean extends CommonObjectBean<Privilege> implements PrivilegesRemote{

	private static final long serialVersionUID = -8430635966944491477L;

	@Override
	public Privilege delete(Long uid) {
		return super.delete(getManager().find(Privilege.class, uid));
	} 
	
	@Override
	public Privilege update(Privilege p) {
		Privilege privilege = getManager().find(Privilege.class, p.getUId());
		privilege.setName(p.getName());
		privilege.setDescription(p.getDescription());
		return super.update(privilege);
	} 
	
}
