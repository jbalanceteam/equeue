package org.itx.jbalance.l1.o;


import javax.ejb.Remote;

import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l1.common.CommonObject;

@Remote
public interface PrivilegesRemote extends CommonObject<Privilege>{
	Privilege delete(Long uid);
	Privilege update(Privilege p);
}
