package org.itx.jbalance.l1.o;

import java.util.List;

import javax.ejb.Remote;

import org.itx.jbalance.l0.o.Permit;
import org.itx.jbalance.l1.common.CommonObject;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;

@Remote
public interface PermitsRemote extends CommonObject<Permit>{
	
	public List<Permit> search(GetPermitsRequest getPermitsRequest);
	public Integer getSearchCount(GetPermitsRequest getPermitsRequest);
	public Integer getAllCount();
}
