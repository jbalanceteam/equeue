package org.itx.jbalance.l1.o;


import javax.ejb.Remote;

import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l1.common.CommonObject;

@Remote
public interface RegionsRemote extends CommonObject<Region>{
	Region delete(Long uid);
	Region update(Region p);
}
