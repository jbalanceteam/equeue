package org.itx.jbalance.l1.o;


import java.util.ArrayList;

import javax.ejb.Remote;

import org.itx.jbalance.l0.o.JuridicalType;


@Remote
public interface JuridicalTypesRemote{
	public ArrayList<JuridicalType> getAll();
}
