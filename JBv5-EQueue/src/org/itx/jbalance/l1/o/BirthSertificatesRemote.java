package org.itx.jbalance.l1.o;


import javax.ejb.Remote;

import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l1.common.CommonObject;

@Remote
public interface BirthSertificatesRemote extends CommonObject<BirthSertificate>{
	BirthSertificate delete(Long uid);
	BirthSertificate update(BirthSertificate p);
}
