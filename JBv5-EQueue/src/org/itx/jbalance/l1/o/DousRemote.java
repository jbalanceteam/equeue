package org.itx.jbalance.l1.o;

import javax.ejb.Remote;

import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l1.common.CommonObject;

@Remote
public interface DousRemote extends CommonObject<Dou> {
	Dou create(Dou object);
	/**
	 * Получение ДОУ по номеру
	 * @param douNumber - номер ДОУ
	 * @return
	 */
	Dou getByNumber(String douNumber);
}
