package org.itx.jbalance.l1.api;

import java.io.Serializable;
import java.util.Date;

import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l0.s.SRegister.Action;

/**
 * Поиск путевок
 * @author apv
 *
 */
public class PermitSearchParams implements Serializable{

	private static final long serialVersionUID = -7710884015406067366L;
	public Date dateFrom;
	public Date dateTo;
	public Action action;
	public Sex	sex;
	
}
