package org.itx.jbalance.l1.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SearchParams implements Serializable{

	private static final long serialVersionUID = 1L;
	Integer from; 
	Integer to;
	List<SortedColumnInfo> sortInfo = new ArrayList<SortedColumnInfo>();
	
	/**
	 * Поиск записей актуальных на дату.
	 * Т.е. с датой создания ДО заданной 
	 * и датой удаления ПОСЛЕ заданной 
	 * 
	 * 
	 * !!! Обработка этого параметра реализована далеко не везде 
	 */
	private Date effectiveDate;
	
	public Integer getFrom() {
		return from;
	}
	public void setFrom(Integer from) {
		this.from = from;
	}
	public Integer getTo() {
		return to;
	}
	public void setTo(Integer to) {
		this.to = to;
	}
	public List<SortedColumnInfo> getSortInfo() {
		return sortInfo;
	}
	public void setSortInfo(List<SortedColumnInfo> sortInfo) {
		this.sortInfo = sortInfo;
	}
	public Date getEffectiveDate() {
		return effectiveDate;
	}
	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}
	
	
}
