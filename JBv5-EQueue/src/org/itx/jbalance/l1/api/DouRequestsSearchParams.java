package org.itx.jbalance.l1.api;

import java.util.List;

import org.itx.jbalance.l0.h.RecordColor;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.IntegerFilter;
import org.itx.jbalance.l1.api.filters.LongFilter;
import org.itx.jbalance.l1.api.filters.StringFilter;

public class DouRequestsSearchParams extends SearchParams{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * exact uid
	 */
	public LongFilter uid;
	
	/**
	 * exact registration number
	 */
	public StringFilter rn;
	public StringFilter firstname;
	public StringFilter lastname;
	public StringFilter middlename;
	
	public StringFilter bsSeria;
	public StringFilter bsNumber;
	
	public String pattern;
	public List<Long> searchedDous;
	
	/**
	 * Поиск по наименованию региона
	 */
	public String region;
	
	public Boolean privilege;
	public Boolean phone;
	
	public Long exact_privilege;
	
	/**
	 * true - путевка выдана
	 * false - путевка не выдана
	 * null - все
	 */
	public OrderStatus orderStatus;
	
	public Integer ageFrom;
	public Integer ageTo;
	
	/**
	 */
	public IntegerFilter plainYear;
	
	/**
	 * if withoutColor is true then recordColor property will be ignored
	 */
	public Boolean withoutColor;
	public RecordColor recordColor;
	
	public DateFilter regDate;
	public DateFilter sverkDate;
	public DateFilter birthday;
	
	public Sex sex;
	
	/**
	 * Найти все РН с номером меньше или равным данному
	 */
	public String minRn;
	
	/**
	 * Поставить льготников вперед
	 */
	public Boolean privilegesToBegin;

	
	
	
	@Override
	public String toString() {
		return "DouRequestsSearchParams [sortInfo="+ sortInfo + ", rn=" + rn + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", middlename=" + middlename
				+ ", pattern=" + pattern + ", searchedDous=" + searchedDous
				+ ", region=" + region + ", privilege=" + privilege
				+ ", phone=" + phone + ", exact_privilege=" + exact_privilege
				+ ", orderStatus=" + orderStatus + ", birthday="
				+ birthday + ", ageFrom="
				+ ageFrom + ", ageTo=" + ageTo + ", plainYear=" + plainYear
				+ ", withoutColor=" + withoutColor + ", recordColor="
				+ recordColor + ", regDate=" + regDate + ", sverkDate="
				+ sverkDate + ", sex=" + sex + ", minRn=" + minRn
				+ ", privilegesToBegin=" + privilegesToBegin + "]";
	}




	/**
	 * Для поиска по статусу
	 * @author apv
	 *
	 */
	public enum OrderStatus{
		/**
		 * Все подряд
		 */
		ALL("Все"),
		/**
		 * Вне очереди
		 */
		OUT_OF_QUEUE("Выведены из очереди"), 
		/**
		 * Путевка не выдана
		 */
		ACTIVE_QUEUE("Активная очередь"), 
		/**
		 * Это подвыборка  ACTIVE_QUEUE - те у кого был отказ
		 */
		REJECT("Получили отказ");
		
		private final String description;
		
		OrderStatus(String descr){
			this.description=descr;
		}

		public String getDescription() {
			return description;
		}
		
		public String toString(){
			return description;
		}

	}
	
	
}
