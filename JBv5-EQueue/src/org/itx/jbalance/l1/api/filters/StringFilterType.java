package org.itx.jbalance.l1.api.filters;

import java.util.List;

//import org.itx.jbalance.l1.utils.Obj;
//import org.itx.jbalance.l1.utils.Str;



/**
 *
 * @author jsweetland
 */
public enum StringFilterType implements FilterType {
	EQUALS_IC("Equals", ArgumentsExpected.ONE, true),
    STARTS_WITH_IC("Starts With", ArgumentsExpected.ONE, true),
    ENDS_WITH_IC("Ends With", ArgumentsExpected.ONE, true),
    CONTAINS_IC("Contains", ArgumentsExpected.ONE, true),
	
	
	/**
     * <p>Equals or equal-to.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> = <i>rvalue</i></tt></p>
     */
    EQUALS("Equals", ArgumentsExpected.ONE, false),
    /**
     * <p>Starts with.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> LIKE '<i>rvalue</i>%'</tt></p>
     */
    STARTS_WITH("Starts With", ArgumentsExpected.ONE, false),
    /**
     * <p>Ends with.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> LIKE '%<i>rvalue</i>'</tt></p>
     */
    ENDS_WITH("Ends With", ArgumentsExpected.ONE, false),
    /**
     * <p>Contains.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> LIKE '%<i>rvalue</i>%'</tt></p>
     */
    CONTAINS("Contains", ArgumentsExpected.ONE, false),
    /**
     * <p>Is null.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> IS NULL}.</tt></p>
     */
    NULL("Is Null", ArgumentsExpected.NONE, false),
    /**
     * <p>Is null.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> IS NULL or <i>length(trim(lvalue)) = 0</i>}.</tt></p>
     */
    BLANK("Is Null or Blank", ArgumentsExpected.NONE, false),
    /**
     * <p>In.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> IN (<i>rvalue</i>[0],
     * <i>rvalue</i>[1], ...)</tt></p>
     */
    IN("In", ArgumentsExpected.ONE_OR_MORE , false),
    /**
     * <p>Between.  Equivalent to:</p>
     *
     * <p><tt><i>lvalue</i> BETWEEN <i>rvalue1</i> AND <i>rvalue2</i></tt> <i>(inclusive)</i></p>
     */
    BETWEEN("Between", ArgumentsExpected.TWO, false);

    private final String description;
    private final ArgumentsExpected argumentsExpected;
    private final boolean ignoreCase; 

    /**
     * Returns a user-friendly description of the comparison operator
     *
     * @return a user-friendly description of the comparison operator
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * Returns the number of arguments expected by this filter type
     *
     * @return number of arguments expected by this filter type
     */
    public ArgumentsExpected getArgumentsExpected() {
        return argumentsExpected;
    }

    /**
     * Creates a new comparison with the specified description (internal use
     * only)
     *
     * @param description A user-friendly description of the comparison
     * operator
     */
    private StringFilterType(String description, ArgumentsExpected argumentsExpected, boolean ignoreCase) {
        this.description = description;
        this.argumentsExpected = argumentsExpected;
        this.ignoreCase = ignoreCase;
    }

    /**
     * Returns the user-friendly description of the comparison.  This returns
     * the same value as {@link #getDescription()}
     *
     * @return the user-friendly description of the comparison
     *
     * @see #getDescription()
     */
    @Override
    public String toString() {
        return description;
    }

    @Override
    public void appendFilter(StringBuilder query, FilterParams params, String fieldName, List<?> filterValues) {
        if(query == null) {
            throw new IllegalArgumentException("Query string builder is required");
        }

        if(params == null) {
            throw new IllegalArgumentException("Filter parameters are required");
        }

        if(FilterUtils.empty(fieldName)) {
            throw new IllegalArgumentException("Field name is required");
        }

        String paramName = null;
        if(this == NULL) {
            query.append(" ");
            query.append(fieldName);
            query.append(" is null");
        } else if (this == BLANK) {
            query.append(" (");
            query.append(fieldName);
            query.append(" is null");
            query.append(" or length(trim(");
            query.append(fieldName);
            query.append(")) = 0)");
        } else if(this == IN) {
            if(filterValues == null || filterValues.isEmpty()) {
                // IN (empty list) will always evaluate to false, but we will
                // get a query syntax exception if the parameter list is
                // missing.  To avoid this, substitute with an expression that
                // will always evaluate to false.
                query.append(" 1=0");
            } else {
                paramName = params.addParam(filterValues);
                query.append(" ");
                query.append(fieldName);
                query.append(" in (:");
                query.append(paramName);
                query.append(")");
            }
        } else if(this == BETWEEN) {
            if(filterValues == null || filterValues.size() != 2) {
                throw new IllegalArgumentException(this + " filter type requires exactly two filter values");
            }
            String paramName1 = params.addParam(filterValues.get(0));
            String paramName2 = params.addParam(filterValues.get(1));

            query.append(" ");
            query.append(fieldName);
            query.append(" between ");
            query.append(" :").append(paramName1);
            query.append(" and :").append(paramName2);
        } else {
            if(filterValues == null || filterValues.size() != 1) {
                throw new IllegalArgumentException(this + " filter type requires exactly one filter value");
            }

            String paramValue = FilterUtils.coerceToString(filterValues.get(0));

            if(ignoreCase){
            	query.append(" lower(");
            	query.append(fieldName);
                query.append(") ");
            } else {
            	query.append(" ");
            	query.append(fieldName);
                query.append(" ");
            }

            switch(this) {
                case CONTAINS_IC:
                    paramName = params.addParam('%' + paramValue + '%');
                    query.append(" like lower(:");
                    query.append(paramName);
                    query.append(")");
                    break;
                case ENDS_WITH_IC:
                    paramName = params.addParam('%' + paramValue);
                    query.append(" like lower(:");
                    query.append(paramName);
                    query.append(")");
                    break;
                case EQUALS_IC:
                    paramName = params.addParam(paramValue);
                    query.append(" = lower(:");
                    query.append(paramName);
                    query.append(")");
                    break;
                case STARTS_WITH_IC:
                    paramName = params.addParam(paramValue + '%');
                    query.append(" like lower(:");
                    query.append(paramName);
                    query.append(")");
                    break;
                    
                case CONTAINS:
                    paramName = params.addParam('%' + paramValue + '%');
                    query.append(" like :");
                    query.append(paramName);
                    break;
                case ENDS_WITH:
                    paramName = params.addParam('%' + paramValue);
                    query.append(" like :");
                    query.append(paramName);
                    break;
                case EQUALS:
                    paramName = params.addParam(paramValue);
                    query.append(" = :");
                    query.append(paramName);
                    break;
                case STARTS_WITH:
                    paramName = params.addParam(paramValue + '%');
                    query.append(" like :");
                    query.append(paramName);
                    break;
                default:
                    // Should never happen, but Sonar will complain if we don't
                    // have a default case
                    throw new UnsupportedOperationException("Unsupported filter type: " + this);
            }
        }
    }
}
