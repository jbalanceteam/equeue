package org.itx.jbalance.l1.api.filters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

//import org.itx.jbalance.l1.utils.DateUtils;

/**
 *
 * @author jsweetland
 */
public class DateFilter extends Filter<DateFilterType, Date> {
    private static final long serialVersionUID = 1L;

    private DateFilterType filterType;
    private List<Date> filterValues;

    public DateFilter() {}

    public DateFilter(DateFilterType filterType, Date filterValue) {
        setFilterType(filterType);
        setFilterValue(filterValue);
    }

    public DateFilter(DateFilterType filterType, List<Date> filterValues) {
        setFilterType(filterType);
        setFilterValues(filterValues);
    }

    public DateFilter(DateFilterType filterType) {
        setFilterType(filterType);
    }
    
    
    public static  DateFilter build(Date startOfDay, Date endOfDay){
    	if(startOfDay != null || endOfDay != null){
    		return new DateFilter(startOfDay, endOfDay);
    	} else {
    		return null;
    	}
    }

    private DateFilter(Date startOfDay, Date endOfDay) {
		if(startOfDay != null && endOfDay != null){
	    	setFilterType(DateFilterType.BETWEEN);
	        setFilterValues(Arrays.asList(startOfDay, endOfDay));
		} else if(startOfDay != null){
			setFilterType(DateFilterType.GTE);
	        setFilterValue(startOfDay);
		} else if(endOfDay != null){
			setFilterType(DateFilterType.LTE);
	        setFilterValue(endOfDay);
		} else {
			throw new RuntimeException("startOfDay or endOfDay mustn't be null");
		}
	}

	@XmlAttribute(required=true)
    @Override
    public DateFilterType getFilterType() {
        return filterType;
    }

    @Override
    public void setFilterType(DateFilterType filterType) {
        this.filterType = filterType;
    }

    @XmlElement(name="filterValue")
    @Override
    public List<Date> getFilterValues() {
        if (filterValues == null) {
            filterValues = new ArrayList<Date>();
        }
        return filterValues;
    }

    @Override
    public void setFilterValues(List<Date> filterValues) {
        this.filterValues = filterValues;
    }
    
 
    
}
