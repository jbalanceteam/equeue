package org.itx.jbalance.l1.api.filters;
/**
 * Перенес сюда частично методы из Str и Obj
 * из за проблем с GWT
 * @author apv
 *
 */
public class FilterUtils {

	
    /**
     * <p>
     * Returns {@code true} if the specified string is {@code null} or contains
     * nothing but whitespace characters (no matter how many whitespace
     * characters it contains.)
     * </p>
     *
     * @param str
     *            The string to check
     * @return {@code true} if the specified string contains no non-whitespace
     *         characters
     */
    public static boolean empty(String str) {
        return str == null || str.trim().isEmpty();
    }
    
    /**
     * Returns null if object is null, otherwise returns obj.toString
     * 
     * @param obj
     * @return
     */
    public static String coerceToString(Object obj) {
        if (obj == null) {
            return null;
        } else if (obj instanceof Enum<?>) {
            return ((Enum<?>) obj).name();
        } else {
            return String.valueOf(obj);
        }
    }
}
