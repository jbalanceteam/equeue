package org.itx.jbalance.l1.api.filters;

import java.util.List;

/**
 *
 * @author jsweetland
 */
public interface FilterType {

    public String getDescription();
    public ArgumentsExpected getArgumentsExpected();
    public abstract void appendFilter(StringBuilder query, FilterParams params, String fieldName, List<?> filterValues);

    public enum ArgumentsExpected {
        NONE,
        ONE,
        TWO,
        ONE_OR_MORE;
    }
}
