package org.itx.jbalance.l1.api.filters;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

//import org.itx.jbalance.l1.utils.Str;



/**
 *
 * @param <F>
 * @param <V>
 * @author jsweetland
 */
public abstract class Filter<F extends FilterType, V> implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    private boolean negated;

    @XmlTransient
    public abstract F getFilterType();

    public abstract void setFilterType(F filterType);

    @XmlTransient
    public abstract List<V> getFilterValues();

    public abstract void setFilterValues(List<V> filterValues);

    @XmlTransient
    public V getFilterValue() {
        return getFilterValues().isEmpty() ? null : getFilterValues().get(0);
    }

    public void setFilterValue(V filterValue) {
        List<V> filterValues = new ArrayList<V>();
        filterValues.add(filterValue);
        setFilterValues(filterValues);
    }

    @XmlAttribute(required = false)
    public boolean isNegated() {
        return negated;
    }

    public void setNegated(boolean negated) {
        this.negated = negated;
    }

    public void appendFilter(StringBuilder query, FilterParams filterParams, String fieldName) {
        if(query == null) {
            throw new IllegalArgumentException("Query string builder is required");
        }

        if(filterParams == null) {
            throw new IllegalArgumentException("Filter parameters are required");
        }

        if(fieldName==null || fieldName.isEmpty()) {
            throw new IllegalArgumentException("Field name is required");
        }

        if(isNegated()) {
            query.append(" not");
        }

        F filterType = getFilterType();
        if(filterType == null) {
            throw new IllegalStateException("Filter must have a filter type");
        }

        filterType.appendFilter(query, filterParams, fieldName, getFilterValues());
    }

//    @GwtIncompatible(value = "")
//    @Override
//    public String toString() {
//        StringBuilder string = new StringBuilder(getClass().getSimpleName());
//        string.append("(");
//        string.append("filterType=").append(getFilterType());
//        string.append(", filterValues={").append(Str.list(getFilterValues(), null, false)).append("}");
//        string.append(", negated=").append(negated);
//        return string.toString();
//    }
//
//    @GwtIncompatible(value = "")
//    public static <F extends Filter<?, ?>> F not(F filter) {
//        try {
//            @SuppressWarnings("unchecked")
//            F clone = (F)filter.clone();
//            clone.setNegated(!filter.isNegated());
//            return clone;
//        } catch(CloneNotSupportedException cnse) {
//            throw new RuntimeException(cnse);
//        }
//    }
    
    public static void appendFilter(Filter<?,?> filter, FilterParams filterParams, StringBuilder query, String fieldName){
		if(filter != null){
			query.append( " and ");
			filter.appendFilter(query, filterParams, fieldName);
		}
	}
    
}
