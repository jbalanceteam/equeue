package org.itx.jbalance.l1.api;

import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.LongFilter;

public class RegisterSearchParams extends SearchParams{

	private static final long serialVersionUID = 1L;
	public String custom;
	public DateFilter registerDate;
	/**
	 * Поиск по содержанию
	 */
	public LongFilter douRequestUid;
	@Override
	public String toString() {
		return "RegisterSearchParams [custom=" + custom + ", registerDate="
				+ registerDate + ", douRequestUid=" + douRequestUid + "]";
	}
	
	
	
}
