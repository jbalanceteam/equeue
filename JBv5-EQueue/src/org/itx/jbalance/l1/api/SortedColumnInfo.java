package org.itx.jbalance.l1.api;

import java.io.Serializable;

public class SortedColumnInfo implements Serializable{
 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
private String name;
 private boolean asc;
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public boolean isAsc() {
	return asc;
}
public void setAsc(boolean asc) {
	this.asc = asc;
}



public SortedColumnInfo() {
	super();
}
public SortedColumnInfo(String name, boolean asc) {
	super();
	this.name = name;
	this.asc = asc;
}
@Override
public String toString() {
	return "SortedColumnInfo [name=" + name + ", asc=" + asc + "]";
}
 
 
}
