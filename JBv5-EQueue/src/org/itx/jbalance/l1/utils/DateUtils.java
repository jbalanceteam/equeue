package org.itx.jbalance.l1.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.DateFilterType;

public class DateUtils {

	Calendar c = Calendar.getInstance();

	public Date getStartOfDay(Date date) {
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, c.getMinimum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getMinimum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getMinimum(Calendar.SECOND));
		c.set(Calendar.MILLISECOND, c.getMinimum(Calendar.MILLISECOND));
		return c.getTime();
	}

	public Date getStartOfMonth(Date date) {
		c.setTime(getStartOfDay(date));
		c.set(Calendar.DAY_OF_MONTH, c.getMinimum(Calendar.DAY_OF_MONTH));
		return c.getTime();
	}

	public Date getStartOfYear(Date date) {
		c.setTime(getStartOfMonth(date));
		c.set(Calendar.MONTH, c.getMinimum(Calendar.MONTH));
		return c.getTime();
	}
	
	
	public Date getStartOfYear(Integer year) {
		c.set(Calendar.YEAR, year);
		return getStartOfYear(c.getTime());
	}
	
	public Date getEndOfYear(Integer year) {
		c.set(Calendar.YEAR, year);
		return getEndOfYear(c.getTime());
	}

	
	public Date getEndOfDay(Date date) {
		c.setTime(date);
		c.set(Calendar.HOUR_OF_DAY, c.getMaximum(Calendar.HOUR_OF_DAY));
		c.set(Calendar.MINUTE, c.getMaximum(Calendar.MINUTE));
		c.set(Calendar.SECOND, c.getMaximum(Calendar.SECOND));
		c.set(Calendar.MILLISECOND, c.getMaximum(Calendar.MILLISECOND));
		return c.getTime();
	}

	public Date getEndOfMonth(Date date) {
		c.setTime(getEndOfDay(date));
		c.set(Calendar.DAY_OF_MONTH, c.getMaximum(Calendar.DAY_OF_MONTH));
		return c.getTime();
	}

	public Date getEndOfYear(Date date) {
		c.setTime(date);
		c.set(Calendar.MONTH, c.getMaximum(Calendar.MONTH));
		c.setTime(getEndOfMonth(c.getTime()));
		return c.getTime();
	}
	
	public String format(Date date, String pattern){
		if(pattern==null)
			pattern="yyyy-MM-dd";
		return new SimpleDateFormat(pattern).format(date);
	}
	
	   /**
     * Как то надо его причесать
     * @param df
     */
    @Deprecated
    public static void moveDatesToBounds(DateFilter dateFilter) {
    	if(dateFilter == null){
    		return;
    	} else if(dateFilter.getFilterType() == DateFilterType.BETWEEN){
			if(dateFilter.getFilterValues().get(0)!=null){
				Date date = dateFilter.getFilterValues().get(0);
				date.setTime( new DateUtils().getStartOfDay(date).getTime());
			}
			if(dateFilter.getFilterValues().size() > 1 && dateFilter.getFilterValues().get(1)!=null){
				Date date = dateFilter.getFilterValues().get(1);
				date.setTime( new DateUtils().getEndOfDay(date).getTime());
			}
    	} else if(dateFilter.getFilterType() == DateFilterType.GT || dateFilter.getFilterType() == DateFilterType.GTE){
    		if(dateFilter.getFilterValue() != null){
				Date date = dateFilter.getFilterValue();
				date.setTime( new DateUtils().getStartOfDay(date).getTime());
			}
    	} else if(dateFilter.getFilterType() == DateFilterType.LT || dateFilter.getFilterType() == DateFilterType.LTE){
    		if(dateFilter.getFilterValue() != null){
				Date date = dateFilter.getFilterValue();
				date.setTime( new DateUtils().getEndOfDay(date).getTime());
			}
    	}
    }
	
}
