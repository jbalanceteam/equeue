package org.itx.jbalance.l1.utils.mailsContent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class HtmlTemplatesProcessor {
	private static final String PROP_NAME_TEMPLATES_DIR="templates.dir";
	public static final String REPORT_CREATE_RN="createRn.html";
	public static final String REPORT_UPDATE_RN="updateRn.html";
	public static final String REPORT_PERMIT="permit.html";
	
	public static final String PARAM_RN="rn";
	public static final String PARAM_STATUS="status";
	private StringBuilder report;
	
	
	
	
	public HtmlTemplatesProcessor(String templateName) {
		report = getTemplateAsString(templateName);
	}
	
	public HtmlTemplatesProcessor(StringBuilder report) {
		this.report = report;
	}


	InputStream getTemplateAsInputStream(String filename) throws FileNotFoundException {
			InputStream resourceAsStream = null;
			
			String templateDirectory = getTemplateDirectory();
			
			if(templateDirectory!=null) {
				File file = new File(templateDirectory+"/"+filename);
				if(file.exists()){
					resourceAsStream = new FileInputStream(file);
				}
			} 
			if(resourceAsStream==null)
				resourceAsStream = getClass().getResourceAsStream("./org/itx/jbalance/l1/utils/mailsContent/"+filename);
			
			if(resourceAsStream==null)
				resourceAsStream = getClass().getResourceAsStream("org/itx/jbalance/l1/utils/mailsContent/"+filename);
			
			if(resourceAsStream==null)
				resourceAsStream = getClass().getResourceAsStream(filename);
			
			if(resourceAsStream==null)
				resourceAsStream = getClass().getResourceAsStream("./"+filename);
			
			
			URL resource = getClass().getResource(".");
			
			System.out.println(resource);
			
			if(resourceAsStream==null)
				resourceAsStream = getClass().getResourceAsStream("classpath:/org/itx/jbalance/l1/utils/mailsContent/"+filename);
			
			if(resourceAsStream==null){
			try {
				URL url = null;
				if(resourceAsStream==null){
					URL baseUrl = getClass().getResource(".");
				     if (baseUrl != null) {
				        url = new URL(baseUrl, filename);
				     } else {
				        url = getClass().getResource(filename);
				     }
				}
				resourceAsStream = new FileInputStream(new File(url.toURI()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return resourceAsStream;
	}
	
	
	 StringBuilder getTemplateAsString(String filename)  {
		try {
			InputStream is = getTemplateAsInputStream(filename);
			if(is == null){
				return null;
			}
			InputStreamReader inputStreamReader = new InputStreamReader(is, "UTF-8");
			
			
			final char[] buffer = new char[250];
			final StringBuilder out = new StringBuilder();
			  
				for (;;) {
			        int rsz = inputStreamReader.read(buffer, 0, buffer.length);
			        if (rsz < 0)
			          break;
			        out.append(buffer, 0, rsz);
			      }
			return out;
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
		 
	}
	
	/**
	 * спользуется в печатны форма на основе шаблонов.
	 * Заменяет <code>param</code> в шаблоне на <code>val</code>
	 * @param doc
	 * @param param
	 * @param val
	 */
	public void setVariable(String param,String val){
		
		String str = "#{"+param+"}";
		while(true){
			int startIndex = report.indexOf(str);
			int endIndex = startIndex + str.length();
			
			if(startIndex== -1){
				break;
			}
			
			report.replace(startIndex, endIndex, val);
		}
	}
	
	
	
	
	@Override
	public String toString() {
		return report.toString();
	}


	private static String getTemplateDirectory(){
		return System.getProperty(PROP_NAME_TEMPLATES_DIR);
	}
}
