package org.itx.jbalance.l1.utils;

import org.hibernate.EmptyInterceptor;
import org.jboss.logging.Logger;

public class CustomNullsFirstInterceptor extends EmptyInterceptor {

    private static final long serialVersionUID = -3156853534261313031L;
    private static Logger logger = Logger.getLogger(CustomNullsFirstInterceptor.class);
    private static final String ORDER_BY_TOKEN = "order by";

    public String onPrepareStatement(String sql) {
//    	logger.debug("",);
        int orderByStart = sql.toLowerCase().indexOf(ORDER_BY_TOKEN);
        if (orderByStart == -1) {
            return super.onPrepareStatement(sql);
        }
        orderByStart += ORDER_BY_TOKEN.length() + 1;
        int orderByEnd = sql.indexOf("limit", orderByStart);
        if (orderByEnd == -1) {
           orderByEnd = sql.indexOf(" UNION ", orderByStart);
            if (orderByEnd == -1) {
                orderByEnd = sql.length();
            }
        }
        String orderByContent = sql.substring(orderByStart, orderByEnd);
        String[] orderByNames = orderByContent.split("\\,");
        for (int i=0; i<orderByNames.length; i++) {
            if (orderByNames[i].trim().length() > 0) {
                if (orderByNames[i].trim().toLowerCase().endsWith("desc")) {
                    orderByNames[i] += " NULLS FIRST";
                } else if (orderByNames[i].trim().toLowerCase().endsWith("asc")){
                    orderByNames[i] += " NULLS LAST";
                }
            }
        }
        
        StringBuffer orderByContent1 = new StringBuffer();
        for (String s : orderByNames) {
        	orderByContent1.append(s);
        	orderByContent1.append(",");
		}
        if(orderByContent1.length() > 1){
        	orderByContent1.delete(orderByContent1.length()-1,orderByContent1.length());
        }
        
        sql = sql.substring(0, orderByStart) + orderByContent + sql.substring(orderByEnd); 
        return super.onPrepareStatement(sql);
    }

}