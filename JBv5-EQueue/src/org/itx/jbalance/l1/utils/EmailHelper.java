package org.itx.jbalance.l1.utils;

import java.util.ArrayList;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jboss.logging.Logger;


/**
 * @author apv@jbalance.org
 */
public class EmailHelper {
    private Logger log =  Logger.getLogger(EmailHelper.class);
    private Session session;
    private MimeMessage message;
    private BodyPart textBodyPart;
    private BodyPart htmlBodyPart;
    private List<BodyPart> alternateBodyParts = new ArrayList<BodyPart>();
    private List<BodyPart> attachmentBodyParts = new ArrayList<BodyPart>();

    public EmailHelper(String from, String to, String cc, String bcc, String subject) throws NamingException, AddressException,
            MessagingException {
        Context ctx = new InitialContext();
        String mailSessionJndiName = "java:/Mail";

        session = (Session) ctx.lookup(mailSessionJndiName);
        
        if(session == null){
        	log.error("Mail session was not found; mailSessionJndiName: "+mailSessionJndiName);
        }
//        session.getProperties().setProperty("mail.smtp.ssl.trust", "*");
//        session.getProperties().put("mail.smtp.ssl.trust", "*");
//        
//        System.err.println(session.getProperties());
//        System.err.println(session.getProperties().propertyNames());
        
        message = new MimeMessage(session);
        message.setHeader("Content-Type", "text/html; charset=UTF-8");
        if (from!=null && !from.isEmpty()) {
            message.addFrom(InternetAddress.parse(from));
        }
        if (to!=null && !to.isEmpty()) {
            message.addRecipients(RecipientType.TO, to);
        }
        
        if (cc!=null && !cc.isEmpty()) {
            message.addRecipients(RecipientType.CC, cc);
        }
    	if (bcc!=null && !bcc.isEmpty()) {
            message.addRecipients(RecipientType.BCC, bcc);
        }
        message.setSubject(subject);
    }

    public void setTextBody(String textBody) throws MessagingException {
        textBodyPart = createPart(null, "plain", textBody);
    }

    public void setHtmlBody(String htmlBody) throws MessagingException {
        htmlBodyPart = createPart(null, "html", htmlBody);
    }

    public void addAlternateBodyPart(String body, String contentType) throws MessagingException {
        alternateBodyParts.add(createPart(null, contentType, body));
    }

    public void addAttachment(String name, String contentType, byte[] bytes) throws MessagingException {
        MimeBodyPart attachmentBodyPart = new MimeBodyPart();
        DataSource source = new javax.mail.util.ByteArrayDataSource(bytes,contentType);
        attachmentBodyPart.setDataHandler(new DataHandler(source));
        attachmentBodyPart.setFileName(name);
        attachmentBodyParts.add(attachmentBodyPart);
    }
    
//    public void addAttachment(String name, String contentType, InputStream inputStream) throws MessagingException {
//        attachmentBodyParts.add(createPart(name, contentType, inputStream));
//    }

    public void send() throws MessagingException {
        MimeMultipart multipart = new MimeMultipart();
        message.setContent(multipart);
        MimeMultipart body = null;
        if (textBodyPart != null && htmlBodyPart != null || !alternateBodyParts.isEmpty()) {
            MimeBodyPart altBodyPart = new MimeBodyPart();
            multipart.addBodyPart(altBodyPart);
            body = new MimeMultipart("alternative");
            altBodyPart.setContent(body);
        } else {
            body = multipart;
        }
        if (textBodyPart != null) {
            body.addBodyPart(textBodyPart);
        }
        if (htmlBodyPart != null) {
            body.addBodyPart(htmlBodyPart);
        }
        for (BodyPart alternateBodyPart : alternateBodyParts) {
            body.addBodyPart(alternateBodyPart);
        }
        for (BodyPart attachment : attachmentBodyParts) {
            multipart.addBodyPart(attachment);
        }
        Transport.send(message);
    }

    private BodyPart createPart(String name, String contentType, String string) throws MessagingException {
    	MimeBodyPart part = new MimeBodyPart();
    	
    	part.setText(string, "utf-8", contentType);
    	
//        part.setDataHandler(new DataHandler(new StringDataSource(name, contentType, string)));
        return part;
    }


//    private class StringDataSource implements DataSource {
//        private String name;
//        private String contentType;
//        private String string;
//
//        public StringDataSource(String name, String contentType, String string) {
//            this.name = name;
//            this.contentType = contentType;
//            this.string = string;
//        }
//
//        @Override
//        public InputStream getInputStream() throws IOException {
//            return new ByteArrayInputStream(string.getBytes());
//        }
//
//        @Override
//        public OutputStream getOutputStream() throws IOException {
//            return null;
//        }
//
//        @Override
//        public String getContentType() {
//            return contentType;
//        }
//
//        @Override
//        public String getName() {
//            return name;
//        }
//    }
}
