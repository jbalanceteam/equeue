package org.itx.jbalance.l0.h;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.validator.NotNull;

/**
 * Документ, для выведения из очереди
 * @author apv
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="HRegister.All",query="from HRegister o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="HRegister.MaxNumber",query="select max(o.dnumber) from HRegister o where CloseDate is null and Version is null ")/*and ContractorOwner = :ContractorOwner*/
})
public class HRegister extends Hdocument<HRegister> {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Дата создания протокола
	 */
	private Date registerDate;
	
	private String member1;
	private String member2;
	private String member3;
	/**
	 * #115 Протоколы в 3 этапа
	 * TODO make this not nullable
	 */
	private RegisterStatus status;
	
	/**
	 * #256 Выводить дату для получения путевки в сервисе номера очереди
	 */
	private Date receivePermitDueDate;
	
	/**
	 * #282 #261 Дата произвольного пересчета возраста детей. если null то возраст считается на текущий день, 
	 * либо на день перевода протокола на третий этап
	 */
	private Date agereprocessdate;
	
	public String getMember1() {
		return member1;
	}
	public void setMember1(String member1) {
		this.member1 = member1;
	}
	public String getMember2() {
		return member2;
	}
	public void setMember2(String member2) {
		this.member2 = member2;
	}
	public String getMember3() {
		return member3;
	}
	public void setMember3(String member3) {
		this.member3 = member3;
	}
	
	@Enumerated(EnumType.STRING)
	@Column(nullable=false)
	@NotNull
	public RegisterStatus getStatus() {
		return status;
	}
	public void setStatus(RegisterStatus status) {
		this.status = status;
	}
	@Column(nullable=false)
	@NotNull
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	
	public Date getReceivePermitDueDate() {
		return receivePermitDueDate;
	}
	
	public void setReceivePermitDueDate(Date receivePermitDueDate) {
		this.receivePermitDueDate = receivePermitDueDate;
	}
	public Date getAgereprocessdate() {
		return agereprocessdate;
	}
	public void setAgereprocessdate(Date agereprocessdate) {
		this.agereprocessdate = agereprocessdate;
	}
	
	

}
