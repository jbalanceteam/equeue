package org.itx.jbalance.l0.h;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.validator.NotNull;
import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.h.Hcontractor;

@Entity
@NamedQueries({
		@NamedQuery(name = "HDouRequest.All", query = "from HDouRequest o where o.version is null and o.closeDate is null"),
		@NamedQuery(name = "HDouRequest.MaxNumber", query = "select max(o.dnumber) from HDouRequest o where Version is null"),/* and ContractorOwner = :ContractorOwner*/
		@NamedQuery(name = "HDouRequest.Specification", query = "from SDouRequest o where o.version is null and o.closeDate is null and o.HUId = :Header order by o.seqNumber")
		})
public class HDouRequest extends Hcontractor {

	public enum Status {
		WAIT("Ожидание"),
		PERMIT("Выписана путевка"),
		DELETE_BY_7_YEAR("Убрали по достижению 7-и лет"), 
		DELETE_BY_PARRENTS_AGREE("Убрали по заявлению родителей");

		private final String description;
		private Status(String s){
			description=s;
		} 
		
		public String getDescription() {
			return description;
		} 
	}

	private static final long serialVersionUID = 1L;
	/**
	 * Данные о ребеночке
	 */
	private Physical child;
	/**
	 * Свидетельство о рождении
	 */
	private BirthSertificate birthSertificate;

	/**
	 * Дата постановки на учет.
	 */
	private Date regDate;

	private String address;

	/**
	 * Признак того, что ребенку выдана  путевка в Группу короткого пребывания
	 */
	private boolean gkp = false;
	/**
	 * Статус РН
	 */
	private Status status;

	/**
	 * Планируемый год поступления ребенка в ДОУ.
	 */
	private Integer year;

	/**
	 * Льгота
	 */
	private Privilege privilege;

	/**
	 * ПРимечание
	 */
	private String comments;

	/**
	 * Разработка #905 ОТдельное поле для Старого РН.
	 */
	private String oldNumber;

	/**
	 * Дата сверки
	 */
	private Date sverkDate;

	private RecordColor recordColor;
	/**
	 * #180: Сделать группу полей "Данные заявителя"
	 * Это поле приходит из МФЦ
	 * @deprecated use 
	 */
	@Deprecated
	private String declarantStr;
	/**
	 * Заявитель
	 */
	private Physical declarant;

	@ManyToOne
	public Physical getChild() {
		return child;
	}

	public void setChild(Physical child) {
		this.child = child;
	}

	@ManyToOne
	public BirthSertificate getBirthSertificate() {
		return birthSertificate;
	}

	public void setBirthSertificate(BirthSertificate birthSertificate) {
		this.birthSertificate = birthSertificate;
	}

	@Enumerated(EnumType.STRING)
	@NotNull
	@Column(nullable=false)
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status s) {
		this.status = s;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	@ManyToOne
	public Privilege getPrivilege() {
		return privilege;
	}

	public void setPrivilege(Privilege privilege) {
		this.privilege = privilege;
	}

	@Lob
	@Column(length = 1024, columnDefinition = "TEXT")
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getOldNumber() {
		return oldNumber;
	}

	public void setOldNumber(String oldNumber) {
		this.oldNumber = oldNumber;
	}


	public Date getSverkDate() {
		return sverkDate;
	}

	public void setSverkDate(Date sverkDate) {
		this.sverkDate = sverkDate;
	}

	@Enumerated(EnumType.ORDINAL)
	public RecordColor getRecordColor() {
		return recordColor;
	}

	public void setRecordColor(RecordColor recordColor) {
		this.recordColor = recordColor;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(nullable=false)
	@NotNull
	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	
	@Column(name="declarant")
	@Deprecated
	public String getDeclarantStr() {
		return declarantStr;
	}

	@Deprecated
	public void setDeclarantStr(String declarant) {
		this.declarantStr = declarant;
	}
	

	public boolean isGkp() {
		return gkp;
	}

	public void setGkp(boolean gkp) {
		this.gkp = gkp;
	}

	@ManyToOne
	public Physical getDeclarant() {
		return declarant;
	}

	public void setDeclarant(Physical declarant) {
		this.declarant = declarant;
	}

	@Override
	public String toString() {
		if (getRegDate() != null) {
			return getDnumber() + "-" + (getRegDate().getYear() + 1900);
		} else {
			return getDnumber() + "";
		}
	}

}
