package org.itx.jbalance.l0.h;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.itx.jbalance.l0.o.Dou;



@Entity 
@NamedQueries({
	@NamedQuery(name="HFreeDivision.All",query="from HFreeDivision o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="HFreeDivision.MaxNumber",query="select max(o.dnumber) from HFreeDivision o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})
public class HFreeDivision extends HStaff<HFreeDivision> {

	private static final long serialVersionUID = 1L;
	private Dou  dou;

	@ManyToOne
	public Dou getDou() {
		return dou;
	}

	public void setDou(Dou dou) {
		this.dou = dou;
	}

	
	
	
	
	
}
