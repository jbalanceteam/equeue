package org.itx.jbalance.l0.h;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.validator.NotNull;
import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.s.SAutoRegister;

/**
 * #118 Автоматическое комплектование
 * Документ используется для сохранения данных введенных на форме "АВТОПРОТОКОЛ"
 * Автопротокол описывает солько место доступно в ДОУ для каждой возростной категории
 * 
 * @author apv
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="HAutoRegister.All",query="from HAutoRegister o where o.version is null and o.closeDate is null")
//	@NamedQuery(name="HAutoRegister.MaxNumber",query="select max(o.dnumber) from HAutoRegister o where CloseDate is null and Version is null ")/*and ContractorOwner = :ContractorOwner*/
})
public class HAutoRegister extends Ubiq<HAutoRegister> {

	private static final long serialVersionUID = 1L;
	/**
	 * Процент льготной категории по умалчанию
	 */
	public static Integer DAFAULT_PRIVELEGE_PART = 40;
	
	/**
	 * Этот тот самый протокол, который в результате получился
	 */
	private HRegister hRegister;
	
	/**
	 * Перечисления возможных методов распределения мест
	 */
	public enum Method{
		/**
		 * Максимально удовлетворить пожелания очередников
		 */
		WISHES,
		/**
		 * Максимально заполнить места в ДОУ
		 */
		DOU_FILL
	}
	/**
	 * Критерий 
	 */
	private Method method;
	/**
	 * Это дата на которую возраста детей из очереди должны соответствовать возрастным группам
	 * 
	 */
	private Date calculationDate;
	
	
	/**
	 * Процент льготной категории
	 * При формировании протокола, льготникам отводится только часть мест. 
	 * После исчерпания этого лимита, места распределяются без учета льготы.
	 */
	private Integer privelegePart;
	/**
	 * Если true - включает в протокол отказы 
	 */
	private boolean includeRefuses = true;
	
	private Set<SAutoRegister>sAutoRegisters = new HashSet<SAutoRegister>();
	
	
	public SAutoRegister addSAutoRegister(Dou dou, Integer douGroup,
			Integer seatsRquested) {
		if(dou == null ){
			throw new RuntimeException();
		}
		if(douGroup == null ){
			throw new RuntimeException();
		}
		SAutoRegister sAutoRegister = new SAutoRegister(dou, douGroup, this, seatsRquested);
		sAutoRegisters.add(sAutoRegister);
		return sAutoRegister;
	}
	
	/**
	 * который в результате получился
	 * @return
	 */
	@OneToOne(optional=true, cascade=CascadeType.ALL)
	public HRegister getHRegister() {
		return hRegister;
	}
	public void setHRegister(HRegister hRegister) {
		this.hRegister = hRegister;
	}
	
	@OneToMany(mappedBy="HAutoRegister", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	public Set<SAutoRegister> getSAutoRegisters() {
		return sAutoRegisters;
	}
	
	
	
	@Enumerated(EnumType.STRING)
	@Column(nullable=false)
	@NotNull
	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	@Transient
	public SAutoRegister getSAutoRegister(Dou dou, Integer douGroup) {
		for (SAutoRegister s : sAutoRegisters) {
			if(s.getVersion() != null || s.getCloseDate() != null){
				continue;
			}
			if(s.getDou().equals(dou) && douGroup!=null && s.getDouGroup().equals(douGroup)){
				return s;
			}
		}
		return null;
	}
	
	/**
	 * Получение списка по возрастной группе
	 * @param douGroup
	 * @return
	 */
	@Transient
	public List<SAutoRegister> getSAutoRegisters(Integer douGroup) {
		List<SAutoRegister>res = new ArrayList<SAutoRegister>();
		for (SAutoRegister s : sAutoRegisters) {
			if(s.getVersion() != null || s.getCloseDate() != null){
				continue;
			}
			if(douGroup!=null && s.getDouGroup().equals(douGroup)){
				res.add(s);
			}
		}
		return res;
	}
	
	public void setSAutoRegisters(Set<SAutoRegister> sAutoRegisters) {
		this.sAutoRegisters = sAutoRegisters;
	}

	@Column(nullable = false)
	public Integer getPrivelegePart() {
		return privelegePart;
	}

	public void setPrivelegePart(Integer privelegePart) {
		this.privelegePart = privelegePart;
	}

	/**
	 * 
	 * @return
	 */
	@Column(nullable=false)
	public Date getCalculationDate() {
		return calculationDate;
	}

	public void setCalculationDate(Date calculationDate) {
		this.calculationDate = calculationDate;
	}

	public boolean isIncludeRefuses() {
		return includeRefuses;
	}

	public void setIncludeRefuses(boolean includeRefuses) {
		this.includeRefuses = includeRefuses;
	}
}
