package org.itx.jbalance.l0.h;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.itx.jbalance.l0.o.Dou;



@Entity
@NamedQueries({
	@NamedQuery(name="HJuridicalType.All",query="from HJuridicalType o where o.version is null and o.closeDate is null")

})
public class HJuridicalType extends HStaff<HJuridicalType> {

	private static final long serialVersionUID = 1L;
	
	private Dou  dou;

	public Dou getDou() {
		return dou;
	}

	public void setDou(Dou dou) {
		this.dou = dou;
	}

	
	
	
	
	
}
