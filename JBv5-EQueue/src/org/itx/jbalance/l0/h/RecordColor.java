package org.itx.jbalance.l0.h;



/**
 * Цвет, которым выделена запись.
 * Пиздец какой-то :(
 * #939
 * @author apv
 *
 */
public enum RecordColor {
	WHILE("Белый"),
	GRAY("Серый"),
	BLUE("Синий"),
	YELLOW("Желтый"),
	ORANGE("Оранжевый"),
	RED("Красный"),
	GREEN("Зеленый");
  final String name;
  
  
  public String getColorName(){
	  return name; 
  }
  
  RecordColor(String s){
	  name=s;
  }
}
