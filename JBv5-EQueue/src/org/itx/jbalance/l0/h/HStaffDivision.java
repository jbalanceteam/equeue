package org.itx.jbalance.l0.h;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Transient;

import org.itx.jbalance.l0.o.Dou;



@Entity 
@NamedQueries({
	@NamedQuery(name="HStaffDivision.All",query="from HStaffDivision o where o.version is null and o.closeDate is null"),
	@NamedQuery(name="HStaffDivision.MaxNumber",query="select max(o.dnumber) from HStaffDivision o where CloseDate is null and Version is null and ContractorOwner = :ContractorOwner")
})
public class HStaffDivision extends HStaff<HStaffDivision> {

	private static final long serialVersionUID = -7898085139290265007L;

	private Dou  dou;
	
	@Transient
	private Integer km;

	@Transient
	private Integer kg;

	@ManyToOne
	public Dou getDou() {
		return dou;
	}

	public void setDou(Dou dou) {
		this.dou = dou;
	}

	@Transient
	public void setKm(Integer km) {
		this.km = km;
	}
	
	@Transient
	public Integer getKm() {
		return km;
	}
	@Transient
	public void setKg(Integer kg) {
		this.kg = kg;
	}

	@Transient
	public Integer getKg() {
		return kg;
	}

	
	
	
	
	
}
