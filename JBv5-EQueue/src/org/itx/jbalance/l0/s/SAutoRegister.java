package org.itx.jbalance.l0.s;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l0.o.Dou;
/**
 * Данные о свободных местах в ДОУ для заданной возрастной группы
 * @author apv
 *
 */
@Entity
public class SAutoRegister extends Ubiq<SAutoRegister>{

	private static final long serialVersionUID = 1L;
	/**
	 * МДОУ
	 */
	private Dou dou;
	/**
	 * ID возрастной группы. см {@code DOUGroups}
	 */
	private Integer douGroup;
	/**
	 * Шапка документа.
	 */
	private HAutoRegister hAutoRegister;
	/**
	 * Сколько мест доступны в МДОУ для данной возрастной группы
	 */
	private Integer seatsRquested;
	/**
	 * Сколько мест использовано
	 */
	private Integer seatsUsed = 0;
	
	/**
	 * Сколько мест использовано для льготной категории
	 */
	private Integer seatsPrivilegeUsed = 0;
	
	
	
	
	public SAutoRegister(Dou dou, Integer douGroup,
			HAutoRegister hAutoRegister, Integer seatsRquested) {
		super();
		if(hAutoRegister == null ){
			throw new RuntimeException();
		}
		
		if(dou == null ){
			throw new RuntimeException();
		}
		
		
		if(douGroup == null ){
			throw new RuntimeException();
		}
		this.dou = dou;
		this.douGroup = douGroup;
		this.hAutoRegister = hAutoRegister;
		this.seatsRquested = seatsRquested;
	}

	public SAutoRegister() {
		super();
	}

	@ManyToOne(optional=false)
	public Dou getDou() {
		return dou;
	}
	
	public void setDou(Dou dou) {
		this.dou = dou;
	}
	
	public Integer getDouGroup() {
		return douGroup;
	}
	
	public void setDouGroup(Integer douGroup) {
		this.douGroup = douGroup;
	}
	
	@ManyToOne(cascade=CascadeType.ALL)
	public HAutoRegister getHAutoRegister() {
		return hAutoRegister;
	}
	public void setHAutoRegister(HAutoRegister hAutoRegister) {
		this.hAutoRegister = hAutoRegister;
	}
	public Integer getSeatsRquested() {
		return seatsRquested;
	}
	public void setSeatsRquested(Integer seatsRquested) {
		this.seatsRquested = seatsRquested;
	}
	public Integer getSeatsUsed() {
		return seatsUsed;
	}
	public void setSeatsUsed(Integer seatsUsed) {
		this.seatsUsed = seatsUsed;
	}

	public Integer getSeatsPrivilegeUsed() {
		return seatsPrivilegeUsed;
	}

	public void setSeatsPrivilegeUsed(Integer seatsPrivilegeUsed) {
		this.seatsPrivilegeUsed = seatsPrivilegeUsed;
	}
	
	
	
}
