package org.itx.jbalance.l0.s;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.itx.jbalance.l0.h.HFreeDivision;
import org.itx.jbalance.l0.o.StaffDivision;



@Entity
@NamedQueries( {
	@NamedQuery(name="SFreeDivision.MaxSeqNumber",query="select max(o.seqNumber) from SFreeDivision o where CloseDate is null and Version is null and HUId = :Header")
	}	
)
public class SFreeDivision extends Sdocument<SFreeDivision,HFreeDivision> {
	
	private static final long serialVersionUID = 1358336214673205748L;

	private StaffDivision division;
	
	private int freeNum;


	public int getFreeNum() {
		return freeNum;
	}

	public void setFreeNum(int freeNum) {
		this.freeNum = freeNum;
	}

	@ManyToOne
	public StaffDivision getDivision() {
		return division;
	}

	public void setDivision(StaffDivision division) {
		this.division = division;
	}
	
	
	
}
