package org.itx.jbalance.l0.s;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.itx.jbalance.l0.h.HJuridicalType;
import org.itx.jbalance.l0.o.JuridicalType;



@Entity
@NamedQueries( {
	@NamedQuery(name="SJuridicalType.MaxSeqNumber",query="select max(o.seqNumber) from SJuridicalType o where CloseDate is null and Version is null and HUId = :Header")
	}	
)
public class SJuridicalType extends SStaff<SJuridicalType,HJuridicalType> {

	private static final long serialVersionUID = 1L;
	private JuridicalType juridicalType;

	public JuridicalType getJuridicalType() {
		return juridicalType;
	}

	public void setJuridicalType(JuridicalType juridicalType) {
		this.juridicalType = juridicalType;
	}
	
	
}
