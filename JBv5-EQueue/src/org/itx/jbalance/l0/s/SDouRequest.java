package org.itx.jbalance.l0.s;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.validator.Max;
import org.hibernate.validator.Min;
import org.itx.jbalance.l0.o.Dou;


@Entity
@NamedQueries( {
	@NamedQuery(name="SDouRequest.MaxSeqNumber",query="select max(o.seqNumber) from SDouRequest o where CloseDate is null and Version is null and HUId = :Header")
}	
)
public class SDouRequest extends Scontractor {

	private static final long serialVersionUID = 1L;
	
	public static final int RATING_MAX = 3;
	public static final int RATING_MIDDLE = 2;
	public static final int RATING_MIN = 1;
	
	/**
	 * #276: Ранжирование ДОУ
	 */
	private Integer rating;
	
	@ManyToOne(optional = true, cascade = CascadeType.REFRESH)
	@JoinColumn(name = "Contractor", nullable = true)
//	@AssociationOverride(name="articleUnit",joinColumns={@JoinColumn(name = "Contractor", nullable = true)})
	public Dou getArticleUnit() {
		return (Dou) contractor;
	}


	public void setArticleUnit(Dou contractor) {
		this.contractor = contractor;
	}


	@Max(RATING_MAX)
	@Min(RATING_MIN)
	public Integer getRating() {
		return rating;
	}


	public void setRating(Integer rating) {
		if(rating != null && (rating > RATING_MAX || rating < RATING_MIN)){
			throw new RuntimeException("rating is out of range");
		}
		this.rating = rating;
	}

	
	
}
