package org.itx.jbalance.l0.s;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import org.hibernate.validator.NotNull;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.Division;
import org.itx.jbalance.l0.o.Dou;


/**
 * Движение по протоколу.
 * 
 * 
 * @author apv
 */
@Entity
@NamedQueries( {
	@NamedQuery(name="SRegister.MaxSeqNumber",query="select max(o.seqNumber) from SRegister o where CloseDate is null and Version is null and HUId = :Header")
}	
)
public class SRegister extends Sdocument<SRegister,HRegister> {


	private static final long serialVersionUID = 8797911634293355747L;


	/**
	 * Основание для выдачи путевки.
	 * Одно поле, короткое наименование и "подсказка"
	 */
	public enum Foundation{
		O1("ОГО","общая городская очередь"),
		O2("К. л/п","карточка личного приема"),
		O3("К . мэра","карточка приема мэра"),
		O4("З.","заявление"),
		O5("Х. орг.","ходатайство организации"),
		O6("Х. ОУ","ходатайство образовательного учреждения"),
		O7("Х. УО","ходатайство Управления образования"),
		O8("Х. зав.","ходатайство заведующих"),
		O9("Х. зав. (с)","ходатайство заведующей на сотрудника"),
		O10("Х. зав. (ш)","ходатайство заведующей на шефствующую организацию"),
		O11("П. из МДОУ______ в МДОУ______","перевод из МДОУ в МДОУ");
		
		public static final Foundation DEFAULT = O1;
		private final String shortName;
		private final String description;
		
		
		 public String getShortName() {
			return shortName;
		}
	
	
		public String getDescription() {
			return description;
		}
	
	
		Foundation(String shortName,String description){
			this.shortName=shortName;
			this.description=description;
		}
	}
	
	
	
	public enum Action {
		GIVE_PERMIT("Выдать путевку", "Путевка"),
		/**
		 * Группа кратковременного пребывания (далее ГКП) - это группа детей, которые находятся в саду 3 часа или 5 часов.
		 * Дети посещают МДОУ но на очень короткое время , обычно до обеда.
		 * Они продолжают стоять в очереди и ожидают места в ДОУ в обычной группе (полного дня) .
		 * 
		 * Сделать по аналогии с "отказом"
		 * Добавить строку ГКП.
		 * 
		 * При выписки путевок в протоколе с параметром ГКП  выбирается также МДОУ, и выписывается путевка.
		 * Ребенок остается в активной очереди.
		 */
		GKP("Группа короткого пребывания", "ГКП"),
		BACK_TO_QUEUE("Вернуть в очередь", "Вернуть"),
		DELETE_BY_7_YEAR("Убрать из очереди по достижению 7-и лет", "Удалить (7 лет)"), 
		DELETE_BY_PARRENTS_AGREE("Убрать из очереди по заявлению родителей", "Удалить (заявление)"),
		REFUSAL("Отказано", "Отказ");
		
		private final String description;
		private final String shortDescription;
		
		private Action(String s, String sd){
			description=s;
			shortDescription = sd;
		}
		public String getDescription() {
			return description;
		} 
		
		public String getShortDescription() {
			return shortDescription;
		} 
	}
	
	/**
	 * Действие с путевкой
	 */
	private Action rnAction;
	
	/**
	 * Номер путевка
	 * Формат:  номер заявки + нпп путевки выданной этому малышу
	 */
	private String number;
	/**
	 * Заявка
	 */
	private HDouRequest douRequest;
	/**
	 * ДОУ в к-е пошел малыш
	 */
	private Dou dou;
	/**
	 * Группа ДОУ
	 */
	private Division division;
	
	private Foundation foundation;
	/**
	 * Это возростная группа, к которой относился очередник на момент включения в протокол.
	 * Заполняется при создании автопротокола.
	 * 
	 * Очередник можнт относиться к нескольким возростным группам.
	 */
	private Integer ageGroupId;
	
	
	@Enumerated(EnumType.ORDINAL)
	public Foundation getFoundation() {
		return foundation;
	}


	public void setFoundation(Foundation foundation) {
		this.foundation = foundation;
	}


	@OneToOne(cascade=CascadeType.REFRESH)
	@JoinColumn(nullable=false)
	@NotNull
	public HDouRequest getDouRequest() {
		return douRequest;
	}
	
	
	public void setDouRequest(HDouRequest douRequest) {
		this.douRequest = douRequest;
	}
	
	@ManyToOne(cascade=CascadeType.REFRESH)
	@JoinColumn(nullable=true)
//	@NotNull
	public Dou getDou() {
		return dou;
	}
	public void setDou(Dou dou) {
		this.dou = dou;
	}
	
	@ManyToOne(cascade=CascadeType.REFRESH)
	@JoinColumn(nullable=true)
	public Division getDivision() {
		return division;
	}
	public void setDivision(Division division) {
		this.division = division;
	}


	public String getNumber() {
		return number;
	}


	public void setNumber(String number) {
		this.number = number;
	}


	@Override
	public String toString() {
		return "SRegister [dou=" + dou + "]";
	}


	@Enumerated(EnumType.STRING)
	@NotNull
	@Column(nullable=false)
	public Action getRnAction() {
		return rnAction;
	}


	public void setRnAction(Action rnAction) {
		this.rnAction = rnAction;
	}


	public Integer getAgeGroupId() {
		return ageGroupId;
	}


	public void setAgeGroupId(Integer ageGroupId) {
		this.ageGroupId = ageGroupId;
	}
	

	
	
}
