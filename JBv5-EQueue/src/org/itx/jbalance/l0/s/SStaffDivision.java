package org.itx.jbalance.l0.s;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.itx.jbalance.l0.h.HStaffDivision;
import org.itx.jbalance.l0.o.StaffDivision;
import org.itx.jbalance.l0.s.Sdocument;



@Entity
@NamedQueries( {
	@NamedQuery(name="SStaffDivision.MaxSeqNumber",query="select max(o.seqNumber) from SStaffDivision o where CloseDate is null and Version is null and HUId = :Header")
	}	
)
public class SStaffDivision extends Sdocument<SStaffDivision,HStaffDivision> {
	
	private static final long serialVersionUID = 1L;
	private StaffDivision division;


	@ManyToOne
	public StaffDivision getDivision() {
		return division;
	}

	public void setDivision(StaffDivision division) {
		this.division = division;
	}
	
	
	
}
