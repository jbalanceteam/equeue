package org.itx.jbalance.l0.o;

import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.validator.NotNull;

/**
 * ДОУ - Дошкольное образовательное учреждение
 * Попросту говоря - детский сад
 * @author apv
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name=Dou.QUERY_ALL,query="from Dou o where o.version is null and o.closeDate is null"),
	@NamedQuery(name=Dou.QUERY_BY_NUMBER, query="from Dou o where o.version is null and o.closeDate is null and o.number = :" + Dou.QUERY_PARAM_NUMBER)
})
public class Dou extends Juridical {
    
	public static final String QUERY_ALL = "Dou.All";
	public static final String QUERY_BY_NUMBER = "Dou.ByNumber";
	
	public static final String QUERY_PARAM_NUMBER = "number";
	
	private static final long serialVersionUID = -1722455974258219388L;
	
	/**
	 * Номер Доу
	 * Обычно это числовое значение: 1,2,3,... 
	 * Но могут быть исключения: дроби, прочая хня
	 */
	private String number;
	
	/**
	 *  тип Доу
	 */
	private String douType;
	
	private Region region;

	
	//private Integer regionId;
	
	@Column(nullable = false, length = 20)
	@NotNull
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}
	
	public static Integer toNumber(String s){
		int ind = 0 ;
		for (char c : s.toCharArray()) {
			if(Character.isDigit(c)){
				ind ++;
			} else {
				break;
			}
		}
		if(ind == 0){
			return null;
		}
		return Integer.parseInt(s.substring(0, ind));
	}
	
	public static Comparator<String>douNumbersComparator = new Comparator<String>(){
		@Override
		public int compare(String o1, String o2) {
			if(o1 == null || o2 == null){
				throw new IllegalArgumentException();
			}
			
			final Integer number1 = toNumber(o1);
			final Integer number2 = toNumber(o2);
			if(number1 != null && number2 != null){
				final int compareTo = number1.compareTo(number2);
				
				if(compareTo != 0){
					return compareTo;
				}
			}
			return o1.compareTo(o2);
		}
	};
	
    /*
     * Тип Доу
     * Поле Name из справочника Типов Доу
     */
    
	public String getDouType() {
		return douType;
	}

	public void setDouType(String douType) {
		this.douType = douType;
	}
	
//	public void setDouTypeId(int douTypeId) {
//		this.douTypeId = douTypeId;
//	}
//	
//	public int getDouTypeId() {
//		return douTypeId;
//	}
	
	
	@ManyToOne
	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	@Override
	public String toString() {
		return "Dou [getJAddress()=" + getJAddress() + ", getOKONH()="
				+ getOKONH() + ", getOKPO()=" + getOKPO() + ", getEMail()="
				+ getEMail() + ", getContactInfo()=" + getContactInfo()
				+ ", getINN()=" + getINN() + ", getPAddress()=" + getPAddress()
				+ ", getName()=" + getName() + ", getDescription()="
				+ getDescription() + "]";
	}

	@Override
	public int compareTo(Contractor o) {
		return douNumbersComparator.compare(this.getNumber(), ((Dou)o).getNumber());
	}

//	public void setRegion(String region) {
//		this.region = region;
//	}
//
//	public String getRegion() {
//		return region;
//	}
//
//	public void setRegionId(Integer regionId) {
//		this.regionId = regionId;
//	}
//
//	public Integer getRegionId() {
//		return regionId;
//	}
	
}
