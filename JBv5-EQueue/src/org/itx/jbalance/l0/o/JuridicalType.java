package org.itx.jbalance.l0.o;


/**
 * Тип юрлица
 * Тип ДОУ
 * ВНИМАНИЕ!!
 * Значения дестко забиты в коде!! 
 * @author apv
 *
 */
//@Entity
//@PrimaryKeyJoinColumn(name = "UId")
//@NamedQueries({
//})
public class JuridicalType extends Staff<JuridicalType>  {
	
	private static final long serialVersionUID = 1L;
	public JuridicalType() {
		super();
		
	}
	public JuridicalType(Long id,String name) {
		super();
		setUId(id);
		setName(name);
	}

}
