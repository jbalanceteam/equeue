package org.itx.jbalance.l0.o;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.itx.jbalance.l0.Ubiq;

/**
 * Письмо. 
 * Используется для отслеживания истории рассылки эл. писем системой.
 * @author apv
 *
 */
@Entity
public class EmailMessage extends Ubiq<EmailMessage> {
    
	private static final long serialVersionUID = -1722455974258219388L;

	private static final int MAX_LENGTH_FROM = 20;
	private static final int MAX_LENGTH_TO = 50;
	private static final int MAX_LENGTH_SUBJECT = 250;
	
	public enum ContentType{
		HTML, TEXT;
	}
	
	
	public enum Status{
		READY, SENT, ERROR;
	}
	
	
	
	
	public EmailMessage() {
		super();
	}
	public EmailMessage(Contractor contractor, String from, String to, String cc,
			String bcc, String subject, String content,
			ContentType contentType) {
		super();
		this.contractor = contractor;
		this.sender = from;
		this.recipientTo = to;
		this.recipientCc = cc;
		this.recipientBcc = bcc;
		this.subject = subject;
		this.content = content;
		this.contentType = contentType;
		this.status = Status.READY;
	}
	/**
	 * Контрагент, которому направлялось письмо
	 */
	
	private Contractor contractor;
	
	/**
	 *  From
	 */
	private String sender;
	private String recipientTo;
	private String recipientCc;
	private String recipientBcc;
	private String subject;
	
	private String content;
	private ContentType contentType;
	
	private Status status;
	
	@ManyToOne
	public Contractor getContractor() {
		return contractor;
	}
	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}
	
	@Column(length=MAX_LENGTH_FROM)
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	@Column(length=MAX_LENGTH_TO)
	public String getRecipientTo() {
		return recipientTo;
	}
	public void setRecipientTo(String to) {
		this.recipientTo = to;
	}
	@Column(length=MAX_LENGTH_TO)
	public String getRecipientCc() {
		return recipientCc;
	}
	public void setRecipientCc(String cc) {
		this.recipientCc = cc;
	}
	@Column(length=MAX_LENGTH_TO)
	public String getRecipientBcc() {
		return recipientBcc;
	}
	public void setRecipientBcc(String bcc) {
		this.recipientBcc = bcc;
	}
	@Column(length=MAX_LENGTH_SUBJECT)
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	
	@Lob
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	@Enumerated(EnumType.STRING)
	public ContentType getContentType() {
		return contentType;
	}
	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}
	@Enumerated(EnumType.STRING)
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	
	
	
	
}
