package org.itx.jbalance.l0.o;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.validator.NotNull;
import org.itx.jbalance.l0.Ubiq;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.s.SRegister;

/**
 * Путевка в МДОУ
 * Путевки выдаюдся при переводе протокола {@code HRegister} на 3-й этип
 * 
 * Почти все поля путевки не изменяемы
 * 
 * Избыточное хранение информации налицо! Данные Permit можно получить из SRegister, кроме поля статуса
 * 
 * @author apv
 *
 */
@Entity
public class Permit extends Ubiq<Permit> {
    
	private static final long serialVersionUID = -1722455974258219388L;
	
	
	
	public enum PermitStatus{
		/**
		 * Выдана
		 */
		ISSUED("Выдана"),
		/**
		 * Не реализована (архив)
		 */
		NOT_IMPLEMENTED("Не реализована (архив)"),
		/**
		 * Реальзована
		 * С путевкой обратились в МДОУ
		 */
		IMPLEMENTED("Договор");
		private final String description;
		private PermitStatus(String description) {
			this.description = description;
		}
		public String getDescription() {
			return description;
		}
	}
	
	/**
	 * Ребенок, которому выдана путевка
	 * Эта информация избыточна, т.к. ребенка можно получить из РН
	 */
	private Physical child;
	/**
	 * Это привелегия, которая стояла для РН на момент выдачи путевки
	 */
	private Privilege privilege;
	/**
	 * Регистрационный номер
	 */
	private HDouRequest hDouRequest;
	
	private String number;
	
	private Date permitDate;
	/**
	 * У путевок выданных до появления Permit entity, поле может быть null
	 */
	private PermitStatus permitStatus;
	/**
	 * Движение в протоколе в результате которого выдана путевка
	 */
	private SRegister sRegister;
	/**
	 * МДОУ в который выдана путевка
	 */
	private Dou dou;
	
	/**
	 * Возраст в месяцах в котором выдана путевка
	 * Это тоже избыточная информация, т.к. можно вычислить из даты рождения и даты путевки, но хранение облегчает поиск и вычисления
	 */
	private Integer age;
	
	
	
	public Permit() {
		super();
	}
	
	

	public Permit(Physical child, Privilege privilege, HDouRequest hDouRequest,
			String number, 
			Date permitDate, PermitStatus permitStatus,
			SRegister sRegister, Dou dou) {
		super();
		if(dou == null){
			throw new RuntimeException("dou can't be null");
		}
		
		if(child == null){
			throw new RuntimeException("child can't be null");
		}
		
		if(hDouRequest == null){
			throw new RuntimeException("hDouRequest can't be null");
		}
		
		if(sRegister == null){
			throw new RuntimeException("sRegister can't be null");
		}
		this.child = child;
		this.privilege = privilege;
		this.hDouRequest = hDouRequest;
		this.number = number;
		this.permitDate = permitDate;
		this.permitStatus = permitStatus;
		this.sRegister = sRegister;
		this.dou = dou;
	}



	@JoinColumn(updatable=false)
	@ManyToOne
	public Physical getChild() {
		return child;
	}

//	@JoinColumn(updatable=false)
	@ManyToOne
	public Privilege getPrivilege() {
		return privilege;
	}


	@JoinColumn(updatable=false)
	@ManyToOne
	public HDouRequest getHDouRequest() {
		return hDouRequest;
	}

	
	
	

	public void setHDouRequest(HDouRequest hDouRequest) {
		this.hDouRequest = hDouRequest;
	}




	public void setSRegister(SRegister sRegister) {
		this.sRegister = sRegister;
	}



	public void setChild(Physical child) {
		this.child = child;
	}



	public void setPrivilege(Privilege privilege) {
		this.privilege = privilege;
	}



	public void setNumber(String number) {
		this.number = number;
	}



	public void setPermitDate(Date permitDate) {
		this.permitDate = permitDate;
	}



	public void setDou(Dou dou) {
		this.dou = dou;
	}



	public String getNumber() {
		return number;
	}


	public Date getPermitDate() {
		return permitDate;
	}

	@Enumerated(EnumType.STRING)
	public PermitStatus getPermitStatus() {
		return permitStatus;
	}

	public void setPermitStatus(PermitStatus permitStatus) {
		this.permitStatus = permitStatus;
	}


	@JoinColumn(updatable=false)
	@ManyToOne
	public SRegister getSRegister() {
		return sRegister;
	}


	@JoinColumn(updatable=false)
	@ManyToOne
	public Dou getDou() {
		return dou;
	}

	

	@NotNull
	public Integer getAge() {
		return age;
	}



	public void setAge(Integer age) {
		this.age = age;
	}



	@Override
	public String toString() {
		return "Permit [child=" + child + ", privilege=" + privilege
				+ ", hDouRequest=" + hDouRequest + ", number=" + number
				+ ", permitDate=" + permitDate + ", permitStatus="
				+ permitStatus + ", sRegister=" + sRegister + ", dou=" + dou
				+ "]";
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((age == null) ? 0 : age.hashCode());
		return result;
	}

}
