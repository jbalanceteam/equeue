package org.itx.jbalance.l0.o;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


/**
 * Свидетельство о рождении
 * @author apv
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="BirthSertificate.All",query="from BirthSertificate o where o.version is null and o.closeDate is null")

})
public class BirthSertificate extends Staff<BirthSertificate> {
	
	private static final long serialVersionUID = 1L;
	
	public enum Sex{
		/**
		 * Мужик
		 */
		M,
		/**
		 * Баба
		 */
		W;
	}
	
	private String seria;
	private String number;
	private Date date;
	/**
	 * Опекун 1 - Мать
	 */
	private String guardian1;
	/**
	 * Опекун 2 - Отец
	 */
	private String guardian2;
	
	
	private Sex sex;
	public String getSeria() {
		return seria;
	}
	public void setSeria(String seria) {
		this.seria = seria;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getGuardian1() {
		return guardian1;
	}
	public void setGuardian1(String guardian1) {
		this.guardian1 = guardian1;
	}
	public String getGuardian2() {
		return guardian2;
	}
	public void setGuardian2(String guardian2) {
		this.guardian2 = guardian2;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Column(nullable=false)
	@Enumerated(EnumType.STRING)
	public Sex getSex() {
		return sex;
	}
	public void setSex(Sex sex) {
		this.sex = sex;
	}
	
	
	
	
	
}
