package org.itx.jbalance.l0.o;

import java.io.Serializable;

/**
 * Справочник детских групп в Доу - hardcode
 * Скорее правочник возростов. 
 * В каждой группе ДОУ дети заданной возростной группы.
 * @author apv
 */

public class DOUGroups implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	
	private String name;

	/**
	 * 
	 */
	private Integer ageFrom;
	
	
	/**
	 * 
	 * 
	 */
	private Integer ageTo;
	
	public DOUGroups() {
		super();
	}

	
	public DOUGroups(int id, String name, Integer ageFrom, Integer ageTo) {
		super();
		this.id = id;
		this.name = name;
		this.ageFrom = ageFrom;
		this.ageTo = ageTo;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getAgeFrom() {
		return ageFrom;
	}

	public void setAgeFrom(Integer ageFrom) {
		this.ageFrom = ageFrom;
	}

	public Integer getAgeTo() {
		return ageTo;
	}

	public void setAgeTo(Integer ageTo) {
		this.ageTo = ageTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DOUGroups other = (DOUGroups) obj;
		if (id != other.id)
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "DOUGroups [id=" + id + ", name=" + name + ", ageFrom="
				+ ageFrom + ", ageTo=" + ageTo + "]";
	}
	
	
	
	
	public String getFromToStr(){
		return (ageFrom / 12) + "." + (ageFrom % 12)   + " - " + (ageTo / 12) + "." + (ageTo % 12) ;
	}
	
}
