package org.itx.jbalance.l0.o;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


/**
 * Льгота.
 * @author apv
 *
 */
@Entity
//@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries({
	@NamedQuery(name="Privilege.All",query="from Privilege o where o.version is null and o.closeDate is null")

})
public class Privilege extends Staff<Privilege>  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
}
