package org.itx.jbalance.l0.o;

import javax.persistence.MappedSuperclass;

/**
 * Staff - это ветвь кадрового учета.
 * @author apv
 *
 */
//@Entity
//@NamedQueries({
//	@NamedQuery(name="Staff.All",query="from Staff o where o.version is null and o.closeDate is null")
//
//})
@MappedSuperclass
public abstract class Staff<C extends Staff<?>> extends Relator<C> {

	private static final long serialVersionUID = 1L;

}
