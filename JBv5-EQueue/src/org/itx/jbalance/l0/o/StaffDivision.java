package org.itx.jbalance.l0.o;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Отел
 * Группа ДОУ  
 * @author apv
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name="StaffDivision.All",query="from StaffDivision o where o.version is null and o.closeDate is null")

})
public class StaffDivision extends Staff<StaffDivision>  {

	private static final long serialVersionUID = 1L;

	/*
	 *  Номер группы
	 */
	private int groupId;


	/**
	 *   Номер типа группы 
	 */
	private int douGroupsId;

	/**
	 *   Количество мест в группе 
	 */
	private int km;
	
	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	
	public int getKm() {
		return km;
	}

	public void setKm(int km) {
		this.km = km;
	}

	public int getDouGroupsId() {
		return douGroupsId;
	}

	public void setDouGroupsId(int douGroupsId) {
		this.douGroupsId = douGroupsId;
	}
	
	
}
