package org.itx.jbalance.l0.o;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;


@Entity
//@PrimaryKeyJoinColumn(name = "UId")
@NamedQueries({
	@NamedQuery(name="Region.All",query="from Region o where o.version is null and o.closeDate is null")

})
public class Region extends Relator <Region> {

	
	
	public Region() {
		super();
	}

	public Region(String name, String description) {
		super(name, description);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	
}
