package org.itx.jbalance.l2_api.ws;

import java.rmi.Remote;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.DouRequestPrerequisiteDTO;
import org.itx.jbalance.l2_api.dto.DouRequestResultDTO;
import org.itx.jbalance.l2_api.dto.ResultDTO;

/**
 * Service для постановки детей в очередь в детские сады
 * 
 * @author shan
 */
//@WebService(name = "MfcWSBean")
//@SOAPBinding(style = Style.DOCUMENT)
public interface MfcWS extends Remote {

	public static final String JNDI_NAME="MfcWS/remote";
	
	/**
	 * метод возвращает все данные которые могут понадобиться для составления
	 * запроса о постановке ребенка на учет
	 * 
	 * @return DouRequestPrerequisiteDTO
	 */
	@WebMethod
	public DouRequestPrerequisiteDTO getDouRequestPrerequisite();

	/**
	 * метод проверяет стоит ли ребенок уже в очереди возвращает
	 * ResultDTO.SUCCESS если ребенок в очереди не стоит
	 * 
	 * @param dousRequestDTO
	 * @return ResultDTO
	 */
	@WebMethod
	public ResultDTO checkDousRequest(
			@WebParam(name = "dousRequestDTO") DouRequestDTO dousRequestDTO);

	/**
	 * метод ставит ребенка в очередь
	 * 
	 * @param dousRequestDTO
	 * @return ResultDTO
	 */
	@WebMethod
	public DouRequestResultDTO makeDousRequest(
			@WebParam(name = "dousRequestDTO") DouRequestDTO dousRequestDTO);

	/**
	 * hello world
	 * 
	 * @return ResultDTO
 	 */
	
	@WebMethod
	public ResultDTO helloWorld();
}
