package org.itx.jbalance.l2_api.ws;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;

import org.itx.jbalance.l1.d.HDouRequestsRemote;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l1.o.PhysicalsRemote;
import org.itx.jbalance.l1.o.BirthSertificatesRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.PrivilegesRemote;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.StringFilter;
import org.itx.jbalance.l1.o.RegionsRemote;
import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.DouRequestPrerequisiteDTO;
import org.itx.jbalance.l2_api.dto.DouRequestResultDTO;
import org.itx.jbalance.l2_api.dto.PrivilegeDTO;
import org.itx.jbalance.l2_api.dto.RegionDTO;
import org.itx.jbalance.l2_api.dto.ResultDTO;
import org.itx.jbalance.l2_api.dto.ResultDTO.Status;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.logging.Logger;
import org.itx.jbalance.l2_api.utils.ConvertHelper;

@Stateless(name="MfcWS")
//@WebService(endpointInterface = "org.itx.jbalance.l2_api.ws.MfcWS", serviceName = "MfcService", portName = "MfcPort")
@Remote(MfcWS.class)
@RemoteBinding(jndiBinding = MfcWS.JNDI_NAME)
public class MfcWSBean implements MfcWS {

	private static final long serialVersionUID = -8430635966944491477L;
	private Logger logger = Logger.getLogger(getClass());

	@EJB
	RegionsRemote regionsEjb;
	@EJB
	DousRemote dousEjb;
	@EJB
	HDouRequestsRemote hDouRequestEjb;
	@EJB
	PhysicalsRemote physicalsEjb;
	@EJB
	BirthSertificatesRemote birthSertificatesEjb;
	@EJB
	PrivilegesRemote privilegesEjb;

	@Override
	public DouRequestPrerequisiteDTO getDouRequestPrerequisite() {
		logger.debug("getDouRequestPrerequisite()");
		DouRequestPrerequisiteDTO prerequisiteDTO = new DouRequestPrerequisiteDTO();

		List<Region> regionDbos = (List<Region>) regionsEjb.getAll(Region.class);
		List<RegionDTO> regionDtos = new ArrayList<RegionDTO>();
		ConvertHelper.regionDBOsToDTOs(regionDbos, regionDtos);

		List<Dou> douDbos = (List<Dou>) dousEjb.getAll(Dou.class);
		List<DouDTO> douDtos = new ArrayList<DouDTO>();
		ConvertHelper.douDBOsToDTOs(douDbos, douDtos);

		List<Privilege> privilegeDbos = (List<Privilege>) privilegesEjb.getAll(Privilege.class);
		List<PrivilegeDTO> privilegeDtos = new ArrayList<PrivilegeDTO>();
		ConvertHelper.privilegeDBOsToDTOs(privilegeDbos, privilegeDtos);

		prerequisiteDTO.setRegionsDTOs(regionDtos);
		prerequisiteDTO.setDousDTOs(douDtos);
		prerequisiteDTO.setPrivilegeDtos(privilegeDtos);

		return prerequisiteDTO;
	}

	@Override
	public ResultDTO checkDousRequest(DouRequestDTO dousRequestDTO) {
//		#403 Ошибка при работе в mfc 
//		разрешено сохранять запись, когда дата рождения, обльше даты выдачи, давай сделаем меньше или равно см скрин в почте
		if(dousRequestDTO == null || dousRequestDTO.getBirthDate() == null || dousRequestDTO.getBsDate() == null || 
				dousRequestDTO.getBsDate().before(dousRequestDTO.getBirthDate())){
			ResultDTO validationError = new ResultDTO();
			validationError.setStatus(Status.FAIL);
			return validationError;
		}
		
		DouRequestsSearchParams params = new DouRequestsSearchParams();
		params.firstname = new StringFilter(dousRequestDTO.getFirstName());
		params.lastname = new StringFilter(dousRequestDTO.getLastName());
		params.middlename = new StringFilter(dousRequestDTO.getPatronymic());
		params.birthday = DateFilter.build(dousRequestDTO.getBirthDate(), dousRequestDTO.getBirthDate());
		
		params.sex = dousRequestDTO.getSex() == 'm' ? Sex.M : Sex.W;

		Integer matchFIOandBirthdayandSex = hDouRequestEjb.getSearchCount(params);

		ResultDTO resultDTO = new ResultDTO();
		if (matchFIOandBirthdayandSex > 0) {
			resultDTO.setStatus(ResultDTO.Status.FAIL);
			resultDTO.setMessage("Ошибка! " + (params.lastname == null?null: params.lastname.getFilterValue())  + " " 
					+ (params.firstname == null?null:params.firstname.getFilterValue()) + " "
					+ (params.middlename == null?null:params.middlename.getFilterValue()) 
					+ " уже существует в очереди.");
			return resultDTO;
		} 
		
//		#347 Добавить проверку на уникальность полей свидетельства о рождении в МФЦ
		params = new DouRequestsSearchParams();
		params.bsSeria = new StringFilter(dousRequestDTO.getBsSeria());
		params.bsNumber = new StringFilter(dousRequestDTO.getBsNumber());
		Integer matchBsSeriaAndNumber= hDouRequestEjb.getSearchCount(params);
		if (matchBsSeriaAndNumber > 0) {
			resultDTO.setStatus(ResultDTO.Status.FAIL);
			resultDTO.setMessage("Введенные серия и номер свидетельства о рождении уже существует в очереди.");
			return resultDTO;
		} 
			
			
		resultDTO.setStatus(ResultDTO.Status.SUCCESS);
		resultDTO.setMessage("Проверка на уникальность успешно пройдена.");
		return resultDTO;
		
	}

	@Override
	public DouRequestResultDTO makeDousRequest(DouRequestDTO dousRequestDTO) {
		
		ResultDTO checkDousRequest = checkDousRequest(dousRequestDTO);
		if(checkDousRequest.getStatus() != ResultDTO.Status.SUCCESS){
			throw new RuntimeException("Validation Error");
		}
		// запрос на постановку в очередь
		HDouRequest hDouRequest = new HDouRequest();

		// регистрируем ребенка
		Physical childDBO = new Physical();
		ConvertHelper.convertDouRequestDTOtoPhysicalDBO(dousRequestDTO,
				childDBO);

		childDBO = physicalsEjb.create(childDBO);

		// добавляем ребенка в запрос
		hDouRequest.setChild(childDBO);

		// регистрируем личные данные ребенка
		BirthSertificate birthSertificateDBO = new BirthSertificate();
		ConvertHelper.convertDouRequestDTOtoBirthSertificateDBO(dousRequestDTO,
				birthSertificateDBO);
		birthSertificateDBO.setName("свидетельство о рождении");
		birthSertificateDBO = birthSertificatesEjb.create(birthSertificateDBO);

		// добавляем личные данные ребенка в запрос
		hDouRequest.setBirthSertificate(birthSertificateDBO);

		// добавляем дополнительные данные в запрос
		hDouRequest.setRegDate(new java.util.Date()); //  дата регистрации - текущий момент


		if (dousRequestDTO.getPrivilegeUid()!=null){
			Privilege privelegeDBO = privilegesEjb.getByUId(dousRequestDTO.getPrivilegeUid(), Privilege.class);
			hDouRequest.setPrivilege(privelegeDBO);
		}
		
		hDouRequest.setStatus(HDouRequest.Status.WAIT);
		hDouRequest.setYear(dousRequestDTO.getYearEnter());
		hDouRequest.setComments(dousRequestDTO.getComments());
		
		// заявитель
		Physical declarant = new Physical();
		ConvertHelper.convertDouRequestDTOtoDeclarant(dousRequestDTO, declarant);
		declarant = physicalsEjb.create(declarant);
		
		hDouRequest.setDeclarant(declarant);
		
		
		hDouRequest.setFoundation("Заявка МФЦ");
		String address = "город " + dousRequestDTO.getChildAddress();
		hDouRequest.setAddress(address);

		// регистрируем сам запрос

		hDouRequest = hDouRequestEjb.create(hDouRequest);

		// находим выбранные пользователем деткие сады
		Map<Long, Integer> dousUids = dousRequestDTO.getDouUids();
		List<Dou> douDBOs = new ArrayList<Dou>();
		for (Long douUid : dousUids.keySet())
			douDBOs.add(dousEjb.getByUId(douUid, Dou.class));

		// и добавляем их в спецификацию запроса
		for (Dou dou : douDBOs) {
			SDouRequest line = new SDouRequest();
			line.setArticleUnit(dou);
			line.setRating(dousUids.get(dou.getUId()));
			line = hDouRequestEjb.createSpecificationLine(line, hDouRequest);
		}
		DouRequestResultDTO resDTO = new DouRequestResultDTO();
		resDTO.setStatus(ResultDTO.Status.SUCCESS);
		resDTO.setMessage("Сохранено.\n Регистрационный номер "+hDouRequest.toString()+"\n UId="+hDouRequest.getUId());
		resDTO.setUId(hDouRequest.getUId());
		resDTO.setDnumber(hDouRequest.toString());
		return resDTO;

	}

	@Override
	public ResultDTO helloWorld() {
		ResultDTO dto = new ResultDTO();
		dto.setStatus(ResultDTO.Status.SUCCESS);
		dto.setMessage("Hello mfc world!");
		return dto;
	}
}
