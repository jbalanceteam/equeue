package org.itx.jbalance.l2_api.ws;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.jws.WebService;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.EmailMessage;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l0.o.EmailMessage.ContentType;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.api.DouRequestsSearchParams.OrderStatus;
import org.itx.jbalance.l1.api.RegisterSearchParams;
import org.itx.jbalance.l1.api.SortedColumnInfo;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.LongFilter;
import org.itx.jbalance.l1.api.filters.StringFilter;
import org.itx.jbalance.l1.d.HDouRequestsLocal;
import org.itx.jbalance.l1.d.HRegisterLocal;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.Emails;
import org.itx.jbalance.l1.o.RegionsRemote;
import org.itx.jbalance.l1.utils.DateUtils;
import org.itx.jbalance.l1.utils.EmailHelper;
import org.itx.jbalance.l2_api.dto.RequestInfo;
import org.itx.jbalance.l2_api.dto.RequestOrderInfo;
import org.itx.jbalance.l2_api.dto.RequestToDouInfo;
import org.itx.jbalance.l2_api.dto.submission.SubmissionPrerequisiteInfo;
import org.itx.jbalance.l2_api.utils.ConvertHelper;
import org.jboss.annotation.security.SecurityDomain;
import org.jboss.logging.Logger;
import org.jboss.wsf.spi.annotation.WebContext;

@Stateless(name = "ExternalWS")
@WebService(endpointInterface="org.itx.jbalance.l2_api.ws.ExternalWS",serviceName="ExternalService", portName="ExternalPort")
@SecurityDomain("EQueue")
//@RolesAllowed ({"EQueueWS"})
@WebContext(contextRoot = "/JBv5-JBv5-EQueue", urlPattern = "/ExternalWSBean", authMethod = "BASIC", transportGuarantee = "NONE", secureWSDLAccess = false)
@Remote(ExternalWS.class)
public class ExternalWSBean implements ExternalWS{

	private static final long serialVersionUID = -8430635966944491477L;
	private Logger logger = Logger.getLogger(getClass());
	@EJB HDouRequestsLocal douRequests;
	@EJB HRegisterLocal registers;
	@EJB DousRemote dousRemote;
	@EJB RegionsRemote regionsRemote;
	@EJB Emails emails;
	
	DateUtils dateUtils = new DateUtils();
	
	public RequestOrderInfo getRequestInfo(String rn, Date birthday){
		logger.debug("getRequestInfo("+rn+","+birthday+")");
		return getRequestInfo(rn, birthday, true);
	}
	
	@Override
	public SubmissionPrerequisiteInfo getSubmissionPrerequisite(){
		SubmissionPrerequisiteInfo result = new SubmissionPrerequisiteInfo();
		List<? extends Dou> douDbos =  dousRemote.getAll(Dou.class);
		ConvertHelper.douDBOsToDTOs(douDbos, result.getDous());
		List<? extends Region> regionsDbos = regionsRemote.getAll(Region.class);
		ConvertHelper.regionDBOsToDTOs(regionsDbos, result.getRegions());
		return result;
	}
	

	private RequestOrderInfo getRequestInfo(String rn, Date birthday, boolean calculateWithPriveleges) {
		if(birthday==null )
			throw new RuntimeException("birthday parameter mustn't be empty");
		if(rn==null || rn.isEmpty())
			throw new RuntimeException("rn parameter mustn't be empty");
		
		DouRequestsSearchParams params = new DouRequestsSearchParams();
//		params.permit = false;
		params.rn=new StringFilter(rn);
		params.birthday = DateFilter.build(dateUtils.getStartOfDay(birthday), dateUtils.getEndOfDay(birthday));
//		params.birthdayFrom = dateUtils.getStartOfDay(birthday);
//		params.birthdayTo = dateUtils.getEndOfDay(birthday);
		
		logger.debug("[0] getRequestInfo("+rn+")  HDouRequest search   params.birthday: "+params.birthday);
		List<HDouRequest> search = douRequests.search(params);
		logger.debug("[1] getRequestInfo("+rn+")  HDouRequest search: "+search);
		if(search==null || search.isEmpty())
			return null;
		if(search.size() != 1)
			throw new RuntimeException();
		
		HDouRequest req = search.get(0);
		RequestOrderInfo requestInfo = new RequestOrderInfo();
		setupRnInfo(req, requestInfo);
		requestInfo.setPrivilege(req.getPrivilege()!=null);
		
		switch(req.getStatus()){
			case DELETE_BY_7_YEAR: 
			case DELETE_BY_PARRENTS_AGREE: 
			case PERMIT:
				fillRegisterData(requestInfo, req);
				break;
			case WAIT: 
				calculeteOrderNumber(requestInfo, req, rn, calculateWithPriveleges);
				break;
		}		
		logger.debug("requestInfo.getPrivius10(): "+ requestInfo.getPrivius10());
		return requestInfo;
	}

	private void setupRnInfo(HDouRequest req, RequestInfo requestInfo) {
		requestInfo.setFirstName( req.getChild().getName());
		requestInfo.setLastName(  req.getChild().getSurname());
		requestInfo.setMiddleName(req.getChild().getPatronymic());
		requestInfo.setRn(req.toString());
		requestInfo.setStatus(req.getStatus()==null?null:req.getStatus().getDescription());
		requestInfo.setPrivilege(req.getPrivilege() != null);
	}

	/** 
	 *  Если выдана путвка, выводить номер протокола, дату протокола, и номер доу куда выдана путевка,. 
	 *  Если ребенка вернули в очередь, после протокола, выводить опять номер очереди согласно фильтрам. 
	 */
	private void fillRegisterData(RequestOrderInfo requestInfo, HDouRequest req) {
		RegisterSearchParams params = new RegisterSearchParams();
		params.douRequestUid =  new LongFilter (req.getUId());
		List<HRegister> search = registers.search(params);
		if(search==null || search.isEmpty()){
			return;
		}else{
			HRegister older=null;
			for (HRegister hRegister : search) {
				if(older==null || older.getRegisterDate().before(hRegister.getRegisterDate()))
					older = hRegister;
			}
			requestInfo.setProtokolDate(older.getRegisterDate());
			requestInfo.setProtokolNum(older.getDnumber() + "");
			requestInfo.setProtokolReceiveDueDate(older.getReceivePermitDueDate());
			
			List<SRegister> specificationLines = registers.getSpecificationLines(older);
			for (SRegister sRegister : specificationLines) {
				logger.debug("sRegister: "+ sRegister+ "   sRegister.getDou(): " + sRegister.getDou());
				if(sRegister.getDouRequest().equals(req)){
					if(sRegister.getDou()!=null)
						requestInfo.setProtokolDouName(sRegister.getDou().getName());
					break;
				}
			}
		}
		
		
	}

	/**
	 * Вычисляем порядковый очередь данного РН в активной очереди
	 * @param requestInfo
	 * @param req
	 * @param rn
	 * @param calculateWithPriveleges учитывать ли льготную очередь
	 */
	private void calculeteOrderNumber(RequestOrderInfo requestInfo, HDouRequest req, String rn, boolean calculateWithPriveleges) {
		List<SDouRequest> spec = douRequests.getSpecificationLines(req);
		logger.debug("[2] getRequestInfo("+rn+")  spec: "+spec);
		DouRequestsSearchParams searchParams = new DouRequestsSearchParams();
		searchParams.orderStatus = OrderStatus.ACTIVE_QUEUE;
		searchParams.minRn=rn;
		
		/* общий номер в очереди безучета льготы */
		Integer  	continuousOrder = douRequests.getSearchCount(searchParams);
		requestInfo.setContinuousOrder(continuousOrder);
//		
		if(calculateWithPriveleges){
			if(requestInfo.getPrivilege()){
				searchParams.privilege = Boolean.TRUE;
			} else {
				searchParams.privilegesToBegin = Boolean.TRUE;
			}
		}
		
		/* общий номер в очереди */
		Integer order = douRequests.getSearchCount(searchParams);
		logger.debug("[3] getRequestInfo("+rn+")  getSearchCount: "+order);
		requestInfo.setOrder(order);
		
		
		/* внутри года */
		Date startOfYear = dateUtils.getStartOfYear(req.getChild().getBirthday());
		Date endOfYear = dateUtils.getEndOfYear  (req.getChild().getBirthday());
		searchParams.birthday = DateFilter.build(startOfYear, endOfYear) ;
		Integer orderByBirthdayYear = douRequests.getSearchCount(searchParams);
		requestInfo.setOrderByYear(orderByBirthdayYear);
		searchParams.birthday = null;
		
		
		List<Long> allDous= new ArrayList<Long>();
		
		searchParams.searchedDous = new ArrayList<Long>();
		for (SDouRequest s : spec) {
			if(s.getArticleUnit()!=null){
				Dou dou = (Dou)s.getArticleUnit();
				RequestToDouInfo douInfo = new RequestToDouInfo();
				douInfo.setDouType(dou.getDouType());
				douInfo.setNumber(dou.getNumber());
				douInfo.setName(dou.getName());
				douInfo.setRegion(dou.getRegion() == null ? null: dou.getRegion().getName());
				
//				/* Общая очередь в САД */
				searchParams.searchedDous.clear();
				searchParams.searchedDous.add(dou.getUId());
//				Эта информация не востребована... прибиваем для ускорения работы сервиса
//				order = douRequests.getSearchCount(searchParams);
//				douInfo.setOrder(order);
				
				
				/* Очередь в сад внутри года */
				Date startOfYear2 = dateUtils.getStartOfYear(req.getChild().getBirthday());
				Date endOfYear2 = dateUtils.getEndOfYear  (req.getChild().getBirthday());
				searchParams.birthday = DateFilter.build(startOfYear2, endOfYear2) ;
				orderByBirthdayYear = douRequests.getSearchCount(searchParams);
				douInfo.setOrderByBirthdayYear(orderByBirthdayYear);
				
				
				
				/* Предидущие 10 в группу садов и по году рождения */
				searchParams.setFrom(1);
				searchParams.setTo(10);
				if(calculateWithPriveleges)
					searchParams.getSortInfo().add(new SortedColumnInfo("privilege", false));
				searchParams.getSortInfo().add(new SortedColumnInfo("rn", false));
//				logger.debug("Search privius 10: ");
				List<HDouRequest> search = douRequests.search(searchParams);
//				logger.debug("Search privius 10: "+ search.size());
//				logger.debug("Search privius 10: "+ search);
				for (HDouRequest hDouRequest : search) {
					RequestInfo re = new RequestInfo();
					setupRnInfo(hDouRequest , re);
//					logger.debug("requestInfo: "+ re);
					douInfo.getPrivius10().add(re);
				}
				searchParams.setFrom(null);
				searchParams.setTo(null);
				searchParams.getSortInfo().clear();
				
				
				
				searchParams.birthday = null;
				
				
				
				
				logger.debug("[4] getRequestInfo("+rn+")  getSearchCount("+dou.getUId()+"): "+order   +"   ByYear: "+orderByBirthdayYear);
				requestInfo.getDous().add(douInfo);
				
				allDous.add(dou.getUId());
			}
		}
		
		/* В группу садов */
		searchParams.searchedDous = allDous;
		order = douRequests.getSearchCount(searchParams);
		requestInfo.setOrderToDous(order);
		
		/* В группу садов по году рождения */
		Date startOfYear2 = dateUtils.getStartOfYear(req.getChild().getBirthday());
		Date endOfYear2 = dateUtils.getEndOfYear  (req.getChild().getBirthday());
		searchParams.birthday = DateFilter.build(startOfYear2, endOfYear2); 
		order = douRequests.getSearchCount(searchParams);
		requestInfo.setOrderToDousByYear(order);
		
		/* Предидущие 10 в группу садов и по году рождения */
		searchParams.setFrom(1);
		searchParams.setTo(10);
		if(calculateWithPriveleges)
			searchParams.getSortInfo().add(new SortedColumnInfo("privilege", false));
		searchParams.getSortInfo().add(new SortedColumnInfo("rn", false));
//		logger.debug("Search privius 10: ");
		List<HDouRequest> search = douRequests.search(searchParams);
//		logger.debug("Search privius 10: "+ search.size());
//		logger.debug("Search privius 10: "+ search);
		for (HDouRequest hDouRequest : search) {
			RequestInfo re = new RequestInfo();
			setupRnInfo(hDouRequest , re);
//			logger.debug("requestInfo: "+ re);
			requestInfo.getPrivius10().add(re);
		}
		
		
	}

	@Override
	public RequestOrderInfo getRequestInfoIgnorePrivileges(String rn,
			Date birthday) {
		return getRequestInfo(rn, birthday, false);
	}

	@Override
	public String sayHello() {
		try {
			emails.create(new EmailMessage(null, "apv@jbalance.org", "apv@jbalance.org,d.e.rodionov@gmail.ru", null, null, "Привет! Это я, Жбосс 4!", "Тестируем ассинхронную отправку", ContentType.TEXT));
//			EmailHelper emailHelper = new EmailHelper("EQueue@jboss4", "apv@jbalance.org,d.rodionov@jbalance.org", null, null, "Привет! Это я, Жбосс 4!");
//			emailHelper.setTextBody("Проверяем кириллицу.");
//			emailHelper.send();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "Hello, Dear!";
	}

	
}
