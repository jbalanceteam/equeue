package org.itx.jbalance.l2_api.ws;


import java.rmi.Remote;
import java.util.Date;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import org.itx.jbalance.l2_api.dto.RequestOrderInfo;
import org.itx.jbalance.l2_api.dto.submission.SubmissionPrerequisiteInfo;
/**
 * Web-service для выгрузки очередников на сайт
 * @author apv
 */
@WebService(name="ExternalWSBean")  
@SOAPBinding(style=Style.DOCUMENT)
public interface ExternalWS	extends Remote {
	public static final String JNDI_NAME="ExternalWS/remote";
	@WebMethod
	public RequestOrderInfo getRequestInfo(
			@WebParam(name="rn")String rn,
			@WebParam(name="birthday") Date birthday
	);
	
	
	@WebMethod
	public String sayHello();
	
	/**
	 * Для отображения формы Submission, требуются данные
	 * НАпример список ДОУ
	 * @return
	 */
	@WebMethod
	public SubmissionPrerequisiteInfo getSubmissionPrerequisite();
	
	
	@WebMethod
	public RequestOrderInfo getRequestInfoIgnorePrivileges(
			@WebParam(name="rn")String rn,
			@WebParam(name="birthday") Date birthday
	);
	
}
