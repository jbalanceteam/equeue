package org.itx.jbalance.l2_api.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.s.SDouRequest;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l0.s.SRegister;
import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l1.d.HDouRequestsLocal;
import org.itx.jbalance.l1.d.HRegisterLocal;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;
import org.itx.jbalance.l2_api.utils.ConvertHelper;
import org.itx.jbalance.l2_api.utils.DousCache;
import org.itx.jbalance.l2_api.utils.RegisterDocNumbersCache;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.logging.Logger;
import static org.itx.jbalance.l1.utils.DateUtils.moveDatesToBounds;

@Stateless
@RemoteBinding(jndiBinding = DouRequestService.JNDI_NAME)
public class DouRequestServiceBean  implements DouRequestService{

	private static final long serialVersionUID = -8430635966944491477L;
	Logger logger = Logger.getLogger(getClass());
	public DouRequestServiceBean() {
		
	}

	@EJB(name = Constants.JNDI_HDOUREQUESTS_LOCAL) HDouRequestsLocal douRequestsLocal;
	@EJB(name=Constants.JNDI_HREGISTER_LOCAL) HRegisterLocal douRegisterLocal;//

//	@EJB(name="Sessionprofile/local") Sessionprofile sessionprofile;
	
//	public void moveDatesToBounds(DateFilter df) {
//		if(df==null)
//			return;
//		if(df.getFilterValues().get(0)!=null){
//			Date date = df.getFilterValues().get(0);
//			date.setTime( new DateUtils().getStartOfDay(date).getTime());
//		}
//		if(df.getFilterValues().get(1)!=null){
//			Date date = df.getFilterValues().get(1);
//			date.setTime( new DateUtils().getEndOfDay(date).getTime());
//		}
//	}
	
	@Override
	public List<DouRequestDTO> getDouRequests(DouRequestsSearchParams params) {
//		douRequestsLocal.setRights(sessionprofile);
		logger.debug("getDouRequests()");
		
		moveDatesToBounds(params.birthday);
		moveDatesToBounds(params.regDate);
		moveDatesToBounds(params.sverkDate);
		
		List<HDouRequest> dboList = douRequestsLocal.search(params);
		ArrayList<DouRequestDTO> result = new ArrayList<DouRequestDTO>();
		for (HDouRequest hDouRequest : dboList) {
			DouRequestDTO douRequestDTO = new DouRequestDTO();
			ConvertHelper.douRequestsDBOToDTO(hDouRequest, douRequestDTO);
			

			String dousByRequestId = DousCache.getInstatce().getDousByRequestId(hDouRequest.getUId());
			String dousR1ByRequestId = DousCache.getInstatce().getDousRating1(hDouRequest.getUId());
			String dousR2ByRequestId = DousCache.getInstatce().getDousRating2(hDouRequest.getUId());
			String dousR3ByRequestId = DousCache.getInstatce().getDousRating3(hDouRequest.getUId());
			if(dousByRequestId==null){
				List<SDouRequest> spec = douRequestsLocal.getSpecificationLines(hDouRequest);
				StringBuffer dous=new StringBuffer();
				StringBuffer dousRating1=new StringBuffer();
				StringBuffer dousRating2=new StringBuffer();
				StringBuffer dousRating3=new StringBuffer();
				for (SDouRequest s : spec) {
					if(s.getArticleUnit()!=null){
						String idDou = s.getArticleUnit().getNumber();
						
						dous.append(idDou);
						dous.append(", ");
						
						if(s.getRating() == 3){
							
							dousRating3.append(idDou);
							dousRating3.append(", ");
						} else if(s.getRating() == 2){
							dousRating2.append(idDou);
							dousRating2.append(", ");
						} else if(s.getRating() == 1){
							dousRating1.append(idDou);
							dousRating1.append(", ");
						}
					}
				}
				if(dous.length()>1){
					dous.delete(dous.length()-2, dous.length());
				}
				
				if(dousRating1.length()>1){
					dousRating1.delete(dousRating1.length()-2, dousRating1.length());
				}
				if(dousRating2.length()>1){
					dousRating2.delete(dousRating2.length()-2, dousRating2.length());
				}
				if(dousRating3.length()>1){
					dousRating3.delete(dousRating3.length()-2, dousRating3.length());
				}
				
				dousByRequestId = dous.toString();
				dousR1ByRequestId = dousRating1.toString();
				dousR2ByRequestId = dousRating2.toString();
				dousR3ByRequestId = dousRating3.toString();
				DousCache.getInstatce().put(hDouRequest.getUId(), dousByRequestId, dousR1ByRequestId, dousR2ByRequestId, dousR3ByRequestId);
			}
			douRequestDTO.setDous(dousByRequestId);
			douRequestDTO.setDousRating1(dousR1ByRequestId);
			douRequestDTO.setDousRating2(dousR2ByRequestId);
			douRequestDTO.setDousRating3(dousR3ByRequestId);
			
			//registerDocNumbers
			String registerDocNumbersByRequestId = RegisterDocNumbersCache.getInstatce().getRegisterDocNumbersByRequestId(hDouRequest.getUId());
			if(dousByRequestId==null){
				List<SRegister> specByDouRequest = douRegisterLocal.getSpecByDouRequest(hDouRequest.getUId());
				StringBuffer registerDocNumbers_buf=new StringBuffer();
				for (SRegister sReg : specByDouRequest) {
					
					if(sReg.getHUId().getStatus().getNumber() == 3){
						continue;
					}
					registerDocNumbers_buf.append(sReg.getHUId());
					registerDocNumbers_buf.append(", ");
				}
				if(registerDocNumbers_buf.length()>1)
					registerDocNumbers_buf.deleteCharAt(registerDocNumbers_buf.length()-2);
				
				registerDocNumbersByRequestId = registerDocNumbers_buf.toString();
				RegisterDocNumbersCache.getInstatce().put(hDouRequest.getUId(), registerDocNumbersByRequestId);
			}
			
			douRequestDTO.setRegisterDocNumbers(registerDocNumbersByRequestId);
			result.add(douRequestDTO);
		}
		return result;
	}
}
