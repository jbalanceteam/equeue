package org.itx.jbalance.l2_api.services;


import java.util.List;

import javax.ejb.Remote;

import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l1.api.RegisterSearchParams;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterLineDTO;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterPrerequisiteInfo;
import org.itx.jbalance.l2_api.dto.equeue.RegisterDTO;

@Remote
public interface RegisterService {
	
	public final static String JNDI_NAME="RegisterService/remote";
	
	List<RegisterDTO> getRegisters(RegisterSearchParams params);
	
	AutoRegisterPrerequisiteInfo getAutoRegisterPrerequisiteInfo();

	HAutoRegister autoRegister(HAutoRegister autoRegister, List<AutoRegisterLineDTO> lines);
}
