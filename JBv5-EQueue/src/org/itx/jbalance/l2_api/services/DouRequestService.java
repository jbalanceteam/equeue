package org.itx.jbalance.l2_api.services;


import java.util.List;

import javax.ejb.Remote;

import org.itx.jbalance.l1.api.DouRequestsSearchParams;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;

@Remote
public interface DouRequestService {
	
	public final static String JNDI_NAME="DouRequestService/remote";
	
	List<DouRequestDTO> getDouRequests(DouRequestsSearchParams params);
}
