package org.itx.jbalance.l2_api.services;


import javax.ejb.Remote;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsResult;

@Remote
@SOAPBinding(style=Style.DOCUMENT)
public interface AttendanceService {
	
	public final static String JNDI_NAME="AttendanceService/remote";
	
	GetPermitsResult getPermits(GetPermitsRequest getPermitsRequest);
}
