package org.itx.jbalance.l2_api.services;

import static org.itx.jbalance.l1.utils.DateUtils.moveDatesToBounds;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;

import org.itx.jbalance.l0.Age;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.Permit;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.o.Permits;
import org.itx.jbalance.l2_api.dto.PrivilegeDTO;
import org.itx.jbalance.l2_api.dto.attendance.ChildDTO;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsRequest;
import org.itx.jbalance.l2_api.dto.attendance.GetPermitsResult;
import org.itx.jbalance.l2_api.dto.attendance.PermitDTO;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.logging.Logger;
import org.jboss.wsf.spi.annotation.WebContext;

@Stateless(name = "AttendanceService")
@RemoteBinding(jndiBinding = AttendanceService.JNDI_NAME)
//@SecurityDomain("EQueue")
@WebService  
@WebContext(contextRoot = "/JBv5-JBv5-EQueue", urlPattern = "/AttendanceServiceBean", authMethod = "BASIC", transportGuarantee = "NONE", secureWSDLAccess = false)
public class AttendanceServiceBean  implements AttendanceService{

	private static final long serialVersionUID = -8430635966944491477L;
	Logger logger = Logger.getLogger(getClass());
	public AttendanceServiceBean() {
		
	}

//	@EJB(name = Constants.JNDI_HDOUREQUESTS_LOCAL) HDouRequestsLocal douRequestsLocal;
//	@EJB(name= Constants.JNDI_HREGISTER_LOCAL) HRegisterLocal hRegisterLocal;
	@EJB(name= Constants.JNDI_PERMITS_LOCAL) Permits permits;
	
	
	private static DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
	/**
	 * 
	 */
	@Override
	public GetPermitsResult getPermits(GetPermitsRequest getPermitsRequest) {
		
		moveDatesToBounds(getPermitsRequest.getBirthday());
		moveDatesToBounds(getPermitsRequest.getPermitDate());
		
		GetPermitsResult getPermitsResult = new GetPermitsResult();
		
		List<Permit> list = permits.search(getPermitsRequest);
		Integer searchCount = permits.getSearchCount(getPermitsRequest);
		Integer allCount = permits.getAllCount();
		
		getPermitsResult.setPermitsByQueryCount(searchCount);
		getPermitsResult.setPermitsCount(allCount);
		
		logger.debug("getPermitsRequest: " + getPermitsRequest);
		for (Permit p : list) {
			PermitDTO  dto = new PermitDTO();
			dto.setUId(p.getUId());
			dto.setDouNumber(p.getDou().getNumber() + " (" + p.getDou().getName() +")");
			dto.setPermitDate(p.getPermitDate());
			dto.setPermitNumber(p.getSRegister() + "");
			dto.setPermitStatus(p.getPermitStatus());
			Age age = new Age();
			int monthes = p.getAge();
			age.setYears(monthes / 12);
			age.setMonths(monthes % 12);
			dto.setAgeOnPermitDate(age);
			
			if(p.getPrivilege() != null){
				PrivilegeDTO privilege = new PrivilegeDTO();
				privilege.setName(p.getPrivilege().getName());
				privilege.setUId(p.getPrivilege().getUId());
				dto.setPrivilege(privilege);
			}
			
			HRegister register = p.getSRegister().getHUId();
			Date registerDate = register.getRegisterDate();
			dto.setRegisterDate(registerDate);
			dto.setRegisterUid(register.getUId());
			dto.setRegisterDateFormatted(dateFormat.format(registerDate));
			
			dto.setRegisterNumber(register.getDnumber() + "");
			dto.setRegNumber(p.getHDouRequest() + "");
			dto.setRegUid(p.getHDouRequest().getUId());
			dto.setUId(p.getUId());
			
			ChildDTO child = new ChildDTO();
			Physical childDbo = p.getChild();
			child.setBirthday(childDbo.getBirthday());
			child.setBirthdayFormated(dateFormat.format(childDbo.getBirthday()));
			child.setFirstName(childDbo.getRealName());
			child.setLastName(childDbo.getSurname());
			child.setPatronymic(childDbo.getPatronymic());
			dto.setChild(child);
			getPermitsResult.getPermits().add(dto);
		}
		logger.debug("getPermitsResult: " + getPermitsResult);
		logger.trace("getPermitsResult.getPermits(): " + getPermitsResult.getPermits());
		return getPermitsResult;
	}

	
}
