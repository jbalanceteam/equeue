package org.itx.jbalance.l2_api.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.itx.jbalance.l0.h.HAutoRegister;
import org.itx.jbalance.l0.h.HRegister;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l1.Constants;
import org.itx.jbalance.l1.api.RegisterSearchParams;
import org.itx.jbalance.l1.d.HAutoRegisterLocal;
import org.itx.jbalance.l1.d.HAutoRegisterRemote;
import org.itx.jbalance.l1.d.HRegisterLocal;
import org.itx.jbalance.l1.o.DOUGroupsRemote;
import org.itx.jbalance.l1.o.DousRemote;
import org.itx.jbalance.l1.o.RegionsRemote;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterLineDTO;
import org.itx.jbalance.l2_api.dto.equeue.AutoRegisterPrerequisiteInfo;
import org.itx.jbalance.l2_api.dto.equeue.RegisterDTO;
import org.itx.jbalance.l2_api.utils.ConvertHelper;
import org.jboss.annotation.ejb.RemoteBinding;
import org.jboss.annotation.ejb.TransactionTimeout;
import org.jboss.logging.Logger;
import static org.itx.jbalance.l1.utils.DateUtils.moveDatesToBounds;

@Stateless(name = "RegisterService")
@RemoteBinding(jndiBinding = RegisterService.JNDI_NAME)
public class RegisterServiceBean implements RegisterService {

	private static final long serialVersionUID = -8430635966944491477L;
	Logger logger = Logger.getLogger(getClass());

	public RegisterServiceBean() {

	}

	@EJB(name = Constants.JNDI_HREGISTER_LOCAL)
	HRegisterLocal hRegisterLocal;
	
	@EJB(name = Constants.JNDI_DOUS_REMOTE)
	DousRemote dousRemote;
	
	@EJB(name = Constants.JNDI_DOUGROUPS_REMOTE)
	DOUGroupsRemote douGroupsRemote;
	
	@EJB(name = Constants.JNDI_REGIONS_REMOTE)
	RegionsRemote regionsRemote;
	
	@EJB(name = Constants.JNDI_HAUTOREGISTER_LOCAL)
	HAutoRegisterLocal autoRegisterLocal;

	@Override
	public List<RegisterDTO> getRegisters(RegisterSearchParams params) {
		logger.debug("getRegister()");

		moveDatesToBounds(params.registerDate);

		List<HRegister> dboList = hRegisterLocal.search(params);
		ArrayList<RegisterDTO> result = new ArrayList<RegisterDTO>();
		for (HRegister hRegister : dboList) {
			RegisterDTO registerDTO = new RegisterDTO();

			Integer specCount = hRegisterLocal.getSpecCount(hRegister.getUId());
			registerDTO.setChildrenCount(specCount);
			registerDTO.setUId(hRegister.getUId());
			registerDTO.setMember1(hRegister.getMember1());
			registerDTO.setMember2(hRegister.getMember2());
			registerDTO.setMember3(hRegister.getMember3());
			registerDTO.setDocNumber(hRegister.getDnumber() + "");
			registerDTO.setRegisterDate(hRegister.getRegisterDate());
			registerDTO.setStatus(hRegister.getStatus());
			registerDTO.setReceiveDueDate(hRegister.getReceivePermitDueDate());

			result.add(registerDTO);
		}

		return result;
	}

	@Override
	public AutoRegisterPrerequisiteInfo getAutoRegisterPrerequisiteInfo() {
		AutoRegisterPrerequisiteInfo autoRegisterPrerequisiteInfo = new AutoRegisterPrerequisiteInfo();
		List<? extends Dou> allDous = dousRemote.getAll(Dou.class);
		ConvertHelper.douDBOsToDTOs(allDous, autoRegisterPrerequisiteInfo.getDous());
		autoRegisterPrerequisiteInfo.getAgeGroups().addAll(douGroupsRemote.getAll());
		ConvertHelper.regionDBOsToDTOs(regionsRemote.getAll(Region.class), autoRegisterPrerequisiteInfo.getRegions());
		return autoRegisterPrerequisiteInfo;
	}

	@TransactionTimeout(3600)
	@org.jboss.ejb3.annotation.TransactionTimeout(3600)
	@Override
	public HAutoRegister autoRegister(HAutoRegister autoRegister,
			List<AutoRegisterLineDTO> lines) {
		List<? extends Dou> allDous = dousRemote.getAll(Dou.class);
		for (AutoRegisterLineDTO line : lines) {
			autoRegister.addSAutoRegister(getDou(allDous, line.getDouId()), line.getAgeGroupId(), line.getCnt());
		}
		return autoRegisterLocal.autoRegister(autoRegister);
	}
	
	private Dou getDou(List<? extends Dou> allDous, Long uid){
		for (Dou dou : allDous) {
			if(dou.getUId().equals(uid)){
				return dou;
			}
		}
		return null;
	}
}
