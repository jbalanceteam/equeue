package org.itx.jbalance.l2_api.dto;

import java.util.Date;
import java.util.Map;

public class DouRequestDTO extends CommonDTO {

	public DouRequestDTO() {
		super();
	}

	private static final long serialVersionUID = 1L;

	// персональная информация о ребенке
	private String firstName;
	private String lastName;
	private String patronymic;
	/**
	 * 'm', 'f'
	 */
	private char sex;
	private Date birthDate;
	//private String birthDate;
	private String bsSeria;
	private String bsNumber;
	private Date bsDate;
	//private String bsDate;

	// информация о детских садах и рейтингом
	private Map<Long, Integer> douUids;
	
	private Integer yearEnter;

	// адрес проживания ребенка
	private String childAddress;


	// дополнительно
	private Long privilegeUid;
	private String comments;
	private String phoneNumber;
	
	private String  declarantLastname;
	private String  declarantFirstname;
	private String  declarantPatronymic;
	private String  declarantPassportSeria;
	private String  declarantPassportNumber;
	private Date declarantPassportIssue;
	private String  declarantPassportAuthority;
	
	private String email;

///////////////////////////////////////////////////////	
	public String getChildAddress() {
		return childAddress;
	}
	
	public void setChildAddress(String childAddress) {
		this.childAddress = childAddress;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

//	public Date getBirthDate() {
//		return birthDate;
//	}
//
//	public void setBirthDate(Date birthDate) {
//		this.birthDate = birthDate;
//	}

	public String getBsSeria() {
		return bsSeria;
	}

	public void setBsSeria(String bsSeria) {
		this.bsSeria = bsSeria;
	}

	public String getBsNumber() {
		return bsNumber;
	}

	public void setBsNumber(String bsNumber) {
		this.bsNumber = bsNumber;
	}

	public Integer getYearEnter() {
		return yearEnter;
	}

	public void setYearEnter(Integer yearEnter) {
		this.yearEnter = yearEnter;
	}

	
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public char getSex() {
		return sex;
	}

	public void setSex(char sex) {
		this.sex = sex;
	}

	public Map<Long, Integer> getDouUids() {
		return douUids;
	}

	public void setDouUids(Map<Long, Integer> douUid) {
		this.douUids = douUid;
	}

	public Long getPrivilegeUid() {
		return privilegeUid;
	}

	public void setPrivilegeUid(Long privilegeUid) {
		this.privilegeUid = privilegeUid;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getBsDate() {
		return bsDate;
	}

	public void setBsDate(Date bsDate) {
		this.bsDate = bsDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDeclarantLastname() {
		return declarantLastname;
	}

	public void setDeclarantLastname(String declarantLastname) {
		this.declarantLastname = declarantLastname;
	}

	public String getDeclarantFirstname() {
		return declarantFirstname;
	}

	public void setDeclarantFirstname(String declarantFirstname) {
		this.declarantFirstname = declarantFirstname;
	}

	public String getDeclarantPatronymic() {
		return declarantPatronymic;
	}

	public void setDeclarantPatronymic(String declarantPatronymic) {
		this.declarantPatronymic = declarantPatronymic;
	}

	public String getDeclarantPassportSeria() {
		return declarantPassportSeria;
	}

	public void setDeclarantPassportSeria(String declarantPassportSeria) {
		this.declarantPassportSeria = declarantPassportSeria;
	}

	public String getDeclarantPassportNumber() {
		return declarantPassportNumber;
	}

	public void setDeclarantPassportNumber(String declarantPassportNumber) {
		this.declarantPassportNumber = declarantPassportNumber;
	}

	public Date getDeclarantPassportIssue() {
		return declarantPassportIssue;
	}

	public void setDeclarantPassportIssue(Date declarantPassportIssue) {
		this.declarantPassportIssue = declarantPassportIssue;
	}

	public String getDeclarantPassportAuthority() {
		return declarantPassportAuthority;
	}

	public void setDeclarantPassportAuthority(String declarantPassportAuthority) {
		this.declarantPassportAuthority = declarantPassportAuthority;
	}
}
