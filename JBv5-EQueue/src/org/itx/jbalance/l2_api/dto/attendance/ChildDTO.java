package org.itx.jbalance.l2_api.dto.attendance;

import java.util.Date;

import org.itx.jbalance.l0.Age;
import org.itx.jbalance.l2_api.dto.CommonDTO;
/**
 * Ребенок
 * @author apv
 *
 */
public class ChildDTO extends CommonDTO{

	private static final long serialVersionUID = 1L;

	private String firstName;

	private String lastName;

	private String patronymic;
	
	private Date birthday;
	
	private Age age;
	/**
	 * Из-за проблем с датами в GWT, будем передавать ее строкой, уже отформатированную
	 */
	private String birthdayFormated;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPatronymic() {
		return patronymic;
	}

	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	
	

	public String getBirthdayFormated() {
		return birthdayFormated;
	}

	public void setBirthdayFormated(String birthdayFormated) {
		this.birthdayFormated = birthdayFormated;
	}
	
	

	public Age getAge() {
		return age;
	}

	public void setAge(Age age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "ChildDTO [firstName=" + firstName + ", lastName=" + lastName
				+ ", patronymic=" + patronymic + ", birthday=" + birthday + "]";
	}
	
	
	
	
}
