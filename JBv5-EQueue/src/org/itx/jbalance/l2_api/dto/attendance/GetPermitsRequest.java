package org.itx.jbalance.l2_api.dto.attendance;

import org.itx.jbalance.l1.api.SearchParams;
import org.itx.jbalance.l1.api.filters.DateFilter;
import org.itx.jbalance.l1.api.filters.IntegerFilter;
import org.itx.jbalance.l1.api.filters.LongFilter;

/**
 * Запрос на получение списка путевок.
 * Этот класс будет содержать фильтры.
 * @author apv
 *
 */
public class GetPermitsRequest extends SearchParams{

	private static final long serialVersionUID = 1L;

	/**
	 * Поиск сразу по множеству полей
	 */
	private String searchByAllTextFields;
	/**
	 * Поиск путевок в конкретные ДОУ
	 */
	private LongFilter dous;
	/**
	 * Дата выдачи путевки с и по. 
	 */
	private DateFilter permitDate;
	/**
	 * Поле дата рождения
	 */
	private DateFilter birthday;
	
	private IntegerFilter age;
	/**
	 * Поиск по определенной отготе
	 */
	private LongFilter exact_privilege;
	/**
	 * Фильтр по наличию льготы
	 */
	private Boolean privilege;
	
	
	private LongFilter childUid;
	
	
	public String getSearchByAllTextFields() {
		return searchByAllTextFields;
	}
	public void setSearchByAllTextFields(String searchByAllTextFields) {
		this.searchByAllTextFields = searchByAllTextFields;
	}
	public LongFilter getDous() {
		return dous;
	}
	public void setDous(LongFilter dous) {
		this.dous = dous;
	}
	public DateFilter getPermitDate() {
		return permitDate;
	}
	public void setPermitDate(DateFilter permitDate) {
		this.permitDate = permitDate;
	}
	public DateFilter getBirthday() {
		return birthday;
	}
	public void setBirthday(DateFilter birthday) {
		this.birthday = birthday;
	}
	public IntegerFilter getAge() {
		return age;
	}
	public void setAge(IntegerFilter age) {
		this.age = age;
	}
	public LongFilter getExact_privilege() {
		return exact_privilege;
	}
	public void setExact_privilege(LongFilter exact_privilege) {
		this.exact_privilege = exact_privilege;
	}
	public Boolean getPrivilege() {
		return privilege;
	}
	public void setPrivilege(Boolean privilege) {
		this.privilege = privilege;
	}

	public LongFilter getChildUid() {
		return childUid;
	}
	public void setChildUid(LongFilter childUid) {
		this.childUid = childUid;
	}
	
	@Override
	public String toString() {
		return "GetPermitsRequest [searchByAllTextFields="
				+ searchByAllTextFields + ", dous=" + dous + ", permitDate="
				+ permitDate + ", birthday=" + birthday + ", age=" + age
				+ ", exact_privilege=" + exact_privilege + ", privilege="
				+ privilege + "]";
	}
	
	
	
	
	
	
}
