package org.itx.jbalance.l2_api.dto.attendance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class GetPermitsResult implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * Всего по запросу найдено
	 */
	private Integer permitsByQueryCount;
	
	/**
	 * Всего путевок в системе
	 */
	private Integer permitsCount;
	
	List<PermitDTO> permits = new ArrayList<PermitDTO>();

	public List<PermitDTO> getPermits() {
		return permits;
	}

	public void setPermits(List<PermitDTO> permits) {
		this.permits = permits;
	}

	public Integer getPermitsByQueryCount() {
		return permitsByQueryCount;
	}

	public void setPermitsByQueryCount(Integer permitsByQueryCount) {
		this.permitsByQueryCount = permitsByQueryCount;
	}

	public Integer getPermitsCount() {
		return permitsCount;
	}

	public void setPermitsCount(Integer permitsCount) {
		this.permitsCount = permitsCount;
	}
	
	
	
	
}
