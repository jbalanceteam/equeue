package org.itx.jbalance.l2_api.dto.attendance;

import java.util.Date;

import org.itx.jbalance.l0.Age;
import org.itx.jbalance.l0.o.Permit.PermitStatus;
import org.itx.jbalance.l2_api.dto.CommonDTO;
import org.itx.jbalance.l2_api.dto.PrivilegeDTO;
import org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO;
/**
 * Путевка в ДОУ
 * 
 * @author apv
 *
 */
public class PermitDTO extends CommonDTO{

	private static final long serialVersionUID = 1L;
	
	private ChildDTO child;
	
	
	private Age ageOnPermitDate;
	
	
//	private DouRequestDTO douRequest;
	/**
	 * Регистрационный номер
	 */
	private String regNumber;
	/**
	 * UID РН
	 */
	private Long regUid;
	
	private String douNumber;
	
	/**
	 * Номер путевки.
	 */
	private String permitNumber;
	
	/**
	 * Номер протокола.
	 */
	private String registerNumber;
	/**
	 * Номер протокола.
	 */
	private Long registerUid;
	
	/**
	 * Номер протокола.
	 */
	private String registerDateFormatted;
	
	/**
	 * Номер протокола.
	 */
	private Date registerDate;
	/**
	 * льгота
	 */
	private PrivilegeDTO privilege;
	
	/**
	 * Дата путевки
	 */
	private Date permitDate;
	public ChildDTO getChild() {
		return child;
	}
	public void setChild(ChildDTO child) {
		this.child = child;
	}
	public String getPermitNumber() {
		return permitNumber;
	}
	public void setPermitNumber(String permitNumber) {
		this.permitNumber = permitNumber;
	}
	public Date getPermitDate() {
		return permitDate;
	}
	public void setPermitDate(Date permitDate) {
		this.permitDate = permitDate;
	}
	
	
	
	
	public String getRegNumber() {
		return regNumber;
	}
	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}
	//	public DouRequestDTO getDouRequest() {
//		return douRequest;
//	}
//	public void setDouRequest(DouRequestDTO douRequest) {
//		this.douRequest = douRequest;
//	}
	
	private PermitStatus permitStatus;
	
	@Override
	public String toString() {
		return "PermitDTO [child=" + child + ", permitNumber=" + permitNumber
				+ ", permitDate=" + permitDate + "]";
	}
	public String getRegisterNumber() {
		return registerNumber;
	}
	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}
	public String getRegisterDateFormatted() {
		return registerDateFormatted;
	}
	public void setRegisterDateFormatted(String registerDateFormatted) {
		this.registerDateFormatted = registerDateFormatted;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public String getDouNumber() {
		return douNumber;
	}
	public void setDouNumber(String douNumber) {
		this.douNumber = douNumber;
	}
	public Age getAgeOnPermitDate() {
		return ageOnPermitDate;
	}
	public void setAgeOnPermitDate(Age ageOnPermitDate) {
		this.ageOnPermitDate = ageOnPermitDate;
	}
	public PrivilegeDTO getPrivilege() {
		return privilege;
	}
	public void setPrivilege(PrivilegeDTO privilege) {
		this.privilege = privilege;
	}
	public PermitStatus getPermitStatus() {
		return permitStatus;
	}
	public void setPermitStatus(PermitStatus permitStatus) {
		this.permitStatus = permitStatus;
	}
	public Long getRegUid() {
		return regUid;
	}
	public void setRegUid(Long regUid) {
		this.regUid = regUid;
	}
	public Long getRegisterUid() {
		return registerUid;
	}
	public void setRegisterUid(Long registerUid) {
		this.registerUid = registerUid;
	}
	
	
	
	
	
	

}
