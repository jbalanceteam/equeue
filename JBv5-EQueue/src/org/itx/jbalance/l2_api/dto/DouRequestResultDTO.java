package org.itx.jbalance.l2_api.dto;

public class DouRequestResultDTO extends ResultDTO {

	private static final long serialVersionUID = 1L;

	private String dnumber;

	public String getDnumber() {
		return dnumber;
	}

	public void setDnumber(String dnumber) {
		this.dnumber = dnumber;
	}
}
