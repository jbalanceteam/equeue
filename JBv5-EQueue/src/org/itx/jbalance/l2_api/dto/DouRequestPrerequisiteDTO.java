package org.itx.jbalance.l2_api.dto;

import java.io.Serializable;
import java.util.List;

/**
 * DouRequestPrerequisiteDTO содержит все данные которые могут понадобиться для
 * составления запроса о постановке ребенка на учет *
 * 
 * @author shan
 * 
 */
public class DouRequestPrerequisiteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2216158479130624099L;

	public DouRequestPrerequisiteDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	private List<RegionDTO> regionsDTOs;
	private List<DouDTO> dousDTOs;
	private List<PrivilegeDTO> privilegeDtos;

	public List<RegionDTO> getRegionsDTOs() {
		return regionsDTOs;
	}

	public void setRegionsDTOs(List<RegionDTO> regionsDTOs) {
		this.regionsDTOs = regionsDTOs;
	}

	public List<DouDTO> getDousDTOs() {
		return dousDTOs;
	}

	public void setDousDTOs(List<DouDTO> dousDTOs) {
		this.dousDTOs = dousDTOs;
	}

	public List<PrivilegeDTO> getPrivilegeDtos() {
		return privilegeDtos;
	}

	public void setPrivilegeDtos(List<PrivilegeDTO> privilegeDtos) {
		this.privilegeDtos = privilegeDtos;
	}
}
