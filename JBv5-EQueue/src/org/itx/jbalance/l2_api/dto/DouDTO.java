package org.itx.jbalance.l2_api.dto;


public class DouDTO extends CommonDTO {


	public DouDTO() {
		super();
	}

	private static final long serialVersionUID = 4055094638288434834L;
	
	private String number;
	
	private String douType;
	
	private String name;
	
	private String address;

	private Long regionUid;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDouType() {
		return douType;
	}

	public void setDouType(String douType) {
		this.douType = douType;
	}

	public Long getRegionUid() {
		return regionUid;
	}

	public void setRegionUid(Long regionUid) {
		this.regionUid = regionUid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	

}
