package org.itx.jbalance.l2_api.dto;

import java.io.Serializable;


//import org.itx.jbalance.l2_api.dto.CommonDTO.Status;

public class ResultDTO extends CommonDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public ResultDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	//private static final long serialVersionUID = -3974498065275700224L;
	private Status status;
	private String message;

	public enum Status {
		SUCCESS, FAIL
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
