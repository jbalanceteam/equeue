package org.itx.jbalance.l2_api.dto.equeue;

import java.io.Serializable;

public class AutoRegisterLineDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public AutoRegisterLineDTO() {
		super();
	}
	
	public AutoRegisterLineDTO(Long douId, Integer ageGroupId, Integer cnt) {
		super();
		this.douId = douId;
		this.ageGroupId = ageGroupId;
		this.cnt = cnt;
	}
	private Long douId;
	private Integer ageGroupId;
	private Integer cnt;
	public Long getDouId() {
		return douId;
	}
	public Integer getAgeGroupId() {
		return ageGroupId;
	}
	public Integer getCnt() {
		return cnt;
	}
	
	
	
	
	
}
