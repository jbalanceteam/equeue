package org.itx.jbalance.l2_api.dto.equeue;

import java.util.Date;

import org.itx.jbalance.l0.Age;
import org.itx.jbalance.l0.h.RecordColor;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l2_api.dto.CommonDTO;
import org.itx.jbalance.l2_api.dto.PrivilegeDTO;

/**
 * ДТО для списка заявок
 * @author apv
 *
 */
public class DouRequestDTO extends CommonDTO{

	public DouRequestDTO() {
		super();
	}

	private static final long serialVersionUID = 1L;
	/***** Request fields  *******/
	private Date regDate;
	private String address;
	private String status;
	private Integer year;
	private PrivilegeDTO privilege;
	private String comments;
	private String oldNumber;
	private Date sverkDate;
	private RecordColor recordColor;
	private Age age;
	/**
	 * Список всех ДОУ через запятую
	 */
	private String dous;
	/**
	 * Список ДОУ с рейтингом 1
	 */
	private String dousRating1;
	/**
	 * Список ДОУ с рейтингом 2
	 */
	private String dousRating2;
	/**
	 * Список ДОУ с рейтингом 3
	 */
	private String dousRating3;
	private String registerDocNumbers;
	
	public String getRegisterDocNumbers() {
		return registerDocNumbers;
	}


	public void setRegisterDocNumbers(String registerDocNumbers) {
		this.registerDocNumbers = registerDocNumbers;
	}

	private String regNumber;
	
	/***** Child fields  *******/
	private String RealName;
	private String Surname; 
	/**
	 * Отчество
	 */
	private String Patronymic;
	private String phone;
	
	/**
	 * Дата рождения
	 */
	private Date birthday;
	private String birthdayFormated;
	
	/***** Birthday sertificate fields  *******/
	/**
	 * Серия и номер паспорта
	 * TODO физ.лицо может иметь несколько паспортов?
	 */
	private String birthdaySertificateSeria;
	private String birthdaySertificateNumber;
	private Date birthdaySertificateDate;
	/**
	 * Опекун 1 - Мать
	 */
	private String guardian1;
	/**
	 * Опекун 2 - Отец
	 */
	private String guardian2;
	
	
	private Sex sex;
	
	private String declarant;
	
	private String email;


	public Date getRegDate() {
		return regDate;
	}

	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public PrivilegeDTO getPrivilege() {
		return privilege;
	}

	public void setPrivilege(PrivilegeDTO privilege) {
		this.privilege = privilege;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getOldNumber() {
		return oldNumber;
	}

	public void setOldNumber(String oldNumber) {
		this.oldNumber = oldNumber;
	}

	public Date getSverkDate() {
		return sverkDate;
	}

	public void setSverkDate(Date sverkDate) {
		this.sverkDate = sverkDate;
	}

	public RecordColor getRecordColor() {
		return recordColor;
	}

	public void setRecordColor(RecordColor recordColor) {
		this.recordColor = recordColor;
	}

	public String getDous() {
		return dous;
	}

	public void setDous(String dous) {
		this.dous = dous;
	}

	public String getRealName() {
		return RealName;
	}

	public void setRealName(String realName) {
		RealName = realName;
	}

	public String getSurname() {
		return Surname;
	}

	public void setSurname(String surname) {
		Surname = surname;
	}

	public String getPatronymic() {
		return Patronymic;
	}

	public void setPatronymic(String patronymic) {
		Patronymic = patronymic;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getBirthdaySertificateSeria() {
		return birthdaySertificateSeria;
	}

	public void setBirthdaySertificateSeria(String birthdaySertificateSeria) {
		this.birthdaySertificateSeria = birthdaySertificateSeria;
	}

	public String getBirthdaySertificateNumber() {
		return birthdaySertificateNumber;
	}

	public void setBirthdaySertificateNumber(String birthdaySertificateNumber) {
		this.birthdaySertificateNumber = birthdaySertificateNumber;
	}

	public Date getBirthdaySertificateDate() {
		return birthdaySertificateDate;
	}

	public void setBirthdaySertificateDate(Date date) {
		this.birthdaySertificateDate = date;
	}

	public String getGuardian1() {
		return guardian1;
	}

	public void setGuardian1(String guardian1) {
		this.guardian1 = guardian1;
	}

	public String getGuardian2() {
		return guardian2;
	}

	public void setGuardian2(String guardian2) {
		this.guardian2 = guardian2;
	}

	public Sex getSex() {
		return sex;
	}

	public void setSex(Sex sex) {
		this.sex = sex;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getBirthdayFormated() {
		return birthdayFormated;
	}

	public void setBirthdayFormated(String birthdayFormated) {
		this.birthdayFormated = birthdayFormated;
	}

	public Age getAge() {
		return age;
	}

	public void setAge(Age age) {
		this.age = age;
	}

	public String getRegNumber() {
		return regNumber;
	}

	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}

	public String getDeclarant() {
		return declarant;
	}

	public void setDeclarant(String declarant) {
		this.declarant = declarant;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDousRating1() {
		return dousRating1;
	}

	public void setDousRating1(String dousRating1) {
		this.dousRating1 = dousRating1;
	}

	public String getDousRating2() {
		return dousRating2;
	}

	public void setDousRating2(String dousRating2) {
		this.dousRating2 = dousRating2;
	}

	public String getDousRating3() {
		return dousRating3;
	}

	public void setDousRating3(String dousRating3) {
		this.dousRating3 = dousRating3;
	}
}
