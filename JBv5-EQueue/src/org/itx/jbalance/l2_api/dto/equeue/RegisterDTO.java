package org.itx.jbalance.l2_api.dto.equeue;

import java.util.Date;

import org.itx.jbalance.l0.h.RegisterStatus;
import org.itx.jbalance.l2_api.dto.CommonDTO;


/**
 * ДТО протокола
 * @author apv
 *
 */
public class RegisterDTO extends CommonDTO{

	public RegisterDTO() {
		super();
	}


	private static final long serialVersionUID = 1L;
	
	private Date registerDate;
	private Date receiveDueDate;
	private String member1;
	private String member2;
	private String member3;
	private String docNumber;
	private Integer childrenCount;
	private RegisterStatus status;
	private Date ageReprocessDate;

	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public String getMember1() {
		return member1;
	}
	public void setMember1(String member1) {
		this.member1 = member1;
	}
	public String getMember2() {
		return member2;
	}
	public void setMember2(String member2) {
		this.member2 = member2;
	}
	public String getMember3() {
		return member3;
	}
	public void setMember3(String member3) {
		this.member3 = member3;
	}
	public String getDocNumber() {
		return docNumber;
	}
	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}
	public Integer getChildrenCount() {
		return childrenCount;
	}
	public void setChildrenCount(Integer childrenCount) {
		this.childrenCount = childrenCount;
	}
	public RegisterStatus getStatus() {
		return status;
	}
	public void setStatus(RegisterStatus status) {
		this.status = status;
	}
	public Date getReceiveDueDate() {
		return receiveDueDate;
	}
	public void setReceiveDueDate(Date receiveDueDate) {
		this.receiveDueDate = receiveDueDate;
	}
	public Date getAgeReprocessDate() {
		return ageReprocessDate;
	}
	public void setAgeReprocessDate(Date ageReprocessDate) {
		this.ageReprocessDate = ageReprocessDate;
	}
	
	
	
	
	
	
}
