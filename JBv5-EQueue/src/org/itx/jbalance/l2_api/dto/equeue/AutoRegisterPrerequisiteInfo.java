package org.itx.jbalance.l2_api.dto.equeue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.l0.o.DOUGroups;
import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.RegionDTO;
/**
 * Данные для отображения формы АВТОПРОТОКОЛ
 * @author apv
 *
 */
public class AutoRegisterPrerequisiteInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<DouDTO> dous;
	private List<RegionDTO> regionsDTOs;
	private List<DOUGroups> ageGroups;
	
	public List<DouDTO> getDous() {
		if(dous == null){
			dous = new ArrayList<DouDTO>();
		}
		return dous;
	}

	public void setDous(List<DouDTO> dous) {
		this.dous = dous;
	}
	
	public List<RegionDTO> getRegions() {
		if(regionsDTOs == null){
			regionsDTOs = new ArrayList<RegionDTO>();
		}
		return regionsDTOs;
	}

	public void setRegions(List<RegionDTO> regionsDTOs) {
		this.regionsDTOs = regionsDTOs;
	}

	public List<DOUGroups> getAgeGroups() {
		if(ageGroups == null){
			ageGroups = new ArrayList<DOUGroups>();
		}
		return ageGroups;
	}

	public void setAgeGroups(List<DOUGroups> ageGroups) {
		this.ageGroups = ageGroups;
	}

	
	
}
