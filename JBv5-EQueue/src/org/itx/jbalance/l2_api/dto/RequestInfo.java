package org.itx.jbalance.l2_api.dto;

import java.io.Serializable;

public class RequestInfo implements Serializable{

	public RequestInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;

	private Boolean privilege;
	/**
	 * рн 
	 */
	private String rn;
	/**
	 * Имя 
	 */
	private String firstName;
	/**
	 * Фамилия
	 */
	private String lastName;
	/**
	 * Отчество
	 */
	private String middleName;
	

	/**
	 * Статус рег номера:
	 * = Ожидание
	 * = Выписана путевка
	 * = Убрали по достижению 7-и лет
	 * = Убрали по заявлению родителей
	 */
	private String status;
	
	public String getRn() {
		return rn;
	}

	public void setRn(String rn) {
		this.rn = rn;
	}

	
	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getPrivilege() {
		return privilege;
	}

	public void setPrivilege(Boolean privilege) {
		this.privilege = privilege;
	}
	
	@Override
	public String toString() {
		return "RequestInfo [rn=" + rn + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", middleName=" + middleName
				+ ", status=" + status + "]";
	}
	
	
	
}
