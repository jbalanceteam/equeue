package org.itx.jbalance.l2_api.dto;

public class PrivilegeDTO extends CommonDTO {
	public PrivilegeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = -4171292306687715934L;

	private String Name;

	
	public String getName() {
		return Name;
	}


	public void setName(String name) {
		Name = name;
	}
	
	@Override
	public String toString() {
		return Name;
	}
	
}
