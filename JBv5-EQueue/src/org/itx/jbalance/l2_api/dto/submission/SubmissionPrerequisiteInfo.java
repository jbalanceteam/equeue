package org.itx.jbalance.l2_api.dto.submission;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.RegionDTO;
/**
 * Данные для отображения формы Submissions
 * @author apv
 *
 */
public class SubmissionPrerequisiteInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	private List<DouDTO> dous;
	private List<RegionDTO> regionsDTOs;
	
	public List<DouDTO> getDous() {
		if(dous == null){
			dous = new ArrayList<DouDTO>();
		}
		return dous;
	}

	public void setDous(List<DouDTO> dous) {
		this.dous = dous;
	}
	
	public List<RegionDTO> getRegions() {
		if(regionsDTOs == null){
			regionsDTOs = new ArrayList<RegionDTO>();
		}
		return regionsDTOs;
	}

	public void setRegions(List<RegionDTO> regionsDTOs) {
		this.regionsDTOs = regionsDTOs;
	}

	
}
