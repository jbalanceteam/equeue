package org.itx.jbalance.l2_api.dto;


public class RegionDTO extends CommonDTO {
	public RegionDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = -4171292306687715913L;

	private String Name;

	
	public String getName() {
		return Name;
	}


	public void setName(String name) {
		Name = name;
	}
	
	@Override
	public String toString() {
		return Name;
	}
	

}
