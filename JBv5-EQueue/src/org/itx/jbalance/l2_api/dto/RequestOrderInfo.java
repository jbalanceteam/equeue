package org.itx.jbalance.l2_api.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RequestOrderInfo extends RequestInfo{

	public RequestOrderInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;

	
	/**
	 * порядковый номер по всей текущей очереди 
	 */
	private Integer order;
	/**
	 * порядковый номер в группу ДОУ 
	 */
	private Integer orderToDous;
	
	/**
	 * порядковый номер в группу ДОУ по году роджения
	 */
	private Integer orderToDousByYear;
	
	/**
	 * порядковый номер по году роджения
	 * Сколько детей такого же года роджения стоит перед ним.
	 */
	private Integer orderByYear;
	/**
	 * порядковый номер без учета льготы, годарождения и.т.д.
	 */
	private Integer continuousOrder;
	
	
	/**
	 * Если выдана путвка, это поле будет содержать дату протокола
	 * иначе null
	 */
	private Date protokolDate;
	/**
	 * Если выдана путвка, это поле будет содержать номер протокола
	 * иначе null
	 */
	private String protokolNum;
	/**
	 * Если выдана путвка, это поле будет содержать наименование ДОУ
	 * иначе null
	 */
	private String protokolDouName;
	/**
	 * список ДОУ + порядковый номер в каждый из них для данного рн 
	 */
	private List<RequestToDouInfo> dous = new ArrayList<RequestToDouInfo>();
	
	 
	 /**
	  * Предидущие РН
	  */
	private List<RequestInfo> privius10  = new ArrayList<RequestInfo>();
	
	
	
	/**
	 * Дата обращения за путевкой
	 * Дата обращения - уведомление получателя о том, до какой даты он должен прийти в УО для получения путевки
	 */
	private Date protokolReceiveDueDate;


	

	public Integer getOrder() {
		return order;
	}

	public void setOrder(Integer order) {
		this.order = order;
	}
	public Integer getOrderToDous() {
		return orderToDous;
	}

	public void setOrderToDous(Integer orderToDous) {
		this.orderToDous = orderToDous;
	}

	
	public Integer getOrderToDousByYear() {
		return orderToDousByYear;
	}

	public void setOrderToDousByYear(Integer orderToDousByYear) {
		this.orderToDousByYear = orderToDousByYear;
	}

	
	public Date getProtokolDate() {
		return protokolDate;
	}

	public void setProtokolDate(Date protokolDate) {
		this.protokolDate = protokolDate;
	}

	public String getProtokolNum() {
		return protokolNum;
	}

	public void setProtokolNum(String protokolNum) {
		this.protokolNum = protokolNum;
	}

	public String getProtokolDouName() {
		return protokolDouName;
	}

	public void setProtokolDouName(String protokolDouName) {
		this.protokolDouName = protokolDouName;
	}
	
	public List<RequestToDouInfo> getDous() {
		return dous;
	}

	public void setDous(List<RequestToDouInfo> dous) {
		this.dous = dous;
	}

	public List<RequestInfo> getPrivius10() {
		return privius10;
	}

	public void setPrivius10(List<RequestInfo> privius10) {
		this.privius10 = privius10;
	}

	public Integer getOrderByYear() {
		return orderByYear;
	}

	public void setOrderByYear(Integer orderByYear) {
		this.orderByYear = orderByYear;
	}

	

	public void setContinuousOrder(Integer continuousOrder) {
		this.continuousOrder = continuousOrder;
	}
	
	public Integer getContinuousOrder() {
		return continuousOrder;
	}

	public Date getProtokolReceiveDueDate() {
		return protokolReceiveDueDate;
	}

	public void setProtokolReceiveDueDate(Date protokolReciveDueDate) {
		this.protokolReceiveDueDate = protokolReciveDueDate;
	}

	
}
