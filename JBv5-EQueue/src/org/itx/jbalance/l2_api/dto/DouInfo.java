package org.itx.jbalance.l2_api.dto;

public class DouInfo{
	private String number;
	private String douType;
	private String region;
	private String name;
	
	
	public DouInfo() {
		super();
	}
	public DouInfo(String number, String douType, String region, String name) {
		super();
		this.number = number;
		this.douType = douType;
		this.region = region;
		this.name = name;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getDouType() {
		return douType;
	}
	public void setDouType(String douType) {
		this.douType = douType;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
