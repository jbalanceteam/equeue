package org.itx.jbalance.l2_api.dto;

import java.io.Serializable;

public class CommonDTO implements Serializable{
	
	public CommonDTO() {
		super();
	}

	private static final long serialVersionUID = 1L;
	
	private Long UId;
	
	public Long getUId() {
		return UId;
	}

	public void setUId(Long uId) {
		UId = uId;
	}

	
}
