package org.itx.jbalance.l2_api.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RequestToDouInfo implements Serializable{
	private static final long serialVersionUID = 6122480002755126521L;
	/**
	 * @deprecated use number field instead
	 */
	@Deprecated
	private int idDou;
	private String number;
	private String douType;
	private String region;
	private String name;
	
	private Integer orderByBirthdayYear;
	private Integer order;
	
	/**
	  * Предидущие РН
	  */
	 List<RequestInfo> privius10  = new ArrayList<RequestInfo>();
	
	public RequestToDouInfo() {
		super();
	}
	public RequestToDouInfo(int idDou, String douType, String region, String name) {
		super();
		this.idDou = idDou;
		this.douType = douType;
		this.region = region;
		this.name = name;
	}
	public int getIdDou() {
		return idDou;
	}
	public void setIdDou(int idDou) {
		this.idDou = idDou;
	}
	public String getDouType() {
		return douType;
	}
	public void setDouType(String douType) {
		this.douType = douType;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getOrderByBirthdayYear() {
		return orderByBirthdayYear;
	}
	public void setOrderByBirthdayYear(Integer orderByBirthdayYear) {
		this.orderByBirthdayYear = orderByBirthdayYear;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public List<RequestInfo> getPrivius10() {
		return privius10;
	}
	public void setPrivius10(List<RequestInfo> privius10) {
		this.privius10 = privius10;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
	
	
}
