package org.itx.jbalance.l2_api.utils;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;



public class DousCache {

	private static DousCache instatce = new DousCache();
	
	public static DousCache getInstatce(){
		return instatce;
	}
	
	private Map<Long, RatedDousIds> cache = new Hashtable<Long, RatedDousIds>();
	
	private DousCache(){
		
	}
	
	
	public void put(Long requestID, String all, String rating1, String rating2, String rating3){
		cache.put(requestID, new RatedDousIds(rating1, rating2, rating3, all));
	}
	
	public void remove(Long requestID){
		cache.remove(requestID);
	}
	
	public void clear(){
		cache.clear();
	}
	
	public String getDousByRequestId(Long requestID){
		RatedDousIds ratedDousIds = cache.get(requestID);
		if(ratedDousIds != null){
			return ratedDousIds.all;
		} else {
			return null;
		}
	}
	
	
	public String getDousRating1(Long requestID){
		RatedDousIds ratedDousIds = cache.get(requestID);
		if(ratedDousIds != null){
			return ratedDousIds.rating1;
		} else {
			return null;
		}
	}
	
	public String getDousRating2(Long requestID){
		RatedDousIds ratedDousIds = cache.get(requestID);
		if(ratedDousIds != null){
			return ratedDousIds.rating2;
		} else {
			return null;
		}
	}
	
	public String getDousRating3(Long requestID){
		RatedDousIds ratedDousIds = cache.get(requestID);
		if(ratedDousIds != null){
			return ratedDousIds.rating3;
		} else {
			return null;
		}
	}
	
	/**
	 * В связи с введением рейтинга, мы будем хранить 4 троки:
	 * ДОУ с рейтингом 1
	 * ДОУ с рейтингом 2
	 * ДОУ с рейтингом 3
	 * ВСЕ ДОУ - для обратной совместимости
	 * @author apv
	 *
	 */
	private class RatedDousIds implements Serializable{
		private String rating1;
		private String rating2;
		private String rating3;
		private String all;
		private RatedDousIds(String rating1, String rating2, String rating3,
				String all) {
			super();
			this.rating1 = rating1;
			this.rating2 = rating2;
			this.rating3 = rating3;
			this.all = all;
		}
		
		
	}
	
}
