package org.itx.jbalance.l2_api.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.itx.jbalance.l0.AgeCalculator;
import org.itx.jbalance.l0.h.HDouRequest;
import org.itx.jbalance.l0.o.BirthSertificate;
import org.itx.jbalance.l0.o.BirthSertificate.Sex;
import org.itx.jbalance.l0.o.Dou;
import org.itx.jbalance.l0.o.Physical;
import org.itx.jbalance.l0.o.Privilege;
import org.itx.jbalance.l0.o.Region;
import org.itx.jbalance.l2_api.dto.DouDTO;
import org.itx.jbalance.l2_api.dto.DouRequestDTO;
import org.itx.jbalance.l2_api.dto.PrivilegeDTO;
import org.itx.jbalance.l2_api.dto.RegionDTO;

public class ConvertHelper {
	public static void regionDBOsToDTOs(List<? extends Region> dbos, List<RegionDTO> dtos) {
		for (Region dbo : dbos) {
			RegionDTO dto = new RegionDTO();
			dto.setUId(dbo.getUId());
			dto.setName(dbo.getName());
			dtos.add(dto);
		}
	}

	public static void douDBOsToDTOs(List<? extends Dou> douDbos, List<DouDTO> douDtos) {
		for (Dou dbo : douDbos) {
			DouDTO dto = new DouDTO();
			dto.setUId(dbo.getUId());
			dto.setNumber(dbo.getNumber());
			dto.setName(dbo.getName());
			dto.setAddress(dbo.getPAddress());
			dto.setRegionUid(dbo.getRegion().getUId());
			douDtos.add(dto);
		}

	}

	public static void convertDouRequestDTOtoPhysicalDBO(
			DouRequestDTO dousRequestDTO, Physical childDBO) {
		childDBO.setSurname(dousRequestDTO.getLastName());
		childDBO.setName(dousRequestDTO.getFirstName());
		childDBO.setRealName(dousRequestDTO.getFirstName());
		childDBO.setPatronymic(dousRequestDTO.getPatronymic());
		childDBO.setBirthday(dousRequestDTO.getBirthDate());
		childDBO.setEMail(dousRequestDTO.getEmail());
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//		try {
//			childDBO.setBirthday(format.parse(dousRequestDTO.getBirthDate()));
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
		childDBO.setPhone(dousRequestDTO.getPhoneNumber());
	}

	public static void convertDouRequestDTOtoBirthSertificateDBO(
			DouRequestDTO dousRequestDTO, BirthSertificate birthSertificateDBO) {
		//SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		birthSertificateDBO.setSex(dousRequestDTO.getSex()=='m'?Sex.M:Sex.W);
		birthSertificateDBO.setSeria(dousRequestDTO.getBsSeria());
		birthSertificateDBO.setNumber(dousRequestDTO.getBsNumber());
		birthSertificateDBO.setDate(dousRequestDTO.getBsDate());
	}
	
	public static void convertDouRequestDTOtoDeclarant(
			DouRequestDTO dousRequestDTO, Physical declarantDBO) {
		declarantDBO.getPassport().setSeria(dousRequestDTO.getDeclarantPassportSeria());
		declarantDBO.getPassport().setNumber(dousRequestDTO.getDeclarantPassportNumber());
		declarantDBO.getPassport().setDateOfIssue(dousRequestDTO.getDeclarantPassportIssue());
		declarantDBO.getPassport().setAuthority(dousRequestDTO.getDeclarantPassportAuthority());
		declarantDBO.setName(dousRequestDTO.getDeclarantFirstname());
		declarantDBO.setRealName(dousRequestDTO.getDeclarantFirstname());
		declarantDBO.setSurname(dousRequestDTO.getDeclarantLastname());
		declarantDBO.setPatronymic(dousRequestDTO.getDeclarantPatronymic());
	}

	public static void privilegeDBOsToDTOs(List<Privilege> privilegeDbos,
			List<PrivilegeDTO> privilegeDtos) {
		for (Privilege privilegeDBO : privilegeDbos) {
			PrivilegeDTO privilegeDto = new PrivilegeDTO();
			privilegeDto.setUId(privilegeDBO.getUId());
			privilegeDto.setName(privilegeDBO.getName());
			privilegeDtos.add(privilegeDto);
		}
	}
	
	
	private static DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
	
	public static void douRequestsDBOToDTO(HDouRequest dbo,
			org.itx.jbalance.l2_api.dto.equeue.DouRequestDTO dto) {
		
			dto.setUId(dbo.getUId());
			dto.setAddress(dbo.getAddress());
			dto.setBirthday(dbo.getChild().getBirthday());
			dto.setComments(dbo.getComments());
			dto.setBirthdaySertificateDate(dbo.getBirthSertificate().getDate());
			dto.setBirthdaySertificateNumber(dbo.getBirthSertificate().getNumber());
			dto.setBirthdaySertificateSeria(dbo.getBirthSertificate().getSeria());
			dto.setGuardian1(dbo.getBirthSertificate().getGuardian1());
			dto.setGuardian2(dbo.getBirthSertificate().getGuardian2());
			dto.setOldNumber(dbo.getOldNumber());
			dto.setPatronymic(dbo.getChild().getPatronymic());
			dto.setEmail(dbo.getChild().getEMail());

			final StringBuffer declarantSb = new StringBuffer();
			if(dbo.getDeclarant() != null){
				declarantSb.append(dbo.getDeclarant().getSurname());
				declarantSb.append(" ");
				declarantSb.append(dbo.getDeclarant().getRealName());
				declarantSb.append(" ");
				declarantSb.append(dbo.getDeclarant().getPatronymic());
				declarantSb.append(" ");
				if(dbo.getDeclarant().getPassport() != null){
					declarantSb.append(dbo.getDeclarant().getPassport().getSeria());
					declarantSb.append(" ");
					declarantSb.append(dbo.getDeclarant().getPassport().getNumber());
					
					if(dbo.getDeclarant().getPassport().getDateOfIssue()!=null){
						declarantSb.append(" ");
						declarantSb.append(dateFormat.format(dbo.getDeclarant().getPassport().getDateOfIssue()));
					}
					declarantSb.append(" ");
					declarantSb.append(dbo.getDeclarant().getPassport().getAuthority());
				}
			}
			
			if(dbo.getDeclarantStr() != null && !dbo.getDeclarantStr().isEmpty()){
				declarantSb.append(" ");
				declarantSb.append(dbo.getDeclarantStr());
			}
			dto.setDeclarant(declarantSb.toString());
			
			
			if( dbo.getPrivilege()!=null){
				PrivilegeDTO privilege = new PrivilegeDTO();
				privilege.setName(dbo.getPrivilege().getName());
				privilege.setUId(dbo.getPrivilege().getUId());
				dto.setPrivilege(privilege); 
			}
			dto.setRealName(dbo.getChild().getRealName());
			dto.setRecordColor(dbo.getRecordColor());
			dto.setRegDate(dbo.getRegDate());
			dto.setSex(dbo.getBirthSertificate().getSex());
			dto.setStatus(dbo.getStatus().getDescription());
			dto.setSurname(dbo.getChild().getSurname());
			dto.setSverkDate(dbo.getSverkDate());
			
			dto.setYear(dbo.getYear());
			dto.setPhone(dbo.getChild().getPhone());
			if(dbo.getChild().getBirthday()!=null){
				dto.setBirthdayFormated(dateFormat.format(dbo.getChild().getBirthday()));
			}
			dto.setAge(AgeCalculator.calculateAge(dbo.getChild().getBirthday(), new Date()));
			dto.setRegNumber(dbo.toString());
	}

}
