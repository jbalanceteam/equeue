package org.itx.jbalance.l2_api.utils;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

import org.jboss.logging.Logger;

@MessageDriven(activationConfig =   {
        @ActivationConfigProperty(propertyName="destinationType", propertyValue="javax.jms.Queue"),
        @ActivationConfigProperty(propertyName="destination", propertyValue=CacheHandlerMDB.DESTINITION)
        })
public class CacheHandlerMDB implements MessageListener {
	public final static String DESTINITION = "queue/dousCache";

	Logger logger = Logger.getLogger(getClass());
	
	@Override
	public void onMessage(Message message) {
		logger.debug("onMessage("+ message+")");
		try {
			long requestUid = message.getLongProperty("uid");
			DousCache.getInstatce().remove(requestUid);
			RegisterDocNumbersCache.getInstatce().remove(requestUid);
		} catch (JMSException e) {
			logger.warn(e);
			DousCache.getInstatce().clear();
			RegisterDocNumbersCache.getInstatce().clear();
		}
	}
}
