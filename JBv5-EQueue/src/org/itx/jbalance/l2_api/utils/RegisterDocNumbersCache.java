package org.itx.jbalance.l2_api.utils;

import java.util.Hashtable;
import java.util.Map;

public class RegisterDocNumbersCache {
	private static RegisterDocNumbersCache instatce = new RegisterDocNumbersCache();
	
	public static RegisterDocNumbersCache getInstatce(){
		return instatce;
	}
	
	private RegisterDocNumbersCache(){
		
	}
	
	private Map<Long, String> cache = new Hashtable<Long, String>();
	
	public void put(Long requestID, String registerDocNumbers){
		cache.put(requestID, registerDocNumbers);
	}
	
	public void remove(Long requestID){
		cache.remove(requestID);
	}
	
	public void clear(){
		cache.clear();
	}
	
	public String getRegisterDocNumbersByRequestId(Long requestID){
		return cache.get(requestID);
	}
	
}
