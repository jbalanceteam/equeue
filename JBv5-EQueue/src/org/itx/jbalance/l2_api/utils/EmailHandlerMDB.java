package org.itx.jbalance.l2_api.utils;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.itx.jbalance.l0.o.EmailMessage;
import org.itx.jbalance.l0.o.EmailMessage.ContentType;
import org.itx.jbalance.l0.o.EmailMessage.Status;
import org.itx.jbalance.l1.o.Emails;
import org.itx.jbalance.l1.utils.EmailHelper;
import org.jboss.logging.Logger;

/**
 * Этот класс используется для ассинхроной рассылки писем
 * 
 * @author apv@jbalance.org
 *
 */
@MessageDriven(activationConfig =   {
        @ActivationConfigProperty(propertyName="destinationType", propertyValue="javax.jms.Queue"),
        @ActivationConfigProperty(propertyName="destination", propertyValue=EmailHandlerMDB.DESTINITION)
        })
public class EmailHandlerMDB implements MessageListener {
	public final static String DESTINITION = "queue/EmailHandler";

	Logger logger = Logger.getLogger(getClass());
	
	@Override
	public void onMessage(Message message) {
		logger.debug("onMessage("+ message+")");
		try {
			Long emailUid = message.getLongProperty("uid");
			if(emailUid == null){
				throw new RuntimeException("uid param is required");
			}
			Emails emailsEjb = getEmails();
			EmailMessage email = emailsEjb.getByUId(emailUid, EmailMessage.class);
			if(email == null){
				throw new RuntimeException("Can not find email recors wit uid = "+emailUid);
			}
			Status newStatus = null;
			
			try{
				switch(email.getStatus()){
					case READY:
						EmailHelper emailHelper = new EmailHelper(email.getSender(), email.getRecipientTo(), email.getRecipientCc(), email.getRecipientBcc(), email.getSubject());
						if(ContentType.HTML.equals(email.getContentType())){
							emailHelper.setHtmlBody(email.getContent());
						}else if(ContentType.TEXT.equals(email.getContentType())){
							emailHelper.setTextBody(email.getContent());
						}
						emailHelper.send();
						newStatus = Status.SENT;
						break;
					case SENT:
						break;
					case ERROR:
						break;
				}
			}catch(Exception e){
				logger.warn("", e);
				newStatus = Status.ERROR;
			}
			if(!newStatus.equals(email.getStatus())){
				email.setStatus(newStatus);
				email = emailsEjb.update(email);
			}
		} catch (JMSException e) {
			logger.warn("", e);
		} 
	}
	
	
	private Emails getEmails(){
		try {
			Context context = new InitialContext();
			return (Emails) context.lookup("Emails/local");
		} catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}
}
